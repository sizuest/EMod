<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<modelDescription>
    <title>Amplifier / power supply</title>
    <content>&lt;p&gt;This model implements the energetic behaviour of an amplifier or power supply control for electric drives based on a Willan's approach. Given the input $P_{dmd}$ as the sum of the power demands of all umpstream component. The model will compute the required electric power \(P_{tot}\) to be supplied to the amplifier or power control in order to meet this demand as follows:
$$P_{tot}= \left\lbrace \begin{matrix}P_{ctrl}+ \frac{P_{dmd}}{\eta\left(P_{dmd}\right)} &amp; \text{if }s=1\\0&amp;\text{else}\end{matrix} \right .$$
\(s\) indicates the value of the state-input (0=OFF, 1=ON) and \(P_{ctrl}\) is the constant control power required by the device.
The load specific efficiency is calculated based on liner interpolation using the efficiency samples \(\underline\eta_s\) and power samples \(\underline P_s\), both with \(N\) elements:
$$\eta\left(P_{dmd}\right)=\left\lbrace\begin{matrix}\eta_{s,1}&amp;\forall\,P_{dmd}\leq P_{s,1}\\\eta_{s,N}&amp;\forall\,P_{dmd}\geq P_{s,N}\\\eta_{s,i}+\frac{\eta_{s,i+1}-\eta_{s,i}}{P_{s,i+1}-P_{s,i}} \cdot \left(P_{dmd}-P_{s,i}\right)&amp;\text{else}\end{matrix}\right .$$
Important: \(P_{s,i}\lt P_{s,i+1}\) must hold \(\forall\,i\in\lbrace 1,2,\ldots,N-1\rbrace\).&lt;/p&gt;
The difference electric power input and output power are assumed to be thermal losses, thus:
$$\dot{Q}_{loss}=P_{tot}-P_{dmd}$$

&lt;p&gt;Two different supplies of electric power to the amplifier or power supply are considered: First the control power required to oeprated the MC of the device (usually 12/24 VDC supply); second, the supply by the net or intermediated DC circuit. The second one is called the &lt;i&gt;supply power&lt;/i&gt; and denoted as \(P_{supp}\):
$$P_{supp}=P_{tot}-P_{ctrl}$$
The useable power of this component type is equal to the power demand of the down-stream components:
$$P_{use}=P_{dmd}$$&lt;/p&gt;</content>
    <image></image>
    <type>Amplifier</type>
    <inputDescriptions>
        <name>State</name>
        <symbol>s</symbol>
        <unit>none</unit>
        <description>Operational state of the component (0=OFF, 1=ON)</description>
    </inputDescriptions>
    <inputDescriptions>
        <name>PDmd</name>
        <symbol>P_{dmd}</symbol>
        <unit>W</unit>
        <description>Electric energy demand of the connected drives</description>
    </inputDescriptions>
    <outputDescriptions>
        <name>PSupply</name>
        <symbol>P_{supp}</symbol>
        <unit>W</unit>
        <description>Electric power demand on the upstream power supply</description>
    </outputDescriptions>
    <outputDescriptions>
        <name>PLoss</name>
        <symbol>\dot{Q}_{loss}</symbol>
        <unit>W</unit>
        <description>Accounting (thermal) losses in the device</description>
    </outputDescriptions>
    <outputDescriptions>
        <name>PTotal</name>
        <symbol>P_{tot}</symbol>
        <unit>W</unit>
        <description>Total electric power demand of the component</description>
    </outputDescriptions>
    <outputDescriptions>
        <name>PUse</name>
        <symbol>P_{use}</symbol>
        <unit>W</unit>
        <description>Electric power supplied to the downstream components</description>
    </outputDescriptions>
    <outputDescriptions>
        <name>Efficiency</name>
        <symbol>\eta</symbol>
        <unit>none</unit>
        <description>Efficiency of the operational point (PUse/PTotal)</description>
    </outputDescriptions>
    <parameterDescriptions>
        <name>Efficiency</name>
        <symbol>\underline\eta_s</symbol>
        <unit>none</unit>
        <description>Vector of the efficiencies corresponding to the power samples</description>
    </parameterDescriptions>
    <parameterDescriptions>
        <name>PowerCtrl</name>
        <symbol>P_{ctrl}</symbol>
        <unit>W</unit>
        <description>Electric power demand of the integrated control unit if the component in ON</description>
    </parameterDescriptions>
    <parameterDescriptions>
        <name>PowerSamples</name>
        <symbol>\underline P_s</symbol>
        <unit>W</unit>
        <description>Sample vector of power demands supplied by the component to the downstream drives</description>
    </parameterDescriptions>
</modelDescription>
