<!DOCTYPE html>
<html>
	<head>
		<script type="text/x-mathjax-config">
			MathJax.Hub.Config({
				TeX: { equationNumbers: { autoNumber: "all" } }
			});
		</script>		<script type="text/x-mathjax-config">
			MathJax.Hub.Config({
				tex2jax: { inlineMath: [['$','$'], ['\\(','\\)']], processEscapes: true }
			});
		</script>		<script src='https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML'></script>
		<link rel="stylesheet" type="text/css" href="../../../help/style.css" />
	</head>
	<body>
		<div><h1>Bearing<br>(Bearing)</h1><hr></div>
		<div><h2>Model description</h2>
			<p>This model represents the dynamics of a roller bearing.
The frictional losses at the rotational speed  $\omega$ are calculated as 
\begin{equation}\label{eq:2_motorBearingQDot}
	\dot{Q}_b=\omega\cdot M_b,
\end{equation}
where as $M_b$ is the frictional torque. To characterize the frictional torque, the approach by <a href="#Harris">Harris and Kotzalas</a> is used:
$$M_b=M_{b,1}+M_{b,2}$$
$M_{b,1}$ describes the frictional torque due to the applied load, while $M_{b,2}$ includes the torque due to viscose friction in the lubricant.
According to <a href="#Palmgren">Palmgren</a>, $M_{b,1}$ depends on the static equivalent load $F_s$, the static basic load rating $C_s$, the applied load $F_\beta$, the bearing diameter $d_b$ and the two empirical parameters $z$ and $y$:
\begin{equation}
	M_{b,1}=z\cdot \left(\frac{F_s}{C_s}\right)^y\cdot F_\beta\cdot d_b
\end{equation}
The applied load depends on the axial and radial factor of the bearing - $X$ and $Y$, the contact angle $\alpha_b$ and the axial and radial force - $F_a$ and $F_r$ - acting on the bearing:
\begin{equation}
	F_\beta = \max\left\lbrace X\cdot F_a\cdot \cot\alpha_b-Y\cdot F_{ro},\cdot F_{r}\right\rbrace
\end{equation}
For radial ball bearings, $X=0.9$ and $Y=0.1$ applies.
The factors for other bearing configurations are listed in <a href="#DIN281">DIN 281</a>.</p>
<p>Harris and Kotzalas choose the empirical parameters $y$ and $z$ depend on $\alpha_b$ and the bearing type as shown in the following table:
<br>
<table>
	<tr><td><b>Bearing Type</b></td><td><b>$\alpha_b$</b></td><td><b>$z$</b></td><td><b>$y$</b></td></tr>
	<tr><td>Radial deep-groove</td><td>0</td><td>0.0005</td><td>0.55</td></tr>
	<tr><td>Angular-contact</td><td>30-40</td><td>0.001</td><td>0.33</td></tr>
	<tr><td>Thrust</td><td>90</td><td>0.0008</td><td>0.33</td></tr>
	<tr><td>Double-row, self-aligning</td><td>10</td><td> 0.0003</td><td>0.40</td></tr>
</table>
</p>
<p>The contribution of viscose friction to the bearing torque can be estimated using the empirical formula by <a href="#Palmgren">Palmgren</a>
\begin{equation}
	M_{b,2}=\begin{cases}	4.5\cdot  10^{-2}\cdot f_0\cdot \left(\nu_{lub}\cdot \omega\right)^{2/3}\cdot d_m^3	& \text{if } \nu_0\cdot n\geq210\cdot 10^{-6}\\
							160\cdot 10^{-7}\cdot f_{0}\cdot d_m^3	& \text{else}
				\end{cases}
\end{equation}
The values suggested for $f_0$ are shown in the following table:
<table>
	<tr><td><b>Bearing Type</b></td><td><b>Grease</b></td><td><b>Oil Mist</b></td><td><b>Oil Bath</b></td><td><b>Oil Jet</b></td></tr>
	<tr><td>Cylindrical roller with cage</td><td>0.8</td><td>2.1</td><td>3.1</td><td>3.1</td></tr>
	 <tr><td>Cylindrical roller full complement</td><td>7.5</td><td> - </td><td>7.5</td><td> -</td></tr>
	 <tr><td>Thrust cylindrical roller</td><td>9</td><td> -	</td><td>3.5</td><td>8</td></tr>
</table></p>
<p>
The heat flux through all $N$ rollers from side 1 to side 2 is given as
\begin{equation}
	\dot{Q}_{1\rightarrow2}=-\dot{Q}_{2\rightarrow1}=N\cdot \lambda_i(d_m, \omega)\cdot (T_1-T_2).
\end{equation}
$\lambda_i$ is the thermal resistance of a single roller and approximated using the approach by <a href="Weidermann">Weidermann</a>:
\begin{equation}
	 \lambda_i(d_m, \omega) \approx \sqrt{14 + 2 \cdot\log \frac{\omega\cdot d_m}{2} - 2 \cdot \log(.007)} \cdot 0.02
\end{equation}
</p>

<ol class="literature">
<li id="Harris">Harris T A and Kotzalas M N (2007) Rolling Bearing Analysis - Essential Concepts of Bearing Technology. CRC Press, Taylor and Francis Group</li>
<li id="Palmgren">Palmgren A (1959) Ball and Roller Bearing Engineering. 3rd ed. Burbank, Philadelphia</span></li>
<li id="DIN281">International Organization for Standardization (ISO) (2007) ISO DIN 281:2007 Rolling bearings - Dynamic load ratings and rating life. Beuth Verlag: Berlin</li>
<li id="Weidermann">Weidermann, F. (2001) �Praxisnahe thermische simulation von Lagern und F�hrungen in Werkzeugmaschinen�, CAD-FEM User�s Meeting 19. Potsdam.</li>
</ol></div>
		<div><h2>Inputs</h2>
			<table>
	<tr>
<td><b>Parameter</b></td><td><b>Symbol</b></td><td><b>Unit</b></td><td><b>Description</b></td></tr>
		<tr><td><i>ForceAxial</i></td><td>\(F_a\)</td><td>N</td><td>Force acting on the bearing in axial direction</td></tr>
		<tr><td><i>ForceRadial</i></td><td>\(F_r\)</td><td>N</td><td>Force acting on the bearing in radial direction</td></tr>
		<tr><td><i>RotSpeed</i></td><td>\(\omega\)</td><td>Hz</td><td>Relative rotational speed between in inner and outer ring of the bearing </td></tr>
		<tr><td><i>Temperature1</i></td><td>\(T_o\)</td><td>K</td><td>Temeprature on side 1 (outer) of the bearing</td></tr>
		<tr><td><i>Temperature2</i></td><td>\(T_i\)</td><td>K</td><td>Temperature on side 2 (inner) if the bearing</td></tr>
</table></div>
		<div><h2>Outputs</h2>
			<table>
	<tr>
<td><b>Parameter</b></td><td><b>Symbol</b></td><td><b>Unit</b></td><td><b>Description</b></td></tr>
		<tr><td><i>Torque</i></td><td>\(M_b\)</td><td>Nm</td><td>Resulting firctional torque</td></tr>
		<tr><td><i>PLoss</i></td><td>\(\dot{Q}_b\)</td><td>W</td><td>Resulting heat loss due to frictional losses</td></tr>
		<tr><td><i>HeatFlux</i></td><td>\(\dot{Q}_{1\rightarrow2}\)</td><td>W</td><td>Heat flux to side 1 (outer) of the bearing</td></tr>
		<tr><td><i>HeatFlux</i></td><td>\(\dot{Q}_{2\rightarrow1}\)</td><td>W</td><td>Heat flux to side 2 (inner) of the bearing</td></tr>
</table></div>
		<div><h2>Parameters</h2>
			<table>
	<tr>
<td><b>Parameter</b></td><td><b>Symbol</b></td><td><b>Unit</b></td><td><b>Description</b></td></tr>
		<tr><td><i>NumRollingBodies</i></td><td>\(N\)</td><td>none</td><td>Number of rolling bodies</td></tr>
		<tr><td><i>MeanDiameter</i></td><td>\(d_m\)</td><td>m</td><td>Mean diameter of the bearing</td></tr>
		<tr><td><i>LubricantMaterial</i></td><td>\(\)</td><td></td><td>Material used for the bearing lubrication</td></tr>
		<tr><td><i>C0</i></td><td>\(C_s\)</td><td>N</td><td>Static basic load rating</td></tr>
		<tr><td><i>ContactAngle</i></td><td>\(\alpha_b\)</td><td>deg</td><td>Contact angle</td></tr>
		<tr><td><i>LubricationFactor</i></td><td>\(f0\)</td><td>none</td><td>Lubrication factor by Harris</td></tr>
</table></div>
	</body>
</html>
