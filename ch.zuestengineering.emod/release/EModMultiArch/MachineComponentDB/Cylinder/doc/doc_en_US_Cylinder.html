<!DOCTYPE html>
<html>
	<head>
		<script type="text/x-mathjax-config">
			MathJax.Hub.Config({
				TeX: { equationNumbers: { autoNumber: "all" } }
			});
		</script>		<script type="text/x-mathjax-config">
			MathJax.Hub.Config({
				tex2jax: { inlineMath: [['$','$'], ['\\(','\\)']], processEscapes: true }
			});
		</script>		<script src='https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML'></script>
		<link rel="stylesheet" type="text/css" href="../../../help/style.css" />
	</head>
	<body>
		<div><h1>Hydraulic Piston<br>(Cylinder)</h1><hr></div>
		<div><h2>Model description</h2>
			In this cyclinder model, the losses are assumed to be thermal and thus leading to a temperature raise in the working fluid.
The generic model consists of a hollow stationary structural part, housing a piston.
The piston divides the inside of the structure into two chambers, denoted as side 1 and 2 here on.
For the following system equations, the piston is assumed to move from the left to the right.
For the opposite case, the labelling has to be adapted accordingly.

Given is the operational point of the cylinder by the desired piston speed $v$ and external force $F_{ext}$.
The seal between the piston and the cylinder structure is assumed to be non ideal, leading to the leakage flow $\dot{V}_{leak}$.
Assuming a constant density, the sum of mass for the cylinder is as follow:
\begin{align}\label{eq:2_hydrCylinderMass}
	\dot{V}_{in} &= v\cdot A_1+\dot{V}_{leak}\\
	\dot{V}_{out} &= v\cdot A_2+\dot{V}_{leak}
\end{align}
Where $A_1=d_{cyl}^2\cdot\pi/4$ is the piston area towards chamber one, and $A_2=(d_{cyl}^2-d_{rod}^2)\cdot\pi/4$ the piston surface towards chamber two.
Following the model by Gnielinski for the pressure loss in an annular flow, the leakage flow can be estimated based on the gap width $\Delta r_{cyl}$ between the cylinder housing and the piston:
\begin{equation}\label{eq:2_hydrCylinderLeak}
	\dot{V}_{leak}=\frac{\left(p_1-p_2\right)\cdot\pi\cdot\left(2\cdot r_{cyl}-\Delta r_{cyl}\right)\cdot\Delta r_{cyl}^3}{8\cdot\nu_{fl}\cdot\rho_{fl}\cdot b_{pis}}
\end{equation}
Where $r_{cyl}$ and $b_{pis}$ are the inner radius of the cylinder and the thickness of the piston respectively.
$\nu_{fl}$ and $\rho_{fl}$ are the kinematic viscosity and density of the fluid.
The total force by the fluid acting on the piston can be obtained by integration of the pressure in both chambers:
\begin{equation}
	F_{tot} = p_1\cdot A_1-p_2\cdot A_2
\end{equation}
Due to friction between the piston and the cylinder, not all of the force $F_{tot}$ is transferred to the shaft.
This losses can be characterized by the cylinders hydraulic-mechanical efficiency $\eta_{cyl}$:
\begin{equation}\label{eq:2_hydrCylinderForce}
	F_{tot}=\frac{F_{ext}}{\eta_{cyl}}
\end{equation}
To move the cylinder, the fluid has to be forced inside chamber 1 through the opening of area $A_{in}$, while the fluid in chamber 2 has to be pushed out through the opening of area $A_{out}$.
Using Bernoulli's principle in combination with (\ref{eq:2_hydrCylinderMass}--\ref{eq:2_hydrCylinderForce}), the required inlet pressure for the desired operational point can be obtained:
\begin{equation}
	p_{in} =\frac{F_{ext}}{A_1\cdot\eta_{cyl}}+\frac{A_2}{A_1}\cdot p_{out}+\rho\cdot\frac{v^2}{2}\cdot\left(1-\frac{A_2}{A_1}\right)+\frac{\rho}{2}\cdot\left(\frac{A_2}{A_1}\cdot\frac{\dot{V}_{out}^2}{A_{out}^2}-\frac{\dot{V}_{in}^2}{A_{in}^2}\right)
\end{equation}
With this equation, the energetic behaviour of hydraulic cylinder is fully described. Based on the operational point $(v,\,F_{ext})$, the pressure and flow rate $(p_{in},\,\dot{V}_{in})$ to be delivered by the up-stream component can be quantified.

For the characterization of the cylinders thermal behaviour, two thermal energy storages are identified: The Cylinder structure and the fluid mass.
Both are characterized by their temperatures -- $T_{cyl}$ and $T_{cyl,fl}$, their specific heat capacities -- $c_{p,cyl}$ and $c_{p,fl}$, and temperatures $T_{cyl}$ and $T_{cyl,fl}$.
While $m_{cyl}$ is constant, $m_{cyl,fl}$ depends on the current position $x$ of the cylinder:
\begin{equation}
	m_{cyl,fl}=\left[A_1\cdot x+A_2\cdot\left(l_{cyl}-x-b_{pis}\right)\right]\cdot\rho_{fl}
\end{equation}
The two thermal energy storages are presumed to interact by convective heat transfer on the inner skin surface, characterized by the convection constant $\alpha_{cyl,i}$.
Additionally, a thermal interface between the cylinder structure and the ambient of temperature $T_{cyl,\infty}$ characterized by its area $S_{cyl}$ and heat transfer coefficient $\alpha_{cyl,o}$ is introduced.
It is further assumed, that the frictional losses due to the leakage flow and the piston friction are transferred to the fluid as thermal losses.
Given this set-up, the temperatures of the cylinder and the contained fluid can be described as a set of coupled \acp{ODE}:
\begin{align}
	\frac{\partial}{\partial t}T_{cyl,fl}\cdot m_{cyl,fl}\cdot c_{p,fl}=& c_{p,fl}\cdot\left(\dot{m}_{in}\cdot T_{in}-\dot{m}_{out}\cdot T_{out}\right)+\\\nonumber
	&-2\cdot\pi\cdot\alpha_i\cdot l_{cyl}\cdot r_{cyl}\cdot\left(T_{cyl}-T_{cyl,fl}\right)+\\\nonumber
	&+v\cdot F_{ext}\cdot\left(\frac{1-\eta_{cyl}}{\eta_{cyl}}\right)+\dot{V}_{leak}\cdot\left(p_1-p_2\right)\\
	\frac{\partial}{\partial t}T_{cyl}\cdot m_{cyl}\cdot c_{p,cyl}=&2\cdot\pi\cdot\alpha_i\cdot l_{cyl}\cdot r_{cyl}\cdot\left(T_{cyl}-T_{cyl,fl}\right)+\\\nonumber
	&-\alpha_{o}\cdot S_{cyl}\cdot\left(T_{cyl}-T_{cyl,\infty}\right)
\end{align}
The heat transfer constant $\alpha_{cyl,i}$ depends on the heat conductivity $\lambda_{fl}$ of the fluid and the Nusselt number $\text{Nu}_{cyl,i}$:
\begin{equation}
	\alpha_{cyl,i}=\text{Nu}_{cyl,i}\cdot\frac{\lambda_{fl}}{2\cdot r_{cyl}}
\end{equation}
If the piston is not moved, free convection must be assumed.
Hence, $\text{Nu}_{cyl,i}$ can be calculated according toVDI Wärmeatlas. 
If the piston is moved, laminar and turbulent flow must be considered.
For laminar flow inside the cylinder ($\text{Re}_{cyl}=2\cdot v\cdot r_{cyl}/\nu_{fl}<2300$), $\text{Nu}_i=3.66$ can be used according to VDI Wärmeatlas.
For turbulent flow ($\text{Re}_{cyl}=2\cdot v\cdot r_{cyl}/\nu_{fl}>10^5$), $\text{Nu}_{cyl,i}$ must be computed according to the Reynolds number $\text{Re}$ and Prandtl number $\text{Pr}$ of the current flow regime:
\begin{equation}
	\text{Nu}_{cyl,i}=\frac{(\xi/8)\cdot\text{Re}\cdot\text{Pr}}{1+12.7\cdot\sqrt{\xi/8}\cdot\left(\text{Pr}^{2/3}-1\right)}\cdot\left[1+\cdot\left(\frac{2\cdot r_{cyl}}{l_{cyl}}\right)^{2/3}\right]
\end{equation}
with
\begin{equation}
	\xi=\left(1.8\cdot\log_{10}\text{Re}-1.5\right)^{-2}
\end{equation}
For flow regimes in-between, linear interpolation is required.

<ol class="literature">
	<li>Gnielinski V (2007) Berechnung des Druckverlustes in glatten konzentrischen Ringspalten bei ausgebildeter laminarer und turbulenter isothermer Str&ouml;mung. Chemie Ingenieur Technik 79:91–95. doi: 10.1002/cite.200600126</li>
	<li>VDI Wärmeatlas</li>
</ol></div>
		<div><h2>Inputs</h2>
			<table>
	<tr>
<td><b>Parameter</b></td><td><b>Symbol</b></td><td><b>Unit</b></td><td><b>Description</b></td></tr>
		<tr><td><i>Force</i></td><td>\(F_{ext}\)</td><td>N</td><td></td></tr>
		<tr><td><i>Velocity</i></td><td>\(v\)</td><td>m/s</td><td></td></tr>
		<tr><td><i>FluidIn</i></td><td>\(T_{in}, \dot{V}_{in}, p_{in}\)</td><td>none</td><td></td></tr>
</table></div>
		<div><h2>Outputs</h2>
			<table>
	<tr>
<td><b>Parameter</b></td><td><b>Symbol</b></td><td><b>Unit</b></td><td><b>Description</b></td></tr>
		<tr><td><i>PUse</i></td><td>\(\)</td><td>W</td><td></td></tr>
		<tr><td><i>PLoss</i></td><td>\(\)</td><td>W</td><td></td></tr>
		<tr><td><i>PTotal</i></td><td>\(\)</td><td>W</td><td></td></tr>
		<tr><td><i>PressureDifference</i></td><td>\(\)</td><td>Pa</td><td></td></tr>
		<tr><td><i>FluidOut</i></td><td>\(\)</td><td>none</td><td></td></tr>
</table></div>
		<div><h2>Parameters</h2>
			<table>
	<tr>
<td><b>Parameter</b></td><td><b>Symbol</b></td><td><b>Unit</b></td><td><b>Description</b></td></tr>
		<tr><td><i>Efficiency</i></td><td>\(\eta_{cyl}\)</td><td>none</td><td></td></tr>
		<tr><td><i>StructureMaterial</i></td><td>\(\)</td><td></td><td></td></tr>
		<tr><td><i>PistonThickness</i></td><td>\(b_{pis}\)</td><td>m</td><td></td></tr>
		<tr><td><i>PistonRodDiameter</i></td><td>\(d_{rod}\)</td><td>m</td><td></td></tr>
		<tr><td><i>StructuralMass</i></td><td>\(m_{cyl}\)</td><td>kg</td><td></td></tr>
		<tr><td><i>CylinderStroke</i></td><td>\(l_{cyl}\)</td><td>m</td><td></td></tr>
		<tr><td><i>PistonDiameter</i></td><td>\(d_{cyl}\)</td><td>m</td><td></td></tr>
</table></div>
	</body>
</html>
