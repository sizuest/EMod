/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.utils;

import static org.junit.Assert.*;

import org.junit.Test;

import ch.zuestengineering.emod.utils.SamplePeriodConverter;

/**
 * @author dhampl
 *
 */
public class SamplePeriodConverterTest {

	/**
	 * Test method for
	 * {@link ch.zuestengineering.emod.utils.SamplePeriodConverter#convertSamples(double, double, double[], boolean)}.
	 */
	@Test
	public void testConvertSamples() {
		double[] samples = { 1, 5, 3, 7 }; // values
		double originalPeriod = 1.0; // sample period = 1s
		double targetPeriod = 0.2; // target sample period = 200ms
		double[] expresult = { 1, 1.8, 2.6, 3.4, 4.2, 5, 4.6, 4.2, 3.8, 3.4, 3, 3.8, 4.6, 5.4, 6.2, 7, 7, 7, 7, 7 };
		double[] result = null;
		try {
			result = SamplePeriodConverter.convertSamples(originalPeriod, targetPeriod, samples, true);
		} catch (Exception e) {
			e.printStackTrace();
		}
		assertEquals("1s -> 200ms, number of samples", 20, result.length);
		for (int i = 0; i < expresult.length; i++) {
			assertEquals("1s -> 200ms, values", expresult[i], result[i], 0.001);
		}

		try {
			result = SamplePeriodConverter.convertSamples(targetPeriod, originalPeriod, result, true);
		} catch (Exception e) {
			e.printStackTrace();
		}
		assertEquals("200ms -> 1s, number of samples", 4, result.length);
		for (int i = 0; i < samples.length; i++) {
			assertEquals("200ms -> 1s, values", samples[i], result[i], 0.0001);
		}

		targetPeriod = 0.3333;
		double[] expectedresult = { 1, 2.333, 3.666, 5, 4.3333, 3.6666, 3, 4.333, 5.666, 7, 7, 7 };
		try {
			result = SamplePeriodConverter.convertSamples(originalPeriod, targetPeriod, samples, true);
		} catch (Exception e) {
			e.printStackTrace();
		}
		assertEquals("1s -> 333ms, number of samples", 12, result.length);
		for (int i = 0; i < result.length; i++) {
			assertEquals("1s -> 333ms, values", expectedresult[i], result[i], 0.01);
		}
	}
}
