/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.utils;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

/**
 * @author dhampl
 *
 */
@RunWith(Suite.class)
@SuiteClasses({ AlgoTest.class, SamplePeriodConverterTest.class })
public class UtilsTestSuite {

}
