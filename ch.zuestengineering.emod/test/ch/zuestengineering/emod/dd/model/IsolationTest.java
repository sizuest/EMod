/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.dd.model;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * @author Simon Z�st
 *
 */
public class IsolationTest {

	/**
	 * Test layer class
	 */
	@Test
	public void testLayer() {
		IsolationLayer layer = new IsolationLayer("PUR", .01);
		
		assertEquals("Thermal resistance, planar",      16,     layer.getThermalResistance(),             0);
		assertEquals("Thermal resistance, cylindrical", 23.083, layer.getThermalResistanceCircular(0.01), 0.001);
	}
	
	/**
	 * Test isolation class
	 */
	@Test
	public void testIsolation() {
		Isolation iso = new Isolation();
		
		iso.addLayer(new IsolationLayer("PUR", .01));
		iso.addLayer(new IsolationLayer("PUR", .01));
		
		assertEquals("Thermal resistance",                 8,     iso.getThermalResistance(), 0);
		iso.cleanUp();
		assertEquals("Thermal resistance, post clean-up",  8,     iso.getThermalResistance(), 0);
		assertEquals("Number of layers, post clean-up",    1,     iso.getLayers().size(),     0);

	}

	

}
