/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.model.thermal;

import org.junit.Test;

import ch.zuestengineering.emod.model.thermal.FreeHeatTransfer;

import static org.junit.Assert.assertEquals;

/**
 * @author simon
 *
 */
public class FreeHeatTransfereTest {

	/**
	 * Test FreeHeatTransfer class
	 */
	@Test
	public void testFreeHeatTransfere() {
		FreeHeatTransfer fht = new FreeHeatTransfer("Example", "ThermalTest");

		// No temperature gradient
		fht.getInput("Temperature1").setValue(293);
		fht.getInput("Temperature2").setValue(293);
		fht.update();

		assertEquals("Heatflow 1->2", 0, fht.getOutput("PThermal12").getValue(), 0);
		assertEquals("Heatflow 1->2", 0, fht.getOutput("PThermal21").getValue(), 0);

		// 10K temperature gradient
		fht.getInput("Temperature1").setValue(293);
		fht.getInput("Temperature2").setValue(283);
		fht.update();

		assertEquals("Heatflow 1->2", 2.22, fht.getOutput("PThermal12").getValue(), 0.1);
		assertEquals("Heatflow 1->2", -2.22, fht.getOutput("PThermal21").getValue(), 0.1);

	}

}
