/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.model.thermal;

import org.junit.Test;

import ch.zuestengineering.emod.model.thermal.LayerStorage;

import static org.junit.Assert.assertEquals;

/**
 * @author simon
 *
 */
public class LayerStorageTest {

	/**
	 * Test LayerStorage class
	 */
	@Test
	public void testLayerStorageTest() {
		// LayerStorage ls = new LayerStorage("Example","ThermalTest", 303);
		LayerStorage ls = new LayerStorage("Water", 0.01, 1, 1, 10, 293);
		ls.setSimulationTimestep(0.2);

		// No temperature gradient
		ls.getInput("TemperatureIn").setValue(293);
		ls.getInput("TemperatureAmb").setValue(293);
		ls.getInput("MassFlow").setValue(0);
		ls.update();

		assertEquals("Outflow temperature", 293, ls.getOutput("TemperatureOut").getValue(), 0);
		assertEquals("Thermal Loss", 0.0, ls.getOutput("PLoss").getValue(), 1E-9);

		ls.getInput("TemperatureAmb").setValue(0);
		ls.getInput("MassFlow").setValue(1);
		ls.update();

		// Loss is equal to average temperature drop * thermal resistance *
		// surface
		assertEquals("Thermal Loss", 293 * 1 * 1, ls.getOutput("PLoss").getValue(), 1);

		// After 10s steady state is reached
		for (int i = 0; i < 50; i++)
			ls.update();

		assertEquals("Outflow temperature", 292.8, ls.getOutput("TemperatureOut").getValue(), .2);

	}

}
