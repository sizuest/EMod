/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.model.thermal;

import org.junit.Test;

import ch.zuestengineering.emod.model.thermal.ForcedHeatTransfer;

import static org.junit.Assert.assertEquals;

/**
 * @author simon
 *
 */
public class ForcedHeatTransfereTest {

	/**
	 * Test Amplifier class
	 */
	@Test
	public void testForcedHeatTransfereTest() {
		ForcedHeatTransfer fht = new ForcedHeatTransfer("Example", "ThermalTest");

		// No temperature gradient
		fht.getInput("Temperature1").setValue(293);
		fht.getInput("Temperature2").setValue(293);
		fht.getInput("MassFlow").setValue(0);
		fht.update();

		assertEquals("Heatflow 1->2", 0, fht.getOutput("PThermal12").getValue(), 0);
		assertEquals("Heatflow 1->2", 0, fht.getOutput("PThermal21").getValue(), 0);

		// 10K temperature gradient
		fht.getInput("Temperature1").setValue(293);
		fht.getInput("Temperature2").setValue(283);
		fht.update();

		assertEquals("Heatflow 1->2", 0, fht.getOutput("PThermal12").getValue(), 0);
		assertEquals("Heatflow 1->2", 0, fht.getOutput("PThermal21").getValue(), 0);

		// Turn on mass flow
		fht.getInput("MassFlow").setValue(1);
		fht.update();

		assertEquals("Heatflow 1->2", 41800, fht.getOutput("PThermal12").getValue(), 50);
		assertEquals("Heatflow 1->2", -41800, fht.getOutput("PThermal21").getValue(), 50);

	}

}
