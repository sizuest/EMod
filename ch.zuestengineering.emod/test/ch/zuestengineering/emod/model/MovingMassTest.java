/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.model;

import org.junit.Test;

import ch.zuestengineering.emod.model.pm.MovingMass;

import static org.junit.Assert.assertEquals;

/**
 * @author Simon Z�st
 *
 */
public class MovingMassTest {

	/**
	 * Test MovingMass class
	 */
	@Test
	public void testMovingMass() {
		// New Mass with 75kg, moving at an 90° angle
		MovingMass mm = new MovingMass(75, 1, 0, 0);

		// Set time step to 1s
		mm.setSimulationTimestep(1);

		// 1ST TEST
		// Set speed [m/s]
		mm.getInput("SpeedLin").setValue(0);
		// update outputs
		mm.update();
		assertEquals("Force", 735.75, mm.getOutput("Force").getValue(), 0.01);

		// 2ND TEST
		// Set speed [m/s]
		mm.getInput("SpeedLin").setValue(1);
		// update outputs
		mm.update();
		assertEquals("Force", 810.75, mm.getOutput("Force").getValue(), 0.01);
		mm.update();
		assertEquals("Force", 735.75, mm.getOutput("Force").getValue(), 0.01);

	}

}
