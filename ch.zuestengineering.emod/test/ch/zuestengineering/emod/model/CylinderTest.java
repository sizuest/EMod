/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.model;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

import ch.zuestengineering.emod.model.linking.FluidContainer;
import ch.zuestengineering.emod.model.pm.Cylinder;

/**
 * @author sizuest
 *
 */
public class CylinderTest {

	/**
	 * Test Cylinder class
	 */
	@Test
	public void testCylinder() {
		Cylinder cyl = new Cylinder("Example");

		cyl.getDynamicState("Position").setInitialCondition(0);

		// TODO

		cyl.getInput("Force").setValue(0);
		cyl.getInput("Velocity").setValue(0);
		cyl.setSimulationTimestep(1);
		cyl.update();

		assertEquals("No Force/movement", 0, ((FluidContainer) cyl.getOutput("FluidOut")).getFluidCircuitProperties().getFlowRate(), 0);

		cyl.getInput("Force").setValue(1000);
		cyl.getInput("Velocity").setValue(0);
		cyl.update();

		assertEquals("Clamping", 606419, cyl.getOutput("PressureDifference").getValue(), 1000);
		assertEquals("Clamping", 0, ((FluidContainer) cyl.getOutput("FluidOut")).getFluidCircuitProperties().getFlowRate(), 1E-6);

	}

}
