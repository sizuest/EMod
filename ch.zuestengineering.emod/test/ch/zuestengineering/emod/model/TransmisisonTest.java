/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.model;

import org.junit.Test;

import ch.zuestengineering.emod.model.pm.Transmission;

import static org.junit.Assert.assertEquals;

/**
 * @author Simon Z�st
 *
 */
public class TransmisisonTest {

	/**
	 * Test Fan class
	 */
	@Test
	public void testTransmisison() {
		Transmission transm = new Transmission("Example");

		// Set fan to "off"
		transm.getInput("RotSpeed").setValue(0);
		transm.getInput("Torque").setValue(0);
		transm.update();
		//
		assertEquals("Resulting speed", 0, transm.getOutput("RotSpeed").getValue(), 0);
		assertEquals("Resulting torque", 0, transm.getOutput("Torque").getValue(), 0);
		assertEquals("Heat loss", 0, transm.getOutput("PLoss").getValue(), 0);

		// Set fan to "on"
		transm.getInput("RotSpeed").setValue(10);
		transm.getInput("Torque").setValue(10);
		transm.update();
		//
		assertEquals("Resulting speed", 1, transm.getOutput("RotSpeed").getValue(), 0);
		assertEquals("Resulting torque", 111.1, transm.getOutput("Torque").getValue(), 0.1);
		assertEquals("Heat loss", 69.8, transm.getOutput("PLoss").getValue(), 0.2);
	}

}
