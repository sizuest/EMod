/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.model.parameters;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;

import org.junit.Test;

import ch.zuestengineering.emod.dd.Duct;
import ch.zuestengineering.emod.model.APhysicalComponent;
import ch.zuestengineering.emod.model.material.Material;
import ch.zuestengineering.emod.model.pm.MotorAC;
import ch.zuestengineering.emod.model.pm.MotorDC;
import ch.zuestengineering.emod.model.pm.Pump;
import ch.zuestengineering.emod.model.pm.Spindle;
import ch.zuestengineering.emod.model.units.SiUnit;

/**
 * @author sizuest
 *
 */
public class ParameterSetTest {

	/**
	 * 
	 */
	@Test
	public void testInteger() {
		int i = 17;
		
		ParameterSet ps = new ParameterSet();
		ps.setInteger("Integer", i);
		
		assertEquals("Wrong name", 0, ps.getInteger("Intege"), 0);
		assertEquals("Wrong type", true, Double.isNaN(ps.getDouble("Integer")));
		assertEquals("Right name", i, ps.getInteger("Integer"), 0);
	}
	
	/**
	 * 
	 */
	@Test
	public void testNumeric() {
		double d = Math.PI;
		
		ParameterSet ps = new ParameterSet();
		ps.setDouble("Numeric", d);
		
		assertEquals("Wrong name", true, Double.isNaN(ps.getDouble("Numeri")));
		assertEquals("Wrong type", 0, ps.getInteger("Numeric"), 0);
		assertEquals("Right name", d, ps.getDouble("Numeric"), 0);
	}
	
	/**
	 * 
	 */
	@Test
	public void testBoolean() {
		boolean b = true;
		
		ParameterSet ps = new ParameterSet();
		ps.setBoolean("Boolean", b);
		
		assertEquals("Wrong name", false, ps.getBoolean("Boolea"));
		assertEquals("Wrong type", true, Double.isNaN(ps.getDouble("Boolean")));
		assertEquals("Right name", b, ps.getBoolean("Boolean"));
	}
	
	/**
	 * 
	 */
	@Test
	public void testString() {
		String s = "TestString";
		
		ParameterSet ps = new ParameterSet();
		ps.setString("String", s);
		
		assertEquals("Wrong name", null, ps.getString("Strin"));
		assertEquals("Wrong type", true, Double.isNaN(ps.getDouble("String")));
		assertEquals("Right name", s, ps.getString("String"));
	}
	
	/**
	 * 
	 */
	@Test
	public void testVector() {
		double[] v = {0.1,2.2,1.3};
		
		ParameterSet ps = new ParameterSet();
		ps.setVector("Vector", v);
		
		assertEquals("Wrong name", null, ps.getVector("Vecto"));
		assertEquals("Wrong type", true, Double.isNaN(ps.getDouble("Vector")));
		assertEquals("Right name", v, ps.getVector("Vector"));
	}
	
	/**
	 * 
	 */
	@Test
	public void testMaxtrix() {
		double[][] m = {{0.1,2.2,1.3},{-0.1,-2.2,-1.3}};
		
		ParameterSet ps = new ParameterSet();
		ps.setMatrix("Matrix", m);
		
		assertEquals("Wrong name", true, ps.getMatrix("Matri") == null);
		assertEquals("Wrong type", true, Double.isNaN(ps.getDouble("Matrix")));
		assertEquals("Right name", true, ps.getMatrix("Matrix").equals(m));
	}
	
	/**
	 * 
	 */
	@Test
	public void testPhysicalValue() {
		PhysicalValue pv = new PhysicalValue(5.45, new SiUnit("J/s"));
		
		ParameterSet ps = new ParameterSet();
		ps.setPhysicalValue("PhysicalValue", pv);
		
		assertEquals("Wrong name", true, ps.getPhysicalValue("PhysicalValu") == null);
		assertEquals("Wrong type", true, Double.isNaN(ps.getDouble("PhysicalValue")));
		assertEquals("Right name", true, ps.getPhysicalValue("PhysicalValue").equals(pv));
	}
	
	/**
	 * 
	 */
	@Test
	public void testMaterial() {
		Material mat = new Material("Water");
		
		ParameterSet ps = new ParameterSet();
		ps.setMaterial("Material", mat);
		
		assertEquals("Wrong name", true, ps.getMaterial("Materia") == null);
		assertEquals("Wrong type", true, Double.isNaN(ps.getDouble("Material")));
		assertEquals("Right name", true, ps.getMaterial("Material").equals(mat));

	}
	
	/**
	 * 
	 */
	@Test
	public void testModel() {
		APhysicalComponent mod = new Spindle("Example");
		
		ParameterSet ps = new ParameterSet();
		ps.setModel("Model", mod);
		
		assertEquals("Wrong name", true, ps.getModel("Mode") == null);
		assertEquals("Wrong type", true, Double.isNaN(ps.getDouble("Model")));
		assertEquals("Right name", mod.toString(), ps.getModel("Model").toString());
	}
	
	/**
	 * 
	 */
	@Test
	public void testModels() {
		ArrayList<APhysicalComponent> modls = new ArrayList<>();
		modls.add(new Spindle("Example"));
		modls.add(new Pump("Example"));
		
		ParameterSet ps = new ParameterSet();
		ps.setModels("Models", modls);
		
		assertEquals("Wrong name", true, ps.getModels("Modelss") == null);
		assertEquals("Wrong type", true, Double.isNaN(ps.getDouble("Models")));
		for(int i=0; i<modls.size(); i++)
			assertEquals("Right name", modls.get(i).toString(), ps.getModels("Models").get(i).toString());
	}
	
	/**
	 * 
	 */
	@Test
	public void testDuct() {
		Duct duct = Duct.buildFromDB("Example");
		
		ParameterSet ps = new ParameterSet();
		ps.setDuct("Duct", duct);
		
		assertEquals("Wrong name", true, ps.getDuct("Duc") == null);
		assertEquals("Wrong type", true, Double.isNaN(ps.getDouble("Duct")));
		assertEquals("Right name", duct.getAllElements().size(), ps.getDuct("Duct").getAllElements().size());
	}
	
	/**
	 * 
	 */
	@Test
	public void testSaveLoad() {		
		ParameterSet ps = new ParameterSet("TEST");
		ps.setComment("Test comment");
		
		int i = 17;
		boolean b = true;
		double d = Math.PI;
		double[] v = {0.1,2.2,1.3};
		double[][] m = {{0.1,2.2,1.3},{-0.1,-2.2,-1.3}};
		String s = "TestString";
		PhysicalValue pv = new PhysicalValue(5.45, new SiUnit("J/s"));
		Material mat = new Material("Water");
		APhysicalComponent mod = new Spindle("Example");
		ArrayList<APhysicalComponent> modls = new ArrayList<>();
		modls.add(new MotorDC("Example"));
		modls.add(new MotorAC("Example"));
		Duct duct = Duct.buildFromDB("Example");
		
		
		ps.setInteger("Integer", i);
		ps.setDouble("Numeric", d);
		ps.setBoolean("Boolean", b);
		ps.setString("String", s);
		ps.setVector("Vector", v);
		ps.setMatrix("Matrix", m);
		ps.setPhysicalValue("PhysicalValue", pv);
		ps.setMaterial("Material", mat);
		ps.setModel("Model", mod);
		ps.setModels("Models", modls);
		ps.setDuct("Duct", duct);
		
		((ParameterModel) ps.get("Model")).setFilter("Spindle");
		((ParameterModels) ps.get("Models")).setFilter("Motor");
		
		ps.save("test.xml");
		
		boolean loadTest = true;
		try {
			ParameterSet ps2 = ParameterSet.load("test.xml");
		} catch(Exception e) {
			loadTest = false;
		}
		
		assertEquals("Successfull loaded", true, loadTest);
		
	}
}
