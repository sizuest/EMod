/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.model;

import org.junit.Test;

import ch.zuestengineering.emod.model.pm.Valve;

import static org.junit.Assert.assertEquals;

/**
 * @author Simon Z�st
 *
 */
public class ValveTest {

	/**
	 * Test Valve class
	 */
	@Test
	public void testValve() {
		Valve val = new Valve("Example");

		// TODO
		val.getFluidPropertiesList().get(0).setFlowRatesIn(new double[] { .25 / 1000 });
		val.getInput("ValveCtrl").setValue(1);
		val.update();

		assertEquals("PressureLoss", 312.1875, val.getPressure(val.getFluidPropertiesList().get(0).getFlowRate()), 10);
	}

}
