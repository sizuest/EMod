/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.model;

import org.junit.Test;

import ch.zuestengineering.emod.Machine;
import ch.zuestengineering.emod.model.linking.FluidConnection;
import ch.zuestengineering.emod.model.pm.Pipe;

/**
 * @author Simon Z�st
 *
 */
public class PipeTest {

	/**
	 * @throws Exception
	 */
	@Test
	public void testPipeConnection() throws Exception {
		Machine.clearMachine();
		Pipe pip1 = (Pipe) Machine.createNewMachineComponent("Pipe", "Example");;
		Pipe pip2 = (Pipe) Machine.createNewMachineComponent("Pipe", "Example");;

		pip1.setSimulationTimestep(0.01);
		pip2.setSimulationTimestep(0.01);

		FluidConnection fc = new FluidConnection(pip1, pip2);
		fc.init(293, 100000);

		pip1.getInput("TemperatureAmb").setValue(293);

		for (int i = 0; i < 100; i++) {
			// what first??
			fc.update();
			System.out.print("pipe1 ");
			pip1.update();
			System.out.print("pipe2 ");
			pip2.update();
		}
	}
}
