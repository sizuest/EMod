/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.model.math;

import org.junit.Test;

import ch.zuestengineering.emod.model.math.Gain;
import ch.zuestengineering.emod.model.units.SiUnit;
import ch.zuestengineering.emod.model.units.Unit;

import static org.junit.Assert.assertEquals;

/**
 * @author simon
 *
 */
public class GainTest {

	/**
	 * Test function for {@link Gain}
	 */
	@Test
	public void testGain() {
		Gain g = new Gain(new SiUnit(Unit.WATT), 3);

		// Set input to 5
		g.getInput("Input").setValue(5);
		g.update();

		assertEquals("Result", 15, g.getOutput("Output").getValue(), 0);

	}

}
