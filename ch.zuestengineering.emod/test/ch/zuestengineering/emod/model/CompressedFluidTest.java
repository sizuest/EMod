/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.model;

import org.junit.Test;

import ch.zuestengineering.emod.model.pm.CompressedFluid;

import static org.junit.Assert.assertEquals;

/**
 * @author Simon Z�st
 *
 */
public class CompressedFluidTest {

	/**
	 * Test Amplifier class
	 */
	@Test
	public void testCompressedFluid() {
		CompressedFluid cf = new CompressedFluid("Example");

		// No flow, nominal conditions
		cf.getInput("Flow").setValue(0);
		cf.getInput("TemperatureAmb").setValue(273);
		cf.getInput("PressureAmb").setValue(101300);
		cf.update();
		assertEquals("Power", 0, cf.getOutput("PTotal").getValue(), 0);

		// flow 1m3/s
		cf.getInput("Flow").setValue(1.0);
		cf.update();
		assertEquals("Power", 500000, cf.getOutput("PTotal").getValue(), 1e4);
	}

}
