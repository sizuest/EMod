/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.model.lc;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import ch.zuestengineering.emod.model.material.Material;
import ch.zuestengineering.emod.model.pm.Amplifier;

/**
 * @author sizuest
 *
 */
public class PhysicalComponentLCTest {

	/**
	 * 
	 */
	@Test
	public void testMaterialAmount() {
		Amplifier amplifier = new Amplifier("Example");
		
		Material m1 = new Material("Steel");
		Material m2 = new Material("Motor");
		
		double cp = (m1.getHeatCapacity(293.15, 1e5)*1.5 + m2.getHeatCapacity(293.15, 1e5)*4)/5.5;
		
		assertEquals("Mass", 5.5, amplifier.getMass(), 0);
		assertEquals("Heat capacity", cp, amplifier.getSpecificHeatCapacity(), 0);
	}
}
