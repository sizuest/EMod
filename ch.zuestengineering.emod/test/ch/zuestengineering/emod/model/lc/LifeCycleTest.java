/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.model.lc;

import org.junit.Test;

import ch.zuestengineering.emod.EModSession;
import ch.zuestengineering.emod.LifeCycle;
import ch.zuestengineering.emod.Machine;
import ch.zuestengineering.emod.States;
import ch.zuestengineering.emod.model.MachineComponent;
import ch.zuestengineering.emod.model.units.SiUnit;
import ch.zuestengineering.emod.simulation.ASimulationControl;
import ch.zuestengineering.emod.simulation.EModSimulationRun;
import ch.zuestengineering.emod.simulation.MachineState;
import ch.zuestengineering.emod.utils.ConfigReader;

/**
 * @author sizuest
 *
 */
public class LifeCycleTest {
	/**
	 * Test inventory generation from machine and simulation config
	 */
	@Test
	public void testInventory() {
		EModSession.newSession("NewMachine", "Mdl", "SimCfg", "Process");
		MachineComponent mc = Machine.addNewMachineComponent("Amplifier", "Example");
		ASimulationControl sc1 = Machine.addNewInputObject("StaticSimulationControl", new SiUnit());
		ASimulationControl sc2 = Machine.addNewInputObject("StaticSimulationControl", new SiUnit("W"));

		Machine.addIOLink(sc1.getOutput(), mc.getComponent().getInput("State"));
		Machine.addIOLink(sc2.getOutput(), mc.getComponent().getInput("PDmd"));

		
		String file = EModSession.getSimulationConfigPath();

		ConfigReader cr = null;
		try {
			cr = new ConfigReader(file);
			cr.ConfigReaderOpen();
			cr.setValue("simulationPeriod", 1.0);
			cr.saveValues();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		States.removeAllStates();
		States.appendState(30, MachineState.OFF);
		States.appendState(20, MachineState.STANDBY);
		States.appendState(10, MachineState.READY);
		States.appendState(5, MachineState.ON);
		States.appendState(1, MachineState.PROCESS);
		
		Machine.saveMachine(EModSession.getMachineName(), EModSession.getMachineConfig());
		States.saveStates(EModSession.getMachineName(), EModSession.getSimulationConfig());
		
		
		EModSimulationRun.EModSimRun();
		
		LifeCycle.fetch();
		
		LifeCycle.save(EModSession.getSimulationConfig());
	}
	
	
	
	
}
