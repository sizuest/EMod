/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.model;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import ch.zuestengineering.emod.model.pm.ConstantComponent;

/**
 * @author Simon Z�st
 *
 */
public class ConstantComponentTest {

	/**
	 * Test LinearMotor class
	 */
	@Test
	public void testConstantComponent() {
		ConstantComponent cc = new ConstantComponent("80mmFan");

		// Check ptotal for input level 0
		cc.getInput("level").setValue(0);
		cc.update();
		assertEquals("Level0", 0.0, cc.getOutput("PTotal").getValue(), 0.0001);

		// Check ptotal for input level 1
		cc.getInput("level").setValue(1);
		cc.update();
		assertEquals("Level1", 50.0, cc.getOutput("PTotal").getValue(), 0.0001);

		// Check ptotal for input level 2
		cc.getInput("level").setValue(2);
		cc.update();
		assertEquals("Level2", 250.0, cc.getOutput("PTotal").getValue(), 0.0001);

	}
}
