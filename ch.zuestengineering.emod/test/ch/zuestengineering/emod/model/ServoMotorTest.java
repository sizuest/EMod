/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.model;

import org.junit.Test;

import ch.zuestengineering.emod.model.pm.MotorDC;

import static org.junit.Assert.assertEquals;

/**
 * @author Simon Z�st
 *
 */
public class ServoMotorTest {

	/**
	 * 
	 */
	@Test
	public void testServoMotor() {
		MotorDC servo = new MotorDC("Example");

		// Disable apply torque and speed
		servo.getInput("Torque").setValue(11.0);
		servo.getInput("RotSpeed").setValue(10.0 / 60);
		servo.update();

		assertEquals("Servo power", 3 * (11.0 + 1.0) / 1.0 * (0.1 * 10.0 / 60 + 2* (11.0 + 1) / 1.0), servo.getOutput("PTotal").getValue(), 5);
		assertEquals("Power Loss", 3 * Math.pow((11.0 + 1) / 1.0, 2.0) * 2.0, servo.getOutput("PLoss").getValue(), 1);
		assertEquals("Efficiency", servo.getOutput("PUse").getValue() / servo.getOutput("PTotal").getValue(), servo.getOutput("Efficiency").getValue(), 0);

	}

}
