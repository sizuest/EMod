Spindle.RotSpeed = RotSpeed
Spindle.Torque = Torque
ForcedFluidFlow.TemperatureIn = TemperatureIn
ForcedFluidFlow.PressureIn = PressureIn
ForcedFluidFlow.FlowRate = FlowRate
Spindle.CoolantIn = ForcedFluidFlow.FluidOut
ForcedFluidFlow.FluidIn = Spindle.CoolantOut
Spindle.State = State
