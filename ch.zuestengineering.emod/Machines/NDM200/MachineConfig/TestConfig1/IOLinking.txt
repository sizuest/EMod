# Input - Output mapping
C1.RotSpeed      = C1_n      # Turing speed from process parameters
C1.Torque        = C1Torque  # Torque from Kienzle
Z.RotSpeed       = xRPM
Z.Torque         = xTorque
X1.RotSpeed      = yRPM
X1.Torque        = yTorque
Fan1.level       = 80mmFan