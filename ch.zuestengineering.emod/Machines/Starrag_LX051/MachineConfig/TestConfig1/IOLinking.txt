# Input - Output mapping
C1.RotSpeed      = C1_n       # Turing speed from process parameters
C1.Torque        = C1_Torque  # Torque from Kienzle
#R1.RotSpeed      = R1_n
#R1.Torque        = R1_Torque
#X1.RotSpeed      = X1_n
#X1.Torque        = X1_Torque
#Z1_ax.Speed      = Z1_v
Z1_ax.ProcessForce = X1_v
Z1_ax.Speed       = Z1_v