/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.dd.gui;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.TableEditor;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.ControlListener;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

import ch.zuestengineering.emod.dd.Duct;
import ch.zuestengineering.emod.dd.model.ADuctElement;
import ch.zuestengineering.emod.dd.model.AHydraulicProfile;
import ch.zuestengineering.emod.dd.model.DuctBypass;
import ch.zuestengineering.emod.dd.model.Isolation;
import ch.zuestengineering.emod.gui.AConfigGUI;
import ch.zuestengineering.emod.gui.icons.IconHandler;
import ch.zuestengineering.emod.gui.utils.ShellUtils;
import ch.zuestengineering.emod.gui.utils.TableUtils;
import ch.zuestengineering.emod.model.parameters.ParameterSet;
import ch.zuestengineering.emod.model.units.SiUnit;
import ch.zuestengineering.emod.utils.LocalizationHandler;

/**
 * Composite to edit the properties of a duct element
 * 
 * @author sizuest
 *
 */
public class EditDuctElementGUI extends AConfigGUI {

	private Table tableProperties;
	private ADuctElement element;
	private ParameterSet parametersNew = new ParameterSet();
	private AHydraulicProfile profileOld;
	private Isolation isolationNew;
	private Duct parentDuct;

	/**
	 * Create a new editor and add it to the parent
	 * 
	 * @param parent
	 * @param style
	 * @param element
	 * @param parentDuct
	 */
	public EditDuctElementGUI(Composite parent, int style, final ADuctElement element, Duct parentDuct) {
		super(parent, style);

		this.element = element;
		this.parametersNew.merge(element.getParameterSet());

		this.parentDuct = parentDuct;

		profileOld = this.element.getProfileIn().clone();

		if (null == this.element.getIsolation())
			isolationNew = new Isolation();
		else
			isolationNew = this.element.getIsolation().clone();

		this.getContent().setLayout(new GridLayout(1, true));

		tableProperties = new Table(this.getContent(), SWT.BORDER | SWT.SINGLE | SWT.V_SCROLL);
		tableProperties.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 2, 1));
		tableProperties.setLinesVisible(true);
		tableProperties.setHeaderVisible(true);

		String[] titles = { LocalizationHandler.getItem("app.dd.elemet.gui.property"),
				LocalizationHandler.getItem("app.dd.elemet.gui.value"),
				LocalizationHandler.getItem("app.dd.elemet.gui.unit"), "        ", "        " };

		for (int i = 0; i < titles.length; i++) {
			TableColumn column = new TableColumn(tableProperties, SWT.NULL);
			column.setText(titles[i]);
			column.setWidth(32);
		}

		updatePropertyTable();
	}

	/**
	 * Create an editor in a new shell
	 * 
	 * @param parent
	 * @param element
	 * @param duct
	 * @return
	 */
	public static Shell editDuctElementGUI(Shell parent, ADuctElement element, Duct duct) {
		final Shell shell = new Shell(parent, SWT.APPLICATION_MODAL | SWT.CLOSE | SWT.MAX | SWT.RESIZE);
		shell.setLayout(new GridLayout(1, true));

		final EditDuctElementGUI gui = new EditDuctElementGUI(shell, SWT.NONE, element, duct);

		shell.setImages(parent.getImages());

		shell.setText(LocalizationHandler.getItem("app.dd.elemet.gui.titel") + " " + element.getName());

		shell.pack();
		shell.layout();
		shell.open();

		shell.setSize(shell.computeSize(SWT.DEFAULT, SWT.DEFAULT));

		shell.addControlListener(new ControlListener() {

			@Override
			public void controlResized(ControlEvent e) {
				gui.layout();
			}

			@Override
			public void controlMoved(ControlEvent e) {
				gui.layout();
			}
		});

		gui.addDisposeListener(new DisposeListener() {
			@Override
			public void widgetDisposed(DisposeEvent e) {
				shell.dispose();
			}
		});

		ShellUtils.putToCenter(shell, parent);

		return shell;
	}

	private void updatePropertyTable() {

		for (Control c : tableProperties.getChildren())
			c.dispose();

		try {
			TableUtils.addCellEditor(tableProperties, this, new int[] { 1 });
		} catch (Exception e1) {
			e1.printStackTrace();
		}

		tableProperties.clearAll();
		tableProperties.setItemCount(0);

		TableItem itemName = new TableItem(tableProperties, SWT.NONE, 0);

		if (!(element instanceof DuctBypass)) {
			new TableItem(tableProperties, SWT.NONE, 1); // Profile
			new TableItem(tableProperties, SWT.NONE, 2); // Isolation

			updateProfileItem();
			updateIsolationItem();
		}

		itemName.setText(0, LocalizationHandler.getItem("app.dd.elemet.gui.name"));
		itemName.setText(1, element.getName());

		for (String key : element.getParameterSet().getNames()) {
			final int idx = tableProperties.getItemCount();
			final TableItem itemParam = new TableItem(tableProperties, SWT.NONE, idx);

			itemParam.setText(0, key);
			try {
				itemParam.setText(1, parametersNew.getPhysicalValue(key).getValueString() + "");
				itemParam.setText(2, parametersNew.getPhysicalValue(key).getUnit().toString());
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		TableColumn[] columns = tableProperties.getColumns();
		for (int j = 0; j < columns.length; j++) {
			columns[j].pack();
		}
	}

	private void updateIsolationItem() {

		TableItem itemIsolation = tableProperties.getItem(2);
		TableEditor editorButton = new TableEditor(tableProperties);

		itemIsolation.setText(0, LocalizationHandler.getItem("app.dd.elemet.gui.isolation"));
		if (null == isolationNew)
			itemIsolation.setText(1, "none");
		else if (isolationNew.isEmpty())
			itemIsolation.setText(1, "none");
		else if (isolationNew.isEmpty())
			itemIsolation.setText(1, "none");
		else {
			itemIsolation.setText(1, isolationNew.toString());
			itemIsolation.setText(2, (new SiUnit("m")).toString());
		}

		editorButton = new TableEditor(tableProperties);
		Button buttonEditIsolation = new Button(tableProperties, SWT.NONE);
		buttonEditIsolation.setText("...");
		buttonEditIsolation.setLayoutData(new GridData(SWT.FILL, SWT.TOP, false, true, 1, 1));
		buttonEditIsolation.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				if (null == isolationNew)
					isolationNew = new Isolation();

				wasEdited();
				editDuctIsolation(isolationNew);
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent event) {
				// Not used
			}
		});
		buttonEditIsolation.pack();
		editorButton.minimumWidth = buttonEditIsolation.getSize().x;
		editorButton.horizontalAlignment = SWT.LEFT;
		editorButton.setEditor(buttonEditIsolation, itemIsolation, 3);

		editorButton = new TableEditor(tableProperties);
		Button buttonDeleteIsolation = new Button(tableProperties, SWT.NONE);
		buttonDeleteIsolation.setImage(IconHandler.getIcon(getDisplay(), "delete_obj"));
		buttonDeleteIsolation.setLayoutData(new GridData(SWT.FILL, SWT.TOP, false, true, 1, 1));
		buttonDeleteIsolation.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				isolationNew = null;
				wasEdited();
				update();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent event) {
				// Not used
			}
		});
		buttonDeleteIsolation.pack();
		editorButton.minimumWidth = buttonDeleteIsolation.getSize().x;
		editorButton.horizontalAlignment = SWT.LEFT;
		editorButton.setEditor(buttonDeleteIsolation, itemIsolation, 4);
	}

	private void updateProfileItem() {
		TableItem itemProfile = tableProperties.getItem(1);

		TableEditor editorButton = new TableEditor(tableProperties);

		itemProfile.setText(0, LocalizationHandler.getItem("app.dd.elemet.gui.profile"));
		itemProfile.setText(1, element.getProfileIn().toString());
		itemProfile.setText(2, (new SiUnit("m")).toString());

		Button buttonEditProfile = new Button(tableProperties, SWT.NONE);
		buttonEditProfile.setText("...");
		buttonEditProfile.setLayoutData(new GridData(SWT.FILL, SWT.TOP, false, true, 1, 1));
		buttonEditProfile.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				editDuctProfile(element);
				wasEdited();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent event) {
				// Not used
			}
		});
		buttonEditProfile.pack();
		editorButton.minimumWidth = buttonEditProfile.getSize().x;
		editorButton.horizontalAlignment = SWT.LEFT;
		editorButton.setEditor(buttonEditProfile, itemProfile, 3);
	}

	private void editDuctProfile(ADuctElement element) {
		Shell shell = EditDuctProfileGUI.editDuctProfileGUI(this.getShell(), element);
		shell.addDisposeListener(new DisposeListener() {

			@Override
			public void widgetDisposed(DisposeEvent e) {
				updateProfileItem();
				TableUtils.packTable(tableProperties);
			}
		});
	}

	private void editDuctIsolation(Isolation isolation) {
		Shell shell = EditDuctIsolationGUI.editDuctIsolationGUI(this.getShell(), isolation);
		shell.addDisposeListener(new DisposeListener() {

			@Override
			public void widgetDisposed(DisposeEvent e) {
				updateIsolationItem();
				TableUtils.packTable(tableProperties);
			}
		});
	}

	@Override
	public void update() {
		updatePropertyTable();
	}

	@Override
	public void save() {

		// Read new name
		if (null != this.parentDuct)
			parentDuct.setElementName(element.getName(), tableProperties.getItem(0).getText(1));
		else
			element.setName(tableProperties.getItem(0).getText(1));

		// Read new Config
		for (int i = 3; i < tableProperties.getItemCount(); i++) {
			try {
				parametersNew.setPhysicalValue(tableProperties.getItem(i).getText(0),
						tableProperties.getItem(i).getText(1), new SiUnit("" + tableProperties.getItem(i).getText(2)));
			} catch (Exception e) {
				logger.warning("Can not parse input '" + tableProperties.getItem(i).getText(0) + "' as number: "
						+ tableProperties.getItem(i).getText(1));
			}
		}

		element.setParameterSet(parametersNew);
		element.setIsolation(isolationNew);

		profileOld = this.element.getProfileIn().clone();

		updatePropertyTable();
	}

	@Override
	public void reset() {
		for (String s : this.parametersNew.getNames())
			try {
				this.parametersNew.setPhysicalValue(s, element.getParameterSet().getPhysicalValue(s));
			} catch (Exception e) {
				e.printStackTrace();
			}

		profileOld = this.element.getProfileIn().clone();
		if (null == this.element.getIsolation())
			isolationNew = new Isolation();
		else
			isolationNew = this.element.getIsolation().clone();

		updatePropertyTable();

		element.setProfile(profileOld);
		profileOld = this.element.getProfileIn().clone();

	}
}
