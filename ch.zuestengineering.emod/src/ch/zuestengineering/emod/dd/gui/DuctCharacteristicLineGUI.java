/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.dd.gui;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;

import ch.zuestengineering.emod.dd.Duct;
import ch.zuestengineering.emod.gui.SelectMachineComponentGUI;
import ch.zuestengineering.emod.model.pm.Pump;
import ch.zuestengineering.emod.utils.LocalizationHandler;

/**
 * Implements the graph of a duct's characteristics
 * 
 * @author simon
 *
 */
public class DuctCharacteristicLineGUI extends ADDGUITab {

	private Menu menuChartCC;
	private DuctCharacteristicLine chartCC;

	protected DuctCharacteristicLineGUI(Composite parent, int i, Duct duct) {
		super(parent, i);

		this.duct = duct;

		init();
	}

	@Override
	public void init() {
		setLayout(new GridLayout(1, false));

		chartCC = new DuctCharacteristicLine(this, duct);
		chartCC.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));

		/* Popup */
		menuChartCC = new Menu(chartCC);

		MenuItem itemAddPump = new MenuItem(menuChartCC, SWT.NONE);
		itemAddPump.setText(LocalizationHandler.getItem("app.dd.testing.gui.analysis.characteristicdiag.addpump"));
		itemAddPump.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				openModelSelectGUI();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				// Not used
			}
		});

		chartCC.getChart().setMenu(menuChartCC);
		chartCC.getChart().getPlotArea().setMenu(menuChartCC);

		chartCC.getChart().layout();
	}

	@Override
	public void update() {
		chartCC.update();
	}

	@Override
	public void save() {
		// TODO Auto-generated method stub

	}

	@Override
	public void wasEdited() {
		// TODO Auto-generated method stub

	}

	private void openModelSelectGUI() {
		SelectMachineComponentGUI compGUI = new SelectMachineComponentGUI(this.getShell());
		String selection = compGUI.open("Pump");
		try {
			addPump(selection);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void addPump(final Pump pump) {

		// Check if type is unique
		for (Pump p : chartCC.getPumps())
			if (p.getType().equals(pump.getType()))
				return;

		chartCC.addPump(pump);

		pump.getFluidPropertiesList().get(0).setMaterial(duct.getMaterial());
		pump.updatePumpMap(1, duct.getInletTemperature());

		// Add menu item to remove pump
		if (menuChartCC.getItemCount() == 1)
			new MenuItem(menuChartCC, SWT.SEPARATOR);
		MenuItem item = new MenuItem(menuChartCC, SWT.NONE);
		item.setText(LocalizationHandler.getItem("app.dd.testing.gui.analysis.characteristicdiag.removepump") + ": '"
				+ pump.getType() + "'");
		item.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				removePump(pump.getType());
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});
	}

	/**
	 * Add the pump map of the given type to the characteristic diagram
	 * 
	 * @param type
	 */
	public void addPump(String type) {
		try {
			addPump(new Pump(type));
		} catch (Exception e2) {
		}
	}

	private void removePump(String type) {

		int index = chartCC.removePump(type);
		;

		if (-1 == index)
			return;

		// Dispose and remove elements
		menuChartCC.getItem(index + 2).dispose();

		if (menuChartCC.getItemCount() == 2)
			menuChartCC.getItem(1).dispose();

	}

	/**
	 * Set the fluid material for the pump maps
	 * 
	 * @param type
	 */
	public void setMaterial(String type) {
		chartCC.setMaterial(type);
	}

	@Override
	public void setDuct(Duct duct) {
		super.setDuct(duct);

		chartCC.setDuct(duct);
	}

}
