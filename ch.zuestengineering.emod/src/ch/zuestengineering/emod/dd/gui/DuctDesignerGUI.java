/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.dd.gui;

import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Logger;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyleRange;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.MenuEvent;
import org.eclipse.swt.events.MenuListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Monitor;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;

import ch.zuestengineering.emod.LogLevel;
import ch.zuestengineering.emod.dd.Duct;
import ch.zuestengineering.emod.dd.model.DuctPipe;
import ch.zuestengineering.emod.gui.MachineComponentDBGUI;
import ch.zuestengineering.emod.gui.help.HelpBrowserGUI;
import ch.zuestengineering.emod.gui.icons.IconHandler;
import ch.zuestengineering.emod.gui.material.EditMaterialGUI;
import ch.zuestengineering.emod.gui.material.MaterialDBGUI;
import ch.zuestengineering.emod.gui.modelling.EditMachineComponentGUI;
import ch.zuestengineering.emod.gui.utils.ShowButtons;
import ch.zuestengineering.emod.licensing.LicenseActions;
import ch.zuestengineering.emod.licensing.LicenseHandler;
import ch.zuestengineering.emod.utils.LocalizationHandler;

/**
 * main gui class for dd application
 * 
 * @author Simon Z�st
 * 
 */
public class DuctDesignerGUI {

	private static Logger logger = Logger.getLogger(DuctDesignerGUI.class.getName());

	protected static Shell shell;
	protected Display disp;

	protected Duct duct;
	protected String path = "";

	private MenuItem editRedo, editUndo;

	// protected DuctConfigGUI ductDesigner;
	protected DuctConfigGraphGUI ductDesigner;
	protected StyledText console;

	/**
	 * New duct Desinger GUI
	 * 
	 * @param display
	 */
	public DuctDesignerGUI(Display display) {
		disp = display;
		shell = new Shell(display);

		shell.setText(LocalizationHandler.getItem("app.dd.gui.title"));
		if (display.getBounds().width >= 1024)
			shell.setSize(1024, 768);
		else
			shell.setSize(display.getBounds().width, display.getBounds().height);

		Monitor primary = display.getPrimaryMonitor();
		Rectangle bounds = primary.getBounds();
		Rectangle rect = shell.getBounds();

		int x = bounds.x + (bounds.width - rect.width) / 2;
		int y = bounds.y + (bounds.height - rect.height) / 2;

		shell.setLocation(x, y);

		shell.setLayout(new FillLayout());

		// Icon
		shell.setImages(
				new Image[] { new Image(Display.getDefault(), "src/resources/icons/DuctDesignerIcon_128x128.png"),
						new Image(Display.getDefault(), "src/resources/icons/DuctDesignerIcon_48x48.png"),
						new Image(Display.getDefault(), "src/resources/icons/DuctDesignerIcon_32x32.png"),
						new Image(Display.getDefault(), "src/resources/icons/DuctDesignerIcon_22x22.png"),
						new Image(Display.getDefault(), "src/resources/icons/DuctDesignerIcon_16x16.png") });

		// init menu bar
		logger.log(LogLevel.DEBUG, "init menu");
		initMenu();

		duct = new Duct("Duct");

		// init tabs
		logger.log(LogLevel.DEBUG, "init tabs");
		initTabs();

		shell.open();

		newDuct();

		ductDesigner.showAll();

		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}

	}

	/**
	 * Initializes the main menu bar.
	 */
	private void initMenu() {
		// create menu bar
		Menu menuBar = new Menu(shell, SWT.BAR);

		// create "File" tab and items
		MenuItem fileMenuHeader = new MenuItem(menuBar, SWT.CASCADE);
		fileMenuHeader.setText(LocalizationHandler.getItem("app.gui.menu.file"));
		Menu fileMenu = new Menu(shell, SWT.DROP_DOWN);
		fileMenuHeader.setMenu(fileMenu);
		MenuItem fileNewItem = new MenuItem(fileMenu, SWT.PUSH);
		fileNewItem.setText(LocalizationHandler.getItem("app.gui.menu.file.new"));
		MenuItem fileOpenItem = new MenuItem(fileMenu, SWT.PUSH);
		fileOpenItem.setText(LocalizationHandler.getItem("app.gui.menu.file.open"));
		MenuItem fileSaveItem = new MenuItem(fileMenu, SWT.PUSH);
		fileSaveItem.setImage(IconHandler.getIcon(disp, "save_edit"));
		fileSaveItem.setText(LocalizationHandler.getItem("app.gui.menu.file.save"));
		MenuItem fileSaveAsItem = new MenuItem(fileMenu, SWT.PUSH);
		fileSaveAsItem.setImage(IconHandler.getIcon(disp, "saveall_edit"));
		fileSaveAsItem.setText(LocalizationHandler.getItem("app.gui.menu.file.saveas"));
		MenuItem fileExitItem = new MenuItem(fileMenu, SWT.PUSH);
		fileExitItem.setText(LocalizationHandler.getItem("app.gui.menu.file.exit"));

		// create "Edit" tab and items
		MenuItem editMenuHeader = new MenuItem(menuBar, SWT.CASCADE);
		editMenuHeader.setText(LocalizationHandler.getItem("app.gui.menu.edit"));
		Menu editMenu = new Menu(shell, SWT.DROP_DOWN);
		editMenuHeader.setMenu(editMenu);
		editUndo = new MenuItem(editMenu, SWT.PUSH);
		editUndo.setText(LocalizationHandler.getItem("app.gui.menu.edit.undo"));
		editUndo.setEnabled(false);
		editUndo.setImage(IconHandler.getIcon(disp, "undo_edit"));
		editRedo = new MenuItem(editMenu, SWT.PUSH);
		editRedo.setText(LocalizationHandler.getItem("app.gui.menu.edit.redo"));
		editRedo.setEnabled(false);
		editRedo.setImage(IconHandler.getIcon(disp, "redo_edit"));
		// Bar
		new MenuItem(editMenu, SWT.SEPARATOR);
		MenuItem editSetProfile = new MenuItem(editMenu, SWT.PUSH);
		editSetProfile.setText(LocalizationHandler.getItem("app.gui.dd.menu.edit.profile"));

		// create "Database Components" tab and items
		MenuItem compDBMenuHeader = new MenuItem(menuBar, SWT.CASCADE);
		compDBMenuHeader.setText(LocalizationHandler.getItem("app.gui.menu.compDB"));
		Menu compDBMenu = new Menu(shell, SWT.DROP_DOWN);
		compDBMenuHeader.setMenu(compDBMenu);
		MenuItem compDBNewItem = new MenuItem(compDBMenu, SWT.PUSH);
		compDBNewItem.setText(LocalizationHandler.getItem("app.gui.menu.compDB.new"));
		MenuItem compDBOpenItem = new MenuItem(compDBMenu, SWT.PUSH);
		compDBOpenItem.setText(LocalizationHandler.getItem("app.gui.menu.compDB.open"));
		// MenuItem compDBImportItem = new MenuItem(compDBMenu, SWT.PUSH);
		// compDBImportItem.setText(LocalizationHandler.getItem("app.gui.menu.compDB.import"));

		// create "Database Material" tab and items
		MenuItem matDBMenuHeader = new MenuItem(menuBar, SWT.CASCADE);
		matDBMenuHeader.setText(LocalizationHandler.getItem("app.gui.menu.matDB"));
		Menu matDBMenu = new Menu(shell, SWT.DROP_DOWN);
		matDBMenuHeader.setMenu(matDBMenu);
		MenuItem matDBNewItem = new MenuItem(matDBMenu, SWT.PUSH);
		matDBNewItem.setText(LocalizationHandler.getItem("app.gui.menu.matDB.new"));
		MenuItem matDBOpenItem = new MenuItem(matDBMenu, SWT.PUSH);
		matDBOpenItem.setText(LocalizationHandler.getItem("app.gui.menu.matDB.open"));

		// create "Help" tab and items
		MenuItem helpMenuHeader = new MenuItem(menuBar, SWT.CASCADE);
		helpMenuHeader.setText(LocalizationHandler.getItem("app.gui.menu.help"));
		Menu helpMenu = new Menu(shell, SWT.DROP_DOWN);
		helpMenuHeader.setMenu(helpMenu);
		// MenuItem helpContentItem = new MenuItem(helpMenu, SWT.PUSH);
		// helpContentItem.setText(LocalizationHandler.getItem("app.gui.menu.help.content"));
		MenuItem helpModelsItem = new MenuItem(helpMenu, SWT.PUSH);
		helpModelsItem.setText(LocalizationHandler.getItem("app.gui.menu.help.openhelp"));

		// add listeners
		fileNewItem.addSelectionListener(new fileNewItemListener());
		fileOpenItem.addSelectionListener(new fileOpenItemListener());
		fileSaveItem.addSelectionListener(new fileSaveItemListener());
		fileSaveAsItem.addSelectionListener(new fileSaveAsItemListener());
		fileExitItem.addSelectionListener(new fileExitItemListener());

		editMenu.addMenuListener(new MenuListener() {
			@Override
			public void menuShown(MenuEvent e) {
				setUndoRedoAvailability();
			}

			@Override
			public void menuHidden(MenuEvent e) {
				// Not used
			}
		});

		editRedo.addSelectionListener(new editRedoItemListener());
		editUndo.addSelectionListener(new editUndoItemListener());

		editSetProfile.addSelectionListener(new editProfileItemListener());

		compDBNewItem.addSelectionListener(new compDBNewItemListener());
		compDBOpenItem.addSelectionListener(new compDBOpenItemListener());

		matDBNewItem.addSelectionListener(new matDBNewItemListener());
		matDBOpenItem.addSelectionListener(new matDBOpenItemListener());

		helpModelsItem.addSelectionListener(new helpItemListener());

		// ductDesignTestItem.addSelectionListener(new
		// ductDesignTestItemListener());

		/* License dependent visibility */
		fileNewItem.setEnabled(LicenseHandler.can(LicenseActions.EDIT_DUCTMDL));
		fileSaveItem.setEnabled(LicenseHandler.can(LicenseActions.EDIT_DUCTMDL));
		fileSaveAsItem.setEnabled(LicenseHandler.can(LicenseActions.EDIT_DUCTMDL));

		compDBMenu.setEnabled(LicenseHandler.can(LicenseActions.EDIT_MDLLIBRARY));
		matDBMenu.setEnabled(LicenseHandler.can(LicenseActions.EDIT_MATLIBRARY));

		shell.setMenuBar(menuBar);
	}

	private void initTabs() {

		ductDesigner = new DuctConfigGraphGUI(shell, SWT.NONE, this.duct, ShowButtons.NONE);

		// tab for console
		final TabItem tabConsoleItem = new TabItem(ductDesigner.getTabFolder(), SWT.NONE);
		tabConsoleItem.setText(LocalizationHandler.getItem("app.gui.tabs.console"));
		tabConsoleItem.setToolTipText(LocalizationHandler.getItem("app.gui.tabs.consoletooltip"));
		tabConsoleItem.setControl(initConsole(ductDesigner.getTabFolder()));

		ductDesigner.getTabFolder().setSelection(0);

	}

	private StyledText initConsole(TabFolder tabFolder) {
		console = new StyledText(tabFolder, SWT.V_SCROLL | SWT.H_SCROLL);
		console.setEditable(false);
		console.setFont(new Font(disp, "Mono", 8, SWT.NORMAL));

		// Pipe Console
		final PrintStream backupSystemOutStream = System.out;
		final PrintStream backupSystemErrStream = System.err;
		System.setOut(new PrintStream(backupSystemOutStream) {
			@Override
			public void print(final String s) {
				disp.asyncExec(new Runnable() {
					@Override
					public void run() {
						String msg = "[" + (new SimpleDateFormat("yyyy.MM.dd HH:mm:ss")).format((new Date()))
								+ "] INFO:  " + s + "\n";
						console.append(msg);

						StyleRange[] sr = new StyleRange[1];
						sr[0] = new StyleRange(console.getCharCount() - msg.length(), msg.length(),
								disp.getSystemColor(SWT.COLOR_BLACK), disp.getSystemColor(SWT.COLOR_WHITE));
						console.replaceStyleRanges(sr[0].start, sr[0].length, sr);
					}
				});
				super.print(s);
			}
		});
		System.setErr(new PrintStream(backupSystemErrStream) {
			@Override
			public void print(final String s) {
				disp.asyncExec(new Runnable() {
					@Override
					public void run() {
						String msg = "[" + (new SimpleDateFormat("yyyy.MM.dd HH:mm:ss")).format((new Date()))
								+ "] ERROR: " + s + "\n";
						console.append(msg);

						StyleRange[] sr = new StyleRange[1];
						sr[0] = new StyleRange(console.getCharCount() - msg.length(), msg.length(),
								disp.getSystemColor(SWT.COLOR_RED), disp.getSystemColor(SWT.COLOR_WHITE));
						console.replaceStyleRanges(sr[0].start, sr[0].length, sr);
					}
				});
				super.print(s);
			}
		});

		return console;
	}

	/**
	 * menu item action listener for save item
	 * 
	 * @author dhampl
	 * 
	 */
	class fileNewItemListener implements SelectionListener {
		@Override
		public void widgetSelected(SelectionEvent event) {
			logger.log(LogLevel.DEBUG, "menu new item selected");
			newDuct();
		}

		@Override
		public void widgetDefaultSelected(SelectionEvent event) {
			// Not used
		}
	}

	/**
	 * menu item action listener for save item
	 * 
	 * @author sizuest
	 * 
	 */
	class fileSaveItemListener implements SelectionListener {
		@Override
		public void widgetSelected(SelectionEvent event) {
			logger.log(LogLevel.DEBUG, "menu save item selected");
			saveDuct();
		}

		@Override
		public void widgetDefaultSelected(SelectionEvent event) {
			// Not used
		}
	}

	/**
	 * menu item action listener for save as item
	 * 
	 * @author dhampl
	 * 
	 */
	class fileSaveAsItemListener implements SelectionListener {
		@Override
		public void widgetSelected(SelectionEvent event) {
			saveDuctAs();
		}

		@Override
		public void widgetDefaultSelected(SelectionEvent event) {
			// Not used
		}
	}

	/**
	 * menu item action listener for load item
	 * 
	 * @author dhampl
	 * 
	 */
	class fileOpenItemListener implements SelectionListener {
		@Override
		public void widgetSelected(SelectionEvent event) {
			openDuct();
		}

		@Override
		public void widgetDefaultSelected(SelectionEvent event) {
			// Not used
		}
	}

	/**
	 * menu item action listener for exit item
	 * 
	 * @author dhampl
	 * 
	 */
	class fileExitItemListener implements SelectionListener {
		@Override
		public void widgetSelected(SelectionEvent event) {
			logger.log(LogLevel.DEBUG, "menu exit item selected");
			shell.close();
			disp.dispose();
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.eclipse.swt.events.SelectionListener#widgetDefaultSelected(org
		 * .eclipse.swt.events.SelectionEvent)
		 */
		@Override
		public void widgetDefaultSelected(SelectionEvent e) {
			shell.close();
			disp.dispose();
		}
	}

	class editUndoItemListener implements SelectionListener {
		@Override
		public void widgetSelected(SelectionEvent event) {
			undo();
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.eclipse.swt.events.SelectionListener#widgetDefaultSelected(org
		 * .eclipse.swt.events.SelectionEvent)
		 */
		@Override
		public void widgetDefaultSelected(SelectionEvent e) {
			// Not used
		}
	}

	class editProfileItemListener implements SelectionListener {
		@Override
		public void widgetSelected(SelectionEvent event) {
			// create a dummy element to edit it's profile
			final DuctPipe element = new DuctPipe(1, 0.1, 0, 1);
			if (null != duct.getInletProfile())
				element.setProfile(null);

			Shell profileShell = EditDuctProfileGUI.editDuctProfileGUI(shell, element);

			profileShell.addDisposeListener(new DisposeListener() {
				@Override
				public void widgetDisposed(DisposeEvent e) {
					if (element.getProfile() != null) {
						duct.setProfile(element.getProfile());
						ductDesigner.update();
					}
				}
			});
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.eclipse.swt.events.SelectionListener#widgetDefaultSelected(org
		 * .eclipse.swt.events.SelectionEvent)
		 */
		@Override
		public void widgetDefaultSelected(SelectionEvent e) {
			// Not used
		}
	}

	class editRedoItemListener implements SelectionListener {
		@Override
		public void widgetSelected(SelectionEvent event) {
			redo();
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.eclipse.swt.events.SelectionListener#widgetDefaultSelected(org
		 * .eclipse.swt.events.SelectionEvent)
		 */
		@Override
		public void widgetDefaultSelected(SelectionEvent e) {
			// Not used
		}
	}

	/**
	 * menu item action listener for comp DB new item
	 * 
	 * @author manick
	 * 
	 */
	class compDBNewItemListener implements SelectionListener {
		@Override
		public void widgetSelected(SelectionEvent event) {
			EditMachineComponentGUI.newMachineComponentGUI(shell);
		}

		@Override
		public void widgetDefaultSelected(SelectionEvent event) {
			// Not used
		}
	}

	/**
	 * menu item action listener for comp DB open item
	 * 
	 * @author manick
	 * 
	 */
	class compDBOpenItemListener implements SelectionListener {
		@Override
		public void widgetSelected(SelectionEvent event) {
			new MachineComponentDBGUI(shell);
		}

		@Override
		public void widgetDefaultSelected(SelectionEvent event) {
			// Not used
		}
	}

	/**
	 * menu item action listener for material DB new item
	 * 
	 * @author manick
	 * 
	 */
	class matDBNewItemListener implements SelectionListener {
		@Override
		public void widgetSelected(SelectionEvent event) {
			EditMaterialGUI.newMaterialGUI(shell);
		}

		@Override
		public void widgetDefaultSelected(SelectionEvent event) {
			// Not used
		}
	}

	/**
	 * menu item action listener for material DB open item
	 * 
	 * @author manick
	 * 
	 */
	class matDBOpenItemListener implements SelectionListener {
		@Override
		public void widgetSelected(SelectionEvent event) {
			new MaterialDBGUI(shell);
		}

		@Override
		public void widgetDefaultSelected(SelectionEvent event) {
			// Not used
		}
	}

	/**
	 * menu item action listener for help about item
	 * 
	 * @author manick
	 * 
	 */
	class helpItemListener implements SelectionListener {
		@Override
		public void widgetSelected(SelectionEvent event) {
			logger.log(LogLevel.DEBUG, "help models item selected");
			HelpBrowserGUI.show(shell, "/Models/Ductdesigner");
		}

		@Override
		public void widgetDefaultSelected(SelectionEvent event) {
			// Not used
		}
	}

	/**
	 * returns the position of the shell (used to center new windows on current
	 * position)
	 * 
	 * @author manick
	 * @return shell position
	 * 
	 */

	public static int[] shellPosition() {
		// get postion of current shell
		Rectangle rect = shell.getBounds();

		// find the middle of the shell and return two dimensional array
		// position[0]: middle of the shell in horizontal direction
		// position[1]: middle of the shell in vertical direction
		int[] position = { 0, 0 };
		position[0] = rect.x + rect.width / 2;
		position[1] = rect.y + rect.height / 2;

		// return array
		return position;
	}

	private void newDuct() {
		this.path = "";
		this.duct.clear();
		this.duct.setName("New Duct");
		this.ductDesigner.setDuct(duct);
		this.ductDesigner.update();

		shell.setText("DuctDesigner: " + duct.getName());
	}

	private void saveDuct() {
		if ("" == this.path)
			saveDuctAs();
		else
			this.duct.saveToFile(this.path);

		shell.setText("DuctDesigner: " + duct.getName());
	}

	private void saveDuctAs() {
		String path = getFilePath(LocalizationHandler.getItem("app.gui.file.saveas"), SWT.SAVE);
		if (null == path)
			return;

		this.path = path;
		this.duct.saveToFile(this.path);

		shell.setText("DuctDesigner: " + duct.getName());
	}

	private void openDuct() {
		String path = getFilePath(LocalizationHandler.getItem("app.gui.file.open"), SWT.OPEN);
		if (null == path)
			return;

		this.path = path;
		this.duct = Duct.buildFromFile(this.path);
		this.duct.setRootDuct();
		this.ductDesigner.setDuct(duct);
		this.ductDesigner.update();

		shell.setText("DuctDesigner: " + duct.getName());
	}

	private String getFilePath(String titel, int style) {
		FileDialog fd = new FileDialog(shell, style);
		fd.setText(titel);
		fd.setFilterPath("C:/");
		fd.setFilterExtensions(new String[] { "*.duct", "*.*" });
		String selected = fd.open();

		return selected;
	}

	private void undo() {
		this.duct.undo();
		this.ductDesigner.update();
		setUndoRedoAvailability();
	}

	private void redo() {
		this.duct.redo();
		this.ductDesigner.update();
		setUndoRedoAvailability();
	}

	private void setUndoRedoAvailability() {
		editUndo.setEnabled(this.duct.getHistory().undoPossible());
		editRedo.setEnabled(this.duct.getHistory().redoPossible());

		editUndo.setText(LocalizationHandler.getItem("app.gui.menu.edit.undo") + " "
				+ LocalizationHandler.getItem(this.duct.getHistory().getUndoComment()));
		editRedo.setText(LocalizationHandler.getItem("app.gui.menu.edit.redo") + " "
				+ LocalizationHandler.getItem(this.duct.getHistory().getRedoComment()));
	}
}
