/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.dd.gui;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.TableEditor;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.ControlListener;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

import ch.zuestengineering.emod.States;
import ch.zuestengineering.emod.dd.model.Isolation;
import ch.zuestengineering.emod.dd.model.IsolationLayer;
import ch.zuestengineering.emod.gui.AConfigGUI;
import ch.zuestengineering.emod.gui.SelectMaterialGUI;
import ch.zuestengineering.emod.gui.icons.IconHandler;
import ch.zuestengineering.emod.gui.utils.ShellUtils;
import ch.zuestengineering.emod.gui.utils.TableUtils;
import ch.zuestengineering.emod.model.units.SiUnit;
import ch.zuestengineering.emod.utils.LocalizationHandler;

/**
 * Composite to edit the isolation of a duct
 * 
 * @author sizuest
 *
 */
public class EditDuctIsolationGUI extends AConfigGUI {
	private Table tableProperties;
	private Isolation isolationOld, isolationNew;

	/**
	 * Create the composite and add it to the parent
	 * 
	 * @param parent
	 * @param style
	 * @param iso
	 */
	public EditDuctIsolationGUI(Composite parent, int style, final Isolation iso) {
		super(parent, style);

		if (null != iso) {
			this.isolationOld = iso;
			this.isolationNew = iso.clone();
		}

		this.getContent().setLayout(new GridLayout(1, true));

		tableProperties = new Table(this.getContent(), SWT.BORDER | SWT.SINGLE | SWT.V_SCROLL);
		tableProperties.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 2, 1));
		tableProperties.setLinesVisible(true);
		tableProperties.setHeaderVisible(true);

		String[] titles = { LocalizationHandler.getItem("app.dd.elemet.gui.isolation.material"),
				LocalizationHandler.getItem("app.dd.elemet.gui.isolation.thickness") + " [" + (new SiUnit("m")) + "]",
				"        ", "        ", "        ", "        " };

		for (int i = 0; i < titles.length; i++) {
			TableColumn column = new TableColumn(tableProperties, SWT.NULL);
			column.setText(titles[i]);
			column.setWidth(32);
		}

		updatePropertyTable();
	}

	/**
	 * Create the composite in a new shell
	 * 
	 * @param parent
	 * @param isolation
	 * @return
	 */
	public static Shell editDuctIsolationGUI(Shell parent, Isolation isolation) {
		final Shell shell = new Shell(parent, SWT.SYSTEM_MODAL | SWT.CLOSE | SWT.MAX | SWT.RESIZE);
		shell.setLayout(new GridLayout());

		final EditDuctIsolationGUI gui = new EditDuctIsolationGUI(shell, SWT.NONE, isolation);

		shell.setText(LocalizationHandler.getItem("app.dd.elemet.gui.isolation.titel"));

		shell.setImages(parent.getImages());

		shell.layout();
		shell.open();
		shell.layout();

		shell.setSize(shell.computeSize(SWT.DEFAULT, SWT.DEFAULT));

		shell.addControlListener(new ControlListener() {

			@Override
			public void controlResized(ControlEvent e) {
				gui.layout();
			}

			@Override
			public void controlMoved(ControlEvent e) {
				gui.layout();
			}
		});

		gui.addDisposeListener(new DisposeListener() {
			@Override
			public void widgetDisposed(DisposeEvent e) {
				shell.dispose();
			}
		});

		ShellUtils.putToCenter(shell, parent);

		return shell;
	}

	private void updatePropertyTable() {

		tableProperties.clearAll();
		tableProperties.setItemCount(0);

		for (Control c : tableProperties.getChildren())
			c.dispose();

		try {
			TableUtils.addCellEditor(tableProperties, EditDuctIsolationGUI.class.getMethod("tableThicknessToLayers"),
					this, new int[] { 1 });
		} catch (Exception e) {
			e.printStackTrace();
		}

		layersToTable();

		TableUtils.packTable(tableProperties);

		this.pack();
		this.layout();
		this.getShell().pack();
		this.getShell().layout();
	}

	private void openMaterialGUI(int idx) {
		SelectMaterialGUI matGUI = new SelectMaterialGUI(this.getShell());
		String selection = matGUI.open();
		if (selection != "" & selection != null) {
			if (idx >= isolationNew.getLayers().size())
				isolationNew.addLayer(new IsolationLayer(selection, 0.01));
			else
				isolationNew.getLayers().get(idx).setMaterial(selection);
		}

		layersToTable();
	}

	@Override
	public void save() {
		isolationNew.cleanUp();
		isolationOld.setIsolation(isolationNew);
		updatePropertyTable();
	}

	@Override
	public void reset() {
		isolationNew = isolationOld.clone();

		updatePropertyTable();
	}

	private void addTableItem(final int index) {
		tableProperties.setRedraw(false);

		IsolationLayer layer = isolationNew.getLayers().get(index);

		// create new table item in the tableModelView
		final TableItem item = new TableItem(tableProperties, SWT.NONE, index);
		// create button to edit material
		final Button buttonSelectMaterial = new Button(tableProperties, SWT.PUSH);
		// create button to append a new layer
		final Button buttonAddLayer = new Button(tableProperties, SWT.PUSH);
		// create button to move layer up
		final Button buttonUpLayer = new Button(tableProperties, SWT.PUSH);
		// create button to move layer down
		final Button buttonDownLayer = new Button(tableProperties, SWT.PUSH);
		// create button to delete layer
		final Button buttonDeleteLayer = new Button(tableProperties, SWT.PUSH);

		item.setText(0, layer.getMaterial().toString());
		item.setText(1, layer.getThickness() + "");

		// create button to select material
		buttonSelectMaterial.setText("...");
		buttonSelectMaterial.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				openMaterialGUI(index);
				updatePropertyTable();
				wasEdited();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent event) {
				// Not used
			}
		});
		// pack the button and set it into the cell
		buttonSelectMaterial.pack();
		TableEditor editor1 = new TableEditor(tableProperties);
		editor1.minimumWidth = buttonSelectMaterial.getSize().x;
		editor1.horizontalAlignment = SWT.RIGHT;
		editor1.setEditor(buttonSelectMaterial, item, 0);

		// create button to add a new row
		buttonAddLayer.setImage(IconHandler.getIcon(getDisplay(), "add_obj"));
		buttonAddLayer.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				isolationNew.addLayer(index + 1, new IsolationLayer());
				updatePropertyTable();
				wasEdited();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent event) {
				// Not used
			}
		});
		// pack the button and set it into the cell
		buttonAddLayer.pack();
		TableEditor editor2 = new TableEditor(tableProperties);
		editor2.minimumWidth = buttonAddLayer.getSize().x;
		editor2.horizontalAlignment = SWT.LEFT;
		editor2.setEditor(buttonAddLayer, item, 2);

		// create button to move element up
		if (index > 0) {
			buttonUpLayer.setImage(IconHandler.getIcon(getDisplay(), "backward_nav"));
			buttonUpLayer.addSelectionListener(new SelectionListener() {
				@Override
				public void widgetSelected(SelectionEvent event) {
					isolationNew.moveLayerIn(index);
					updatePropertyTable();
					wasEdited();
				}

				@Override
				public void widgetDefaultSelected(SelectionEvent event) {
					// Not used
				}
			});
			// pack the button and set it into the cell
			buttonUpLayer.pack();
			TableEditor editor3 = new TableEditor(tableProperties);
			editor3.minimumWidth = buttonUpLayer.getSize().x;
			editor3.horizontalAlignment = SWT.LEFT;
			editor3.setEditor(buttonUpLayer, item, 3);
		}

		// create button to move element up
		if (index + 1 < isolationNew.getLayers().size()) {
			buttonDownLayer.setImage(IconHandler.getIcon(getDisplay(), "forward_nav"));
			buttonDownLayer.addSelectionListener(new SelectionListener() {
				@Override
				public void widgetSelected(SelectionEvent event) {
					isolationNew.moveLayerOut(index);
					updatePropertyTable();
					wasEdited();
				}

				@Override
				public void widgetDefaultSelected(SelectionEvent event) {
					// Not used
				}
			});
			// pack the button and set it into the cell
			buttonDownLayer.pack();
			TableEditor editor4 = new TableEditor(tableProperties);
			editor4.minimumWidth = buttonDownLayer.getSize().x;
			editor4.horizontalAlignment = SWT.LEFT;
			editor4.setEditor(buttonDownLayer, item, 4);
		}

		// create button to delete last row
		buttonDeleteLayer.setImage(IconHandler.getIcon(getDisplay(), "delete_obj"));
		buttonDeleteLayer.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				isolationNew.getLayers().remove(index);
				updatePropertyTable();
				wasEdited();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent event) {
				// Not used
			}
		});
		// pack the button and set it into the cell
		buttonDeleteLayer.pack();
		TableEditor editor5 = new TableEditor(tableProperties);
		editor5.minimumWidth = buttonDeleteLayer.getSize().x;
		editor5.horizontalAlignment = SWT.LEFT;
		editor5.setEditor(buttonDeleteLayer, item, 5);

		if (States.getStateCount() == 1)
			buttonDeleteLayer.setEnabled(false);

		TableColumn[] columns = tableProperties.getColumns();
		for (int i = 0; i < columns.length; i++) {
			columns[i].pack();
		}

		// if a cell gets deleted, make shure that the combo and buttons get
		// deleted too!
		item.addDisposeListener(new DisposeListener() {
			@Override
			public void widgetDisposed(DisposeEvent e) {
				buttonAddLayer.dispose();
				buttonDeleteLayer.dispose();
				buttonDownLayer.dispose();
				buttonUpLayer.dispose();
			}
		});
		tableProperties.setRedraw(true);
	}

	private void layersToTable() {
		if (isolationNew.getLayers().size() == 0) {
			// create button to add a new row
			final TableItem item = new TableItem(tableProperties, SWT.NONE, 0);
			final Button buttonAddLayer = new Button(tableProperties, SWT.PUSH);
			buttonAddLayer.setImage(IconHandler.getIcon(getDisplay(), "add_obj"));
			buttonAddLayer.addSelectionListener(new SelectionListener() {
				@Override
				public void widgetSelected(SelectionEvent event) {
					isolationNew.addLayer(new IsolationLayer());
					updatePropertyTable();
					wasEdited();
				}

				@Override
				public void widgetDefaultSelected(SelectionEvent event) {
					// Not used
				}
			});
			// pack the button and set it into the cell
			buttonAddLayer.pack();
			TableEditor editor2 = new TableEditor(tableProperties);
			editor2.minimumWidth = buttonAddLayer.getSize().x;
			editor2.horizontalAlignment = SWT.LEFT;
			editor2.setEditor(buttonAddLayer, item, 2);
		}

		for (int i = 0; i < isolationNew.getLayers().size(); i++) {
			addTableItem(i);
		}
	}

	/**
	 * Writes the tickness of the layers in the table back to the objects
	 */
	public void tableThicknessToLayers() {

		wasEdited();

		for (int i = 0; i < isolationNew.getLayers().size(); i++) {
			double thickness;

			thickness = Double.valueOf(tableProperties.getItem(i).getText(1));

			if (Double.isFinite(thickness))
				isolationNew.getLayers().get(i).setThickness(thickness);

		}
	}

}
