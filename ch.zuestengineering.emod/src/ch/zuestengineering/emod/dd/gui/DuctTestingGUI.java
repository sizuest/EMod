/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.dd.gui;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.TableEditor;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

import ch.zuestengineering.emod.dd.Duct;
import ch.zuestengineering.emod.gui.AGUITab;
import ch.zuestengineering.emod.gui.SelectMaterialGUI;
import ch.zuestengineering.emod.gui.utils.TableUtils;
import ch.zuestengineering.emod.model.material.Material;
import ch.zuestengineering.emod.utils.LocalizationHandler;

/**
 * Composite for the testing and analysis of a duct
 * 
 * @author sizuest
 *
 */
public class DuctTestingGUI extends AGUITab {

	private Duct duct;

	private TabFolder tabAnalysis;
	private static Table tableOpPoint;

	private DuctResultTableGUI resultTableGUI;
	private DuctResultGraphGUI resultGraphGUI;
	private DuctCharacteristicLineGUI characteristicLineGUI;
	private DuctSensitivityGUI sensitivityGUI;

	private double lastInletTemperature, lastInletPressure, lastOutletPressure, lastFlowRate;
	private String lastMaterial;

	/**
	 * Create a new analysis composite and add it to the parent
	 * 
	 * @param parent
	 * @param duct
	 */
	public DuctTestingGUI(Composite parent, Duct duct) {
		super(parent, SWT.NONE);
		this.setLayout(new GridLayout(1, true));

		this.duct = duct;

		this.duct.setMaterial(new Material(duct.getMaterialName()));

		init();
	}

	@Override
	public void init() {

		/* OP Table */
		tableOpPoint = new Table(this, SWT.BORDER | SWT.MULTI | SWT.V_SCROLL);
		tableOpPoint.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));
		tableOpPoint.setLinesVisible(true);
		tableOpPoint.setHeaderVisible(true);

		String[] titlesOP = { LocalizationHandler.getItem("app.gui.parameter"),
				LocalizationHandler.getItem("app.gui.parameter.value"),
				LocalizationHandler.getItem("app.gui.parameter.unit"), "        " };
		for (int i = 0; i < titlesOP.length; i++) {
			TableColumn column = new TableColumn(tableOpPoint, SWT.NULL);
			column.setText(titlesOP[i]);
			if (1 == i)
				column.setAlignment(SWT.RIGHT);
		}

		/* Tabs Analysis */
		tabAnalysis = new TabFolder(this, SWT.NONE);
		tabAnalysis.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));

		/* Testing Table */
		resultTableGUI = new DuctResultTableGUI(tabAnalysis, SWT.NONE, duct);

		/* Testing Chart */
		resultGraphGUI = new DuctResultGraphGUI(tabAnalysis, SWT.NONE, duct);

		/* Chart Characteristics */
		characteristicLineGUI = new DuctCharacteristicLineGUI(tabAnalysis, SWT.NONE, duct);

		/* Sensitivity */
		sensitivityGUI = new DuctSensitivityGUI(tabAnalysis, SWT.NONE, duct);

		TabItem tabAnalysisNum = new TabItem(tabAnalysis, SWT.NONE);
		tabAnalysisNum.setText(LocalizationHandler.getItem("app.dd.testing.gui.analysis.numerical"));
		tabAnalysisNum.setToolTipText("");
		tabAnalysisNum.setControl(resultTableGUI);

		TabItem tabAnalysisPlot = new TabItem(tabAnalysis, SWT.NONE);
		tabAnalysisPlot.setText(LocalizationHandler.getItem("app.dd.testing.gui.analysis.graphical"));
		tabAnalysisPlot.setToolTipText("");
		tabAnalysisPlot.setControl(resultGraphGUI);

		TabItem tabCharacteristicsPlot = new TabItem(tabAnalysis, SWT.NONE);
		tabCharacteristicsPlot.setText(LocalizationHandler.getItem("app.dd.testing.gui.analysis.characteristicdiag"));
		tabCharacteristicsPlot.setToolTipText("");
		tabCharacteristicsPlot.setControl(characteristicLineGUI);

		final TabItem tabSensitivity = new TabItem(tabAnalysis, SWT.NONE);
		tabSensitivity.setText("Parameter sensitivity");
		tabSensitivity.setToolTipText("");
		tabSensitivity.setControl(sensitivityGUI);

		/* Add editor and cp */
		try {
			TableUtils.addCellEditor(tableOpPoint, this.getClass().getDeclaredMethod("setOperationalPoint"), this,
					new int[] { 1 });
		} catch (Exception e) {
			e.printStackTrace();
		}

		/* Save current OP */
		lastInletPressure = Math.round(duct.getInletPressure() / 1000.0) / 1000.0;
		lastInletTemperature = Math.round(duct.getInletTemperature() * 10.0 - 2731.5) / 10.0;
		lastOutletPressure = Math.round(duct.getOutletPressure() / 1000.0) / 1000.0;
		lastFlowRate = Math.round(duct.getFlowRate() * 60E6) / 1000.0;
		lastMaterial = duct.getMaterialName();

		/* Update */
		update();

	}

	@Override
	public void update() {

		this.setEnabled(false);

		this.redraw();
		this.layout();

		duct.setMaterial(new Material(duct.getMaterialName()));
		duct.cleanUpFittings();

		setDuct(duct);

		updateOPTable();

		resultTableGUI.update();
		resultGraphGUI.update();
		characteristicLineGUI.update();
		sensitivityGUI.update();

		this.setEnabled(true);
	}

	/**
	 * Read the OP from the table and update everything
	 */
	public void setOperationalPoint() {
		readOPTable();
		update();
	}

	/**
	 * Set the fluid material and update everything, including the pump maps
	 * 
	 * @param type
	 */
	public void setMaterial(String type) {
		duct.setMaterial(type);
		characteristicLineGUI.setMaterial(type);
		update();
	}

	private void readOPTable() {

		fetchOPTable();

		/* Find the value which has changed */
		if (lastInletPressure != roundValue(duct.getInletPressure() / 1E6))
			duct.setInletPressure(lastInletPressure * 1E6);
		else if (lastOutletPressure != roundValue(duct.getOutletPressure() / 1E6))
			duct.setOutletPressure(lastOutletPressure * 1E6);
		else if (lastInletTemperature != roundValue(duct.getInletTemperature() - 273.15))
			duct.setInletTemperature(lastInletTemperature + 273.15);
		else if (lastFlowRate != roundValue(duct.getMassFlowRate() * 60E3))
			duct.setFlowRate(lastFlowRate / 60E3);
		else if (!lastMaterial.equals(duct.getMaterialName()))
			setMaterial(lastMaterial);

		fetchOPTable();

	}

	private double roundValue(double value) {
		return Double.valueOf(String.format("%.3g", value));
	}

	private void fetchOPTable() {
		/* Read the values from the table */
		for (int i = 0; i < tableOpPoint.getItemCount(); i++)
			try {
				switch (i) {
				case 0:
					lastFlowRate = Double.parseDouble(tableOpPoint.getItem(i).getText(1));
					break;
				case 1:
					lastInletPressure = Double.parseDouble(tableOpPoint.getItem(i).getText(1));
					break;
				case 2:
					lastOutletPressure = Double.parseDouble(tableOpPoint.getItem(i).getText(1));
					break;
				case 3:
					lastInletTemperature = Double.parseDouble(tableOpPoint.getItem(i).getText(1));
					break;
				case 4:
					lastMaterial = tableOpPoint.getItem(i).getText(1);
				}
			} catch (Exception e) {
			}
	}

	private void updateOPTable() {

		for (TableItem it : tableOpPoint.getItems()) {
			it.dispose();
		}

		tableOpPoint.clearAll();
		tableOpPoint.setItemCount(0);

		TableItem itemFlowRate, itemPressureIn, itemPressureOut, itemTemperatureF, itemMaterial;

		itemFlowRate = new TableItem(tableOpPoint, SWT.LEFT, 0);
		itemPressureIn = new TableItem(tableOpPoint, SWT.LEFT, 1);
		itemPressureOut = new TableItem(tableOpPoint, SWT.LEFT, 2);
		itemTemperatureF = new TableItem(tableOpPoint, SWT.LEFT, 3);
		itemMaterial = new TableItem(tableOpPoint, SWT.LEFT, 4);

		itemFlowRate.setText(0, LocalizationHandler.getItem("app.dd.testing.gui.flowrate"));
		itemFlowRate.setText(1, String.format("%.3g", duct.getFlowRateNominal() * 60E3));
		itemFlowRate.setText(2, "l/min");

		itemPressureIn.setText(0, LocalizationHandler.getItem("app.dd.testing.gui.pressurein"));
		itemPressureIn.setText(1, String.format("%.3g", duct.getInletPressure() / 1E6));
		itemPressureIn.setText(2, "MPa");

		itemPressureOut.setText(0, LocalizationHandler.getItem("app.dd.testing.gui.pressureout"));
		itemPressureOut.setText(1, String.format("%.3g", duct.getOutletPressure() / 1E6));
		itemPressureOut.setText(2, "MPa");

		itemTemperatureF.setText(0, LocalizationHandler.getItem("app.dd.testing.gui.temperaturefluid"));
		itemTemperatureF.setText(1, String.format("%.3g", duct.getInletTemperature() - 273.15));
		itemTemperatureF.setText(2, "�C");

		itemMaterial.setText(0, LocalizationHandler.getItem("app.dd.testing.gui.coolant"));
		itemMaterial.setText(1, duct.getMaterialName());
		itemMaterial.setText(2, "");

		final TableEditor editorButton = new TableEditor(tableOpPoint);

		final Button buttonEditMaterial = new Button(tableOpPoint, SWT.NONE);
		buttonEditMaterial.setText("...");
		buttonEditMaterial.setLayoutData(new GridData(SWT.FILL, SWT.TOP, false, true, 1, 1));
		buttonEditMaterial.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				openMaterialGUI();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent event) {
				// Not used
			}
		});
		buttonEditMaterial.pack();
		editorButton.minimumWidth = buttonEditMaterial.getSize().x;
		editorButton.horizontalAlignment = SWT.LEFT;
		editorButton.setEditor(buttonEditMaterial, itemMaterial, 3);

		itemMaterial.addDisposeListener(new DisposeListener() {
			@Override
			public void widgetDisposed(DisposeEvent e) {
				buttonEditMaterial.dispose();
				editorButton.dispose();
			}
		});

		TableColumn[] columns = tableOpPoint.getColumns();
		for (int j = 0; j < columns.length; j++) {
			columns[j].pack();
		}
	}

	private void openMaterialGUI() {
		SelectMaterialGUI matGUI = new SelectMaterialGUI(this.getShell());
		String selection = matGUI.open();
		if (selection != "" & selection != null)
			setMaterial(selection);
	}

	/**
	 * Set the duct object to be handled
	 * 
	 * @param duct
	 */
	public void setDuct(Duct duct) {
		this.duct = duct;
		sensitivityGUI.setDuct(duct);
		resultGraphGUI.setDuct(duct);
		resultTableGUI.setDuct(duct);
		characteristicLineGUI.setDuct(duct);
	}

	@Override
	public void save() {
		// TODO Auto-generated method stub

	}

	@Override
	public void wasEdited() {
		save();
	}

}
