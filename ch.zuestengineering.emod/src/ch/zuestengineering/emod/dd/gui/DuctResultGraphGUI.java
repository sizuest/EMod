/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.dd.gui;

import java.text.DecimalFormat;
import java.util.ArrayList;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.swtchart.IAxis;
import org.swtchart.ILineSeries;
import org.swtchart.ILineSeries.PlotSymbolType;
import org.swtchart.ISeries.SeriesType;
import org.swtchart.LineStyle;

import ch.zuestengineering.emod.dd.Duct;
import ch.zuestengineering.emod.dd.model.ADuctElement;
import ch.zuestengineering.emod.dd.model.DuctBypass;
import ch.zuestengineering.emod.model.units.SiUnit;
import ch.zuestengineering.emod.utils.LocalizationHandler;

/**
 * Implements a graph based representation of the results of a duct analysis
 * 
 * @author simon
 *
 */
public class DuctResultGraphGUI extends ADDGUITab {

	private DuctResultGraph chartTesting;
	private ILineSeries lineSeriesPressureTesting, lineSeriesHTCTesting, lineSeriesTemperatureFluid,
			lineSeriesTemperatureWall;
	private Color colorPressure, colorHTC, colorTemperature;
	private Menu menuChartTesting;

	protected DuctResultGraphGUI(Composite parent, int i, Duct duct) {
		super(parent, i);

		this.duct = duct;

		init();
	}

	@Override
	public void init() {

		setLayout(new GridLayout(1, false));

		colorPressure = new Color(getDisplay(), new RGB(0, 0, 255));
		colorHTC = new Color(getDisplay(), new RGB(255, 80, 0));
		colorTemperature = new Color(getDisplay(), new RGB(0, 255, 0));

		chartTesting = new DuctResultGraph(this, SWT.NONE);
		chartTesting.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));

		chartTesting.getChart().getAxisSet().getXAxis(0).getTitle().setText(
				LocalizationHandler.getItem("app.dd.testing.gui.location") + " [" + (new SiUnit("m")).toString() + "]");
		chartTesting.getChart().getAxisSet().getXAxis(0).getTick()
				.setForeground(Display.getDefault().getSystemColor(0));
		chartTesting.getChart().getAxisSet().getXAxis(0).getTitle()
				.setForeground(Display.getDefault().getSystemColor(0));
		chartTesting.getChart().getAxisSet().createYAxis();
		chartTesting.getChart().getAxisSet().createYAxis();
		chartTesting.getChart().getAxisSet().getYAxis(0).getTitle().setText(
				LocalizationHandler.getItem("app.dd.testing.gui.htc") + " [" + (new SiUnit("W/K")).toString() + "]");
		chartTesting.getChart().getAxisSet().getYAxis(0).getTick().setForeground(colorHTC);
		chartTesting.getChart().getAxisSet().getYAxis(0).getTitle().setForeground(colorHTC);
		chartTesting.getChart().getAxisSet().getYAxis(1).getTick().setFormat(new DecimalFormat("0.###E0"));
		chartTesting.getChart().getAxisSet().getYAxis(1).getTitle()
				.setText(LocalizationHandler.getItem("app.dd.testing.gui.pressure") + " [MPa]");
		chartTesting.getChart().getAxisSet().getYAxis(1).getTick().setForeground(colorPressure);
		chartTesting.getChart().getAxisSet().getYAxis(1).getTitle().setForeground(colorPressure);
		chartTesting.getChart().getAxisSet().getYAxis(2).getTitle()
				.setText(LocalizationHandler.getItem("app.dd.testing.gui.temperature") + " [�C]");
		chartTesting.getChart().getAxisSet().getYAxis(2).getTick().setForeground(colorTemperature);
		chartTesting.getChart().getAxisSet().getYAxis(2).getTitle().setForeground(colorTemperature);
		chartTesting.getChart().getTitle().setVisible(false);

		final Composite plotArea = chartTesting.getChart().getPlotArea();

		plotArea.addListener(SWT.MouseHover, new Listener() {
			@Override
			public void handleEvent(Event event) {
				IAxis xAxis = chartTesting.getChart().getAxisSet().getXAxis(0);

				double x = xAxis.getDataCoordinate(event.x);

				plotArea.setToolTipText(getElementName(duct, x));
			}
		});

		lineSeriesHTCTesting = (ILineSeries) chartTesting.getChart().getSeriesSet().createSeries(SeriesType.LINE,
				LocalizationHandler.getItem("app.dd.testing.gui.htc"));
		lineSeriesHTCTesting.setYAxisId(0);
		lineSeriesHTCTesting.setLineColor(colorHTC);
		lineSeriesHTCTesting.setLineWidth(2);
		lineSeriesHTCTesting.enableArea(true);
		lineSeriesHTCTesting.setSymbolType(PlotSymbolType.NONE);

		lineSeriesPressureTesting = (ILineSeries) chartTesting.getChart().getSeriesSet().createSeries(SeriesType.LINE,
				LocalizationHandler.getItem("app.dd.testing.gui.pressure"));
		lineSeriesPressureTesting.setYAxisId(1);
		lineSeriesPressureTesting.setLineColor(colorPressure);
		lineSeriesPressureTesting.setLineWidth(2);
		lineSeriesPressureTesting.setSymbolType(PlotSymbolType.NONE);

		lineSeriesTemperatureFluid = (ILineSeries) chartTesting.getChart().getSeriesSet().createSeries(SeriesType.LINE,
				LocalizationHandler.getItem("app.dd.testing.gui.temperature.fluid"));
		lineSeriesTemperatureFluid.setYAxisId(2);
		lineSeriesTemperatureFluid.setLineColor(colorTemperature);
		lineSeriesTemperatureFluid.setLineWidth(2);
		lineSeriesTemperatureFluid.setSymbolType(PlotSymbolType.NONE);

		lineSeriesTemperatureWall = (ILineSeries) chartTesting.getChart().getSeriesSet().createSeries(SeriesType.LINE,
				LocalizationHandler.getItem("app.dd.testing.gui.temperature.bulk"));
		lineSeriesTemperatureWall.setYAxisId(2);
		lineSeriesTemperatureWall.setLineColor(colorTemperature);
		lineSeriesTemperatureWall.setLineWidth(2);
		lineSeriesTemperatureWall.setSymbolType(PlotSymbolType.NONE);
		lineSeriesTemperatureWall.setLineStyle(LineStyle.DASH);

		menuChartTesting = new Menu(chartTesting);

		chartTesting.setMenu(menuChartTesting);
		chartTesting.getChart().getPlotArea().setMenu(menuChartTesting);

	}

	@Override
	public void update() {

		if (duct.getElements().size() == 0) {
			lineSeriesPressureTesting.setXSeries(new double[] { 0 });
			lineSeriesPressureTesting.setYSeries(new double[] { 0 });

			lineSeriesHTCTesting.setXSeries(new double[] { 0 });
			lineSeriesHTCTesting.setYSeries(new double[] { 0 });

			lineSeriesTemperatureFluid.setXSeries(new double[] { 0 });
			lineSeriesTemperatureFluid.setYSeries(new double[] { 0 });

			lineSeriesTemperatureWall.setXSeries(new double[] { 0 });
			lineSeriesTemperatureWall.setYSeries(new double[] { 0 });
		} else {

			double[] pressure, position1, htc, position2, temperatureFluid, temperatureWall;
			ArrayList<Double> positionArr = getPositions(this.duct, 0),
					htcArr = getHTCs(this.duct, duct.getInletPressure(), duct.getInletTemperature(),
							duct.getFlowRate()),
					pressureArr = getPressures(this.duct, duct.getInletPressure(), duct.getInletTemperature(),
							duct.getFlowRate()),
					tempFluidArr = getTemperaturesOut(this.duct, duct.getInletPressure(), duct.getInletTemperature(),
							duct.getFlowRate()),
					tempWallArr = getTemperaturesWall(this.duct, duct.getInletPressure(), duct.getInletTemperature(),
							duct.getFlowRate());

			if (positionArr.size() < 1)
				return;

			pressure = new double[pressureArr.size() + 1];
			temperatureFluid = new double[pressureArr.size() + 1];
			temperatureWall = new double[positionArr.size() * 2];
			position1 = new double[positionArr.size() + 1];
			htc = new double[htcArr.size() * 2];
			position2 = new double[positionArr.size() * 2];

			pressure[0] = duct.getInletPressure() * 1E-6;
			temperatureFluid[0] = duct.getInletTemperature() - 273.15;
			position1[0] = 0;
			for (int i = 0; i < pressureArr.size(); i++) {
				pressure[i + 1] = pressureArr.get(i) * 1E-6;
				temperatureFluid[i + 1] = tempFluidArr.get(i) - 273.15;
				position1[i + 1] = positionArr.get(i);
			}

			htc[0] = htcArr.get(0);
			position2[0] = 0;
			temperatureWall[0] = tempWallArr.get(0) - 273.15;

			int i;
			for (i = 0; i < positionArr.size(); i++) {
				htc[2 * i + 1] = htcArr.get(i);
				position2[2 * i + 1] = positionArr.get(i);

				temperatureWall[2 * i + 1] = tempWallArr.get(i) - 273.15;

				if (i < positionArr.size() - 1) {
					htc[2 * i + 2] = htcArr.get(i + 1);
					position2[2 * i + 2] = positionArr.get(i);

					temperatureWall[2 * i + 2] = tempWallArr.get(i + 1) - 273.15;
				}
			}

			lineSeriesPressureTesting.setXSeries(position1);
			lineSeriesPressureTesting.setYSeries(pressure);

			lineSeriesHTCTesting.setXSeries(position2);
			lineSeriesHTCTesting.setYSeries(htc);

			lineSeriesTemperatureFluid.setXSeries(position1);
			lineSeriesTemperatureFluid.setYSeries(temperatureFluid);

			lineSeriesTemperatureWall.setXSeries(position2);
			lineSeriesTemperatureWall.setYSeries(temperatureWall);
		}

		chartTesting.getChart().getLegend().setVisible(false);

		try {
			chartTesting.getChart().getAxisSet().adjustRange();
			chartTesting.redraw();
		} catch (Exception e) {
			logger.warning("Chart update failed!");
		}

		chartTesting.pack();
		this.layout();
	}

	@Override
	public void save() {
		// TODO Auto-generated method stub

	}

	@Override
	public void wasEdited() {
		// TODO Auto-generated method stub

	}

	private String getElementName(Duct duct, double x) {

		double pos = 0;

		for (ADuctElement e : duct.getElements())
			if (pos + e.getLength() > x) {
				if (e instanceof DuctBypass)
					return e.getName() + ": " + getElementName(((DuctBypass) e).getPrimary(), x - pos);
				else
					return e.getName();
			} else
				pos += e.getLength();

		return "";
	}

	/**
	 * Returns an ArrayList with the temperatures at the walls of the elements
	 * 
	 * @param duct
	 * @param inlet
	 * @param flowRate
	 * @return
	 */
	private ArrayList<Double> getTemperaturesWall(Duct duct, double pressureIn, double temperatureIn, double flowRate) {
		ArrayList<Double> temperatures = new ArrayList<Double>();

		for (ADuctElement e : duct.getElements()) {
			if (e instanceof DuctBypass) {
				temperatures.addAll(getTemperaturesWall(((DuctBypass) e).getPrimary(), pressureIn, temperatureIn,
						((DuctBypass) e).getFlowRatePrimary(flowRate, pressureIn, temperatureIn)));
			} else {
				temperatures.add(e.getWallTemperature(temperatureIn, flowRate, pressureIn));
			}

			pressureIn = e.getPressureOut(flowRate, pressureIn, temperatureIn);
			temperatureIn = e.getTemperatureOut(temperatureIn, flowRate, pressureIn);
		}

		return temperatures;
	}

	/**
	 * Returns an ArrayList with the HTC of each element
	 * 
	 * @param duct
	 * @param pressureIn
	 * @param flowRate
	 * @return
	 */
	private ArrayList<Double> getHTCs(Duct duct, double pressureIn, double temperatureIn, double flowRate) {
		ArrayList<Double> htcs = new ArrayList<Double>();
		ArrayList<Double> pressures = getPressures(duct, pressureIn, temperatureIn, flowRate);
		ArrayList<Double> temperatures = getTemperaturesOut(duct, pressureIn, temperatureIn, flowRate);

		for (ADuctElement e : duct.getElements()) {
			if (e instanceof DuctBypass) {
				if (htcs.size() > 0)
					htcs.addAll(getHTCs(((DuctBypass) e).getPrimary(), pressures.get(htcs.size() - 1),
							temperatures.get(htcs.size() - 1),
							((DuctBypass) e).getFlowRatePrimary(flowRate, pressureIn, duct.getInletTemperature())));
				else
					htcs.addAll(getHTCs(((DuctBypass) e).getPrimary(), pressureIn, temperatureIn,
							((DuctBypass) e).getFlowRatePrimary(flowRate, pressureIn, duct.getInletTemperature())));
			} else {
				htcs.add(
						e.getRth(flowRate, pressures.get(htcs.size()), temperatures.get(htcs.size())) * e.getSurface());
			}
		}

		return htcs;
	}

	/**
	 * Returns an ArrayList with the temperatures at the outlets of the elements
	 * 
	 * @param duct
	 * @param inlet
	 * @param flowRate
	 * @return
	 */
	private ArrayList<Double> getTemperaturesOut(Duct duct, double pressureIn, double temperatureIn, double flowRate) {
		ArrayList<Double> temperatures = new ArrayList<Double>();

		for (ADuctElement e : duct.getElements()) {
			if (e instanceof DuctBypass) {
				temperatures.addAll(getTemperaturesOut(((DuctBypass) e).getPrimary(), pressureIn, temperatureIn,
						((DuctBypass) e).getFlowRatePrimary(flowRate, pressureIn, temperatureIn)));
			} else {
				temperatures.add(e.getTemperatureOut(temperatureIn, flowRate, pressureIn));
			}

			pressureIn = e.getPressureOut(flowRate, pressureIn, temperatureIn);
			temperatureIn = e.getTemperatureOut(temperatureIn, flowRate, pressureIn);
		}

		return temperatures;
	}

	/**
	 * Returns an ArrayList with the pressures at the outlets of the elements
	 * 
	 * @param duct
	 * @param pressureIn
	 * @param flowRate
	 * @return
	 */
	private ArrayList<Double> getPressures(Duct duct, double pressureIn, double temperatureIn, double flowRate) {
		ArrayList<Double> pressures = new ArrayList<Double>();

		for (ADuctElement e : duct.getElements()) {
			if (e instanceof DuctBypass) {
				pressures.addAll(getPressures(((DuctBypass) e).getPrimary(), pressureIn, temperatureIn,
						((DuctBypass) e).getFlowRatePrimary(flowRate, pressureIn, duct.getInletTemperature())));
				if (pressures.size() > 0)
					pressureIn = pressures.get(pressures.size() - 1);
			} else {
				pressureIn = e.getPressureOut(duct.getFlowRate(), pressureIn, temperatureIn);
				pressures.add(pressureIn);
			}

			temperatureIn = e.getTemperatureOut(temperatureIn, flowRate, pressureIn);
		}

		return pressures;
	}

	/**
	 * Retuens and ArrayList with the position of the elements outlets
	 * 
	 * @param duct
	 * @param offset
	 * @return
	 */
	private ArrayList<Double> getPositions(Duct duct, double offset) {
		ArrayList<Double> positions = new ArrayList<Double>();

		for (ADuctElement e : duct.getElements()) {
			if (e instanceof DuctBypass) {
				positions.addAll(getPositions(((DuctBypass) e).getPrimary(), offset));
				if (positions.size() > 0)
					offset = positions.get(positions.size() - 1);
			} else {
				offset += e.getLength();
				positions.add(offset);
			}
		}

		return positions;
	}

}
