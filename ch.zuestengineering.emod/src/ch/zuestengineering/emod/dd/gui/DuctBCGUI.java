/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.dd.gui;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.TableEditor;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

import ch.zuestengineering.emod.dd.Duct;
import ch.zuestengineering.emod.dd.model.ADuctElement;
import ch.zuestengineering.emod.dd.model.DuctBypass;
import ch.zuestengineering.emod.dd.model.DuctFitting;
import ch.zuestengineering.emod.gui.AGUITab;
import ch.zuestengineering.emod.gui.utils.TableUtils;
import ch.zuestengineering.emod.model.material.Material;
import ch.zuestengineering.emod.model.units.SiUnit;
import ch.zuestengineering.emod.utils.LocalizationHandler;

/**
 * Composite for the testing and analysis of a duct
 * 
 * @author sizuest
 *
 */
public class DuctBCGUI extends AGUITab {

	private Duct duct;

	private static Table tableBC;

	/**
	 * Create a new analysis composite and add it to the parent
	 * 
	 * @param parent
	 * @param duct
	 */
	public DuctBCGUI(Composite parent, Duct duct) {
		super(parent, SWT.NONE);
		this.setLayout(new GridLayout(1, true));

		this.duct = duct;

		this.duct.setMaterial(new Material(duct.getMaterialName()));

		init();
	}

	@Override
	public void init() {

		/* BC Table */
		tableBC = new Table(this, SWT.BORDER | SWT.MULTI | SWT.V_SCROLL);
		tableBC.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		tableBC.setLinesVisible(true);
		tableBC.setHeaderVisible(true);

		String[] titlesBC = { LocalizationHandler.getItem("app.dd.config.gui.element"),
				LocalizationHandler.getItem("app.dd.testing.gui.bc.temperature") + " [�C]",
				LocalizationHandler.getItem("app.dd.testing.gui.bc.heatFlux") + " [" + (new SiUnit("W")).toString()
						+ "]",
				LocalizationHandler.getItem("app.dd.elemet.gui.adiabatic"), "        " };
		for (int i = 0; i < titlesBC.length; i++) {
			TableColumn column = new TableColumn(tableBC, SWT.NULL);
			column.setText(titlesBC[i]);
			if (1 == i)
				column.setAlignment(SWT.RIGHT);
		}

		/* Update */
		update();

	}

	@Override
	public void update() {

		this.setEnabled(false);

		this.redraw();
		this.layout();

		duct.setMaterial(new Material(duct.getMaterialName()));
		duct.cleanUpFittings();

		setDuct(duct);

		updateBCTable();

		this.setEnabled(true);
	}

	/**
	 * Read the OP from the table and update everything
	 */
	public void setOperationalPoint() {
		update();
	}

	/**
	 * Read the BC from the table and update everything
	 */
	public void setBoundaryConditions() {
		readBCTable();
	}

	/**
	 * Set the fluid material and update everything, including the pump maps
	 * 
	 * @param type
	 */
	public void setMaterial(String type) {
		duct.setMaterial(type);
		update();
	}

	private void readBCTable() {
		ADuctElement curElement;

		for (int i = 0; i < tableBC.getItemCount(); i++) {
			// Fetch element object by name
			// curElement =
			// duct.getElement(tableBC.getItem(i).getText(0).replaceAll("\\s*[0-9]:\\s*",
			// ""));
			curElement = duct.getAllElements().get(i);

			if (!(curElement.getAdiabatic())) {

				// First try: wall temperature
				try {
					curElement.setWallTemperature(Double.parseDouble(tableBC.getItem(i).getText(1)) + 273.15);
				} catch (Exception e1) {

					tableBC.getItem(i).setText(1, "");
					curElement.setWallTemperature(Double.NaN);

					// Second try: heat source
					try {
						curElement.setHeatSource(Double.parseDouble(tableBC.getItem(i).getText(2)));
					} catch (Exception e2) {
						tableBC.getItem(i).setText(2, "");
						curElement.setHeatSource(Double.NaN);
					}
				}
			}

		}
	}

	private void updateBCTable() {
		tableBC.setEnabled(false);

		for (Control c : tableBC.getChildren()) {
			c.dispose();
		}

		tableBC.clearAll();
		tableBC.setItemCount(0);

		try {
			TableUtils.addCellEditor(tableBC, this.getClass().getDeclaredMethod("setBoundaryConditions"), this,
					new int[] { 1, 2 });
		} catch (Exception e) {
			e.printStackTrace();
		}

		addToBCTable(duct, "", duct.getFlowRate(), duct.getInletTemperature(), duct.getInletPressure());

		TableColumn[] columns = tableBC.getColumns();
		for (int j = 0; j < columns.length; j++) {
			columns[j].pack();
		}

		tableBC.setEnabled(true);

	}

	private void addToBCTable(Duct duct, String prefix, double flowRate, double temperatureIn, double pressureIn) {

		for (final ADuctElement e : duct.getElements()) {
			if (!(e instanceof DuctFitting)) {
				int i = tableBC.getItemCount();
				final TableItem itemProp = new TableItem(tableBC, SWT.RIGHT, i);

				itemProp.setText(0, prefix + e.getName());

				if (e instanceof DuctBypass) {
					addToBCTable(((DuctBypass) e).getPrimary(), prefix.replaceFirst("[0-9]:", "  ") + "  1: ",
							((DuctBypass) e).getFlowRatePrimary(flowRate, pressureIn, temperatureIn), temperatureIn,
							pressureIn);
					addToBCTable(((DuctBypass) e).getSecondary(), prefix.replaceFirst("[0-9]:", "  ") + "  2: ",
							((DuctBypass) e).getFlowRateSecondary(flowRate, pressureIn, temperatureIn), temperatureIn,
							pressureIn);
				} else {
					if (e.hasWallTemperature())
						itemProp.setText(1, String.format("%.3g", e.getWallTemperature(0, 0, 0) - 273.15));
					else
						itemProp.setText(1, "       ");

					if (e.hasHeatSource())
						itemProp.setText(2, String.format("%.3g", e.getWallHeatFlux(0, 0, 0)));
					else
						itemProp.setText(2, "        ");

					// Checkbox for adiabatic
					TableEditor editorButton = new TableEditor(tableBC);

					editorButton = new TableEditor(tableBC);
					final Button buttonEditAdiabatic = new Button(tableBC, SWT.CHECK);
					buttonEditAdiabatic.setSelection(e.getAdiabatic());
					buttonEditAdiabatic.setLayoutData(new GridData(SWT.CENTER, SWT.TOP, false, true, 1, 1));
					buttonEditAdiabatic.addSelectionListener(new SelectionListener() {
						@Override
						public void widgetSelected(SelectionEvent event) {
							e.setAdiabatic(buttonEditAdiabatic.getSelection());
						}

						@Override
						public void widgetDefaultSelected(SelectionEvent event) {
							// Not used
						}
					});
					buttonEditAdiabatic.pack();
					editorButton.minimumWidth = buttonEditAdiabatic.getSize().x;
					editorButton.horizontalAlignment = SWT.CENTER;
					editorButton.setEditor(buttonEditAdiabatic, itemProp, 3);
				}
			}

			temperatureIn = e.getTemperatureOut(temperatureIn, flowRate, pressureIn);
			pressureIn = e.getPressureOut(flowRate, pressureIn, temperatureIn);

		}
	}

	/**
	 * Set the duct object to be handled
	 * 
	 * @param duct
	 */
	public void setDuct(Duct duct) {
		this.duct = duct;
	}

	@Override
	public void save() {
		// TODO Auto-generated method stub

	}

	@Override
	public void wasEdited() {
		save();
	}

}
