/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.dd.gui;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

import ch.zuestengineering.emod.dd.Duct;
import ch.zuestengineering.emod.dd.model.ADuctElement;
import ch.zuestengineering.emod.dd.model.DuctBypass;
import ch.zuestengineering.emod.dd.model.DuctFitting;
import ch.zuestengineering.emod.gui.utils.TableUtils;
import ch.zuestengineering.emod.model.units.SiUnit;
import ch.zuestengineering.emod.utils.LocalizationHandler;

/**
 * Implements a table based representation of the results of a duct analysis
 * 
 * @author simon
 *
 */
public class DuctResultTableGUI extends ADDGUITab {

	private static Table tableTesting;

	protected DuctResultTableGUI(Composite parent, int i, Duct duct) {
		super(parent, i);

		this.duct = duct;

		init();
	}

	@Override
	public void init() {
		setLayout(new GridLayout(1, false));

		tableTesting = new Table(this, SWT.BORDER | SWT.MULTI | SWT.V_SCROLL);
		tableTesting.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		tableTesting.setLinesVisible(true);
		tableTesting.setHeaderVisible(true);

		String[] titlesTest = { LocalizationHandler.getItem("app.dd.config.gui.element"),
				"V [" + SiUnit.pow(new SiUnit("m"), 3).toString() + "]", "S [" + (new SiUnit("m^2")).toString() + "]",
				"l [" + (new SiUnit("m")).toString() + "]", "p [MPa]", "\u03B6 [-]", "R [W/�C]",
				"\u03B1 [" + (new SiUnit("W m^-2 K^-1")).toString() + "]", "Q [l/min]", "T F [�C]", "T W [�C]",
				"Re [-]", "" };
		for (int i = 0; i < titlesTest.length; i++) {
			TableColumn column = new TableColumn(tableTesting, SWT.NULL);
			column.setText(titlesTest[i]);
			column.setAlignment(SWT.RIGHT);
		}

		try {
			TableUtils.addCopyToClipboard(tableTesting);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void update() {
		tableTesting.setEnabled(false);

		tableTesting.clearAll();
		tableTesting.setItemCount(0);

		addToTestingTable(duct, "", duct.getFlowRate(), duct.getInletTemperature(), duct.getInletPressure());

		int i = tableTesting.getItemCount();
		TableItem itemProp = new TableItem(tableTesting, SWT.RIGHT, i);

		itemProp.setText(0, "TOTAL");
		itemProp.setText(1, String.format("%.3g", duct.getVolume()));
		itemProp.setText(2, String.format("%.3g", duct.getSurface()));
		itemProp.setText(3, String.format("%.3g", duct.getLength()));
		itemProp.setText(4, String.format("%.3g",
				1E-6 * duct.getPressureDrop(duct.getFlowRate(), duct.getInletPressure(), duct.getInletTemperature())));
		// itemProp.setText(5, String.format("%.3g",
		// duct.getPressureLossCoefficient(duct.getFlowRate(), duct.getInletPressure(),
		// duct.getInletTemperature())));
		itemProp.setText(6, String.format("%.3g",
				duct.getThermalResistance(duct.getFlowRate(), duct.getInletPressure(), duct.getInletTemperature())));
		itemProp.setText(7, String.format("%.3g",
				duct.getHTC(duct.getFlowRate(), duct.getInletPressure(), duct.getInletTemperature())));
		itemProp.setFont(new Font(itemProp.getDisplay(), "Arial", 10, SWT.BOLD));

		TableColumn[] columns = tableTesting.getColumns();
		for (int j = 0; j < columns.length; j++) {
			columns[j].pack();
		}

		tableTesting.setEnabled(true);
	}

	@Override
	public void save() {
		// TODO Auto-generated method stub

	}

	@Override
	public void wasEdited() {
		// TODO Auto-generated method stub

	}

	private void addToTestingTable(Duct duct, String prefix, double flowRate, double temperatureIn, double pressureIn) {

		for (ADuctElement e : duct.getElements()) {
			int i = tableTesting.getItemCount();
			final TableItem itemProp = new TableItem(tableTesting, SWT.RIGHT, i);

			itemProp.setText(0, prefix + e.getName());
			itemProp.setText(1, String.format("%.3g", e.getVolume()));
			itemProp.setText(2, String.format("%.3g", e.getSurface()));
			itemProp.setText(3, String.format("%.3g", e.getLength()));
			itemProp.setText(4, String.format("%.3g", 1E-6 * e.getPressureDrop(flowRate, pressureIn, temperatureIn)));
			itemProp.setText(5,
					String.format("%.3g", e.getPressureLossCoefficient(flowRate, pressureIn, temperatureIn)));
			itemProp.setText(6, String.format("%.3g", e.getRth(flowRate, pressureIn, temperatureIn) * e.getSurface()));
			itemProp.setText(7, String.format("%.3g", e.getRth(flowRate, pressureIn, temperatureIn)));
			itemProp.setText(8, String.format("%.3g", flowRate * 60E3));
			itemProp.setText(9,
					String.format("%.3g", -273.15 + e.getTemperatureOut(temperatureIn, flowRate, pressureIn)));
			if (!(e instanceof DuctBypass))
				itemProp.setText(10,
						String.format("%.3g", -273.15 + e.getWallTemperature(temperatureIn, flowRate, pressureIn)));

			if (!(e instanceof DuctFitting)) {
				double Re = e.getRe(temperatureIn, flowRate, pressureIn);
				itemProp.setText(11, String.format("%.3g", Re));
				itemProp.setBackground(11,
						new Color(getDisplay(), (int) Math.max(0, Math.min(255, 255 - (Re * 255 - 2300 * 255) / 7700)),
								(int) Math.max(0, Math.min(255, (Re * 255 - 2300 * 255) / 7700)), 0));
			}

			if (e instanceof DuctBypass) {
				addToTestingTable(((DuctBypass) e).getPrimary(), prefix.replaceFirst("[0-9]:", "  ") + "  1: ",
						((DuctBypass) e).getFlowRatePrimary(flowRate, pressureIn, temperatureIn), temperatureIn,
						pressureIn);
				addToTestingTable(((DuctBypass) e).getSecondary(), prefix.replaceFirst("[0-9]:", "  ") + "  2: ",
						((DuctBypass) e).getFlowRateSecondary(flowRate, pressureIn, temperatureIn), temperatureIn,
						pressureIn);
			}

			temperatureIn = e.getTemperatureOut(temperatureIn, flowRate, pressureIn);
			pressureIn = e.getPressureOut(flowRate, pressureIn, temperatureIn);

		}
	}

}
