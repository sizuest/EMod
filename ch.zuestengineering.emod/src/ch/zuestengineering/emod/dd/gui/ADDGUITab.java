/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.dd.gui;

import org.eclipse.swt.widgets.Composite;

import ch.zuestengineering.emod.dd.Duct;
import ch.zuestengineering.emod.gui.AGUITab;

/**
 * Implements the definitions for a DD gui tab
 * 
 * @author simon
 *
 */
public abstract class ADDGUITab extends AGUITab {

	protected Duct duct = new Duct();

	/**
	 * @param parent
	 * @param i
	 */
	public ADDGUITab(Composite parent, int i) {
		super(parent, i);
	}

	/**
	 * Set the duct object to be handled
	 * 
	 * @param duct
	 */
	public void setDuct(Duct duct) {
		this.duct = duct;
	}

}
