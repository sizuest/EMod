/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.dd.gui;

import org.eclipse.swt.widgets.Composite;
import org.swtchart.IAxis;

import ch.zuestengineering.emod.gui.utils.AChart;

/**
 * @author Simon Z�st
 *
 */
public class DuctResultGraph extends AChart {

	/**
	 * @param parent
	 * @param style
	 */
	public DuctResultGraph(Composite parent, int style) {
		super(parent, style);
	}

	@Override
	protected int getLineWidth() {
		return 2;
	}

	@Override
	protected void zoomIn(int x, int y) {
		for (IAxis ax : getChart().getAxisSet().getXAxes())
			ax.zoomIn(ax.getDataCoordinate(x));

		adjustLineWidth();
		getChart().redraw();
	}

	@Override
	protected void zoomOut(int x, int y) {
		for (IAxis ax : getChart().getAxisSet().getXAxes())
			ax.zoomOut(ax.getDataCoordinate(x));
		adjustLineWidth();
		getChart().redraw();
	}

}
