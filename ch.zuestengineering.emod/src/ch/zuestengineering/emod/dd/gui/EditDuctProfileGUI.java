/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.dd.gui;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CCombo;
import org.eclipse.swt.custom.TableEditor;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.ControlListener;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

import ch.zuestengineering.emod.dd.Duct;
import ch.zuestengineering.emod.dd.model.ADuctElement;
import ch.zuestengineering.emod.dd.model.AHydraulicProfile;
import ch.zuestengineering.emod.gui.AConfigGUI;
import ch.zuestengineering.emod.gui.utils.ShellUtils;
import ch.zuestengineering.emod.gui.utils.TableUtils;
import ch.zuestengineering.emod.model.parameters.ParameterSet;
import ch.zuestengineering.emod.model.units.SiUnit;
import ch.zuestengineering.emod.utils.LocalizationHandler;

/**
 * GUI to edit a duct
 * 
 * @author sizuest
 *
 */
public class EditDuctProfileGUI extends AConfigGUI {

	private Table tableProperties;
	private AHydraulicProfile profileNew;
	private AHydraulicProfile[] candidates = Duct.getAvailableProfiles();
	private ParameterSet allParameters = new ParameterSet();
	private CCombo comboProfile;
	private ADuctElement element;

	/**
	 * Create a duct editor and add it to the parent composite
	 * 
	 * @param parent
	 * @param style
	 * @param element
	 */
	public EditDuctProfileGUI(Composite parent, int style, ADuctElement element) {
		super(parent, style);

		this.element = element;

		if (element.getProfileIn() == null)
			this.profileNew = candidates[0];

		/* Write global parameterset */
		for (int i = 0; i < candidates.length; i++) {
			if (element.getProfileIn() != null)
				if (candidates[i].getClass().equals(element.getProfileIn().getClass())) {
					candidates[i].setParameterSet(element.getProfileIn().getParameterSet());
					this.profileNew = candidates[i];
				}

			allParameters.merge(candidates[i].getParameterSet());
		}

		this.getContent().setLayout(new GridLayout(1, true));

		tableProperties = new Table(this.getContent(), SWT.BORDER | SWT.SINGLE | SWT.V_SCROLL);
		tableProperties.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 2, 1));
		tableProperties.setLinesVisible(true);
		tableProperties.setHeaderVisible(true);

		String[] titles = { LocalizationHandler.getItem("app.dd.elemet.gui.property"),
				LocalizationHandler.getItem("app.dd.elemet.gui.value"),
				LocalizationHandler.getItem("app.dd.elemet.gui.unit"), "        " };

		for (int i = 0; i < titles.length; i++) {
			TableColumn column = new TableColumn(tableProperties, SWT.NULL);
			column.setText(titles[i]);
			column.setWidth(32);
		}

		updatePropertyTable();

	}

	/**
	 * Create a duct designer in a new shell
	 * 
	 * @param parent
	 * @param element
	 * @return
	 */
	public static Shell editDuctProfileGUI(Shell parent, ADuctElement element) {
		final Shell shell = new Shell(parent, SWT.SYSTEM_MODAL | SWT.CLOSE | SWT.MAX | SWT.RESIZE);
		shell.setLayout(new GridLayout());

		final EditDuctProfileGUI gui = new EditDuctProfileGUI(shell, SWT.NONE, element);

		shell.setText(LocalizationHandler.getItem("app.dd.elemet.gui.profile.title") + ": " + element.getName());

		shell.setImages(parent.getImages());

		gui.getContent().layout();
		gui.layout();

		shell.pack();
		shell.layout();
		shell.open();

		shell.setSize(shell.computeSize(SWT.DEFAULT, SWT.DEFAULT));

		shell.addControlListener(new ControlListener() {

			@Override
			public void controlResized(ControlEvent e) {
				gui.layout();
			}

			@Override
			public void controlMoved(ControlEvent e) {
				gui.layout();
			}
		});

		gui.addDisposeListener(new DisposeListener() {
			@Override
			public void widgetDisposed(DisposeEvent e) {
				shell.dispose();
			}
		});

		ShellUtils.putToCenter(shell, parent);

		return shell;
	}

	private void updateProfile() {
		element.setProfile(this.profileNew);
	}

	private void updatePropertyTable() {

		tableProperties.clearAll();
		tableProperties.setItemCount(0);

		for (Control c : tableProperties.getChildren())
			c.dispose();

		try {
			TableUtils.addCellEditor(tableProperties, this, new int[] { 1 });
		} catch (Exception e1) {
			e1.printStackTrace();
		}

		TableItem itemType = new TableItem(tableProperties, SWT.NONE, 0);

		itemType.setText(0, LocalizationHandler.getItem("app.dd.elemet.gui.profile.shape"));

		comboProfile = new CCombo(tableProperties, SWT.NONE);
		String[] comboItems = new String[candidates.length];
		for (int i = 0; i < candidates.length; i++)
			comboItems[i] = candidates[i].getClass().getSimpleName().replace("HP", "");
		comboProfile.setItems(comboItems);

		TableEditor editorCombo = new TableEditor(tableProperties);
		comboProfile.setText(this.profileNew.getClass().getSimpleName().replace("HP", ""));
		comboProfile.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				comboProfile.setEnabled(false);

				profileNew = candidates[comboProfile.getSelectionIndex()];
				profileNew.setParameterSet(allParameters);
				comboProfile.dispose();

				wasEdited();

				updatePropertyTable();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent event) {
				// Not used
			}
		});

		comboProfile.pack();
		editorCombo.minimumWidth = comboProfile.getSize().x;
		editorCombo.grabHorizontal = true;
		editorCombo.horizontalAlignment = SWT.LEFT;
		editorCombo.setEditor(comboProfile, itemType, 1);

		for (String key : this.profileNew.getParameterSet().getNames()) {

			final int idx = tableProperties.getItemCount();
			final TableItem itemParam = new TableItem(tableProperties, SWT.NONE, idx);

			itemParam.setText(0, key);
			try {
				itemParam.setText(1, allParameters.getPhysicalValue(key).getValue() + "");
				itemParam.setText(2, allParameters.getPhysicalValue(key).getUnit().toString());
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		for (int i = 0; i < allParameters.getParameterSet().size()
				- this.profileNew.getParameterSet().getParameterSet().size(); i++) {
			final int idx = tableProperties.getItemCount();
			final TableItem itemParam = new TableItem(tableProperties, SWT.NONE, idx);
			itemParam.setText(0, "");
		}

		TableColumn[] columns = tableProperties.getColumns();
		for (int j = 0; j < columns.length; j++) {
			columns[j].pack();
		}

		this.layout();
	}

	@Override
	public void save() {

		// Read new Config
		for (int i = 1; i < tableProperties.getItemCount(); i++) {
			try {
				allParameters.setPhysicalValue(tableProperties.getItem(i).getText(0),
						Double.valueOf(tableProperties.getItem(i).getText(1)),
						new SiUnit("" + tableProperties.getItem(i).getText(2)));
			} catch (Exception e) {
				logger.warning("Can not parse input '" + tableProperties.getItem(i).getText(0) + "' as number: "
						+ tableProperties.getItem(i).getText(1));
			}
		}

		profileNew.setParameterSet(allParameters);
		updateProfile();
		updatePropertyTable();
	}

	@Override
	public void reset() {

		for (int i = 0; i < candidates.length; i++) {
			if (candidates[i].getClass().equals(element.getProfileIn().getClass())) {
				candidates[i].setParameterSet(element.getProfileIn().getParameterSet());
				this.profileNew = candidates[i];
			}
			allParameters.merge(candidates[i].getParameterSet());
		}

		updatePropertyTable();
	}
}
