/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.dd.gui;

import java.util.ArrayList;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

import ch.zuestengineering.emod.dd.Duct;
import ch.zuestengineering.emod.dd.analysis.DuctSensitivity;
import ch.zuestengineering.emod.dd.analysis.SensitivityResult;
import ch.zuestengineering.emod.gui.utils.TableUtils;

/**
 * Implements a GUI for the sensitivity analysis of a duct
 * 
 * @author Simon Z�st
 *
 */
public class DuctSensitivityGUI extends ADDGUITab {

	private DuctSensitivity sensitivity;

	private Table table;
	private Combo combo;

	/**
	 * @param parent
	 * @param style
	 * @param duct
	 */
	public DuctSensitivityGUI(Composite parent, int style, Duct duct) {
		super(parent, style);

		this.duct = duct;

		init();
	}

	/**
	 * 
	 */
	@Override
	public void init() {

		this.setLayout(new GridLayout(2, false));

		combo = new Combo(this, SWT.BORDER);
		combo.setItems(new String[] { "pressure", "HTC", "HTC/pressure" });
		combo.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				update();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				// TODO Auto-generated method stub

			}
		});
		combo.select(0);

		table = new Table(this, SWT.BORDER | SWT.MULTI | SWT.V_SCROLL);
		table.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 2, 1));
		table.setLinesVisible(true);
		table.setHeaderVisible(true);

		TableUtils.addCopyToClipboard(table);

	}

	@Override
	public void update() {
		table.setVisible(false);

		sensitivity = new DuctSensitivity(duct);

		for (TableColumn tc : table.getColumns())
			tc.dispose();

		table.clearAll();
		table.removeAll();

		SensitivityResult results;

		if (combo.getSelectionIndex() == 0)
			results = sensitivity.getPressureSensitivity();
		else if (combo.getSelectionIndex() == 1)
			results = sensitivity.getHTCSensitivity(293.15);
		else
			results = sensitivity.getHTCPerPSensitivity(293.15);

		double maxVal = results.getMax();

		ArrayList<String> cols = results.getParameterNames();
		ArrayList<String> rows = results.getElementNames();

		(new TableColumn(table, SWT.NULL)).setText("  ");
		for (String s : cols)
			(new TableColumn(table, SWT.NULL)).setText(s);

		for (String e : rows) {
			TableItem item = new TableItem(table, SWT.CENTER);
			item.setText(0, e);

			for (int i = 0; i < cols.size(); i++) {
				if (!Double.isNaN(results.get(e, cols.get(i))))
					item.setText(i + 1, String.format("%.3g", results.get(e, cols.get(i))));
				item.setBackground(i + 1, getColor(results.get(e, cols.get(i)), maxVal));
			}
		}

		table.setVisible(true);

		TableColumn[] columns = table.getColumns();
		for (int j = 0; j < columns.length; j++) {
			columns[j].pack();
		}

		this.layout();

	}

	private Color getColor(double value, double max) {
		if (max == 0)
			return new Color(getDisplay(), 255, 255, 255);

		if (value < 0)
			return new Color(getDisplay(), 255, 255 - (int) (-255 * value / max), 255 - (int) (-255 * value / max));
		else
			return new Color(getDisplay(), 255 - (int) (255 * value / max), 255, 255 - (int) (255 * value / max));
	}

	@Override
	public void save() {
	}

	@Override
	public void wasEdited() {
	}

}
