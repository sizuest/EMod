/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.dd.gui;

import java.util.ArrayList;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.swtchart.ILineSeries;
import org.swtchart.ILineSeries.PlotSymbolType;
import org.swtchart.ISeries.SeriesType;
import org.swtchart.LineStyle;

import ch.zuestengineering.emod.dd.Duct;
import ch.zuestengineering.emod.gui.utils.AChart;
import ch.zuestengineering.emod.model.pm.Pump;
import ch.zuestengineering.emod.model.units.SiUnit;
import ch.zuestengineering.emod.utils.LocalizationHandler;

/**
 * @author Simon Z�st
 *
 */
public class DuctCharacteristicLine extends AChart {

	Duct duct;

	private ILineSeries lineSeriesPressureCC, lineSeriesHTCCC;
	private ArrayList<ILineSeries> lineSeriesPumps = new ArrayList<ILineSeries>();
	private Color colorPressure, colorHTC;
	private ArrayList<Pump> pumps = new ArrayList<Pump>();

	private double maxFlowRate = 10;

	/**
	 * @param parent
	 * @param duct
	 */
	public DuctCharacteristicLine(Composite parent, Duct duct) {
		super(parent, SWT.NONE);

		this.duct = duct;

		colorPressure = new Color(getDisplay(), new RGB(0, 0, 255));
		colorHTC = new Color(getDisplay(), new RGB(255, 80, 0));

		getChart().getAxisSet().getXAxis(0).getTitle()
				.setText(LocalizationHandler.getItem("app.dd.testing.gui.flowrate") + " [l/min]");
		getChart().getAxisSet().getXAxis(0).getTick().setForeground(Display.getDefault().getSystemColor(0));
		getChart().getAxisSet().getXAxis(0).getTitle().setForeground(Display.getDefault().getSystemColor(0));
		getChart().getAxisSet().createYAxis();
		getChart().getAxisSet().getYAxis(0).getTitle().setText(
				LocalizationHandler.getItem("app.dd.testing.gui.htc") + " [" + (new SiUnit("W/K")).toString() + "]");
		getChart().getAxisSet().getYAxis(0).getTick().setForeground(colorHTC);
		getChart().getAxisSet().getYAxis(0).getTitle().setForeground(colorHTC);
		getChart().getAxisSet().getYAxis(1).getTitle()
				.setText(LocalizationHandler.getItem("app.dd.testing.gui.pressure") + " [MPa]");
		getChart().getAxisSet().getYAxis(1).getTick().setForeground(colorPressure);
		getChart().getAxisSet().getYAxis(1).getTitle().setForeground(colorPressure);
		getChart().getTitle().setVisible(false);

		lineSeriesHTCCC = (ILineSeries) getChart().getSeriesSet().createSeries(SeriesType.LINE,
				LocalizationHandler.getItem("app.dd.testing.gui.htc"));
		lineSeriesHTCCC.setYAxisId(0);
		lineSeriesHTCCC.setLineColor(colorHTC);
		lineSeriesHTCCC.setLineWidth(2);
		lineSeriesHTCCC.enableArea(true);
		lineSeriesHTCCC.setSymbolType(PlotSymbolType.NONE);

		lineSeriesPressureCC = (ILineSeries) getChart().getSeriesSet().createSeries(SeriesType.LINE,
				LocalizationHandler.getItem("app.dd.testing.gui.pressure"));
		lineSeriesPressureCC.setYAxisId(1);
		lineSeriesPressureCC.setLineColor(colorPressure);
		lineSeriesPressureCC.setLineWidth(2);
		lineSeriesPressureCC.setSymbolType(PlotSymbolType.NONE);
	}

	@Override
	protected int getLineWidth() {
		return 2;
	}

	@Override
	protected void zoomIn(int x, int y) {
		this.maxFlowRate *= .9;
		update();
	}

	@Override
	protected void zoomOut(int x, int y) {
		this.maxFlowRate *= 1.1;
		update();
	}

	@Override
	public void update() {
		double[] pressure, htc, flowRate, pressurePump;
		int N = 100;

		/* Duct */
		pressure = new double[N];
		flowRate = new double[N];
		htc = new double[N];

		for (int i = 0; i < N; i++) {
			flowRate[i] = i * this.maxFlowRate / (N - 1);
			pressure[i] = 1E-6
					* duct.getPressureDrop(flowRate[i] / 60E3, duct.getInletPressure(), duct.getInletTemperature());
			htc[i] = duct.getThermalResistance(flowRate[i] / 60E3, duct.getInletPressure(), duct.getInletTemperature());

			if (!Double.isFinite(htc[i]))
				htc[i] = 0;
		}

		lineSeriesPressureCC.setXSeries(flowRate);
		lineSeriesPressureCC.setYSeries(pressure);

		lineSeriesHTCCC.setXSeries(flowRate);
		lineSeriesHTCCC.setYSeries(htc);

		/* Pumps */
		pressurePump = new double[N];

		for (int i = 0; i < this.pumps.size(); i++) {
			pumps.get(i).updatePumpMap(1, duct.getInletTemperature());
			for (int j = 0; j < N; j++)
				pressurePump[j] = 1E-6 * this.pumps.get(i).getPressure(flowRate[j] / 60E3);

			this.lineSeriesPumps.get(i).setXSeries(flowRate);
			this.lineSeriesPumps.get(i).setYSeries(pressurePump);
		}

		getChart().getAxisSet().adjustRange();
		getChart().redraw();
	}

	/**
	 * @param pump
	 */
	public void addPump(final Pump pump) {

		pump.getFluidPropertiesList().get(0).setMaterial(duct.getMaterial());
		pump.updatePumpMap(1, duct.getInletTemperature());

		// Add Pump to array
		this.pumps.add(pump);

		// Create and add new line series
		ILineSeries line = (ILineSeries) getChart().getSeriesSet().createSeries(SeriesType.LINE,
				LocalizationHandler.getItem("app.dd.testing.gui.analysis.characteristicdiag.pump") + ": "
						+ pump.getType());
		line.setYAxisId(1);
		line.setLineStyle(LineStyle.DOT);
		line.setLineWidth(2);
		line.setSymbolType(PlotSymbolType.NONE);
		this.lineSeriesPumps.add(line);

		// Adjust colors
		setPumpColors();

		update();
	}

	/**
	 * Add the pump map of the given type to the characteristic diagram
	 * 
	 * @param type
	 */
	public void addPump(String type) {
		try {
			addPump(new Pump(type));
		} catch (Exception e2) {
			// Not used
		}
	}

	/**
	 * @param type
	 * @return
	 */
	public int removePump(String type) {
		// Find index
		int index = -1;

		for (int i = 0; i < this.pumps.size(); i++)
			if (this.pumps.get(i).getType().equals(type)) {
				index = i;
				break;
			}

		if (-1 == index)
			return -1;

		// Dispose and remove elements
		pumps.remove(index);
		getChart().getSeriesSet().deleteSeries(this.lineSeriesPumps.get(index).getId());
		this.lineSeriesPumps.remove(index);

		// Adjust colors
		setPumpColors();

		update();

		return index;

	}

	private void setPumpColors() {
		int i = 1;
		for (ILineSeries l : this.lineSeriesPumps)
			l.setLineColor(
					new Color(Display.getCurrent(), new RGB(0, 100 + 155 * i++ / this.lineSeriesPumps.size(), 0)));
	}

	/**
	 * Set the fluid material for the pump maps
	 * 
	 * @param type
	 */
	public void setMaterial(String type) {
		for (Pump p : pumps)
			p.getFluidPropertiesList().get(0).setMaterial(duct.getMaterial());
	}

	/**
	 * @return
	 */
	public ArrayList<Pump> getPumps() {
		return pumps;
	}

	/**
	 * @param duct
	 */
	public void setDuct(Duct duct) {
		this.duct = duct;
	}

}
