/*******************************************************************************
  * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.dd;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.Writer;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.logging.Logger;
import java.util.regex.Pattern;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlTransient;

import ch.zuestengineering.emod.EModFileHandling;
import ch.zuestengineering.emod.dd.model.ADuctElement;
import ch.zuestengineering.emod.dd.model.AHydraulicProfile;
import ch.zuestengineering.emod.dd.model.DuctArc;
import ch.zuestengineering.emod.dd.model.DuctBypass;
import ch.zuestengineering.emod.dd.model.DuctDefinedValues;
import ch.zuestengineering.emod.dd.model.DuctDefinedValues2;
import ch.zuestengineering.emod.dd.model.DuctDrilling;
import ch.zuestengineering.emod.dd.model.DuctElbowFitting;
import ch.zuestengineering.emod.dd.model.DuctExtension;
import ch.zuestengineering.emod.dd.model.DuctFitting;
import ch.zuestengineering.emod.dd.model.DuctFlowAround;
import ch.zuestengineering.emod.dd.model.DuctHelix;
import ch.zuestengineering.emod.dd.model.DuctPartialFlowAround;
import ch.zuestengineering.emod.dd.model.DuctPipe;
import ch.zuestengineering.emod.dd.model.DuctReduction;
import ch.zuestengineering.emod.dd.model.DuctStaggeredHelix;
import ch.zuestengineering.emod.dd.model.DuctWireTurbulator;
import ch.zuestengineering.emod.dd.model.HPAnnulus;
import ch.zuestengineering.emod.dd.model.HPCircular;
import ch.zuestengineering.emod.dd.model.HPRectangular;
import ch.zuestengineering.emod.dd.model.Isolation;
import ch.zuestengineering.emod.dd.model.IsolationLayer;
import ch.zuestengineering.emod.model.material.Material;
import ch.zuestengineering.emod.utils.PropertiesHandler;
import ch.zuestengineering.emod.utils.Undo;

/**
 * Implements the generic flow rate dependent properties of a duct: - HTC - Zeta
 * 
 * @author sizuest
 * 
 */
@XmlRootElement(namespace = "ch.zuestengineering.emod")
@XmlSeeAlso({ ADuctElement.class, AHydraulicProfile.class, DuctDrilling.class, DuctPipe.class, DuctElbowFitting.class,
		DuctFlowAround.class, DuctFitting.class, DuctHelix.class, DuctElbowFitting.class, DuctDefinedValues.class,
		DuctDefinedValues2.class, DuctBypass.class, DuctArc.class, DuctWireTurbulator.class, DuctStaggeredHelix.class,
		DuctPartialFlowAround.class, DuctExtension.class, DuctReduction.class, HPRectangular.class, HPCircular.class,
		HPAnnulus.class, Isolation.class, IsolationLayer.class, Material.class, DuctSettings.class })
@XmlAccessorType(XmlAccessType.FIELD)
public class Duct implements Cloneable {
	private String name;
	@XmlTransient
	private Undo<Duct> history;
	@XmlTransient
	private Duct rootDuct;
	@XmlElement
	private DuctSettings settings = new DuctSettings();

	/**
	 * @return the settings
	 */
	@XmlTransient
	private DuctSettings getSettings() {
		if (null == settings)
			settings = new DuctSettings();
		return settings;
	}

	/**
	 * @param pressure
	 */
	@XmlTransient
	public void setInletPressure(double pressure) {
		if (getSettings().getInletPressure() == pressure)
			return;

		getSettings().setInletPressure(pressure);
		updateFlowRate();
	}

	/**
	 * @param pressure
	 */
	@XmlTransient
	public void setOutletPressure(double pressure) {
		if (getSettings().getOutletPressure() == pressure)
			return;

		getSettings().setOutletPressure(pressure);
		updateFlowRate();
	}

	/**
	 * @param temp
	 */
	@XmlTransient
	public void setInletTemperature(double temp) {
		if (getSettings().getInletTemperature() == temp)
			return;

		getSettings().setInletTemperature(temp);
		updateFlowRate();
	}

	/**
	 * @param flowRate
	 */
	@XmlTransient
	public void setFlowRate(double flowRate) {
		if (getSettings().getFlowRate() == flowRate)
			return;

		getSettings().setFlowRate(flowRate);
		updateInletPressure();
	}

	/**
	 * @param flowRate
	 */
	@XmlTransient
	public void setMassFlowRate(double flowRate) {
		if (getSettings().getMassFlowRate() == flowRate)
			return;

		getSettings().setMassFlowRate(flowRate);
		updateInletPressure();
	}

	/**
	 * @return
	 */
	public double getInletPressure() {
		return getSettings().getInletPressure();
	}

	/**
	 * @return
	 */
	public double getOutletPressure() {
		return getSettings().getOutletPressure();
	}

	/**
	 * @return
	 */
	public double getInletTemperature() {
		return getSettings().getInletTemperature();
	}

	/**
	 * @return
	 */
	public double getFlowRate() {
		return getSettings().getFlowRate();
	}

	/**
	 * @return
	 */
	public double getFlowRateNominal() {
		return getSettings().getFlowRateNominal();
	}

	/**
	 * @return
	 */
	public double getMassFlowRate() {
		return getSettings().getMassFlowRate();
	}

	/**
	 * @return
	 */
	@XmlTransient
	public String getMaterialName() {
		return getSettings().getMaterialType();
	}

	private void updateFlowRate() {
		getSettings().setMassFlowRate(getMassFlowRate(getSettings().getInletPressure(),
				getSettings().getOutletPressure(), getSettings().getInletTemperature()));
	}

	private void updateInletPressure() {
		getSettings().setInletPressure(getInletPressure(getSettings().getFlowRate(),
				getSettings().getInletTemperature(), getSettings().getOutletPressure()));
	}

	private static Logger logger = Logger.getLogger(DuctDesignerMain.class.getName());

	@XmlElementWrapper
	@XmlElement
	private ArrayList<ADuctElement> elements = new ArrayList<ADuctElement>();

	/**
	 * Constructor for unmarshaller
	 */
	public Duct() {
	}

	/**
	 * @param name
	 */
	public Duct(String name) {
		this.name = name;
		init();
	}

	/**
	 * Called by unmarshaller after loading from XML
	 * 
	 * @param u
	 * @param parent
	 */
	public void afterUnmarshal(final Unmarshaller u, final Object parent) {
		init();
	}

	private void init() {
		cleanUpFittings();

		history = new Undo<Duct>(10, (new Duct()).clone(getRootDuct()));
	}

	/**
	 * Checks and corrects all fittings (diameters of the adjected elements)
	 */
	private void reconnectFittings() {
		for (int i = 1; i < elements.size() - 1; i++)
			if (getElement(i) instanceof DuctFitting)
				((DuctFitting) getElement(i)).setProfiles(getElement(i - 1).getProfileOut(),
						getElement(i + 1).getProfileIn());
	}

	/**
	 * Inserts fittings, adapts fittings or removes fittings where ever required
	 */
	public void cleanUpFittings() {
		if (elements.size() == 0)
			return;

		reconnectFittings();
		if (getElement(0) instanceof DuctFitting) {
			removeElement(getElement(0).getName());
		}
		for (int i = elements.size() - 2; i >= 0; i--) {
			if (getElement(i) instanceof DuctFitting) {
				if (((DuctFitting) getElement(i)).hasEqualProfiles()) {
					elements.remove(i);
					continue;
				}

				if (getElement(i + 1) instanceof DuctFitting) {
					elements.remove(i + 1);
					continue;
				}

				getElement(i).setName("Fitting_" + getElement(i - 1).getName() + "-" + getElement(i + 1).getName());
			} else if (!(getElement(i + 1) instanceof DuctFitting)
					& (getElement(i).getDiameter() != getElement(i + 1).getDiameter())) {
				elements.add(i + 1,
						new DuctFitting("Fitting_" + getElement(i).getName() + "-" + getElement(i + 1).getName(),
								getElement(i).getProfileOut(), getElement(i + 1).getProfileIn()));
				elements.get(i + 1).setMaterial(getMaterial());
			}
		}

		if (elements.size() == 0)
			return;

		if (getElement(elements.size() - 1) instanceof DuctFitting) {
			removeElement(getElement(elements.size() - 1).getName());
		}
	}

	/**
	 * Build the duct from the provided component type, component name and object
	 * name
	 * 
	 * Example: Cooling channel of the Spindle 'Example': Type: Spindle Name:
	 * Example Obj: CoolingDuct
	 * 
	 * --> Spindle_Example_CoolingDuct.xml
	 * 
	 * @param type
	 * @param name
	 * @param obj
	 * @return {@link Duct}
	 */
	public static Duct buildFromFile(String type, String name, String obj) {
		return buildFromDB(type + "_" + name + "_" + obj);
	}

	/**
	 * Build the duct from the provided name of an xml-file. If no xml-file is
	 * existing, a new duct with the given name will created.
	 * 
	 * @param path
	 * @return {@link Duct}
	 */
	public static Duct buildFromFile(String path) {
		Duct duct = initFromFile(path);

		if (null == duct) {
			return new Duct();
		} else
			return duct;
	}

	/**
	 * Build the duct from the DB provided name of an xml-file. If no xml-file is
	 * existing, a new duct with the given name will created.
	 * 
	 * @param name
	 * @return {@link Duct}
	 */
	public static Duct buildFromDB(String name) {
		Duct duct = initFromFile(getPath(name));

		if (null == duct) {
			duct = new Duct(name);
			duct.addElement(new DuctDrilling("default", .005, 1, 1));

			return duct;
		}

		duct.setName(name);
		return duct;
	}

	/**
	 * Build the duct from the provided path to an xml-file.
	 * 
	 * @param path
	 * @return {@link Duct}
	 */
	public static Duct initFromFile(String path) {
		Duct duct = null;

		File file = new File(path);
		if (!file.exists() || file.isDirectory())
			return null;

		try {
			EModFileHandling.migrateToNewVersion(path);
			JAXBContext context = JAXBContext.newInstance(Duct.class);
			Unmarshaller um = context.createUnmarshaller();
			duct = (Duct) um.unmarshal(new FileReader(path));
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

		String[] pathParts = path.split(Pattern.quote(File.separator));

		/* Link settings */
		DuctSettings settings = duct.getSettings();
		for (ADuctElement e : duct.getAllElements())
			if (e instanceof DuctBypass) {
				((DuctBypass) e).getPrimary().settings = settings;
				((DuctBypass) e).getSecondary().settings = settings;
			}

		/* Set name */
		if (pathParts.length > 0)
			duct.setName(pathParts[pathParts.length - 1]);

		return duct;
	}

	/**
	 * Create a new duct element by the NAME of the elements class. The class must
	 * be named DuctNAME and be located in
	 * {@link ch.zuestengineering.emod.model.fluid}
	 * 
	 * @param type
	 * @return {@ADuctElement};
	 */
	public static ADuctElement newDuctElement(String type) {
		Object element = null;

		// Try to create and parametrize the object
		try {
			// Get class and constructor objects
			String path = ADuctElement.class.getPackage().getName();
			Class<?> cl = Class.forName(path + ".Duct" + type);
			Constructor<?> co = cl.getConstructor(String.class);
			// initialize new component
			element = co.newInstance(type);
		} catch (Exception e) {
			logger.severe("Duct: Unable to create component " + type);
			e.printStackTrace();
			return null;
		}

		return (ADuctElement) element;
	}

	/**
	 * Create a new duct profile by the NAME of the elements class. The class must
	 * be named HPNAME and be located in ch.ethz.inspire.emod.dd.model
	 * 
	 * @param type
	 * @return
	 */
	public static AHydraulicProfile newDuctProfile(String type) {
		Object element = null;

		// Try to create and parametrize the object
		try {
			// Get class and constructor objects
			String path = ADuctElement.class.getPackage().getName();
			Class<?> cl = Class.forName(path + ".HP" + type);
			Constructor<?> co = cl.getConstructor();
			// initialize new component
			element = co.newInstance();
		} catch (Exception e) {
			logger.severe("Duct: Unable to create profile " + type);
			e.printStackTrace();
			return null;
		}

		return (AHydraulicProfile) element;
	}

	/**
	 * Generates and returns the path of the duct with the provided name in the duct
	 * DB
	 * 
	 * @param name
	 * @return path (String)
	 */
	private static String getPath(String name) {
		String prefix = PropertiesHandler.getProperty("app.DuctDBPathPrefix");
		String path = prefix + "/Duct_" + name + ".xml";

		return path;
	}

	/**
	 * Returns the path of the config file of the current instance
	 */
	private String getPath() {
		return getPath(name);
	}

	/**
	 * Saves the current instance as xml file
	 */
	public void save() {
		saveToFile(getPath());
	}

	/**
	 * Saves the current instance as xml file at the path provided
	 * 
	 * @param path
	 */
	public void saveToFile(String path) {
		removeUnusedIsolations();

		try {
			String[] pathParts = path.split(File.separator);

			if (pathParts.length > 0)
				this.setName(pathParts[pathParts.length - 1]);
		} catch (Exception e) {
		}

		try {
			JAXBContext context = JAXBContext.newInstance(Duct.class);
			Marshaller m = context.createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

			Writer w = new FileWriter(path);
			m.marshal(this, w);
			w.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Removes unused isolation elements. Unused means either the material is null,
	 * or its type is set to 'none'
	 */
	private void removeUnusedIsolations() {
		for (ADuctElement e : elements)
			if (null != e.getIsolation())
				if (e.getIsolation().isEmpty())
					e.setIsolation(null);
	}

	/**
	 * Add a new element to the duct
	 * 
	 * @param e {@link ADuctElement}
	 */
	public void addElement(ADuctElement e) {
		e.setName(getUniqueElementName(e.getName()));
		e.setMaterial(getMaterial());
		addElement(elements.size(), e);

		setMaterial();
	}

	/**
	 * Add a new element to the duct at the given index i
	 * 
	 * @param i
	 * @param e {@link ADuctElement}
	 */
	public void addElement(int i, ADuctElement e) {

		/*
		 * If the last element added has a different diameter, add a fitting
		 */
		if (elements.size() > 0 & i > 0)
			if (elements.get(i - 1).getProfileOut().getDiameter() != e.getProfileIn().getDiameter()) {
				DuctFitting df = new DuctFitting("Fitting_" + elements.get(i - 1).getName() + "-" + e.getName(),
						elements.get(i - 1).getProfileOut(), e.getProfileIn());
				elements.add(i, df);
				// df.setMaterial(getMaterial());
				i++;
			}

		e.setName(getUniqueElementName(e.getName()));
		elements.add(i, e);
		// e.setMaterial(getMaterial());

		if (elements.size() > i + 1)
			if (elements.get(i + 1).getProfileIn().getDiameter() != e.getProfileOut().getDiameter()) {
				DuctFitting df = new DuctFitting("Fitting_" + e.getName() + "-" + elements.get(i + 1).getName(),
						e.getProfileOut(), elements.get(i + 1).getProfileIn());
				elements.add(i + 1, df);
				// df.setMaterial(getMaterial());
				cleanUpFittings();
			}

		if (null == history)
			history = new Undo<Duct>(10, (new Duct()).clone(getRootDuct()));

		getHistory().add((new Duct()).clone(getRootDuct()), "app.dd.actions.add");

		if (e instanceof DuctBypass)
			((DuctBypass) e).setRootDuct(getRootDuct());
	}

	/**
	 * @return
	 */
	private Duct getRootDuct() {
		if (null == rootDuct)
			return this;
		return rootDuct;
	}

	/**
	 * Moves the element with the provided name one rank up (fittings will be
	 * skiped)
	 * 
	 * @param name
	 */
	public void moveElementUp(String name) {
		ADuctElement e = getElement(name);
		moveElementUp(e);
	}

	/**
	 * Moves the element e one rank up (fittings will be skiped)
	 * 
	 * @param e
	 */
	public void moveElementUp(ADuctElement e) {
		if (null == e)
			return;

		removeAllFittings();

		int index = elements.indexOf(e);

		if (index > 0)
			Collections.swap(this.elements, index, index - 1);

		cleanUpFittings();

		getHistory().add((new Duct()).clone(getRootDuct()), "app.dd.actions.move");
	}

	/**
	 * Moves the element e one rank up (fittings will be skiped)
	 * 
	 * @param i
	 */
	public void moveElementUp(int i) {
		ADuctElement e = getElement(i);
		moveElementUp(e);
	}

	/**
	 * Moves the element with the provided name one rank down (fittings will be
	 * skiped)
	 * 
	 * @param name
	 */
	public void moveElementDown(String name) {
		ADuctElement e = getElement(name);
		moveElementDown(e);
	}

	/**
	 * Moves the element e one rank down (fittings will be skiped)
	 * 
	 * @param e
	 */
	public void moveElementDown(ADuctElement e) {
		if (null == e)
			return;

		removeAllFittings();

		int index = elements.indexOf(e);

		if (this.elements.size() > index & index > -1)
			Collections.swap(this.elements, index, index + 1);

		cleanUpFittings();

		getHistory().add((new Duct()).clone(getRootDuct()), "app.dd.actions.move");
	}

	/**
	 * Moves the element e one rank down (fittings will be skiped)
	 * 
	 * @param i
	 */
	public void moveElementDown(int i) {
		ADuctElement e = getElement(i);
		moveElementDown(e);
	}

	/**
	 * Removes all fittings in the duct
	 */
	private void removeAllFittings() {
		for (ADuctElement e : this.elements)
			if (e instanceof DuctFitting) {
				elements.remove(e);
				removeAllFittings();
				return;
			}
	}

	/**
	 * Replaces the element with the given index by the new element e
	 * 
	 * @param i
	 * @param e
	 */
	public void replaceElement(int i, ADuctElement e) {

		elements.set(i, e);
		reconnectFittings();

		getHistory().add((new Duct()).clone(getRootDuct()), "replace " + e.getName());
	}

	/**
	 * Replace the element with the given name by the new element e
	 * 
	 * @param name
	 * @param e
	 */
	public void replaceElement(String name, ADuctElement e) {
		int i = getElementIndex(name);

		if (i >= 0)
			replaceElement(i, e);
	}

	/**
	 * Finds the index of an element with the provided name
	 * 
	 * @param name
	 * @return index
	 */
	public int getElementIndex(String name) {
		for (int i = 0; i < elements.size(); i++)
			if (elements.get(i).getName().equals(name))
				return i;

		return -1;
	}

	/**
	 * Get duct element with index i
	 * 
	 * @param i
	 * @return {@link ADuctElement}
	 */
	public ADuctElement getElement(int i) {
		return elements.get(i);
	}

	/**
	 * Get duct element with given name
	 * 
	 * @param name
	 * @return {@link ADuctElement}
	 */
	public ADuctElement getElement(String name) {
		for (ADuctElement e : getAllElements())
			if (e.getName().equals(name))
				return e;

		return null;
	}

	/**
	 * Changes the name of an element
	 * 
	 * @param name    old name
	 * @param newname new name
	 */
	public void setElementName(String name, String newname) {

		ADuctElement e = getElement(name);

		if (null == e) {
			Exception ex = new Exception("Unable to rename element " + name + " : No element with this name");
			ex.printStackTrace();
			return;
		}
		// No rename required if new and old name are the same
		else if (name.equals(newname))
			return;
		else
			e.setName(getUniqueElementName(newname));

		getHistory().add((new Duct()).clone(getRootDuct()), "app.dd.actions.editname");
	}

	/**
	 * returns a unique element name based on the provided prefix
	 * 
	 * @param name
	 * 
	 * @return prefix(_[0-9]+)?
	 */
	public String getUniqueElementName(String name) {
		String prefix = name.replaceAll("_[0-9]+$", "");

		int idx = 0;

		// Loop until name is unique
		while (null != getElement(name))
			name = prefix + "_" + (++idx);

		return name;
	}

	/**
	 * Removes the element with the given name
	 * 
	 * @param name
	 */
	public void removeElement(String name) {

		int i = getElementIndex(name);

		if (i >= 0) {
			elements.remove(i);
			cleanUpFittings();
		}

		if (getHistory() != null)
			getHistory().add((new Duct()).clone(getRootDuct()), "app.dd.actions.remove");
	}

	/**
	 * Sets the material of the ducts and all its elements
	 * 
	 * @param material
	 */
	public void setMaterial(Material material) {

		settings.setMaterial(material);
		setMaterial();

	}

	/**
	 * set common Material
	 */
	public void setMaterial() {
		for (ADuctElement e : elements)
			e.setMaterial(getMaterial());

		if (null != getMaterial())
			updateFlowRate();
	}

	/**
	 * Sets the material of the ducts and all its elements
	 * 
	 * @param material
	 */
	public void setMaterial(String material) {
		setMaterial(new Material(material));
	}

	/**
	 * Returns the current material
	 * 
	 * @return {@link Material}
	 */
	public Material getMaterial() {
		return getSettings().getMaterial();
	}

	/**
	 * Returns the current thermal resistance
	 * 
	 * @param flowRate      [m^3/s]
	 * @param pressureIn    [Pa]
	 * @param temperatureIn [K]
	 * @return [W/K]
	 */
	public double getThermalResistance(double flowRate, double pressureIn, double temperatureIn) {
		double htc;
		double Rth = 0;
		double lastp = pressureIn;
		double lastT = temperatureIn;

		for (ADuctElement e : elements) {
			htc = e.getRth(Math.abs(flowRate), lastp, lastT);
			double Sh = e.getHydraulicSurface();
			Rth += htc * Sh;
			if(!Double.isFinite(Rth))
				System.out.print("tmp");
			lastp = e.getPressureOut(flowRate, lastp, lastT);
			lastT = e.getTemperatureOut(lastT, flowRate, lastp);
		}

		return Rth;
	}

	/**
	 * Returns the Thermal resistance for the given operational condition
	 * 
	 * @param flowRate
	 * @param pressureIn
	 * @param temperatureIn
	 * @param temperatureWall
	 * @return
	 */
	public double getThermalResistance(double flowRate, double pressureIn, double temperatureIn,
			double temperatureWall) {

		if (Double.isNaN(temperatureIn))
			return 0;

		for (ADuctElement e : elements)
			e.setWallTemperature(temperatureWall);

		return getThermalResistance(flowRate, pressureIn, temperatureIn);
	}

	/**
	 * Returns the HTC for the given operational condition
	 * 
	 * @param flowRate
	 * @param pressure
	 * @param temperatureFluid
	 * @return
	 */
	public double getHTC(double flowRate, double pressure, double temperatureFluid) {
		return getThermalResistance(flowRate, pressure, temperatureFluid) / getSurface();
	}

	/**
	 * Returns the HTC for the given operational condition
	 * 
	 * @param flowRate
	 * @param pressure
	 * @param temperatureFluid
	 * @param temperatureWall
	 * @return
	 */
	public double getHTC(double flowRate, double pressure, double temperatureFluid, double temperatureWall) {

		for (ADuctElement e : elements)
			e.setWallTemperature(temperatureWall);

		return getHTC(flowRate, pressure, temperatureFluid);
	}

	/**
	 * Returns the current pressure loss
	 * 
	 * @param flowRate      [m^3/s]
	 * @param pressureIn    [Pa]
	 * @param temperatureIn [K]
	 * @return [Pa s^2/m^6]
	 */
	public double getPressureDrop(double flowRate, double pressureIn, double temperatureIn) {
		/*
		 * Simples case: no flow
		 */
		if (0 == flowRate)
			return 0;

		if (!Double.isFinite(flowRate))
			flowRate = 1E-6;

		double pressureOut = pressureIn, pressureInLast = pressureIn;

		// Global mass flow rate
		double massFlowRate = flowRate * getSettings().getMaterial().getDensity(temperatureIn, pressureIn);

		for (ADuctElement e : elements) {
			// Local flow rate
			flowRate = getSettings().getFlowRate(massFlowRate, temperatureIn, pressureIn);

			// Compute outlet conditions
			pressureIn = e.getPressureOut(flowRate, pressureInLast, temperatureIn);
			temperatureIn = e.getTemperatureOut(temperatureIn, flowRate, pressureInLast);
			pressureInLast = pressureIn;
		}

		return -(pressureIn - pressureOut);
	}

	/**
	 * Returns the current hydraulic resistance with following assumption: p(V) =
	 * k*V^2 --> k = p(V)/V²
	 * 
	 * @param flowRate         [m^3/s]
	 * @param pressureIn       [Pa]
	 * @param temperatureFluid [K]
	 * @return [Pa s^2/m^6]
	 */
	public double getPressureLossCoefficient(double flowRate, double pressureIn, double temperatureFluid) {
		if (flowRate == 0)
			return 0;

		return getPressureDrop(flowRate, pressureIn, temperatureFluid) / Math.pow(flowRate, 2);
	}

	/**
	 * Returns the derivative of the pressure loss
	 * 
	 * @param flowRate
	 * @param pressureIn
	 * @param temperatureFluid
	 * @return
	 */
	public double getPressureLossDrivative(double flowRate, double pressureIn, double temperatureFluid) {
		double fac = 1.05;
		if (flowRate == 0)
			return 0;
		return (getPressureDrop(flowRate * fac, pressureIn, temperatureFluid)
				- getPressureDrop(flowRate, pressureIn, temperatureFluid)) / (fac - 1) / flowRate;
	}

	/**
	 * Returns the outlet temperature
	 * 
	 * @param flowRate      [kg/s]
	 * @param pressureIn    [Pa]
	 * @param temperatureIn [K]
	 * @return
	 */
	public double getTemperatureOut(double flowRate, double pressureIn, double temperatureIn) {

		if (0 == flowRate)
			return temperatureIn;

		double pressureInLast = pressureIn;

		// Global mass flow rate
		double massFlowRate = flowRate * getSettings().getMaterial().getDensity(temperatureIn, pressureIn);

		for (ADuctElement e : elements) {
			// Local flow rate
			flowRate = getSettings().getFlowRate(massFlowRate, temperatureIn, pressureIn);

			// Compute outlet conditions
			pressureIn = e.getPressureOut(flowRate, pressureInLast, temperatureIn);
			temperatureIn = e.getTemperatureOut(temperatureIn, flowRate, pressureInLast);
			pressureInLast = pressureIn;
		}

		return temperatureIn;
	}

	/**
	 * Returns the total duct length
	 * 
	 * @return [m]
	 */
	public double getLength() {
		double length = 0;
		for (ADuctElement e : elements)
			length += e.getLength();

		return length;
	}

	/**
	 * Returns the total duct surface
	 * 
	 * @return [m^2]
	 */
	public double getSurface() {
		double area = 0;
		for (ADuctElement e : elements)
			area += e.getSurface();

		return area;
	}

	/**
	 * Returns the total Duct volume
	 * 
	 * @return [m^3]
	 */
	public double getVolume() {
		double vol = 0;
		for (ADuctElement e : elements)
			vol += e.getVolume();

		return vol;
	}

	/**
	 * @return List of duct elements
	 */
	public ArrayList<ADuctElement> getElements() {
		return this.elements;
	}

	/**
	 * @return List of duct elements
	 */
	public ArrayList<ADuctElement> getElementsExceptFittings() {
		ArrayList<ADuctElement> retr = new ArrayList<ADuctElement>();

		for (ADuctElement e : getElements())
			if (!(e instanceof DuctFitting))
				retr.add(e);

		return retr;
	}

	/**
	 * Inlet profile
	 * 
	 * @return
	 */
	public AHydraulicProfile getInletProfile() {
		if (elements.size() == 0)
			return null;
		else
			return getElement(0).getProfile();
	}

	/**
	 * Outlet profile
	 * 
	 * @return
	 */
	public AHydraulicProfile getOutletProfile() {
		if (elements.size() == 0)
			return null;
		else
			return getElement(getElements().size() - 1).getProfile();
	}

	/**
	 * @return List of duct element parents, including elements of bypasses, but
	 *         without fittings
	 */
	public ArrayList<ADuctElement> getAllElements() {
		ArrayList<ADuctElement> elements = new ArrayList<ADuctElement>();

		for (ADuctElement e : getElements()) {
			if (!(e instanceof DuctFitting))
				elements.add(e);
			if (e instanceof DuctBypass) {
				elements.addAll(((DuctBypass) e).getPrimary().getAllElements());
				if (((DuctBypass) e).getPrimary().getElements().size() == 0)
					elements.add(new DuctPipe(e.getName() + "_Branch1"));

				elements.addAll(((DuctBypass) e).getSecondary().getAllElements());
				if (((DuctBypass) e).getSecondary().getElements().size() == 0)
					elements.add(new DuctPipe(e.getName() + "_Branch2"));
			}
		}

		return elements;
	}

	/**
	 * Clear the duct (removes all elements)
	 */
	public void clear() {
		for (int i = elements.size() - 1; i >= 0; i--)
			elements.remove(i);

		if (null != history)
			history.clear((new Duct()).clone(getRootDuct()));
	}

	/**
	 * Clones the duct (create a clone of the elements list)
	 * 
	 * @param duct
	 * @return
	 */
	public Duct clone(Duct duct) {
		this.elements = new ArrayList<ADuctElement>();

		for (ADuctElement e : duct.getElements())
			this.elements.add(e.clone());

		cleanUpFittings();

		this.settings.clone(duct.settings);

		return this;
	}

	/**
	 * Clones the duct (create a clone of the elements list)
	 * 
	 * @return
	 */
	@Override
	public Duct clone() {

		Duct clone = new Duct(getName());
		clone.getSettings().clone(getSettings());

		for (ADuctElement e : this.getElements())
			clone.addElement(e.clone());

		clone.cleanUpFittings();

		return clone;
	}

	/**
	 * Returns the history of the duct editting
	 * 
	 * @return
	 */
	public Undo<Duct> getHistory() {
		return getRootDuct().history;
	}

	/**
	 * Undo the last edit
	 */
	public void undo() {
		clone(this.history.undo());

		this.setRootDuct();
	}

	/**
	 * Undo the last edit
	 */
	public void redo() {
		clone(this.history.redo());

		this.setRootDuct();
	}

	/**
	 * Returns the name of the duct
	 * 
	 * @return
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @param rootDuct
	 */
	@XmlTransient
	public void setRootDuct(Duct rootDuct) {
		if (null == rootDuct)
			return;

		this.rootDuct = rootDuct;

		for (ADuctElement e : elements)
			if (e instanceof DuctBypass)
				((DuctBypass) e).setRootDuct(rootDuct);
	}

	/**
	 * 
	 */
	public void setRootDuct() {
		this.setRootDuct(getRootDuct());
	}

	/**
	 * Computes the flow rate [kg/s] possible with the given inlet and outlet
	 * conditions
	 * 
	 * @param inletPressure
	 * @param outletPressure
	 * @param inletTemperature
	 * @return
	 */
	public double getMassFlowRate(double inletPressure, double outletPressure, double inletTemperature) {
		double flowRate = getSettings().getFlowRate();

		if (!Double.isFinite(inletPressure))
			inletPressure = outletPressure;

		if (!Double.isFinite(flowRate))
			flowRate = .002;

		double targetPressureLoss = inletPressure - outletPressure;
		double currentPressureLoss;

		for (int i = 0; i < 100; i++) {
			currentPressureLoss = getPressureDrop(flowRate, inletPressure, inletTemperature);

			// Quit if target pressure loss is reached
			if (Math.abs(currentPressureLoss / targetPressureLoss - 1) < 1E-4)
				break;

			double zeta = currentPressureLoss / flowRate / flowRate;
			flowRate = Math.sqrt(targetPressureLoss / zeta);

		}

		return flowRate * getSettings().getMaterial().getDensity(inletTemperature, inletPressure);
	}

	/**
	 * finds the inlet pressure statisfying the given inlet and outlet conditions
	 * 
	 * @param flowRate
	 * @param inletTemperature
	 * @param outletPressure
	 * @return
	 */
	public double getInletPressure(double flowRate, double inletTemperature, double outletPressure) {
		double inletPressure = 1E5 + outletPressure;

		double lastInletPressure = 1E5 + outletPressure;

		for (int i = 0; i < 100; i++) {
			inletPressure = outletPressure + getPressureDrop(flowRate, lastInletPressure, inletTemperature);

			// Quit if target pressure loss is reached
			if (Math.abs(lastInletPressure / inletPressure - 1) < 1E-4)
				break;

			lastInletPressure = inletPressure;

		}

		return inletPressure;
	}

	/**
	 * Performs a swap of the elements with the given indexes
	 * 
	 * @param idx1
	 * @param idx2
	 * @return
	 */
	public boolean swap(int idx1, int idx2) {
		try {
			if (idx1 >= 0 & idx2 >= 0 & idx1 < elements.size() & idx2 < elements.size()) {
				Collections.swap(elements, idx1, idx2);
				return true;
			}
		} catch (Exception e) {
		}

		return false;
	}

	/**
	 * @param idxOld
	 * @param idxNew
	 * @return
	 */
	public boolean move(int idxOld, int idxNew) {
		if (idxOld >= 0 & idxNew >= 0 & idxOld < elements.size() & idxNew < elements.size()) {

			if (idxOld < idxNew) {
				moveElementDown(idxOld);
				return move(idxOld + 1, idxNew);
			} else if (idxOld > idxNew) {
				moveElementUp(idxOld);
				return move(idxOld - 1, idxNew);
			}
		}

		return false;
	}

	/**
	 * Moves an element to a new duct at the given index
	 * 
	 * @param duct
	 * @param element
	 * @param idxNew
	 */
	public void moveElement(Duct duct, ADuctElement element, int idxNew) {
		elements.remove(element);
		duct.addElement(idxNew, element);
	}

	/**
	 * Sets the same profile for all elements
	 * 
	 * @param profile
	 */
	public void setProfile(AHydraulicProfile profile) {
		for (ADuctElement e : getElementsExceptFittings())
			if (e instanceof DuctBypass) {
				((DuctBypass) e).getPrimary().setProfile(profile);
				((DuctBypass) e).getSecondary().setProfile(profile);
			} else
				e.setProfile(profile.clone());
	}

	/**
	 * Returns a sorted list of all available profiles
	 * 
	 * @return
	 */
	public static AHydraulicProfile[] getAvailableProfiles() {
		ArrayList<AHydraulicProfile> list = new ArrayList<>();
		list.add(new HPCircular());
		list.add(new HPRectangular());
		list.add(new HPAnnulus());

		AHydraulicProfile[] retList = list.toArray(new AHydraulicProfile[1]);

		Arrays.sort(retList, new Comparator<AHydraulicProfile>() {
			@Override
			public int compare(AHydraulicProfile e1, AHydraulicProfile e2) {
				String n1, n2;
				n1 = e1.getClass().getSimpleName().replace("HP", "");
				n2 = e2.getClass().getSimpleName().replace("HP", "");

				return n1.compareTo(n2);
			}
		});

		return retList;

	}

	/**
	 * Returns a List of all possible duct elements, without fittings
	 * 
	 * @return
	 */
	public static ADuctElement[] getAvailableElements() {
		return getAvailableElements(false);
	}

	/**
	 * Returns a list of all possible duct elements, the argument indicates whther
	 * or not to include fittings
	 * 
	 * @param includeFittings
	 * @return
	 */
	public static ADuctElement[] getAvailableElements(boolean includeFittings) {

		ArrayList<ADuctElement> list = new ArrayList<>();
		list.add(new DuctDrilling());
		list.add(new DuctFlowAround());
		list.add(new DuctHelix());
		list.add(new DuctStaggeredHelix());
		list.add(new DuctPipe());
		list.add(new DuctElbowFitting());
		list.add(new DuctArc());
		list.add(new DuctPartialFlowAround());
		list.add(new DuctDefinedValues());
		list.add(new DuctDefinedValues2());
		list.add(new DuctBypass());
		list.add(new DuctWireTurbulator());
		list.add(new DuctExtension());
		list.add(new DuctReduction());

		if (includeFittings) {
			list.add(new DuctFitting());
		}

		ADuctElement[] retList = list.toArray(new ADuctElement[1]);

		Arrays.sort(retList, new Comparator<ADuctElement>() {
			@Override
			public int compare(ADuctElement e1, ADuctElement e2) {
				String n1, n2;
				n1 = e1.getClass().getSimpleName().replace("Duct", "");
				n2 = e2.getClass().getSimpleName().replace("Duct", "");

				return n1.compareTo(n2);
			}
		});

		return retList;
	}
}
