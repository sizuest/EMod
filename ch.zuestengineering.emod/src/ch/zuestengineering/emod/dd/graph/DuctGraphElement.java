/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.dd.graph;

import java.awt.Color;
import java.awt.Font;

import org.piccolo2d.extras.nodes.PComposite;
import org.piccolo2d.extras.swt.PSWTPath;
import org.piccolo2d.extras.swt.PSWTText;
import org.piccolo2d.util.PBounds;

import ch.zuestengineering.emod.dd.model.ADuctElement;
import ch.zuestengineering.emod.gui.modelling.GraphDecorationUtils;

/**
 * Representation of a ADuctElement in the graphical editor
 * 
 * @author simon
 *
 */
public class DuctGraphElement extends PComposite {

	private static final long serialVersionUID = 1L;

	/* Element to be represented */
	protected ADuctElement ductElement;

	/* Element name */
	protected PSWTText textName, textParams;
	/* Surrounding box, isolation */
	protected PSWTPath box, isolation, bc;
	protected PComposite gloom;
	/* Representation of the element */
	protected PComposite element;

	/* Input / Output */
	protected DuctGraphElementIO input, output;

	protected DuctGraph parent;

	protected boolean isSelected = false;

	/**
	 * Create a new graphical representation of the stated duct element
	 * 
	 * @param ductElement
	 * @param parent
	 */
	public DuctGraphElement(ADuctElement ductElement, DuctGraph parent) {
		this.ductElement = ductElement;
		this.parent = parent;

		input = new DuctGraphElementIO();
		output = new DuctGraphElementIO();

		textParams = new PSWTText("", new Font("Arial", 0, 6));

		element = new PComposite();

		textName = new PSWTText(ductElement.getName());
		element.addChild(textName);
		textName.setOffset(-textName.getBounds().getWidth() / 2, 0);
		element.setBounds(-75, 0, 150, textName.getHeight());

		box = PSWTPath.createRoundRectangle((int) element.getFullBounds().getX() - 10,
				(int) element.getFullBounds().getY() - 10, (int) element.getBounds().getWidth() + 20,
				(int) element.getBounds().getHeight() + 20, 5, 5);

		isolation = PSWTPath.createRoundRectangle((int) element.getFullBounds().getX() - 20,
				(int) element.getFullBounds().getY() - 10, (int) element.getBounds().getWidth() + 40,
				(int) element.getBounds().getHeight() + 20, 5, 5);
		isolation.setPaint(Color.BLUE);
		isolation.setStrokeColor(Color.BLUE);

		bc = PSWTPath.createRoundRectangle((int) element.getFullBounds().getX() - 20,
				(int) element.getFullBounds().getY() - 10, (int) element.getBounds().getWidth() + 40,
				(int) element.getBounds().getHeight() + 20, 5, 5);
		bc.setPaint(Color.RED);

		this.setBounds(box.getFullBounds());

		this.addChild(bc);
		this.addChild(isolation);
		this.addChild(box);
		this.addChild(input);
		this.addChild(output);
		this.addChild(textParams);
		this.addChild(element);
		update();

		textParams.setGreekThreshold(0);
		textName.setGreekThreshold(0);

		setSelected(false);
	}

	/**
	 * Updates all text and sizes
	 */
	public void update() {
		// Box
		box.setPathToRoundRectangle((int) element.getBounds().getX() - 10, (int) element.getBounds().getY() - 10,
				(int) element.getBounds().getWidth() + 20, (int) element.getBounds().getHeight() + 20, 5, 5);
		this.setBounds(box.getBounds());

		// Boundary condition
		if (ductElement.hasBoundaryCondition()) {
			int offset = 0;
			if (ductElement.hasIsolation())
				offset = 10;

			bc.setPathToRoundRectangle((int) element.getBounds().getX() - 20 - 1 * offset,
					(int) element.getBounds().getY() - 10, (int) element.getBounds().getWidth() + 40 + 2 * offset,
					(int) element.getBounds().getHeight() + 20, 5, 5);

			setBCColor();
		} else
			bc.setPathToRoundRectangle((int) element.getBounds().getX() - 10, (int) element.getBounds().getY() - 10,
					(int) element.getBounds().getWidth() + 20, (int) element.getBounds().getHeight() + 20, 5, 5);

		// Isolation
		if (ductElement.hasIsolation())
			isolation.setPathToRoundRectangle((int) element.getBounds().getX() - 20,
					(int) element.getBounds().getY() - 10, (int) element.getBounds().getWidth() + 40,
					(int) element.getBounds().getHeight() + 20, 5, 5);
		else
			isolation.setPathToRoundRectangle((int) element.getBounds().getX() - 10,
					(int) element.getBounds().getY() - 10, (int) element.getBounds().getWidth() + 20,
					(int) element.getBounds().getHeight() + 20, 5, 5);

		// IO
		input.setOffset(box.getCenter().getX() - input.getWidth() / 2,
				box.getBounds().getMinY() - input.getHeight() - 2);
		output.setOffset(box.getCenter().getX() - output.getWidth() / 2, box.getBounds().getMaxY());

		intermediateUpdate();

		PBounds p = this.getBounds();
		if (p.getHeight() < textParams.getBounds().getHeight()) {
			p.height = textParams.getBounds().getHeight();
			p.y = textParams.getGlobalBounds().y;
			this.setBounds(p);
		}
	}

	private void setBCColor() {
		if (ductElement.getAdiabatic()) {
			bc.setPaint(Color.GRAY);
			bc.setStrokeColor(Color.GRAY);
		} else if (ductElement.hasHeatSource()) {
			bc.setPaint(Color.RED);
			bc.setStrokeColor(Color.RED);
		} else {
			bc.setPaint(Color.ORANGE);
			bc.setStrokeColor(Color.ORANGE);
		}
	}

	/**
	 * @return
	 */
	public DuctGraphElementIO getInput() {
		return input;
	}

	/**
	 * @return
	 */
	public DuctGraphElementIO getOutput() {
		return output;
	}

	/**
	 * @return
	 */
	public ADuctElement getElement() {
		return this.ductElement;
	}

	/**
	 * @return
	 */
	public DuctGraph getDuctGraph() {
		return this.parent;
	}

	/**
	 * Remove the element from its parent
	 */
	@Override
	public void removeFromParent() {
		/*
		 * We have to do some additional taks: Remove the element from the duct and
		 * redraw
		 */
		parent.getDuct().removeElement(this.ductElement.getName());
		parent.redraw();
		parent.updateAll();
		super.removeFromParent();
	}

	/**
	 * Mark the node as selected or unselected
	 * 
	 * @param b
	 */
	public void setSelected(boolean b) {
		isSelected = b;

		if (b) {
			Color col = getBaseColorDark();
			box.setPaint(col);
			textName.setBackgroundColor(col);
			gloom = GraphDecorationUtils.addGloomToRectangle(this, bc, col);
		} else {
			Color col = getBaseColor();
			box.setPaint(col);
			textName.setBackgroundColor(col);
			if (null != gloom) {
				gloom.removeFromParent();
			}
		}
	}

	/**
	 * Updates the color of the element
	 */
	public void updateColor() {
		setSelected(getSelected());
	}

	/**
	 * @return
	 */
	public boolean getSelected() {
		return isSelected;
	}

	/**
	 * 
	 */
	public void intermediateUpdate() {
		// Name
		textName.setText(ductElement.getName());
		textName.setOffset(-textName.getBounds().getWidth() / 2, 0);

		// Params
		String paramsText = ductElement.getModelType() + "\n" + ductElement.getProfile().toString();
		if (ductElement.hasIsolation())
			paramsText += "\n" + ductElement.getIsolation().toString();
		for (String pname : ductElement.getParameterSet().getNames())
			try {
				paramsText += "\n" + pname + "=" + ductElement.getParameterSet().getPhysicalValue(pname).toString();
			} catch (Exception e) {
				e.printStackTrace();
			}
		textParams.setText(paramsText);
		textParams.setOffset(20 + box.getBounds().getMaxX(),
				box.getBounds().getCenterY() - textParams.getBounds().getCenterY());
	}

	/**
	 * @return
	 */
	public Color getBaseColor() {
		return parent.getBaseColor();
	}

	/**
	 * @return
	 */
	public Color getBaseColorDark() {
		Color color = getBaseColor();

		return color.darker();
	}

	/**
	 * Adjusts the color of the two textfields outside of the box
	 * 
	 * @param col
	 */
	public void setBackgroundColor(Color col) {
		textParams.setBackgroundColor(col);
	}

}
