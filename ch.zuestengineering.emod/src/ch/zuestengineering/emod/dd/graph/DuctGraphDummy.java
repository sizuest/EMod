/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.dd.graph;

import java.awt.Color;

import ch.zuestengineering.emod.dd.model.DuctDrilling;

/**
 * @author sizuest
 *
 */
public class DuctGraphDummy extends DuctGraphElement {

	private static final long serialVersionUID = 1L;

	/**
	 * @param parent
	 */
	public DuctGraphDummy(DuctGraph parent) {
		super(new DuctDrilling("Dummy"), parent);

		textName.setText("...");
		textName.setOffset(-textName.getBounds().getWidth() / 2, 0);
		textName.setPaint(new Color(230, 230, 230));
		box.setPathToRectangle((int) element.getBounds().getX() - 10, (int) element.getBounds().getY() - 10,
				(int) element.getBounds().getWidth() + 20, (int) element.getBounds().getHeight() + 20);
		box.setPaint(new Color(230, 230, 230));
		box.setStrokeColor(new Color(230, 230, 230));

		textParams.removeFromParent();

		input.setVisible(false);
		output.setVisible(false);

		update();

	}

	@Override
	public void update() {
		super.update();
		box.setPathToRectangle((int) element.getBounds().getX() - 10, (int) element.getBounds().getY() - 10,
				(int) element.getBounds().getWidth() + 20, (int) element.getBounds().getHeight() + 20);
	}

	@Override
	public void intermediateUpdate() {
		super.intermediateUpdate();
		textParams.setText("");
		textName.setText("...");
		textName.setOffset(-textName.getBounds().getWidth() / 2, 0);
	}

	@Override
	public void updateColor() {
	}

}
