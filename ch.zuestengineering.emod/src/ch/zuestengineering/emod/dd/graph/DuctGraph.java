/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.dd.graph;

import java.awt.Color;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Collections;

import org.piccolo2d.PNode;
import org.piccolo2d.extras.nodes.PComposite;
import org.piccolo2d.util.PBounds;

import ch.zuestengineering.emod.dd.Duct;
import ch.zuestengineering.emod.dd.model.ADuctElement;
import ch.zuestengineering.emod.dd.model.DuctBypass;

/**
 * Representation of a Duct in the graphical editor
 * 
 * @author simon
 *
 */
public class DuctGraph extends PComposite {

	private static final long serialVersionUID = 1L;

	/* Duct to be represented */
	protected Duct duct;
	/* Main graph */
	protected DuctGraph parentGraph;

	protected DuctGraph parent;

	protected ArrayList<DuctGraphElement> elements = new ArrayList<DuctGraphElement>();

	/* ClipBoard */
	private ADuctElement clipboard = null;

	/**
	 * @param duct
	 * @param parentGraph
	 */
	public DuctGraph(Duct duct, DuctGraph parentGraph) {
		this.duct = duct;
		this.parentGraph = parentGraph;

		redraw();
	}

	/**
	 * Get the main graph
	 * 
	 * @return
	 */
	public DuctGraph getMainGraph() {
		if (null != parentGraph)
			return parentGraph;
		else
			return this;
	}

	/**
	 * Updates all the colors
	 */
	public void updateAllColors() {
		updateAllColors(getMainGraph());
	}

	/**
	 * Updates all the colors
	 */
	public void updateColors() {
		updateAllColors(this);
	}

	private void updateAllColors(DuctGraph g) {
		for (int i = 0; i < g.getChildrenCount(); i++) {
			if (g.getChild(i) instanceof DuctGraphElement)
				((DuctGraphElement) g.getChild(i)).updateColor();

		}
	}

	/**
	 * Updates graph starting from the source graph
	 */
	public void updateAll() {
		if (null != parentGraph)
			parentGraph.update();
		else
			update();

		updateAllColors();
	}

	/**
	 * Updates all elements
	 */
	public void update() {

		// Reposition elements
		int lastPos = 0;

		for (DuctGraphElement e : elements) {
			// e.update();
			e.intermediateUpdate();
			if (!e.getSelected()) {
				e.setOffset(-e.getBounds().getCenterX(), -(e.getBounds().getMinY() - lastPos));
			}
			lastPos += e.getBounds().getHeight();
		}

		// Update bounds
		PBounds ductBounds = new PBounds(0, 0, 0, 0);

		for (int i = 0; i < this.getChildrenCount(); i++) {
			if (this.getChild(i) instanceof DuctGraphElement)
				if (((DuctGraphElement) this.getChild(i)).isSelected)
					continue;
			ductBounds.add(this.getChild(i).getFullBounds());
		}
		this.setBounds(ductBounds);

		updateColors();
	}

	/**
	 * Redraws all elements
	 */
	public void redraw() {

		this.removeAllChildren();

		// Add elements
		for (ADuctElement e : duct.getElementsExceptFittings()) {
			DuctGraphElement ge;

			if (e instanceof DuctBypass)
				ge = new DuctGraphBypass(e, this);
			else
				ge = new DuctGraphElement(e, this);
			elements.add(ge);
			this.addChild(ge);
		}

		if (elements.size() == 0) {
			this.addChild(new DuctGraphDummy(this));
		}

		// Update the positions
		update();

	}

	/**
	 * @param duct
	 */
	public void redraw(Duct duct) {
		this.duct = duct;
		redraw();
	}

	@Override
	public void removeAllChildren() {
		super.removeAllChildren();
		elements = new ArrayList<DuctGraphElement>();
	}

	/**
	 * Returns the input object
	 * 
	 * @return
	 */
	public DuctGraphElementIO getInput() {
		return elements.get(0).getInput();
	}

	/**
	 * Returns the output object
	 * 
	 * @return
	 */
	public DuctGraphElementIO getOutput() {
		return elements.get(elements.size() - 1).getOutput();
	}

	/**
	 * Moves an element to the hovered position
	 * 
	 * @param element
	 * @param position
	 */
	public void moveElement(DuctGraphElement element, Point2D position) {

		// We won't handle dummy elements
		if (element instanceof DuctGraphDummy)
			return;

		// Find new parent graph
		DuctGraph dg = getMainGraph().getDuctGraph(position, element);

		// Find new element index
		int idxNew;
		if (dg.getElements().size() == 0)
			idxNew = 0;
		else
			idxNew = Math.min(dg.getElementIndex(element), dg.getElements().size() - 1);

		// If the element is a Bypass, we have to double check if it is not added to
		// itself!
		if (element instanceof DuctGraphBypass) {
			DuctGraph tmp = dg;
			while (!tmp.equals(getMainGraph())) {
				if (tmp.equals(((DuctGraphBypass) element).getDuctGraphPrimary())
						| tmp.equals(((DuctGraphBypass) element).getDuctGraphScondary()))
					return;

				// Move one graph up
				try {
					tmp = (DuctGraph) dg.getParent().getParent();
				} catch (Exception e) {
					tmp = getMainGraph();
				}
			}
		}

		// Test, if duct stays the same
		if (element.getDuctGraph().equals(dg)) {
			int idxOld = elements.indexOf(element);

			while (idxOld < idxNew) {
				dg.getDuct().moveElementDown(element.getElement());
				Collections.swap(elements, idxOld, ++idxOld);
			}

			while (idxOld > idxNew) {
				dg.getDuct().moveElementUp(element.getElement());
				Collections.swap(elements, idxOld, --idxOld);
			}

		}
		// If not, move the element to the other duct
		else {
			element.getDuctGraph().duct.moveElement(dg.duct, element.getElement(), idxNew);
			moveElement(dg, element, idxNew);
		}
	}

	/**
	 * Moves an element to a new duct graph at the given index
	 * 
	 * @param dg
	 * @param element
	 * @param idxNew
	 */
	public void moveElement(DuctGraph dg, DuctGraphElement element, int idxNew) {
		dg.elements.add(idxNew, element);
		element.parent = dg;
		elements.remove(element);
	}

	/**
	 * Returns the list of all elements
	 * 
	 * @return
	 */
	public ArrayList<DuctGraphElement> getElements() {
		return elements;
	}

	/**
	 * Returns the list of all elements including the ones in the bypasses
	 * 
	 * @return
	 */
	public ArrayList<DuctGraphElement> getAllElements() {
		ArrayList<DuctGraphElement> out = new ArrayList<>();

		for (DuctGraphElement e : getElements()) {
			out.add(e);
			if (e instanceof DuctGraphBypass) {
				out.addAll(((DuctGraphBypass) e).getElements());
			}
		}

		return out;
	}

	/**
	 * Returns the index of the hovered element in the current duct
	 * 
	 * @param position
	 * @return
	 */
	public int getElementIndex(Point2D position) {

		if (elements.size() == 0)
			return 0;

		for (int i = 0; i < elements.size(); i++) {
			if (elements.get(i).getGlobalBounds().getCenterY() > position.getY()) {
				return i;
			}
		}

		return elements.size();
	}

	/**
	 * Returns the index of the hovered element in the current duct
	 * 
	 * @param element
	 * @return
	 */
	public int getElementIndex(DuctGraphElement element) {

		if (elements.size() == 0)
			return 0;

		if (elements.get(0).getGlobalBounds().getCenterY() > element.getGlobalBounds().getCenterY()) {
			return 0;
		}

		for (int i = 1; i < elements.size(); i++) {
			if (elements.get(i).getGlobalBounds().getCenterY() > element.getGlobalBounds().getCenterY()) {
				if (elements.get(i - 1).equals(element))
					return i - 1;
				else
					return i;
			}
		}

		return elements.size();
	}

	/**
	 * @param position
	 * @return
	 */
	public DuctGraphElement getSelection(Point2D position) {
		DuctGraphElement ret = null;

		for (DuctGraphElement e : elements) {
			if (e.getGlobalBounds().contains(position))
				if (e instanceof DuctGraphBypass)
					ret = ((DuctGraphBypass) e).getSelection(position);
				else
					ret = e;

			if (null != ret)
				break;
		}

		return ret;
	}

	/**
	 * @return
	 * 
	 */
	public Duct getDuct() {
		return duct;
	}

	/**
	 * @param p
	 * @param source
	 * @return
	 */
	public DuctGraph getDuctGraph(Point2D p, DuctGraphElement source) {

		ArrayList<DuctGraphElement> elements;

		elements = this.elements;

		for (DuctGraphElement e : elements) {
			if (e.equals(source))
				break;
			if (e instanceof DuctGraphBypass)
				if (e.getGlobalBounds().contains(p)) {
					DuctGraph dg = ((DuctGraphBypass) e).getDuctGraph(p);
					if (this.equals(dg))
						return this;
					else
						return dg.getDuctGraph(p, source);
				}
		}

		return this;
	}

	/**
	 * @param p
	 * @return
	 */
	public DuctGraph getDuctGraph(Point2D p) {
		return getDuctGraph(p, null);
	}

	/**
	 * Performs the cut operation: Selection is moved to the clipboard, GraphElement
	 * is removed
	 * 
	 * @param selection
	 */
	public void cut(DuctGraphElement selection) {
		if (selection == null)
			return;

		clipboard = selection.getElement().clone();
		selection.removeFromParent();
	}

	/**
	 * Performs the copy operation: Selection is copied to the clipboard
	 * 
	 * @param selection
	 */
	public void copy(DuctGraphElement selection) {
		clipboard = selection.getElement().clone();
	}

	/**
	 * Performs the past operation: Content of the clipboard is past at the given
	 * location
	 * 
	 * @param point
	 * @return
	 */
	public ADuctElement past(Point2D point) {
		ADuctElement clipboard = this.clipboard;

		if (null != clipboard) {
			addElement(point, clipboard);
			this.clipboard = clipboard.clone();
		}

		return clipboard;
	}

	/**
	 * Adds a new element to the duct
	 * 
	 * @param p
	 * @param e
	 */
	public void addElement(Point2D p, ADuctElement e) {
		DuctGraph ductGraph = getMainGraph().getDuctGraph(p);
		int index = getMainGraph().getElementIndex(p);

		if (index < 0 | null == ductGraph)
			return;

		addElement(index, e, ductGraph.getDuct());
	}

	/**
	 * Adds a new element to the duct
	 * 
	 * @param index
	 * @param e
	 * @param duct
	 */
	private void addElement(int index, ADuctElement e, Duct duct) {

		if (duct.getElementsExceptFittings().size() > index)
			duct.addElement(index, e);
		else
			duct.addElement(e);

		redraw();
	}

	/**
	 * @return
	 */
	public int getBaseColorIndex() {
		int i = getBaseColorIndex(this);
		return i;
	}

	private int getBaseColorIndex(PNode p) {
		if (null == p)
			return 0;
		if (p.equals(getMainGraph()))
			return 0;
		if (p instanceof DuctGraph)
			return 1 + getBaseColorIndex(p.getParent());
		else
			return 0 + getBaseColorIndex(p.getParent());
	}

	/**
	 * @return
	 */
	public Color getBaseColor() {
		Color[] colors = { new Color(217, 226, 243), new Color(251, 228, 213), new Color(255, 242, 204),
				new Color(226, 239, 217) };

		return colors[getBaseColorIndex() % colors.length];
	}

	/**
	 * Set the background color of all members
	 * 
	 * @param col
	 */
	public void setBackgroundColor(Color col) {
		for (DuctGraphElement e : elements)
			e.setBackgroundColor(col);
	}

}
