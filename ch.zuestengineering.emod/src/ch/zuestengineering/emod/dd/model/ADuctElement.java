/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.dd.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import ch.zuestengineering.emod.model.fluid.Fluid;
import ch.zuestengineering.emod.model.material.Material;
import ch.zuestengineering.emod.model.parameters.Parameterizable;

/**
 * Implements the abstract class for a hydraulic duct element
 * 
 * @author simon
 * 
 */
@XmlRootElement
public abstract class ADuctElement implements Parameterizable, Cloneable, Comparable<ADuctElement> {

	protected String name;
	@XmlTransient
	protected Material material;
	protected AHydraulicProfile profile = new HPCircular(.01);
	protected double length = .1;
	protected Isolation isolation = null;
	@XmlElement
	protected double heatSource = Double.NaN;
	@XmlElement
	protected double wallTemperature = Double.NaN;
	@XmlElement
	protected boolean isAdiabatic = false;

	/**
	 * Sets the current fluid material
	 * 
	 * @param material {@link Material}
	 */
	@XmlTransient
	public void setMaterial(Material material) {
		this.material = material;
	}

	/**
	 * Returns the current material.
	 * 
	 * @return {@link Material}
	 */
	public Material getMaterial() {
		return this.material;
	}

	/**
	 * Returns the element name
	 * 
	 * @return element name
	 */
	public String getName() {
		if (null == this.name)
			return this.getClass().getSimpleName();

		return this.name;
	}

	/**
	 * Sets the element name
	 * 
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Returns the (hydraulic) diameter
	 * 
	 * @return [m]
	 */
	public double getDiameter() {
		return this.profile.getDiameter();
	}

	/**
	 * Returns the length
	 * 
	 * @return [m]
	 */
	public double getLength() {
		return this.length;
	}

	/**
	 * Returns the free surface
	 * 
	 * @return [m²]
	 */
	public double getSurface() {
		return this.length * profile.getPerimeter();
	}

	/**
	 * Returns the hydraulic surface
	 * 
	 * @return [m²]
	 */
	public double getHydraulicSurface() {
		return this.length * Fluid.hydraulicPerimeter(this.profile);
	}

	/**
	 * Returns the total volume
	 * 
	 * @return [m³]
	 */
	public double getVolume() {
		return this.length * profile.getArea();
	}

	/**
	 * Sets the isolation
	 * 
	 * @param isolation {@link Isolation}
	 */
	public void setIsolation(Isolation isolation) {
		this.isolation = isolation;
	}

	/**
	 * Returns the outlet pressure for the given operational condition
	 * 
	 * @param flowRate         [m�/s]
	 * @param pressureIn       [Pa]
	 * @param temperatureFluid [K]
	 * @return [Pa]
	 */
	public double getPressureOut(double flowRate, double pressureIn, double temperatureFluid) {
		return pressureIn - Math.signum(flowRate) * getPressureDrop(Math.abs(flowRate), pressureIn, temperatureFluid);
	}

	/**
	 * Returns the heat transfer coefficient for the given operational condition
	 * 
	 * @param flowRate         [m�/s]
	 * @param pressure         [Pa]
	 * @param temperatureFluid [K]
	 * @param temperatureWall  [K]
	 * @return [W/K/m²]
	 */
	protected abstract double getHTC(double flowRate, double pressure, double temperatureFluid, double temperatureWall);

	/**
	 * Returns the current HTC
	 * 
	 * @param flowRate
	 * @param pressure
	 * @param temperatureFluid
	 * @return
	 */
	public double getHTC(double flowRate, double pressure, double temperatureFluid) {
		return getHTC(flowRate, pressure, temperatureFluid,
				this.getWallTemperature(temperatureFluid, flowRate, pressure));
	}

	/**
	 * Returns the total heat transfer coefficient for the given operational
	 * condition
	 * 
	 * @param flowRate         [m³/s]
	 * @param pressure         [Pa]
	 * @param temperatureFluid [K]
	 * @param temperatureWall  [K]
	 * @return [W/K/m²]
	 */
	protected double getRth(double flowRate, double pressure, double temperatureFluid, double temperatureWall) {
		double Rth = getHTC(flowRate, pressure, temperatureFluid, temperatureWall);

		if (getAdiabatic())
			return 0;

		if (hasIsolation())
			Rth = 1 / (1 / Rth + 1 / getIsolation().getThermalResistance(getProfile()));

		return Rth;
	}

	/**
	 * Returns whether the element is configured as adiabatic
	 * 
	 * @return
	 */
	public boolean getAdiabatic() {
		return isAdiabatic;
	}

	/**
	 * Sets the adiabatic status of the element
	 * 
	 * @param a
	 */
	public void setAdiabatic(boolean a) {
		this.isAdiabatic = a;

		if (isAdiabatic) {
			this.heatSource = Double.NaN;
			this.wallTemperature = Double.NaN;
		}
	}

	/**
	 * Returns the current thermal resistance
	 * 
	 * @param flowRate
	 * @param pressure
	 * @param temperatureFluid
	 * @return
	 */
	public double getRth(double flowRate, double pressure, double temperatureFluid) {
		return getRth(flowRate, pressure, temperatureFluid,
				this.getWallTemperature(temperatureFluid, flowRate, pressure));
	}

	/**
	 * Returns the heat pressure drop for the given operational condition
	 * 
	 * @param flowRate         [m³/s]
	 * @param pressure         [Pa]
	 * @param temperatureFluid [K]
	 * @return [Pa]
	 */
	public abstract double getPressureDrop(double flowRate, double pressure, double temperatureFluid);

	/**
	 * Returns the heat pressure drop for the given operational condition
	 * 
	 * @param flowRate         [m3/s]
	 * @param pressure         [Pa]
	 * @param temperatureFluid [K]
	 * @return [-]
	 */
	public double getPressureLossCoefficient(double flowRate, double pressure, double temperatureFluid) {
		if (0 == flowRate | getMaterial() == null)
			return Double.NaN;

		return 2 * getPressureDrop(flowRate, pressure, temperatureFluid) * Math.pow(getProfile().getArea(), 2)
				/ Math.pow(flowRate, 2) * Math.signum(flowRate) / getMaterial().getDensity(temperatureFluid, pressure);
	}

	/**
	 * Returns the hydraulic profile object
	 * 
	 * @return {@link AHydraulicProfile}
	 */
	public AHydraulicProfile getProfile() {
		return this.profile;
	}

	/**
	 * Returns the hydraulic profile object
	 * 
	 * @return {@link AHydraulicProfile}
	 */
	public AHydraulicProfile getProfileIn() {
		return this.profile;
	}

	/**
	 * Returns the hydraulic profile object
	 * 
	 * @return {@link AHydraulicProfile}
	 */
	public AHydraulicProfile getProfileOut() {
		return this.profile;
	}

	/**
	 * Returns whether the duct has a isolation or not
	 * 
	 * @return
	 */
	public boolean hasIsolation() {
		if (this.isolation == null)
			return false;
		if (this.isolation.isEmpty())
			return false;
		else
			return true;
	}

	/**
	 * Returns the wall temperature
	 * 
	 * @param temperatureIn
	 * @param flowRate
	 * @param pressure
	 * @return
	 */
	public double getWallTemperature(double temperatureIn, double flowRate, double pressure) {
		double T = Double.NaN;

		// Simplest case: No heat transfer
		if (Double.isNaN(this.heatSource) & Double.isNaN(this.wallTemperature))
			T = temperatureIn;

		// Wall temperature
		if (Double.isNaN(this.heatSource) & !Double.isNaN(this.wallTemperature))
			T = this.wallTemperature;

		// Wall heat flux
		if (!Double.isNaN(this.heatSource) & Double.isNaN(this.wallTemperature)) {
			int i = 0;
			double Tlast;
			T = 293;
			do {
				Tlast = T;
				T = (temperatureIn + this.heatSource / 2 / getMaterial().getHeatCapacity(temperatureIn, pressure) / flowRate
						/ getMaterial().getDensity(temperatureIn, pressure))
						+ this.heatSource / getRth(flowRate, pressure, temperatureIn, Tlast) / getSurface();
				i++;
			} while (Math.abs(T / Tlast) > 1E-3 & i < 10);
		}

		return T;
	}

	/**
	 * Returns the outlet temperature
	 * 
	 * @param temperatureIn
	 * @param flowRate
	 * @param pressure
	 * @return
	 */
	public double getTemperatureOut(double temperatureIn, double flowRate, double pressure) {
		return getTemperature(temperatureIn, flowRate, pressure, getLength());
	}

	/**
	 * Returns the wall heat flux
	 * 
	 * @param temperatureIn
	 * @param flowRate
	 * @param pressure
	 * @return
	 * 
	 */
	public double getWallHeatFlux(double temperatureIn, double flowRate, double pressure) {
		double Qdot;

		// Simplest case: No heat transfer
		if (Double.isNaN(this.heatSource) & Double.isNaN(this.wallTemperature))
			Qdot = 0;

		// Wall temperature
		if (Double.isNaN(this.heatSource) & !Double.isNaN(this.wallTemperature))
			Qdot = (getTemperatureOut(temperatureIn, flowRate, pressure) - temperatureIn) * flowRate
					* getMaterial().getDensity(temperatureIn, pressure) * getMaterial().getHeatCapacity(temperatureIn, pressure);

		// Wall heat flux
		if (!Double.isNaN(this.heatSource) & Double.isNaN(this.wallTemperature))
			Qdot = this.heatSource;

		else
			Qdot = Double.NaN;

		return Qdot;
	}

	/**
	 * Returns the temperature at position x
	 * 
	 * @param temperatureIn
	 * @param flowRate
	 * @param pressure
	 * @param x
	 * @return
	 */
	public double getTemperature(double temperatureIn, double flowRate, double pressure, double x) {

		double T = Double.NaN;

		// Simplest case: No heat transfer
		if (Double.isNaN(this.heatSource) & Double.isNaN(this.wallTemperature))
			T = temperatureIn;

		// Wall temperature
		if (Double.isNaN(this.heatSource) & !Double.isNaN(this.wallTemperature))
			if (flowRate > 0)
				T = this.wallTemperature + Math
						.exp(-getProfile().getPerimeter() * x
								* getRth(flowRate, pressure, temperatureIn, this.wallTemperature)
								/ getMaterial().getHeatCapacity(temperatureIn, pressure)
								/ (flowRate * getMaterial().getDensity(temperatureIn, pressure)))
						* (temperatureIn - this.wallTemperature);
			else
				T = this.wallTemperature;

		// Wall heat flux
		if (!Double.isNaN(this.heatSource) & Double.isNaN(this.wallTemperature))
			T = temperatureIn + this.heatSource / (flowRate * getMaterial().getDensity(temperatureIn, pressure))
					/ getLength() / getMaterial().getHeatCapacity(temperatureIn, pressure) * x;

		return T;
	}

	/**
	 * Sets the wall heat flux and resets the wall temperature
	 * 
	 * @param heatSource [W]
	 */
	public void setHeatSource(double heatSource) {
		this.heatSource = heatSource;
		this.wallTemperature = Double.NaN;
	}

	/**
	 * Sets the wall temperature and resets the wall heat flux
	 * 
	 * @param wallTemperature [K]
	 */
	public void setWallTemperature(double wallTemperature) {
		this.wallTemperature = wallTemperature;
		this.heatSource = Double.NaN;
	}

	/**
	 * Sets the hydraulic profile
	 * 
	 * @param profile {@link AHydraulicProfile}
	 */
	@XmlElement
	public void setProfile(AHydraulicProfile profile) {
		this.profile = profile;
	}

	/**
	 * Returns the isolation object
	 * 
	 * @return {@link Isolation}
	 */
	@XmlElement
	public Isolation getIsolation() {
		if (this.isolation != null)
			if (this.isolation.isEmpty())
				return null;
			else
				return this.isolation;
		else
			return null;
	}

	@Override
	public abstract ADuctElement clone();

	/**
	 * Indicates if a wall temperature BC has been set
	 * 
	 * @return
	 */
	public boolean hasWallTemperature() {
		return !(Double.isNaN(wallTemperature));
	}

	/**
	 * Indicates if a heat flux has been set
	 * 
	 * @return
	 */
	public boolean hasHeatSource() {
		return !(Double.isNaN(heatSource));
	}

	/**
	 * Computes the Reynolds Number for the given conditions
	 * 
	 * @param temperatureIn
	 * @param flowRate
	 * @param pressureIn
	 * @return
	 */
	public double getRe(double temperatureIn, double flowRate, double pressureIn) {
		return Fluid.reynoldsNumber(getMaterial(), temperatureIn, pressureIn, getProfile(), flowRate);
	}

	/**
	 * Returns whether there is a bc defined
	 * 
	 * @return
	 */
	public boolean hasBoundaryCondition() {
		if (getAdiabatic() | !Double.isNaN(heatSource) | !Double.isNaN(wallTemperature))
			return true;
		return false;
	}

	@Override
	public int compareTo(ADuctElement e) {
		return this.getName().compareTo(e.getName());
	}

	/**
	 * @return
	 */
	public String getModelType() {
		return this.getClass().getSimpleName().replace("Duct", "");
	}

}
