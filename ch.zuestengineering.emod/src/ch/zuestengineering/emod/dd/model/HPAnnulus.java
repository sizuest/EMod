/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
/**
 * 
 */
package ch.zuestengineering.emod.dd.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

import ch.zuestengineering.emod.model.parameters.ParameterSet;
import ch.zuestengineering.emod.model.units.SiUnit;

/**
 * @author Simon Z�st
 *
 */
public class HPAnnulus extends AHydraulicProfile {
	@XmlElement
	private double radius1;
	@XmlElement
	private double radius2;

	/**
	 * 
	 */
	public HPAnnulus() {
	}

	/**
	 * @param radius1
	 * @param radius2
	 */
	public HPAnnulus(double radius1, double radius2) {
		this.radius1 = radius1;
		this.radius2 = radius2;
	}

	@Override
	public ParameterSet getParameterSet() {
		ParameterSet params = new ParameterSet();
		params.setPhysicalValue("Radius1", radius1, new SiUnit("m"));
		params.setPhysicalValue("Radius2", radius2, new SiUnit("m"));
		return params;
	}

	@Override
	@XmlTransient
	public void setParameterSet(ParameterSet ps) {
		radius1 = ps.getPhysicalValue("Radius1").getValue();
		radius2 = ps.getPhysicalValue("Radius2").getValue();
	}

	@Override
	public AHydraulicProfile clone() {
		HPAnnulus hp = new HPAnnulus();
		hp.radius1 = this.radius1;
		hp.radius2 = this.radius2;
		return hp;
	}

	@Override
	public String toString() {
		return "R1=" + radius1 + " m, R2=" + radius2 + " m";
	}

	@Override
	public double getHeight() {
		return 2 * radius1 - 2 * radius2;
	}

	@Override
	public double getWidth() {
		return getHeight();
	}

	@Override
	public double getArea() {
		return Math.PI * (radius1 * radius1 - radius2 * radius2);
	}

	@Override
	public double getPerimeter() {
		return Math.PI * 2 * (radius1 + radius2);
	}

}
