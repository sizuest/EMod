/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.dd.model;

import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import ch.zuestengineering.emod.model.fluid.Fluid;
import ch.zuestengineering.emod.model.parameters.ParameterSet;
import ch.zuestengineering.emod.model.units.SiUnit;

/**
 * Implements the hydrodynamic properties of a pipe
 * 
 * @author sizuest
 * 
 */
@XmlRootElement
public class DuctPipe extends ADuctElement {
	@XmlElement
	private double roughness = 1E-6;
	@XmlElement
	private double length = 1;
	@XmlElement
	private double count = 1;

	/**
	 * Constructor called from XmlUnmarshaller.
	 */
	public DuctPipe() {
		super();
	}

	/**
	 * @param u
	 * @param parent
	 */
	public void afterUnmarshal(final Unmarshaller u, final Object parent) {
		init();
	}

	/**
	 * Constructor by name
	 * 
	 * @param name
	 */
	public DuctPipe(String name) {
		super();
		this.name = name;
		init();
	}

	/**
	 * Constructor
	 * 
	 * @param length
	 * @param diameter
	 * @param roughness
	 * @param c 
	 */
	public DuctPipe(double length, double diameter, double roughness, double c) {
		super();
		this.length = length;
		this.profile = new HPCircular(diameter / 2);
		this.roughness = roughness;
		this.count = c;
		init();
	}

	/**
	 * Constructor for testing
	 * 
	 * @param name
	 * @param length
	 * @param diameter
	 */
	public DuctPipe(String name, double length, AHydraulicProfile diameter) {
		super();
		this.name = name;
		this.profile = diameter;
		init();
	}

	/**
	 * Initializes the element
	 */
	private void init() {
		super.length = this.length;
	}
	
	@Override
	public double getSurface() {
		return this.count * super.getSurface();
	}

	@Override
	public double getHydraulicSurface() {
		return this.count * super.getHydraulicSurface();
	}

	@Override
	public double getVolume() {
		return this.count * super.getVolume();
	}

	@Override
	public double getHTC(double flowRate, double pressure, double temperatureFluid, double temperatureWall) {
		return Fluid.convectionForcedPipe(this.material, temperatureWall, temperatureFluid, pressure, this.length,
				this.profile, flowRate / this.count);
	}

	@Override
	public double getPressureDrop(double flowRate, double pressure, double temperatureFluid) {
		return Fluid.pressureLossFrictionPipe(material, temperatureFluid, pressure, this.length, getDiameter(),
				flowRate, this.roughness / this.count);

	}

	@Override
	public ParameterSet getParameterSet() {
		ParameterSet ps = new ParameterSet(this.name);
		ps.setPhysicalValue("Length", this.length, new SiUnit("m"));
		ps.setPhysicalValue("Wall Roughness", this.roughness, new SiUnit("m"));
		ps.setPhysicalValue("Count", this.count, new SiUnit(""));
		return ps;
	}

	@Override
	@XmlTransient
	public void setParameterSet(ParameterSet ps) {
		this.length = ps.getPhysicalValue("Length").getValue();
		super.length = this.length;
		this.roughness = ps.getPhysicalValue("Wall Roughness").getValue();
		this.count = ps.getPhysicalValue("Count").getValue();
	}

	@Override
	public DuctPipe clone() {
		DuctPipe clone = new DuctPipe();

		clone.setParameterSet(this.getParameterSet());
		if (null == this.isolation)
			clone.setIsolation(null);
		else
			clone.setIsolation(this.isolation.clone());
		clone.setName(this.getName());

		clone.setProfile(getProfile().clone());

		return clone;
	}
	
	@Override
	public double getRe(double temperatureIn, double flowRate, double pressureIn) {
		return Fluid.reynoldsNumber(getMaterial(), temperatureIn, pressureIn, getProfile(), flowRate / this.count);
	}

}
