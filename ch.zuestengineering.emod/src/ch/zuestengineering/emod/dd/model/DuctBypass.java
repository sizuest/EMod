/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.dd.model;

import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import ch.zuestengineering.emod.dd.Duct;
import ch.zuestengineering.emod.model.material.Material;
import ch.zuestengineering.emod.model.parameters.ParameterSet;

/**
 * Duct element presenting a bypass
 * 
 * @author sizuest
 *
 */
@XmlRootElement
public class DuctBypass extends ADuctElement {

	@XmlElement
	Duct ductPrimary = new Duct(this.name + ": 1"), ductSecondary = new Duct(this.name + ": 2");

	double lastFlowRate, lastPressure, lastTemperatureFluid;
	double flowRatePrimary;

	/**
	 * Constructor for unmarshaller
	 */
	public DuctBypass() {
	}

	/**
	 * Constructor by name
	 * 
	 * @param name
	 */
	public DuctBypass(String name) {
		super();
		this.name = name;
		init();
	}

	/**
	 * called unmarshaller
	 * 
	 * @param u
	 * @param parent
	 */
	public void afterUnmarshal(final Unmarshaller u, final Object parent) {
		init();
	}

	private void init() {
		lastFlowRate = Double.NaN;
		lastPressure = Double.NaN;
		lastTemperatureFluid = Double.NaN;

		setMaterial(getMaterial());

		getPrimary().cleanUpFittings();
		getSecondary().cleanUpFittings();
	}

	@Override
	public double getHTC(double flowRate, double pressure, double temperatureFluid, double temperatureWall) {

		double htcPrimary = getPrimary().getHTC(getFlowRatePrimary(flowRate, pressure, temperatureFluid), pressure,
				temperatureFluid);

		return htcPrimary;
	}

	@Override
	public double getPressureDrop(double flowRate, double pressure, double temperatureFluid) {

		double pressureDrop = getPrimary().getPressureDrop(getFlowRatePrimary(flowRate, pressure, temperatureFluid),
				pressure, temperatureFluid);

		return pressureDrop;
	}

	/**
	 * Get the flow rate in the primary route
	 * 
	 * @param flowRate
	 * @param pressure
	 * @param temperatureFluid
	 * @return
	 */
	public double getFlowRatePrimary(double flowRate, double pressure, double temperatureFluid) {
		updateFlowRates(flowRate, pressure, temperatureFluid);

		return this.flowRatePrimary;
	}

	/**
	 * Get the flow rate in the secondary route
	 * 
	 * @param flowRate
	 * @param pressure
	 * @param temperatureFluid
	 * @return
	 */
	public double getFlowRateSecondary(double flowRate, double pressure, double temperatureFluid) {
		updateFlowRates(flowRate, pressure, temperatureFluid);

		return flowRate - this.flowRatePrimary;
	}

	@Override
	public DuctBypass clone() {
		DuctBypass element = new DuctBypass();
		element.init();
		element.setPrimary(element.ductPrimary.clone(this.ductPrimary));
		element.setSecondary(element.ductSecondary.clone(this.ductSecondary));

		element.setParameterSet(this.getParameterSet());
		if (null == this.isolation)
			element.setIsolation(null);
		else
			element.setIsolation(this.isolation.clone());

		element.setName(this.getName());

		return element;
	}

	@Override
	public ParameterSet getParameterSet() {
		return new ParameterSet();
	}

	@Override
	public void setParameterSet(ParameterSet ps) {
		// Not used here
	}

	/**
	 * Get the duct representing the primary rout
	 * 
	 * @return
	 */
	public Duct getPrimary() {
		return this.ductPrimary;
	}

	/**
	 * Get the duct representing the primary rout
	 * 
	 * @return
	 */
	public Duct getSecondary() {
		return this.ductSecondary;
	}

	private void setPrimary(Duct duct) {
		this.ductPrimary = duct;
	}

	private void setSecondary(Duct duct) {
		this.ductSecondary = duct;
	}

	private void updateFlowRates(double flowRate, double pressure, double temperatureFluid) {
		if (flowRate == lastFlowRate & pressure == lastPressure & temperatureFluid == lastTemperatureFluid)
			return;

		if (Double.isNaN(flowRate))
			return;

		lastFlowRate = flowRate;
		lastPressure = pressure;
		lastTemperatureFluid = temperatureFluid;

		if (Double.isNaN(this.flowRatePrimary) | 0 == this.flowRatePrimary | flowRate <= this.flowRatePrimary)
			this.flowRatePrimary = flowRate * .5;

		if (1E-9 >= flowRate) {
			this.flowRatePrimary = 0;
			return;
		}

		double k1 = 0, k2 = 0;
		double r = Double.POSITIVE_INFINITY;

		double flowRateMin = 0;
		double flowRateMax = flowRate;
		double p1, p2;

		for (int i = 0; i < 8; i++) {

			k1 = getPrimary().getPressureLossCoefficient(Math.max(this.flowRatePrimary, 1E-3 * flowRate), pressure,
					temperatureFluid);
			k2 = getSecondary().getPressureLossCoefficient(Math.max(flowRate - this.flowRatePrimary, 1E-3 * flowRate),
					pressure, temperatureFluid);

			if (k1 == k2)
				this.flowRatePrimary = flowRate * .5;
			else
				this.flowRatePrimary = flowRate * ((k2 - Math.sqrt(k1 * k2)) / (k2 - k1));

			p1 = getPrimary().getPressureDrop(this.flowRatePrimary, pressure, temperatureFluid);
			p2 = getSecondary().getPressureDrop(flowRate - this.flowRatePrimary, pressure, temperatureFluid);

			r = (p1 - p2) / p1;
			if (Math.abs(r) <= 1E-4)
				break;
		}

		if (Math.abs(r) > 1E-4) {

			p1 = getPrimary().getPressureDrop(this.flowRatePrimary, pressure, temperatureFluid);
			p2 = getSecondary().getPressureDrop(flowRate - this.flowRatePrimary, pressure, temperatureFluid);

			if (p1 < p2)
				flowRateMin = this.flowRatePrimary;
			else if (p1 > p2)
				flowRateMax = this.flowRatePrimary;

			for (int i = 0; i < 8; i++) {

				this.flowRatePrimary = (flowRateMax + flowRateMin) / 2;

				p1 = getPrimary().getPressureDrop(this.flowRatePrimary, pressure, temperatureFluid);
				p2 = getSecondary().getPressureDrop(flowRate - this.flowRatePrimary, pressure, temperatureFluid);

				if (p1 < p2)
					flowRateMin = this.flowRatePrimary;
				else if (p1 > p2)
					flowRateMax = this.flowRatePrimary;
				else
					break;

				r = (p1 - p2) / p1;
				if (Math.abs(r) <= 1E-3)
					break;
			}
		}

		ductPrimary.setFlowRate(flowRatePrimary);
		ductSecondary.setFlowRate(flowRate - flowRatePrimary);

	}

	@Override
	public void setMaterial(Material material) {
		this.material = material;
		getPrimary().setMaterial(material);
		getSecondary().setMaterial(material);
	}

	/**
	 * Returns the (hydraulic) diameter
	 * 
	 * @return [m]
	 */
	@Override
	public double getDiameter() {
		if (getPrimary().getElements().size() > 0)
			return getPrimary().getElement(0).getDiameter();
		else
			return 0.01;
	}

	/**
	 * Returns the length
	 * 
	 * @return [m]
	 */
	@Override
	public double getLength() {
		return this.getPrimary().getLength();
	}

	/**
	 * Returns the free surface
	 * 
	 * @return [m²]
	 */
	@Override
	public double getSurface() {
		return this.getPrimary().getSurface();
	}

	/**
	 * Returns the hydraulic surface
	 * 
	 * @return [m²]
	 */
	@Override
	public double getHydraulicSurface() {
		return this.getPrimary().getSurface();
	}

	/**
	 * Returns the total volume
	 * 
	 * @return [m³]
	 */
	@Override
	public double getVolume() {
		return this.getPrimary().getVolume();
	}

	@Override
	public AHydraulicProfile getProfile() {
		if (getPrimary().getElements().size() > 0)
			return getPrimary().getElement(0).getProfileIn();
		else
			return this.profile;
	}

	/**
	 * Returns the hydraulic profile object
	 * 
	 * @return {@link AHydraulicProfile}
	 */
	@Override
	public AHydraulicProfile getProfileIn() {
		if (getPrimary().getElements().size() > 0)
			return getPrimary().getElement(0).getProfileIn();
		else
			return this.profile;
	}

	/**
	 * Returns the hydraulic profile object
	 * 
	 * @return {@link AHydraulicProfile}
	 */
	@Override
	public AHydraulicProfile getProfileOut() {
		if (getPrimary().getElements().size() > 0)
			return getPrimary().getElement(getPrimary().getElements().size() - 1).getProfileOut();
		else
			return this.profile;
	}

	/**
	 * Returns the wall temperature
	 * 
	 * Override for bypass
	 * 
	 * @param temperatureIn
	 * @param flowRate
	 * @param pressure
	 * @return
	 */
	@Override
	public double getWallTemperature(double temperatureIn, double flowRate, double pressure) {
		return Double.NaN;
	}

	/**
	 * Returns the outlet temperature
	 * 
	 * Override for bypass
	 * 
	 * @param temperatureIn
	 * @param flowRate
	 * @return
	 */
	@Override
	public double getTemperatureOut(double temperatureIn, double flowRate, double pressureIn) {
		double T1, T2, Q1, Q2;

		Q1 = getFlowRatePrimary(flowRate, pressureIn, temperatureIn);
		Q2 = getFlowRateSecondary(flowRate, pressureIn, temperatureIn);
		T1 = ductPrimary.getTemperatureOut(Q1, pressureIn, temperatureIn);
		T2 = ductSecondary.getTemperatureOut(Q2, pressureIn, temperatureIn);

		return (T1 * Q1 + T2 * Q2) / (Q1 + Q2);
	}

	/**
	 * Returns the wall heat flux
	 * 
	 * Override for bypass
	 * 
	 * @param temperatureIn
	 * @param flowRate
	 * @return
	 * 
	 */
	@Override
	public double getWallHeatFlux(double temperatureIn, double flowRate, double pressureIn) {
		double heatFlux = 0;

		double Q1, Q2;

		Q1 = getFlowRatePrimary(flowRate, pressureIn, temperatureIn);
		Q2 = getFlowRateSecondary(flowRate, pressureIn, temperatureIn);

		for (ADuctElement e : ductPrimary.getElements())
			heatFlux += e.getWallHeatFlux(temperatureIn, Q1, pressureIn);

		for (ADuctElement e : ductSecondary.getElements())
			heatFlux += e.getWallHeatFlux(temperatureIn, Q2, pressureIn);

		return heatFlux;

	}

	/**
	 * Returns the temperature at position x
	 * 
	 * Override for bypass
	 * 
	 * @param temperatureIn
	 * @param flowRate
	 * @param pressure
	 * @param x
	 * @return
	 */
	@Override
	public double getTemperature(double temperatureIn, double flowRate, double pressure, double x) {
		return Double.NaN;
	}

	/**
	 * @param rootDuct
	 */
	public void setRootDuct(Duct rootDuct) {
		ductPrimary.setRootDuct(rootDuct);
		ductSecondary.setRootDuct(rootDuct);
	}

}
