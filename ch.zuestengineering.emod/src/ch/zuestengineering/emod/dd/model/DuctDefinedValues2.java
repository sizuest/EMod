/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.dd.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

import ch.zuestengineering.emod.model.fluid.Fluid;
import ch.zuestengineering.emod.model.parameters.ParameterSet;
import ch.zuestengineering.emod.model.units.SiUnit;
import ch.zuestengineering.emod.utils.Algo;

/**
 * Model of a duct element with defined HTC and Zeta
 * 
 * @author sizuest
 *
 */
public class DuctDefinedValues2 extends ADuctElement {
	@XmlElement
	double[] zetas = { 1, 2 };
	@XmlElement
	double[] alphas = { 100, 150 };
	@XmlElement
	double[] flowRates = { 4, 6 };
	@XmlElement
	double surface = 1;
	@XmlElement
	double length = .1;
	@XmlElement
	double volume = 0.01;

	/**
	 * Constructor for unmarshaller
	 */
	public DuctDefinedValues2() {
		super();
	}

	/**
	 * New element with given name
	 * 
	 * @param name
	 */
	public DuctDefinedValues2(String name) {
		super();
		this.name = name;
	}

	@Override
	public ParameterSet getParameterSet() {
		ParameterSet ps = new ParameterSet();

		ps.setPhysicalValue("PressureLossCoefficients", this.zetas, new SiUnit("-"));
		ps.setPhysicalValue("HeatTransferCoefficients", this.alphas, new SiUnit("W K^-1"));
		ps.setPhysicalValue("FlowRateSamples", flowRates, new SiUnit("l/min"));
		ps.setPhysicalValue("Surface", this.surface, new SiUnit("m^2"));
		ps.setPhysicalValue("Length", this.length, new SiUnit("m"));
		ps.setPhysicalValue("Volume", this.volume, new SiUnit("m^3"));

		return ps;
	}

	@Override
	@XmlTransient
	public void setParameterSet(ParameterSet ps) {
		this.zetas = ps.getPhysicalValue("PressureLossCoefficients").getValues();
		this.alphas = ps.getPhysicalValue("HeatTransferCoefficients").getValues();
		this.flowRates = ps.getPhysicalValue("FlowRateSamples").getValues();
		this.surface = ps.getPhysicalValue("Surface").getValue();
		this.length = ps.getPhysicalValue("Length").getValue();
		this.volume = ps.getPhysicalValue("Volume").getValue();

		super.length = this.length;
	}

	private double getAlpha(double flowRate) {
		if (alphas.length == 1)
			return alphas[0];
		else
			return Algo.linearInterpolation(flowRate * 60E3, flowRates, alphas, true);
	}

	private double getZeta(double flowRate) {
		if (zetas.length == 1)
			return zetas[0];
		else
			return Algo.linearInterpolation(flowRate * 60E3, flowRates, zetas, true);
	}

	@Override
	public double getHTC(double flowRate, double pressure, double temperatureFluid, double temperatureWall) {

		return getAlpha(flowRate) / getSurface();
	}

	@Override
	public double getPressureDrop(double flowRate, double pressure, double temperatureFluid) {
		return getZeta(flowRate) * Math.pow(flowRate, 2.0) * Fluid.sign(flowRate)
				* getMaterial().getDensity(temperatureFluid, pressure) / 2 / Math.pow(getProfile().getArea(), 2.0);
	}

	@Override
	public double getSurface() {
		return surface;
	}

	@Override
	public double getVolume() {
		return volume;
	}

	@Override
	public double getHydraulicSurface() {
		return getSurface();
	}

	@Override
	public DuctDefinedValues2 clone() {
		DuctDefinedValues2 clone = new DuctDefinedValues2();

		clone.setParameterSet(this.getParameterSet());
		if (null == this.isolation)
			clone.setIsolation(null);
		else
			clone.setIsolation(this.isolation.clone());
		clone.setName(this.getName());

		clone.setProfile(getProfile().clone());

		return clone;
	}

	@Override
	public double getPressureLossCoefficient(double flowRate, double pressure, double temperatureFluid) {
		return getZeta(flowRate);
	}

}
