/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.dd.model;

import javax.xml.bind.annotation.XmlTransient;

import ch.zuestengineering.emod.model.fluid.Fluid;
import ch.zuestengineering.emod.model.parameters.ParameterSet;

/**
 * Implements the hydrodynamic properties of a fitting
 * 
 * @author sizuest
 * 
 */
public class DuctFitting extends ADuctElement {

	private AHydraulicProfile p1 = new HPCircular(1), p2 = new HPCircular(1);

	/**
	 * Constructor for XmlUnmarshaler
	 */
	public DuctFitting() {
	}

	/**
	 * constructor by name and connected elements
	 * 
	 * @param name
	 * @param p1
	 * @param p2
	 */
	public DuctFitting(String name, AHydraulicProfile p1, AHydraulicProfile p2) {
		this.name = name;
		this.p1 = p1;
		this.p2 = p2;
		init();
	}

	/**
	 * Constructor by name
	 * 
	 * @param name
	 */
	public DuctFitting(String name) {
		this.name = name;
		this.p1 = new HPCircular();
		this.p2 = new HPCircular();
		init();
	}

	/**
	 * Initializes the element
	 */
	private void init() {
		this.profile = new HPCircular((p1.getDiameter() + p2.getDiameter()) / 2);
		this.length = 0; // Dummy
	}

	/**
	 * Sets the profiles
	 * 
	 * @param p1
	 * @param p2
	 */
	public void setProfiles(AHydraulicProfile p1, AHydraulicProfile p2) {
		this.p1 = p1;
		this.p2 = p2;
		init();
	}

	/**
	 * @return true if profiles are equal in (hydr. diameter)
	 */
	public boolean hasEqualProfiles() {
		if (p1.getDiameter() == p2.getDiameter())
			return true;
		else
			return false;
	}

	@Override
	public double getHTC(double flowRate, double pressure, double temperatureFluid, double temperatureWall) {
		return 0;
	}

	@Override
	public double getPressureDrop(double flowRate, double pressure, double temperatureFluid) {
		return Fluid.pressureLossFitting(getMaterial(), temperatureFluid, pressure, getLength(), p1, p2, flowRate);
	}

	@Override
	public ParameterSet getParameterSet() {
		return new ParameterSet();
	}

	@Override
	@XmlTransient
	public void setParameterSet(ParameterSet ps) {
		// Not used
	}

	@Override
	public DuctFitting clone() {
		DuctFitting clone = new DuctFitting();

		clone.setParameterSet(this.getParameterSet());
		if (null == this.isolation)
			clone.setIsolation(null);
		else
			clone.setIsolation(this.isolation.clone());
		clone.setName(this.getName());

		clone.p1 = this.p1.clone();
		clone.p2 = this.p2.clone();

		return clone;
	}

}
