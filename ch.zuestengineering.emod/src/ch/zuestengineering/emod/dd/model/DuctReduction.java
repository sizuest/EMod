/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.dd.model;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import ch.zuestengineering.emod.model.fluid.Fluid;
import ch.zuestengineering.emod.model.parameters.ParameterSet;
import ch.zuestengineering.emod.model.units.SiUnit;

/**
 * Implements the hydrodynamic properties of a fitting
 * 
 * @author sizuest
 * 
 */
@XmlRootElement
public class DuctReduction extends ADuctElement {
	@XmlAttribute
	private double diameter2 = 1;

	/**
	 * Constructor for XmlUnmarshaler
	 */
	public DuctReduction() {
	}

	/**
	 * constructor by name and connected elements
	 * 
	 * @param name
	 * @param diameter2
	 */
	public DuctReduction(String name, double diameter2) {
		this.name = name;
		this.diameter2 = diameter2;
		init();
	}

	/**
	 * Constructor by name
	 * 
	 * @param name
	 */
	public DuctReduction(String name) {
		this.name = name;
		init();
	}

	/**
	 * Initializes the element
	 */
	private void init() {
		this.length = 0; // Dummy
	}

	@Override
	public double getHTC(double flowRate, double pressure, double temperatureFluid, double temperatureWall) {
		return 0;
	}

	@Override
	public double getPressureDrop(double flowRate, double pressure, double temperatureFluid) {
		return Fluid.pressureLossReduction(getMaterial(), temperatureFluid, pressure, getLength(), getProfile(),
				new HPCircular(diameter2 / 2.0), flowRate);
	}

	@Override
	public ParameterSet getParameterSet() {
		ParameterSet params = new ParameterSet();
		params.setPhysicalValue("diameter", diameter2, new SiUnit("m"));
		return params;
	}

	@Override
	@XmlTransient
	public void setParameterSet(ParameterSet ps) {
		diameter2 = ps.getPhysicalValue("diameter").getValue();
	}

	@Override
	public DuctReduction clone() {
		DuctReduction clone = new DuctReduction();

		clone.setParameterSet(this.getParameterSet());
		if (null == this.isolation)
			clone.setIsolation(null);
		else
			clone.setIsolation(this.isolation.clone());
		clone.setName(this.getName());

		clone.setParameterSet(this.getParameterSet());
		return clone;
	}

}
