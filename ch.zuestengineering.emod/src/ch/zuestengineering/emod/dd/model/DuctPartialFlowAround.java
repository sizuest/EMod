/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.dd.model;

import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import ch.zuestengineering.emod.model.fluid.Fluid;
import ch.zuestengineering.emod.model.parameters.ParameterSet;
import ch.zuestengineering.emod.model.units.SiUnit;

/**
 * Implements the hydrodynamic properties of a helix
 * 
 * @author sizuest
 * 
 */
@XmlRootElement
public class DuctPartialFlowAround extends ADuctElement {
	@XmlElement
	double radius = .1;
	@XmlElement
	double angle = 270;

	/**
	 * Constructor called from XmlUnmarshaller.
	 */
	public DuctPartialFlowAround() {
		super();
	}

	/**
	 * @param u
	 * @param parent
	 */
	public void afterUnmarshal(final Unmarshaller u, final Object parent) {
		// post xml init method (loading physics data)
		init();
	}

	/**
	 * Constructor by name
	 * 
	 * @param name
	 */
	public DuctPartialFlowAround(String name) {
		super();
		this.name = name;
	}

	/**
	 * Constructor for testing
	 * 
	 * @param name
	 * @param r
	 * @param angle
	 * @param profile
	 */
	public DuctPartialFlowAround(String name, double r, double angle, AHydraulicProfile profile) {
		this.name = name;
		this.radius = r;
		this.angle = angle;
		this.profile = profile;
		init();
	}

	/**
	 * Initializes the element
	 */
	private void init() {
		this.length = this.radius * angle * Math.PI / 180.0;
	}

	@Override
	public double getHTC(double flowRate, double pressure, double temperatureFluid, double temperatureWall) {
		return Fluid.convectionForcedCoil(getMaterial(), temperatureWall, temperatureFluid, pressure, profile,
				radius * 2, 0, flowRate);
		// return Fluid.convectionForcedPipe(getMaterial(), temperatureWall,
		// temperatureFluid, getLength(), getProfile(), flowRate);
	}

	@Override
	public double getPressureDrop(double flowRate, double pressure, double temperatureFluid) {
		return Fluid.pressureLossFrictionCoil(getMaterial(), temperatureFluid, pressure, length, profile, 2 * radius, 0,
				flowRate);
	}

	@Override
	public ParameterSet getParameterSet() {
		ParameterSet ps = new ParameterSet(this.name);

		ps.setPhysicalValue("Radius", this.radius, new SiUnit("m"));
		ps.setPhysicalValue("Angle", this.angle, new SiUnit("�"));
		return ps;
	}

	@Override
	@XmlTransient
	public void setParameterSet(ParameterSet ps) {
		this.radius = ps.getPhysicalValue("Radius").getValue();
		this.angle = ps.getPhysicalValue("Angle").getValue();

		init();
	}

	@Override
	public DuctPartialFlowAround clone() {
		DuctPartialFlowAround clone = new DuctPartialFlowAround();

		clone.setParameterSet(this.getParameterSet());
		if (null == this.isolation)
			clone.setIsolation(null);
		else
			clone.setIsolation(this.isolation.clone());
		clone.setName(this.getName());

		clone.setProfile(getProfile().clone());

		return clone;
	}

}
