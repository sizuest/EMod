/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.dd.model;

import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import ch.zuestengineering.emod.model.fluid.Fluid;
import ch.zuestengineering.emod.model.parameters.ParameterSet;
import ch.zuestengineering.emod.model.units.SiUnit;

/**
 * Implements the hydrodynamic properties of a drill hole with a wire turbulator
 * The wire turbulator is assumed to cover the whole length of the drill hole
 * 
 * @author sizuest
 * 
 */
@XmlRootElement
public class DuctWireTurbulator extends ADuctElement {
	@XmlElement
	private double length = .1;
	@XmlElement
	private double count = 1;
	@XmlElement
	private double windingDistance = .01;
	@XmlElement
	private double wireDiameter = .001;

	/**
	 * Constructor called from XmlUnmarshaller.
	 */
	public DuctWireTurbulator() {
		super();
	}

	/**
	 * @param u
	 * @param parent
	 */
	public void afterUnmarshal(final Unmarshaller u, final Object parent) {
		// post xml init method (loading physics data)
		init();
	}

	/**
	 * Constructor by name
	 * 
	 * @param name
	 */
	public DuctWireTurbulator(String name) {
		super();
		this.name = name;
	}

	/**
	 * Constructor for Testing
	 * 
	 * @param name
	 * @param d
	 * @param l
	 * @param s
	 * @param dw
	 * @param c
	 */
	public DuctWireTurbulator(String name, double d, double l, double s, double dw, double c) {
		this.name = name;
		this.profile = new HPCircular(d / 2);
		this.length = l;
		this.windingDistance = s;
		this.windingDistance = dw;
		this.count = c;
		init();
	}

	/**
	 * Initializes the element
	 */
	private void init() {
		super.length = this.length;
	}

	@Override
	public double getSurface() {
		return this.count * super.getSurface();
	}

	@Override
	public double getHydraulicSurface() {
		return this.count * super.getHydraulicSurface();
	}

	@Override
	public double getVolume() {
		return this.count * super.getVolume();
	}

	@Override
	public double getHTC(double flowRate, double pressure, double temperatureFluid, double temperatureWall) {
		return Fluid.convectionForcedPipeWithWireTurbulator(material, temperatureFluid, temperatureWall, pressure,
				length, this.profile, windingDistance, wireDiameter, length, flowRate / this.count);
	}

	@Override
	public double getPressureDrop(double flowRate, double pressure, double temperatureFluid) {
		return Fluid.pressureLossFrictionPipeWithTurbulator(getMaterial(), temperatureFluid, pressure, length,
				getProfile(), windingDistance, wireDiameter, length, flowRate / this.count, .0);
	}

	@Override
	public ParameterSet getParameterSet() {
		ParameterSet ps = new ParameterSet(this.name);
		ps.setPhysicalValue("Length", this.length, new SiUnit("m"));
		ps.setPhysicalValue("Count", this.count, new SiUnit(""));
		ps.setPhysicalValue("Winding Distance", this.windingDistance, new SiUnit("m"));
		ps.setPhysicalValue("Wire Diameter", this.wireDiameter, new SiUnit("m"));
		return ps;
	}

	@Override
	@XmlTransient
	public void setParameterSet(ParameterSet ps) {
		this.length = ps.getPhysicalValue("Length").getValue();
		this.count = ps.getPhysicalValue("Count").getValue();
		this.windingDistance = ps.getPhysicalValue("Winding Distance").getValue();
		this.wireDiameter = ps.getPhysicalValue("Wire Diameter").getValue();
		init();
	}

	@Override
	public DuctWireTurbulator clone() {
		DuctWireTurbulator clone = new DuctWireTurbulator();

		clone.setParameterSet(this.getParameterSet());
		if (null == this.isolation)
			clone.setIsolation(null);
		else
			clone.setIsolation(this.isolation.clone());
		clone.setName(this.getName());

		clone.setProfile(getProfile().clone());

		return clone;
	}

	@Override
	public double getRe(double temperatureIn, double flowRate, double pressureIn) {
		return Fluid.reynoldsNumber(getMaterial(), temperatureIn, pressureIn, getProfile(), flowRate / count);
	}

}
