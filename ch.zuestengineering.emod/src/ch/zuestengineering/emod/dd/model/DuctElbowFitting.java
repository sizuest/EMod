/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.dd.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import ch.zuestengineering.emod.model.fluid.Fluid;
import ch.zuestengineering.emod.model.parameters.ParameterSet;
import ch.zuestengineering.emod.model.units.SiUnit;

/**
 * Implements the hydrodynamic properties of a elbow fitting
 * 
 * @author sizuest
 * 
 */
@XmlRootElement
public class DuctElbowFitting extends ADuctElement {

	@XmlElement
	double count = 1;

	/**
	 * Constructor for XmlUnmarshaller
	 */
	public DuctElbowFitting() {
		length = 0;
	}

	/**
	 * New elbow Fitting with the given name
	 * 
	 * @param name
	 */
	public DuctElbowFitting(String name) {
		this.name = name;
		length = 0;
	}

	@Override
	public double getHTC(double flowRate, double pressure, double temperatureFluid, double temperatureWall) {
		return 0;
	}

	@Override
	public double getPressureDrop(double flowRate, double pressure, double temperatureFluid) {
		return Fluid.pressureLoss90Angle(getMaterial(), temperatureFluid, pressure, getProfileIn(), flowRate / count);
	}

	@Override
	public ParameterSet getParameterSet() {
		ParameterSet ps = new ParameterSet();
		ps.setPhysicalValue("Count", this.count, new SiUnit());
		return ps;
	}

	@Override
	@XmlTransient
	public void setParameterSet(ParameterSet ps) {
		this.count = ps.getPhysicalValue("Count").getValue();
	}

	@Override
	public DuctElbowFitting clone() {
		DuctElbowFitting clone = new DuctElbowFitting();

		clone.setParameterSet(this.getParameterSet());
		if (null == this.isolation)
			clone.setIsolation(null);
		else
			clone.setIsolation(this.isolation.clone());
		clone.setName(this.getName());

		clone.setProfile(getProfile().clone());

		return clone;
	}

	@Override
	public double getRe(double temperatureIn, double flowRate, double pressureIn) {
		return Fluid.reynoldsNumber(getMaterial(), temperatureIn, pressureIn, getProfile(), flowRate / count);
	}
}
