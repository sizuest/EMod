/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.dd.model;

import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import ch.zuestengineering.emod.model.material.Material;

/**
 * @author Simon Z�st
 *
 */
@XmlRootElement
public class IsolationLayer {
	@XmlElement
	private double thickness;
	@XmlElement
	private String type = "none";
	private Material material = new Material();

	/**
	 * 
	 */
	public IsolationLayer() {
	}

	/**
	 * @param type
	 * @param thickness
	 */
	public IsolationLayer(String type, double thickness) {
		this.type = type;
		this.thickness = thickness;
		init();
	}

	/**
	 * @param u
	 * @param parent
	 */
	public void afterUnmarshal(final Unmarshaller u, final Object parent) {
		// post xml init method (loading physics data)
		init();
	}

	private void init() {
		this.material = new Material(type);
	}

	/**
	 * Returns the area specific thermal resistance
	 * 
	 * @return [W/m^2/K]
	 */
	public double getThermalResistance() {
		if (null == this.material)
			return Double.POSITIVE_INFINITY;

		return this.material.getThermalConductivity(293.15, 1E5) / this.thickness;
	}

	/**
	 * Returns the area specific thermal resistance for cylindrical area with radius
	 * ri
	 * 
	 * @param ri
	 * @return
	 */
	public double getThermalResistanceCircular(double ri) {
		if (null == this.material)
			return Double.POSITIVE_INFINITY;

		return this.material.getThermalConductivity(293.15, 1E5) / ri / Math.log(1 + this.thickness / ri);
	}

	@Override
	public String toString() {
		return this.material.getType() + ": " + this.thickness + " m";
	}

	/**
	 * Set the material by name
	 * 
	 * @param type
	 */
	public void setMaterial(String type) {
		this.type = type;
		init();
	}

	/**
	 * Returns a copy of the isolation
	 * 
	 * @return {@link Isolation}
	 */
	@Override
	public IsolationLayer clone() {
		return new IsolationLayer(type, thickness);
	}

	/**
	 * @param iso
	 */
	public void setIsolation(IsolationLayer iso) {
		this.type = iso.type;
		this.thickness = iso.thickness;
		init();
	}

	/**
	 * @return
	 */
	public Material getMaterial() {
		return this.material;
	}

	/**
	 * Get the thickness of the isolation
	 * 
	 * @return
	 */
	@XmlTransient
	public double getThickness() {
		return this.thickness;
	}

	/**
	 * @param thickness
	 */
	public void setThickness(double thickness) {
		this.thickness = thickness;
	}

	/**
	 * Thickens the isolation
	 * 
	 * @param thickness
	 */
	public void addThickness(double thickness) {
		this.thickness += thickness;

	}
}
