/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.dd.model;

import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import ch.zuestengineering.emod.model.fluid.Fluid;
import ch.zuestengineering.emod.model.parameters.ParameterSet;
import ch.zuestengineering.emod.model.units.SiUnit;
import ch.zuestengineering.emod.utils.Algo;

/**
 * Implements the hydrodynamic properties of a staggered helix
 * 
 * @author Simon Z�st
 *
 */
@XmlRootElement
public class DuctStaggeredHelix extends ADuctElement {
	@XmlElement
	double radius = .1;
	@XmlElement
	double height = .1;
	@XmlElement
	double distance = .05;
	@XmlElement
	double angle = 35;

	double N = 1;

	/**
	 * Constructor called from XmlUnmarshaller.
	 */
	public DuctStaggeredHelix() {
		super();
	}

	/**
	 * @param u
	 * @param parent
	 */
	public void afterUnmarshal(final Unmarshaller u, final Object parent) {
		// post xml init method (loading physics data)
		init();
	}

	/**
	 * Constructor by name
	 * 
	 * @param name
	 */
	public DuctStaggeredHelix(String name) {
		super();
		this.name = name;
	}

	/**
	 * Constructor for testing
	 * 
	 * @param name
	 * @param r
	 * @param h
	 * @param d
	 * @param a
	 * @param profile
	 */
	public DuctStaggeredHelix(String name, double r, double h, double d, double a, AHydraulicProfile profile) {
		this.name = name;
		this.radius = r;
		this.height = h;
		this.distance = d;
		this.angle = a;
		this.profile = profile;
		init();
	}

	/**
	 * Initializes the element
	 */
	private void init() {
		N = height / distance;
		this.length = (N + 1) * 2 * radius * Math.PI - distance / Math.sin(angle / 180.0 * Math.PI);
	}

	@Override
	public double getHTC(double flowRate, double pressure, double temperatureFluid, double temperatureWall) {
		return Fluid.convectionForcedCoil(getMaterial(), temperatureWall, temperatureFluid, pressure, profile,
				radius * 2, distance, flowRate);
	}

	@Override
	public double getPressureDrop(double flowRate, double pressure, double temperatureFluid) {
		double pHelix = Fluid.pressureLossFrictionCoil(getMaterial(), temperatureFluid, pressure, length, profile,
				2 * radius, distance, flowRate);
		double pSteps = getZeta(angle) * N * getMaterial().getDensity(temperatureFluid, pressure)
				* Math.pow(flowRate, 2.0) * Math.signum(flowRate) / Math.pow(getProfile().getArea(), 2.0);
		return pHelix + pSteps;
	}

	@Override
	public ParameterSet getParameterSet() {
		ParameterSet ps = new ParameterSet(this.name);

		ps.setPhysicalValue("Radius", this.radius, new SiUnit("m"));
		ps.setPhysicalValue("Height", this.height, new SiUnit("m"));
		ps.setPhysicalValue("Distance", this.distance, new SiUnit("m"));
		ps.setPhysicalValue("StepAngle", this.angle, new SiUnit("�"));
		return ps;
	}

	@Override
	@XmlTransient
	public void setParameterSet(ParameterSet ps) {
		this.radius = ps.getPhysicalValue("Radius").getValue();
		this.height = ps.getPhysicalValue("Height").getValue();
		this.distance = ps.getPhysicalValue("Distance").getValue();
		this.angle = ps.getPhysicalValue("StepAngle").getValue();

		init();
	}

	@Override
	public DuctHelix clone() {
		DuctHelix clone = new DuctHelix();

		clone.setParameterSet(this.getParameterSet());
		if (null == this.isolation)
			clone.setIsolation(null);
		else
			clone.setIsolation(this.isolation.clone());
		clone.setName(this.getName());

		clone.setProfile(getProfile().clone());

		return clone;
	}

	private double getZeta(double angle) {
		double[] angles = { 0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110 };
		double[] zetas = { 0, .05, .1, .2, .3, .4, .6, .8, 1, 1.3, 1.55, 1.9 };

		return Algo.linearInterpolation(angle, angles, zetas);
	}
}
