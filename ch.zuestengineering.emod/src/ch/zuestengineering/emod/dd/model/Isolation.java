/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.dd.model;

import java.util.ArrayList;
import java.util.Collections;

import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * General implementation of a duct element isolation
 * 
 * @author sizuest
 * 
 */
@XmlRootElement
public class Isolation {
	@XmlElement
	private double thickness;
	@XmlElement
	private String type = null;
	@XmlElement
	private ArrayList<IsolationLayer> layers = new ArrayList<>();

	/**
	 * 
	 */
	public Isolation() {
	}

	/**
	 * @param type
	 * @param thickness
	 */
	public Isolation(String type, double thickness) {
		layers.add(new IsolationLayer(type, thickness));
		init();
	}

	/**
	 * Performs the clean up actions: - combine layers if possible - remove layers
	 * with thickness zero
	 */
	public void cleanUp() {

		/*
		 * Combine layers with of the same material
		 */
		int idx = 0;
		for (int i = 1; i < layers.size(); i++) {
			if (layers.get(i).getMaterial().equals(layers.get(idx).getMaterial())) {
				layers.get(idx).addThickness(layers.get(i).getThickness());
				layers.get(i).setThickness(0);
			} else
				idx = i;
		}

		/*
		 * Remove layers with thickness zero
		 */
		for (int i = layers.size() - 1; i >= 0; i--)
			if (layers.get(i).getThickness() == 0)
				layers.remove(i);
	}

	/**
	 * @param u
	 * @param parent
	 */
	public void afterUnmarshal(final Unmarshaller u, final Object parent) {
		/*
		 * Legacy code: Required for old config files, where only one thickness and
		 * materil are provided
		 */
		if (!Double.isNaN(thickness) && type != null && !type.equals("none")) {
			layers.add(new IsolationLayer(type, thickness));
		}
		thickness = Double.NaN;
		type = null;
		init();
	}

	private void init() {
	}

	/**
	 * Returns the area specific thermal resistance
	 * 
	 * @return [W/m^2/K]
	 */
	public double getThermalResistance() {
		if (layers.size() == 0)
			return Double.POSITIVE_INFINITY;

		double rSum = 0, rTmp;

		for (IsolationLayer il : layers) {
			rTmp = il.getThermalResistance();

			if (rTmp != 0)
				rSum += 1 / rTmp;
		}

		if (rSum != 0)
			return 1 / rSum;
		else
			return 0;
	}

	/**
	 * Returns the area specific thermal resistance for a pipe
	 * 
	 * @param ri
	 * @return [W/m^2/K]
	 */
	public double getThermalResistanceCircular(double ri) {
		if (layers.size() == 0)
			return Double.POSITIVE_INFINITY;

		double rSum = 0, rTmp;

		for (IsolationLayer il : layers) {
			rTmp = il.getThermalResistanceCircular(ri);

			if (rTmp != 0)
				rSum += 1 / rTmp;

			ri += il.getThickness();
		}

		if (rSum != 0)
			return 1 / rSum;
		else
			return 0;
	}

	@Override
	public String toString() {
		switch (layers.size()) {
		case 0:
			return "-empty-";
		case 1:
			return layers.get(0).toString();
		default:
			return layers.size() + " layers";
		}
	}

	/**
	 * Set the material by name
	 * 
	 * @param type
	 */
	public void setMaterial(String type) {
		this.type = type;
		init();
	}

	/**
	 * Returns a copy of the isolation
	 * 
	 * @return {@link Isolation}
	 */
	@Override
	public Isolation clone() {
		Isolation iso = new Isolation();

		for (IsolationLayer il : getLayers())
			iso.addLayer(il.clone());

		return iso;
	}

	/**
	 * @param iso
	 */
	public void setIsolation(Isolation iso) {
		this.layers = iso.layers;
		init();
	}

	/**
	 * Returns whether the isolation is empty (no layers) or not
	 * 
	 * @return
	 */
	public boolean isEmpty() {
		return layers.isEmpty();
	}

	/**
	 * Sets the layers
	 * 
	 * @param layers
	 */
	public void setLayers(ArrayList<IsolationLayer> layers) {
		this.layers = layers;
	}

	/**
	 * Returns the layers
	 * 
	 * @return
	 */
	@XmlTransient
	public ArrayList<IsolationLayer> getLayers() {
		return layers;
	}

	/**
	 * Adds a new layer
	 * 
	 * @param layer
	 */
	public void addLayer(IsolationLayer layer) {
		layers.add(layer);
	}

	/**
	 * Moves a isolation layer up in the array
	 * 
	 * @param idx
	 */
	public void moveLayerIn(int idx) {
		if (idx >= layers.size() | idx < 1)
			return;

		Collections.swap(layers, idx, idx - 1);
	}

	/**
	 * Moves a isolation layer down in the array
	 * 
	 * @param idx
	 */
	public void moveLayerOut(int idx) {
		if (idx < 0 | idx >= layers.size() - 1)
			return;

		Collections.swap(layers, idx, idx + 1);
	}

	/**
	 * @param i
	 * @param isolationLayer
	 */
	public void addLayer(int i, IsolationLayer isolationLayer) {
		layers.add(i, isolationLayer);
	}

	/**
	 * Returns the resistance dependent on the profile type
	 * 
	 * @param profile
	 * @return
	 */
	public double getThermalResistance(AHydraulicProfile profile) {
		if (profile instanceof HPCircular)
			return getThermalResistanceCircular(profile.getDiameter() / 2);
		else
			return getThermalResistance();
	}
}
