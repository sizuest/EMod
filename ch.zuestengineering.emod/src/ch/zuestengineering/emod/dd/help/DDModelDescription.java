/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.dd.help;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.Writer;
import java.util.ArrayList;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import ch.zuestengineering.emod.dd.Duct;
import ch.zuestengineering.emod.dd.model.ADuctElement;
import ch.zuestengineering.emod.help.HelpFormater;
import ch.zuestengineering.emod.help.AModelHelp;
import ch.zuestengineering.emod.help.ValueDescription;
import ch.zuestengineering.emod.model.parameters.ParameterSet;
import ch.zuestengineering.emod.utils.PropertiesHandler;

/**
 * Implements the description of a duct element model based on the HelpTopic
 * class
 * 
 * Defintion: A DD model description consists of the following five elements: -
 * title: The elements name, followed by the modell's class name e.g.: Helix
 * (Helix) - content: Technical and theoretical description of the model -
 * params: List and description of all model parameters
 * 
 * @author Simon Z�st
 *
 */
@XmlRootElement
public class DDModelDescription extends AModelHelp {

	@XmlElement
	protected ArrayList<ValueDescription> parameterDescriptions = new ArrayList<>();

	/**
	 * Constructor for unmarshaller
	 */
	public DDModelDescription() {
	}

	/**
	 * @param u
	 * @param parent
	 */
	public void afterUnmarshal(Unmarshaller u, Object parent) {
		init();
	}

	/**
	 * @param type
	 */
	public DDModelDescription(String type) {
		this.type = type;

		parameterDescriptions = new ArrayList<ValueDescription>();

		title = "";
		content = "";

		init();
	}

	private void init() {
	}

	/**
	 * @return the parameterDescriptions
	 */
	public ArrayList<ValueDescription> getParameterDescriptions() {
		return parameterDescriptions;
	}

	/**
	 * Returns the description of the parameters as HTML table
	 * 
	 * @return
	 */
	public String getParametersAsHTMLTable() {
		return ValueDescription.getAsHTMLTable(getParameterDescriptions());
	}

	/**
	 * Loads a saved mode description for the given type
	 * 
	 * @param type
	 * @return
	 */
	public static DDModelDescription load(String type) {
		DDModelDescription help = null;
		try {
			JAXBContext context = JAXBContext.newInstance(DDModelDescription.class);
			Unmarshaller um = context.createUnmarshaller();
			help = (DDModelDescription) um.unmarshal(new FileReader(getPath(type)));
			help.type = type;
		} catch (Exception e) {
			e.printStackTrace();
			help = new DDModelDescription(type);
		}

		return help;
	}

	/**
	 * Saves the model description for the given model type
	 * 
	 * @param type
	 */
	public void save(String type) {
		try {
			JAXBContext context = JAXBContext.newInstance(DDModelDescription.class);
			Marshaller m = context.createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

			File path = new File(getPath(type));
			path.getParentFile().mkdirs();
			path.createNewFile();

			Writer w = new FileWriter(path);
			m.marshal(this, w);
			w.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Returns the path of the model documentation for the given model type
	 * 
	 * @param type
	 * @return
	 */
	public static String getPath(String type) {
		String filePath;

		String path_prefix = PropertiesHandler.getProperty("app.DuctDBPathPrefix");
		filePath = path_prefix + "/doc/doc_en_US_" + type + ".xml";

		return filePath;
	}

	/**
	 * Merges the parameters of the model and the description Model -> Name+Unit
	 * Desciption -> Desciption based on name
	 */
	private void getParametersFromModel() {

		ADuctElement mdl = Duct.newDuctElement(type);

		ParameterSet paramSet = mdl.getParameterSet();

		ArrayList<String> params, descr;

		params = paramSet.getNames();
		descr = new ArrayList<String>();

		for (ValueDescription vd : getParameterDescriptions())
			descr.add(vd.getName());

		for (String s : params) {
			int idx = descr.indexOf(s);

			String unit = "";
			try {
				unit = paramSet.getPhysicalValue(s).getUnit().toHTML();
			} catch (Exception e) {
			}

			if (idx >= 0)
				getParameterDescriptions().get(idx).setUnit(unit);
			else
				getParameterDescriptions().add(new ValueDescription(s, unit, ""));
		}
	}

	/**
	 * Returns the decription of the parameter with the stated name
	 * 
	 * @param name
	 * @return
	 */
	public String getParameterDescription(String name) {
		return getValueDescription(getParameterDescriptions(), name);
	}

	private String getValueDescription(ArrayList<ValueDescription> src, String name) {
		for (ValueDescription vd : src)
			if (vd.getName().equals(name))
				return vd.getDescription();

		return "Description not available";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.zuestengineering.emod.help.AModelHelp#getHTMLContent()
	 */
	@Override
	public String getHTMLContent() {
		String ret = "";
		ret += HelpFormater.formatParagraph(getImageHTML());
		ret += HelpFormater.formatParagraph("Model description", getContent());
		ret += HelpFormater.formatParagraph("Parameters", getParameterDescriptions());
		return ret;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.zuestengineering.emod.help.AModelHelp#compareToModel()
	 */
	@Override
	public void compareToModel() {
		getParametersFromModel();
	}

}
