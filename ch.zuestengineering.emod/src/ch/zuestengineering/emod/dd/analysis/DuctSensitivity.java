/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.dd.analysis;

import ch.zuestengineering.emod.dd.Duct;
import ch.zuestengineering.emod.dd.model.ADuctElement;
import ch.zuestengineering.emod.model.parameters.ParameterSet;

/**
 * Implements a sensitivity analysis of a duct
 * 
 * @author Simon Z�st
 *
 */
public class DuctSensitivity {
	/*
	 * Local copy of the duct to be analyzed
	 */
	private Duct duct;

	/**
	 * Ratio of the parameter variations to be performed during the variation
	 */
	private static double ALPHA = 0.05;

	/**
	 * Create a sensitivity analysis of the stated object
	 * 
	 * @param duct
	 */
	public DuctSensitivity(Duct duct) {
		// Create a local copy of the duct
		this.duct = new Duct("CopyOf_" + duct.getName());
		setDuct(duct);
	}

	/**
	 * sets the duct to be analyzed
	 * 
	 * @param duct
	 */
	public void setDuct(Duct duct) {
		this.duct.clone(duct);
		this.duct.setMaterial(this.duct.getMaterial());
	}

	/**
	 * Returns the pressure sensitivity for all element's parameters
	 * 
	 * @return
	 */
	public SensitivityResult getPressureSensitivity() {
		SensitivityResult ret = new SensitivityResult();

		for (ADuctElement e : duct.getAllElements()) {
			ParameterSet params = e.getParameterSet();

			for (String p : params.getNames())
				ret.set(e.getName(), p, getPressureSensitivity(e.getName(), p));
		}

		for (ADuctElement e : duct.getAllElements()) {
			ParameterSet profile = e.getProfile().getParameterSet();

			for (String p : profile.getNames())
				ret.set(e.getName(), "P: " + p, getPressureSensitivityProfile(e.getName(), p));
		}

		return ret;
	}

	/**
	 * Returns the htc sensitivity for all element's parameters
	 * 
	 * @param wallTemeprature
	 * @return
	 */
	public SensitivityResult getHTCSensitivity(double wallTemeprature) {
		SensitivityResult ret = new SensitivityResult();

		for (ADuctElement e : duct.getAllElements()) {
			ParameterSet params = e.getParameterSet();

			for (String p : params.getNames())
				ret.set(e.getName(), p, getHTCSensitivity(e.getName(), p, wallTemeprature));

		}

		for (ADuctElement e : duct.getAllElements()) {
			ParameterSet profile = e.getProfile().getParameterSet();

			for (String p : profile.getNames())
				ret.set(e.getName(), "P: " + p, getHTCSensitivityProfile(e.getName(), p, wallTemeprature));
		}

		return ret;
	}

	/**
	 * Returns the htc/pressure sensitivity for all element's parameters
	 * 
	 * @param wallTemeprature
	 * @return
	 */
	public SensitivityResult getHTCPerPSensitivity(double wallTemeprature) {
		SensitivityResult ret = new SensitivityResult();

		for (ADuctElement e : duct.getAllElements()) {
			ParameterSet params = e.getParameterSet();

			for (String p : params.getNames())
				ret.set(e.getName(), p,
						getHTCSensitivity(e.getName(), p, wallTemeprature) - getPressureSensitivity(e.getName(), p));

		}

		for (ADuctElement e : duct.getAllElements()) {
			ParameterSet profile = e.getProfile().getParameterSet();

			for (String p : profile.getNames())
				ret.set(e.getName(), "P: " + p, getHTCSensitivityProfile(e.getName(), p, wallTemeprature)
						- getPressureSensitivityProfile(e.getName(), p));
		}

		return ret;
	}

	private double getPressureSensitivity(String elementName, String parameter) {
		double p0, p1;

		// Element
		ADuctElement element = duct.getElement(elementName);

		// ParmeterSet
		ParameterSet params0 = element.getParameterSet(), params1 = element.getParameterSet();

		// Initial pressure
		p0 = duct.getPressureDrop(duct.getFlowRate(), duct.getInletPressure(), duct.getInletTemperature());

		try {
			// Do parameter Variation
			params1.setPhysicalValue(parameter, params1.getPhysicalValue(parameter).getValue() * (1 + ALPHA),
					params1.getPhysicalValue(parameter).getUnit());
			element.setParameterSet(params1);

			// Modified pressure
			p1 = duct.getPressureDrop(duct.getFlowRate(), duct.getInletPressure(), duct.getInletTemperature());
		} catch (Exception e) {
			p1 = Double.NaN;
		}

		// Reset parameters
		element.setParameterSet(params0);

		// Calculate sensitivity
		return 1 / ALPHA * (p1 / p0 - 1);
	}

	private double getPressureSensitivityProfile(String elementName, String parameter) {
		double p0, p1;

		// Element
		ADuctElement element = duct.getElement(elementName);

		// ParmeterSet
		ParameterSet params0 = element.getProfile().getParameterSet(), params1 = element.getProfile().getParameterSet();

		// Initial pressure
		p0 = duct.getPressureDrop(duct.getFlowRate(), duct.getInletPressure(), duct.getInletTemperature());

		try {
			// Do parameter Variation
			params1.setPhysicalValue(parameter, params1.getPhysicalValue(parameter).getValue() * (1 + ALPHA),
					params1.getPhysicalValue(parameter).getUnit());
			element.getProfile().setParameterSet(params1);

			// Modified pressure
			p1 = duct.getPressureDrop(duct.getFlowRate(), duct.getInletPressure(), duct.getInletTemperature());
		} catch (Exception e) {
			p1 = Double.NaN;
		}

		// Reset parameters
		element.getProfile().setParameterSet(params0);

		// Calculate sensitivity
		return 1 / ALPHA * (p1 / p0 - 1);
	}

	private double getHTCSensitivity(String elementName, String parameter, double wallTemeprature) {
		double htc0, htc1;

		// Element
		ADuctElement element = duct.getElement(elementName);

		// ParmeterSet
		ParameterSet params0 = element.getParameterSet(), params1 = element.getParameterSet();

		// Initial pressure
		htc0 = duct.getHTC(duct.getFlowRate(), duct.getInletPressure(), duct.getInletTemperature(), wallTemeprature);

		try {
			// Do parameter Variation
			params1.setPhysicalValue(parameter, params1.getPhysicalValue(parameter).getValue() * (1 + ALPHA),
					params1.getPhysicalValue(parameter).getUnit());
			element.setParameterSet(params1);

			// Modified pressure
			htc1 = duct.getHTC(duct.getFlowRate(), duct.getInletPressure(), duct.getInletTemperature(),
					wallTemeprature);
		} catch (Exception e) {
			htc1 = Double.NaN;
		}

		// Reset parameters
		element.setParameterSet(params0);

		// Calculate sensitivity
		return 1 / ALPHA * (htc1 / htc0 - 1);
	}

	private double getHTCSensitivityProfile(String elementName, String parameter, double wallTemeprature) {
		double htc0, htc1;

		// Element
		ADuctElement element = duct.getElement(elementName);

		// ParmeterSet
		ParameterSet params0 = element.getProfile().getParameterSet(), params1 = element.getProfile().getParameterSet();

		// Initial pressure
		htc0 = duct.getHTC(duct.getFlowRate(), duct.getInletPressure(), duct.getInletTemperature(), wallTemeprature);

		try {
			// Do parameter Variation
			params1.setPhysicalValue(parameter, params1.getPhysicalValue(parameter).getValue() * (1 + ALPHA),
					params1.getPhysicalValue(parameter).getUnit());
			element.getProfile().setParameterSet(params1);

			// Modified pressure
			htc1 = duct.getHTC(duct.getFlowRate(), duct.getInletPressure(), duct.getInletTemperature(),
					wallTemeprature);
		} catch (Exception e) {
			htc1 = Double.NaN;
		}

		// Reset parameters
		element.getProfile().setParameterSet(params0);

		// Calculate sensitivity
		return 1 / ALPHA * (htc1 / htc0 - 1);
	}

}
