/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.dd.analysis;

import java.util.ArrayList;

/**
 * Implements the results of a sensitivity analysis
 * 
 * @author Simon Z�st
 *
 */
public class SensitivityResult {
	private ArrayList<String> elementNames;
	private ArrayList<String> parameterNames;

	private ArrayList<ArrayList<Double>> sensitivities;

	/**
	 */
	public SensitivityResult() {
		this.elementNames = new ArrayList<String>();
		this.parameterNames = new ArrayList<String>();

		this.sensitivities = new ArrayList<ArrayList<Double>>();
	}

	/**
	 * @return
	 */
	public ArrayList<String> getElementNames() {
		return elementNames;
	}

	/**
	 * @return
	 */
	public ArrayList<String> getParameterNames() {
		return parameterNames;
	}

	/**
	 * Returns the sensitivity matrix
	 * 
	 * @return
	 */
	public ArrayList<ArrayList<Double>> getSensitivities() {
		return sensitivities;
	}

	/**
	 * returns the sensitivity of the elements parameter
	 * 
	 * @param element
	 * @param parameter
	 * @return
	 */
	public double get(String element, String parameter) {
		int idx1, idx2;

		idx1 = elementNames.indexOf(element);
		idx2 = parameterNames.indexOf(parameter);

		if (idx1 < 0 || idx2 < 0)
			return Double.NaN;

		return sensitivities.get(idx1).get(idx2);
	}

	/**
	 * Sets a sensitivity value
	 * 
	 * @param element
	 * @param parameter
	 * @param sensitivity
	 */
	public void set(String element, String parameter, double sensitivity) {
		int idx1, idx2;

		// Find location
		idx1 = elementNames.indexOf(element);
		if (idx1 == -1)
			idx1 = addElement(element);

		idx2 = parameterNames.indexOf(parameter);
		if (idx2 == -1)
			idx2 = addParameter(parameter);

		sensitivities.get(idx1).set(idx2, sensitivity);

	}

	private int addElement(String s) {
		elementNames.add(s);

		sensitivities.add(new ArrayList<Double>());

		for (int i = 0; i < parameterNames.size(); i++)
			sensitivities.get(sensitivities.size() - 1).add(Double.NaN);

		return elementNames.size() - 1;
	}

	private int addParameter(String s) {
		parameterNames.add(s);

		for (int i = 0; i < sensitivities.size(); i++)
			sensitivities.get(i).add(Double.NaN);

		return parameterNames.size() - 1;
	}

	/**
	 * Returns the maximal absolute sensitivity
	 * 
	 * @return
	 */
	public double getMax() {
		double max = 0;

		for (int i = 0; i < sensitivities.size(); i++)
			for (int j = 0; j < sensitivities.get(i).size(); j++)
				if (!Double.isNaN(sensitivities.get(i).get(j)))
					max = Math.max(max, Math.abs(sensitivities.get(i).get(j)));

		return max;
	}
}
