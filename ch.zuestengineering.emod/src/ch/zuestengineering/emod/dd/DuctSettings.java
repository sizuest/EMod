/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.dd;

import javax.xml.bind.annotation.XmlRootElement;

import ch.zuestengineering.emod.model.material.Material;

/**
 * Implements a container to save settings set during the use of DD
 * 
 * @author Simon Z�st
 *
 */
@XmlRootElement
public class DuctSettings {
	private Material material = new Material("Water");
	private double massFlowRate = 1E-4;
	private double inletPressure = 1E5;
	private double inletTemperature = 293.15;
	private double outletPressure = 0;

	/**
	 * @return the inletTemperature
	 */
	public double getInletTemperature() {
		return inletTemperature;
	}

	/**
	 * @param inletTemperature the inletTemperature to set
	 */
	public void setInletTemperature(double inletTemperature) {
		this.inletTemperature = inletTemperature;
	}

	/**
	 * @return
	 */
	public String getMaterialType() {
		return material.getType();
	}

	/**
	 * @return
	 */
	public Material getMaterial() {
		return material;
	}

	/**
	 * @param material
	 */
	public void setMaterial(String material) {
		this.material.setMaterial(material);
	}

	/**
	 * @param material
	 */
	public void setMaterial(Material material) {
		if (material != null & this.material != null)
			this.material.setMaterial(material.getType());
		else
			this.material = material;
	}

	/**
	 * @return
	 */
	public double getMassFlowRate() {
		return massFlowRate;
	}

	/**
	 * Retuns the flow rate [m3/s] at inlet conditions
	 * 
	 * @return
	 */
	public double getFlowRate() {
		return getFlowRate(massFlowRate, inletTemperature, inletPressure);
	}

	/**
	 * Retuns the flow rate [m3/s] at inlet conditions
	 * 
	 * @return
	 */
	public double getFlowRateNominal() {
		return getFlowRate(massFlowRate, 293.15, 1E5);
	}

	/**
	 * @param massFlowRate
	 * @param inletTemperature
	 * @param inletPressure
	 * @return
	 */
	public double getFlowRate(double massFlowRate, double inletTemperature, double inletPressure) {
		if (null == material)
			return Double.NaN;

		return massFlowRate / material.getDensity(inletTemperature, inletPressure);
	}

	/**
	 * Sets the flow rate in m3/s at inlet conditions
	 * 
	 * @param flowRate
	 */
	public void setFlowRate(double flowRate) {
		setMassFlowRate(flowRate * getMaterial().getDensity(inletTemperature, inletPressure));
	}

	/**
	 * @return
	 */
	public double getAvgPressure() {
		return 0.5 * (getInletPressure() - getOutletPressure());
	}

	/**
	 * Sets the flow rate in m3/s at inlet conditions
	 * 
	 * @param flowRate
	 */
	public void setMassFlowRate(double flowRate) {
		this.massFlowRate = flowRate;
	}

	/**
	 * @return
	 */
	public double getInletPressure() {
		return inletPressure;
	}

	/**
	 * @param inletPressure
	 */
	public void setInletPressure(double inletPressure) {
		this.inletPressure = inletPressure;
	}

	/**
	 * @return the outletPressure
	 */
	public double getOutletPressure() {
		return outletPressure;
	}

	/**
	 * @param outletPressure the outletPressure to set
	 */
	public void setOutletPressure(double outletPressure) {
		this.outletPressure = outletPressure;
	}

	/**
	 * Clones the settings
	 * 
	 * @param settings
	 */
	public void clone(DuctSettings settings) {
		this.inletPressure = settings.inletPressure;
		this.inletTemperature = settings.inletTemperature;
		this.massFlowRate = settings.massFlowRate;
		if (settings.material != null)
			this.material = new Material(settings.getMaterialType());

	}

}
