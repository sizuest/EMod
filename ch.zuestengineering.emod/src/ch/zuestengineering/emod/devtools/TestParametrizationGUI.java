/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.devtools;

import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

import ch.zuestengineering.emod.gui.modelling.EditMachineComponentGUI;
import ch.zuestengineering.emod.gui.utils.ShellUtils;

/**
 * Minimal implementation of a graphical license generator
 * 
 * @author Simon Z�st
 *
 */
public class TestParametrizationGUI {

	protected static Shell shell;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Display display = new Display();

		shell = new Shell(display);

		shell.setLayout(new GridLayout(3, false));

		shell.open();

		Shell gui = EditMachineComponentGUI.editMachineComponentGUI(shell, "Test", "GUI");
		ShellUtils.putToCenter(shell, gui);

		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}

	}
}
