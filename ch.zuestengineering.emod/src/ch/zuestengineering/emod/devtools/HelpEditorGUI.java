package ch.zuestengineering.emod.devtools;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;

import ch.zuestengineering.emod.help.AHelpTopic;
import ch.zuestengineering.emod.help.Help;
import ch.zuestengineering.emod.help.HelpTopicGeneric;

/**
 * Implements a GUI to edit the help library
 * 
 * @author Simon Z�st
 *
 */
public class HelpEditorGUI {
	protected static Shell shell;

	protected Tree treeIndex;
	protected Button buttonSave;
	protected Composite compositeEditor;
	protected Button buttonEdit, buttonAdd, buttonRemove, buttonUp, buttonDown;

	/**
	 * @param shell
	 */
	public HelpEditorGUI(Shell shell) {
		init();

		shell.open();
		shell.layout();
		shell.pack();
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Display display = new Display();

		shell = new Shell(display);

		new HelpEditorGUI(shell);

		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}

	}

	private void init() {
		Help.load("help/index.xml");

		shell.setLayout(new GridLayout(2, false));

		treeIndex = new Tree(shell, SWT.BORDER);
		treeIndex.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, true, 1, 1));

		fillTree();

		compositeEditor = new Composite(shell, SWT.NONE);
		compositeEditor.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));

		Composite treeActions = new Composite(shell, SWT.NONE);
		treeActions.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
		treeActions.setLayout(new GridLayout(5, true));

		buttonEdit = new Button(treeActions, SWT.PUSH);
		buttonEdit.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		buttonEdit.setText("E");
		buttonEdit.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				if (treeIndex.getSelection().length > 0)
					editTopic(treeIndex.getSelection()[0].getText());
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});
		buttonAdd = new Button(treeActions, SWT.PUSH);
		buttonAdd.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		buttonAdd.setText("+");
		buttonAdd.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				try {
					String name = Help.addTopic(new HelpTopicGeneric("New Topic"),
							treeIndex.getSelection()[0].getText());
					editTopic(treeIndex.getSelection()[0].getText() + "/" + name);
				} catch (Exception e1) {
					e1.printStackTrace();
				} finally {
					fillTree();
				}
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});
		buttonRemove = new Button(treeActions, SWT.PUSH);
		buttonRemove.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		buttonRemove.setText("-");
		buttonRemove.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				try {
					Help.removeTopic(treeIndex.getSelection()[0].getText());
					fillTree();
				} catch (Exception e1) {
					e1.printStackTrace();
				} finally {
					fillTree();
				}
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		buttonUp = new Button(treeActions, SWT.PUSH);
		buttonUp.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		buttonUp.setText("up");
		buttonUp.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				try {
					Help.moveTopicUp(treeIndex.getSelection()[0].getText());
					fillTree();
				} catch (Exception e1) {
					e1.printStackTrace();
				} finally {
					fillTree();
				}
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		buttonDown = new Button(treeActions, SWT.PUSH);
		buttonDown.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		buttonDown.setText("dw");
		buttonDown.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				try {
					Help.moveTopicDown(treeIndex.getSelection()[0].getText());
					fillTree();
				} catch (Exception e1) {
					e1.printStackTrace();
				} finally {
					fillTree();
				}
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		buttonSave = new Button(shell, SWT.PUSH);
		buttonSave.setLayoutData(new GridData(SWT.RIGHT, SWT.BOTTOM, false, false, 1, 2));
		buttonSave.setText("Write configuration");
		buttonSave.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				Help.save();
				Help.createHTMLFiles();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});
	}

	private void editTopic(String path) {
		AHelpTopic topic = Help.getTopic(path);

		genericEditor(topic);
	}

	/**
	 * 
	 */
	public void fillTree() {

		for (Control c : treeIndex.getChildren())
			c.dispose();

		treeIndex.clearAll(true);

		treeIndex.setItemCount(0);

		TreeItem item = new TreeItem(treeIndex, SWT.NONE);
		item.setText("ROOT");

		for (AHelpTopic ht : Help.getRootEntry().getChildren())
			fillTree(ht, item, "/");

		item.setExpanded(true);

		shell.layout();

	}

	private void fillTree(AHelpTopic topic, TreeItem parent, String pathPrefix) {
		TreeItem item = new TreeItem(parent, SWT.NONE);
		item.setText(pathPrefix + topic.getPathName());

		for (AHelpTopic ht : topic.getChildren())
			fillTree(ht, parent, pathPrefix + topic.getPathName() + "/");
	}

	private void genericEditor(AHelpTopic topic) {
		for (Control c : compositeEditor.getChildren())
			c.dispose();

		new TopicEditor(topic, compositeEditor, SWT.NONE, this);
	}
}

class TopicEditor extends Composite {

	Composite parent;
	AHelpTopic topic;
	Text textTitle, textKeyWord, textContent;
	Button buttonSave, buttonRestore;
	HelpEditorGUI gui;

	public TopicEditor(AHelpTopic topic, Composite parent, int style, HelpEditorGUI gui) {
		super(parent, style);
		this.topic = topic;
		this.parent = parent;
		this.gui = gui;
		init();
	}

	private void init() {
		parent.setLayout(new GridLayout(2, false));

		Label lableText = new Label(parent, SWT.NONE);
		lableText.setText("Titel");
		lableText.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 2, 1));

		textTitle = new Text(parent, SWT.BORDER);
		textTitle.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 2, 1));

		Label lableKeywords = new Label(parent, SWT.NONE);
		lableKeywords.setText("KeyWords (separated by ';'");
		lableKeywords.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 2, 1));

		textKeyWord = new Text(parent, SWT.BORDER);
		textKeyWord.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 2, 1));

		Label lableContent = new Label(parent, SWT.NONE);
		lableContent.setText("Content");
		lableContent.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 2, 1));

		textContent = new Text(parent, SWT.BORDER | SWT.MULTI | SWT.V_SCROLL | SWT.WRAP);
		textContent.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 2, 1));

		buttonRestore = new Button(parent, SWT.PUSH);
		buttonRestore.setText("Restore");
		buttonRestore.setLayoutData(new GridData(SWT.LEFT, SWT.BOTTOM, false, false, 1, 1));
		buttonRestore.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				read();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		buttonSave = new Button(parent, SWT.PUSH);
		buttonSave.setText("Save");
		buttonSave.setLayoutData(new GridData(SWT.RIGHT, SWT.BOTTOM, false, false, 1, 1));
		buttonSave.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				write();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		read();

		parent.layout();
	}

	private void read() {
		textTitle.setText(topic.getTitle());
		textKeyWord.setText(topic.getKeyWords());
		textContent.setText(topic.getContent());
	}

	private void write() {
		topic.setTitle(textTitle.getText());
		topic.setKeyWords(textKeyWord.getText());
		topic.setContent(textContent.getText());

		gui.fillTree();
	}

}
