/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.devtools;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import ch.zuestengineering.emod.licensing.License;
import ch.zuestengineering.emod.licensing.LicenseHandler;
import ch.zuestengineering.emod.licensing.LicenseType;

/**
 * Minimal implementation of a graphical license generator
 * 
 * @author Simon Z�st
 *
 */
public class LicenseToolGUI {

	protected static Shell shell;

	protected static Label labelOwner, labelOrg, labelType, labelMachineName, labelUserName, labelExpire, labelRSA;
	protected static Text textOwner, textOrg, textExpire, textMachineName, textUserName, textRSA;
	protected static Combo comboType;
	protected static Button buttonRSA, buttonGenerate;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Display display = new Display();

		shell = new Shell(display);

		shell.setLayout(new GridLayout(3, false));

		labelOwner = new Label(shell, SWT.NONE);
		labelOwner.setText("Owner");
		labelOwner.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1));

		textOwner = new Text(shell, SWT.BORDER);
		textOwner.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));

		labelOrg = new Label(shell, SWT.NONE);
		labelOrg.setText("Organization");
		labelOrg.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1));

		textOrg = new Text(shell, SWT.BORDER);
		textOrg.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));

		labelType = new Label(shell, SWT.NONE);
		labelType.setText("Type");
		labelType.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1));

		comboType = new Combo(shell, SWT.BORDER);
		comboType.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));

		String[] entries = new String[LicenseType.values().length];
		int i = 0;
		for (LicenseType l : LicenseType.values())
			entries[i++] = l.toString();
		comboType.setItems(entries);
		
		labelMachineName = new Label(shell, SWT.NONE);
		labelMachineName.setText("Machine name");
		labelMachineName.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1));

		textMachineName = new Text(shell, SWT.BORDER);
		textMachineName.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));
		
		labelUserName = new Label(shell, SWT.NONE);
		labelUserName.setText("User name");
		labelUserName.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1));

		textUserName = new Text(shell, SWT.BORDER);
		textUserName.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));

		labelExpire = new Label(shell, SWT.NONE);
		labelExpire.setText("Expire on [dd.mm.yyyy]");
		labelExpire.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1));

		textExpire = new Text(shell, SWT.BORDER);
		textExpire.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));

		labelRSA = new Label(shell, SWT.NONE);
		labelRSA.setText("Path to private key");
		labelRSA.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1));

		textRSA = new Text(shell, SWT.BORDER);
		textRSA.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		buttonRSA = new Button(shell, SWT.PUSH);
		buttonRSA.setText("...");
		buttonRSA.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1));

		buttonRSA.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				FileDialog fd = new FileDialog(shell, SWT.SAVE);
				fd.setText("Path to RSA private key");
				fd.setFilterPath("C:/");
				String[] filterExt = { "*.der" };
				fd.setFilterExtensions(filterExt);
				String selected = fd.open();
				if (selected == null) {
					return;
				}
				textRSA.setText(selected);
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				// TODO Auto-generated method stub

			}
		});

		buttonGenerate = new Button(shell, SWT.PUSH);
		buttonGenerate.setText("Generate");
		buttonGenerate.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 3, 1));

		buttonGenerate.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				generateLic();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		shell.open();
		shell.layout();
		shell.pack();

		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}

	}

	private static void generateLic() {

		try {
			License license = new License();
			DateFormat format = new SimpleDateFormat("dd.MM.yyyy");

			license.setOwner(textOwner.getText());
			license.setOrganization(textOrg.getText());
			System.out.println(format.parse(textExpire.getText()));
			license.setExpire(format.parse(textExpire.getText()));
			license.setType(LicenseType.valueOf(comboType.getText()));
			license.setMachine(textMachineName.getText());
			license.setUser(textUserName.getText());
			license.setKey(LicenseHandler.generateLicenseKey(license, textRSA.getText()));

			FileDialog fd = new FileDialog(shell, SWT.SAVE);
			fd.setText("License file path");
			fd.setFilterPath("C:/");
			String[] filterExt = { "*.lic" };
			fd.setFilterExtensions(filterExt);
			fd.setFileName("license.lic");
			String selected = fd.open();
			if (selected == null) {
				return;
			}

			license.generateLicenseFile(selected);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
