/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.devtools;

import java.util.ArrayList;
import java.util.Arrays;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;

import ch.zuestengineering.emod.dd.Duct;
import ch.zuestengineering.emod.dd.help.DDProfileDescription;
import ch.zuestengineering.emod.dd.model.AHydraulicProfile;
import ch.zuestengineering.emod.gui.utils.TableUtils;
import ch.zuestengineering.emod.help.Help;
import ch.zuestengineering.emod.help.HelpTopicModel;
import ch.zuestengineering.emod.help.ValueDescription;

/**
 * Implements a simple GUI to edit model documentations
 * 
 * @author simon
 *
 */
public class DDProfileDescriptionToolGUI {

	protected static Shell shell;

	protected static DDProfileDescription description = null;

	protected static Combo comboMdlType, comboImage;
	protected static Button buttonLoad, buttonSave, buttonPreview, buttonAll;
	protected static Text textTitle, textDescription, textZeta, textNusselt;
	protected static Table tableParams;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Display display = new Display();

		shell = new Shell(display);

		init();

		shell.open();
		shell.layout();
		shell.pack();

		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}

	}

	private static void init() {
		shell.setLayout(new GridLayout(3, false));

		comboMdlType = new Combo(shell, SWT.BORDER);
		comboMdlType.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));

		ArrayList<String> elements = new ArrayList<>();
		for (AHydraulicProfile e : Duct.getAvailableProfiles())
			elements.add(e.getClass().getSimpleName().replace("HP", ""));

		String[] subitems = elements.toArray(new String[1]);
		Arrays.sort(subitems);
		comboMdlType.setItems(subitems);

		buttonLoad = new Button(shell, SWT.PUSH);
		buttonLoad.setText("load");
		buttonLoad.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				load(comboMdlType.getText());
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		buttonSave = new Button(shell, SWT.PUSH);
		buttonSave.setText("save");
		buttonSave.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				save(comboMdlType.getText());
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		Label labelTitle = new Label(shell, SWT.NONE);
		labelTitle.setText("Name");
		labelTitle.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, false, 1, 1));

		Label labelImage = new Label(shell, SWT.NONE);
		labelImage.setText("Image");
		labelImage.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, false, 2, 1));

		textTitle = new Text(shell, SWT.BORDER);
		textTitle.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));

		Label labelDescription = new Label(shell, SWT.NONE);
		labelDescription.setText("Description");
		labelDescription.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, false, 3, 1));

		textDescription = new Text(shell, SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL | SWT.WRAP);
		textDescription.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 3, 1));

		Label labelParams = new Label(shell, SWT.NONE);
		labelParams.setText("Parameters");
		labelParams.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, false, 3, 1));

		tableParams = new Table(shell, SWT.BORDER);
		tableParams.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 3, 1));

		addCols(tableParams);

		try {
			TableUtils.addCellEditor(tableParams, new int[] { 1, 3 });
		} catch (Exception e) {
			e.printStackTrace();
		}

		buttonAll = new Button(shell, SWT.PUSH);
		buttonAll.setText("gernerate all");
		buttonAll.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, false, 3, 1));
		buttonAll.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				for (String s : comboMdlType.getItems()) {
					load(s);
					save(s);

					if (Help.getTopic("EModHelp/Models/Ductdesigner/Profiles/" + s).equals(Help.getRootEntry())) {
						HelpTopicModel topic = new HelpTopicModel();
						topic.setTitle(s);
						topic.setContent("DD/P/" + s);
						Help.addTopic(topic, "EModHelp/Models/Ductdesigner/Profiles/");

					}

				}

				Help.save();
				Help.createHTMLFiles();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		update();
	}

	private static void addCols(Table table) {
		String[] colNames = { "Name", "Symbol", "Unit", "Description" };

		for (String s : colNames)
			(new TableColumn(table, SWT.NONE)).setText(s);

		table.setHeaderVisible(true);
		table.setLinesVisible(true);
	}

	private static void update() {
		// Reset all
		textTitle.setText("");
		textDescription.setText("");

		for (TableItem ti : tableParams.getItems())
			ti.dispose();

		if (null == description)
			return;

		textTitle.setText(description.getTitle());
		textDescription.setText(description.getContent());

		fillTable(tableParams, description.getParameterDescriptions());

	}

	private static void fillTable(Table table, ArrayList<ValueDescription> src) {
		for (ValueDescription vd : src) {
			TableItem ti = new TableItem(table, SWT.NONE);
			ti.setText(0, vd.getName());
			ti.setText(1, vd.getSymbol());
			ti.setText(2, vd.getUnit());
			ti.setText(3, vd.getDescription());
		}

		for (TableColumn tc : table.getColumns())
			tc.pack();
	}

	private static void readTable(Table table, ArrayList<ValueDescription> tar) {
		for (int i = 0; i < tar.size(); i++) {
			tar.get(i).setSymbol(table.getItem(i).getText(1));
			tar.get(i).setDescription(table.getItem(i).getText(3));
		}
	}

	private static void load(String type) {
		description = DDProfileDescription.load(type);
		description.compareToModel();
		update();
	}

	private static void save(String type) {
		if (null == description)
			description = new DDProfileDescription(type);

		description.setTitle(textTitle.getText());
		description.setContent(textDescription.getText());

		readTable(tableParams, description.getParameterDescriptions());

		description.save(type);
	}
}
