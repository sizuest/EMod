/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.devtools;

import java.util.HashMap;

import ch.zuestengineering.emod.lca.building.BuildingClass;
import ch.zuestengineering.emod.lca.building.BuildingEnergyDemand;
import ch.zuestengineering.emod.lca.building.CoolingType;
import ch.zuestengineering.emod.lca.ecofactors.EcoFactor;
import ch.zuestengineering.emod.lca.ecofactors.EcoFactorTypes;
import ch.zuestengineering.emod.lca.ecofactors.EcoFactorsBurning;
import ch.zuestengineering.emod.lca.ecofactors.EcoFactorsElectricEnergy;
import ch.zuestengineering.emod.lca.ecofactors.EcoFactorsLandFill;
import ch.zuestengineering.emod.lca.ecofactors.EcoFactorsProduction;
import ch.zuestengineering.emod.lca.ecofactors.EcoFactorsRawMaterial;
import ch.zuestengineering.emod.lca.ecofactors.EcoFactorsRecycling;
import ch.zuestengineering.emod.lca.ecofactors.EcoFactorsTransport;
import ch.zuestengineering.emod.lca.typedef.ElectricEnergySource;
import ch.zuestengineering.emod.lca.typedef.RawMaterialType;
import ch.zuestengineering.emod.lca.typedef.TransportationType;
import ch.zuestengineering.emod.model.units.SiUnit;

/**
 * @author sizuest
 *
 */
public class GenerateEcoFactors {

	/**
	 * 
	 */
	public void testEcoFactorsRawMaterial() {
		EcoFactorsRawMaterial.getEcoFactors().setReferenceUnit(new SiUnit("kg"));

		HashMap<EcoFactorTypes, Double> factors = new HashMap<>();
		factors.put(EcoFactorTypes.PRIMARYENERGY, 30.0);
		factors.put(EcoFactorTypes.UBP, Double.NaN);
		factors.put(EcoFactorTypes.CO2EQ, Double.NaN);
		EcoFactorsRawMaterial.getEcoFactors().put(RawMaterialType.STEEL_IRON, new EcoFactor(factors));

		factors = new HashMap<>();
		factors.put(EcoFactorTypes.PRIMARYENERGY, 45.0);
		factors.put(EcoFactorTypes.UBP, Double.NaN);
		factors.put(EcoFactorTypes.CO2EQ, Double.NaN);
		EcoFactorsRawMaterial.getEcoFactors().put(RawMaterialType.CAST_IRON, new EcoFactor(factors));

		factors = new HashMap<>();
		factors.put(EcoFactorTypes.PRIMARYENERGY, 80.0);
		factors.put(EcoFactorTypes.UBP, Double.NaN);
		factors.put(EcoFactorTypes.CO2EQ, Double.NaN);
		EcoFactorsRawMaterial.getEcoFactors().put(RawMaterialType.STEEL_HIGHALLOYED, new EcoFactor(factors));

		factors = new HashMap<>();
		factors.put(EcoFactorTypes.PRIMARYENERGY, 80.0);
		factors.put(EcoFactorTypes.UBP, Double.NaN);
		factors.put(EcoFactorTypes.CO2EQ, Double.NaN);
		EcoFactorsRawMaterial.getEcoFactors().put(RawMaterialType.PE_PP_PET_POLYESTER, new EcoFactor(factors));

		factors = new HashMap<>();
		factors.put(EcoFactorTypes.PRIMARYENERGY, 90.0);
		factors.put(EcoFactorTypes.UBP, Double.NaN);
		factors.put(EcoFactorTypes.CO2EQ, Double.NaN);
		EcoFactorsRawMaterial.getEcoFactors().put(RawMaterialType.PS, new EcoFactor(factors));

		factors = new HashMap<>();
		factors.put(EcoFactorTypes.PRIMARYENERGY, 100.0);
		factors.put(EcoFactorTypes.UBP, Double.NaN);
		factors.put(EcoFactorTypes.CO2EQ, Double.NaN);
		EcoFactorsRawMaterial.getEcoFactors().put(RawMaterialType.NON_FERROUS_METAL, new EcoFactor(factors));

		factors = new HashMap<>();
		factors.put(EcoFactorTypes.PRIMARYENERGY, 105.0);
		factors.put(EcoFactorTypes.UBP, Double.NaN);
		factors.put(EcoFactorTypes.CO2EQ, Double.NaN);
		EcoFactorsRawMaterial.getEcoFactors().put(RawMaterialType.PUR_PC, new EcoFactor(factors));

		factors = new HashMap<>();
		factors.put(EcoFactorTypes.PRIMARYENERGY, 150.0);
		factors.put(EcoFactorTypes.UBP, Double.NaN);
		factors.put(EcoFactorTypes.CO2EQ, Double.NaN);
		EcoFactorsRawMaterial.getEcoFactors().put(RawMaterialType.POLYAMIDS_EP, new EcoFactor(factors));

		factors = new HashMap<>();
		factors.put(EcoFactorTypes.PRIMARYENERGY, 170.0);
		factors.put(EcoFactorTypes.UBP, Double.NaN);
		factors.put(EcoFactorTypes.CO2EQ, Double.NaN);
		EcoFactorsRawMaterial.getEcoFactors().put(RawMaterialType.ELECTRONICS, new EcoFactor(factors));

		factors = new HashMap<>();
		factors.put(EcoFactorTypes.PRIMARYENERGY, 22.0);
		factors.put(EcoFactorTypes.UBP, Double.NaN);
		factors.put(EcoFactorTypes.CO2EQ, Double.NaN);
		EcoFactorsRawMaterial.getEcoFactors().put(RawMaterialType.WOOD, new EcoFactor(factors));

		factors = new HashMap<>();
		factors.put(EcoFactorTypes.PRIMARYENERGY, 38.0);
		factors.put(EcoFactorTypes.UBP, Double.NaN);
		factors.put(EcoFactorTypes.CO2EQ, Double.NaN);
		EcoFactorsRawMaterial.getEcoFactors().put(RawMaterialType.CARDBOARD, new EcoFactor(factors));

		factors = new HashMap<>();
		factors.put(EcoFactorTypes.PRIMARYENERGY, 3.0);
		factors.put(EcoFactorTypes.UBP, Double.NaN);
		factors.put(EcoFactorTypes.CO2EQ, Double.NaN);
		EcoFactorsRawMaterial.getEcoFactors().put(RawMaterialType.BRICK_CONCRETE, new EcoFactor(factors));

		factors = new HashMap<>();
		factors.put(EcoFactorTypes.PRIMARYENERGY, 4.0);
		factors.put(EcoFactorTypes.UBP, Double.NaN);
		factors.put(EcoFactorTypes.CO2EQ, Double.NaN);
		EcoFactorsRawMaterial.getEcoFactors().put(RawMaterialType.CEMENT, new EcoFactor(factors));

		factors = new HashMap<>();
		factors.put(EcoFactorTypes.PRIMARYENERGY, 0.2);
		factors.put(EcoFactorTypes.UBP, Double.NaN);
		factors.put(EcoFactorTypes.CO2EQ, Double.NaN);
		EcoFactorsRawMaterial.getEcoFactors().put(RawMaterialType.SAND_GRAVEL, new EcoFactor(factors));

		factors = new HashMap<>();
		factors.put(EcoFactorTypes.PRIMARYENERGY, 40.0);
		factors.put(EcoFactorTypes.UBP, Double.NaN);
		factors.put(EcoFactorTypes.CO2EQ, Double.NaN);
		EcoFactorsRawMaterial.getEcoFactors().put(RawMaterialType.LUBRICANT, new EcoFactor(factors));

		factors = new HashMap<>();
		factors.put(EcoFactorTypes.PRIMARYENERGY, 1.03);
		factors.put(EcoFactorTypes.UBP, Double.NaN);
		factors.put(EcoFactorTypes.CO2EQ, Double.NaN);
		EcoFactorsRawMaterial.getEcoFactors().put(RawMaterialType.COMPRESSED_AIR, new EcoFactor(factors));

		factors = new HashMap<>();
		factors.put(EcoFactorTypes.PRIMARYENERGY, 4.5);
		factors.put(EcoFactorTypes.UBP, Double.NaN);
		factors.put(EcoFactorTypes.CO2EQ, Double.NaN);
		EcoFactorsRawMaterial.getEcoFactors().put(RawMaterialType.PROCESS_GAS, new EcoFactor(factors));

		factors = new HashMap<>();
		factors.put(EcoFactorTypes.PRIMARYENERGY, 0.0054);
		factors.put(EcoFactorTypes.UBP, Double.NaN);
		factors.put(EcoFactorTypes.CO2EQ, Double.NaN);
		EcoFactorsRawMaterial.getEcoFactors().put(RawMaterialType.WATER, new EcoFactor(factors));

		EcoFactorsRawMaterial.save();

	}

	/**
	 * 
	 */
	public void testEcoFactorsProduction() {
		EcoFactorsProduction.getEcoFactors().setReferenceUnit(new SiUnit("kg"));

		HashMap<EcoFactorTypes, Double> factors = new HashMap<>();
		factors.put(EcoFactorTypes.PRIMARYENERGY, 25.0);
		factors.put(EcoFactorTypes.UBP, Double.NaN);
		factors.put(EcoFactorTypes.CO2EQ, Double.NaN);
		EcoFactorsProduction.getEcoFactors().put(RawMaterialType.STEEL_IRON, new EcoFactor(factors));

		factors = new HashMap<>();
		factors.put(EcoFactorTypes.PRIMARYENERGY, 12.0);
		factors.put(EcoFactorTypes.UBP, Double.NaN);
		factors.put(EcoFactorTypes.CO2EQ, Double.NaN);
		EcoFactorsProduction.getEcoFactors().put(RawMaterialType.CAST_IRON, new EcoFactor(factors));

		factors = new HashMap<>();
		factors.put(EcoFactorTypes.PRIMARYENERGY, 30.0);
		factors.put(EcoFactorTypes.UBP, Double.NaN);
		factors.put(EcoFactorTypes.CO2EQ, Double.NaN);
		EcoFactorsProduction.getEcoFactors().put(RawMaterialType.STEEL_HIGHALLOYED, new EcoFactor(factors));

		factors = new HashMap<>();
		factors.put(EcoFactorTypes.PRIMARYENERGY, 20.0);
		factors.put(EcoFactorTypes.UBP, Double.NaN);
		factors.put(EcoFactorTypes.CO2EQ, Double.NaN);
		EcoFactorsProduction.getEcoFactors().put(RawMaterialType.PE_PP_PET_POLYESTER, new EcoFactor(factors));

		factors = new HashMap<>();
		factors.put(EcoFactorTypes.PRIMARYENERGY, 20.0);
		factors.put(EcoFactorTypes.UBP, Double.NaN);
		factors.put(EcoFactorTypes.CO2EQ, Double.NaN);
		EcoFactorsProduction.getEcoFactors().put(RawMaterialType.PS, new EcoFactor(factors));

		factors = new HashMap<>();
		factors.put(EcoFactorTypes.PRIMARYENERGY, 30.0);
		factors.put(EcoFactorTypes.UBP, Double.NaN);
		factors.put(EcoFactorTypes.CO2EQ, Double.NaN);
		EcoFactorsProduction.getEcoFactors().put(RawMaterialType.NON_FERROUS_METAL, new EcoFactor(factors));

		factors = new HashMap<>();
		factors.put(EcoFactorTypes.PRIMARYENERGY, 20.0);
		factors.put(EcoFactorTypes.UBP, Double.NaN);
		factors.put(EcoFactorTypes.CO2EQ, Double.NaN);
		EcoFactorsProduction.getEcoFactors().put(RawMaterialType.PUR_PC, new EcoFactor(factors));

		factors = new HashMap<>();
		factors.put(EcoFactorTypes.PRIMARYENERGY, 20.0);
		factors.put(EcoFactorTypes.UBP, Double.NaN);
		factors.put(EcoFactorTypes.CO2EQ, Double.NaN);
		EcoFactorsProduction.getEcoFactors().put(RawMaterialType.POLYAMIDS_EP, new EcoFactor(factors));

		factors = new HashMap<>();
		factors.put(EcoFactorTypes.PRIMARYENERGY, 0.0);
		factors.put(EcoFactorTypes.UBP, Double.NaN);
		factors.put(EcoFactorTypes.CO2EQ, Double.NaN);
		EcoFactorsProduction.getEcoFactors().put(RawMaterialType.ELECTRONICS, new EcoFactor(factors));

		factors = new HashMap<>();
		factors.put(EcoFactorTypes.PRIMARYENERGY, 0.0);
		factors.put(EcoFactorTypes.UBP, Double.NaN);
		factors.put(EcoFactorTypes.CO2EQ, Double.NaN);
		EcoFactorsProduction.getEcoFactors().put(RawMaterialType.WOOD, new EcoFactor(factors));

		factors = new HashMap<>();
		factors.put(EcoFactorTypes.PRIMARYENERGY, 0.0);
		factors.put(EcoFactorTypes.UBP, Double.NaN);
		factors.put(EcoFactorTypes.CO2EQ, Double.NaN);
		EcoFactorsProduction.getEcoFactors().put(RawMaterialType.CARDBOARD, new EcoFactor(factors));

		factors = new HashMap<>();
		factors.put(EcoFactorTypes.PRIMARYENERGY, 0.0);
		factors.put(EcoFactorTypes.UBP, Double.NaN);
		factors.put(EcoFactorTypes.CO2EQ, Double.NaN);
		EcoFactorsProduction.getEcoFactors().put(RawMaterialType.BRICK_CONCRETE, new EcoFactor(factors));

		factors = new HashMap<>();
		factors.put(EcoFactorTypes.PRIMARYENERGY, 0.0);
		factors.put(EcoFactorTypes.UBP, Double.NaN);
		factors.put(EcoFactorTypes.CO2EQ, Double.NaN);
		EcoFactorsProduction.getEcoFactors().put(RawMaterialType.CEMENT, new EcoFactor(factors));

		factors = new HashMap<>();
		factors.put(EcoFactorTypes.PRIMARYENERGY, 0.0);
		factors.put(EcoFactorTypes.UBP, Double.NaN);
		factors.put(EcoFactorTypes.CO2EQ, Double.NaN);
		EcoFactorsProduction.getEcoFactors().put(RawMaterialType.SAND_GRAVEL, new EcoFactor(factors));

		factors = new HashMap<>();
		factors.put(EcoFactorTypes.PRIMARYENERGY, 0.0);
		factors.put(EcoFactorTypes.UBP, Double.NaN);
		factors.put(EcoFactorTypes.CO2EQ, Double.NaN);
		EcoFactorsProduction.getEcoFactors().put(RawMaterialType.LUBRICANT, new EcoFactor(factors));

		factors = new HashMap<>();
		factors.put(EcoFactorTypes.PRIMARYENERGY, 0.0);
		factors.put(EcoFactorTypes.UBP, Double.NaN);
		factors.put(EcoFactorTypes.CO2EQ, Double.NaN);
		EcoFactorsProduction.getEcoFactors().put(RawMaterialType.COMPRESSED_AIR, new EcoFactor(factors));

		factors = new HashMap<>();
		factors.put(EcoFactorTypes.PRIMARYENERGY, 0.0);
		factors.put(EcoFactorTypes.UBP, Double.NaN);
		factors.put(EcoFactorTypes.CO2EQ, Double.NaN);
		EcoFactorsProduction.getEcoFactors().put(RawMaterialType.PROCESS_GAS, new EcoFactor(factors));

		factors = new HashMap<>();
		factors.put(EcoFactorTypes.PRIMARYENERGY, 0.0);
		factors.put(EcoFactorTypes.UBP, Double.NaN);
		factors.put(EcoFactorTypes.CO2EQ, Double.NaN);
		EcoFactorsProduction.getEcoFactors().put(RawMaterialType.WATER, new EcoFactor(factors));

		EcoFactorsProduction.save();
	}

	/**
	 * 
	 */
	public void testEcoFactorsRecycling() {
		EcoFactorsRecycling.getEcoFactors().setReferenceUnit(new SiUnit("kg"));

		HashMap<EcoFactorTypes, Double> factors = new HashMap<>();
		factors.put(EcoFactorTypes.PRIMARYENERGY, -16.0);
		factors.put(EcoFactorTypes.UBP, Double.NaN);
		factors.put(EcoFactorTypes.CO2EQ, Double.NaN);
		EcoFactorsRecycling.getEcoFactors().put(RawMaterialType.STEEL_IRON, new EcoFactor(factors));

		factors = new HashMap<>();
		factors.put(EcoFactorTypes.PRIMARYENERGY, -16.0);
		factors.put(EcoFactorTypes.UBP, Double.NaN);
		factors.put(EcoFactorTypes.CO2EQ, Double.NaN);
		EcoFactorsRecycling.getEcoFactors().put(RawMaterialType.CAST_IRON, new EcoFactor(factors));

		factors = new HashMap<>();
		factors.put(EcoFactorTypes.PRIMARYENERGY, -16.0);
		factors.put(EcoFactorTypes.UBP, Double.NaN);
		factors.put(EcoFactorTypes.CO2EQ, Double.NaN);
		EcoFactorsRecycling.getEcoFactors().put(RawMaterialType.STEEL_HIGHALLOYED, new EcoFactor(factors));

		factors = new HashMap<>();
		factors.put(EcoFactorTypes.PRIMARYENERGY, -16.0);
		factors.put(EcoFactorTypes.UBP, Double.NaN);
		factors.put(EcoFactorTypes.CO2EQ, Double.NaN);
		EcoFactorsRecycling.getEcoFactors().put(RawMaterialType.PE_PP_PET_POLYESTER, new EcoFactor(factors));

		factors = new HashMap<>();
		factors.put(EcoFactorTypes.PRIMARYENERGY, -16.0);
		factors.put(EcoFactorTypes.UBP, Double.NaN);
		factors.put(EcoFactorTypes.CO2EQ, Double.NaN);
		EcoFactorsRecycling.getEcoFactors().put(RawMaterialType.PS, new EcoFactor(factors));

		factors = new HashMap<>();
		factors.put(EcoFactorTypes.PRIMARYENERGY, -16.0);
		factors.put(EcoFactorTypes.UBP, Double.NaN);
		factors.put(EcoFactorTypes.CO2EQ, Double.NaN);
		EcoFactorsRecycling.getEcoFactors().put(RawMaterialType.NON_FERROUS_METAL, new EcoFactor(factors));

		factors = new HashMap<>();
		factors.put(EcoFactorTypes.PRIMARYENERGY, -16.0);
		factors.put(EcoFactorTypes.UBP, Double.NaN);
		factors.put(EcoFactorTypes.CO2EQ, Double.NaN);
		EcoFactorsRecycling.getEcoFactors().put(RawMaterialType.PUR_PC, new EcoFactor(factors));

		factors = new HashMap<>();
		factors.put(EcoFactorTypes.PRIMARYENERGY, -16.0);
		factors.put(EcoFactorTypes.UBP, Double.NaN);
		factors.put(EcoFactorTypes.CO2EQ, Double.NaN);
		EcoFactorsRecycling.getEcoFactors().put(RawMaterialType.POLYAMIDS_EP, new EcoFactor(factors));

		factors = new HashMap<>();
		factors.put(EcoFactorTypes.PRIMARYENERGY, -16.0);
		factors.put(EcoFactorTypes.UBP, Double.NaN);
		factors.put(EcoFactorTypes.CO2EQ, Double.NaN);
		EcoFactorsRecycling.getEcoFactors().put(RawMaterialType.ELECTRONICS, new EcoFactor(factors));

		factors = new HashMap<>();
		factors.put(EcoFactorTypes.PRIMARYENERGY, -16.0);
		factors.put(EcoFactorTypes.UBP, Double.NaN);
		factors.put(EcoFactorTypes.CO2EQ, Double.NaN);
		EcoFactorsRecycling.getEcoFactors().put(RawMaterialType.WOOD, new EcoFactor(factors));

		factors = new HashMap<>();
		factors.put(EcoFactorTypes.PRIMARYENERGY, -16.0);
		factors.put(EcoFactorTypes.UBP, Double.NaN);
		factors.put(EcoFactorTypes.CO2EQ, Double.NaN);
		EcoFactorsRecycling.getEcoFactors().put(RawMaterialType.CARDBOARD, new EcoFactor(factors));

		factors = new HashMap<>();
		factors.put(EcoFactorTypes.PRIMARYENERGY, 0.0);
		factors.put(EcoFactorTypes.UBP, Double.NaN);
		factors.put(EcoFactorTypes.CO2EQ, Double.NaN);
		EcoFactorsRecycling.getEcoFactors().put(RawMaterialType.BRICK_CONCRETE, new EcoFactor(factors));

		factors = new HashMap<>();
		factors.put(EcoFactorTypes.PRIMARYENERGY, 0.0);
		factors.put(EcoFactorTypes.UBP, Double.NaN);
		factors.put(EcoFactorTypes.CO2EQ, Double.NaN);
		EcoFactorsRecycling.getEcoFactors().put(RawMaterialType.CEMENT, new EcoFactor(factors));

		factors = new HashMap<>();
		factors.put(EcoFactorTypes.PRIMARYENERGY, 0.0);
		factors.put(EcoFactorTypes.UBP, Double.NaN);
		factors.put(EcoFactorTypes.CO2EQ, Double.NaN);
		EcoFactorsRecycling.getEcoFactors().put(RawMaterialType.SAND_GRAVEL, new EcoFactor(factors));

		factors = new HashMap<>();
		factors.put(EcoFactorTypes.PRIMARYENERGY, -16.0);
		factors.put(EcoFactorTypes.UBP, Double.NaN);
		factors.put(EcoFactorTypes.CO2EQ, Double.NaN);
		EcoFactorsRecycling.getEcoFactors().put(RawMaterialType.LUBRICANT, new EcoFactor(factors));

		factors = new HashMap<>();
		factors.put(EcoFactorTypes.PRIMARYENERGY, 0.0);
		factors.put(EcoFactorTypes.UBP, Double.NaN);
		factors.put(EcoFactorTypes.CO2EQ, Double.NaN);
		EcoFactorsProduction.getEcoFactors().put(RawMaterialType.COMPRESSED_AIR, new EcoFactor(factors));

		factors = new HashMap<>();
		factors.put(EcoFactorTypes.PRIMARYENERGY, 0.0);
		factors.put(EcoFactorTypes.UBP, Double.NaN);
		factors.put(EcoFactorTypes.CO2EQ, Double.NaN);
		EcoFactorsRecycling.getEcoFactors().put(RawMaterialType.PROCESS_GAS, new EcoFactor(factors));

		factors = new HashMap<>();
		factors.put(EcoFactorTypes.PRIMARYENERGY, 0.0);
		factors.put(EcoFactorTypes.UBP, Double.NaN);
		factors.put(EcoFactorTypes.CO2EQ, Double.NaN);
		EcoFactorsRecycling.getEcoFactors().put(RawMaterialType.WATER, new EcoFactor(factors));

		EcoFactorsRecycling.save();
	}

	/**
	 * 
	 */
	public void testEcoFactorsBurning() {
		EcoFactorsBurning.getEcoFactors().setReferenceUnit(new SiUnit("kg"));

		HashMap<EcoFactorTypes, Double> factors = new HashMap<>();
		factors.put(EcoFactorTypes.PRIMARYENERGY, Double.NaN);
		factors.put(EcoFactorTypes.UBP, Double.NaN);
		factors.put(EcoFactorTypes.CO2EQ, Double.NaN);
		EcoFactorsBurning.getEcoFactors().put(RawMaterialType.STEEL_IRON, new EcoFactor(factors));

		factors = new HashMap<>();
		factors.put(EcoFactorTypes.PRIMARYENERGY, Double.NaN);
		factors.put(EcoFactorTypes.UBP, Double.NaN);
		factors.put(EcoFactorTypes.CO2EQ, Double.NaN);
		EcoFactorsBurning.getEcoFactors().put(RawMaterialType.CAST_IRON, new EcoFactor(factors));

		factors = new HashMap<>();
		factors.put(EcoFactorTypes.PRIMARYENERGY, Double.NaN);
		factors.put(EcoFactorTypes.UBP, Double.NaN);
		factors.put(EcoFactorTypes.CO2EQ, Double.NaN);
		EcoFactorsBurning.getEcoFactors().put(RawMaterialType.STEEL_HIGHALLOYED, new EcoFactor(factors));

		factors = new HashMap<>();
		factors.put(EcoFactorTypes.PRIMARYENERGY, 16.0);
		factors.put(EcoFactorTypes.UBP, Double.NaN);
		factors.put(EcoFactorTypes.CO2EQ, Double.NaN);
		EcoFactorsBurning.getEcoFactors().put(RawMaterialType.PE_PP_PET_POLYESTER, new EcoFactor(factors));

		factors = new HashMap<>();
		factors.put(EcoFactorTypes.PRIMARYENERGY, 16.0);
		factors.put(EcoFactorTypes.UBP, Double.NaN);
		factors.put(EcoFactorTypes.CO2EQ, Double.NaN);
		EcoFactorsBurning.getEcoFactors().put(RawMaterialType.PS, new EcoFactor(factors));

		factors = new HashMap<>();
		factors.put(EcoFactorTypes.PRIMARYENERGY, Double.NaN);
		factors.put(EcoFactorTypes.UBP, Double.NaN);
		factors.put(EcoFactorTypes.CO2EQ, Double.NaN);
		EcoFactorsBurning.getEcoFactors().put(RawMaterialType.NON_FERROUS_METAL, new EcoFactor(factors));

		factors = new HashMap<>();
		factors.put(EcoFactorTypes.PRIMARYENERGY, 16.0);
		factors.put(EcoFactorTypes.UBP, Double.NaN);
		factors.put(EcoFactorTypes.CO2EQ, Double.NaN);
		EcoFactorsBurning.getEcoFactors().put(RawMaterialType.PUR_PC, new EcoFactor(factors));

		factors = new HashMap<>();
		factors.put(EcoFactorTypes.PRIMARYENERGY, 16.0);
		factors.put(EcoFactorTypes.UBP, Double.NaN);
		factors.put(EcoFactorTypes.CO2EQ, Double.NaN);
		EcoFactorsBurning.getEcoFactors().put(RawMaterialType.POLYAMIDS_EP, new EcoFactor(factors));

		factors = new HashMap<>();
		factors.put(EcoFactorTypes.PRIMARYENERGY, Double.NaN);
		factors.put(EcoFactorTypes.UBP, Double.NaN);
		factors.put(EcoFactorTypes.CO2EQ, Double.NaN);
		EcoFactorsBurning.getEcoFactors().put(RawMaterialType.ELECTRONICS, new EcoFactor(factors));

		factors = new HashMap<>();
		factors.put(EcoFactorTypes.PRIMARYENERGY, 8.0);
		factors.put(EcoFactorTypes.UBP, Double.NaN);
		factors.put(EcoFactorTypes.CO2EQ, Double.NaN);
		EcoFactorsBurning.getEcoFactors().put(RawMaterialType.WOOD, new EcoFactor(factors));

		factors = new HashMap<>();
		factors.put(EcoFactorTypes.PRIMARYENERGY, 8.0);
		factors.put(EcoFactorTypes.UBP, Double.NaN);
		factors.put(EcoFactorTypes.CO2EQ, Double.NaN);
		EcoFactorsBurning.getEcoFactors().put(RawMaterialType.CARDBOARD, new EcoFactor(factors));

		factors = new HashMap<>();
		factors.put(EcoFactorTypes.PRIMARYENERGY, Double.NaN);
		factors.put(EcoFactorTypes.UBP, Double.NaN);
		factors.put(EcoFactorTypes.CO2EQ, Double.NaN);
		EcoFactorsBurning.getEcoFactors().put(RawMaterialType.BRICK_CONCRETE, new EcoFactor(factors));

		factors = new HashMap<>();
		factors.put(EcoFactorTypes.PRIMARYENERGY, Double.NaN);
		factors.put(EcoFactorTypes.UBP, Double.NaN);
		factors.put(EcoFactorTypes.CO2EQ, Double.NaN);
		EcoFactorsBurning.getEcoFactors().put(RawMaterialType.CEMENT, new EcoFactor(factors));

		factors = new HashMap<>();
		factors.put(EcoFactorTypes.PRIMARYENERGY, Double.NaN);
		factors.put(EcoFactorTypes.UBP, Double.NaN);
		factors.put(EcoFactorTypes.CO2EQ, Double.NaN);
		EcoFactorsBurning.getEcoFactors().put(RawMaterialType.SAND_GRAVEL, new EcoFactor(factors));

		factors = new HashMap<>();
		factors.put(EcoFactorTypes.PRIMARYENERGY, 20.0);
		factors.put(EcoFactorTypes.UBP, Double.NaN);
		factors.put(EcoFactorTypes.CO2EQ, Double.NaN);
		EcoFactorsBurning.getEcoFactors().put(RawMaterialType.LUBRICANT, new EcoFactor(factors));

		factors = new HashMap<>();
		factors.put(EcoFactorTypes.PRIMARYENERGY, Double.NaN);
		factors.put(EcoFactorTypes.UBP, Double.NaN);
		factors.put(EcoFactorTypes.CO2EQ, Double.NaN);
		EcoFactorsBurning.getEcoFactors().put(RawMaterialType.COMPRESSED_AIR, new EcoFactor(factors));

		factors = new HashMap<>();
		factors.put(EcoFactorTypes.PRIMARYENERGY, Double.NaN);
		factors.put(EcoFactorTypes.UBP, Double.NaN);
		factors.put(EcoFactorTypes.CO2EQ, Double.NaN);
		EcoFactorsBurning.getEcoFactors().put(RawMaterialType.PROCESS_GAS, new EcoFactor(factors));

		factors = new HashMap<>();
		factors.put(EcoFactorTypes.PRIMARYENERGY, Double.NaN);
		factors.put(EcoFactorTypes.UBP, Double.NaN);
		factors.put(EcoFactorTypes.CO2EQ, Double.NaN);
		EcoFactorsBurning.getEcoFactors().put(RawMaterialType.WATER, new EcoFactor(factors));

		EcoFactorsBurning.save();
	}

	/**
	 * 
	 */
	public void testEcoFactorsLandFill() {
		EcoFactorsLandFill.getEcoFactors().setReferenceUnit(new SiUnit("kg"));

		HashMap<EcoFactorTypes, Double> factors = new HashMap<>();
		factors.put(EcoFactorTypes.PRIMARYENERGY, 0.0);
		factors.put(EcoFactorTypes.UBP, Double.NaN);
		factors.put(EcoFactorTypes.CO2EQ, Double.NaN);
		EcoFactorsLandFill.getEcoFactors().put(RawMaterialType.STEEL_IRON, new EcoFactor(factors));

		factors = new HashMap<>();
		factors.put(EcoFactorTypes.PRIMARYENERGY, 0.0);
		factors.put(EcoFactorTypes.UBP, Double.NaN);
		factors.put(EcoFactorTypes.CO2EQ, Double.NaN);
		EcoFactorsLandFill.getEcoFactors().put(RawMaterialType.CAST_IRON, new EcoFactor(factors));

		factors = new HashMap<>();
		factors.put(EcoFactorTypes.PRIMARYENERGY, 0.0);
		factors.put(EcoFactorTypes.UBP, Double.NaN);
		factors.put(EcoFactorTypes.CO2EQ, Double.NaN);
		EcoFactorsLandFill.getEcoFactors().put(RawMaterialType.STEEL_HIGHALLOYED, new EcoFactor(factors));

		factors = new HashMap<>();
		factors.put(EcoFactorTypes.PRIMARYENERGY, 0.0);
		factors.put(EcoFactorTypes.UBP, Double.NaN);
		factors.put(EcoFactorTypes.CO2EQ, Double.NaN);
		EcoFactorsLandFill.getEcoFactors().put(RawMaterialType.PE_PP_PET_POLYESTER, new EcoFactor(factors));

		factors = new HashMap<>();
		factors.put(EcoFactorTypes.PRIMARYENERGY, 0.0);
		factors.put(EcoFactorTypes.UBP, Double.NaN);
		factors.put(EcoFactorTypes.CO2EQ, Double.NaN);
		EcoFactorsLandFill.getEcoFactors().put(RawMaterialType.PS, new EcoFactor(factors));

		factors = new HashMap<>();
		factors.put(EcoFactorTypes.PRIMARYENERGY, 0.0);
		factors.put(EcoFactorTypes.UBP, Double.NaN);
		factors.put(EcoFactorTypes.CO2EQ, Double.NaN);
		EcoFactorsLandFill.getEcoFactors().put(RawMaterialType.NON_FERROUS_METAL, new EcoFactor(factors));

		factors = new HashMap<>();
		factors.put(EcoFactorTypes.PRIMARYENERGY, 0.0);
		factors.put(EcoFactorTypes.UBP, Double.NaN);
		factors.put(EcoFactorTypes.CO2EQ, Double.NaN);
		EcoFactorsLandFill.getEcoFactors().put(RawMaterialType.PUR_PC, new EcoFactor(factors));

		factors = new HashMap<>();
		factors.put(EcoFactorTypes.PRIMARYENERGY, 0.0);
		factors.put(EcoFactorTypes.UBP, Double.NaN);
		factors.put(EcoFactorTypes.CO2EQ, Double.NaN);
		EcoFactorsLandFill.getEcoFactors().put(RawMaterialType.POLYAMIDS_EP, new EcoFactor(factors));

		factors = new HashMap<>();
		factors.put(EcoFactorTypes.PRIMARYENERGY, 0.0);
		factors.put(EcoFactorTypes.UBP, Double.NaN);
		factors.put(EcoFactorTypes.CO2EQ, Double.NaN);
		EcoFactorsLandFill.getEcoFactors().put(RawMaterialType.ELECTRONICS, new EcoFactor(factors));

		factors = new HashMap<>();
		factors.put(EcoFactorTypes.PRIMARYENERGY, 0.0);
		factors.put(EcoFactorTypes.UBP, Double.NaN);
		factors.put(EcoFactorTypes.CO2EQ, Double.NaN);
		EcoFactorsLandFill.getEcoFactors().put(RawMaterialType.WOOD, new EcoFactor(factors));

		factors = new HashMap<>();
		factors.put(EcoFactorTypes.PRIMARYENERGY, 0.0);
		factors.put(EcoFactorTypes.UBP, Double.NaN);
		factors.put(EcoFactorTypes.CO2EQ, Double.NaN);
		EcoFactorsLandFill.getEcoFactors().put(RawMaterialType.CARDBOARD, new EcoFactor(factors));

		factors = new HashMap<>();
		factors.put(EcoFactorTypes.PRIMARYENERGY, 0.0);
		factors.put(EcoFactorTypes.UBP, Double.NaN);
		factors.put(EcoFactorTypes.CO2EQ, Double.NaN);
		EcoFactorsLandFill.getEcoFactors().put(RawMaterialType.BRICK_CONCRETE, new EcoFactor(factors));

		factors = new HashMap<>();
		factors.put(EcoFactorTypes.PRIMARYENERGY, 0.0);
		factors.put(EcoFactorTypes.UBP, Double.NaN);
		factors.put(EcoFactorTypes.CO2EQ, Double.NaN);
		EcoFactorsLandFill.getEcoFactors().put(RawMaterialType.CEMENT, new EcoFactor(factors));

		factors = new HashMap<>();
		factors.put(EcoFactorTypes.PRIMARYENERGY, 0.0);
		factors.put(EcoFactorTypes.UBP, Double.NaN);
		factors.put(EcoFactorTypes.CO2EQ, Double.NaN);
		EcoFactorsLandFill.getEcoFactors().put(RawMaterialType.SAND_GRAVEL, new EcoFactor(factors));

		factors = new HashMap<>();
		factors.put(EcoFactorTypes.PRIMARYENERGY, 0.0);
		factors.put(EcoFactorTypes.UBP, Double.NaN);
		factors.put(EcoFactorTypes.CO2EQ, Double.NaN);
		EcoFactorsLandFill.getEcoFactors().put(RawMaterialType.LUBRICANT, new EcoFactor(factors));

		factors = new HashMap<>();
		factors.put(EcoFactorTypes.PRIMARYENERGY, 0.0);
		factors.put(EcoFactorTypes.UBP, Double.NaN);
		factors.put(EcoFactorTypes.CO2EQ, Double.NaN);
		EcoFactorsLandFill.getEcoFactors().put(RawMaterialType.COMPRESSED_AIR, new EcoFactor(factors));

		factors = new HashMap<>();
		factors.put(EcoFactorTypes.PRIMARYENERGY, 0.0);
		factors.put(EcoFactorTypes.UBP, Double.NaN);
		factors.put(EcoFactorTypes.CO2EQ, Double.NaN);
		EcoFactorsLandFill.getEcoFactors().put(RawMaterialType.PROCESS_GAS, new EcoFactor(factors));

		factors = new HashMap<>();
		factors.put(EcoFactorTypes.PRIMARYENERGY, 0.0);
		factors.put(EcoFactorTypes.UBP, Double.NaN);
		factors.put(EcoFactorTypes.CO2EQ, Double.NaN);
		EcoFactorsLandFill.getEcoFactors().put(RawMaterialType.WATER, new EcoFactor(factors));

		EcoFactorsLandFill.save();
	}

	/**
	 * 
	 */
	public void testEcoFactorsTransportation() {
		EcoFactorsTransport.getEcoFactors().setReferenceUnit(new SiUnit("kg km"));

		HashMap<EcoFactorTypes, Double> factors = new HashMap<>();
		factors.put(EcoFactorTypes.PRIMARYENERGY, 9.0e-3);
		factors.put(EcoFactorTypes.UBP, Double.NaN);
		factors.put(EcoFactorTypes.CO2EQ, Double.NaN);
		EcoFactorsTransport.getEcoFactors().put(TransportationType.CAR, new EcoFactor(factors));

		factors = new HashMap<>();
		factors.put(EcoFactorTypes.PRIMARYENERGY, 11.7e-3);
		factors.put(EcoFactorTypes.UBP, Double.NaN);
		factors.put(EcoFactorTypes.CO2EQ, Double.NaN);
		EcoFactorsTransport.getEcoFactors().put(TransportationType.PLANE, new EcoFactor(factors));

		factors = new HashMap<>();
		factors.put(EcoFactorTypes.PRIMARYENERGY, 0.35e-3);
		factors.put(EcoFactorTypes.UBP, Double.NaN);
		factors.put(EcoFactorTypes.CO2EQ, Double.NaN);
		EcoFactorsTransport.getEcoFactors().put(TransportationType.SHIP, new EcoFactor(factors));

		factors = new HashMap<>();
		factors.put(EcoFactorTypes.PRIMARYENERGY, 0.8e-3);
		factors.put(EcoFactorTypes.UBP, Double.NaN);
		factors.put(EcoFactorTypes.CO2EQ, Double.NaN);
		EcoFactorsTransport.getEcoFactors().put(TransportationType.TRAIN, new EcoFactor(factors));

		factors = new HashMap<>();
		factors.put(EcoFactorTypes.PRIMARYENERGY, 0.5e-3);
		factors.put(EcoFactorTypes.UBP, Double.NaN);
		factors.put(EcoFactorTypes.CO2EQ, Double.NaN);
		EcoFactorsTransport.getEcoFactors().put(TransportationType.TRUCK, new EcoFactor(factors));

		factors = new HashMap<>();
		factors.put(EcoFactorTypes.PRIMARYENERGY, 5.6e-3);
		factors.put(EcoFactorTypes.UBP, Double.NaN);
		factors.put(EcoFactorTypes.CO2EQ, Double.NaN);
		EcoFactorsTransport.getEcoFactors().put(TransportationType.VAN, new EcoFactor(factors));

		EcoFactorsTransport.save();
	}

	/**
	 * 
	 */
	public void testEcoFactorsElectricEnergy() {
		EcoFactorsElectricEnergy.getEcoFactors().setReferenceUnit(new SiUnit("Wh"));

		HashMap<EcoFactorTypes, Double> factors = new HashMap<>();
		factors.put(EcoFactorTypes.PRIMARYENERGY, 4.7e-3);
		factors.put(EcoFactorTypes.UBP, Double.NaN);
		factors.put(EcoFactorTypes.CO2EQ, Double.NaN);
		EcoFactorsElectricEnergy.getEcoFactors().put(ElectricEnergySource.CH, new EcoFactor(factors));

		factors = new HashMap<>();
		factors.put(EcoFactorTypes.PRIMARYENERGY, 8.3e-3);
		factors.put(EcoFactorTypes.UBP, Double.NaN);
		factors.put(EcoFactorTypes.CO2EQ, Double.NaN);
		EcoFactorsElectricEnergy.getEcoFactors().put(ElectricEnergySource.EU, new EcoFactor(factors));

		factors = new HashMap<>();
		factors.put(EcoFactorTypes.PRIMARYENERGY, 11.9e-3);
		factors.put(EcoFactorTypes.UBP, Double.NaN);
		factors.put(EcoFactorTypes.CO2EQ, Double.NaN);
		EcoFactorsElectricEnergy.getEcoFactors().put(ElectricEnergySource.GLOBAL, new EcoFactor(factors));

		EcoFactorsElectricEnergy.save();
	}

	/**
	 * 
	 */
	public void testEcoBuildingERR() {
		BuildingEnergyDemand.getERRTable().set(BuildingClass.A, CoolingType.WATER, 5.05);
		BuildingEnergyDemand.getERRTable().set(BuildingClass.A, CoolingType.AIR, 2.6);
		BuildingEnergyDemand.getERRTable().set(BuildingClass.B, CoolingType.WATER, 4.85);
		BuildingEnergyDemand.getERRTable().set(BuildingClass.B, CoolingType.AIR, 2.5);
		BuildingEnergyDemand.getERRTable().set(BuildingClass.C, CoolingType.WATER, 4.45);
		BuildingEnergyDemand.getERRTable().set(BuildingClass.C, CoolingType.AIR, 2.3);
		BuildingEnergyDemand.getERRTable().set(BuildingClass.D, CoolingType.WATER, 4.05);
		BuildingEnergyDemand.getERRTable().set(BuildingClass.D, CoolingType.AIR, 2.1);
		BuildingEnergyDemand.getERRTable().set(BuildingClass.E, CoolingType.WATER, 3.65);
		BuildingEnergyDemand.getERRTable().set(BuildingClass.E, CoolingType.AIR, 1.9);
		BuildingEnergyDemand.getERRTable().set(BuildingClass.F, CoolingType.WATER, 3.25);
		BuildingEnergyDemand.getERRTable().set(BuildingClass.F, CoolingType.AIR, 1.7);
		BuildingEnergyDemand.getERRTable().set(BuildingClass.NONE, CoolingType.WATER, 3.05);
		BuildingEnergyDemand.getERRTable().set(BuildingClass.NONE, CoolingType.AIR, 1.6);

		BuildingEnergyDemand.save();
	}
}
