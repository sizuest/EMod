/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.simulation;

import java.util.logging.Logger;

import ch.zuestengineering.emod.EModSession;
import ch.zuestengineering.emod.Machine;
import ch.zuestengineering.emod.Process;
import ch.zuestengineering.emod.States;

/**
 * A EMod Simulation run based on machine and config names
 * 
 * @author sizuest
 *
 */
public class EModSimulationRun {

	private static Logger logger = Logger.getLogger(EModSimulationRun.class.getName());

	/**
	 * Method to run the simulation, copied from EModMain.java
	 */
	public static void EModSimRun() {
		if (EModSimulationMain.getRunningStatus()) {
			logger.warning("Simulation is allready running");
			return;
		}

		// Get name of machine
		String machineName = EModSession.getMachineName();

		if (machineName == null) {
			Exception e = new Exception("No machine name defined in the application configuration (app.config)!");
			e.printStackTrace();
			return;
		}

		// Get name of the machine configuration
		String machineConfigName = EModSession.getMachineConfig();
		if (machineConfigName == null) {
			Exception e = new Exception(
					"No machine config name defined in the application configuration (app.config)!");
			e.printStackTrace();
			return;
		}

		// Get name of the simulation configuration
		String simulationConfigName = EModSession.getSimulationConfig();
		if (simulationConfigName == null) {
			Exception e = new Exception(
					"No simulation config name defined in the application configuration (app.config)!");
			e.printStackTrace();
			return;
		}

		/* Build machine: Read and check machine configuration */
		Machine.buildMachine(machineName, machineConfigName);
		Machine.loadInitialConditions();

		States.readStates();

		/* Setup the simulation: Read simulation config */
		EModSimulationMain sim = new EModSimulationMain();

		/* Connect simulation with machine config */
		sim.setMachineComponentList(Machine.getMachineComponentList());
		sim.setIOConnectionList(Machine.getInstance().getIOLinkList());
		sim.setInputparamObjectList(Machine.getInstance().getInputObjectList());

		/* Setup the process */
		Process.loadProcess(EModSession.getProcessName());

		/* Set process parameters for simulation */
		sim.setProcessParamsforSimulation();

		/* Set simulation period for all simulation objects */
		sim.updateSimulationPeriod();

		/* Run the simulation */
		sim.runSimulation();
	}
}
