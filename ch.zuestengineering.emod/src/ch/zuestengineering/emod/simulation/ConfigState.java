/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.simulation;

/**
 * Representation of the status of a configuration
 * 
 * @author simon
 *
 */
public enum ConfigState {
	/**
	 * Everything ok, no problems expected
	 */
	OK,
	/**
	 * Minor issues, no technical problems expected
	 */
	WARNING,
	/**
	 * Major issues, technical problems expected
	 */
	ERROR,
}
