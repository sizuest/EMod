/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.simulation;

import java.io.File;
import java.io.FileWriter;
import java.io.BufferedWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import ch.zuestengineering.emod.model.MachineComponent;
import ch.zuestengineering.emod.model.fluid.Floodable;
import ch.zuestengineering.emod.model.fluid.FluidCircuitSolver;
import ch.zuestengineering.emod.model.linking.FluidContainer;
import ch.zuestengineering.emod.model.linking.IOContainer;
import ch.zuestengineering.emod.model.units.ContainerType;
import ch.zuestengineering.emod.model.units.SiUnit;
import ch.zuestengineering.emod.model.units.Unit;

/**
 * Definition of the simulation output (written as file)
 * 
 * FORMAT:
 * 
 * Time		State	[MCName1]-Input-1	...	[MCName1]-Output-1	...	[MCName1]-State-1	...	[SCName]-Sim	[SCName]-Sim	...	[FCName1]-FluidCircuit-1	[FCName1]-FluidCircuit-1
 * 					[MCName1].[Input1]	...	[MCName1].[Output1]	...	[MCName1].[State1]	...	[SCName].State	[SCName].Value	...	[FCName1]-NumberOfIterations[FCName1]-MaxChangeRate
 * 					[Unit]				...	[Unit]				...	[Unit]				...	[Unit]			[Unit]			...	[-]							[-]
 * 					[Type]				...	[Type]				...	STATE				...	INPUT			INPUT			...	FLUIDCIRCUITSOLVER			FLUIDCIRCUITSOLVER
 * VALUE	VALUE	VALUE				...	VALUE				...	VALUE				...	VALUE			VALUE			...	VALUE						VALUE
 * .		.		.						.						.						.				.					.							.
 * .		.		.						.						.						.				.					.							.
 * .		.		.						.						.						.				.					.							.
 * 
 * Time: 	Simulation time in second
 * State: 	Machine state during at the time	
 * MCName:	Name of the machine component (as defined in the model)	
 * SCName:	Name of the simulation control (as defined in the model)	
 * FCName:	Name of the fluid circuit (automatically defined)
 * Type:	Type of the signal {@link ContainerType}
 * 
 * Remark: For FluidContainers, three entries are generated (for inputs and outputs); e.g.:
 * 	[MCName1]-Output-1-T				[MCName1]-Output-1-V			[MCName1]-Output-1-p
 * 	[MCName1].[Output1]-Temperature		[MCName1].[Output1]-FlowRate	[MCName1].[Output1]-Pressure
 * 	[K]									[m3/s]							[Pa]
 * 	FLUIDDYNAMIC						FLUIDDYNAMIC					FLUIDDYNAMIC
 * 
 * @author andreas
 * 
 */
public class SimulationOutput {

	private BufferedWriter outfile;
	private ArrayList<MachineComponent> mclist;
	private List<ASimulationControl> simlist;
	private ArrayList<FluidCircuitSolver> fcslist;

	private int flushcnt;
	private DecimalFormat format;

	private String separator = "\t";

	/**
	 * Create data logging file. Write file header.
	 * 
	 * @param filename Name of file to write to simulation output to.
	 * @param list     List of model components.
	 * @param slist    List of simulation control objects.
	 * @param flist
	 */
	public SimulationOutput(String filename, ArrayList<MachineComponent> list, List<ASimulationControl> slist,
			ArrayList<FluidCircuitSolver> flist) {
		/* Init variables: */
		flushcnt = 0;
		mclist = list;
		simlist = slist;
		fcslist = flist;
		format = new DecimalFormat("####0.00");

		try {
			/* Create output file: */
			File file = new File(filename);
			file.getParentFile().mkdirs();
			outfile = new BufferedWriter(new FileWriter(file));

			/* ****** Make file header: ******* */
			/*
			 * 1st Line: Time\tState\tMcName1 Input 1\tMcName1 Input 2\tMcName1 Output 1\t...
			 */
			outfile.write("Time");
			outfile.write(separator + "State");
			for (MachineComponent mc : mclist) {
				for (int i = 0; i < mc.getComponent().getInputs().size(); i++) {
					if (mc.getComponent().getInputs().get(i).hasReference())
						continue;
					if (mc.getComponent() instanceof Floodable
							& mc.getComponent().getInputs().get(i) instanceof FluidContainer) {
						outfile.write(separator + mc.getName() + "-Input-" + (i + 1) + "-T");
						outfile.write(separator + mc.getName() + "-Input-" + (i + 1) + "-V");
						outfile.write(separator + mc.getName() + "-Input-" + (i + 1) + "-p");
					} else
						outfile.write(separator + mc.getName() + "-Input-" + (i + 1));
				}
				for (int i = 0; i < mc.getComponent().getOutputs().size(); i++) {
					if (mc.getComponent().getOutputs().get(i).hasReference())
						continue;
					if (mc.getComponent() instanceof Floodable
							& mc.getComponent().getOutputs().get(i) instanceof FluidContainer) {
						outfile.write(separator + mc.getName() + "-Output-" + (i + 1) + "-T");
						outfile.write(separator + mc.getName() + "-Output-" + (i + 1) + "-V");
						outfile.write(separator + mc.getName() + "-Output-" + (i + 1) + "-p");
					} else
						outfile.write(separator + mc.getName() + "-Output-" + (i + 1));
				}
				if (null != mc.getComponent().getDynamicStateList())
					for (int i = 0; i < mc.getComponent().getDynamicStateList().size(); i++) {
						outfile.write(separator + mc.getName() + "-State-" + (i + 1));
					}
			}
			for (ASimulationControl sc : simlist) {
				outfile.write(separator + sc.getName() + "-Sim");
				outfile.write(separator + sc.getName() + "-Sim");
			}
			for (FluidCircuitSolver fcs : flist) {
				outfile.write(separator + fcs.getName() + "-FluidCircuit-1");
				outfile.write(separator + fcs.getName() + "-FluidCircuit-2");
			}
			outfile.write("\n");
			/*
			 * 2nd line: Time\tMcName1.InputName1\tMcName1.InputName2\tMcName1.OutputName1
			 * \t...
			 */
			outfile.write("   ");
			outfile.write(separator);
			for (MachineComponent mc : mclist) {
				for (IOContainer input : mc.getComponent().getInputs()) {
					if (input.hasReference())
						continue;
					if (mc.getComponent() instanceof Floodable & input instanceof FluidContainer) {
						outfile.write(separator + mc.getName() + "." + input.getName() + "-Temperature");
						outfile.write(separator + mc.getName() + "." + input.getName() + "-FlowRate");
						outfile.write(separator + mc.getName() + "." + input.getName() + "-Pressure");
					} else
						outfile.write(separator + mc.getName() + "." + input.getName());
				}
				for (IOContainer output : mc.getComponent().getOutputs()) {
					if (output.hasReference())
						continue;
					if (mc.getComponent() instanceof Floodable & output instanceof FluidContainer) {
						outfile.write(separator + mc.getName() + "." + output.getName() + "-Temperature");
						outfile.write(separator + mc.getName() + "." + output.getName() + "-FlowRate");
						outfile.write(separator + mc.getName() + "." + output.getName() + "-Pressure");
					} else
						outfile.write(separator + mc.getName() + "." + output.getName());
				}
				if (null != mc.getComponent().getDynamicStateList())
					for (DynamicState state : mc.getComponent().getDynamicStateList()) {
						outfile.write(separator + mc.getName() + ".State." + state.getName());
					}
			}
			for (ASimulationControl sc : simlist) {
				outfile.write(separator + sc.getOutput().getName() + " State");
				outfile.write(separator + sc.getOutput().getName() + " Value");
			}
			for (FluidCircuitSolver fcs : flist) {
				outfile.write(separator + fcs.getName() + "-NumberOfIterations");
				outfile.write(separator + fcs.getName() + "-MaxChangeRate");
			}
			outfile.write("\n");
			/* 3rd line: [s]\t[WATT]\t[TEMP]\t... */
			outfile.write("[s]");
			outfile.write(separator);
			for (MachineComponent mc : mclist) {
				for (IOContainer input : mc.getComponent().getInputs()) {
					if (input.hasReference())
						continue;
					if (mc.getComponent() instanceof Floodable & input instanceof FluidContainer) {
						outfile.write(separator + "[" + (new SiUnit(Unit.KELVIN)).toString() + "]");
						outfile.write(separator + "[" + (new SiUnit(Unit.METERCUBIC_S)).toString() + "]");
						outfile.write(separator + "[" + (new SiUnit(Unit.PA)).toString() + "]");
					} else
						outfile.write(separator + "[" + input.getUnit().toString() + "]");
				}
				for (IOContainer output : mc.getComponent().getOutputs()) {
					if (output.hasReference())
						continue;
					if (mc.getComponent() instanceof Floodable & output instanceof FluidContainer) {
						outfile.write(separator + "[" + (new SiUnit(Unit.KELVIN)).toString() + "]");
						outfile.write(separator + "[" + (new SiUnit(Unit.METERCUBIC_S)).toString() + "]");
						outfile.write(separator + "[" + (new SiUnit(Unit.PA)).toString() + "]");
					} else
						outfile.write(separator + "[" + output.getUnit().toString() + "]");
				}
				if (null != mc.getComponent().getDynamicStateList())
					for (DynamicState state : mc.getComponent().getDynamicStateList()) {
						outfile.write(separator + "[" + state.getUnit().toString() + "]");
					}
			}
			for (ASimulationControl sc : simlist) {
				outfile.write(separator + "       ");
				outfile.write(separator + "[" + sc.getOutput().getUnit().toString() + "]");
			}
			for (int i = 0; i < flist.size(); i++) {
				outfile.write(separator + "[-]");
				outfile.write(separator + "[-]");
			}
			outfile.write("\n");
			/* 4th line: -\tType\tType\t... */
			outfile.write("TIME");
			outfile.write(separator);
			for (MachineComponent mc : mclist) {
				for (IOContainer input : mc.getComponent().getInputs()) {
					if (input.hasReference())
						continue;
					if (mc.getComponent() instanceof Floodable & input instanceof FluidContainer) {
						outfile.write(separator + ContainerType.FLUIDDYNAMIC.toString());
						outfile.write(separator + ContainerType.FLUIDDYNAMIC.toString());
						outfile.write(separator + ContainerType.FLUIDDYNAMIC.toString());
					} else
						outfile.write(separator + input.getType().toString());
				}
				for (IOContainer output : mc.getComponent().getOutputs()) {
					if (output.hasReference())
						continue;
					if (mc.getComponent() instanceof Floodable & output instanceof FluidContainer) {
						outfile.write(separator + ContainerType.FLUIDDYNAMIC.toString());
						outfile.write(separator + ContainerType.FLUIDDYNAMIC.toString());
						outfile.write(separator + ContainerType.FLUIDDYNAMIC.toString());
					} else
						outfile.write(separator + output.getType().toString());
				}
				if (null != mc.getComponent().getDynamicStateList())
					for (int i = 0; i < mc.getComponent().getDynamicStateList().size(); i++) {
						outfile.write(separator + "STATE");
					}
			}
			for (int i = 0; i < simlist.size(); i++) {
				outfile.write(separator + "       ");
				outfile.write(separator + "INPUT");
			}
			for (int i = 0; i < flist.size(); i++) {
				outfile.write(separator + "FLUIDCIRCUITSOLVER");
				outfile.write(separator + "FLUIDCIRCUITSOLVER");
			}
			outfile.write("\n");

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Write samples to data logger.
	 * 
	 * @param time   Actual time in [s].
	 * @param mstate
	 */
	public void logData(double time, MachineState mstate) {
		try {
			outfile.write(format.format(time));
			outfile.write(separator + mstate.toString());
			for (MachineComponent mc : mclist) {
				for (IOContainer input : mc.getComponent().getInputs()) {
					if (input.hasReference())
						continue;
					if (mc.getComponent() instanceof Floodable & input instanceof FluidContainer) {
						outfile.write(separator + ((FluidContainer) input).getTemperature());
						outfile.write(separator + ((FluidContainer) input).getFluidCircuitProperties().getFlowRate());
						outfile.write(separator + ((FluidContainer) input).getPressure());
					} else
						outfile.write(separator + input.getValue());
				}
				for (IOContainer output : mc.getComponent().getOutputs()) {
					if (output.hasReference())
						continue;
					if (mc.getComponent() instanceof Floodable & output instanceof FluidContainer) {
						outfile.write(separator + ((FluidContainer) output).getTemperature());
						outfile.write(separator + ((FluidContainer) output).getFluidCircuitProperties().getFlowRate());
						outfile.write(separator + ((FluidContainer) output).getPressure());
					} else
						outfile.write(separator + output.getValue());
				}
				if (null != mc.getComponent().getDynamicStateList())
					for (DynamicState state : mc.getComponent().getDynamicStateList()) {
						outfile.write(separator + state.getValue());
					}
			}
			for (ASimulationControl sc : simlist) {
				outfile.write(separator + sc.getState().toString());
				outfile.write(separator + sc.getOutput().getValue());
			}
			for (FluidCircuitSolver fcs : fcslist) {
				outfile.write(separator + fcs.getIterations());
				outfile.write(separator + fcs.getChangeRate());
			}
			outfile.write("\n");

			// Flush file every 32th samples only.
			if (flushcnt >= 32) {
				outfile.flush();
				flushcnt = 0;
			}
			flushcnt++;
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Close data logger.
	 */
	public void close() {
		try {
			outfile.flush();
			outfile.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
