/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.simulation;

/**
 * All energy related machine states. Usually, a machine implements a subset of
 * the defined states only.
 * 
 * @author dhampl
 * 
 */
public enum MachineState {
	/**
	 * Machine is off
	 */
	OFF,
	/**
	 * Machine is in standby (low ready)
	 */
	STANDBY,
	/**
	 * Machine is ready for processing (high ready)
	 */
	READY,
	/**
	 * Machine is on
	 */
	ON,
	/**
	 * Machine is processing
	 */
	PROCESS;
}
