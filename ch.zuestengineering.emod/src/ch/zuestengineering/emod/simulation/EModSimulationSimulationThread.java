/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.simulation;

/**
 * Implementation of a threaded simulation run
 * 
 * @author sizuest
 *
 */
public class EModSimulationSimulationThread extends Thread {

	@Override
	public void run() {
		EModSimulationRun.EModSimRun();
	}

}
