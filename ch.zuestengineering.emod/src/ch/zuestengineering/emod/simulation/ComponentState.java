/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.simulation;

/**
 * States of machine components. A component implements a subset of the defined
 * states only.
 * 
 * @author andreas
 * 
 */
public enum ComponentState {
	/**
	 * Component is turned on
	 */
	ON,
	/**
	 * Component is turned off
	 */
	OFF,
	/**
	 * Component is in standby
	 */
	STANDBY,
	/**
	 * Component is ready
	 */
	READY,
	/**
	 * Component is in periodic operation
	 */
	PERIODIC,
	/**
	 * Component is in controlled operation
	 */
	CONTROLLED;
}
