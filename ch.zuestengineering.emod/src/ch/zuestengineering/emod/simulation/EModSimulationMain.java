/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.simulation;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import ch.zuestengineering.emod.EModSession;
import ch.zuestengineering.emod.LifeCycle;
import ch.zuestengineering.emod.Machine;
import ch.zuestengineering.emod.Process;
import ch.zuestengineering.emod.femexport.FEMOutput;
import ch.zuestengineering.emod.gui.solver.SimulationLogger;
import ch.zuestengineering.emod.lca.LifeCycleResultFile;
import ch.zuestengineering.emod.model.MachineComponent;
import ch.zuestengineering.emod.model.fluid.FluidCircuit;
import ch.zuestengineering.emod.model.fluid.FluidCircuitSolver;
import ch.zuestengineering.emod.model.linking.IOConnection;
import ch.zuestengineering.emod.utils.ConfigReader;

/**
 * Main simulation class
 * 
 * IMPORTANT: Only one simulation can take place at once!
 * 
 * @author dhampl
 * 
 */
public class EModSimulationMain {

	private static Logger logger = Logger.getLogger(EModSimulationMain.class.getName());

	private double sampleperiod;
	private SimulationState machineState;
	private List<IOConnection> connectionList;
	private ArrayList<MachineComponent> machineComponentList;
	private List<ASimulationControl> simulators;

	private boolean doFEMOutput = true;
	private boolean doLCAnalysis = true;

	private static int progress = 0;
	private static String message = "";
	private static boolean running = false;
	private static boolean forcedStop = false;
	
	private int fsMaxIter;
	private double fsRelTol, fsMinFlowRate;
	private int failedUpdates;
	private MachineState mstate;

	/**
	 */
	public EModSimulationMain() {

		/* Read simulation states from file */
		machineState = new SimulationState();

		machineComponentList = null;
		connectionList = null;
		simulators = null;

		readConfig();
	}

	/**
	 * Returns the current progress of the simulation
	 * 
	 * @return
	 */
	public static int getProgress() {
		return Math.min(100, Math.max(0, progress));
	}

	/**
	 * Current simulation message
	 * 
	 * @return
	 */
	public static String getMessage() {
		return message;
	}

	/**
	 * Current running status
	 * 
	 * @return
	 */
	public static boolean getRunningStatus() {
		return running;
	}

	/**
	 * Indicates whether the forced stop option is set
	 * 
	 * @return
	 */
	public static boolean getForcedStop() {
		return forcedStop;
	}

	/**
	 * Require a forced stop If a forced stop is required, the current iteration is
	 * finished and the data is logged, befor the routine quits
	 * 
	 * @param b
	 */
	public static void setForcedStop(boolean b) {
		forcedStop = b;
	}

	private void readConfig() {
		String file = EModSession.getSimulationConfigPath();

		ConfigReader cr = null;
		try {
			cr = new ConfigReader(file);
			cr.ConfigReaderOpen();
			sampleperiod = cr.getDoubleValue("simulationPeriod");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Set machine component list
	 * 
	 * @param list
	 */
	public void setMachineComponentList(ArrayList<MachineComponent> list) {
		machineComponentList = list;
	}

	/**
	 * Set IO connection list
	 * 
	 * @param list
	 */
	public void setIOConnectionList(List<IOConnection> list) {
		connectionList = list;
	}

	/**
	 * Set list with object for input parameter generation.
	 * 
	 * @param list
	 */
	public void setInputparamObjectList(List<ASimulationControl> list) {
		simulators = list;
	}

	/**
	 * Set the process parameters 8time series) to the simulation control objects.
	 */
	public void setProcessParamsforSimulation() {
		if (null == simulators)
			return;

		for (ASimulationControl sc : simulators) {
			if (sc.getClass() == ProcessSimulationControl.class) {
				/* Set samples for process parameters */
				double[] samples = null;
				try {
					samples = Process.getInstance().getDoubleArray(sc.getName());
					((ProcessSimulationControl) sc).setProcessSamples(samples, Process.getTime());
				} catch (Exception ex) {
					Exception e = new Exception(
							"Error for process parameter '" + sc.getName() + "'\n" + ex.getMessage());
					e.printStackTrace();
				}

			} else if (sc instanceof GeometricKienzleSimulationControl) {
				/*
				 * Set and calculate the process moments for the Kienzle simulators
				 */
				((GeometricKienzleSimulationControl) sc).installKienzleInputParameters(Process.getInstance());
			}
		}
	}

	/**
	 * Do the simulation
	 */
	public void runSimulation() {

		/**
		 * Only one simulation can be runned at a time
		 */
		if (running) {
			logger.warning("Simulation is allready running");
			return;
		}

		setTitle("Simulation Run " + EModSession.getMachineName());
		setInfo("Model:   \t" + EModSession.getMachineConfig());
		setInfo("Scenario:\t" + EModSession.getSimulationConfig());
		setInfo("Process: \t" + EModSession.getProcessName());

		/* Set flags */
		running = true;
		forcedStop = false;

		double time;
		progress = 0;
		ArrayList<FluidCircuitSolver> fluidSolver;

		/* Check if all lists are defined: */
		if ((simulators == null) || (machineComponentList == null) || (connectionList == null)) {
			Exception ex = new Exception("Undefined simulation lists");
			ex.printStackTrace();
			return;
		}

		setSubTitle("Model Initilization");
		time = 0.0;

		/* Init simulation control objects. */
		setProgress("Preparing simulation controls");
		mstate = machineState.getState(time);		
		simulators.parallelStream().forEach(sc -> {
			sc.setState(mstate);
			sc.update();
		});
		
		setDone();
		setInfo("Simulation controls: " + simulators.size() + " element(s)");

		/* Init machine component objects */
		setProgress("Preparing machine components");
		for (MachineComponent mc : machineComponentList)
			mc.getComponent().preSimulation();
		setDone();
		setInfo("Machine components: " + machineComponentList.size() + " element(s)");

		/* Create solver for fluid circuits */
		setProgress("Initializing fluid circuit solver");

		fluidSolver = new ArrayList<>();

		// Find unique independent fluid circuits
		ArrayList<FluidCircuit> fluidPropertyLists = Machine.getFluidCircuits();
		int c = 0;
		for (FluidCircuit fc : fluidPropertyLists)
			fluidSolver.add(new FluidCircuitSolver("FluidCircuit " + (c++) + " (" + fc.getName() + ")", fc.getMembers(),
					Machine.getFluidConnectionList(fc)));
		setDone();

		setInfo("Fluid Solver: Found " + fluidSolver.size() + " independent fluid circuit(s).");

		/* Read fluid solver and output settings */
		setProgress("Loading solver settings");
		
		try {
			ConfigReader simulationConfigReader = new ConfigReader(EModSession.getSimulationConfigPath());
			simulationConfigReader.ConfigReaderOpen();

			fsMaxIter = simulationConfigReader.getValue("FluidSolver.MaxIter", 20);
			fsRelTol = simulationConfigReader.getValue("FluidSolver.RelTol", 1E-4);
			fsMinFlowRate = simulationConfigReader.getValue("FluidSolver.MinFlowRate", 1E-9);
			doFEMOutput = simulationConfigReader.getValue("Output.FEM", true);
			doLCAnalysis = simulationConfigReader.getValue("Output.LC", true);

		} catch (Exception e) {
			e.printStackTrace();
			fsMaxIter = 20;
			fsRelTol = 1E-4;
			fsMinFlowRate = 1E-9;
		}
		setDone();

		setProgress("Looking for steady state solution of the fluid circuits");
		/* Find steady state for fluid circuits */
		fluidSolver.parallelStream().forEach(fs -> {
				try {
					fs.solve(50 * fsMaxIter, fsRelTol, fsMinFlowRate);
				} catch (Exception e) {
					e.printStackTrace();
				}
			});
		
		setDone();

		for (FluidCircuitSolver fs : fluidSolver)
			setInfo(fs.getName() + ":\n\t Covergence reached after " + fs.getIterations() + " iteration(s)");

		setSubTitle("Simulation");

		/*
		 * Create simulation output file to store the simulation data.
		 */
		String path = EModSession.getResultFilePath();
		SimulationOutput simoutput = new SimulationOutput(path, machineComponentList, simulators, fluidSolver);

		String fempath = EModSession.getFEMExportFilePath();
		FEMOutput femoutput = new FEMOutput(fempath, machineComponentList);

		/*
		 * Time 0.0 s: All model and simulation outputs must be initiated.
		 */
		// Log data at time 0.0 s
		simoutput.logData(time, machineState.getState(0));
		/*
		 * Continue the simulation loop, while end time is not reached 
		 * and no forced stop is called. If a forced stop is called,
		 * the current iteration of this loop should be finished in order
		 * to produce a valid simulation output.
		 */
		while (time < machineState.simEndTime() & !forcedStop) {

			/* Increment actual simulation time */
			time += sampleperiod;

			/* Get next value from simulation control. */
			mstate = machineState.getState(time);

			// PARALLELIZATION: 
			simulators.parallelStream().forEach(sc -> {
				sc.setState(mstate);
				sc.update();
			});

			/* Set the inputs of all component models. */
			setInputs();

			/*
			 * Iterate all models. The outputs of all component models are updated.
			 */
			failedUpdates = 0;
			// PARALLELIZATION: 
			machineComponentList.parallelStream().forEach(mc -> {
				try {
					mc.getComponent().update();
					if (doFEMOutput)
						mc.getComponent().updateBoundaryConditions();

					
				} catch (Exception e) {
					failedUpdates++;
					logger.severe("EModSimulationMain.runSimulation(): update component not working for '"
							+ mc.getComponent().toString() +"'");
				}
			});

			/* Solve fluid circuits */
			// PARALLELIZATION: 
			fluidSolver.parallelStream().forEach(fs -> {
				try {
					fs.solve(fsMaxIter, fsRelTol, fsMinFlowRate);
				} catch (Exception e) {
					e.printStackTrace();
					logger.severe("EModSimulationMain.runSimulation(): solving of fluid circuit did not working for '"
							+ fs.toString() +"'");
				}
			});

			/* Log data of the actual time sample */
			simoutput.logData(time, mstate);

			if (doFEMOutput)
				femoutput.logData(time, mstate);

			/* Update Progress */
			setIterationInfo(time, machineComponentList.size()-failedUpdates, machineComponentList.size(), fluidSolver);
			progress = (int) (time / machineState.simEndTime() * 100);
		}

		/* Close simulation output */
		simoutput.close();
		femoutput.close();

		/* Reset flag */
		running = false;
		if(forcedStop)
			setInfo("Simulation aborted by user");
		else
			setInfo("Simulation completet");
		
		/* LC Analysis */
		if(doLCAnalysis) {
			computeLCA();
		}
	}
	
	/**
	 * computeLCA
	 */
	public static void computeLCA() {
		setSubTitle("Performing LC Analysis");
		
		setInfo("Unit:             \t" + LifeCycle.getEcoFactor());
		setInfo("Life-time:        \t" + LifeCycle.getScenario().getProductLifeTimeInYears()+" year(s)");
		setInfo("Service interval: \t" + LifeCycle.getScenario().getServiceIntervalInYears()+" year(s)");
		setInfo("Local service:    \t" + LifeCycle.getScenario().isLocalService());
		setInfo("Building class:   \t" + LifeCycle.getScenario().getBuildingClass());
		setInfo("Include HVAC:     \t" + LifeCycle.getScenario().getIncludeHVAC());
		setInfo("El. energy source:\t" + LifeCycle.getScenario().getElectricEnergySource());
		
		try {
			setProgress("Performing life cycle analysis");
			LifeCycle.fetch();
			(new LifeCycleResultFile(LifeCycle.getResultDetailed(), LifeCycle.getEcoFactor())).save();
			setDone();
		} catch(Exception e) {
			setFailed();
			e.printStackTrace();
		}
	}

	/**
	 * sets all inputs
	 */
	private void setInputs() {
		for (IOConnection ioc : connectionList) {
			ioc.update();
		}
	}

	/**
	 * Sets the simulation period for all components
	 */
	public void updateSimulationPeriod() {
		if (null != simulators)
			for (ASimulationControl sc : simulators)
				sc.setSimulationPeriod(sampleperiod);

		for (MachineComponent mc : machineComponentList)
			mc.getComponent().setSimulationTimestep(sampleperiod);
	}

	/**
	 * @return the simulation period [s]
	 */
	public double getSampleperiod() {
		return sampleperiod;
	}

	/**
	 * @param text
	 */
	private static void setInfo(String text) {
		message = text;
		SimulationLogger.info(text);
		logger.info(text);
	}

	/**
	 * @param text
	 */
	private static void setProgress(String text) {
		message = text;
		SimulationLogger.progress(text);
		logger.info(text);
	}

	private static void setIterationInfo(double timestep, int successfullUpdates, int totalUpdates,
			ArrayList<FluidCircuitSolver> fcSolvers) {
		String fcInfo = "";

		for (FluidCircuitSolver fc : fcSolvers)
			fcInfo += String.format("\t%2.3e (%2d iter.)", fc.getChangeRate(), fc.getIterations());

		SimulationLogger.info(String.format("%5.1f\t%4d/%-4d%s", timestep, successfullUpdates, totalUpdates, fcInfo));
	}

	/**
	 * @param text
	 */
	private static void setTitle(String text) {
		message = text;
		SimulationLogger.title(text);
		logger.info(text);
	}

	/**
	 * @param text
	 */
	private static void setSubTitle(String text) {
		message = text;
		SimulationLogger.subtitle(text);
		logger.info(text);
	}

	/**
	 * 
	 */
	private static void setDone() {
		message = "";
		SimulationLogger.done();
	}
	
	/**
	 * 
	 */
	private static void setFailed() {
		message = "";
		SimulationLogger.failed();
	}
}
