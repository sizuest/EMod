/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod;

import java.io.File;
import java.util.List;

import ch.zuestengineering.emod.model.MachineComponent;
import ch.zuestengineering.emod.model.ParameterCheckReport;
import ch.zuestengineering.emod.model.fluid.FluidCircuit;
import ch.zuestengineering.emod.model.linking.FluidContainer;
import ch.zuestengineering.emod.model.linking.IOConnection;
import ch.zuestengineering.emod.model.linking.IOContainer;
import ch.zuestengineering.emod.simulation.ASimulationControl;
import ch.zuestengineering.emod.simulation.ConfigCheckResult;
import ch.zuestengineering.emod.simulation.ConfigState;
import ch.zuestengineering.emod.simulation.DynamicState;

/**
 * Bundle of static functions to check a configuration. All functions should
 * return a {@link ConfigCheckResult}
 * 
 * @author simon
 *
 */
public class ConfigurationChecker {

	/**
	 * Performs all machine model relevant checks: 
	 * - checkMachineComponentConfigurations() 
	 * - checkSimulatorConfigurations() 
	 * - checkIOLinking() 
	 * - checkFluidCircuits()
	 * 
	 * @return
	 */
	public static ConfigCheckResult checkMachineConfig() {
		ConfigCheckResult result = new ConfigCheckResult();

		// Perform all machine relevant checks
		result.addAll(checkMachineComponentConfigurations());
		result.addAll(checkSimulatorConfigurations());
		result.addAll(checkIOLinking());
		result.addAll(checkFluidCircuits());

		return result;
	}

	/**
	 * Performs all simulation config relevant checks: 
	 * - checkInitialConditions() 
	 * - checkStateSequence()
	 * 
	 * @return
	 */
	public static ConfigCheckResult checkSimulationConfig() {
		ConfigCheckResult result = new ConfigCheckResult();

		// Perform all relevant checks
		result.addAll(checkInitialConditions());
		result.addAll(checkStateMap());

		return result;
	}

	/**
	 * Check machine components: 
	 * - Does the machine have at least one component? 
	 * - Do all components pass the parameter check?
	 * 
	 * @return
	 */
	public static ConfigCheckResult checkMachineComponentConfigurations() {
		ConfigCheckResult result = new ConfigCheckResult();

		if (Machine.getMachineComponentList().size() == 0)
			result.add(ConfigState.ERROR, "ComponetModels", "Model does not include any components");

		for (MachineComponent mc : Machine.getMachineComponentList()) {
			ParameterCheckReport report = mc.getComponent().checkConfigParams();
			if (report.hasErrors())
				result.add(ConfigState.ERROR, mc.getName() + ":" + mc.getComponent().getType(), report.getMessages());
		}

		return result;
	}

	/**
	 * Check simulator components
	 * 
	 * @return
	 */
	public static ConfigCheckResult checkSimulatorConfigurations() {
		ConfigCheckResult result = new ConfigCheckResult();
		/* At the moment, no error modes are present for the simulation.
		 * Hence, this function will return an empty {@link ConfigCheckResult} */
		return result;
	}

	/**
	 * Check IOLinking: 
	 * - Do linkings exist? 
	 * - Are targets linked multiple times?
	 * - Are targets not linked
	 * 
	 * @return
	 */
	public static ConfigCheckResult checkIOLinking() {
		ConfigCheckResult result = new ConfigCheckResult();

		int mc_in_iolist_cnt = 0;

		if (null == Machine.getInstance().getIOLinkList()) {
			result.add(ConfigState.WARNING, "IOLinking", "No connections defined!");
		} else
			for (MachineComponent mc : Machine.getMachineComponentList()) {
				for (IOContainer mcinput : mc.getComponent().getInputs()) {
					if (mcinput instanceof FluidContainer)
						continue;

					mc_in_iolist_cnt = 0;
					for (IOConnection iolink : Machine.getInstance().getIOLinkList()) {
						if (mcinput == iolink.getTarget()) {
							mc_in_iolist_cnt++;
						}
					}
					/*
					 * If the input is not linked, inform the user about the situation and
					 * report the default value which will be used.
					 */
					if (mc_in_iolist_cnt == 0) {
						result.add(ConfigState.WARNING, "IOLinking",
								"Input " + mc.getName() + "." + mcinput.getName() + " is not connected! Default value '" 
								+ mcinput.getValue() + (mcinput.getUnit().toString().equals("none") ? "" : " "+mcinput.getUnit().toString()) +"' will be used.");
					} 
					/*
					 * Inputs can not be linked multiple time
					 * Remark: If an input is linked multiple times, only the last link
					 * in the I/O-List with this input would be used
					 */
					else if (mc_in_iolist_cnt >= 2) {
						result.add(ConfigState.ERROR, "IOLinking",
								"Input " + mc.getName() + "." + mcinput.getName() + " is linked multiple times!");
					}
				}
			}

		return result;
	}

	/**
	 * Check fluid circuits:
	 * - Are all fluid circuits closed?
	 * - Is a source (e.g. a tank) defined (this is required to define a material)
	 * 
	 * @return
	 */
	public static ConfigCheckResult checkFluidCircuits() {
		ConfigCheckResult result = new ConfigCheckResult();
		
		for(FluidCircuit fc: Machine.getFluidCircuits()) {
			if(!fc.isClosed())
				result.add(ConfigState.ERROR, "FluidCircuits", "Fluid circuit " + fc.getName() + " is not closed!");
			if(!fc.hasSource())
				result.add(ConfigState.ERROR, "FluidCircuits", "Fluid circuit " + fc.getName() + " has no source!");
		}

		return result;
	}

	/**
	 * Check if all initial conditions are set
	 * 
	 * @return
	 */
	public static ConfigCheckResult checkInitialConditions() {
		ConfigCheckResult result = new ConfigCheckResult();

		for(DynamicState ds: Machine.getDynamicStatesList())
			if(!Double.isFinite(ds.getInitialValue()))
				result.add(ConfigState.WARNING, "InitialConditions", "Initial condition " + ds.getName() + " is not defined!");
		
		return result;
	}

	/**
	 * Check the state sequence
	 * 
	 * @return
	 */
	public static ConfigCheckResult checkStateMap() {
		ConfigCheckResult result = new ConfigCheckResult();

		// At least one state must be set
		if (States.getStateCount() == 0)
			result.add(ConfigState.ERROR, "StateMap", "State map does not include any state definition");

		// Durations of length 0 should be avoided
		for (int i = 0; i < States.getStateCount(); i++)
			if (States.getDuration(i) == 0)
				result.add(ConfigState.WARNING, "StateMap", "State map does contain states with duration 0");

		// Default message
		if (result.getMessages().size() == 0)
			result.add(ConfigState.OK, "StateMap", "Test passed");

		return result;
	}

	/**
	 * Check process file
	 * 
	 * @return
	 */
	public static ConfigCheckResult checkProcess() {
		ConfigCheckResult result = new ConfigCheckResult();

		// Load the process first
		Process.loadProcess(EModSession.getProcessName());

		List<ASimulationControl> simulators = Machine.getInstance().getVariableInputObjectList();

		// For each simulator, the process file must contain a value vector
		for (ASimulationControl sc : simulators)
			if (!(Process.getVariableNames().contains(sc.getName())))
				result.add(ConfigState.ERROR, "Process",
						"Process configuration does not includes a value vector for '" + sc.getName() + "'");

		// At least one time step is required
		if (Process.getTime().length == 0)
			result.add(ConfigState.ERROR, "Process", "Process configuration does not include any time step");
		else if (Process.getTime().length == 1)
			result.add(ConfigState.WARNING, "Process", "Process configuration includes only one time step");

		// Default message
		if (result.getMessages().size() == 0)
			result.add(ConfigState.OK, "Process", "Test passed");

		return result;
	}

	/**
	 * Get the results of the checks for the currently selected configuration
	 * 
	 * @return
	 */
	public static ConfigCheckResult checkResults() {
		return checkResults(EModSession.getMachineConfig(), EModSession.getSimulationConfig(),
				EModSession.getProcessName());
	}

	/**
	 * Returns the check results for the stated configuration
	 * 
	 * @param machine
	 * @param simCfg
	 * @param process
	 * @return
	 */
	public static ConfigCheckResult checkResults(String machine, String simCfg, String process) {
		ConfigCheckResult result = new ConfigCheckResult();

		File resultFile = new File(EModSession.getResultFilePath(machine, simCfg, process));

		if (!resultFile.exists())
			result.add(ConfigState.ERROR, "Result", "Result file '" + resultFile.getPath() + "' does not exist");
		else {
			File machineFile, simFile, processFile;
			machineFile = new File(EModSession.getMachineConfigPath(machine));
			simFile = new File(EModSession.getSimulationConfigPath(simCfg));
			processFile = new File(EModSession.getProcessConfigPath(simCfg, process));

			if (machineFile.lastModified() > resultFile.lastModified() + 500)
				result.add(ConfigState.WARNING, "Result", "Result file is older than the machine configuration");

			if (simFile.lastModified() > resultFile.lastModified() + 500)
				result.add(ConfigState.WARNING, "Result", "Result file is older than the simulation configuration");

			if (processFile.lastModified() > resultFile.lastModified() + 500)
				result.add(ConfigState.WARNING, "Result", "Result file is older than the process configuration");

		}

		return result;
	}

	/**
	 * Checks, if the fem-bc results are available for the stated configuation
	 * 
	 * @param machine
	 * @param simCfg
	 * @param process
	 * @return
	 */
	public static ConfigCheckResult checkFEM(String machine, String simCfg, String process) {
		ConfigCheckResult result = new ConfigCheckResult();

		File resultFile = new File(EModSession.getFEMExportFilePath(machine, simCfg, process));

		if (!resultFile.exists())
			result.add(ConfigState.ERROR, "Result", "Result file '" + resultFile.getPath() + "' does not exist");
		else {
			File machineFile, simFile, processFile;
			machineFile = new File(EModSession.getMachineConfigPath(machine));
			simFile = new File(EModSession.getSimulationConfigPath(simCfg));
			processFile = new File(EModSession.getProcessConfigPath(simCfg, process));

			if (machineFile.lastModified() > resultFile.lastModified())
				result.add(ConfigState.WARNING, "FEM Export", "Result file is older than the machine configuration");

			if (simFile.lastModified() > resultFile.lastModified())
				result.add(ConfigState.WARNING, "FEM Export", "Result file is older than the simulation configuration");

			if (processFile.lastModified() > resultFile.lastModified())
				result.add(ConfigState.WARNING, "FEM Export", "Result file is older than the process configuration");

		}

		return result;
	}

	/**
	 * checkLC
	 * 
	 * @param machine 
	 * @param simCfg 
	 * @param process 
	 * @return
	 */
	public static ConfigCheckResult checkLC(String machine, String simCfg, String process) {
		ConfigCheckResult result = new ConfigCheckResult();

		File resultFile = new File(EModSession.getLCResultFilePath(machine, simCfg, process));

		if (!resultFile.exists())
			result.add(ConfigState.ERROR, "Result", "Result file '" + resultFile.getPath() + "' does not exist");
		else {
			File machineFile, simFile, processFile;
			machineFile = new File(EModSession.getMachineConfigPath(machine));
			simFile = new File(EModSession.getSimulationConfigPath(simCfg));
			processFile = new File(EModSession.getProcessConfigPath(simCfg, process));

			if (machineFile.lastModified() > resultFile.lastModified())
				result.add(ConfigState.WARNING, "LC Result", "Result file is older than the machine configuration");

			if (simFile.lastModified() > resultFile.lastModified())
				result.add(ConfigState.WARNING, "LC Result", "Result file is older than the simulation configuration");

			if (processFile.lastModified() > resultFile.lastModified())
				result.add(ConfigState.WARNING, "LC Result", "Result file is older than the process configuration");

		}

		return result;
	}

}
