/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.model;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import ch.zuestengineering.emod.femexport.BoundaryCondition;
import ch.zuestengineering.emod.lca.inventory.MaterialAmount;
import ch.zuestengineering.emod.lca.inventory.RessourceDemand;
import ch.zuestengineering.emod.lca.inventory.Transportation;
import ch.zuestengineering.emod.model.fluid.Floodable;
import ch.zuestengineering.emod.model.lc.LCProperties;
import ch.zuestengineering.emod.model.linking.IOContainer;
import ch.zuestengineering.emod.model.parameters.ParameterSet;
import ch.zuestengineering.emod.model.units.SiUnit;
import ch.zuestengineering.emod.simulation.DynamicState;
import ch.zuestengineering.emod.utils.PropertiesHandler;

/**
 * Abstract machine component.
 * 
 * @author dhampl
 * 
 */
@XmlRootElement
public abstract class APhysicalComponent {

	/* Inputs and Outputs */
	protected List<IOContainer> inputs = new ArrayList<IOContainer>();
	protected List<IOContainer> outputs = new ArrayList<IOContainer>();
	/* Boundary conditions */
	protected List<BoundaryCondition> boundaryConditions = new ArrayList<BoundaryCondition>();
	/* Parametrization */
	@XmlElement
	protected ParameterSet parameterSet = new ParameterSet();
	/* LC properties */
	@XmlElement
	protected LCProperties lcProperties;
	/* Dynamic states */
	protected ArrayList<DynamicState> dynamicStates;
	/* Time step */
	protected double timestep;
	protected Logger logger = Logger.getLogger(getModelType());
	/* Library link */
	@XmlElement
	protected boolean useLibraryLink = true;

	/**
	 * @param id
	 * @param value
	 */
	public void setInput(int id, double value) {
		inputs.get(id).setValue(value);
	}

	/**
	 * Returns the value of the output with number 'id'
	 * 
	 * @param id
	 * @return value of the output
	 */
	public double getOutput(int id) {
		return outputs.get(id).getValue();
	}

	/**
	 * Returns the value of the input with number 'id'
	 * 
	 * @param id
	 * @return value of the input
	 */
	public double getInput(int id) {
		return inputs.get(id).getValue();
	}

	/**
	 * Returns the list of inputs
	 * 
	 * @return List of {@link IOContainer}
	 */
	public List<IOContainer> getInputs() {
		return inputs;
	}

	/**
	 * Returns the list of outputs
	 * 
	 * @return List of {@link IOContainer}
	 */
	public List<IOContainer> getOutputs() {
		return outputs;
	}

	/**
	 * Returns the input of the given name, returns null if the input does not exist
	 * 
	 * @param name
	 * @return {@link IOContainer}
	 */
	public IOContainer getInput(String name) {
		IOContainer temp = null;
		for (IOContainer ioc : inputs) {
			if (ioc.getName().equals(name)) {
				temp = ioc;
				break;
			}
		}
		return temp;
	}

	/**
	 * Returns the output of the given name, returns null if the output does not
	 * exist
	 * 
	 * @param name
	 * @return {@link IOContainer}
	 */
	public IOContainer getOutput(String name) {
		IOContainer temp = null;
		for (IOContainer ioc : outputs) {
			if (ioc.getName().equals(name)) {
				temp = ioc;
				break;
			}
		}
		return temp;
	}

	/**
	 * Returns the array of boundary conditions
	 * 
	 * @return List of {@link BoundaryCondition}
	 */
	public List<BoundaryCondition> getBoundaryConditions() {
		return this.boundaryConditions;
	}

	/**
	 * Looks through the list of BC and returns the one with the given name. If no
	 * match occurs, null is returned.
	 * 
	 * @param name
	 * @return
	 */
	public BoundaryCondition getBoundaryCondition(String name) {
		BoundaryCondition temp = null;
		for (BoundaryCondition bc : boundaryConditions) {
			if (bc.getName().equals(name)) {
				temp = bc;
				break;
			}
		}

		return temp;
	}

	/**
	 * abstract method to return the Type of the Component needs to be overriden by
	 * components
	 * 
	 * @return String of the Fluid
	 */
	public abstract String getType();

	/**
	 * abstract method to update the Component needs to be overriden by components
	 */
	public abstract void update();

	/**
	 * abstract method to updated the boundary conditions of the component
	 */
	public abstract void updateBoundaryConditions();

	/**
	 * To be executed before Simulation
	 */
	public void preSimulation() {
		/* Initizalize dynamic states */
		if (null == dynamicStates)
			return;
		for (DynamicState ic : dynamicStates)
			try {
				ic.loadInitialCondition();
			} catch (Exception e) {
				e.printStackTrace();
			}

		/* Flood circuit */
		if (this instanceof Floodable)
			((Floodable) this).flood();
	}

	/**
	 * Sets the sample time for the model
	 * 
	 * @param timestep sample time in seconds
	 */
	public void setSimulationTimestep(double timestep) {
		this.timestep = timestep;
		if (dynamicStates != null)
			for (DynamicState ds : dynamicStates)
				ds.setTimestep(timestep);

	}

	/**
	 * Sets the type of the component according to the argument
	 * 
	 * @param type
	 */
	public abstract void setType(String type);

	/**
	 * Returns the type of the model used, referring to the folders in the
	 * MachineComponentDB. If not over-written, this is the class name.
	 * 
	 * @return Model Type
	 */
	public String getModelType() {
		return this.getClass().getSimpleName();
	}

	/**
	 * Adds a new initial condition to the set
	 * 
	 * @param name
	 * @param unit
	 * @return the created {@link DynamicState}
	 */
	public DynamicState newDynamicState(String name, SiUnit unit) {
		DynamicState ic = new DynamicState(name, unit);
		dynamicStates.add(ic);

		return ic;
	}

	/**
	 * Returns the {@link DynamicState} with the given name and parent
	 * 
	 * @param name
	 * @return {@link DynamicState}
	 */
	public DynamicState getDynamicState(String name) {
		for (DynamicState ds : this.getDynamicStateList())
			if (ds.getName().equals(name))
				return ds;

		return null;
	}

	/**
	 * List of all initial conditions in this set
	 * 
	 * @return {@link DynamicState}
	 */
	public ArrayList<DynamicState> getDynamicStateList() {
		return dynamicStates;
	}

	/**
	 * Sets the parent of all initial conditions
	 * 
	 * @param parent
	 */
	public void setDynamicStateParent(String parent) {
		if (null == parent) {
			return;
		}
		if (dynamicStates != null)
			for (DynamicState ic : this.getDynamicStateList())
				ic.setParent(parent);
	}

	/**
	 * Perfors a check of the parameters and returns a report
	 * 
	 * @return
	 */
	public abstract ParameterCheckReport checkConfigParams();
	
	/**
	 * Load all parameters
	 */
	public void loadParameters() {
		loadTechnicalParameters();
		loadLCProperties();
	}
	
	/**
	 * Save all parameters
	 */
	public void saveParameters() {
		saveTechnicalParameters();
		saveLCProperties();
	}

	/**
	 * Loads the parameters
	 */
	public void loadTechnicalParameters() {
		setLibraryLink(libraryHasTechnicalConfig() & useLibraryLink);
		
		if(hasLibraryLink())
			this.parameterSet = ParameterSet.load(getParameterFile());
		else
			logger.info(getModelType()+": "+getType()+": Skipped loading of parameter file, since library link is deactivated or file is missing!");

		/*
		 * Needed for old parameter files using {@link ComponentConfigReader}
		 */
		if (null == parameterSet)
			updateParameterSet();
	}

	/**
	 * Saves the parameter set
	 */
	public void saveTechnicalParameters() {
		if(hasLibraryLink())
			this.parameterSet.save(getParameterFile());
		else
			logger.info(getModelType()+": "+getType()+": Skipped saving of parameter file, since library link is deactivated!");
	}

	/**
	 * Loads the lc properties
	 */
	public void loadLCProperties() {
		if(hasLibraryLink())
			this.lcProperties = LCProperties.load(getLCFile());
		else
			logger.info(getModelType()+": "+getType()+": Skipped loading of life-cycle parameters, since library link is deactivated!");
		
		if(null==this.lcProperties)
			this.lcProperties = new LCProperties();
	}

	/**
	 * Saves the lc proeprties
	 */
	public void saveLCProperties() {
		if(hasLibraryLink())
			this.lcProperties.save(getLCFile());
		else
			logger.info(getModelType()+": "+getType()+": Skipped saving of life-cycle parameters, since library link is deactivated!");
	}

	/**
	 * Needed to update old parameter sets using 'ComponentConfigReader' to the new
	 * data structure 'ParameterSet'
	 * 
	 * This function should be removed in future versions
	 */
	@Deprecated
	protected abstract void updateParameterSet();

	/**
	 * @return the parameterSet
	 */
	public ParameterSet getParameterSet() {
		return parameterSet;
	}

	/**
	 * @param parameterSet the parameterSet to set
	 */
	@XmlTransient
	public void setParameterSet(ParameterSet parameterSet) {
		this.parameterSet = parameterSet;
	}

	/**
	 * Returns the path to the model;
	 * 
	 * @return
	 */
	public String getParameterFile() {
		return getParameterFile(getModelType(), getType());
	}

	/**
	 * Returns the path to the model;
	 * 
	 * @return
	 */
	public String getLCFile() {
		return getLCFile(getModelType(), getType());
	}

	/**
	 * Returns the path to the model;
	 * 
	 * @param model
	 * @param type
	 * @return
	 */
	public static String getParameterFile(String model, String type) {
		String filePath;
		String path_prefix = PropertiesHandler.getProperty("app.MachineComponentDBPathPrefix");
		if (path_prefix == null)
			path_prefix = "MachineComponentDB"; // set default path prefix
		filePath = path_prefix + "/" + model + "/" + model + "_" + type + ".xml";

		return filePath;
	}

	/**
	 * Returns the path to the model;
	 * 
	 * @param model
	 * @param type
	 * @return
	 */
	public static String getLCFile(String model, String type) {
		String filePath;
		String path_prefix = PropertiesHandler.getProperty("app.MachineComponentDBPathPrefix");
		if (path_prefix == null)
			path_prefix = "MachineComponentDB"; // set default path prefix
		filePath = path_prefix + "/" + model + "/" + model + "_" + type + "_lc.xml";

		return filePath;
	}

	/**
	 * @return the materialDemand
	 */
	public List<RessourceDemand> getRessourceDemand() {
		return getLcProperties().getRessourceDemand();
	}

	/**
	 * @return the materialAmount
	 */
	public List<MaterialAmount> getMaterialAmount() {
		return getLcProperties().getMaterialAmount();
	}

	/**
	 * @return the transportation list
	 */
	public List<Transportation> getTransportation() {
		return getLcProperties().getTransportation();
	}

	public String toString() {
		return getModelType() + "_" + getType();
	}

	/**
	 * @return the lcProperties
	 */
	@XmlTransient
	public LCProperties getLcProperties() {
		if (null == lcProperties)
			loadLCProperties();

		return lcProperties;
	}

	/**
	 * @param lcProperties the lcProperties to set
	 */
	public void setLcProperties(LCProperties lcProperties) {
		lcProperties.updateMasses();
		this.lcProperties = lcProperties;
	}

	/**
	 * Returns the mass of the component based on its material list
	 * 
	 * @return the mass in kg
	 */
	public double getMass() {
		double mass = 0;

		for (MaterialAmount m : getMaterialAmount())
			mass += m.getAmount();

		return mass;
	}

	/**
	 * Returns the heat capacity of the component based on its material list
	 * 
	 * @return the heat capacity in J/K
	 */
	public double getStructureHeatCapacity() {
		double c = 0;

		if (getMaterialAmount().size() == 0)
			return Double.NaN;

		for (MaterialAmount m : getMaterialAmount())
			c += m.getMaterial().getHeatCapacity(293.15, 1e5) * m.getAmount();

		return c;

	}

	/**
	 * Returns the specific heat capacity of the component based on its material
	 * list
	 * 
	 * @return the heat capacity in J/kg/K
	 */
	public double getSpecificHeatCapacity() {
		return getStructureHeatCapacity() / getMass();
	}

	/**
	 * @return the useLibraryLink
	 */
	public boolean hasLibraryLink() {
		return useLibraryLink;
	}

	/**
	 * @param useLibraryLink the useLibraryLink to set
	 */
	@XmlTransient
	public void setLibraryLink(boolean useLibraryLink) {
		this.useLibraryLink = useLibraryLink;
	}
	
	private boolean libraryHasTechnicalConfig() {
		return (new File(getParameterFile())).exists();
	}
}
