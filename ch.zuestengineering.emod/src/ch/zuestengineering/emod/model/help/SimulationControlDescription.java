/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.model.help;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.Writer;
import java.util.ArrayList;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import ch.zuestengineering.emod.Machine;
import ch.zuestengineering.emod.help.AModelHelp;
import ch.zuestengineering.emod.help.HelpFormater;
import ch.zuestengineering.emod.help.ValueDescription;
import ch.zuestengineering.emod.model.units.SiUnit;
import ch.zuestengineering.emod.simulation.ASimulationControl;
import ch.zuestengineering.emod.utils.PropertiesHandler;
import ch.zuestengineering.emod.utils.SimulationConfigReader;

/**
 * Implements the description of a simulation control based on the AModelHelp
 * class
 * 
 * @author Simon Z�st
 *
 */
@XmlRootElement
public class SimulationControlDescription extends AModelHelp {
	@XmlElement
	protected ArrayList<ValueDescription> outputDescriptions;
	@XmlElement
	protected ArrayList<ValueDescription> parameterDescriptions;

	/**
	 * Constructor for unmarshaller
	 */
	public SimulationControlDescription() {
	}

	/**
	 * @param u
	 * @param parent
	 */
	public void afterUnmarshal(Unmarshaller u, Object parent) {
		init();
	}

	/**
	 * @param type
	 */
	public SimulationControlDescription(String type) {
		this.type = type;

		outputDescriptions = new ArrayList<ValueDescription>();
		parameterDescriptions = new ArrayList<ValueDescription>();

		title = "";
		content = "";

		init();
	}

	private void init() {
	}

	/**
	 * @return the outputDescriptions
	 */
	public ArrayList<ValueDescription> getOutputDescriptions() {
		return outputDescriptions;
	}

	/**
	 * @return the parameterDescriptions
	 */
	public ArrayList<ValueDescription> getParameterDescriptions() {
		return parameterDescriptions;
	}

	/**
	 * Loads a saved simulation control description for the given type
	 * 
	 * @param type
	 * @return
	 */
	public static SimulationControlDescription load(String type) {
		SimulationControlDescription help = null;
		try {
			JAXBContext context = JAXBContext.newInstance(SimulationControlDescription.class);
			Unmarshaller um = context.createUnmarshaller();
			help = (SimulationControlDescription) um.unmarshal(new FileReader(getPath(type)));
		} catch (Exception e) {
			help = new SimulationControlDescription(type);
		}

		return help;
	}

	/**
	 * Saves the simulation control description for the given model type
	 * 
	 * @param type
	 */
	public void save(String type) {
		try {
			JAXBContext context = JAXBContext.newInstance(SimulationControlDescription.class);
			Marshaller m = context.createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

			File path = new File(getPath(type));
			path.getParentFile().mkdirs();
			path.createNewFile();

			Writer w = new FileWriter(path);
			m.marshal(this, w);
			w.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Returns the path of the model documentation for the given model type
	 * 
	 * @param type
	 * @return
	 */
	public static String getPath(String type) {
		String filePath;

		String path_prefix = PropertiesHandler.getProperty("app.MachineComponentDBPathPrefix");
		filePath = path_prefix + "/SimulationControl/doc/doc_en_US_" + type + ".xml";

		return filePath;
	}

	/**
	 * Merges the parameters of the simulation control and the description Model ->
	 * Name+Unit Desciption -> Desciption based on name
	 */
	private void getParametersFromModel() {
		SimulationConfigReader paramFile;
		try {
			paramFile = new SimulationConfigReader(type, "Example");
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}

		ArrayList<String> params, descr;

		params = paramFile.getKeys();
		descr = new ArrayList<String>();

		for (ValueDescription vd : getParameterDescriptions())
			descr.add(vd.getName());

		for (String s : params) {
			int idx = descr.indexOf(s);

			String unit = "";
			try {
				unit = paramFile.getPhysicalValue(s).getUnit().toHTML();
			} catch (Exception e) {
			}

			if (idx >= 0)
				getParameterDescriptions().get(idx).setUnit(unit);
			else
				getParameterDescriptions().add(new ValueDescription(s, unit, ""));
		}
	}

	private void getOuputsFromModel() {
		ASimulationControl mdl;
		try {
			mdl = Machine.createNewInputObject("Example", type, new SiUnit());
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}

		if (mdl == null)
			return;

		for (ValueDescription vd : outputDescriptions) {
			if (vd.getName().equals(mdl.getOutput().getName()))
				return;
		}

		outputDescriptions.add(new ValueDescription(mdl.getOutput().getName(), "VAR", "todo"));

	}

	/**
	 * Returns the decription of the parameter with the stated name
	 * 
	 * @param name
	 * @return
	 */
	public String getParameterDescription(String name) {
		return getValueDescription(getParameterDescriptions(), name);
	}

	private String getValueDescription(ArrayList<ValueDescription> src, String name) {
		for (ValueDescription vd : src)
			if (vd.getName().equals(name))
				return vd.getDescription();

		return "Description not available";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.zuestengineering.emod.help.AModelHelp#getHTMLContent()
	 */
	@Override
	public String getHTMLContent() {
		String ret = "";

		ret += HelpFormater.formatParagraph("Model description", content);
		ret += HelpFormater.formatParagraph("Outputs", outputDescriptions);
		ret += HelpFormater.formatParagraph("Parameters", parameterDescriptions);
		return ret;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.zuestengineering.emod.help.AModelHelp#compareToModel()
	 */
	@Override
	public void compareToModel() {
		getParametersFromModel();
		getOuputsFromModel();
	}

}
