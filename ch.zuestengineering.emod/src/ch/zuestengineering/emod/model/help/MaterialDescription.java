/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.model.help;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.Writer;
import java.util.ArrayList;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import ch.zuestengineering.emod.help.AModelHelp;
import ch.zuestengineering.emod.help.HelpFormater;
import ch.zuestengineering.emod.help.ValueDescription;
import ch.zuestengineering.emod.model.material.Material;
import ch.zuestengineering.emod.model.material.MaterialType;
import ch.zuestengineering.emod.utils.MaterialConfigReader;
import ch.zuestengineering.emod.utils.PropertiesHandler;

/**
 * @author Simon Z�st
 *
 */
@XmlRootElement
public class MaterialDescription extends AModelHelp {
	@XmlElement
	protected ArrayList<ValueDescription> parameterDescriptions;

	/**
	 * Constructor for unmarshaller
	 */
	public MaterialDescription() {
	}

	/**
	 * @param u
	 * @param parent
	 */
	public void afterUnmarshal(Unmarshaller u, Object parent) {
		init();
	}

	/**
	 * @param type
	 */
	public MaterialDescription(String type) {
		this.type = type;

		parameterDescriptions = new ArrayList<ValueDescription>();
		title = "";
		content = "";

		init();
	}

	private void init() {
	}

	/**
	 * @return the parameterDescriptions
	 */
	public ArrayList<ValueDescription> getParameterDescriptions() {
		return parameterDescriptions;
	}

	/**
	 * @param type
	 * @return
	 */
	public static MaterialDescription load(String type) {
		MaterialDescription help = null;
		try {
			JAXBContext context = JAXBContext.newInstance(MaterialDescription.class);
			Unmarshaller um = context.createUnmarshaller();
			help = (MaterialDescription) um.unmarshal(new FileReader(getPath(type)));
		} catch (Exception e) {
			help = new MaterialDescription(type);
		}

		return help;
	}

	/**
	 * Saves the model description for the given model type
	 * 
	 * @param type
	 */
	public void save(String type) {
		try {
			JAXBContext context = JAXBContext.newInstance(MaterialDescription.class);
			Marshaller m = context.createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

			File path = new File(getPath(type));
			path.getParentFile().mkdirs();
			path.createNewFile();

			Writer w = new FileWriter(path);
			m.marshal(this, w);
			w.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Merges the parameters of the model and the description Model -> Name+Unit
	 * Desciption -> Desciption based on name
	 */
	private void getParametersFromModel() {

		// Create Material example
		new Material(MaterialType.valueOf(type), type + "Example");

		MaterialConfigReader paramFile;
		try {
			paramFile = new MaterialConfigReader("Material", type + "Example");
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}

		ArrayList<String> params, descr;

		params = paramFile.getKeys();
		descr = new ArrayList<String>();

		for (ValueDescription vd : getParameterDescriptions())
			descr.add(vd.getName());

		for (String s : params) {
			if (s.equals("MaterialType"))
				continue;

			int idx = descr.indexOf(s);

			String unit = "";
			try {
				unit = paramFile.getPhysicalValue(s).getUnit().toHTML();
			} catch (Exception e) {
			}

			if (idx >= 0)
				getParameterDescriptions().get(idx).setUnit(unit);
			else
				getParameterDescriptions().add(new ValueDescription(s, unit, ""));
		}
	}

	/**
	 * Returns the path of the model documentation for the given model type
	 * 
	 * @param type
	 * @return
	 */
	public static String getPath(String type) {
		String filePath;

		String path_prefix = PropertiesHandler.getProperty("app.MaterialDBPathPrefix");
		filePath = path_prefix + "/doc/doc_en_US_" + type + ".xml";

		return filePath;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.zuestengineering.emod.help.AModelHelp#getHTMLContent()
	 */
	@Override
	public String getHTMLContent() {
		String ret = "";

		ret += HelpFormater.formatParagraph("Model description", content);
		ret += HelpFormater.formatParagraph("Parameters", parameterDescriptions);

		return ret;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.zuestengineering.emod.help.AModelHelp#compareToModel()
	 */
	@Override
	public void compareToModel() {
		getParametersFromModel();
	}
}
