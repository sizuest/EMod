/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.model.help;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import ch.zuestengineering.emod.Machine;
import ch.zuestengineering.emod.help.AModelHelp;
import ch.zuestengineering.emod.help.HelpFormater;
import ch.zuestengineering.emod.help.ValueDescription;
import ch.zuestengineering.emod.model.APhysicalComponent;
import ch.zuestengineering.emod.model.linking.IOContainer;
import ch.zuestengineering.emod.utils.ComponentConfigReader;
import ch.zuestengineering.emod.utils.PropertiesHandler;

/**
 * Implements the description of a model based on the AModelHelp class
 * 
 * Defintion: A Model description consists of the following elements: - inputs:
 * List and description of all model inputs - outputs: List and description of
 * all model outputs - params: List and description of all model parameters
 * (from the library)
 * 
 * @author simon
 *
 */
@XmlRootElement
public class ModelDescription extends AModelHelp {
	@XmlElement
	protected ArrayList<ValueDescription> inputDescriptions;
	@XmlElement
	protected ArrayList<ValueDescription> outputDescriptions;
	@XmlElement
	protected ArrayList<ValueDescription> parameterDescriptions;

	/**
	 * Constructor for unmarshaller
	 */
	public ModelDescription() {
	}

	/**
	 * @param u
	 * @param parent
	 */
	public void afterUnmarshal(Unmarshaller u, Object parent) {
		init();
	}

	/**
	 * @param type
	 */
	public ModelDescription(String type) {
		this.type = type;

		inputDescriptions = new ArrayList<ValueDescription>();
		outputDescriptions = new ArrayList<ValueDescription>();
		parameterDescriptions = new ArrayList<ValueDescription>();

		title = "";
		content = "";

		init();
	}

	private void init() {
	}

	/**
	 * @return the inputDescriptions
	 */
	public ArrayList<ValueDescription> getInputDescriptions() {
		return inputDescriptions;
	}

	/**
	 * @return the outputDescriptions
	 */
	public ArrayList<ValueDescription> getOutputDescriptions() {
		return outputDescriptions;
	}

	/**
	 * @return the parameterDescriptions
	 */
	public ArrayList<ValueDescription> getParameterDescriptions() {
		return parameterDescriptions;
	}

	/**
	 * Loads a saved mode description for the given type
	 * 
	 * @param type
	 * @return
	 */
	public static ModelDescription load(String type) {
		ModelDescription help = null;
		try {
			JAXBContext context = JAXBContext.newInstance(ModelDescription.class);
			Unmarshaller um = context.createUnmarshaller();
			help = (ModelDescription) um.unmarshal(new FileReader(getPath(type)));
		} catch (Exception e) {
			help = new ModelDescription(type);
		}

		return help;
	}

	/**
	 * Saves the model description for the given model type
	 * 
	 * @param type
	 */
	public void save(String type) {
		try {
			JAXBContext context = JAXBContext.newInstance(ModelDescription.class);
			Marshaller m = context.createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

			File path = new File(getPath(type));
			path.getParentFile().mkdirs();
			path.createNewFile();

			Writer w = new FileWriter(path);
			m.marshal(this, w);
			w.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Returns the path of the model documentation for the given model type
	 * 
	 * @param type
	 * @return
	 */
	public static String getPath(String type) {
		String filePath;

		String path_prefix = PropertiesHandler.getProperty("app.MachineComponentDBPathPrefix");
		filePath = path_prefix + "/" + type + "/doc/doc_en_US_" + type + ".xml";

		return filePath;
	}

	/**
	 * Merges the parameters of the model and the description Model -> Name+Unit
	 * Desciption -> Desciption based on name
	 */
	private void getParametersFromModel() {
		ComponentConfigReader paramFile;
		try {
			paramFile = new ComponentConfigReader(type, "Example");
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}

		ArrayList<String> params, descr;

		params = paramFile.getKeys();
		descr = new ArrayList<String>();

		for (ValueDescription vd : getParameterDescriptions())
			descr.add(vd.getName());

		for (String s : params) {
			int idx = descr.indexOf(s);

			String unit = "";
			try {
				unit = paramFile.getPhysicalValue(s).getUnit().toHTML();
			} catch (Exception e) {
			}

			if (idx >= 0)
				getParameterDescriptions().get(idx).setUnit(unit);
			else
				getParameterDescriptions().add(new ValueDescription(s, unit, ""));
		}
	}

	private void getInputsFromModel() {
		APhysicalComponent mdl = Machine.createNewMachineComponent(type, "Example");

		if (mdl == null)
			return;

		getIOFromModel(mdl.getInputs(), getInputDescriptions());
	}

	private void getOuputsFromModel() {
		APhysicalComponent mdl = Machine.createNewMachineComponent(type, "Example");

		if (mdl == null)
			return;

		getIOFromModel(mdl.getOutputs(), getOutputDescriptions());
	}

	private void getIOFromModel(List<IOContainer> src, ArrayList<ValueDescription> tar) {
		ArrayList<String> sigs, units, descr;

		sigs = new ArrayList<String>();
		units = new ArrayList<String>();
		for (IOContainer ic : src)
			if (!ic.hasReference()) {
				sigs.add(ic.getName());
				units.add(ic.getUnit().toHTML());
			}

		descr = new ArrayList<String>();
		for (ValueDescription vd : tar)
			descr.add(vd.getName());

		for (int i = 0; i < sigs.size(); i++) {
			int idx = descr.indexOf(sigs.get(i));

			String unit = "";
			try {
				unit = units.get(i);
			} catch (Exception e) {
			}

			if (idx >= 0)
				tar.get(idx).setUnit(unit);
			else
				tar.add(new ValueDescription(sigs.get(i), unit, ""));
		}

	}

	/**
	 * Returns the decription of the parameter with the stated name
	 * 
	 * @param name
	 * @return
	 */
	public String getParameterDescription(String name) {
		return getValueDescription(getParameterDescriptions(), name);
	}

	private String getValueDescription(ArrayList<ValueDescription> src, String name) {
		for (ValueDescription vd : src)
			if (vd.getName().equals(name))
				return vd.getDescription();

		return "Description not available";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.zuestengineering.emod.help.AModelHelp#getHTMLContent()
	 */
	@Override
	public String getHTMLContent() {
		String ret = "";

		ret += HelpFormater.formatParagraph("Model description", content);
		ret += HelpFormater.formatParagraph("Inputs", inputDescriptions);
		ret += HelpFormater.formatParagraph("Outputs", outputDescriptions);
		ret += HelpFormater.formatParagraph("Parameters", parameterDescriptions);
		return ret;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.zuestengineering.emod.help.AModelHelp#compareToModel()
	 */
	@Override
	public void compareToModel() {
		getParametersFromModel();
		getInputsFromModel();
		getOuputsFromModel();
	}
}
