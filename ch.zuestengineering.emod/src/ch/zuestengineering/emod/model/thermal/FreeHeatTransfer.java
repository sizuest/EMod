/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.model.thermal;

import java.io.File;
import java.util.ArrayList;

import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;

import ch.zuestengineering.emod.EModSession;
import ch.zuestengineering.emod.model.APhysicalComponent;
import ch.zuestengineering.emod.model.ParameterCheckReport;
import ch.zuestengineering.emod.model.linking.IOContainer;
import ch.zuestengineering.emod.model.units.*;
import ch.zuestengineering.emod.utils.ComponentConfigReader;
import ch.zuestengineering.emod.utils.Defines;

/**
 * General free heat transfer class
 * 
 * Calculates the heat flow trough a wall with different layers and convection
 * on both sides. The temperature gradient is given by the inputs
 * 
 * Assumptions: Conduction and convection are dominant, where radiation is
 * negligible. The convection/conduction constants are no dependent on
 * temperature
 * 
 * 
 * @author simon
 * 
 */

public class FreeHeatTransfer extends APhysicalComponent {
	@XmlElement
	protected String type;
	@XmlElement
	protected String parentType;

	// Input Lists
	private IOContainer temp1;
	private IOContainer temp2;
	// Output parameters:
	private IOContainer pth12;
	private IOContainer pth21;

	// Unit of the element
	private double surf;
	private double[] alpha;
	private double[] lambda;
	private double[] dWall;

	// Heat transfere resistance
	private double thRessistance;

	/**
	 * Constructor called from XmlUnmarshaller. Attribute 'type' is set by
	 * XmlUnmarshaller.
	 */
	public FreeHeatTransfer() {
		super();
	}

	/**
	 * post xml init method (loading physics data)
	 * 
	 * @param u
	 * @param parent
	 */
	public void afterUnmarshal(Unmarshaller u, Object parent) {
		init();
	}

	/**
	 * Free heat transfere constructor
	 * 
	 * @param type
	 * @param parentType
	 */
	public FreeHeatTransfer(String type, String parentType) {
		super();

		this.type = type;
		this.parentType = parentType;

		init();
	}

	/**
	 * Called from constructor or after unmarshaller.
	 */
	private void init() {
		/* Define Input parameters */
		inputs = new ArrayList<IOContainer>();
		temp1 = new IOContainer("Temperature1", new SiUnit(Unit.KELVIN), 273, ContainerType.THERMAL);
		temp2 = new IOContainer("Temperature2", new SiUnit(Unit.KELVIN), 273, ContainerType.THERMAL);
		inputs.add(temp1);
		inputs.add(temp2);

		/* Define output parameters */
		outputs = new ArrayList<IOContainer>();
		pth12 = new IOContainer("PThermal12", new SiUnit(Unit.WATT), 0, ContainerType.THERMAL);
		pth21 = new IOContainer("PThermal21", new SiUnit(Unit.WATT), 0, ContainerType.THERMAL);
		outputs.add(pth12);
		outputs.add(pth21);

		/* Read configuration parameters: */
		loadParameters();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.ethz.inspire.emod.model.APhysicalComponent#update()
	 * 
	 * @Override
	 */
	@Override
	public void update() {

		/*
		 * The heat transfere from 1 to 2 is Qdot12 [W] = k [W/m²] * A [m²] * (T_2 -
		 * T_1)
		 */

		pth12.setValue(thRessistance * surf * (temp1.getValue() - temp2.getValue()));
		pth21.setValue(-pth12.getValue());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.ethz.inspire.emod.model.APhysicalComponent#getType()
	 */
	@Override
	public String getType() {
		return type;
	}

	@Override
	public void setType(String type) {
		// TODO this.type = type;

	}

	@Override
	public void updateBoundaryConditions() {
		// TODO Auto-generated method stub

	}

	@Override
	public ParameterCheckReport checkConfigParams() {
		ParameterCheckReport report = new ParameterCheckReport();
		// Check model parameters:

		if (surf < 0) {
			report.add("", "Negative value: Surface must be non negative");
		}

		for (int i = 0; i < alpha.length; i++)
			if (alpha[i] < 0) {
				report.add("ConvectionConstant", "Negative value: ConvectionConstant must be non negative");
			}

		if (lambda.length != dWall.length) {
			report.add("ConductionConstant", "Array size: ConductionConstant and WallThicknesses must have same size");
		}
		for (int i = 0; i < lambda.length; i++)
			if (lambda[i] < 0) {
				report.add("ConductionConstant", "Negative value: ConductionConstant must be non negative");
				break;
			}
		for (int i = 0; i < dWall.length; i++)
			if (dWall[i] <= 0 && lambda[i] != 0) {
				report.add("WallThickness", "Negative value: WallThickness must be positive");
				break;
			}
		return report;
	}

	@Override
	public void loadParameters() {
		ComponentConfigReader params = null;
		/*
		 * If no parent model file is configured, the local configuration file will be
		 * opened. Otherwise the cfg file of the parent will be opened
		 */
		if (parentType.isEmpty()) {
			String path = EModSession.getRootPath() + File.separator + Defines.MACHINECONFIGDIR + File.separator
					+ EModSession.getMachineConfig() + File.separator + this.getClass().getSimpleName() + "_" + type
					+ ".xml";
			try {
				params = new ComponentConfigReader(path);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {

			/* Open file containing the parameters of the parent model type */
			try {
				params = new ComponentConfigReader(parentType, type);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		/* Read the config parameter: */
		try {
			surf = params.getDoubleValue("thermal.Surface");
			alpha = params.getDoubleArray("thermal.ConvectionConstants");
			lambda = params.getDoubleArray("thermal.ConductionConstants");
			dWall = params.getDoubleArray("thermal.WallThicknesses");
		} catch (Exception e) {
			e.printStackTrace();
		}
		params.Close(); /* Model configuration file not needed anymore. */

		/*
		 * Calculate thermal ressistance k, with cases: - lambda=alpha=0 k = 0 - alpha=0
		 * k=lambda/d; - lambda=0 k=alpha; - else k = (1/alpha+d/lambda)^-1
		 */
		thRessistance = 0;

		for (int i = 0; i < alpha.length; i++) {
			if (0 < alpha[i])
				thRessistance += 1 / alpha[i];
		}

		for (int i = 0; i < lambda.length; i++) {
			if (0 < lambda[i])
				thRessistance += dWall[i] / lambda[i];
		}

		if (0 != thRessistance)
			thRessistance = 1 / thRessistance;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.zuestengineering.emod.model.APhysicalComponent#updateParameterSet()
	 */
	@Override
	protected void updateParameterSet() {
		// TODO Auto-generated method stub

	}
}
