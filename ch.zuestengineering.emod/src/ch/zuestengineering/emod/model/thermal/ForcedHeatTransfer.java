/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.model.thermal;

import java.io.File;
import java.util.ArrayList;

import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;

import ch.zuestengineering.emod.EModSession;
import ch.zuestengineering.emod.model.APhysicalComponent;
import ch.zuestengineering.emod.model.ParameterCheckReport;
import ch.zuestengineering.emod.model.linking.IOContainer;
import ch.zuestengineering.emod.model.material.Material;
import ch.zuestengineering.emod.model.units.*;
import ch.zuestengineering.emod.utils.ComponentConfigReader;
import ch.zuestengineering.emod.utils.Defines;

/**
 * General forced heat transfer class
 * 
 * Assumptions: Total heat transfer takes place with internal energy of a moved
 * fluid. The internal heat capacity is assumed as constant
 * 
 * 
 * @author simon
 * 
 */

public class ForcedHeatTransfer extends APhysicalComponent {
	@XmlElement
	protected String type;
	@XmlElement
	protected String parentType;

	// Input Lists
	private IOContainer temp1;
	private IOContainer temp2;
	private IOContainer massFlow;
	// Output parameters:
	private IOContainer pth12;
	private IOContainer pth21;

	// Fluid properties
	private double cp;
	private String fluidType;
	private Material fluid;

	/**
	 * Constructor called from XmlUnmarshaller. Attribute 'type' is set by
	 * XmlUnmarshaller.
	 */
	public ForcedHeatTransfer() {
		super();
	}

	/**
	 * post xml init method (loading physics data)
	 * 
	 * @param u
	 * @param parent
	 */
	public void afterUnmarshal(Unmarshaller u, Object parent) {
		init();
	}

	/**
	 * Homog. Storage constructor
	 * 
	 * @param type
	 * @param parentType
	 */
	public ForcedHeatTransfer(String type, String parentType) {
		super();

		this.type = type;
		this.parentType = parentType;

		init();
	}

	/**
	 * Called from constructor or after unmarshaller.
	 */
	private void init() {
		/* Define Input parameters */
		inputs = new ArrayList<IOContainer>();
		temp1 = new IOContainer("Temperature1", new SiUnit(Unit.KELVIN), 273, ContainerType.THERMAL);
		temp2 = new IOContainer("Temperature2", new SiUnit(Unit.KELVIN), 273, ContainerType.THERMAL);
		massFlow = new IOContainer("MassFlow", new SiUnit(Unit.KG_S), 0, ContainerType.FLUIDDYNAMIC);
		inputs.add(temp1);
		inputs.add(temp2);
		inputs.add(massFlow);

		/* Define output parameters */
		outputs = new ArrayList<IOContainer>();
		pth12 = new IOContainer("PThermal12", new SiUnit(Unit.WATT), 0, ContainerType.THERMAL);
		pth21 = new IOContainer("PThermal21", new SiUnit(Unit.WATT), 0, ContainerType.THERMAL);
		outputs.add(pth12);
		outputs.add(pth21);

		/* Read configuration parameters: */
		loadParameters();

		// Create Fluid object
		fluid = new Material(fluidType);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.ethz.inspire.emod.model.APhysicalComponent#update()
	 * 
	 * @Override
	 */
	@Override
	public void update() {

		cp = fluid.getHeatCapacity(temp1.getValue(), 1e5);

		/*
		 * The heat transfere from 1 to 2 is Qdot12 [W] = cp [J/K/kg] * mDot [kg/s] *
		 * (T_1 - T_2) [K]
		 */

		pth12.setValue(cp * massFlow.getValue() * (temp1.getValue() - temp2.getValue()));
		pth21.setValue(-pth12.getValue());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.ethz.inspire.emod.model.APhysicalComponent#getType()
	 */
	@Override
	public String getType() {
		return type;
	}

	@Override
	public void setType(String type) {
		// TODO this.type = type;
	}

	@Override
	public void updateBoundaryConditions() {
		// TODO Auto-generated method stub

	}

	@Override
	public ParameterCheckReport checkConfigParams() {
		return new ParameterCheckReport();
	}

	@Override
	public void loadParameters() {
		ComponentConfigReader params = null;
		/*
		 * If no parent model file is configured, the local configuration file will be
		 * opened. Otherwise the cfg file of the parent will be opened
		 */
		if (parentType.isEmpty()) {

			String path = EModSession.getRootPath() + File.separator + Defines.MACHINECONFIGDIR + File.separator
					+ EModSession.getMachineConfig() + File.separator + this.getClass().getSimpleName() + "_" + type
					+ ".xml";
			try {
				params = new ComponentConfigReader(path);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {

			/* Open file containing the parameters of the parent model type */
			try {
				params = new ComponentConfigReader(parentType, type);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		/* Read the config parameter: */
		try {
			fluidType = params.getString("Material");
		} catch (Exception e) {
			e.printStackTrace();
		}
		params.Close(); /* Model configuration file not needed anymore. */

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.zuestengineering.emod.model.APhysicalComponent#updateParameterSet()
	 */
	@Override
	protected void updateParameterSet() {
		// TODO Auto-generated method stub

	}
}
