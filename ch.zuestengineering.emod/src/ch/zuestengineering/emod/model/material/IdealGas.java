/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.model.material;

import ch.zuestengineering.emod.model.parameters.PhysicalValue;
import ch.zuestengineering.emod.model.units.SiConstants;
import ch.zuestengineering.emod.utils.Algo;
import ch.zuestengineering.emod.utils.MaterialConfigReader;

/**
 * Implements the properties of an ideal gas
 * 
 * @author Simon Z�st
 *
 */
/**
 * @author sizuest
 *
 */
public class IdealGas extends AMaterial {

	private double M; // Molar mass
	private double[] kappaSamples; // isentropic coefficient;
	private double[] temperatureSamples;
	private double cp; // specific heat capacity
	private double d; // Diameter

	/**
	 * @param type
	 */
	public IdealGas(String type) {
		super(type);
	}

	@Override
	public double getThermalConductivity(double temperature, double pressure) {
		double thermalConductivity = 1 / 3.0 * getDensity(temperature, pressure) * getMeanParticleVelocity(temperature)
				/ getHeatCapacityAtConstantVolume(temperature, pressure);
		return thermalConductivity;
	}

	@Override
	public double getViscosityDynamic(double temperature, double pressure) {
		// double viscosity = 5.0/16 *
		// Math.sqrt(Math.PI*getMolecularMass()*SiConstants.kB.getValue()*temperature) /
		// (Math.PI * Math.pow(getLennardJonesDiameter(), 2));
		// viscosity = 1.71E-5;
		double viscosity = .3333 * getParticleDensity(temperature, pressure) * getMolecularMass()
				* getMeanParticleVelocity(temperature) * getMeanFreepath(temperature, pressure);
		return viscosity;
	}

	@Override
	/**
	 * Ideal gas equation:
	 * 
	 * p V = m Rs T --> rho = m/V = p / Rs / T
	 */
	public double getDensity(double temperature, double pressure) {
		if (pressure <= 0)
			pressure = 1;

		return pressure / temperature / getRs();
	}

	@Override
	public double getHeatCapacityAtConstantPressure(double temperature, double pressure) {
		if (Double.isNaN(cp))
			return 5.0 / 2 * getRs();
		else
			return cp;
	}

	@Override
	public double getHeatCapacityAtConstantVolume(double temperature, double pressure) {
		if (Double.isNaN(cp))
			return 3.0 / 2 * getRs();
		else
			return cp / getHeatCapacityRatio(temperature, pressure);
	}

	@Override
	protected void init() {
		if (null == this.type)
			return;

		/*
		 * ********************************************************************* Read
		 * configuration parameters:
		 ********************************************************************/
		MaterialConfigReader params = null;
		/* Open file containing the parameters of the model type: */
		try {
			params = new MaterialConfigReader("Material", type);

			M = params.getValue("MolarMass", new PhysicalValue(28.96E-3, "kg/mol")).getValue();
			kappaSamples = params.getValue("HeatCapacityRatio", new PhysicalValue(1.4, "")).getValues();
			temperatureSamples = params.getValue("TemperatureSamples", new PhysicalValue(293.15, "K")).getValues();
			d = params.getValue("MolecularDiameter", new PhysicalValue(4E-10, "m")).getValue();
			cp = params.getValue("HeatCapacity", new PhysicalValue(1005, "J/kg/K")).getValue();

			params.saveValues();

			params.Close(); /* Model configuration file not needed anymore. */
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void checkConfigParams() throws Exception {
		// Check physical value
		if (M < 0) {
			throw new Exception("Material, type:" + type + ": Molar mass must be bigger than zero!");
		}
		if(kappaSamples.length != temperatureSamples.length)
			throw new Exception("Material, type:" + type + ": Temperature vector must match heat capacity ratio vector");
		
		for(int i=0; i<kappaSamples.length; i++)
			if (kappaSamples[i] < 0) {
				throw new Exception("Material, type:" + type + ": Heat Capacity Ratio must be bigger than zero!");
			}
		if (d < 0) {
			throw new Exception("Material, type:" + type + ": Molecular Diameter must be bigger than zero!");
		}
	}

	/**
	 * @return
	 */
	public double getRs() {
		return SiConstants.R.getValue() / M;
	}

	private double getMolecularMass() {
		return M / SiConstants.NA.getValue();
	}

	private double getMeanParticleVelocity(double temperature) {
		return Math.sqrt(8 / Math.PI * getRs() * temperature);
	}

	private double getMeanFreepath(double t, double p) {
		return 1 / Math.sqrt(2) / Math.PI / getParticleDensity(t, p) / Math.pow(d, 2);
	}

	private double getParticleDensity(double t, double p) {
		return p / SiConstants.kB.getValue() / t;
	}

	/**
	 * @param temperature 
	 * @param pressure 
	 * @return
	 */
	public double getHeatCapacityRatio(double temperature, double pressure) {
		return Algo.linearInterpolation(temperature, temperatureSamples, kappaSamples, true);
	}

}
