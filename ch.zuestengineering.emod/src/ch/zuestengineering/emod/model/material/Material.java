/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.model.material;

import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

import ch.zuestengineering.emod.lca.typedef.RawMaterialType;
import ch.zuestengineering.emod.model.fluid.FluidCircuitProperties;
import ch.zuestengineering.emod.utils.MaterialConfigReader;

/**
 * General Material model class. Provides fluid properties
 * 
 * @author sizuest
 * 
 */
public class Material implements Cloneable {
	@XmlElement
	protected String type;

	@XmlElement
	protected RawMaterialType materialClass = RawMaterialType.UNDEFINED;

	protected MaterialType mdlType = MaterialType.FLUID;

	private AMaterial material = null;

	/**
	 * Constructor called from XmlUnmarshaller. Attribute 'type' is set by
	 * XmlUnmarshaller.
	 */
	public Material() {
		this.type = null;
	}

	/**
	 * @param u
	 * @param parent
	 */
	public void afterUnmarshal(final Unmarshaller u, final Object parent) {
		// post xml init method (loading physics data)
		init();
	}

	/**
	 * Fluid constructor
	 * 
	 * @param type
	 */
	public Material(String type) {
		this.type = type;
		init();
	}

	/**
	 * @param mdlType
	 * @param type
	 */
	public Material(MaterialType mdlType, String type) {
		this.type = type;
		this.mdlType = mdlType;
		init();
	}

	/**
	 * Called from constructor or after unmarshaller.
	 */
	private void init() {
		if (null == this.type)
			return;

		if ("" == this.type)
			this.type = "Example";

		MaterialConfigReader params = null;

		/* Open file containing the parameters of the model type: */
		try {

			params = new MaterialConfigReader("Material", type);
			try {
				mdlType = MaterialType.valueOf(params.getString("ModelType"));
			} catch(Exception e) {
				mdlType = MaterialType.valueOf(params.getValue("MaterialType", mdlType.toString()));
				params.setValue("ModelType", mdlType.toString());
				params.deleteValue("MaterialType");
			}
			materialClass = RawMaterialType.valueOf(params.getValue("MaterialClass", materialClass.toString()));
			params.saveValues();
			params.Close(); /* Model configuration file not needed anymore. */

			switch (mdlType) {
			case IDEAL_GAS:
				material = new IdealGas(type);
				break;
			case SOLID:
				material = new Solid(type);
				break;
			case COMPOUND:
				material = new Compound(type);
				break;
			case MIXTURE:
				material = new Mixture(type);
				break;
			case FLUID:
			default:
				material = new Fluid(type);
				break;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Viscosity (dyn)
	 * 
	 * @param temperature [K]
	 * @param pressure
	 * @return viscosity [Pa s]
	 */
	public double getViscosityDynamic(double temperature, double pressure) {
		if(null==material)
			return Double.NaN;
		
		return material.getViscosityDynamic(temperature, pressure);
	}

	/**
	 * Viscosity (kin)
	 * 
	 * @param temperature
	 * @param pressure
	 * @return viscosity [m2/s]
	 */
	public double getViscosityKinematic(double temperature, double pressure) {
		if(null==material)
			return Double.NaN;
		
		return material.getViscosityKinematic(temperature, pressure);
	}

	/**
	 * Density
	 * 
	 * @param temperature [K]
	 * @param pressure
	 * @return density [kg/m3]
	 */
	public double getDensity(double temperature, double pressure) {
		if(null==material)
			return Double.NaN;
		
		return material.getDensity(temperature, pressure);
	}

	/**
	 * Returns the apparent heat capacity hat the given temperature
	 * @param temperature 
	 * @param pressure 
	 * 
	 * @return apparent heat capacity [J/kg/K]
	 */
	public double getHeatCapacity(double temperature, double pressure) {
		if(null==material)
			return Double.NaN;
		
		return material.getHeatCapacityAtConstantPressure(temperature, pressure);
	}
	
	/**
	 * Returns the apparent heat capacity hat the given temperature
	 * @param fcp 
	 * 
	 * @return apparent heat capacity [J/kg/K]
	 */
	public double getHeatCapacity(FluidCircuitProperties fcp) {
		if(null==material)
			return Double.NaN;
		
		return material.getHeatCapacityAtConstantPressure(fcp.getTemperature(), fcp.getPressure());
	} 

	/**
	 * @return type of material
	 */
	public String getType() {
		if (null == type)
			return "none";

		return type;
	}

	/**
	 * @param temperature
	 * @param pressure
	 * @return thermal conductivity [W/(m K)]
	 */
	public double getThermalConductivity(double temperature, double pressure) {
		if(null==material)
			return Double.NaN;
		
		return material.getThermalConductivity(temperature, pressure);
	}

	/**
	 * @param type
	 */
	public void setMaterial(String type) {
		this.type = type;
		init();
	}

	/**
	 * @param material
	 */
	public void setMaterial(Material material) {
		this.type = material.getType();
		init();
	}

	/**
	 * Compares to materials, return true if the same type is used
	 * 
	 * @param material
	 * @return true/false
	 */
	public boolean equals(Material material) {
		if (null == material)
			return false;
		if (null == type | null == material.getType())
			return false;
		return this.type.equals(material.getType());
	}

	/**
	 * @return the mdlType
	 */
	public MaterialType getMdlType() {
		return mdlType;
	}

	@Override
	public String toString() {
		if (null != type)
			return type;
		else
			return "none";
	}

	@Override
	public Material clone() {
		return new Material(getType());
	}

	/**
	 * @return
	 */
	@XmlTransient
	public RawMaterialType getMaterialClass() {
		if(null==material)
			return RawMaterialType.UNDEFINED;
		
		return materialClass;
	}

	/**
	 * @param materialClass the materialClass to set
	 */
	public void setMaterialClass(RawMaterialType materialClass) {
		this.materialClass = materialClass;
	}
}
