/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.model.material;

/**
 * Definition of a material class
 * 
 * @author Simon Z�st
 *
 */
public abstract class AMaterial {

	protected String type;

	/**
	 * @param type
	 */
	public AMaterial(String type) {
		this.type = type;
		init();

		// Validate the parameters:
		try {
			checkConfigParams();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	protected abstract void init();

	protected abstract void checkConfigParams() throws Exception;

	/**
	 * @param temperature
	 * @param pressure
	 * @return thermal conductivity [W/(m K)]
	 */
	public abstract double getThermalConductivity(double temperature, double pressure);

	/**
	 * Viscosity (dyn)
	 * 
	 * @param temperature [K]
	 * @param pressure
	 * @return viscosity [Pa s]
	 */
	public abstract double getViscosityDynamic(double temperature, double pressure);

	/**
	 * Viscosity (kin)
	 * 
	 * @param temperature
	 * @param pressure
	 * @return viscosity [m2/s]
	 */
	public double getViscosityKinematic(double temperature, double pressure) {
		double rho, eta;

		rho = getDensity(temperature, pressure);
		eta = getViscosityDynamic(temperature, pressure);

		return eta / rho;
	}

	/**
	 * Density
	 * 
	 * @param temperature [K]
	 * @param pressure
	 * @return density [kg/m3]
	 */
	public abstract double getDensity(double temperature, double pressure);

	/**
	 * Returns the apparent heat capacity at constant pressure
	 * @param temperature 
	 * @param pressure 
	 * 
	 * @return apparent heat capacity [J/kg/K]
	 */
	public abstract double getHeatCapacityAtConstantPressure(double temperature, double pressure);

	/**
	 * Returns the apparent heat capacity at constant volume
	 * @param temperature 
	 * @param pressure 
	 * 
	 * @return apparent heat capacity [J/kg/K]
	 */
	public abstract double getHeatCapacityAtConstantVolume(double temperature, double pressure);

}
