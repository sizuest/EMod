/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.model.material;

/**
 * Different types of materials
 * 
 * @author Simon Z�st
 *
 */
public enum MaterialType {
	/**
	 * Solid
	 */
	SOLID,
	/**
	 * Fluid
	 */
	FLUID,
	/**
	 * Ideal Gas
	 */
	IDEAL_GAS,
	/**
	 * Material compound
	 */
	COMPOUND,
	/**
	 * Fluid mixture
	 */
	MIXTURE
}
