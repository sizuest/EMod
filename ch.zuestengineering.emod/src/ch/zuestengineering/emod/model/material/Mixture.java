/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.model.material;

import java.util.ArrayList;

import ch.zuestengineering.emod.model.parameters.PhysicalValue;
import ch.zuestengineering.emod.utils.MaterialConfigReader;

/**
 * @author sizuest
 *
 */
public class Mixture extends AMaterial {
	private ArrayList<MixtureIngredient> ingredients = new ArrayList<>();
	
	private String[] materialNames;
	private double[] fractions;

	/**
	 * @param type
	 */
	public Mixture(String type) {
		super(type);
		
		initFromFile();
	}
	
	/**
	 * Constructor for temporary Materials
	 * 
	 * @param materialNames
	 * @param fractions
	 */
	public Mixture(String[] materialNames, double[] fractions) {		
		super("Temporary Material");
		
		this.materialNames = materialNames;
		this.fractions     = fractions;
		
		initTemporary();
	}

	/* (non-Javadoc)
	 * @see ch.zuestengineering.emod.model.material.AMaterial#init()
	 */
	@Override
	protected void init() {
		// Not used here to distinguish between saved and temporary materials
	}
	
	protected void initFromFile() {
		
		if (null == this.type)
			return;

		/*
		 * ********************************************************************* Read
		 * configuration parameters:
		 ********************************************************************/
		MaterialConfigReader params = null;
		/* Open file containing the parameters of the model type: */
		try {
			params = new MaterialConfigReader("Material", type);

			materialNames = params.getValue("Fluids", new String[] { "Water", "Vascomill-22" });
			fractions     = params.getValue("Fractions", new PhysicalValue(new double[] { .8, .2}, "")).getValues();

			params.saveValues();

			params.Close(); /* Model configuration file not needed anymore. */
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		normalizeFractions();
		loadMaterials();
	}
	
	protected void initTemporary() {
		normalizeFractions();
		loadMaterials();
	}
	
	private void loadMaterials() {	
		ingredients.clear();
		
		for(int i=0; i<materialNames.length; i++) {
			ingredients.add(new MixtureIngredient(materialNames[i], fractions[i]));
		}
	}
	
	private void normalizeFractions() {
		double sum = 0;
		
		for(int i=0; i<fractions.length; i++)
			sum += fractions[i];
		
		if(0==sum) {
			sum = 1;
			fractions[0] = 1;
		}
		
		if(1==sum)
			return;
		
		for(int i=0; i<fractions.length; i++)
			fractions[i] /= sum;
	}

	/* (non-Javadoc)
	 * @see ch.zuestengineering.emod.model.material.AMaterial#checkConfigParams()
	 */
	@Override
	protected void checkConfigParams() throws Exception {
		for(MixtureIngredient ci: ingredients)
			ci.getType().checkConfigParams();

	}

	/* (non-Javadoc)
	 * @see ch.zuestengineering.emod.model.material.AMaterial#getThermalConductivity(double, double)
	 */
	@Override
	public double getThermalConductivity(double temperature, double pressure) {
		// ZS: Assumption: stack of individual layers with same cross-section
		
		double num = 0, den = 0;
		for(MixtureIngredient ci: ingredients) {
			num += ci.getFration()/ci.getType().getDensity(temperature, pressure);
			den += ci.getFration()/ci.getType().getDensity(temperature, pressure)/ci.getType().getThermalConductivity(temperature, pressure);
		}
		
		return num/den;
	}

	/* (non-Javadoc)
	 * @see ch.zuestengineering.emod.model.material.AMaterial#getViscosityDynamic(double, double)
	 */
	@Override
	public double getViscosityDynamic(double temperature, double pressure) {
		double ret = 0;
		for(MixtureIngredient ci: ingredients)
			ret += Math.log(ci.getType().getViscosityDynamic(temperature, pressure))*ci.getFration();
		
		return Math.exp(ret);
	}

	/* (non-Javadoc)
	 * @see ch.zuestengineering.emod.model.material.AMaterial#getDensity(double, double)
	 */
	@Override
	public double getDensity(double temperature, double pressure) {
		double ret = 0;
		for(MixtureIngredient ci: ingredients)
			ret += ci.getType().getDensity(temperature, pressure)*ci.getFration();
		
		return ret;
	}

	/* (non-Javadoc)
	 * @see ch.zuestengineering.emod.model.material.AMaterial#getHeatCapacityAtConstantPressure(double, double)
	 */
	@Override
	public double getHeatCapacityAtConstantPressure(double temperature, double pressure) {
		double ret = 0;
		for(MixtureIngredient ci: ingredients)
			ret += ci.getType().getHeatCapacityAtConstantPressure(temperature, pressure)*ci.getFration();
		
		return ret;
	}

	/* (non-Javadoc)
	 * @see ch.zuestengineering.emod.model.material.AMaterial#getHeatCapacityAtConstantVolume(double, double)
	 */
	@Override
	public double getHeatCapacityAtConstantVolume(double temperature, double pressure) {
		double ret = 0;
		for(MixtureIngredient ci: ingredients)
			ret += ci.getType().getHeatCapacityAtConstantVolume(temperature, pressure)*ci.getFration();
		
		return ret;
	}

}

class MixtureIngredient {
	private Fluid TYPE;
	private double FRACTION;
	
	public MixtureIngredient(String type, double fraction) {
		this.TYPE = new Fluid(type);
		this.FRACTION = fraction;
	}
	
	/**
	 * @return the tYPE
	 */
	public Fluid getType() {
		return TYPE;
	}
	/**
	 * @return the fRACTION
	 */
	public double getFration() {
		return FRACTION;
	}
}
