/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.model.material;

import ch.zuestengineering.emod.model.parameters.PhysicalValue;
import ch.zuestengineering.emod.model.units.SiConstants;
import ch.zuestengineering.emod.utils.Algo;
import ch.zuestengineering.emod.utils.MaterialConfigReader;

/**
 * Implements the generic material properties of a fluid
 * 
 * @author Simon Z�st
 *
 */
public class Fluid extends AMaterial {

	/**
	 * @param type
	 */
	public Fluid(String type) {
		super(type);
	}

	private double heatCapacity;
	private double[] temperatureSamples;
	private double[] viscositySamples;
	private double[] densitySamples;
	private double thermalConductivity;

	// Parameters for viscosity
	private double viscosityEta0 = Double.NaN;
	private double viscosityEa = Double.NaN;

	@Override
	public double getThermalConductivity(double temperature, double pressure) {
		return thermalConductivity;
	}

	@Override
	public double getViscosityDynamic(double temperature, double pressure) {
		double eta;
		/*
		 * 
		 */
		if (2 == viscositySamples.length & Double.isNaN(viscosityEa) & Double.isNaN(viscosityEta0)) {
			double[][] H = new double[viscositySamples.length][2];
			double[] y = new double[viscositySamples.length];

			for (int i = 0; i < viscositySamples.length; i++) {
				H[i][0] = 1;
				H[i][1] = 1 / SiConstants.R.getValue() / temperatureSamples[i];
				y[i] = Math.log(viscositySamples[i]);
			}

			double[] p = Algo.findLeastSquares(H, y);

			viscosityEta0 = Math.exp(p[0]);
			viscosityEa = p[1];
		}

		if (!Double.isNaN(viscosityEa) & !Double.isNaN(viscosityEta0))
			eta = viscosityEta0 * Math.exp(viscosityEa / SiConstants.R.getValue() / temperature);
		else
			eta = Algo.logInterpolation(temperature, temperatureSamples, viscositySamples);

		return eta;
	}

	@Override
	public double getDensity(double temperature, double pressure) {
		return Algo.linearInterpolation(temperature, temperatureSamples, densitySamples);
	}

	@Override
	public double getHeatCapacityAtConstantPressure(double temperature, double pressure) {
		return heatCapacity;
	}

	@Override
	protected void init() {
		if (null == this.type)
			return;

		/*
		 * ********************************************************************* Read
		 * configuration parameters:
		 ********************************************************************/
		MaterialConfigReader params = null;
		/* Open file containing the parameters of the model type: */
		try {
			params = new MaterialConfigReader("Material", type);

			// Remove old variables
			params.deleteValue("PressureSamples");

			temperatureSamples = params.getValue("TemperatureSamples",
					new PhysicalValue(new double[] { 273.15, 293.15, 303.15, 313.15, 323.15, 333.15, 343.15 }, "K"))
					.getValues();
			viscositySamples = params
					.getValue("ViscositySamples", new PhysicalValue(
							new double[] { 0.001791, 0.001001, 7.97E-4, 6.52E-4, 5.46E-4, 4.66E-4, 4.03E-4 }, "Pa s"))
					.getValues();
			densitySamples = params.getValue("DensityMatrix",
					new PhysicalValue(new double[] { 999.0, 998.0, 995.0, 992.0, 988.0, 983.0, 977.0 }, "kg m^-3"))
					.getValues();
			heatCapacity = params.getValue("HeatCapacity", new PhysicalValue(4180.0, "J kg^-1 K^-1")).getValue();
			thermalConductivity = params.getValue("ThermalConductivity", new PhysicalValue(0.6, "W m^-1 K^-1"))
					.getValue();

			params.saveValues();

			params.Close(); /* Model configuration file not needed anymore. */
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void checkConfigParams() throws Exception {
		// Check dimensions
		if (viscositySamples.length != temperatureSamples.length)
			throw new Exception(
					"Valve, type:" + type + ": ViscositySamples and TemperatureSamples must have the same dimensions!");

		for (int i = 1; i < temperatureSamples.length; i++) {
			if (temperatureSamples[i] <= temperatureSamples[i - 1]) {
				throw new Exception("Valve, type:" + type + ": Sample vector 'TemperatureSamples' must be sorted!");
			}
		}

		// Check physical value
		if (heatCapacity < 0) {
			throw new Exception("Valve, type:" + type + ": Heat capacity must be bigger than zero!");
		}

		for (int i = 0; i < temperatureSamples.length; i++) {
			if (temperatureSamples[i] < 0) {
				throw new Exception("Valve, type:" + type + ": Temperature must be bigger than zero!");
			}
		}
		for (int i = 0; i < viscositySamples.length; i++) {
			if (viscositySamples[i] < 0) {
				throw new Exception("Valve, type:" + type + ": Viscosity must be bigger than zero!");
			}
		}
	}

	@Override
	public double getHeatCapacityAtConstantVolume(double temperature, double pressure) {
		return Double.NaN;
	}

}
