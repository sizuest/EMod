/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.model.linking;

import java.util.logging.Logger;

import ch.zuestengineering.emod.model.units.*;

/**
 * Container for all model i/o
 * 
 * @author dhampl
 * 
 */
public class IOContainer {

	private String name;
	private SiUnit unit;

	private IOContainer reference;

	private double value;

	private ContainerType type;

	protected Logger logger = Logger.getLogger(IOContainer.class.getName());

	/**
	 * IOContainer
	 */
	public IOContainer() {
	}

	/**
	 * @param name
	 * @param unit
	 * @param value
	 */
	public IOContainer(String name, SiUnit unit, double value) {
		super();
		this.name = name;
		this.unit = unit;
		this.value = value;
		this.type = ContainerType.NONE;
		this.reference = this;
	}

	/**
	 * @param name
	 * @param unit
	 * @param value
	 * @param type
	 */
	public IOContainer(String name, SiUnit unit, double value, ContainerType type) {
		super();
		this.name = name;
		this.unit = unit;
		this.value = value;
		this.type = type;
		this.reference = this;
	}

	/**
	 * @param name
	 * @param reference
	 */
	public IOContainer(String name, IOContainer reference) {
		super();
		this.name = name;
		this.unit = reference.unit;
		this.value = reference.value;
		this.type = reference.type;
		this.reference = reference;
	}

	/**
	 * @return the value
	 */
	public double getValue() {
		return value;
	}

	/**
	 * @param value the value to set
	 */
	public void setValue(double value) {
		this.value = value;
	}

	/**
	 * @return the reference
	 */
	public IOContainer getReference() {
		return reference;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the unit
	 */
	public SiUnit getUnit() {
		return unit;
	}

	/**
	 * @return the type
	 */
	public ContainerType getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(ContainerType type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return name + " " + value + " " + unit + " " + type;
	}

	/**
	 * Sets the unit
	 * 
	 * @param unit
	 */
	public void setUnit(SiUnit unit) {
		this.unit = unit;
	}

	/**
	 * returns whether the connection has a reference
	 * 
	 * @return
	 */
	public boolean hasReference() {
		return !this.equals(getReference());
	}

	/**
	 * Sets the name of the sc
	 * 
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * addValue
	 * @param value
	 */
	public void addValue(double value) {
		this.value += value;
	}
}
