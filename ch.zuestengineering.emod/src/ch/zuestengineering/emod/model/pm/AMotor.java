/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.model.pm;

import java.util.ArrayList;

import ch.zuestengineering.emod.femexport.BoundaryCondition;
import ch.zuestengineering.emod.femexport.BoundaryConditionType;
import ch.zuestengineering.emod.model.APhysicalComponent;
import ch.zuestengineering.emod.model.linking.IOContainer;
import ch.zuestengineering.emod.model.units.SiUnit;

/**
 * General Motor model abstract class.
 **/

public abstract class AMotor extends APhysicalComponent {
	// Input parameters:
	protected IOContainer rotspeed;
	protected IOContainer force;
	// Output parameters:
	protected IOContainer pmech;
	protected IOContainer ploss;
	protected IOContainer pel;
	protected IOContainer efficiency;
	// Boundary conditions
	protected BoundaryCondition bcHeatSrcStator;
	protected BoundaryCondition bcHeatSrcRotor;

	protected void init() {
		/* Boundary conditions */
		boundaryConditions = new ArrayList<BoundaryCondition>();
		bcHeatSrcStator = new BoundaryCondition("HeatSrcStator", new SiUnit("W"), 0, BoundaryConditionType.NEUMANN);
		bcHeatSrcRotor = new BoundaryCondition("HeatSrcRotor", new SiUnit("W"), 0, BoundaryConditionType.NEUMANN);
		boundaryConditions.add(bcHeatSrcStator);
		boundaryConditions.add(bcHeatSrcRotor);
	}

}
