/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.model.pm;

import java.util.ArrayList;

import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import ch.zuestengineering.emod.model.APhysicalComponent;
import ch.zuestengineering.emod.model.ParameterCheckReport;
import ch.zuestengineering.emod.model.fluid.FECZeta;
import ch.zuestengineering.emod.model.fluid.Floodable;
import ch.zuestengineering.emod.model.fluid.FluidCircuitProperties;
import ch.zuestengineering.emod.model.linking.FluidContainer;
import ch.zuestengineering.emod.model.linking.IOContainer;
import ch.zuestengineering.emod.model.parameters.ParameterSet;
import ch.zuestengineering.emod.model.thermal.ThermalElement;
import ch.zuestengineering.emod.model.units.ContainerType;
import ch.zuestengineering.emod.model.units.SiUnit;
import ch.zuestengineering.emod.model.units.Unit;
import ch.zuestengineering.emod.simulation.DynamicState;
import ch.zuestengineering.emod.utils.ComponentConfigReader;

/**
 * General Heat exchanger model class. Implements the physical model of a heat
 * exchanger with external coolant supply.
 * 
 * 
 * Inputlist: 1: State : [-] : State of the HE 2: Fluid1In : [-] : Fluid 1
 * flowing into the HE 3: Fluid2In : [-] : Fluid 2 flowing into the HE
 * Outputlist: 1: PTotal : [W] : Demanded electrical power 2: PLoss : [W] :
 * Losses 3: Fluid1Out : [-] : Fluid 1 flowing out of the HE 4: Fluid2Out : [-]
 * : Fluid 2 flowing out of the HE
 * 
 * Config parameters: PressureSamples : [Pa] : Pressure samples for liner
 * interpolation FlowRateSamples : [m^3/s] : Volumetric flow samples for liner
 * interpolation ElectricalPower : [W] : Power samples for liner interpolation
 * 
 * @author sizuest
 * 
 */

@XmlRootElement
public class HeatExchanger extends APhysicalComponent implements Floodable {
	@XmlElement
	protected String type;

	// Input parameters
	private IOContainer level;
	private FluidContainer fluid1In;
	private FluidContainer fluid2In;

	// Output parameters
	private IOContainer ptotal, ploss, pth;
	private FluidContainer fluid1Out;
	private FluidContainer fluid2Out;

	// Fluid properties
	private FluidCircuitProperties fluidProperties1;
	private FluidCircuitProperties fluidProperties2;

	private FECZeta zeta1, zeta2;

	private ThermalElement fluid1, fluid2;

	// Parameters
	private double zetaValue1, zetaValue2;
	private double htc;
	private double tempOn, tempOff;
	private double power;
	private double volume1, volume2;

	private boolean wasCooling = false;

	private boolean isInitialized = false;

	/**
	 * Default heat exchanger
	 */
	public HeatExchanger() {
		super();
	}

	/**
	 * HeatExchanger constructor
	 * 
	 * @param type
	 */
	public HeatExchanger(String type) {
		super();

		this.type = type;
		init();
	}

	/**
	 * post xml init method (loading physics data)
	 * 
	 * @param u
	 * @param parent
	 */
	public void afterUnmarshal(final Unmarshaller u, final Object parent) {
		init();
	}

	private void init() {
		zeta1 = new FECZeta(1);
		zeta2 = new FECZeta(1);

		fluidProperties1 = new FluidCircuitProperties(zeta1);
		fluidProperties2 = new FluidCircuitProperties(zeta2);

		// Inputs
		inputs = new ArrayList<IOContainer>();
		level = new IOContainer("State", new SiUnit(Unit.NONE), 0, ContainerType.CONTROL);
		fluid1In = new FluidContainer("Fluid1In", new SiUnit(Unit.NONE), ContainerType.FLUIDDYNAMIC, fluidProperties1);
		fluid2In = new FluidContainer("Fluid2In", new SiUnit(Unit.NONE), ContainerType.FLUIDDYNAMIC, fluidProperties2);
		inputs.add(level);
		inputs.add(fluid1In);
		inputs.add(fluid2In);

		// Outputs
		outputs = new ArrayList<IOContainer>();
		ptotal = new IOContainer("PTotal", new SiUnit(Unit.WATT), 0, ContainerType.ELECTRIC);
		ploss = new IOContainer("PLoss", new SiUnit(Unit.WATT), 0, ContainerType.THERMAL);
		pth = new IOContainer("PThermal", new SiUnit(Unit.WATT), 0, ContainerType.THERMAL);
		fluid1Out = new FluidContainer("Fluid1Out", new SiUnit(Unit.NONE), ContainerType.FLUIDDYNAMIC,
				fluidProperties1);
		fluid2Out = new FluidContainer("Fluid2Out", new SiUnit(Unit.NONE), ContainerType.FLUIDDYNAMIC,
				fluidProperties2);
		outputs.add(ptotal);
		outputs.add(ploss);
		outputs.add(pth);
		outputs.add(fluid1Out);
		outputs.add(fluid2Out);

		/* Create thermal elements */
		fluid1 = new ThermalElement("Example", 1.0);
		fluid2 = new ThermalElement("Example", 1.0);

		/* Read configuration parameters: */
		loadParameters();

		fluid1.getTemperature().setName("Temperature1");
		fluid2.getTemperature().setName("Temperature2");

		fluid1.getMass().setName("Mass1");
		fluid2.getMass().setName("Mass2");

		fluid1.setMaterial(fluidProperties1.getMaterial());
		fluid2.setMaterial(fluidProperties2.getMaterial());

		dynamicStates = new ArrayList<DynamicState>();
		dynamicStates.add(fluid1.getTemperature());
		dynamicStates.add(fluid2.getTemperature());

		fluidProperties1.setTemperature(fluid1.getTemperature());
		fluidProperties2.setTemperature(fluid2.getTemperature());

	}

	@Override
	public String getType() {
		return type;
	}

	@Override
	public void update() {
		fluid1.setMaterial(fluidProperties1.getMaterial());
		fluid2.setMaterial(fluidProperties2.getMaterial());

		double htc = 0;

		if (!isInitialized) {
			fluid1.getMass().setInitialCondition(
					volume1 * fluid1.getMaterial().getDensity(fluid1.getTemperature().getValue(), 1E5));
			fluid2.getMass().setInitialCondition(
					volume2 * fluid2.getMaterial().getDensity(fluid2.getTemperature().getValue(), 1E5));
			isInitialized = true;
		}

		if (level.getValue() == 1) {
			/* Controlled Temperature: tempOff=tempOn */
			if (tempOff == tempOn) {
				double heatFlux;

				heatFlux = Math.max(0, (fluid1In.getTemperature() - tempOn) * fluidProperties1.getMassFlowRate()
						* fluidProperties1.getMaterial().getHeatCapacity(fluidProperties1));

				fluid1.setHeatInput(-heatFlux);
				fluid2.setHeatInput(heatFlux);

				ptotal.setValue(power);
				ploss.setValue(power);
			} else {

				ptotal.setValue(power);
				ploss.setValue(power);

				if ((wasCooling & fluidProperties1.getTemperatureIn() > tempOff)
						| (!wasCooling & fluidProperties1.getTemperatureIn() >= tempOn)) {
					wasCooling = true;
					htc = this.htc;

				} else
					wasCooling = false;

			}
		} else {
			wasCooling = false;
			ptotal.setValue(0);
			ploss.setValue(0);
			fluid1.setHeatInput(0);
			fluid2.setHeatInput(0);
		}

		fluid1.setThermalResistance(htc);
		fluid2.setThermalResistance(htc);

		fluid1.setTemperatureAmb(fluid2.getTemperature().getValue());
		fluid2.setTemperatureAmb(fluid1.getTemperature().getValue());

		fluid1.integrate(timestep, fluidProperties1.getFlowRate(), fluidProperties1.getFlowRate(),
				fluidProperties1.getPressure());
		fluid2.integrate(timestep, fluidProperties2.getFlowRate(), fluidProperties2.getFlowRate(),
				fluidProperties2.getPressure());

		fluid1.setTemperatureIn(fluid1In.getTemperature());
		fluid2.setTemperatureIn(fluid2In.getTemperature());

		pth.setValue(0.5 * (fluid1.getBoundaryHeatFlux() - fluid2.getBoundaryHeatFlux()));
	}

	@Override
	public void setType(String type) {
		this.type = type;
	}

	@Override
	public ArrayList<FluidCircuitProperties> getFluidPropertiesList() {
		ArrayList<FluidCircuitProperties> out = new ArrayList<FluidCircuitProperties>();
		out.add(fluidProperties1);
		out.add(fluidProperties2);
		return out;
	}

	@Override
	public void flood() {/* Not used */
	}

	@Override
	public void updateBoundaryConditions() {/* Not used */
	}

	@Override
	public ParameterCheckReport checkConfigParams() {
		ParameterCheckReport report = new ParameterCheckReport();

		if (zetaValue1 <= 0) {
			report.add("PressureLossCoefficient1",
					"Negative or zero: 'PressureLossCoefficient1' must be strictly positive");
		}
		if (zetaValue2 <= 0) {
			report.add("PressureLossCoefficient2",
					"Negative or zero: 'PressureLossCoefficient2' must be strictly positive");
		}
		if (htc <= 0) {
			report.add("HeatTransferCoefficient",
					"Negative or zero: 'HeatTransferCoefficient' must be strictly positive");
		}
		if (tempOn <= 0) {
			report.add("TemperatureHigh", "Negative or zero: 'TemperatureHigh' must be strictly positive");
		}
		if (tempOff <= 0) {
			report.add("TemperatureLow", "Negative or zero: 'TemperatureLow' must be strictly positive");
		}
		if (tempOn <= tempOff) {
			report.add("TemperatureHigh",
					"Values not matching: 'TemperatureHigh' must be bigger than 'TemperatureLow'");
		}
		if (power == 0) {
			report.add("Power", "Negative: 'Power' must be positive");
		}
		if (volume1 <= 0) {
			report.add("Volume1", "Negative or zero: 'Volume1' must be strictly positive");
		}
		if (volume2 <= 0) {
			report.add("Volume2", "Negative or zero: 'Volume2' must be strictly positive");
		}

		return report;
	}

	@Override
	public void loadParameters() {

		super.loadParameters();

		zetaValue1 = parameterSet.getPhysicalValue("PressureLossCoefficient1").getValue();
		zetaValue2 = parameterSet.getPhysicalValue("PressureLossCoefficient2").getValue();
		htc = parameterSet.getPhysicalValue("HeatTransferCoefficient").getValue();
		tempOn = parameterSet.getPhysicalValue("TemperatureHigh").getValue();
		tempOff = parameterSet.getPhysicalValue("TemperatureLow").getValue();
		power = parameterSet.getPhysicalValue("Power").getValue();
		volume1 = parameterSet.getPhysicalValue("Volume1").getValue();
		volume2 = parameterSet.getPhysicalValue("Volume2").getValue();

		/* Set pressure loss coeff. */
		zeta1.setZeta(zetaValue1);
		zeta2.setZeta(zetaValue2);

		fluid1.setVolume(volume1);
		fluid2.setVolume(volume2);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.zuestengineering.emod.model.APhysicalComponent#updateParameterSet()
	 */
	@Override
	protected void updateParameterSet() {
		logger.warning(
				"Parameter set is empty; tying for old parameter set format (this option will be removed in later versions) ...");

		ComponentConfigReader params = null;
		/* Open file containing the parameters of the model type: */
		try {
			params = new ComponentConfigReader(getModelType(), type);
		} catch (Exception e) {
			e.printStackTrace();
		}

		/* Read the config parameter: */
		try {
			zetaValue1 = params.getPhysicalValue("PressureLossCoefficient1", new SiUnit("Pa s^2 m^-6")).getValue();
			zetaValue2 = params.getPhysicalValue("PressureLossCoefficient2", new SiUnit("Pa s^2 m^-6")).getValue();

			htc = params.getPhysicalValue("HeatTransferCoefficient", new SiUnit("W K^-1")).getValue();

			tempOn = params.getPhysicalValue("TemperatureHigh", new SiUnit("K")).getValue();
			tempOff = params.getPhysicalValue("TemperatureLow", new SiUnit("K")).getValue();

			power = params.getPhysicalValue("Power", new SiUnit("W")).getValue();

			volume1 = params.getPhysicalValue("Volume1", new SiUnit("m^3")).getValue();
			volume2 = params.getPhysicalValue("Volume2", new SiUnit("m^3")).getValue();
		} catch (Exception e) {
			e.printStackTrace();
		}

		parameterSet = new ParameterSet(this.toString());
		parameterSet.setPhysicalValue("PressureLossCoefficient1", zetaValue1, new SiUnit("Pa s^2 m^-6"));
		parameterSet.setPhysicalValue("PressureLossCoefficient2", zetaValue2, new SiUnit("Pa s^2 m^-6"));
		parameterSet.setPhysicalValue("HeatTransferCoefficient", htc, new SiUnit("W K^-1"));
		parameterSet.setPhysicalValue("TemperatureHigh", tempOn, new SiUnit("K"));
		parameterSet.setPhysicalValue("TemperatureLow", tempOff, new SiUnit("K"));
		parameterSet.setPhysicalValue("Power", power, new SiUnit("W"));
		parameterSet.setPhysicalValue("Volume1", volume1, new SiUnit("m^3"));
		parameterSet.setPhysicalValue("Volume2", volume2, new SiUnit("m^3"));
		parameterSet.save(getParameterFile());
		logger.info("Parameter file successfully converted to new format");

	}

}
