/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.model.pm;

import java.lang.reflect.Constructor;
import java.util.ArrayList;

import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import ch.zuestengineering.emod.dd.Duct;
import ch.zuestengineering.emod.femexport.BoundaryCondition;
import ch.zuestengineering.emod.femexport.BoundaryConditionType;
import ch.zuestengineering.emod.model.APhysicalComponent;
import ch.zuestengineering.emod.model.ParameterCheckReport;
import ch.zuestengineering.emod.model.fluid.FECDuct;
import ch.zuestengineering.emod.model.fluid.Floodable;
import ch.zuestengineering.emod.model.fluid.FluidCircuitProperties;
import ch.zuestengineering.emod.model.linking.FluidContainer;
import ch.zuestengineering.emod.model.linking.IOContainer;
import ch.zuestengineering.emod.model.parameters.ParameterSet;
import ch.zuestengineering.emod.model.thermal.ThermalArray;
import ch.zuestengineering.emod.model.thermal.ThermalElement;
import ch.zuestengineering.emod.model.units.*;
import ch.zuestengineering.emod.simulation.DynamicState;
import ch.zuestengineering.emod.utils.ComponentConfigReader;

/**
 * General rotational axis model class. Implements the physical model of a
 * linear axis. From the input parameter rotational speed and process torque,
 * the requested motor torque is calculated
 * 
 * Assumptions: The inertia of mass and frictional forces are negligible
 * 
 * Inputlist: 1: State : [-] : State of the axis (0/1) 2: Speed : [Hz] : Actual
 * rotational speed 3: ProcessTorque: [Nm] : Actual process torque 4: CoolantIn
 * : [-] : Coolant fluid in Outputlist: 1: PTotal : [W] : Input power 2: PUse :
 * [W] : Output power (usable) 3: PLoss : [W] : Output power (losses) 4:
 * CoolantOut : [-] : Coolant fluid out
 * 
 * Config parameters: Transmission : [-] : Transmission between the motor (rpm)
 * and the axis (rpm) Inertia : [kg m] : Inertia of the moving part MotorType
 * StructureMass StructureMaterial StructureDuct
 * 
 * @author simon
 * 
 */
@XmlRootElement
public class RotAxis extends APhysicalComponent implements Floodable {

	@XmlElement
	protected String type;

	// Input parameters:
	private IOContainer state;
	private IOContainer speed;
	private IOContainer torque;
	private FluidContainer coolantIn;
	private FluidContainer lubricantIn;
	// Output parameters:
	private IOContainer puse;
	private IOContainer ploss;
	private IOContainer ptotal;
	private FluidContainer coolantOut;
	private FluidContainer lubricantOut;
	// Boundary conditions
	private BoundaryCondition bcHeatSrcBrake;
	private BoundaryCondition bcCoolantTemperature;
	private BoundaryCondition bcCoolantHTC;

	// Save last input values
	private double lastspeed, lasttorque;

	// Parameters used by the model.
	private double inertia;
	private double movingMass;
	private double lever;
	private double mass;
	private double powerBrakeOn, powerBrakeOff;
	private boolean hasThermalStorrage;

	// Global values
	private double alphaCoolant = 0, alphaLubricant = 0;

	// SubModels
	private AMotor motor;
	private Transmission transmission;
	private ArrayList<Bearing> bearings;
	private ThermalElement massCooled;
	private ThermalArray coolant, lubricant;
	private Duct ductCoolant, ductLubricant;
	private MovingMass massMoved;

	// FluidProperties
	FluidCircuitProperties coolantProperties;
	private boolean coolantConnected = false;
	private FluidCircuitProperties lubricantProperties;
	private boolean lubricantConnected = false;

	/**
	 * Constructor called from XmlUnmarshaller. Attribute 'type' is set by
	 * XmlUnmarshaller.
	 */
	public RotAxis() {
		super();
	}

	/**
	 * post xml init method (loading physics data)
	 * 
	 * @param u
	 * @param parent
	 */
	public void afterUnmarshal(Unmarshaller u, Object parent) {
		init();
	}

	/**
	 * Linear Axis constructor
	 * 
	 * @param type
	 */
	public RotAxis(String type) {
		super();

		this.type = type;
		init();
	}

	/**
	 * Called from constructor or after unmarshaller.
	 */
	private void init() {
		/* Define Input parameters */
		inputs = new ArrayList<IOContainer>();
		state = new IOContainer("State", new SiUnit(Unit.NONE), 0, ContainerType.CONTROL);
		speed = new IOContainer("Speed", new SiUnit(Unit.REVOLUTIONS_S), 0, ContainerType.MECHANIC);
		torque = new IOContainer("ProcessTorque", new SiUnit(Unit.NEWTONMETER), 0, ContainerType.MECHANIC);
		inputs.add(state);
		inputs.add(speed);
		inputs.add(torque);

		/* Define output parameters */
		outputs = new ArrayList<IOContainer>();
		puse = new IOContainer("PUse", new SiUnit(Unit.WATT), 0, ContainerType.MECHANIC);
		ploss = new IOContainer("PLoss", new SiUnit(Unit.WATT), 0, ContainerType.THERMAL);
		ptotal = new IOContainer("PTotal", new SiUnit(Unit.WATT), 0, ContainerType.ELECTRIC);
		outputs.add(puse);
		outputs.add(ploss);
		outputs.add(ptotal);

		ductCoolant = new Duct();
		ductLubricant = new Duct();
		coolant = new ThermalArray("Example", 1, 20);
		lubricant = new ThermalArray("Example", 1, 20);
		massCooled = new ThermalElement("Example", 1);

		/* Read configuration parameters: */
		loadParameters();

		/* Fluid properties */
		coolantProperties = new FluidCircuitProperties(new FECDuct(ductCoolant, coolant.getTemperature()),
				coolant.getTemperature());
		lubricantProperties = new FluidCircuitProperties(new FECDuct(ductLubricant, lubricant.getTemperature()),
				lubricant.getTemperature());

		/* Define fluid in-/outputs */
		coolantIn = new FluidContainer("CoolantIn", new SiUnit(Unit.NONE), ContainerType.FLUIDDYNAMIC,
				coolantProperties);
		inputs.add(coolantIn);
		coolantOut = new FluidContainer("CoolantOut", new SiUnit(Unit.NONE), ContainerType.FLUIDDYNAMIC,
				coolantProperties);
		outputs.add(coolantOut);

		/* Define lubricant in-/outputs */
		lubricantIn = new FluidContainer("LubricantIn", new SiUnit(Unit.NONE), ContainerType.FLUIDDYNAMIC,
				lubricantProperties);
		inputs.add(lubricantIn);
		lubricantOut = new FluidContainer("LubricantOut", new SiUnit(Unit.NONE), ContainerType.FLUIDDYNAMIC,
				lubricantProperties);
		outputs.add(lubricantOut);

		// Change state name
		coolant.getTemperature().setName("TemperatureCoolant");
		massCooled.getTemperature().setName("TemperatureStructure");
		lubricant.getTemperature().setName("TemperatureLubricant");

		/* Fluid circuit parameters */
		coolant.setMaterial(coolantProperties.getMaterial());
		ductCoolant.setMaterial(coolantProperties.getMaterial());
		lubricant.setMaterial(lubricantProperties.getMaterial());
		ductLubricant.setMaterial(lubricantProperties.getMaterial());
		ductCoolant.setMaterial(coolantProperties.getMaterial());

		/* Initialize sub-model */
		massMoved = new MovingMass(movingMass, inertia, 0, lever);

		dynamicStates = new ArrayList<DynamicState>();
		dynamicStates.add(0, massMoved.getDynamicStateList().get(1));
		dynamicStates.add(1, massCooled.getTemperature());
		dynamicStates.add(2, coolant.getTemperature());
		dynamicStates.add(2, lubricant.getTemperature());

		/* Boundary conditions */
		boundaryConditions = new ArrayList<BoundaryCondition>();
		bcHeatSrcBrake = new BoundaryCondition("BrakeHeatSrc", new SiUnit("W"), 0, BoundaryConditionType.NEUMANN);
		bcCoolantTemperature = new BoundaryCondition("CoolantTemperature", new SiUnit("K"), 293.15,
				BoundaryConditionType.ROBIN);
		bcCoolantHTC = new BoundaryCondition("CoolantHTC", new SiUnit("W/K"), 0, BoundaryConditionType.ROBIN);

		// Motor
		for (BoundaryCondition bc : motor.getBoundaryConditions())
			bc.setName("Motor" + bc.getName());
		boundaryConditions.addAll(motor.getBoundaryConditions());
		// Bearings
		if (bearings != null) {
			int i = 1;
			for (Bearing b : bearings) {
				for (BoundaryCondition bc : b.getBoundaryConditions())
					bc.setName("Bearing" + i + bc.getName());
				boundaryConditions.addAll(b.getBoundaryConditions());
				i++;
			}
		}
		// Transmission
		if (null != transmission) {
			for (BoundaryCondition bc : transmission.getBoundaryConditions())
				bc.setName("Transmission" + bc.getName());
			boundaryConditions.addAll(transmission.getBoundaryConditions());
		}
		// Others
		boundaryConditions.add(bcHeatSrcBrake);
		boundaryConditions.add(bcCoolantTemperature);
		boundaryConditions.add(bcCoolantHTC);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.ethz.inspire.emod.model.APhysicalComponent#update()
	 */
	@Override
	public void update() {

		lastspeed = speed.getValue();
		lasttorque = torque.getValue();

		/* Update sub model mass */
		massMoved.setSimulationTimestep(timestep);
		massMoved.getInput("SpeedRot").setValue(speed.getValue()*2*Math.PI);
		massMoved.update();

		lasttorque += massMoved.getOutput("Torque").getValue();

		/* Update sub model bearings */
		if (bearings != null)
			for (Bearing b : bearings) {
				b.getInput("RotSpeed").setValue(lastspeed);
				b.getInput("Temperature1").setValue(massCooled.getTemperature().getInitialValue());
				b.getInput("Temperature2").setValue(massCooled.getTemperature().getInitialValue());
				b.update();
				lasttorque += b.getOutput("Torque").getValue();
			}

		/* Update sub model transmission */
		if (null != transmission) {
			transmission.getInput("RotSpeed").setValue(lastspeed);
			transmission.getInput("Torque").setValue(lasttorque);
			transmission.update();

			lastspeed = transmission.getOutput("RotSpeed").getValue();
			lasttorque = transmission.getOutput("Torque").getValue();
		}

		if (1 == state.getValue()) {

			/*
			 * Rotation speed The requested rotational speed is given by the transmission
			 */
			motor.getInput("RotSpeed").setValue(lastspeed);

			/*
			 * Torque The absolute torque is given as T = k*Tp
			 */
			motor.getInput("Torque").setValue(lasttorque);
			motor.update();
			/* Powers */
			puse.setValue(Math.abs(lastspeed * 2 * Math.PI * lasttorque));
			ptotal.setValue(motor.getOutput("PTotal").getValue() + powerBrakeOff);
			ploss.setValue(ptotal.getValue() - puse.getValue());
		} else {
			motor.getOutput("PUse").setValue(0);
			motor.getOutput("PLoss").setValue(powerBrakeOn);
			motor.getOutput("PTotal").setValue(powerBrakeOn);

			puse.setValue(0);
			ptotal.setValue(0);
			ploss.setValue(0);
		}

		if (coolantConnected) {

			// Thermal resistance
			if (hasThermalStorrage)
				alphaCoolant = ductCoolant.getThermalResistance(coolantProperties.getFlowRate(),
						coolantProperties.getPressureIn(), coolant.getTemperature().getValue(),
						massCooled.getTemperature().getValue());
			else
				alphaCoolant = Double.POSITIVE_INFINITY;

			// Coolant
			coolant.setThermalResistance(alphaCoolant);
			coolant.setFlowRate(coolantProperties);
			coolant.setHeatSource(0.0);
			coolant.setTemperatureAmb(massCooled.getTemperature().getValue());
			coolant.setTemperatureIn(coolantIn.getTemperature());

			// Thermal flows
			massCooled.setHeatInput(motor.getOutput("PLoss").getValue());
			massCooled.addHeatInput(coolant.getHeatLoss());

			// Update submodels
			coolant.integrate(timestep, 0, 0, coolantProperties.getPressure());
		}
		if (lubricantConnected) {
			// Thermal resistance
			if (hasThermalStorrage)
				alphaLubricant = ductLubricant.getThermalResistance(lubricantProperties.getFlowRate(),
						lubricantProperties.getPressureIn(), lubricant.getTemperature().getValue(),
						massCooled.getTemperature().getValue());
			else
				alphaLubricant = Double.POSITIVE_INFINITY;

			// Coolant
			lubricant.setThermalResistance(alphaLubricant);
			lubricant.setFlowRate(lubricantProperties);
			lubricant.setHeatSource(0.0);
			lubricant.setTemperatureAmb(massCooled.getTemperature().getValue());
			lubricant.setTemperatureIn(lubricantIn.getTemperature());

			// Thermal flows
			// massCooled.addHeatInput(lubricant.getHeatLoss());

			// Update submodel
			lubricant.integrate(timestep, 0, 0, lubricantProperties.getPressure());
		}

		if (hasThermalStorrage) {
			massCooled.addHeatInput(motor.getOutput("PLoss").getValue());
			massCooled.integrate(timestep);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.ethz.inspire.emod.model.APhysicalComponent#getType()
	 */
	@Override
	public String getType() {
		return type;
	}

	@Override
	public void setType(String type) {
		this.type = type;
	}

	@Override
	public ArrayList<FluidCircuitProperties> getFluidPropertiesList() {
		ArrayList<FluidCircuitProperties> out = new ArrayList<FluidCircuitProperties>();
		if (coolantConnected)
			out.add(coolantProperties);
		if (lubricantConnected)
			out.add(lubricantProperties);
		return out;
	}

	@Override
	public void flood() {
		if (coolantProperties.getPost().size() != 0 | coolantProperties.getPre().size() != 0)
			coolantConnected = true;
		if (lubricantProperties.getPost().size() != 0 | lubricantProperties.getPre().size() != 0)
			lubricantConnected = true;

	}

	@Override
	public void updateBoundaryConditions() {
		motor.updateBoundaryConditions();
		if (bearings != null)
			for (Bearing b : bearings)
				b.updateBoundaryConditions();

		if (null != transmission)
			transmission.updateBoundaryConditions();

		bcHeatSrcBrake.setValue(state.getValue() * powerBrakeOff + (state.getValue() - 1) * powerBrakeOn);

		if (!coolantConnected) {
			bcCoolantTemperature.setValue(Double.NaN);
			bcCoolantHTC.setValue(Double.NaN);
		} else {
			bcCoolantTemperature.setValue(coolant.getTemperature().getValue());
			bcCoolantHTC.setValue(alphaCoolant);
		}

	}

	@Override
	public ParameterCheckReport checkConfigParams() {
		ParameterCheckReport report = new ParameterCheckReport();
		// Check model parameters:
		// Parameter must be non negative
		if (inertia < 0) {
			report.add("Inertia", "Negative value: Inertia must be non negative");
		}
		return report;
	}

	@Override
	public void loadParameters() {

		super.loadParameters();

		inertia = parameterSet.getPhysicalValue("Inertia").getValue();
		mass = parameterSet.getPhysicalValue("StructureMass").getValue();
		powerBrakeOn = parameterSet.getPhysicalValue("PowerBreakOn").getValue();
		powerBrakeOff = parameterSet.getPhysicalValue("PowerBreakOff").getValue();
		movingMass = parameterSet.getPhysicalValue("Mass").getValue();
		lever = parameterSet.getPhysicalValue("Lever").getValue();
		hasThermalStorrage = parameterSet.getBoolean("HasThermalStorage");

		/* Sub Model Motor */
		motor = (AMotor) parameterSet.getModel("MotorType");
		if(parameterSet.getModel("MotorType", true).getFilter().equals("")) {
			parameterSet.getModel("MotorType", true).setFilter("Motor");
			parameterSet.save(getParameterFile());
		}

		/* Sub Model Transmission */
		transmission = (Transmission) parameterSet.getModel("TransmissionType");
		if(parameterSet.getModel("TransmissionType", true).getFilter().equals("")) {
			parameterSet.getModel("TransmissionType", true).setFilter("Transmission");
			parameterSet.save(getParameterFile());
		}

		/* Sub Model Bearing */
		bearings = new ArrayList<>();
		for (APhysicalComponent b : parameterSet.getModels("BearingType"))
			if(b instanceof Bearing)
				bearings.add((Bearing) b);
		if(parameterSet.getModels("BearingType", true).getFilter().equals("")) {
			parameterSet.getModels("BearingType", true).setFilter("Bearing");
			parameterSet.save(getParameterFile());
		}

		/* Duct */
		ductCoolant = parameterSet.getDuct("StructureDuct");
		ductLubricant = parameterSet.getDuct("LubricantDuct");
		coolant.setVolume(ductCoolant.getVolume());
		lubricant.setVolume(ductLubricant.getVolume());
		massCooled.setMaterial(parameterSet.getMaterial("StructureMaterial"));
		massCooled.setMass(mass);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.zuestengineering.emod.model.APhysicalComponent#updateParameterSet()
	 */
	@Override
	protected void updateParameterSet() {
		logger.warning(
				"Parameter set is empty; tying for old parameter set format (this option will be removed in later versions) ...");

		ComponentConfigReader params = null;
		/* Open file containing the parameters of the model type: */
		try {
			params = new ComponentConfigReader(getModelType(), type);
		} catch (Exception e) {
			e.printStackTrace();
		}

		/* Read the config parameter: */
		try {
			String transmissionType = params.getValue("TransmissionType", "");
			String[] bearingType = params.getValue("BearingType", new String[0]);
			inertia = params.getPhysicalValue("Inertia", new SiUnit("kg m^2")).getValue();
			String motorType = params.getString("MotorType");
			mass = params.getPhysicalValue("StructureMass", new SiUnit("kg")).getValue();
			powerBrakeOn = params.getPhysicalValue("PowerBreakOn", new SiUnit("W")).getValue();
			powerBrakeOff = params.getPhysicalValue("PowerBreakOff", new SiUnit("W")).getValue();
			movingMass = params.getPhysicalValue("Mass", new SiUnit("kg")).getValue();
			lever = params.getPhysicalValue("Lever", new SiUnit("m")).getValue();
			hasThermalStorrage = params.getValue("HasThermalStorage", true);

			/* Sub Model Motor */
			String[] mdlType = motorType.split("_", 2);

			// Create Sub Element
			try {
				// Get class and constructor objects
				String path = APhysicalComponent.class.getPackage().getName();
				Class<?> cl = Class.forName(path + ".pm." + mdlType[0]);
				Constructor<?> co = cl.getConstructor(String.class);
				// initialize new component
				motor = (AMotor) co.newInstance(mdlType[1]);
			} catch (Exception e) {
				Exception ex = new Exception(
						"Unable to create component " + mdlType[0] + "(" + mdlType[1] + ")" + " : " + e.getMessage());
				ex.printStackTrace();
				motor = null;
			}

			/* Sub Model Transmission */
			if (transmissionType.equals(""))
				transmission = null;
			else
				transmission = new Transmission(transmissionType);

			/* Sub Model Bearing */
			if (bearingType[0].equals(""))
				bearings = null;
			else {
				bearings = new ArrayList<>();
				for (int i = 0; i < bearingType.length; i++)
					bearings.add(new Bearing(bearingType[i]));
			}

			/* Duct */
			ductCoolant.clone(
					Duct.buildFromFile(getModelType(), getType(), params.getValue("StructureDuct", "StructureDuct")));
			ductLubricant.clone(
					Duct.buildFromFile(getModelType(), getType(), params.getValue("LubricantDuct", "LubricantDuct")));
			coolant.setVolume(ductCoolant.getVolume());
			lubricant.setVolume(ductLubricant.getVolume());
			massCooled.setMaterial(params.getString("StructureMaterial"));
			massCooled.setMass(mass);

		} catch (Exception e) {
			e.printStackTrace();
		}
		params.Close(); /* Model configuration file not needed anymore. */

		parameterSet = new ParameterSet(this.toString());
		parameterSet.setModel("MotorType", motor);
		parameterSet.setModel("TransmissionType", transmission);
		parameterSet.setBearings("BearingType", bearings);
		parameterSet.setPhysicalValue("Inertia", inertia, new SiUnit("kg m^2"));
		parameterSet.setPhysicalValue("StructureMass", mass, new SiUnit("kg"));
		parameterSet.setPhysicalValue("PowerBreakOn", powerBrakeOn, new SiUnit("W"));
		parameterSet.setPhysicalValue("PowerBreakOff", powerBrakeOff, new SiUnit("W"));
		parameterSet.setPhysicalValue("Mass", movingMass, new SiUnit("kg"));
		parameterSet.setPhysicalValue("Lever", lever, new SiUnit("m"));
		parameterSet.setBoolean("HasThermalStorage", hasThermalStorrage);
		parameterSet.setDuct("StructureDuct", ductCoolant);
		parameterSet.setDuct("LubricantDuct", ductLubricant);
		parameterSet.setMaterial("StructureMaterial", massCooled.getMaterial());

		parameterSet.save(getParameterFile());
		logger.info("Parameter file successfully converted to new format");

	}

}
