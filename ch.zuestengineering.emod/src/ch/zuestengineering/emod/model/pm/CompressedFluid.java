/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.model.pm;

import java.util.ArrayList;

import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import ch.zuestengineering.emod.femexport.BoundaryCondition;
import ch.zuestengineering.emod.femexport.BoundaryConditionType;
import ch.zuestengineering.emod.model.APhysicalComponent;
import ch.zuestengineering.emod.model.ParameterCheckReport;
import ch.zuestengineering.emod.model.linking.IOContainer;
import ch.zuestengineering.emod.model.parameters.ParameterSet;
import ch.zuestengineering.emod.model.units.*;
import ch.zuestengineering.emod.utils.ComponentConfigReader;

/**
 * General compressed fluid class. Implements the physical of a compressed fluid
 * consumer
 * 
 * Assumptions: Ideal gas equation, isentropic expansion
 * 
 * Inputlist: 1: Flow : [m3/s] : Demanded flow under ambient conditions 2:
 * TemperatureAmb : [K] : Ambient temperature 3: PressureAmb : [Pa] : Ambient
 * Pressure Outputlist: 1: PTotal : [W] : Power demand to generate the flow 2:
 * PUse : [W] : Power available in the fluid 3: PLoss : [W] : Power loss during
 * the generation
 * 
 * Config parameters: Density : [N/mm] : density of the fluid under ambient
 * conditions HeatCapacity : [J/kg/K] : internal heat capacity of the fluid
 * IsentropicCoefficient : [-] : isentropic coefficent of the fluid
 * SupplyPressure : [Pa] : pressure of the fluid in the supply
 * 
 * @author sizuest
 * 
 */
@XmlRootElement
public class CompressedFluid extends APhysicalComponent {

	@XmlElement
	protected String type;

	// Input parameters:
	private IOContainer vFlowDot;
	private IOContainer tempAmb;
	private IOContainer pAmb;
	// Output parameters:
	private IOContainer ptotal;
	private IOContainer puse;
	private IOContainer ploss;
	// Boundary conditions
	private BoundaryCondition bcHeatSource;

	// Parameters used by the model.
	private double rho; // [kg/m3] Fluid density at
						// normal conditions
	private double cp; // [J/K/kg] Heat capacity
	private double gamma; // [-] Isentropic exponent
	private double psupply; // [Pa] Supply pressure

	/**
	 * Constructor called from XmlUnmarshaller. Attribute 'type' is set by
	 * XmlUnmarshaller.
	 */
	public CompressedFluid() {
		super();
	}

	/**
	 * post xml init method (loading physics data)
	 * 
	 * @param u
	 * @param parent
	 */
	public void afterUnmarshal(final Unmarshaller u, final Object parent) {
		init();
	}

	/**
	 * Compressed fluid constructor
	 * 
	 * @param type
	 */
	public CompressedFluid(String type) {
		super();

		this.type = type;
		init();
	}

	/**
	 * Called from constructor or after unmarshaller.
	 */
	private void init() {
		/* Define Input parameters */
		inputs = new ArrayList<IOContainer>();
		vFlowDot = new IOContainer("Flow", new SiUnit(Unit.METERCUBIC_S), 0, ContainerType.FLUIDDYNAMIC);
		tempAmb = new IOContainer("TemperatureAmb", new SiUnit(Unit.KELVIN), 0, ContainerType.THERMAL);
		pAmb = new IOContainer("PressureAmb", new SiUnit(Unit.PA), 100000, ContainerType.FLUIDDYNAMIC);
		inputs.add(vFlowDot);
		inputs.add(tempAmb);
		inputs.add(pAmb);

		/* Define output parameters */
		outputs = new ArrayList<IOContainer>();
		ptotal = new IOContainer("PTotal", new SiUnit(Unit.WATT), 0, ContainerType.ELECTRIC);
		puse = new IOContainer("PUse", new SiUnit(Unit.WATT), 0, ContainerType.FLUIDDYNAMIC);
		ploss = new IOContainer("PLoss", new SiUnit(Unit.WATT), 0, ContainerType.THERMAL);
		outputs.add(ptotal);
		outputs.add(puse);
		outputs.add(ploss);

		/* Boundary conditions */
		boundaryConditions = new ArrayList<BoundaryCondition>();
		bcHeatSource = new BoundaryCondition("HeatSrc", new SiUnit("W"), 0, BoundaryConditionType.NEUMANN);
		boundaryConditions.add(bcHeatSource);

		/* Read configuration parameters: */
		loadParameters();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.ethz.inspire.emod.model.APhysicalComponent#update()
	 */
	@Override
	public void update() {

		if (pAmb.getValue() == 0) {
			ptotal.setValue(0);
			puse.setValue(0);
			ploss.setValue(0);
			return;
		}
		/*
		 * Calculate power required to generate the flow PTotal [W] = cp [J/kg/K] * Tamb
		 * [K] * [ (psupply [Pa] /pamb [Pa] )^{(kappa-1)/kappa} ] * rho [kg/m3] * Vdot
		 * [m3/s]
		 */
		ptotal.setValue(cp * tempAmb.getValue() * (Math.pow(psupply / pAmb.getValue(), gamma - 1) - 1) * rho
				* vFlowDot.getValue());
		/*
		 * Calculate power flow trough fluid PUse [W] = psupply [Pa] * Vdot [m³/s]
		 */
		puse.setValue(psupply * vFlowDot.getValue());
		/*
		 * Calculate power loss during the generation PLoss [W] = PTotal-PUse
		 */
		ploss.setValue(ptotal.getValue() - puse.getValue());

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.ethz.inspire.emod.model.APhysicalComponent#getType()
	 */
	@Override
	public String getType() {
		return type;
	}

	@Override
	public void setType(String type) {
		this.type = type;
	}

	@Override
	public void updateBoundaryConditions() {
		bcHeatSource.setValue(0); // TODO
	}

	@Override
	public ParameterCheckReport checkConfigParams() {
		ParameterCheckReport report = new ParameterCheckReport();
		// Check model parameters:
		// Parameter must be non negative and non zero
		if (rho <= 0) {
			report.add("Density", "Non positive value: Density must be non negative and non zero");
		}
		if (cp <= 0) {
			report.add("HeatCapacity", "Non positive value: Heat capacity must be non negative and non zero");
		}
		if (gamma <= 0) {
			report.add("IsentropicCoefficient",
					"Non positive value: Isentropic coefficient must be non negative and non zero");
		}
		if (psupply <= 0) {
			report.add("SupplyPressure", "Non positive value: Supply pressure must be non negative and non zero");
		}

		return report;
	}

	@Override
	public void loadParameters() {

		super.loadParameters();

		rho = parameterSet.getPhysicalValue("Density").getValue();
		cp = parameterSet.getPhysicalValue("HeatCapacity").getValue();
		gamma = parameterSet.getPhysicalValue("IsentropicCoefficient").getValue();
		psupply = parameterSet.getPhysicalValue("SupplyPressure").getValue();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.zuestengineering.emod.model.APhysicalComponent#updateParameterSet()
	 */
	@Override
	protected void updateParameterSet() {
		logger.warning(
				"Parameter set is empty; tying for old parameter set format (this option will be removed in later versions) ...");
		ComponentConfigReader params = null;
		/* Open file containing the parameters of the model type: */
		try {
			params = new ComponentConfigReader(getModelType(), type);
		} catch (Exception e) {
			e.printStackTrace();
		}

		/* Read the config parameter: */
		try {
			rho = params.getPhysicalValue("Density", new SiUnit("kg m^-3")).getValue();
			cp = params.getPhysicalValue("HeatCapacity", new SiUnit("J kg^-1 K^-1")).getValue();
			gamma = params.getPhysicalValue("IsentropicCoefficient", new SiUnit("")).getValue();
			psupply = params.getPhysicalValue("SupplyPressure", new SiUnit("Pa")).getValue();
		} catch (Exception e) {
			e.printStackTrace();
		}
		params.Close(); /* Model configuration file not needed anymore. */

		parameterSet = new ParameterSet(this.toString());
		parameterSet.setPhysicalValue("Density", rho, new SiUnit("kg m^-3"));
		parameterSet.setPhysicalValue("HeatCapacity", cp, new SiUnit("J kg^-1 K^-1"));
		parameterSet.setPhysicalValue("IsentropicCoefficient", gamma, new SiUnit(""));
		parameterSet.setPhysicalValue("SupplyPressure", psupply, new SiUnit("Pa"));
		parameterSet.save(getParameterFile());
		logger.info("Parameter file successfully converted to new format");
	}
}
