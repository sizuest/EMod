/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.model.pm;

import java.util.ArrayList;

import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import ch.zuestengineering.emod.model.APhysicalComponent;
import ch.zuestengineering.emod.model.ParameterCheckReport;
import ch.zuestengineering.emod.model.fluid.FECBypass;
import ch.zuestengineering.emod.model.fluid.Floodable;
import ch.zuestengineering.emod.model.fluid.FluidCircuitProperties;
import ch.zuestengineering.emod.model.linking.FluidContainer;
import ch.zuestengineering.emod.model.linking.IOContainer;
import ch.zuestengineering.emod.model.parameters.ParameterSet;
import ch.zuestengineering.emod.model.parameters.PhysicalValue;
import ch.zuestengineering.emod.model.units.ContainerType;
import ch.zuestengineering.emod.model.units.SiUnit;
import ch.zuestengineering.emod.model.units.Unit;
import ch.zuestengineering.emod.simulation.DynamicState;
import ch.zuestengineering.emod.utils.ComponentConfigReader;

/**
 * General Bypass model class. Implements the physical model of a bypass.
 * 
 * Assumptions:
 * 
 * 
 * 
 * Inputlist: 1: FluidIn : [-]
 * 
 * Outputlist: 1: FluidOut : [-] 2: Zeta : [-] : Debug
 * 
 * Config parameters: PressureMax : [Pa]
 * 
 * @author kraandre
 * 
 */
@XmlRootElement
public class Bypass extends APhysicalComponent implements Floodable {

	@XmlElement
	protected String type;

	// Input parameters:
	private FluidContainer fluidIn;

	// Output parameters:
	private FluidContainer fluidOut;
	private IOContainer valvePosition;

	// Parameters used by the model.
	private double pressureMax;
	private double zeta;
	private double area;

	private double sr = 0;

	// Fluid properties
	private FluidCircuitProperties fluidProperties;
	private FECBypass bypassCharacteristics;

	private DynamicState temperature;

	/**
	 * Constructor called from XmlUnmarshaller. Attribute 'type' is set by
	 * XmlUnmarshaller.
	 */
	public Bypass() {
		super();
	}

	/**
	 * @param u
	 * @param parent
	 */
	public void afterUnmarshal(Unmarshaller u, Object parent) {
		// post xml init method (loading physics data)
		init();
	}

	/**
	 * Valve constructor
	 * 
	 * @param type
	 */
	public Bypass(String type) {
		super();

		this.type = type;
		init();
	}

	/**
	 * Called from constructor or after unmarshaller.
	 */
	private void init() {

		/* Define Input parameters */
		inputs = new ArrayList<IOContainer>();

		bypassCharacteristics = new FECBypass(this);
		temperature = new DynamicState("Temperature", new SiUnit(Unit.KELVIN));

		/* Fluid Properties */
		fluidProperties = new FluidCircuitProperties(bypassCharacteristics, temperature);

		/* Define output parameters */
		outputs = new ArrayList<IOContainer>();

		/* Fluid in- and output */
		fluidIn = new FluidContainer("FluidIn", new SiUnit(Unit.NONE), ContainerType.FLUIDDYNAMIC, fluidProperties);
		fluidOut = new FluidContainer("FluidOut", new SiUnit(Unit.NONE), ContainerType.FLUIDDYNAMIC, fluidProperties);
		valvePosition = new IOContainer("ValvePosition", new SiUnit(""), 0, ContainerType.INFORMATION);
		inputs.add(fluidIn);
		outputs.add(fluidOut);
		outputs.add(valvePosition);

		/* Read configuration parameters: */
		loadParameters();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.ethz.inspire.emod.model.APhysicalComponent#update()
	 */
	@Override
	public void update() {

		// Adjust temperature out
		temperature.setInitialCondition(fluidProperties.getTemperatureIn());
		if (fluidProperties.getFlowRate() != 0)
			temperature.addValue(fluidProperties.getInternalLoss()
					/ (fluidProperties.getMaterial().getHeatCapacity(fluidProperties) * fluidProperties.getMassFlowRate()));

		sr = getSr(fluidProperties.getPressureDrop(), 0.1);
		valvePosition.setValue(sr);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.ethz.inspire.emod.model.APhysicalComponent#getType()
	 */
	@Override
	public String getType() {
		return type;
	}

	@Override
	public void setType(String type) {
		this.type = type;
	}

	@Override
	public ArrayList<FluidCircuitProperties> getFluidPropertiesList() {
		ArrayList<FluidCircuitProperties> out = new ArrayList<FluidCircuitProperties>();
		out.add(fluidProperties);
		return out;
	}

	@Override
	public void flood() {/* Not used */
	}

	@Override
	public void updateBoundaryConditions() {
		// TODO Auto-generated method stub

	}

	/**
	 * @param pressure
	 * @param up
	 * @return
	 */
	public double getSr(double pressure, double up) {
		// double ar = 10.0/up/pressureMax;

		// return 0.5 * ( 1 + ar * (pressure - pressureMax) /
		// Math.sqrt(1+ar*ar*Math.pow(pressure - pressureMax, 2.0)));

		if (pressure >= getPressureMax())
			return 1;
		else
			return 0;

	}

	/**
	 * Returns the artificial pressure loss coefficient:
	 * 
	 * p = zetaBar*VDot^2
	 * 
	 * @param pressure
	 * @return
	 */
	public double getZetaBar(double pressure) {
		double rho = fluidProperties.getMaterial().getDensity(fluidProperties.getTemperatureIn(), pressure);
		sr = getSr(pressure, 0.1);

		return rho * zeta / Math.pow(area * sr, 2.0) / 2.0;
	}

	/**
	 * Returns the begin of the vale opening
	 * 
	 * @return
	 */
	public double getPressureMax() {
		return pressureMax;
	}

	@Override
	public ParameterCheckReport checkConfigParams() {
		ParameterCheckReport report = new ParameterCheckReport();

		if (pressureMax <= 0) {
			report.add("PressureMax", "Dimension missmatch: PressureMax must be greater than zero!");
		}

		return report;
	}

	@Override
	public void loadParameters() {

		super.loadParameters();

		pressureMax = parameterSet.getPhysicalValue("PressureMax").getValue();
		zeta = parameterSet.getPhysicalValue("PressureLossCoefficient").getValue();
		area = parameterSet.getPhysicalValue("Area").getValue();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.zuestengineering.emod.model.APhysicalComponent#updateParameterSet()
	 */
	@Override
	protected void updateParameterSet() {
		logger.warning(
				"Parameter set is empty; tying for old parameter set format (this option will be removed in later versions) ...");
		ComponentConfigReader params = null;
		/* Open file containing the parameters of the model type: */
		try {
			params = new ComponentConfigReader(getModelType(), type);
		} catch (Exception e) {
			e.printStackTrace();
		}

		/* Read the config parameter: */
		try {
			pressureMax = params.getValue("PressureMax", new PhysicalValue(1.5E6, "Pa")).getValue();
			zeta = params.getValue("PressureLossCoefficient", new PhysicalValue(5, "")).getValue();
			area = params.getValue("Area", new PhysicalValue(5.0E-4, "m^2")).getValue();
			params.saveValues();
		} catch (Exception e) {
			e.printStackTrace();
		}
		params.Close(); /* Model configuration file not needed anymore. */

		parameterSet = new ParameterSet(this.toString());
		parameterSet.setPhysicalValue("PressureMax", pressureMax, new SiUnit("Pa"));
		parameterSet.setPhysicalValue("PressureLossCoefficient", zeta, new SiUnit(""));
		parameterSet.setPhysicalValue("Area", area, new SiUnit("m^2"));
		parameterSet.save(getParameterFile());
		logger.info("Parameter file successfully converted to new format");
	}
}
