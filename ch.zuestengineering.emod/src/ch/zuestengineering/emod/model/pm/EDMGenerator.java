/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.model.pm;

import java.util.ArrayList;

import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import ch.zuestengineering.emod.dd.Duct;
import ch.zuestengineering.emod.femexport.BoundaryCondition;
import ch.zuestengineering.emod.femexport.BoundaryConditionType;
import ch.zuestengineering.emod.model.APhysicalComponent;
import ch.zuestengineering.emod.model.ParameterCheckReport;
import ch.zuestengineering.emod.model.fluid.FECDuct;
import ch.zuestengineering.emod.model.fluid.Floodable;
import ch.zuestengineering.emod.model.fluid.FluidCircuitProperties;
import ch.zuestengineering.emod.model.linking.FluidContainer;
import ch.zuestengineering.emod.model.linking.IOContainer;
import ch.zuestengineering.emod.model.material.Material;
import ch.zuestengineering.emod.model.parameters.ParameterSet;
import ch.zuestengineering.emod.model.thermal.ThermalArray;
import ch.zuestengineering.emod.model.thermal.ThermalElement;
import ch.zuestengineering.emod.model.units.ContainerType;
import ch.zuestengineering.emod.model.units.SiUnit;
import ch.zuestengineering.emod.model.units.Unit;
import ch.zuestengineering.emod.simulation.DynamicState;
import ch.zuestengineering.emod.utils.Algo;

/**
 * Implements the physical model of a edm generator with a heat sorce and
 * a thermal interface to a fluid (coolant)
 * 
 * @author Simon Z�st
 *
 */
@XmlRootElement
public class EDMGenerator extends APhysicalComponent implements Floodable {

	@XmlElement
	protected String type;

	// Inputs
	protected IOContainer level;
	protected FluidContainer fluidIn;
	// Outputs
	protected IOContainer ptotal;
	protected IOContainer puse;
	protected IOContainer ploss;
	protected FluidContainer fluidOut;
	// Fluid Properties
	protected FluidCircuitProperties fluidProperties;

	// Model parameters
	protected double maxPower;
	protected double ctrlPower;
	protected double[] powerSamples;
	protected double[] efficiencySamples;

	// Intermediate results
	private double currentHTC;
	private double currentHeatTransfer;
	private double lastTemperatureIn;

	// Sub models
	protected Duct duct;
	protected ThermalElement structure;
	protected ThermalArray fluid;

	// Boundary conditions
	protected BoundaryCondition bcHeatSource;
	protected BoundaryCondition bcHeatFlux;
	protected BoundaryCondition bcFluidTemperature;
	protected BoundaryCondition bcHTC;
	protected BoundaryCondition bcStructureTemperature;

	/**
	 * Constructor called from XmlUnmarshaller. Attribute 'type' is set by
	 * XmlUnmarshaller.
	 */
	public EDMGenerator() {
		super();
	}

	/**
	 * @param u
	 * @param parent
	 */
	public void afterUnmarshal(Unmarshaller u, Object parent) {
		// post xml init method (loading physics data)
		init();
	}

	/**
	 * CooledHeatSource constructor
	 * 
	 * @param type
	 */
	public EDMGenerator(String type) {
		super();

		this.type = type;
		init();
	}

	/**
	 * Initialize the model
	 */
	private void init() {
		// Inputs
		inputs = new ArrayList<IOContainer>();
		level = new IOContainer("Level", new SiUnit(""), 0, ContainerType.CONTROL);
		inputs.add(level);
		
		// Outputs
		outputs = new ArrayList<IOContainer>();
		ptotal = new IOContainer("PTotal", new SiUnit("W"), 0, ContainerType.ELECTRIC);
		ploss = new IOContainer( "PLoss",  new SiUnit("W"), 0, ContainerType.THERMAL);
		puse = new IOContainer(  "PUse",   new SiUnit("W"), 0, ContainerType.ELECTRIC);

		outputs.add(ptotal);
		outputs.add(ploss);
		outputs.add(puse);
		
		/* Read configuration parameters: */
		loadParameters();

		// Add states
		dynamicStates = new ArrayList<DynamicState>();
		dynamicStates.add(0, structure.getTemperature());

		// Change state names
		structure.getTemperature().setName("TemperatureStructure");


		/* Thermal models */
		fluid = new ThermalArray("Example", duct.getVolume(), 20);

		/* Fluid properties */
		fluidProperties = new FluidCircuitProperties(new FECDuct(duct, fluid.getTemperature()),	fluid.getTemperature());

		/* Define fluid in-/outputs */
		fluidIn  = new FluidContainer("CoolantIn",  new SiUnit(Unit.NONE), ContainerType.FLUIDDYNAMIC, fluidProperties);
		fluidOut = new FluidContainer("CoolantOut", new SiUnit(Unit.NONE), ContainerType.FLUIDDYNAMIC, fluidProperties);
		inputs.add(fluidIn);
		outputs.add(fluidOut);
		// Change state names
		fluid.getTemperature().setName("TemperatureCoolant");
		// Add state
		dynamicStates.add(1, fluid.getTemperature());

		/* Fluid circuit parameters */
		fluid.setMaterial(fluidProperties.getMaterial());
		duct.setMaterial(fluidProperties.getMaterial());

		/* Boundary conditions */
		boundaryConditions = new ArrayList<BoundaryCondition>();

		bcHTC = new BoundaryCondition("CoolantHTC", new SiUnit("W/K"), 0, BoundaryConditionType.ROBIN);
		bcFluidTemperature = new BoundaryCondition("CoolantTemperature", new SiUnit("K"), 293.15, BoundaryConditionType.ROBIN);
		bcHeatFlux = new BoundaryCondition("CoolantHeatFlux", new SiUnit("W"), 0, BoundaryConditionType.NEUMANN);
		bcHeatSource = new BoundaryCondition("HeatSource", new SiUnit("W"), 0, BoundaryConditionType.NEUMANN);
		bcStructureTemperature = new BoundaryCondition("StructureTemperature", new SiUnit("K"), 293.15,	BoundaryConditionType.DIRICHLET);

		boundaryConditions.add(bcHTC);
		boundaryConditions.add(bcFluidTemperature);
		boundaryConditions.add(bcHeatFlux);
		boundaryConditions.add(bcHeatSource);
		boundaryConditions.add(bcStructureTemperature);
	}

	@Override
	public ArrayList<FluidCircuitProperties> getFluidPropertiesList() {
		ArrayList<FluidCircuitProperties> out = new ArrayList<FluidCircuitProperties>();
		out.add(fluidProperties);
		return out;
	}

	@Override
	public void flood() {
	}

	@Override
	public String getType() {
		return type;
	}

	@Override
	public void update() {
		if (fluidIn.getTemperature() <= 0) {
			if (Double.isNaN(lastTemperatureIn))
				lastTemperatureIn = structure.getTemperature().getValue();
		} else
			lastTemperatureIn = fluidIn.getTemperature();
		
		
		// Electrics
		double efficiency = Algo.linearInterpolation(maxPower*level.getValue(), powerSamples, efficiencySamples, false);
		puse.setValue(efficiency*maxPower*level.getValue());
		ptotal.setValue(maxPower*level.getValue()+ctrlPower);
		ploss.setValue((1.0-efficiency)*maxPower*level.getValue()+ctrlPower);

		// Thermal resistance
		currentHTC = duct.getThermalResistance(fluidProperties.getFlowRate(),
				fluidProperties.getPressureIn(), fluidProperties.getTemperatureIn(),
				structure.getTemperature().getValue());

		// Coolant
		fluid.setThermalResistance(currentHTC);
		fluid.setFlowRate(fluidProperties);
		fluid.setHeatSource(0.0);
		fluid.setTemperatureAmb(structure.getTemperature().getValue());
		fluid.setTemperatureIn(fluidIn.getTemperature());

		// Thermal flows
		structure.setHeatInput(ploss.getValue());
		structure.addHeatInput(fluid.getHeatLoss());

		// Update submodels
		structure.integrate(timestep);
		// TODO set Pressure!
		fluid.integrate(timestep, 0, 0, 100000);
	}
	
	@Override
	public void loadParameters() {
		super.loadParameters();
		
		/* Structure Mass */
		double massStructure       = parameterSet.getDouble("StructureMass");
		Material materialStructure = parameterSet.getMaterial("StructureMaterial");
		structure = new ThermalElement(materialStructure, massStructure);
		
		/* Efficiency Map*/
		ctrlPower         = parameterSet.getDouble("ControlPower");
		powerSamples      = parameterSet.getVector("PowerSamples");
		efficiencySamples = parameterSet.getVector("EfficiencySamples");
		maxPower = Algo.getMaximum(powerSamples);
		
		/* duct */
		duct = parameterSet.getDuct("Duct");
		
	}

	@Override
	public void updateBoundaryConditions() {
		bcStructureTemperature.setValue(structure.getTemperature().getValue());
		bcHeatSource.setValue(ploss.getValue());

		bcHTC.setValue(currentHTC);
		bcFluidTemperature.setValue(fluid.getTemperature().getValue());
		bcHeatFlux.setValue(currentHeatTransfer);
	}

	@Override
	public void setType(String type) {
		this.type = type;
	}

	@Override
	public ParameterCheckReport checkConfigParams() {
		ParameterCheckReport report = new ParameterCheckReport();
		
		if (ctrlPower < 0) {
			report.add("ControlPower", "Negative value: ControlPower must be non negative");
		}
		for(double d: efficiencySamples) {
			if (d < 0) {
				report.add("EfficiencySamples", "Negative value: EfficiencySamples must be non negative");
			}
			else if (d > 1) {
				report.add("EfficiencySamples", "Non nominal value: EfficiencySamples must be less or equal 1");
			}
		}
		
		for(double d: powerSamples)
			if (d < 0) {
				report.add("PowerSamples", "Negative value: PowerSamples must be non negative");
			}
		
		for(int i=0; i<powerSamples.length-1; i++)
			if (powerSamples[i] > powerSamples[i+1]) {
				report.add("PowerSamples", "Unsorted: PowerSamples must be sorted");
			}
		
		return report;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.zuestengineering.emod.model.APhysicalComponent#updateParameterSet()
	 */
	@Override
	protected void updateParameterSet() {
		logger.warning("Parameter set is empty ...");
		
		parameterSet = new ParameterSet(this.toString());
		
		parameterSet.setDouble("StructureMass", 1.0);
		parameterSet.setMaterial("StructureMaterial", new Material("Steel"));
		parameterSet.setDouble("ControlPower", 10);
		parameterSet.setVector("PowerSamples", new double[] {0, 100, 500, 1000, 2000, 3000});
		parameterSet.setVector("EfficiencySamples", new double[] {0, .01, .02, .05, .06, .07});
		parameterSet.setDuct("Duct", new Duct());
		
		parameterSet.save(getParameterFile());
		logger.info("Parameter file successfully created");

	}

}
