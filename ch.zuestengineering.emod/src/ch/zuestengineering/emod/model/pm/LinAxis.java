/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.model.pm;

import java.lang.reflect.Constructor;
import java.util.ArrayList;

import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import ch.zuestengineering.emod.dd.Duct;
import ch.zuestengineering.emod.femexport.BoundaryCondition;
import ch.zuestengineering.emod.femexport.BoundaryConditionType;
import ch.zuestengineering.emod.model.APhysicalComponent;
import ch.zuestengineering.emod.model.ParameterCheckReport;
import ch.zuestengineering.emod.model.fluid.FECDuct;
import ch.zuestengineering.emod.model.fluid.Floodable;
import ch.zuestengineering.emod.model.fluid.FluidCircuitProperties;
import ch.zuestengineering.emod.model.linking.FluidContainer;
import ch.zuestengineering.emod.model.linking.IOContainer;
import ch.zuestengineering.emod.model.parameters.ParameterSet;
import ch.zuestengineering.emod.model.thermal.ThermalArray;
import ch.zuestengineering.emod.model.thermal.ThermalElement;
import ch.zuestengineering.emod.model.units.*;
import ch.zuestengineering.emod.simulation.DynamicState;
import ch.zuestengineering.emod.utils.ComponentConfigReader;

/**
 * General linear axis model class. Implements the physical model of a linear
 * axis. From the input parameter translational speed and process force, the
 * requested motor torque is calculated
 * 
 * Assumptions: The inertia of mass and frictional forces are negligible
 * 
 * @author simon
 * 
 */
@XmlRootElement
public class LinAxis extends APhysicalComponent implements Floodable {

	@XmlElement
	protected String type;

	// Input parameters:
	private IOContainer state;
	private IOContainer speed;
	private IOContainer force;
	private FluidContainer coolantIn;
	private FluidContainer lubricantIn;
	// Output parameters:
	private IOContainer puse;
	private IOContainer ploss;
	private IOContainer ptotal;
	private FluidContainer coolantOut;
	private FluidContainer lubricantOut;
	// Boundary conditions
	private BoundaryCondition bcHeatSrcTransmission;
	private BoundaryCondition bcHeatSrcBrake;
	private BoundaryCondition bcCoolantTemperature;
	private BoundaryCondition bcCoolantHTC;

	// Save last input values
	private double lastspeed, lastforce, lastrotspeed, lasttorque;
	private double plossTransmission;

	// Parameters used by the model.
	private double transmission;
	private double eta;
	private double massValue;
	private double alpha;
	private double powerBrakeOn, powerBrakeOff;

	// Global values
	double alphaCoolant = 0, alphaLubricant = 0;

	// Submodel
	private AMotor motor;
	private ArrayList<Bearing> bearings;
	private ThermalElement massCooled;
	private ThermalArray coolant, lubricant;
	private Duct ductCoolant, ductLubricant;
	private MovingMass massMoved;
	private boolean hasThermalStorrage;

	// FluidProperties
	private FluidCircuitProperties coolantProperties;
	private boolean coolantConnected = false;
	private FluidCircuitProperties lubricantProperties;
	private boolean lubricantConnected = false;

	/**
	 * Constructor called from XmlUnmarshaller. Attribute 'type' is set by
	 * XmlUnmarshaller.
	 */
	public LinAxis() {
		super();
	}

	/**
	 * post xml init method (loading physics data)
	 * 
	 * @param u
	 * @param parent
	 */
	public void afterUnmarshal(final Unmarshaller u, final Object parent) {
		init();
	}

	/**
	 * Linear Axis constructor
	 * 
	 * @param type
	 */
	public LinAxis(String type) {
		super();

		this.type = type;
		init();
	}

	/**
	 * Called from constructor or after unmarshaller.
	 */
	private void init() {
		/* Define Input parameters */
		inputs = new ArrayList<IOContainer>();
		state = new IOContainer("State", new SiUnit(Unit.NONE), 0, ContainerType.CONTROL);
		speed = new IOContainer("Speed", new SiUnit(Unit.M_S), 0, ContainerType.MECHANIC);
		force = new IOContainer("ProcessForce", new SiUnit(Unit.NEWTON), 0, ContainerType.MECHANIC);
		inputs.add(state);
		inputs.add(speed);
		inputs.add(force);

		/* Define output parameters */
		outputs = new ArrayList<IOContainer>();
		puse = new IOContainer("PUse", new SiUnit(Unit.WATT), 0, ContainerType.MECHANIC);
		ploss = new IOContainer("PLoss", new SiUnit(Unit.WATT), 0, ContainerType.THERMAL);
		ptotal = new IOContainer("PTotal", new SiUnit(Unit.WATT), 0, ContainerType.ELECTRIC);
		outputs.add(puse);
		outputs.add(ploss);
		outputs.add(ptotal);

		/* Initialize sub-model */
		massMoved = new MovingMass(massValue, 1, alpha, 0);

		/* Ducts */
		ductCoolant = new Duct();
		ductLubricant = new Duct();
		coolant = new ThermalArray("Example", 1, 20);
		lubricant = new ThermalArray("Example", 1, 20);
		massCooled = new ThermalElement("Example", 1);

		/* Read configuration parameters: */
		loadParameters();

		/* Fluid properties */
		coolantProperties = new FluidCircuitProperties(new FECDuct(ductCoolant, coolant.getTemperature()),
				coolant.getTemperature());
		lubricantProperties = new FluidCircuitProperties(new FECDuct(ductLubricant, lubricant.getTemperature()),
				lubricant.getTemperature());

		/* Define coolant in-/outputs */
		coolantIn = new FluidContainer("CoolantIn", new SiUnit(Unit.NONE), ContainerType.FLUIDDYNAMIC,
				coolantProperties);
		inputs.add(coolantIn);
		coolantOut = new FluidContainer("CoolantOut", new SiUnit(Unit.NONE), ContainerType.FLUIDDYNAMIC,
				coolantProperties);
		outputs.add(coolantOut);

		/* Define lubricant in-/outputs */
		lubricantIn = new FluidContainer("LubricantIn", new SiUnit(Unit.NONE), ContainerType.FLUIDDYNAMIC,
				lubricantProperties);
		inputs.add(lubricantIn);
		lubricantOut = new FluidContainer("LubricantOut", new SiUnit(Unit.NONE), ContainerType.FLUIDDYNAMIC,
				lubricantProperties);
		outputs.add(lubricantOut);

		// Change state name
		coolant.getTemperature().setName("TemperatureCoolant");
		lubricant.getTemperature().setName("TemperatureLubricant");
		massCooled.getTemperature().setName("TemperatureStructure");

		/* Fluid circuit parameters */
		coolant.setMaterial(coolantProperties.getMaterial());
		ductCoolant.setMaterial(coolantProperties.getMaterial());
		lubricant.setMaterial(lubricantProperties.getMaterial());
		ductLubricant.setMaterial(lubricantProperties.getMaterial());
		ductCoolant.setMaterial(coolantProperties.getMaterial());

		dynamicStates = new ArrayList<DynamicState>();
		dynamicStates.add(massMoved.getDynamicStateList().get(0));
		dynamicStates.add(1, massCooled.getTemperature());
		dynamicStates.add(2, coolant.getTemperature());
		dynamicStates.add(3, lubricant.getTemperature());

		/* Boundary conditions */
		boundaryConditions = new ArrayList<BoundaryCondition>();
		bcHeatSrcTransmission = new BoundaryCondition("TransmissionHeatSrc", new SiUnit("W"), 0,
				BoundaryConditionType.NEUMANN);
		bcHeatSrcBrake = new BoundaryCondition("BrakeHeatSrc", new SiUnit("W"), 0, BoundaryConditionType.NEUMANN);
		bcCoolantTemperature = new BoundaryCondition("CoolantTemperature", new SiUnit("K"), 293.15,
				BoundaryConditionType.ROBIN);
		bcCoolantHTC = new BoundaryCondition("CoolantHTC", new SiUnit("W/K"), 0, BoundaryConditionType.ROBIN);

		// Motor
		for (BoundaryCondition bc : motor.getBoundaryConditions())
			bc.setName("Motor" + bc.getName());
		boundaryConditions.addAll(motor.getBoundaryConditions());
		// Bearings
		for (int i = 0; i < bearings.size(); i++) {
			for (BoundaryCondition bc : bearings.get(i).getBoundaryConditions())
				bc.setName("Bearing" + (i + 1) + bc.getName());
			boundaryConditions.addAll(bearings.get(i).getBoundaryConditions());
		}
		// Others
		boundaryConditions.add(bcHeatSrcTransmission);
		boundaryConditions.add(bcHeatSrcBrake);
		boundaryConditions.add(bcCoolantTemperature);
		boundaryConditions.add(bcCoolantHTC);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.ethz.inspire.emod.model.APhysicalComponent#update()
	 */
	@Override
	public void update() {

		lastspeed = speed.getValue(); // [m/s]
		lastforce = force.getValue(); // [N]

		/* Update sub model */
		massMoved.setSimulationTimestep(timestep);
		massMoved.getInput("SpeedLin").setValue(speed.getValue());
		massMoved.update();

		lastforce += massMoved.getOutput("Force").getValue(); // [N]

		if (1 == state.getValue()) {

			/* We have to differ between ball screws and direct drives: */
			if (motor instanceof ALinearMotor) {
				motor.getInput("Speed").setValue(lastspeed);
				motor.getInput("Force").setValue(lastforce);
			} else {
				/* Transmission */
				plossTransmission = lastspeed * lastforce * (1 / eta - 1);

				lastrotspeed = lastspeed / transmission; // [Hz]
				lasttorque = transmission * lastforce / 2 / Math.PI * (1 / eta); // [Nm]

				/* Update sub model bearings */
				for (Bearing b : bearings) {
					b.getInput("RotSpeed").setValue(lastrotspeed);
					b.getInput("Temperature1").setValue(massCooled.getTemperature().getInitialValue());
					b.getInput("Temperature2").setValue(massCooled.getTemperature().getInitialValue());
					b.update();
					lasttorque += b.getOutput("Torque").getValue();
				}
				motor.getInput("RotSpeed").setValue(lastrotspeed);
				motor.getInput("Torque").setValue(lasttorque);
			}
			motor.update();
			/* Powers */
			puse.setValue(Math.abs(lastspeed * lastforce));
			ptotal.setValue(motor.getOutput("PTotal").getValue() + powerBrakeOff);
			ploss.setValue(ptotal.getValue() - puse.getValue());

		} else {
			motor.getOutput("PUse").setValue(0);
			motor.getOutput("PLoss").setValue(0);
			motor.getOutput("PTotal").setValue(0);

			puse.setValue(0);
			ptotal.setValue(powerBrakeOn);
			ploss.setValue(powerBrakeOn);
		}

		/*
		 * Define inter model heat fluxes
		 */
		massCooled.setHeatInput(0);

		if (coolantConnected) {
			// Thermal resistance
			if (hasThermalStorrage)
				alphaCoolant = ductCoolant.getThermalResistance(coolantProperties.getFlowRate(),
						coolantProperties.getPressureIn(), coolant.getTemperature().getValue(),
						massCooled.getTemperature().getValue());
			else
				alphaCoolant = Double.POSITIVE_INFINITY;

			// Coolant
			coolant.setThermalResistance(alphaCoolant);
			coolant.setFlowRate(coolantProperties);
			coolant.setHeatSource(0.0);
			coolant.setTemperatureAmb(massCooled.getTemperature().getValue());
			coolant.setTemperatureIn(coolantIn.getTemperature());

			// Thermal flows

			massCooled.addHeatInput(coolant.getHeatLoss());

			// Update submodel
			coolant.integrate(timestep, 0, 0, coolantProperties.getPressure());
		}

		if (lubricantConnected) {
			// Thermal resistance
			if (hasThermalStorrage)
				alphaLubricant = ductLubricant.getThermalResistance(lubricantProperties.getFlowRate(),
						lubricantProperties.getPressureIn(), lubricant.getTemperature().getValue(),
						massCooled.getTemperature().getValue());
			else
				alphaLubricant = Double.POSITIVE_INFINITY;

			// Coolant
			lubricant.setThermalResistance(alphaLubricant);
			lubricant.setFlowRate(lubricantProperties);
			lubricant.setHeatSource(0.0);
			lubricant.setTemperatureAmb(massCooled.getTemperature().getValue());
			lubricant.setTemperatureIn(lubricantIn.getTemperature());

			// Thermal flows
			// massCooled.addHeatInput(lubricant.getHeatLoss());

			// Update submodel
			lubricant.integrate(timestep, 0, 0, lubricantProperties.getPressure());
		}

		if (hasThermalStorrage) {
			massCooled.addHeatInput(motor.getOutput("PLoss").getValue());
			massCooled.integrate(timestep);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.ethz.inspire.emod.model.APhysicalComponent#getType()
	 */
	@Override
	public String getType() {
		return type;
	}

	@Override
	public void setType(String type) {
		this.type = type;
	}

	@Override
	public ArrayList<FluidCircuitProperties> getFluidPropertiesList() {
		ArrayList<FluidCircuitProperties> out = new ArrayList<FluidCircuitProperties>();
		if (coolantConnected)
			out.add(coolantProperties);
		if (lubricantConnected)
			out.add(lubricantProperties);
		return out;
	}

	@Override
	public void flood() {
		if (coolantProperties.getPost().size() != 0 | coolantProperties.getPre().size() != 0)
			coolantConnected = true;
		if (lubricantProperties.getPost().size() != 0 | lubricantProperties.getPre().size() != 0)
			lubricantConnected = true;
	}

	@Override
	public void updateBoundaryConditions() {
		motor.updateBoundaryConditions();
		for (Bearing b : bearings)
			b.updateBoundaryConditions();

		bcHeatSrcBrake.setValue(state.getValue() * powerBrakeOff + (state.getValue() - 1) * powerBrakeOn);
		bcHeatSrcTransmission.setValue(plossTransmission);

		if (!coolantConnected) {
			bcCoolantTemperature.setValue(Double.NaN);
			bcCoolantHTC.setValue(Double.NaN);
		} else {
			bcCoolantTemperature.setValue(coolant.getTemperature().getValue());
			bcCoolantHTC.setValue(alphaCoolant);
		}
	}

	@Override
	public ParameterCheckReport checkConfigParams() {
		ParameterCheckReport report = new ParameterCheckReport();
		// Check model parameters:
		// Parameter must be non negative
		if (massValue < 0) {
			report.add("Mass", "Negative value: Mass must be non negative");
		}
		if (alpha > 90 || alpha < 0) {
			report.add("Alpha", "Value: Alpha must be between 0 and 90 degrees");
		}

		// Transmission must be non zero
		if (0 == transmission) {
			report.add("Transmission", "Zero value: Transmission must be non zero");
		}
		return report;
	}

	@Override
	public void loadParameters() {
		super.loadParameters();

		transmission = parameterSet.getPhysicalValue("Transmission").getValue();
		massValue = parameterSet.getPhysicalValue("Mass").getValue();
		alpha = parameterSet.getPhysicalValue("Alpha").getValue();
		powerBrakeOn = parameterSet.getPhysicalValue("PowerBreakOn").getValue();
		powerBrakeOff = parameterSet.getPhysicalValue("PowerBreakOff").getValue();
		eta = parameterSet.getPhysicalValue("TransmissionEfficiency").getValue();
		hasThermalStorrage = parameterSet.getBoolean("HasThermalStorage");

		/* Sub Model Motor */
		motor = (AMotor) parameterSet.getModel("MotorType");
		if(parameterSet.getModel("MotorType", true).getFilter().equals("")) {
			parameterSet.getModel("MotorType", true).setFilter("Motor");
			parameterSet.save(getParameterFile());
		}

		/* Sub Model Bearing */
		bearings = new ArrayList<Bearing>();
		for (APhysicalComponent pc : parameterSet.getModels("BearingType"))
			if (pc instanceof Bearing)
				bearings.add((Bearing) pc);
		if(parameterSet.getModels("BearingType", true).getFilter().equals("")) {
			parameterSet.getModels("BearingType", true).setFilter("Bearing");
			parameterSet.save(getParameterFile());
		}

		/* Ducts */
		ductCoolant = parameterSet.getDuct("StructureDuct");
		ductLubricant = parameterSet.getDuct("LubricantDuct");
		coolant.setVolume(ductCoolant.getVolume());
		lubricant.setVolume(ductLubricant.getVolume());
		massCooled.setMaterial(parameterSet.getMaterial("StructureMaterial"));
		massCooled.setMass(parameterSet.getPhysicalValue("StructureMass").getValue());

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.zuestengineering.emod.model.APhysicalComponent#updateParameterSet()
	 */
	@Override
	protected void updateParameterSet() {
		logger.warning(
				"Parameter set is empty; tying for old parameter set format (this option will be removed in later versions) ...");

		ComponentConfigReader params = null;
		/* Open file containing the parameters of the model type: */
		try {
			params = new ComponentConfigReader(getModelType(), type);
		} catch (Exception e) {
			e.printStackTrace();
		}

		String motorType;
		String[] bearingType;

		/* Read the config parameter: */
		try {
			transmission = params.getPhysicalValue("Transmission", new SiUnit("m/rev")).getValue();
			bearingType = params.getValue("BearingType", new String[0]);
			massValue = params.getPhysicalValue("Mass", new SiUnit("kg")).getValue();
			alpha = params.getPhysicalValue("Alpha", new SiUnit("deg")).getValue();
			motorType = params.getString("MotorType");
			powerBrakeOn = params.getPhysicalValue("PowerBreakOn", new SiUnit("W")).getValue();
			powerBrakeOff = params.getPhysicalValue("PowerBreakOff", new SiUnit("W")).getValue();
			eta = params.getPhysicalValue("TransmissionEfficiency", new SiUnit()).getValue();
			hasThermalStorrage = params.getValue("HasThermalStorage", true);

			/* Sub Model Motor */
			String[] mdlType = motorType.split("_", 2);

			// Create Sub Element
			try {
				// Get class and constructor objects
				String path = APhysicalComponent.class.getPackage().getName();
				Class<?> cl = Class.forName(path + ".pm." + mdlType[0]);
				Constructor<?> co = cl.getConstructor(String.class);
				// initialize new component
				motor = (AMotor) co.newInstance(mdlType[1]);
			} catch (Exception e) {
				Exception ex = new Exception(
						"Unable to create component " + mdlType[0] + "(" + mdlType[1] + ")" + " : " + e.getMessage());
				ex.printStackTrace();
				motor = null;
			}

			/* Sub Model Bearing */
			bearings = new ArrayList<Bearing>();
			for (int i = 0; i < bearingType.length; i++)
				if (!bearingType[i].equals(""))
					bearings.add(new Bearing(bearingType[i]));

			/* Ducts */
			ductCoolant.clone(
					Duct.buildFromFile(getModelType(), getType(), params.getValue("StructureDuct", "StructureDuct")));
			ductLubricant.clone(
					Duct.buildFromFile(getModelType(), getType(), params.getValue("LubricantDuct", "LubricantDuct")));
			coolant.setVolume(ductCoolant.getVolume());
			lubricant.setVolume(ductLubricant.getVolume());
			massCooled.setMaterial(params.getString("StructureMaterial"));
			massCooled.setMass(params.getDoubleValue("StructureMass"));

		} catch (Exception e) {
			e.printStackTrace();
		}

		params.Close(); /* Model configuration file not needed anymore. */

		parameterSet = new ParameterSet(this.toString());
		parameterSet.setPhysicalValue("Transmission", transmission, new SiUnit("m/rev"));
		parameterSet.setPhysicalValue("Mass", massValue, new SiUnit("kg"));
		parameterSet.setPhysicalValue("Alpha", alpha, new SiUnit("deg"));
		parameterSet.setPhysicalValue("PowerBreakOn", powerBrakeOn, new SiUnit("W"));
		parameterSet.setPhysicalValue("PowerBreakOff", powerBrakeOff, new SiUnit("W"));
		parameterSet.setPhysicalValue("TransmissionEfficiency", eta, new SiUnit("m"));
		parameterSet.setBoolean("HasThermalStorage", hasThermalStorrage);
		parameterSet.setDuct("StructureDuct", ductCoolant);
		parameterSet.setDuct("LubricantDuct", ductLubricant);
		parameterSet.setMaterial("StructureMaterial", massCooled.getMaterial());
		parameterSet.setPhysicalValue("StructureMass", massCooled.getMass().getValue(), new SiUnit("kg"));
		parameterSet.setModel("MotorType", motor);
		parameterSet.setBearings("BearingType", bearings);

		parameterSet.save(getParameterFile());
		logger.info("Parameter file successfully converted to new format");

	}

}
