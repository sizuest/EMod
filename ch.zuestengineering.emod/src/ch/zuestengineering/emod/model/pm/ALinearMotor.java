/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.model.pm;

import ch.zuestengineering.emod.model.linking.IOContainer;

/**
 * General linear motor model abstract class
 * 
 * @author Simon Z�st
 *
 */
public abstract class ALinearMotor extends AMotor {

	// Input parameters:
	protected IOContainer speed;
	protected IOContainer force;

	/**
	 * 
	 */
	public ALinearMotor() {
		super();
	}

}
