/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.model.pm;

import java.util.ArrayList;

import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import ch.zuestengineering.emod.femexport.BoundaryCondition;
import ch.zuestengineering.emod.femexport.BoundaryConditionType;
import ch.zuestengineering.emod.model.APhysicalComponent;
import ch.zuestengineering.emod.model.ParameterCheckReport;
import ch.zuestengineering.emod.model.fluid.FECPump;
import ch.zuestengineering.emod.model.fluid.Floodable;
import ch.zuestengineering.emod.model.fluid.Fluid;
import ch.zuestengineering.emod.model.fluid.FluidCircuitProperties;
import ch.zuestengineering.emod.model.linking.FluidContainer;
import ch.zuestengineering.emod.model.linking.IOContainer;
import ch.zuestengineering.emod.model.material.Material;
import ch.zuestengineering.emod.model.parameters.ParameterSet;
import ch.zuestengineering.emod.model.thermal.ThermalElement;
import ch.zuestengineering.emod.model.units.*;
import ch.zuestengineering.emod.simulation.DynamicState;
import ch.zuestengineering.emod.utils.Algo;
import ch.zuestengineering.emod.utils.ComponentConfigReader;

/**
 * General Pump model class. Implements the physical model of a pump with
 * reservoir. From the input parameter mass flow, the electrical power and the
 * supply mass flow are calculated.
 * 
 * Assumptions: Perfect gas
 * 
 * 
 * Inputlist: 1: State : [-] : State of the Pump 2: FluidIn : [-] : Fluid
 * flowing into Pump Outputlist: 1: PTotal : [W] : Demanded electrical power 2:
 * PLoss : [W] : Thermal pump losses 3: PUse : [W] : Power in the pluid 4:
 * Temperature : [K] : Pump structural Temperature 5: FluidOut : [-] : Fluid
 * flowing out of the Pump
 * 
 * Config parameters: PressureSamples : [Pa] : Pressure samples for liner
 * interpolation FlowRateSamples : [m^3/s] : Volumetric flow samples for liner
 * interpolation ElectricalPower : [W] : Power samples for liner interpolation
 * 
 * @author manick
 * 
 */
@XmlRootElement
public class Pump extends APhysicalComponent implements Floodable {

	@XmlElement
	protected String type;

	// Input parameters:
	private IOContainer pumpCtrl;
	private IOContainer temperatureAmb;
	private FluidContainer fluidIn;

	// Output parameters:
	private IOContainer pel;
	private IOContainer pth;
	private IOContainer pmech;
	private FluidContainer fluidOut;
	private IOContainer heatFlowAmbient;
	private IOContainer heatFlowTank;

	// Boundary conditions
	private BoundaryCondition bcMotorHTC;
	private BoundaryCondition bcMotorHeatSrc;
	private BoundaryCondition bcPumpHTC;
	private BoundaryCondition bcPumpHeatSrc;

	// Parameters used by the model.
	private double[] pressureSamples; // Samples of pressure [Pa]
	private double[] flowRateSamples; // Samples of flow rate [m^3/s]
	private double[] effPumpSamples; // Samples of the pump eff [-]
	private double[] powerSamples; // Samples of power demand [W]
	private double massFluid; // Mass of the fluid in the pump [kg]
	private double massMotor; // Mass of the motor [kg]
	private boolean hasMotorCooling; // Forced convection at the motor
	private boolean isSubmerged; // Submerged Pump (heat flow to tank)
	private double diameterPump; // Diameter of the pump [m]
	private double lengthPump; // Length of the pump [m]
	private double diameterMotor; // Diameter of the motor [m]
	private double lengthMotor; // Length of the motor [m]
	private double rotSpeed; // Nominal rotational speed [rpm]
	private int numImpEyes; // Number of impeller entries [-]
	private int numStages; // Number of stages [-]
	private double flowRateBEP; // Nominal flow rate [m^3/s]
	private double pressureBEP; // Nominal pressure [Pa]
	private double deltaTempMax; // Maximum Temperature twds. amb.

	// Global values
	double heatLossMotor, heatLossFluid;
	double htcMotor, htcPump;

	// Parameters calculated by the model
	private double[] effMotorSamples; // Samples
										// of
										// the
										// motor
										// eff
										// [-]
	private double surfaceMotor, // Surface
									// available
									// for
									// convetion
									// [m2];
			surfacePump;

	// Corrected efficiency map
	private double[] powerSamplesV, pressureSamplesV, flowRateSamplesV, effPumpSamplesV;
	private double lastDensity = 0, lastViscosity = 0, lastLevel = 0;
	private double htcMotorForced;

	private double temperatureInit;

	// Pump Structure
	private ThermalElement structure, fluid;

	// Fluid Properties
	FluidCircuitProperties fluidProperties;
	
	// Air properties
	protected Material materialAir = new Material("Air");

	/**
	 * Constructor called from XmlUnmarshaller. Attribute 'type' is set by
	 * XmlUnmarshaller.
	 */
	public Pump() {
		super();

		this.type = "Example";
		this.temperatureInit = 293;
		init();
		this.fluidProperties.getMaterial().setMaterial("Monoethylenglykol_34");
	}

	/**
	 * @param type
	 * @param temperatureInit
	 * @param fluidType
	 */
	public Pump(String type, double temperatureInit, String fluidType) {
		super();

		this.type = type;
		this.temperatureInit = temperatureInit;
		init();
		this.fluidProperties.getMaterial().setMaterial(fluidType);
	}

	/**
	 * @param u
	 * @param parent
	 */
	public void afterUnmarshal(Unmarshaller u, Object parent) {
		// post xml init method (loading physics data)
		init();
	}

	/**
	 * Pump constructor
	 * 
	 * @param type
	 */
	public Pump(String type) {
		super();

		this.type = type;
		init();
	}

	/**
	 * Called from constructor or after unmarshaller.
	 */
	private void init() {
		/* Define Input parameters */
		inputs = new ArrayList<IOContainer>();
		pumpCtrl = new IOContainer("State", new SiUnit(Unit.NONE), 0, ContainerType.CONTROL);
		temperatureAmb = new IOContainer("TemperatureAmb", new SiUnit(Unit.KELVIN), temperatureInit,
				ContainerType.THERMAL);
		inputs.add(pumpCtrl);
		inputs.add(temperatureAmb);

		/* Define output parameters */
		outputs = new ArrayList<IOContainer>();
		pel = new IOContainer("PTotal", new SiUnit(Unit.WATT), 0.00, ContainerType.ELECTRIC);
		pth = new IOContainer("PLoss", new SiUnit(Unit.WATT), 0.00, ContainerType.THERMAL);
		pmech = new IOContainer("PUse", new SiUnit(Unit.WATT), 0.00, ContainerType.FLUIDDYNAMIC);
		heatFlowAmbient = new IOContainer("HeatFlowAmbient", new SiUnit(Unit.WATT), 0, ContainerType.THERMAL);
		heatFlowTank = new IOContainer("HeatFlowTank", new SiUnit(Unit.WATT), 0, ContainerType.THERMAL);
		outputs.add(pel);
		outputs.add(pth);
		outputs.add(pmech);
		outputs.add(heatFlowAmbient);
		outputs.add(heatFlowTank);

		/* Boundary conditions */
		boundaryConditions = new ArrayList<BoundaryCondition>();
		bcMotorHeatSrc = new BoundaryCondition("HeatSrcMotor", new SiUnit("W"), 0, BoundaryConditionType.NEUMANN);
		bcMotorHTC = new BoundaryCondition("MotorHTC", new SiUnit("W/K"), 0, BoundaryConditionType.ROBIN);
		bcPumpHeatSrc = new BoundaryCondition("HeatSrcPump", new SiUnit("W"), 0, BoundaryConditionType.NEUMANN);
		bcPumpHTC = new BoundaryCondition("PumpHTC", new SiUnit("W/K"), 0, BoundaryConditionType.ROBIN);
		boundaryConditions.add(bcMotorHeatSrc);
		boundaryConditions.add(bcMotorHTC);
		boundaryConditions.add(bcPumpHeatSrc);
		boundaryConditions.add(bcPumpHTC);

		/* Sub models */
		structure = new ThermalElement(new Material("Motor"), 1);
		fluid = new ThermalElement(new Material("Example"), 1);

		/* Read configuration parameters: */
		loadParameters();

		/* Dynamic states */
		dynamicStates = new ArrayList<DynamicState>();

		/* Structure */
		structure.getTemperature().setName("TemperatureStructure");
		dynamicStates.add(structure.getTemperature());

		/* Fluid */
		fluid.getTemperature().setName("TemperatureFluid");
		dynamicStates.add(fluid.getTemperature());

		/* Define FlowRate */
		fluidProperties = new FluidCircuitProperties(new FECPump(this, pumpCtrl), fluid.getTemperature());
		fluidProperties.setMaterial(new Material("Example"));
		fluid.setMaterial(fluidProperties.getMaterial());

		/* Define FluidIn parameter */
		fluidIn = new FluidContainer("FluidIn", new SiUnit(Unit.NONE), ContainerType.FLUIDDYNAMIC, fluidProperties);
		inputs.add(fluidIn);

		/* Define FluidOut parameter */
		fluidOut = new FluidContainer("FluidOut", new SiUnit(Unit.NONE), ContainerType.FLUIDDYNAMIC, fluidProperties);
		outputs.add(fluidOut);
		fluidOut.getFluidCircuitProperties().setTemperature(fluid.getTemperature());

	}

	/**
	 * Validate the model parameters.
	 */
	public ParameterCheckReport checkConfigParams() {
		ParameterCheckReport report = new ParameterCheckReport();
		// Check model parameters:
		// Check dimensions:
		if (pressureSamples.length != flowRateSamples.length)
			report.add("PressureSamples",
					"Dimension missmatch: Vector 'PressureSamples' must have same dimension as 'FlowRateSamples' ("
							+ pressureSamples.length + "!=" + flowRateSamples.length + ")!");
		if (pressureSamples.length != powerSamples.length)
			report.add("FlowRateSamples",
					"Dimension missmatch: Vector 'PressureSamples' must have same dimension as 'PowertSamples' ("
							+ pressureSamples.length + "!=" + powerSamples.length + ")!");
		if (pressureSamples.length != effPumpSamples.length)
			report.add("EfficiencySamples",
					"Dimension missmatch: Vector 'PressureSamples' must have same dimension as 'EffPumpSamples' ("
							+ pressureSamples.length + "!=" + effPumpSamples.length + ")!");

		// Check if sorted:
		for (int i = 1; i < flowRateSamples.length; i++) {
			if (flowRateSamples[i] <= flowRateSamples[i - 1]) {
				report.add("FlowRateSamples", "Sample vector 'FlowRateSamples' must be sorted!");
				break;
			}
		}
		// Check value
		for (int i = 1; i < powerSamples.length; i++)
			if (powerSamples[i] <= 0) {
				report.add("PowerSamples", "Negative or zero value: Pump power must be strictly positive!");
				break;
			}

		return report;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.ethz.inspire.emod.model.APhysicalComponent#update()
	 */
	@Override
	public void update() {

		double flowRate = fluidProperties.getFlowRate();

		// flowRate = Math.max(0, Math.min(flowRate,
		// Algo.getMaximum(flowRateSamplesV)));

		fluid.setMaterial(fluidProperties.getMaterial());

		/* Check if pump map has to be updated */
		if(lastLevel != pumpCtrl.getValue() | 
		   fluid.getMaterial().getDensity(fluidProperties.getTemperature(), fluidProperties.getPressure()) != lastDensity |
		   fluid.getMaterial().getViscosityDynamic(fluidProperties.getTemperature(), fluidProperties.getPressure()) != lastViscosity) {
			lastLevel     = pumpCtrl.getValue();
			lastDensity   = fluid.getMaterial().getDensity(fluidProperties.getTemperature(), fluidProperties.getPressure());
			lastViscosity = fluid.getMaterial().getViscosityKinematic(fluidProperties.getTemperature(), fluidProperties.getPressure());
			updatePumpMap(lastLevel, lastDensity, lastViscosity);
		}

		/* If pump is running calculate flow rate and power demand */
		if (pumpCtrl.getValue() > 0) {
			// Resulting power demand
			pel.setValue(Algo.linearInterpolation(flowRate, flowRateSamplesV, powerSamplesV));
		} else {
			pel.setValue(0);
		}

		/*
		 * The mechanical power is given by the pressure and the voluminal flow: Pmech =
		 * pFluid [Pa] * Vdot [m3/s]
		 */
		pmech.setValue(-flowRate * fluidProperties.getPressureDrop());

		/*
		 * The Losses are the difference between electrical and mechanical power
		 */
		pth.setValue(Math.max(0, pel.getValue() - pmech.getValue()));

		/* Losses */
		double curEff = Math.min(Math.max(Algo.linearInterpolation(flowRate, flowRateSamplesV, effMotorSamples), 0), 1);
		if (pumpCtrl.getValue() > 0 & curEff <= 1 & curEff > 0)
			heatLossMotor = pel.getValue() * (1 - curEff);
		else
			heatLossMotor = 0;

		heatLossFluid = pth.getValue() - heatLossMotor;

		/* Heat fluxes */

		// HTC estimations
		if (pumpCtrl.getValue() > 0 & hasMotorCooling)
			htcMotor = htcMotorForced;
		else
			htcMotor = Fluid.convectionFreeCylinderVert(materialAir, structure.getTemperature().getValue(),
					temperatureAmb.getValue(), 1E5, lengthMotor, diameterMotor);

		if (isSubmerged)
			htcPump = Fluid.convectionFreeCylinderVert(fluid.getMaterial(), fluid.getTemperature().getValue(),
					fluidIn.getTemperature(), fluidProperties.getPressureIn(), lengthPump, diameterPump);
		else
			htcPump = Fluid.convectionFreeCylinderVert(materialAir, fluid.getTemperature().getValue(),
					temperatureAmb.getValue(), 1E5, lengthPump, diameterPump);

		if (pumpCtrl.getValue() > 0)
			htcPump *= 10;

		// Structure
		structure.setHeatInput(heatLossMotor);
		structure.setThermalResistance(htcMotor * surfaceMotor);
		structure.setTemperatureAmb(temperatureAmb.getValue());

		// Fluid
		fluid.setHeatInput(heatLossFluid);
		fluid.setThermalResistance(htcPump * surfacePump);
		fluid.setTemperatureIn(fluidIn.getTemperature());
		if (isSubmerged)
			fluid.setTemperatureAmb(fluidIn.getTemperature());
		else
			fluid.setTemperatureAmb(temperatureAmb.getValue());

		/* Integrate */
		structure.integrate(timestep);
		fluid.integrate(timestep, flowRate, flowRate, fluidProperties.getPressure());

		/* Write outputs */
		if (isSubmerged) {
			heatFlowAmbient.setValue(structure.getBoundaryHeatFlux());
			heatFlowTank.setValue(fluid.getBoundaryHeatFlux());
		} else {
			heatFlowAmbient.setValue(structure.getBoundaryHeatFlux() + fluid.getBoundaryHeatFlux());
			heatFlowTank.setValue(0);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.ethz.inspire.emod.model.APhysicalComponent#getType()
	 */
	@Override
	public String getType() {
		return type;
	}

	/**
	 * set Type of the Pump
	 * 
	 * @param type the type of the pump to set
	 */
	@Override
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * get the fluid type
	 * 
	 * @return the fluid type
	 */
	public String getFluidType() {
		return fluidProperties.getMaterial().getType();
	}

	/**
	 * Recalculate pump map for new viscosity
	 * @param u Relative rot speed (0...1)
	 * @param temperature Temperature [K]
	 */
	public void updatePumpMap(double u, double temperature) {

		if (Double.isNaN(temperature))
			return;

		double density = fluid.getMaterial().getDensity(temperature, 1E5);
		double viscosity = fluid.getMaterial().getViscosityKinematic(temperature, 1E5);
		updatePumpMap(u, density, viscosity);
	}

	/**
	 * Recalculate pump map for new viscosity
	 * 
	 * @param rho Density [kg/m^3]
	 * @param nu  Viscosity [m^2/s]
	 */
	private void updatePumpMap(double u, double rho, double nu) {
		
		if(0>=u)
			u=1;

		if (Double.isNaN(rho) | Double.isNaN(nu))
			return;

		double Re, ReMod, fHopt, fEta, fQ, fH, omega, omegaS;

		if (numImpEyes == 0 | numStages == 0) {
			/* Update map */
			for (int i = 0; i < flowRateSamples.length; i++) {
				flowRateSamplesV[i] = flowRateSamples[i]*u;
				pressureSamplesV[i] = pressureSamples[i]*Math.pow(u, 2);
				effPumpSamplesV[i] = effPumpSamples[i];
				powerSamplesV[i] = powerSamples[i]*Math.pow(u, 3);
			}
		} else {
			omega = rotSpeed / 30 * Math.PI;

			/* Reynolds number */
			Re = omega * Math.pow(diameterPump / 2, 2) / nu;

			/* Univ. spec. speed */
			omegaS = omega * Math.sqrt(flowRateBEP / numImpEyes) / Math.pow(pressureBEP / 1000 / numStages, .75);

			/* Modified reynolds number */
			ReMod = Re * Math.pow(omegaS, 1.5) * Math.pow(numImpEyes, 0.75);

			/* Correction factors */
			fHopt = Math.pow(ReMod, -6.7 / Math.pow(ReMod, .735));

			fEta = Math.pow(ReMod, -19.0 / Math.pow(ReMod, 0.705));
			fQ = fHopt;

			/* Update map */
			for (int i = 0; i < flowRateSamples.length; i++) {

				fH = 1 - (1 - fHopt) * Math.pow(flowRateSamples[i] / flowRateBEP, .75);
				flowRateSamplesV[i] = fQ * flowRateSamples[i]*u;
				pressureSamplesV[i] = fH * pressureSamples[i] * rho / 1000 * Math.pow(u, 2);
				effPumpSamplesV[i] = fEta * effPumpSamples[i];

				if (flowRateSamples[i] == 0)
					powerSamplesV[i] = powerSamples[i] * Math.pow(u, 3);
				else
					powerSamplesV[i] = flowRateSamplesV[i] * pressureSamplesV[i] / effPumpSamplesV[i]
							/ effMotorSamples[i];

			}
		}
	}

	@Override
	public ArrayList<FluidCircuitProperties> getFluidPropertiesList() {
		ArrayList<FluidCircuitProperties> out = new ArrayList<FluidCircuitProperties>();
		out.add(fluidProperties);
		return out;
	}

	/**
	 * Returns the pressure resulting from the given flow rate
	 * 
	 * @param flowRate
	 * @return
	 */
	public double getPressure(double flowRate) {
		// if(flowRate<flowRateSamplesV[0] |
		// flowRate>flowRateSamplesV[flowRateSamplesV.length-1])
		// return 0;
		double p = Algo.linearInterpolation(flowRate, flowRateSamplesV, pressureSamplesV, true);

		if (p < 0)
			return 0;
		return p;
	}

	/**
	 * Returns the derivative of the pressure map at the given flow rate
	 * 
	 * @param flowRate
	 * @return
	 */
	public double getPressureDrivative(double flowRate) {
		return Algo.numericalDerivative(flowRate, flowRateSamplesV, pressureSamplesV);
	}

	@Override
	public void flood() {/* Not used */
	}

	@Override
	public void updateBoundaryConditions() {
		if (pumpCtrl.getValue() != 1) {
			bcMotorHeatSrc.setValue(0);
			bcPumpHeatSrc.setValue(0);
		} else {
			bcMotorHeatSrc.setValue(heatLossMotor);
			bcPumpHeatSrc.setValue(heatLossFluid);
		}
		bcMotorHTC.setValue(htcMotor);
		bcPumpHTC.setValue(htcPump);
	}

	@Override
	public void loadParameters() {
		super.loadParameters();

		pressureSamples = parameterSet.getPhysicalValue("PressureSamples").getValues();
		flowRateSamples = parameterSet.getPhysicalValue("FlowRateSamples").getValues();
		powerSamples = parameterSet.getPhysicalValue("PowerSamples").getValues();
		effPumpSamples = parameterSet.getPhysicalValue("EfficiencySamples").getValues();

		massFluid = parameterSet.getPhysicalValue("MassFluid").getValue();
		massMotor = parameterSet.getPhysicalValue("MassMotor").getValue();
		hasMotorCooling = parameterSet.getBoolean("HasMotorCooling");
		isSubmerged = parameterSet.getBoolean("IsSubmerged");
		diameterPump = parameterSet.getPhysicalValue("DiameterPump").getValue();
		lengthPump = parameterSet.getPhysicalValue("LengthPump").getValue();
		diameterMotor = parameterSet.getPhysicalValue("DiameterMotor").getValue();
		lengthMotor = parameterSet.getPhysicalValue("LengthMotor").getValue();
		rotSpeed = parameterSet.getPhysicalValue("NominalRotSpeed").getValue();
		numImpEyes = parameterSet.getInteger("NumberImpellerEyes");
		numStages = parameterSet.getInteger("NumberStages");
		deltaTempMax = parameterSet.getPhysicalValue("MaxTemperatureDifference").getValue();

		/* Sort vectors */
		int[] sortIdx = Algo.sort(flowRateSamples);
		pressureSamples = Algo.sort(pressureSamples, sortIdx);
		powerSamples = Algo.sort(powerSamples, sortIdx);
		effPumpSamples = Algo.sort(effPumpSamples, sortIdx);

		/* BEP */
		int idxBEP = Algo.getMaximumIndex(effPumpSamples);
		flowRateBEP = flowRateSamples[idxBEP];
		pressureBEP = pressureSamples[idxBEP];

		/* Motor efficiency */
		effMotorSamples = new double[effPumpSamples.length];
		for (int i = flowRateSamples.length - 1; i >= 0; i--) {
			if (0 == flowRateSamples[i] & flowRateSamples.length - i > 1)
				effMotorSamples[i] = effMotorSamples[i + 1];
			else if (effPumpSamples[i] == 0)
				effMotorSamples[i] = 0;
			else
				effMotorSamples[i] = pressureSamples[i] * flowRateSamples[i] / powerSamples[i] / effPumpSamples[i];
		}

		/* Surface for convection */
		surfaceMotor = lengthMotor * diameterMotor * Math.PI;
		surfacePump = lengthPump * diameterPump * Math.PI;

		/* Forced convection on motor */
		double lossMax = 0;
		for (int i = 0; i < effPumpSamples.length; i++)
			lossMax = Math.max(powerSamples[i] * (1 - effMotorSamples[i]), lossMax);

		htcMotorForced = lossMax / surfaceMotor / deltaTempMax;
		// htcMotorForced =
		// 2/(surfaceMotor*20/lossMax/2-diameterMotor/2/(new
		// Material("Motor")).getThermalConductivity()*Math.log(2));

		pressureSamplesV = new double[pressureSamples.length];
		flowRateSamplesV = new double[flowRateSamples.length];
		powerSamplesV = new double[powerSamples.length];
		effPumpSamplesV = new double[effPumpSamples.length];

		structure.setMass(massMotor);
		fluid.setMass(massFluid);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.zuestengineering.emod.model.APhysicalComponent#updateParameterSet()
	 */
	@Override
	protected void updateParameterSet() {
		logger.warning(
				"Parameter set is empty; tying for old parameter set format (this option will be removed in later versions) ...");

		ComponentConfigReader params = null;
		/* Open file containing the parameters of the model type: */
		try {
			params = new ComponentConfigReader(getModelType(), type);
		} catch (Exception e) {
			e.printStackTrace();
		}

		/* Read the config parameter: */
		try {
			pressureSamples = params.getPhysicalValue("PressureSamples", new SiUnit("Pa")).getValues();
			flowRateSamples = params.getPhysicalValue("FlowRateSamples", new SiUnit("m^3 s^-1")).getValues();
			powerSamples = params.getPhysicalValue("PowerSamples", new SiUnit("W")).getValues();
			effPumpSamples = params.getPhysicalValue("EfficiencySamples", new SiUnit("")).getValues();
			massFluid = params.getPhysicalValue("MassFluid", new SiUnit("kg")).getValue();
			massMotor = params.getPhysicalValue("MassMotor", new SiUnit("kg")).getValue();
			diameterPump = params.getPhysicalValue("DiameterPump", new SiUnit("m")).getValue();
			lengthPump = params.getPhysicalValue("LengthPump", new SiUnit("m")).getValue();
			diameterMotor = params.getPhysicalValue("DiameterMotor", new SiUnit("m")).getValue();
			lengthMotor = params.getPhysicalValue("LengthMotor", new SiUnit("m")).getValue();
			rotSpeed = params.getPhysicalValue("NominalRotSpeed", new SiUnit("RPM")).getValue();
			deltaTempMax = params.getPhysicalValue("MaxTemperatureDifference", new SiUnit("K")).getValue();
			numImpEyes = params.getIntValue("NumberImpellerEyes");
			numStages = params.getIntValue("NumberStages");
			hasMotorCooling = params.getValue("HasMotorCooling", true);
			isSubmerged = params.getValue("IsSubmerged", true);

		} catch (Exception e) {
			e.printStackTrace();
		}

		params.Close();

		parameterSet = new ParameterSet(this.toString());
		parameterSet.setPhysicalValue("PressureSamples", pressureSamples, new SiUnit("Pa"));
		parameterSet.setPhysicalValue("FlowRateSamples", flowRateSamples, new SiUnit("m^3 s^-1"));
		parameterSet.setPhysicalValue("PowerSamples", powerSamples, new SiUnit("W"));
		parameterSet.setPhysicalValue("EfficiencySamples", effPumpSamples, new SiUnit(""));
		parameterSet.setPhysicalValue("MassFluid", massFluid, new SiUnit("kg"));
		parameterSet.setPhysicalValue("MassMotor", massMotor, new SiUnit("kg"));
		parameterSet.setPhysicalValue("DiameterPump", diameterPump, new SiUnit("m"));
		parameterSet.setPhysicalValue("LengthPump", lengthPump, new SiUnit("m"));
		parameterSet.setPhysicalValue("DiameterMotor", diameterMotor, new SiUnit("m"));
		parameterSet.setPhysicalValue("LengthMotor", lengthMotor, new SiUnit("m"));
		parameterSet.setPhysicalValue("NominalRotSpeed", rotSpeed, new SiUnit("RPM"));
		parameterSet.setPhysicalValue("MaxTemperatureDifference", deltaTempMax, new SiUnit("K"));
		parameterSet.setInteger("NumberImpellerEyes", numImpEyes);
		parameterSet.setInteger("NumberStages", numStages);
		parameterSet.setBoolean("HasMotorCooling", hasMotorCooling);
		parameterSet.setBoolean("IsSubmerged", isSubmerged);
		parameterSet.save(getParameterFile());
		logger.info("Parameter file successfully converted to new format");
	}
}
