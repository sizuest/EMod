/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.model.pm;

import java.util.ArrayList;

import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import ch.zuestengineering.emod.femexport.BoundaryCondition;
import ch.zuestengineering.emod.femexport.BoundaryConditionType;
import ch.zuestengineering.emod.model.APhysicalComponent;
import ch.zuestengineering.emod.model.ParameterCheckReport;
import ch.zuestengineering.emod.model.fluid.FECTank;
import ch.zuestengineering.emod.model.fluid.Fillable;
import ch.zuestengineering.emod.model.fluid.Floodable;
import ch.zuestengineering.emod.model.fluid.Fluid;
import ch.zuestengineering.emod.model.fluid.FluidCircuitProperties;
import ch.zuestengineering.emod.model.linking.FluidContainer;
import ch.zuestengineering.emod.model.linking.IOContainer;
import ch.zuestengineering.emod.model.material.Material;
import ch.zuestengineering.emod.model.parameters.ParameterSet;
import ch.zuestengineering.emod.model.thermal.ThermalElement;
import ch.zuestengineering.emod.model.units.ContainerType;
import ch.zuestengineering.emod.model.units.SiConstants;
import ch.zuestengineering.emod.model.units.SiUnit;
import ch.zuestengineering.emod.model.units.Unit;
import ch.zuestengineering.emod.simulation.DynamicState;
import ch.zuestengineering.emod.utils.ComponentConfigReader;

/**
 * General Tank model class. Implements the physical model of a tank
 * 
 * Assumptions: -No leakage -Separation between laminar and turbulent flow
 * -Smooth surface -Tank wall is rigid
 * 
 * Input list: 1: FluidIn : [-] : Fluid Container with temperature, pressure,
 * mass flow 2: PressureAmb : [Pa] : Ambient Pressure (assuming free surface of
 * fluid in tank) 3: TemperatureAmb: [K] : Ambient temperature Output list: 1:
 * FluidOut : [-] : Fluid Container with temperature, pressure, mass flow
 * 
 * Configuration parameters: Volume : [m^3] or Length : [m] Width : [m] Height :
 * [m]
 * 
 * Material : [-]
 * 
 * 
 * @author manick
 * 
 */
@XmlRootElement
public class Tank extends APhysicalComponent implements Floodable, Fillable {

	@XmlElement
	protected String type;

	// Input parameters:
	private FluidContainer fluidIn;
	private IOContainer temperatureAmb;
	private IOContainer pressureAmb;
	private IOContainer heatFlowIn;
	private IOContainer heatExchangerOut;
	
	// Dummy IOContainers for pressure references
	private IOContainer pressureRefIn;
	private IOContainer pressureRefOut;

	// Output parameters:
	private FluidContainer fluidOut;
	private IOContainer temperatureTank;
	private IOContainer level;

	// Boundary condition
	private BoundaryCondition bcTankTemp;
	private BoundaryCondition bcHeatFlux;

	// Dynamic In- and outputs
	private ArrayList<FluidContainer> inputsDyn;
	private ArrayList<FluidContainer> outputsDyn;
	private ArrayList<IOContainer> heatInDyn;

	// Fluid Properties
	private FluidCircuitProperties fluidPropertiesIn, fluidPropertiesOut;

	// Parameters used by the model.
	private double volume, surface;
	private double length = 0.00;
	private double width = 0.00;
	private double height = 0.00;
	private ThermalElement fluid;
	private double temperatureInit = 293.00;
	private double niveau;

	private boolean materialSet = false;
	
	// Air properties
	protected Material materialAir = new Material("Air");

	/**
	 * Constructor called from XmlUnmarshaller. Attribute 'type' is set by
	 * XmlUnmarshaller.
	 */
	public Tank() {
		super();
	}

	/**
	 * post xml init method (loading physics data)
	 * 
	 * @param u
	 * @param parent
	 */
	public void afterUnmarshal(Unmarshaller u, Object parent) {
		init();
	}

	/**
	 * Tank constructor
	 * 
	 * @param type
	 */
	public Tank(String type) {
		super();

		this.type = type;
		init();
	}

	/**
	 * @param type
	 * @param TInit
	 */
	public Tank(String type, double TInit) {
		super();
		this.type = type;
		init();
		fluid.getTemperature().setInitialCondition(TInit);
		fluid.getTemperature().setInitialCondition();
	}

	/**
	 * Called from constructor or after unmarshaller.
	 * 
	 * @throws Exception
	 */
	private void init() {

		fluid = new ThermalElement("Example", 1);

		fluidPropertiesIn = new FluidCircuitProperties( new FECTank(this, true), null);
		fluidPropertiesOut = new FluidCircuitProperties(new FECTank(this, false), fluid.getTemperature());

		/* Dynamic in-/output Lists */
		inputsDyn = new ArrayList<FluidContainer>();
		outputsDyn = new ArrayList<FluidContainer>();
		heatInDyn = new ArrayList<>();

		/* Define Input parameters */
		inputs = new ArrayList<IOContainer>();
		temperatureAmb = new IOContainer("TemperatureAmb", new SiUnit(Unit.KELVIN), temperatureInit, ContainerType.THERMAL);
		pressureAmb = new IOContainer("PressureAmb", new SiUnit(Unit.PA), 0.00, ContainerType.FLUIDDYNAMIC);
		heatFlowIn = new IOContainer("HeatFlowIn", new SiUnit(Unit.WATT), 0.00, ContainerType.THERMAL);
		heatExchangerOut = new IOContainer("HeatExchangerOut", new SiUnit(Unit.WATT), 0.00, ContainerType.THERMAL);
		inputs.add(temperatureAmb);
		inputs.add(pressureAmb);
		inputs.add(heatExchangerOut);
		inputs.add(heatFlowIn);
		
		// Dummies
		pressureRefIn  = new IOContainer("PressureRefIn",  new SiUnit(Unit.PA), 0.00, ContainerType.FLUIDDYNAMIC);
		pressureRefOut = new IOContainer("PressureRefOut", new SiUnit(Unit.PA), 0.00, ContainerType.FLUIDDYNAMIC);

		/* Boundary conditions */
		boundaryConditions = new ArrayList<BoundaryCondition>();
		bcTankTemp = new BoundaryCondition("Temperature", new SiUnit("K"), 293.15, BoundaryConditionType.DIRICHLET);
		bcHeatFlux = new BoundaryCondition("HeatFluxToAmbient", new SiUnit("W"), 0, BoundaryConditionType.NEUMANN);
		boundaryConditions.add(bcTankTemp);
		boundaryConditions.add(bcHeatFlux);

		/* Define output parameters */
		outputs = new ArrayList<IOContainer>();
		temperatureTank = new IOContainer("TemperatureTank", new SiUnit(Unit.KELVIN), temperatureInit, ContainerType.CONTROL);
		level = new IOContainer("Level", new SiUnit(""), 0, ContainerType.CONTROL);
		outputs.add(temperatureTank);
		outputs.add(level);

		/* Read configuration parameters: */
		loadParameters();

		// TODO manick: test for Fluid
		fluidIn = new FluidContainer("FluidIn", new SiUnit(Unit.NONE), ContainerType.FLUIDDYNAMIC, fluidPropertiesIn);
		inputs.add(fluidIn);
		// TODO manick: test for Fluid
		fluidOut = new FluidContainer("FluidOut", new SiUnit(Unit.NONE), ContainerType.FLUIDDYNAMIC,
				fluidPropertiesOut);
		outputs.add(fluidOut);

		/* Dynamic state */
		dynamicStates = new ArrayList<DynamicState>();
		dynamicStates.add(fluid.getTemperature());
		dynamicStates.add(fluid.getMass());

		/* FlowRate */
		fluidPropertiesOut.setPressureReferenceIn(pressureRefOut);
		fluidPropertiesIn.setPressureReferenceOut(pressureRefIn);
		fluidPropertiesOut.setMaterial(fluid.getMaterial());
		fluidPropertiesIn.setMaterial(fluid.getMaterial());

		fluid.getMass().setInitialCondition(
				getVolume() * fluid.getMaterial().getDensity(fluid.getTemperature().getValue(), 1E5));

	}

	@Override
	public IOContainer getInput(String name) {
		IOContainer temp = null;

		/* Check if FluidIn is requested */
		if (name.equals(fluidIn.getName())) {
			temp = new FluidContainer("FluidIn-" + (inputsDyn.size() + 1), fluidIn, fluidPropertiesIn);
			inputs.add(temp);
			inputsDyn.add((FluidContainer) temp);
			return temp;
		}
		
		/* Check if a reference on FluidIn is requested */
		for (FluidContainer ioc : inputsDyn) {
			if (ioc.getName().equals(name)) {
				return ioc;
			}
		}
		
		/* Check if Heat Input is required */
		if (name.equals(heatFlowIn.getName())) {
			temp = new IOContainer("HeatFlowIn-" + (heatInDyn.size() + 1), heatFlowIn);
			inputs.add(temp);
			heatInDyn.add(temp);
			return temp;
		} 
		
		/* Check if a reference on Heat Input is required */
		for (IOContainer ioc : heatInDyn) {
			if (ioc.getName().equals(name)) {
				return ioc;
			}
		}

		return super.getInput(name);
	}

	@Override
	public IOContainer getOutput(String name) {
		IOContainer temp = null;

		if (name.equals(fluidOut.getName())) {
			temp = new FluidContainer("FluidIOut-" + (outputsDyn.size() + 1), fluidOut, fluidPropertiesOut);
			outputsDyn.add((FluidContainer) temp);
		} else {
			for (FluidContainer ioc : outputsDyn) {
				if (ioc.getName().equals(name)) {
					temp = ioc;
					break;
				}
			}

			if (null == temp)
				for (IOContainer ioc : outputs) {
					if (ioc.getName().equals(name)) {
						temp = ioc;
						break;
					}
				}
		}

		return temp;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.ethz.inspire.emod.model.APhysicalComponent#update()
	 */
	@Override
	public void update() {

		if (!materialSet) {
			fluidPropertiesIn.setMaterial(fluid.getMaterial());
			fluidPropertiesOut.setMaterial(fluid.getMaterial());

			materialSet = true;
		}

		double alphaFluid, thermalResistance;

		double flowRateIn = 0, flowRateOut = 0, avgTemperatureIn = 0;

		/*
		 * ********************************************************************* **
		 */
		/* Calculate and set fluid values: */
		/* TemperatureIn, Pressure, FlowRate, ThermalResistance, */
		/* HeatSource, TemperatureExternal */
		/*
		 * ********************************************************************* **
		 */

		/* Mass flows */
		flowRateIn = fluidPropertiesIn.getFlowRate();
		flowRateOut = fluidPropertiesOut.getFlowRate();
		
		/* Heat inputs */
		heatFlowIn.setValue(0);
		for(IOContainer ioc: heatInDyn)
			heatFlowIn.addValue(ioc.getValue());

		/* Average temperature */
		avgTemperatureIn = fluidPropertiesIn.getTemperatureIn();

		/* Convection */
		alphaFluid = Fluid.convectionFreeCuboid(materialAir, fluid.getTemperature().getValue(),
				temperatureAmb.getValue(), 1E5, length, width, height, false);
		thermalResistance = (alphaFluid * surface);

		/* Forced heat flow */
		fluid.setHeatInput(-heatExchangerOut.getValue() + heatFlowIn.getValue());

		/* Integrate temperature and mass flows */
		fluid.setTemperatureIn(avgTemperatureIn);
		fluid.setTemperatureAmb(temperatureAmb.getValue());
		fluid.setThermalResistance(thermalResistance);
		fluid.integrate(timestep, flowRateIn, flowRateOut, pressureAmb.getValue());

		temperatureTank.setValue(fluid.getTemperature().getValue());
		
		/* Set the level (rel. height) */
		level.setValue(fluid.getVolume()/volume);
		
		/* Dummy pressure references */
		pressureRefIn.setValue(getPressureTop());
		pressureRefOut.setValue(getPressureBottom());

	}
	
	/**
	 * getPressureBottom
	 * @return 
	 */
	public double getPressureBottom() {		
		return pressureAmb.getValue() + SiConstants.g.getValue()*fluidPropertiesOut.getDensity()*(height*level.getValue()+niveau);
	}
	
	/**
	 * getPressureTop
	 * @return 
	 */
	public double getPressureTop() {
		return pressureAmb.getValue() + SiConstants.g.getValue()*fluidPropertiesOut.getDensity()*(height+niveau);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.ethz.inspire.emod.model.APhysicalComponent#getType()
	 */
	@Override
	public String getType() {
		return type;
	}

	@Override
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * get the type of the Fluid
	 * 
	 * @return type of the fluid
	 */
	public String getFluidType() {
		return fluid.getMaterial().getType();
	}

	/**
	 * @return tank volume
	 */
	public double getVolume() {
		return volume;
	}

	@Override
	public ArrayList<FluidCircuitProperties> getFluidPropertiesList() {
		ArrayList<FluidCircuitProperties> out = new ArrayList<FluidCircuitProperties>();
		out.add(fluidPropertiesIn);
		out.add(fluidPropertiesOut);
		return out;
	}

	@Override
	public void flood() {
		fluidPropertiesIn.setMaterial(fluid.getMaterial());
		fluidPropertiesOut.setMaterial(fluid.getMaterial());
	}

	@Override
	public void updateBoundaryConditions() {
		bcTankTemp.setValue(temperatureTank.getValue());
		bcHeatFlux.setValue(fluid.getBoundaryHeatFlux());
	}

	@Override
	public ParameterCheckReport checkConfigParams() {
		ParameterCheckReport report = new ParameterCheckReport();
		if (0 > volume) {
			report.add("Volume", "Non physical value: Variable 'volume' must be bigger than zero!");
		}
		return report;
	}

	@Override
	public void loadParameters() {
		super.loadParameters();

		length = parameterSet.getPhysicalValue("Length").getValue();
		width = parameterSet.getPhysicalValue("Width").getValue();
		height = parameterSet.getPhysicalValue("Height").getValue();
		volume = length * width * height;
		surface = 2 * (length + width) * height + length * width;
		
		try {
			niveau = parameterSet.getPhysicalValue("Niveau").getValue();
		} catch(Exception e) {
			niveau = 0;
			parameterSet.setPhysicalValue("Niveau", 0.0, new SiUnit("m"));
			parameterSet.save(getParameterFile());
			logger.warning("Niveau '0 m' added to tank");
		}

		/* Thermal Element */
		fluid.setMaterial(parameterSet.getMaterial("Material"));
		fluid.getMass().setInitialCondition(volume / fluid.getMaterial().getDensity(293.15, 1E5));

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.zuestengineering.emod.model.APhysicalComponent#updateParameterSet()
	 */
	@Override
	protected void updateParameterSet() {
		logger.warning(
				"Parameter set is empty; tying for old parameter set format (this option will be removed in later versions) ...");

		ComponentConfigReader params = null;
		/* Open file containing the parameters of the model type: */
		try {
			params = new ComponentConfigReader(getModelType(), type);
		} catch (Exception e) {
			e.printStackTrace();
		}

		/* Read the config parameter: */
		try {
			volume = params.getPhysicalValue("Volume", new SiUnit("m^3")).getValue();
			length = Math.pow(volume, .333);
			width = Math.pow(volume, .333);
			height = Math.pow(volume, .333);
			surface = 2 * (length + width) * height + length * width;
		} catch (Exception e) {
			try {
				length = params.getPhysicalValue("Length", new SiUnit("m")).getValue();
				width = params.getPhysicalValue("Width", new SiUnit("m")).getValue();
				height = params.getPhysicalValue("Height", new SiUnit("m")).getValue();
				volume = length * width * height;
				surface = 2 * (length + width) * height + length * width;
			} catch (Exception ee) {
				e.printStackTrace();
				ee.printStackTrace();
			}
		}
		try {

			/* Thermal Element */
			fluid.setMaterial(new Material(params.getString("Material")));
			fluid.getMass().setInitialCondition(volume / fluid.getMaterial().getDensity(293.15, 1E5));

		} catch (Exception e) {
			e.printStackTrace();
		}
		params.Close(); /* Model configuration file not needed anymore. */

		parameterSet = new ParameterSet(this.toString());
		parameterSet.setPhysicalValue("Length", length, new SiUnit("m"));
		parameterSet.setPhysicalValue("Width", width, new SiUnit("m"));
		parameterSet.setPhysicalValue("Height", height, new SiUnit("m"));
		parameterSet.setMaterial("Material", fluid.getMaterial());
		parameterSet.save(getParameterFile());
		logger.info("Parameter file successfully converted to new format");
	}

	/**
	 * isEmpty
	 * @return
	 */
	public boolean isEmpty() {
		return level.getValue()<=0.001;
	}

	
}
