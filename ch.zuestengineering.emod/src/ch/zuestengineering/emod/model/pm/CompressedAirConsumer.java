/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.model.pm;

import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import ch.zuestengineering.emod.model.APhysicalComponent;
import ch.zuestengineering.emod.model.ParameterCheckReport;
import ch.zuestengineering.emod.model.linking.IOContainer;
import ch.zuestengineering.emod.model.material.Material;

/**
 * Implements the generic model of a compressed air consumer
 * 
 * @author Simon Z�st
 *
 */
@XmlRootElement
public class CompressedAirConsumer extends APhysicalComponent {
	// Inputs
	private IOContainer flowRate;
	private IOContainer temperatureIn;
	// Output parameters:
	private IOContainer pth;

	// Parameters
	private double inletPressure;
	private Material air = new Material("AirIdealGas");

	@XmlElement
	protected String type;

	/**
	 * Empty constructor for unmarshaller
	 */
	public CompressedAirConsumer() {
		super();
	}

	/**
	 * @param u
	 * @param parent
	 */
	public void afterUnmarshal(final Unmarshaller u, final Object parent) {
		// post xml init method (loading physics data)
		init();
	}

	private void init() {

	}

	@Override
	public String getType() {
		return type;
	}

	@Override
	public void update() {
		// pth.setValue(value);
	}

	@Override
	public void updateBoundaryConditions() {
		// TODO Auto-generated method stub

	}

	@Override
	public void setType(String type) {
		// TODO Auto-generated method stub

	}

	@Override
	public ParameterCheckReport checkConfigParams() {
		// TODO Auto-generated method stub
		return new ParameterCheckReport();
	}

	@Override
	public void loadParameters() {
		// TODO Auto-generated method stub
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.zuestengineering.emod.model.APhysicalComponent#updateParameterSet()
	 */
	@Override
	protected void updateParameterSet() {
		// TODO Auto-generated method stub

	}

}
