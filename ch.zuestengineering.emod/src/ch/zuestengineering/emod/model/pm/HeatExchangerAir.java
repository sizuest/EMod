/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.model.pm;

import java.util.ArrayList;

import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import ch.zuestengineering.emod.model.APhysicalComponent;
import ch.zuestengineering.emod.model.ParameterCheckReport;
import ch.zuestengineering.emod.model.fluid.FECZeta;
import ch.zuestengineering.emod.model.fluid.Floodable;
import ch.zuestengineering.emod.model.fluid.FluidCircuitProperties;
import ch.zuestengineering.emod.model.linking.FluidContainer;
import ch.zuestengineering.emod.model.linking.IOContainer;
import ch.zuestengineering.emod.model.parameters.ParameterSet;
import ch.zuestengineering.emod.model.thermal.ThermalElement;
import ch.zuestengineering.emod.model.units.ContainerType;
import ch.zuestengineering.emod.model.units.SiUnit;
import ch.zuestengineering.emod.model.units.Unit;
import ch.zuestengineering.emod.simulation.DynamicState;
import ch.zuestengineering.emod.utils.ComponentConfigReader;

/**
 * General Water-Air Heat exchanger model class. Implements the physical model
 * of a heat exchanger with external coolant supply.
 * 
 * 
 * Inputlist: 1: State : [-] : State of the HE 2: TemperatureAmb : [-] : Ambient
 * temperature 3: FluidIn : [-] : Fluid 1 flowing into the HE Outputlist: 1:
 * PTotal : [W] : Demanded electrical power 2: PLoss : [W] : Losses 3: PThermal
 * : [W] : Heat flux out 4: FluidOut : [-] : Fluid 1 flowing out of the HE
 * 
 * Config parameters: PressureSamples : [Pa] : Pressure samples for liner
 * interpolation FlowRateSamples : [m^3/s] : Volumetric flow samples for liner
 * interpolation ElectricalPower : [W] : Power samples for linear interpolation
 * 
 * @author sizuest
 * 
 */

@XmlRootElement
public class HeatExchangerAir extends APhysicalComponent implements Floodable {
	@XmlElement
	protected String type;

	// Input parameters
	private IOContainer level;
	private IOContainer tempAmb;
	private FluidContainer fluidIn;

	// Output parameters
	private IOContainer ptotal, ploss, pth;
	private FluidContainer fluidOut;

	// Fluid properties
	private FluidCircuitProperties fluidProperties;

	private FECZeta zeta;

	private ThermalElement fluid;

	// Parameters
	private double zetaValue;
	private double htc;
	private double tempOn, tempOff;
	private double power;
	private double volume;

	private boolean wasCooling = false;

	private boolean isInitialized = false;

	/**
	 * Default air heat exchanger
	 */
	public HeatExchangerAir() {
		super();
	}

	/**
	 * HeatExchanger constructor
	 * 
	 * @param type
	 */
	public HeatExchangerAir(String type) {
		super();

		this.type = type;
		init();
	}

	/**
	 * post xml init method (loading physics data)
	 * 
	 * @param u
	 * @param parent
	 */
	public void afterUnmarshal(final Unmarshaller u, final Object parent) {
		init();
	}

	private void init() {
		zeta = new FECZeta(1);

		fluidProperties = new FluidCircuitProperties(zeta);

		// Inputs
		inputs = new ArrayList<IOContainer>();
		level = new IOContainer("State", new SiUnit(Unit.NONE), 0, ContainerType.CONTROL);
		tempAmb = new IOContainer("TemperatureAmb", new SiUnit(Unit.KELVIN), 293.15, ContainerType.THERMAL);
		fluidIn = new FluidContainer("FluidIn", new SiUnit(Unit.NONE), ContainerType.FLUIDDYNAMIC, fluidProperties);
		inputs.add(level);
		inputs.add(tempAmb);
		inputs.add(fluidIn);

		// Outputs
		outputs = new ArrayList<IOContainer>();
		ptotal = new IOContainer("PTotal", new SiUnit(Unit.WATT), 0, ContainerType.ELECTRIC);
		ploss = new IOContainer("PLoss", new SiUnit(Unit.WATT), 0, ContainerType.THERMAL);
		pth = new IOContainer("PThermal", new SiUnit(Unit.WATT), 0, ContainerType.THERMAL);
		fluidOut = new FluidContainer("FluidOut", new SiUnit(Unit.NONE), ContainerType.FLUIDDYNAMIC, fluidProperties);
		outputs.add(ptotal);
		outputs.add(ploss);
		outputs.add(pth);
		outputs.add(fluidOut);

		/* Create thermal elements */
		fluid = new ThermalElement("Example", 1.0);

		/* Read configuration parameters: */
		loadParameters();

		fluid.getTemperature().setName("Temperature");

		fluid.getMass().setName("Mass");

		fluid.setMaterial(fluidProperties.getMaterial());

		dynamicStates = new ArrayList<DynamicState>();
		dynamicStates.add(fluid.getTemperature());

		fluidProperties.setTemperature(fluid.getTemperature());

	}

	@Override
	public String getType() {
		return type;
	}

	@Override
	public void update() {
		fluid.setMaterial(fluidProperties.getMaterial());

		if (!isInitialized) {
			fluid.getMass().setInitialCondition(
					volume * fluid.getMaterial().getDensity(fluid.getTemperature().getValue(), 1E5));
			isInitialized = true;
		}

		if (level.getValue() == 1) {

			/* Controlled Temperature: tempOff=tempOn */
			if (tempOff == tempOn) {
				double heatFlux;

				heatFlux = Math.max(0, (fluidIn.getTemperature() - tempOn) * fluidProperties.getMassFlowRate()
						* fluidProperties.getMaterial().getHeatCapacity(fluidProperties));

				fluid.setHeatInput(-heatFlux);

				ptotal.setValue(power);
				ploss.setValue(power);
			} else {
				double htc = 0;

				if ((wasCooling & fluidProperties.getTemperatureIn() > tempOff)
						| (!wasCooling & fluidProperties.getTemperatureIn() >= tempOn)) {
					wasCooling = true;
					htc = this.htc;

					ptotal.setValue(power);
					ploss.setValue(power);
				} else {
					wasCooling = false;

					ptotal.setValue(0);
					ploss.setValue(0);
				}

				fluid.setThermalResistance(htc);
			}

		} else {
			wasCooling = false;
			fluid.setHeatInput(0);
			ptotal.setValue(0);
			ploss.setValue(0);
			fluid.setThermalResistance(0);
		}

		fluid.setTemperatureIn(fluidIn.getTemperature());
		fluid.integrate(timestep, fluidProperties.getFlowRate(), fluidProperties.getFlowRate(),
				fluidProperties.getPressure());
		fluid.setTemperatureAmb(tempAmb.getValue());

		pth.setValue(fluidProperties.getEnthalpyChange() + ploss.getValue());
	}

	@Override
	public void setType(String type) {
		this.type = type;
	}

	@Override
	public ArrayList<FluidCircuitProperties> getFluidPropertiesList() {
		ArrayList<FluidCircuitProperties> out = new ArrayList<FluidCircuitProperties>();
		out.add(fluidProperties);
		return out;
	}

	@Override
	public void flood() {/* Not used */
	}

	@Override
	public void updateBoundaryConditions() {/* Not used */
	}

	@Override
	public ParameterCheckReport checkConfigParams() {
		ParameterCheckReport report = new ParameterCheckReport();

		if (zetaValue <= 0) {
			report.add("PressureLossCoefficient",
					"Negative or zero: 'PressureLossCoefficient' must be strictly positive");
		}
		if (htc <= 0) {
			report.add("HeatTransferCoefficient",
					"Negative or zero: 'HeatTransferCoefficient' must be strictly positive");
		}
		if (tempOn <= 0) {
			report.add("TemperatureHigh", "Negative or zero: 'TemperatureHigh' must be strictly positive");
		}
		if (tempOff <= 0) {
			report.add("TemperatureLow", "Negative or zero: 'TemperatureLow' must be strictly positive");
		}
		if (tempOn <= tempOff) {
			report.add("TemperatureHigh",
					"Values not matching: 'TemperatureHigh' must be bigger than 'TemperatureLow'");
		}
		if (power == 0) {
			report.add("Power", "Negative: 'Power' must be positive");
		}
		if (volume <= 0) {
			report.add("Volume", "Negative or zero: 'Volume' must be strictly positive");
		}

		return report;
	}

	@Override
	public void loadParameters() {

		super.loadParameters();

		zetaValue = parameterSet.getPhysicalValue("PressureLossCoefficient").getValue();
		htc = parameterSet.getPhysicalValue("HeatTransferCoefficient").getValue();
		tempOn = parameterSet.getPhysicalValue("TemperatureHigh").getValue();
		tempOff = parameterSet.getPhysicalValue("TemperatureLow").getValue();
		power = parameterSet.getPhysicalValue("Power").getValue();
		volume = parameterSet.getPhysicalValue("Volume").getValue();

		/* Set pressure loss coeff. */
		zeta.setZeta(zetaValue);

		fluid.setVolume(volume);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.zuestengineering.emod.model.APhysicalComponent#updateParameterSet()
	 */
	@Override
	protected void updateParameterSet() {
		logger.warning(
				"Parameter set is empty; tying for old parameter set format (this option will be removed in later versions) ...");
		ComponentConfigReader params = null;
		/* Open file containing the parameters of the model type: */
		try {
			params = new ComponentConfigReader(getModelType(), type);
		} catch (Exception e) {
			e.printStackTrace();
		}

		/* Read the config parameter: */
		try {
			zetaValue = params.getPhysicalValue("PressureLossCoefficient", new SiUnit("Pa s^2 m^-6")).getValue();
			htc = params.getPhysicalValue("HeatTransferCoefficient", new SiUnit("W m^-2 K^-1")).getValue();
			tempOn = params.getPhysicalValue("TemperatureHigh", new SiUnit("K")).getValue();
			tempOff = params.getPhysicalValue("TemperatureLow", new SiUnit("K")).getValue();
			power = params.getPhysicalValue("Power", new SiUnit("W")).getValue();
			volume = params.getPhysicalValue("Volume", new SiUnit("m^3")).getValue();
		} catch (Exception e) {
			e.printStackTrace();
		}

		parameterSet = new ParameterSet(this.toString());
		parameterSet.setPhysicalValue("PressureLossCoefficient", zetaValue, new SiUnit("Pa s^2 m^-6"));
		parameterSet.setPhysicalValue("HeatTransferCoefficient", htc, new SiUnit("W m^-2 K^-1"));
		parameterSet.setPhysicalValue("TemperatureHigh", tempOn, new SiUnit("K"));
		parameterSet.setPhysicalValue("TemperatureLow", tempOff, new SiUnit("K"));
		parameterSet.setPhysicalValue("Power", power, new SiUnit("W"));
		parameterSet.setPhysicalValue("Volume", volume, new SiUnit("m^3"));
		parameterSet.save(getParameterFile());

		logger.info("Parameter file successfully converted to new format");

	}

}
