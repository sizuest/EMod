/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.model.pm;

import java.util.ArrayList;

import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;

import ch.zuestengineering.emod.model.APhysicalComponent;
import ch.zuestengineering.emod.model.ParameterCheckReport;
import ch.zuestengineering.emod.model.linking.IOContainer;
import ch.zuestengineering.emod.model.parameters.ParameterSet;
import ch.zuestengineering.emod.model.units.ContainerType;
import ch.zuestengineering.emod.model.units.SiUnit;

/**
 * Simulation control class for moments based on the kienzle approximation.
 * 
 * @author dhampl
 * 
 */
public class CuttingProcess extends APhysicalComponent {
	
	@XmlElement
	protected String type;

	/* Model inputs */
	protected IOContainer rotSpeed;
	protected IOContainer linSpeed;
	protected IOContainer diameter;
	protected IOContainer cuttingDepth;
	
	/* Model outputs */
	protected IOContainer torque;
	protected IOContainer forceCutting;
	protected IOContainer forceFeed;
	protected IOContainer forcePassive;
	
	/* Model parameters */
	protected double kappa; // kienzle constant 
	protected double kcCutting; // kienzle constant
	protected double kcFeed; // kienzle constant
	protected double kcPassive; // kienzle constant
	protected double zCutting; // kienzle constant
	protected double zFeed; // kienzle constant
	protected double zPassive; // kienzle constant

	/**
	 *
	 */
	public CuttingProcess() {
		super();
	}
	
	/**
	 * afterUnmarshal
	 * @param u
	 * @param parent
	 */
	public void afterUnmarshal(Unmarshaller u, Object parent) {
		init();
	}

	/**
	 * @param type 
	 */
	public CuttingProcess(String type) {
		super();

		this.type = type;
		init();
	}

	

	private void init() {
		/* Inputs */
		inputs = new ArrayList<>();
		rotSpeed = new IOContainer("RotSpeed", new SiUnit("Hz"), 0.0, ContainerType.MECHANIC);
		linSpeed = new IOContainer("FeedRate", new SiUnit("m/s"), 0.0, ContainerType.MECHANIC);
		cuttingDepth = new IOContainer("CuttingDepth", new SiUnit("m"), 0.0, ContainerType.MECHANIC);
		diameter = new IOContainer("Diameter", new SiUnit("m"), 0.0, ContainerType.MECHANIC);
		inputs.add(rotSpeed);
		inputs.add(linSpeed);
		inputs.add(cuttingDepth);
		inputs.add(diameter);
		
		/* Outputs */
		outputs = new ArrayList<>();
		torque = new IOContainer("Torque", new SiUnit("Nm"), 0.0, ContainerType.MECHANIC);
		forceCutting = new IOContainer("CuttingForce", new SiUnit("N"), 0.0, ContainerType.MECHANIC);
		forceFeed = new IOContainer("FeedForce", new SiUnit("N"), 0.0, ContainerType.MECHANIC);
		forcePassive = new IOContainer("PassiveForce", new SiUnit("N"), 0.0, ContainerType.MECHANIC);
		outputs.add(torque);
		outputs.add(forceCutting);
		outputs.add(forceFeed);
		outputs.add(forcePassive);
		
		/* Parameter */
		loadParameters();
		
		kappa		= 95; // kienzle constant 
		kcCutting	= 1768e6; // kienzle constant
		kcFeed		=  610e6; // kienzle constant
		kcPassive	=  462e6; // kienzle constant
		zCutting	= 0.31; // kienzle constant
		zFeed		= 0.63; // kienzle constant
		zPassive	= 0.51; // kienzle constant
	}
	
	protected double getForce(double kc, double z) {		
		return kc * getChipWidth() * Math.pow(getChipHeight(), 1 - z);
	}
	
	protected double getChipWidth() {
		return cuttingDepth.getValue() / Math.sin(kappa);
	}
	
	protected double getChipHeight() {
		if(rotSpeed.getValue()==0)
			return 0;
		return 2.0*Math.PI*linSpeed.getValue()/rotSpeed.getValue();
	}
	
	protected double getForceCutting() {
		return getForce(kcCutting, zCutting);
	}
	
	protected double getForceFeed() {
		return getForce(kcFeed, zFeed);
	}
	
	protected double getForcePassive() {
		return getForce(kcPassive, zPassive);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.ethz.inspire.emod.simulation.ASimulationControl#update()
	 */
	@Override
	public void update() {
		
		forceCutting.setValue(getForceCutting());
		forceFeed.setValue(getForceFeed());
		forcePassive.setValue(getForcePassive());
		
		if(diameter.getValue() == 0)
			torque.setValue(0.0);
		else
			torque.setValue(forceCutting.getValue()*diameter.getValue()/2.0);

	}

	/* (non-Javadoc)
	 * @see ch.zuestengineering.emod.model.APhysicalComponent#getType()
	 */
	@Override
	public String getType() {
		return this.type;
	}

	/* (non-Javadoc)
	 * @see ch.zuestengineering.emod.model.APhysicalComponent#updateBoundaryConditions()
	 */
	@Override
	public void updateBoundaryConditions() {
		// Not used: No BC
	}

	/* (non-Javadoc)
	 * @see ch.zuestengineering.emod.model.APhysicalComponent#setType(java.lang.String)
	 */
	@Override
	public void setType(String type) {
		this.type = type;
	}

	/* (non-Javadoc)
	 * @see ch.zuestengineering.emod.model.APhysicalComponent#checkConfigParams()
	 */
	@Override
	public ParameterCheckReport checkConfigParams() {
		ParameterCheckReport report = new ParameterCheckReport();
		
		if(kcCutting<=0)
			report.add("Material Constant", "Material constant for Kienzle model (cutting) must be positive");
		if(kcFeed<=0)
			report.add("Material Constant", "Material constant for Kienzle model (feed) must be positive");
		if(kcPassive<=0)
			report.add("Material Constant", "Material constant for Kienzle model (passive) must be positive");
		if(zCutting<=0)
			report.add("Increase rate", "Material constant for Kienzle model (cutting) must be positive");
		if(zFeed<=0)
			report.add("Increase rate", "Material constant for Kienzle model (feed) must be positive");
		if(zPassive<=0)
			report.add("Increase rate", "Material constant for Kienzle model (passive) must be positive");
		
		return report;
	}
	
	@Override
	public void loadParameters() {
		super.loadParameters();

		kappa		= parameterSet.getPhysicalValue("SettingAngle").getValue()/180*Math.PI;
		kcCutting	= parameterSet.getPhysicalValue("MaterialConstantCutting").getValue();
		kcFeed		= parameterSet.getPhysicalValue("MaterialConstantFeed").getValue();
		kcPassive	= parameterSet.getPhysicalValue("MaterialConstantPassive").getValue();
		zCutting	= parameterSet.getPhysicalValue("IncreaseRateCutting").getValue();
		zFeed		= parameterSet.getPhysicalValue("IncreaseRateFeed").getValue();
		zPassive	= parameterSet.getPhysicalValue("IncreaseRatePassive").getValue();

	}

	/* (non-Javadoc)
	 * @see ch.zuestengineering.emod.model.APhysicalComponent#updateParameterSet()
	 */
	@Override
	protected void updateParameterSet() {
		logger.warning("Parameter set is empty; tying to create new parameterset ...");
		
		
		kappa		= 95.0/180.0*Math.PI;
		kcCutting	= 1768e6;
		kcFeed		=  610e6;
		kcPassive	=  462e6; 
		zCutting	= 0.31;
		zFeed		= 0.63; 
		zPassive	= 0.51;
		

		parameterSet = new ParameterSet(this.toString());
		parameterSet.setPhysicalValue("SettingAngle", kappa*180/Math.PI, new SiUnit("�"));
		parameterSet.setPhysicalValue("MaterialConstantCutting", kcCutting, new SiUnit("N m^-2"));
		parameterSet.setPhysicalValue("MaterialConstantFeed",    kcFeed,    new SiUnit("N m^-2"));
		parameterSet.setPhysicalValue("MaterialConstantPassive", kcPassive, new SiUnit("N m^-2"));
		parameterSet.setPhysicalValue("IncreaseRateCutting", zCutting, new SiUnit(""));
		parameterSet.setPhysicalValue("IncreaseRateFeed",    zFeed   , new SiUnit(""));
		parameterSet.setPhysicalValue("IncreaseRatePassive", zPassive, new SiUnit(""));
		parameterSet.save(getParameterFile());
		logger.info("Parameter file successfully converted to new format");
		
	}
}
