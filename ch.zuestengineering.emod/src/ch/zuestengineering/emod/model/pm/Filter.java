/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.model.pm;

import java.util.ArrayList;

import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import ch.zuestengineering.emod.model.APhysicalComponent;
import ch.zuestengineering.emod.model.ParameterCheckReport;
import ch.zuestengineering.emod.model.fluid.FECZeta;
import ch.zuestengineering.emod.model.fluid.Floodable;
import ch.zuestengineering.emod.model.fluid.FluidCircuitProperties;
import ch.zuestengineering.emod.model.linking.FluidContainer;
import ch.zuestengineering.emod.model.linking.IOContainer;
import ch.zuestengineering.emod.model.parameters.ParameterSet;
import ch.zuestengineering.emod.model.units.ContainerType;
import ch.zuestengineering.emod.model.units.SiUnit;
import ch.zuestengineering.emod.model.units.Unit;
import ch.zuestengineering.emod.simulation.DynamicState;

/**
 * General filter model class. Implements the physical model of a filter. 
 * 
 * 
 * 
 * Inputlist: 1: FluidIn : [-]
 * 
 * Outputlist: 
 * 1: FluidOut: [-]
 * 
 * Config parameters: 
 * 	PressureLossCoefficient : [-] 
 *  MaxLoad: [m3]
 * 
 * @author kraandre
 * 
 */
@XmlRootElement
public class Filter extends APhysicalComponent implements Floodable {

	@XmlElement
	protected String type;

	// Input parameters:
	private FluidContainer fluidIn;

	// Output parameters:
	private FluidContainer fluidOut;

	// Parameters used by the model.
	private double zeta;
	private double maxLoad;

	// Fluid properties
	private FluidCircuitProperties fluidProperties;
	
	// States
	private DynamicState loadState;

	/**
	 * Constructor called from XmlUnmarshaller. Attribute 'type' is set by
	 * XmlUnmarshaller.
	 */
	public Filter() {
		super();
	}

	/**
	 * @param u
	 * @param parent
	 */
	public void afterUnmarshal(Unmarshaller u, Object parent) {
		// post xml init method (loading physics data)
		init();
	}

	/**
	 * Valve constructor
	 * 
	 * @param type
	 */
	public Filter(String type) {
		super();

		this.type = type;
		init();
	}

	/**
	 * Called from constructor or after unmarshaller.
	 */
	private void init() {
		
		/* Load parameters */
		loadParameters();

		/* Define Input parameters */
		inputs = new ArrayList<IOContainer>();

		/* Fluid Properties */
		fluidProperties = new FluidCircuitProperties(new FECZeta(zeta));

		/* Define output parameters */
		outputs = new ArrayList<IOContainer>();

		/* Fluid in- and output */
		fluidIn = new FluidContainer("FluidIn", new SiUnit(Unit.NONE), ContainerType.FLUIDDYNAMIC, fluidProperties);
		fluidOut = new FluidContainer("FluidOut", new SiUnit(Unit.NONE), ContainerType.FLUIDDYNAMIC, fluidProperties);
		inputs.add(fluidIn);
		outputs.add(fluidOut);

		/* Dynamic State */
		dynamicStates = new ArrayList<>();
		loadState = new DynamicState("Load", new SiUnit("m^3"));
		dynamicStates.add(loadState);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.ethz.inspire.emod.model.APhysicalComponent#update()
	 */
	@Override
	public void update() {
		/* Integrate load state */
		loadState.setValue(loadState.getValue()+fluidProperties.getFlowRate()*timestep);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.ethz.inspire.emod.model.APhysicalComponent#getType()
	 */
	@Override
	public String getType() {
		return type;
	}

	@Override
	public void setType(String type) {
		this.type = type;
	}

	@Override
	public ArrayList<FluidCircuitProperties> getFluidPropertiesList() {
		ArrayList<FluidCircuitProperties> out = new ArrayList<FluidCircuitProperties>();
		out.add(fluidProperties);
		return out;
	}

	@Override
	public void flood() {/* Not used */
	}

	@Override
	public void updateBoundaryConditions() {/* Not used */
	}

	@Override
	public ParameterCheckReport checkConfigParams() {
		ParameterCheckReport report = new ParameterCheckReport();
		if (zeta <= 0)
			report.add("PressureLossCoefficient", "Pressure loss coefficient must be greater than zero");

		if (maxLoad <= 0)
			report.add("MaxLoad", "MaxLoad must be greater than zero");

		return report;
	}

	@Override
	public void loadParameters() {
		super.loadParameters();

		zeta = parameterSet.getPhysicalValue("PressureLossCoefficient").getValue();
		maxLoad = parameterSet.getPhysicalValue("MaxLoad").getValue();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.zuestengineering.emod.model.APhysicalComponent#updateParameterSet()
	 */
	@Override
	protected void updateParameterSet() {
		logger.warning("Parameter set is empty; creating new default parameterset");

		parameterSet = new ParameterSet(this.toString());
		parameterSet.setPhysicalValue("PressureLossCoefficient", 1.0, new SiUnit("m^-4"));
		parameterSet.setPhysicalValue("MaxLoad", 100, new SiUnit("m^3"));
		parameterSet.save(getParameterFile());
		logger.info("Parameter file successfully created");

	}

}
