/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.model.pm;

import java.util.ArrayList;

import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import java.lang.Math;
import java.lang.reflect.Constructor;

import ch.zuestengineering.emod.dd.Duct;
import ch.zuestengineering.emod.femexport.BoundaryCondition;
import ch.zuestengineering.emod.femexport.BoundaryConditionType;
import ch.zuestengineering.emod.model.APhysicalComponent;
import ch.zuestengineering.emod.model.ParameterCheckReport;
import ch.zuestengineering.emod.model.fluid.FECDuct;
import ch.zuestengineering.emod.model.fluid.Floodable;
import ch.zuestengineering.emod.model.fluid.FluidCircuitProperties;
import ch.zuestengineering.emod.model.linking.FluidContainer;
import ch.zuestengineering.emod.model.linking.IOContainer;
import ch.zuestengineering.emod.model.parameters.ParameterSet;
import ch.zuestengineering.emod.model.parameters.PhysicalValue;
import ch.zuestengineering.emod.model.thermal.ThermalArray;
import ch.zuestengineering.emod.model.thermal.ThermalElement;
import ch.zuestengineering.emod.model.units.*;
import ch.zuestengineering.emod.simulation.DynamicState;
import ch.zuestengineering.emod.utils.ComponentConfigReader;

/**
 * General Spindle model class. Implements the physical model Spindels.
 * 
 * 
 * Inputlist: 1: State : [-] : On/Off 2: RotSpeed : [rpm] : Actual rotational
 * speed 3: Torque : [Nm] : Actual torque Outputlist: 1: PTotal : [W] :
 * Calculated total energy demand 2: PLoss : [W] : Calculated power loss 3: PUse
 * : [W] : Calculated mechanical power 4: TemperatureOut : [K] : Coolant outlet
 * temperature 5: CAirFlow : [l/min]: Compressed air flow rate
 * 
 * Config parameters: TODO
 * 
 * @author sizuest
 * 
 */
@XmlRootElement
public class Spindle extends APhysicalComponent implements Floodable {

	@XmlElement
	protected String type;

	// Input parameters:
	private IOContainer state;
	private IOContainer stateCairLub;
	private IOContainer stateCairSeal;
	private IOContainer rotspeed;
	private IOContainer torque;
	// Output parameters:
	private IOContainer pmech;
	private IOContainer ploss;
	private IOContainer pel;
	private IOContainer cairFlow;
	private IOContainer temperature;
	// Boundary Conditions
	private BoundaryCondition bcHeatSrcAirgap;
	private BoundaryCondition bcHeatSrcCair;
	private BoundaryCondition bcCoolantHTC;
	private BoundaryCondition bcCoolantTemperature;
	private BoundaryCondition bcCoolantHeatFlux;
	// Fluid
	private FluidContainer fluidIn, fluidOut;
	// Fluid Properties
	FluidCircuitProperties fluidProperties;

	// Parameters used by the model.
	private double[] preloadForce;
	private double massStructure;
	private double alphaCoolant, volumeCoolant;
	private double lastTemperatureIn = Double.NaN;

	private double agDiameter;
	private double agGapWidth;
	private double agGapLength;

	private double cairFlowLub;
	private double cairFlowSeal;
	private double transmissionRatio = 1;

	private boolean cairLubHasState = false;
	private boolean cairSealHasState = false;

	// Submodels
	private AMotor motor;
	private ArrayList<Bearing> bearings;
	private ThermalElement structure;
	private ThermalArray coolant;
	private Duct coolingDuct;

	// Global values
	private double frictionAirGap;

	/**
	 * Constructor called from XmlUnmarshaller. Attribute 'type' is set by
	 * XmlUnmarshaller.
	 */
	public Spindle() {
		super();
	}

	/**
	 * @param u
	 * @param parent
	 */
	public void afterUnmarshal(Unmarshaller u, Object parent) {
		// post xml init method (loading physics data)
		init();
	}

	/**
	 * Spindle constructor
	 * 
	 * @param type
	 */
	public Spindle(String type) {
		super();

		this.type = type;
		init();
	}

	@Override
	public IOContainer getInput(String name) {
		IOContainer out = super.getInput(name);

		/*
		 * check if anything is connected to the cair state of the lubrication or the
		 * sealing air
		 */
		if (out.equals(stateCairLub))
			cairLubHasState = true;
		else if (out.equals(stateCairSeal))
			cairSealHasState = true;

		return out;
	}

	/**
	 * Called from constructor or after unmarshaller.
	 */
	private void init() {
		/* Define Input parameters */
		inputs = new ArrayList<IOContainer>();
		state = new IOContainer("State", new SiUnit(Unit.NONE), 0, ContainerType.CONTROL);
		stateCairLub = new IOContainer("Lubrication", new SiUnit(""), 0, ContainerType.CONTROL);
		stateCairSeal = new IOContainer("SealingAir", new SiUnit(""), 0, ContainerType.CONTROL);
		rotspeed = new IOContainer("RotSpeed", new SiUnit(Unit.REVOLUTIONS_S), 0, ContainerType.MECHANIC);
		torque = new IOContainer("Torque", new SiUnit(Unit.NEWTONMETER), 0, ContainerType.MECHANIC);
		inputs.add(state);
		inputs.add(stateCairLub);
		inputs.add(stateCairSeal);
		inputs.add(rotspeed);
		inputs.add(torque);

		/* Define output parameters */
		outputs = new ArrayList<IOContainer>();
		pmech = new IOContainer("PUse", new SiUnit(Unit.WATT), 0, ContainerType.MECHANIC);
		ploss = new IOContainer("PLoss", new SiUnit(Unit.WATT), 0, ContainerType.THERMAL);
		pel = new IOContainer("PTotal", new SiUnit(Unit.WATT), 0, ContainerType.ELECTRIC);
		cairFlow = new IOContainer("CAirFlow", new SiUnit(Unit.METERCUBIC_S), 0, ContainerType.FLUIDDYNAMIC);
		temperature = new IOContainer("Temperature", new SiUnit(Unit.KELVIN), 293, ContainerType.THERMAL);
		outputs.add(pel);
		outputs.add(ploss);
		outputs.add(pmech);
		outputs.add(cairFlow);
		outputs.add(temperature);

		coolingDuct = new Duct();
		structure = new ThermalElement("Example", 1);
		coolant = new ThermalArray("Example", 1, 20);

		/* Read configuration parameters: */
		loadParameters();

		/* Fluid properties */
		fluidProperties = new FluidCircuitProperties(new FECDuct(coolingDuct, coolant.getTemperature()),
				coolant.getTemperature());

		/* Define fluid in-/outputs */
		fluidIn = new FluidContainer("CoolantIn", new SiUnit(Unit.NONE), ContainerType.FLUIDDYNAMIC, fluidProperties);
		inputs.add(fluidIn);
		fluidOut = new FluidContainer("CoolantOut", new SiUnit(Unit.NONE), ContainerType.FLUIDDYNAMIC, fluidProperties);
		outputs.add(fluidOut);

		// Change state names
		structure.getTemperature().setName("TemperatureStructure");
		coolant.getTemperature().setName("TemperatureCoolant");

		// Add states
		dynamicStates = new ArrayList<DynamicState>();
		dynamicStates.add(0, structure.getTemperature());
		dynamicStates.add(1, coolant.getTemperature());

		/* Fluid circuit parameters */
		coolant.setMaterial(fluidProperties.getMaterial());
		coolingDuct.setMaterial(fluidProperties.getMaterial());

		/* Boundary conditions */
		boundaryConditions = new ArrayList<BoundaryCondition>();
		bcHeatSrcAirgap = new BoundaryCondition("HeatSrcAirgap", new SiUnit("W"), 0, BoundaryConditionType.NEUMANN);
		bcHeatSrcCair = new BoundaryCondition("HeatSrcCair", new SiUnit("W"), 0, BoundaryConditionType.NEUMANN);
		bcCoolantHTC = new BoundaryCondition("CoolantHTC", new SiUnit("W/K"), 0, BoundaryConditionType.ROBIN);
		bcCoolantTemperature = new BoundaryCondition("CoolantTemperature", new SiUnit("K"), 293.15,
				BoundaryConditionType.ROBIN);
		bcCoolantHeatFlux = new BoundaryCondition("CoolantHeatFlux", new SiUnit("W"), 0, BoundaryConditionType.NEUMANN);
		// Motor
		for (BoundaryCondition bc : motor.getBoundaryConditions())
			bc.setName("Motor" + bc.getName());
		// Bearing
		boundaryConditions.addAll(motor.getBoundaryConditions());

		int i = 1;
		for (Bearing b : bearings) {
			for (BoundaryCondition bc : b.getBoundaryConditions())
				bc.setName("Bearing" + i + bc.getName());
			boundaryConditions.addAll(b.getBoundaryConditions());
			i++;
		}
		// Others
		boundaryConditions.add(bcHeatSrcAirgap);
		boundaryConditions.add(bcHeatSrcCair);
		boundaryConditions.add(bcCoolantHTC);
		boundaryConditions.add(bcCoolantTemperature);
		boundaryConditions.add(bcCoolantHeatFlux);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.ethz.inspire.emod.model.APhysicalComponent#update()
	 */
	@Override
	public void update() {

		if (fluidIn.getTemperature() <= 0) {
			if (Double.isNaN(lastTemperatureIn))
				lastTemperatureIn = structure.getTemperature().getValue();
		} else
			lastTemperatureIn = fluidIn.getTemperature();

		double frictionTorque = 0;
		double frictionLosses = 0;

		if (1 == state.getValue()) {

			// Bearings
			if (rotspeed.getValue() != 0) {
				int i = 0;
				for (Bearing b : bearings) {
					b.getInput("RotSpeed").setValue(rotspeed.getValue());
					b.getInput("ForceRadial").setValue(0);// TODO
					b.getInput("ForceAxial").setValue(preloadForce[i]);
					b.getInput("Temperature1").setValue(structure.getTemperature().getValue());
					b.getInput("Temperature2").setValue(structure.getTemperature().getValue());
					b.update();

					frictionTorque += b.getOutput("Torque").getValue();
					frictionLosses += b.getOutput("PLoss").getValue();
					i++;
				}
			}

			// Air Gap
			frictionAirGap = AirGap.getTorque(rotspeed.getValue(), structure.getTemperature().getValue(), agDiameter,
					agGapWidth, agGapLength);

			frictionTorque += frictionAirGap;
			frictionLosses += frictionAirGap * rotspeed.getValue() * 2 * Math.PI;

			motor.getInput("RotSpeed").setValue(rotspeed.getValue() / transmissionRatio);
			motor.getInput("Torque").setValue((frictionTorque + torque.getValue()) * transmissionRatio);
			motor.update();

			pmech.setValue(rotspeed.getValue() * torque.getValue() * Math.PI * 2);
			pel.setValue(motor.getOutput("PTotal").getValue());
			// ploss.setValue(motor.getOutput("PLoss").getValue()+frictionLosses);
			ploss.setValue(motor.getOutput("PLoss").getValue() + frictionLosses);
		} else {
			motor.getOutput("PUse").setValue(0);
			motor.getOutput("PLoss").setValue(0);
			motor.getOutput("PTotal").setValue(0);

			pmech.setValue(0);
			pel.setValue(0);
			// ploss.setValue(motor.getOutput("PLoss").getValue()+frictionLosses);
			ploss.setValue(0);
		}
		
		// Compressed air flow
		if (cairSealOn() & cairLubOn())
			cairFlow.setValue(cairFlowLub+cairFlowSeal);
		else if(cairLubOn())
			cairFlow.setValue(cairFlowLub);
		else if(cairSealOn())
			cairFlow.setValue(cairFlowSeal);
		else
			cairFlow.setValue(0);
			
			

		// Thermal resistance
		alphaCoolant = coolingDuct.getThermalResistance(fluidProperties.getFlowRate(), fluidProperties.getPressureIn(),
				fluidProperties.getTemperatureIn(), structure.getTemperature().getValue());

		// Coolant
		coolant.setThermalResistance(alphaCoolant);
		coolant.setFlowRate(fluidProperties);
		coolant.setHeatSource(0.0);
		coolant.setTemperatureAmb(structure.getTemperature().getValue());
		coolant.setTemperatureIn(fluidIn.getTemperature());

		// Thermal flows
		structure.setHeatInput(motor.getOutput("PLoss").getValue() + frictionLosses);
		structure.addHeatInput(coolant.getHeatLoss());

		// Update submodels
		structure.integrate(timestep);
		// TODO set Pressure!
		coolant.integrate(timestep, 0, 0, 100000);

		// Write outputs
		temperature.setValue(structure.getTemperature().getValue());

	}
	
	private boolean cairSealOn() {
		return (1 == stateCairSeal.getValue()) | !cairSealHasState;
	}
	
	private boolean cairLubOn() {
		return (1 == stateCairLub.getValue()) | !cairLubHasState;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.ethz.inspire.emod.model.APhysicalComponent#getType()
	 */
	@Override
	public String getType() {
		return type;
	}

	@Override
	public void setType(String type) {
		this.type = type;
	}

	@Override
	public ArrayList<FluidCircuitProperties> getFluidPropertiesList() {
		ArrayList<FluidCircuitProperties> out = new ArrayList<FluidCircuitProperties>();
		out.add(fluidProperties);
		return out;
	}

	@Override
	public void flood() {/* Not used */
	}

	@Override
	public void updateBoundaryConditions() {
		motor.updateBoundaryConditions();
		for (Bearing b : bearings)
			b.updateBoundaryConditions();

		bcHeatSrcAirgap.setValue(frictionAirGap * rotspeed.getValue() * 2 * Math.PI);
		bcHeatSrcCair.setValue(0); // TODO
		bcCoolantTemperature.setValue(coolant.getTemperature().getValue());
		bcCoolantHTC.setValue(alphaCoolant);
		bcCoolantHeatFlux.setValue(coolant.getHeatLoss());
	}

	@Override
	public ParameterCheckReport checkConfigParams() {
		ParameterCheckReport report = new ParameterCheckReport();
		// TODO Auto-generated method stub
		return report;
	}

	@Override
	public void loadParameters() {
		super.loadParameters();
		
		

		preloadForce = parameterSet.getPhysicalValue("PreloadForce").getValues();
		massStructure = parameterSet.getPhysicalValue("StructureMass").getValue();
		agDiameter = parameterSet.getPhysicalValue("AirGapDiameter").getValue();
		agGapWidth = parameterSet.getPhysicalValue("AirGapWidth").getValue();
		agGapLength = parameterSet.getPhysicalValue("AirGapLength").getValue();
		cairFlowLub = parameterSet.getPhysicalValue("LubricationCompressedAirFlow").getValue();
		cairFlowSeal = parameterSet.getPhysicalValue("SealingAirCompressedAirFlow").getValue();
		
		transmissionRatio = parameterSet.getDouble("TransmissionRatio");
		if(Double.isNaN(transmissionRatio)) {
			parameterSet.setDouble("TransmissionRatio", 1);
			transmissionRatio = 1;
		}

		// Create Sub Elements
		motor = (AMotor) parameterSet.getModel("MotorType");
		if(parameterSet.getModel("MotorType", true).getFilter().equals("")) {
			parameterSet.getModel("MotorType", true).setFilter("Motor");
			parameterSet.save(getParameterFile());
		}

		bearings = new ArrayList<>();
		for (APhysicalComponent b : parameterSet.getModels("BearingType"))
			if (b instanceof Bearing)
				bearings.add((Bearing) b);
		if(parameterSet.getModels("BearingType", true).getFilter().equals("")) {
			parameterSet.getModels("BearingType", true).setFilter("Bearing");
			parameterSet.save(getParameterFile());
		}

		coolingDuct = parameterSet.getDuct("StructureDuct");
		volumeCoolant = coolingDuct.getVolume();
		structure.setMaterial(parameterSet.getMaterial("StructureMaterial"));
		structure.setMass(massStructure);
		coolant.setVolume(volumeCoolant);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.zuestengineering.emod.model.APhysicalComponent#updateParameterSet()
	 */
	@Override
	protected void updateParameterSet() {
		logger.warning(
				"Parameter set is empty; tying for old parameter set format (this option will be removed in later versions) ...");

		ComponentConfigReader params = null;
		/* Open file containing the parameters of the model type: */
		try {
			params = new ComponentConfigReader(getModelType(), type);
		} catch (Exception e) {
			e.printStackTrace();
		}

		/* Read the config parameter: */
		try {

			String motorType = params.getString("MotorType");
			preloadForce = params.getPhysicalValue("PreloadForce", new SiUnit("N")).getValues();
			String[] bearingType = params.getStringArray("BearingType");
			massStructure = params.getPhysicalValue("StructureMass", new SiUnit("kg")).getValue();
			agDiameter = params.getValue("AirGapDiameter", new PhysicalValue(0.0, new SiUnit("m"))).getValue();
			agGapWidth = params.getValue("AirGapWidth", new PhysicalValue(0.0, new SiUnit("m"))).getValue();
			agGapLength = params.getValue("AirGapLength", new PhysicalValue(0.0, new SiUnit("m"))).getValue();
			cairFlowLub = params.getValue("LubricationCompressedAirFlow", new PhysicalValue(0.0, new SiUnit("m^3/s")))
					.getValue();
			cairFlowSeal = params.getValue("SealingAirCompressedAirFlow", new PhysicalValue(0.0, new SiUnit("m^3/s")))
					.getValue();

			String[] mdlType = motorType.split("_");
			String paramType = "";
			for(int i=1; i<mdlType.length; i++)
				paramType+=mdlType[i];

			// Create Sub Elements
			try {
				// Get class and constructor objects
				String path = APhysicalComponent.class.getPackage().getName();
				Class<?> cl = Class.forName(path + ".pm." + mdlType[0]);
				Constructor<?> co = cl.getConstructor(String.class);
				// initialize new component
				motor = (AMotor) co.newInstance(paramType);
			} catch (Exception e) {
				Exception ex = new Exception(
						"Unable to create component " + mdlType[0] + "(" + paramType + ")" + " : " + e.getMessage());
				ex.printStackTrace();
				motor = null;
			}

			bearings = new ArrayList<>();
			for (int i = 0; i < bearingType.length; i++)
				bearings.add(new Bearing(bearingType[i]));

			Duct newDuct = Duct.buildFromFile(getModelType(), getType(), params.getString("StructureDuct"));

			coolingDuct.clone(newDuct);
			coolingDuct.setName(newDuct.getName());
			volumeCoolant = coolingDuct.getVolume();
			structure.setMaterial(params.getString("StructureMaterial"));
			structure.setMass(massStructure);
			coolant.setVolume(volumeCoolant);

		} catch (Exception e) {
			e.printStackTrace();
		}
		params.Close(); /* Model configuration file not needed anymore. */

		parameterSet = new ParameterSet(this.toString());
		parameterSet.setModel("MotorType", motor);
		parameterSet.setPhysicalValue("PreloadForce", preloadForce, new SiUnit("N"));
		parameterSet.setBearings("BearingType", bearings);
		parameterSet.setPhysicalValue("StructureMass", massStructure, new SiUnit("kg"));
		parameterSet.setPhysicalValue("AirGapDiameter", agDiameter, new SiUnit("m"));
		parameterSet.setPhysicalValue("AirGapWidth", agGapWidth, new SiUnit("m"));
		parameterSet.setPhysicalValue("AirGapLength", agGapLength, new SiUnit("m"));
		parameterSet.setPhysicalValue("LubricationCompressedAirFlow", cairFlowLub, new SiUnit("m^3/s"));
		parameterSet.setPhysicalValue("SealingAirCompressedAirFlow", cairFlowSeal, new SiUnit("m^3/s"));
		parameterSet.setDuct("StructureDuct", coolingDuct);
		parameterSet.setMaterial("StructureMaterial", structure.getMaterial());

		parameterSet.save(getParameterFile());
		logger.info("Parameter file successfully converted to new format");

	}
}
