/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.model.pm;

import java.util.ArrayList;

import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.lang.Math;

import ch.zuestengineering.emod.model.APhysicalComponent;
import ch.zuestengineering.emod.model.ParameterCheckReport;
import ch.zuestengineering.emod.model.linking.IOContainer;
import ch.zuestengineering.emod.model.units.*;
import ch.zuestengineering.emod.simulation.DynamicState;

/**
 * General Moving Mass Class. TODO
 * 
 * Assumptions: TODO
 * 
 * Inputlist: 1: Speed : [mm/min] : Required speed
 * 
 * Outputlist: 1: Force : [N] : Resulting force
 * 
 * Config parameters: none
 * 
 * @author simon
 * 
 */
@XmlRootElement
public class MovingMass extends APhysicalComponent {

	@XmlElement
	protected double mass;
	@XmlElement
	protected double inertia;
	@XmlElement
	protected double angle;
	@XmlElement
	protected double lever;

	// Input parameters:
	private IOContainer speedLin, speedRot;
	// Output parameters:
	private IOContainer force, torque;

	// Save last input values
	private double lastspeedLin = 0, lastspeedRot = 0;

	private DynamicState positionLin, positionAng;

	/**
	 * Constructor called from XmlUnmarshaller. Attribute 'type' is set by
	 * XmlUnmarshaller.
	 */
	public MovingMass() {
		super();
	}

	/**
	 * post xml init method (loading physics data)
	 * 
	 * @param u
	 * @param parent
	 */
	public void afterUnmarshal(final Unmarshaller u, final Object parent) {
		init();
	}

	/**
	 * Linear Motor constructor
	 * 
	 * @param mass
	 * @param inertia
	 * @param angle
	 * @param lever
	 */
	public MovingMass(double mass, double inertia, double angle, double lever) {
		super();

		this.angle = angle;
		this.mass = mass;
		this.inertia = inertia;
		this.lever = lever;
		init();
	}

	/**
	 * Called from constructor or after unmarshaller.
	 */
	private void init() {
		/* Define Input parameters */
		inputs = new ArrayList<IOContainer>();
		speedLin = new IOContainer("SpeedLin", new SiUnit(Unit.M_S), 0, ContainerType.MECHANIC);
		speedRot = new IOContainer("SpeedRot", new SiUnit(Unit.REVOLUTIONS_S), 0, ContainerType.MECHANIC);
		inputs.add(speedLin);
		inputs.add(speedRot);

		/* Define output parameters */
		outputs = new ArrayList<IOContainer>();
		force = new IOContainer("Force", new SiUnit(Unit.NEWTON), 0, ContainerType.MECHANIC);
		torque = new IOContainer("Torque", new SiUnit(Unit.NEWTONMETER), 0, ContainerType.MECHANIC);
		outputs.add(force);
		outputs.add(torque);

		/* State */
		positionLin = new DynamicState("Position", new SiUnit(Unit.M), 0);
		positionAng = new DynamicState("Angle", new SiUnit(""), 0);

		dynamicStates = new ArrayList<DynamicState>();
		dynamicStates.add(positionLin);
		dynamicStates.add(positionAng);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.ethz.inspire.emod.model.APhysicalComponent#update()
	 */
	@Override
	public void update() {

		// Get required speed in m/s
		double curspeedLin = speedLin.getValue(), curspeedRot = speedRot.getValue();

		positionLin.addValue((curspeedLin + lastspeedLin) / 2 * timestep);
		positionAng.addValue((curspeedRot + lastspeedRot) / 2 * timestep);

		/*
		 * Force is calculated by F = ( Acceleration + sin(Angle)*g ) * Mass where the
		 * Acceleration is estimated by the velocity change: Acceleration =
		 * (v(t)-v(t-Ts))/Ts
		 */
		force.setValue(((curspeedLin - lastspeedLin) / timestep + Math.cos(angle * Math.PI / 180) * 9.81) * mass);
		torque.setValue((curspeedRot - lastspeedRot) / timestep * inertia
				+ Math.sin(positionAng.getValue() * 2 * Math.PI) * lever * mass * 9.81);

		// Update last speed
		lastspeedLin = curspeedLin;
		lastspeedRot = curspeedRot;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.ethz.inspire.emod.model.APhysicalComponent#getType()
	 */
	@Override
	public String getType() {
		return "m=" + mass + "-alpha=" + angle;
	}

	@Override
	public void setType(String type) {
		// TODO
	}

	@Override
	public void updateBoundaryConditions() {/* No BC */
	}

	@Override
	public ParameterCheckReport checkConfigParams() {
		ParameterCheckReport report = new ParameterCheckReport();
		// Check model parameters:
		// Check dimensions:
		if (mass < 0) {
			report.add("Mass", "Negative value: Mass must be non negative");
		}

		if (lever < 0) {
			report.add("Lever", "Negative value: Lever must be non negative");
		}
		// Check dimensions:
		if (inertia <= 0) {
			report.add("Inertia", "Negative value: Inertia must be non negative");
		}
		return report;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.zuestengineering.emod.model.APhysicalComponent#updateParameterSet()
	 */
	@Override
	protected void updateParameterSet() {
		// TODO Auto-generated method stub

	}

}
