/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.model.pm;

import java.util.ArrayList;

import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import ch.zuestengineering.emod.femexport.BoundaryCondition;
import ch.zuestengineering.emod.femexport.BoundaryConditionType;
import ch.zuestengineering.emod.model.ParameterCheckReport;
import ch.zuestengineering.emod.model.linking.IOContainer;
import ch.zuestengineering.emod.model.parameters.ParameterSet;
import ch.zuestengineering.emod.model.parameters.PhysicalValue;
import ch.zuestengineering.emod.model.units.ContainerType;
import ch.zuestengineering.emod.model.units.SiUnit;
import ch.zuestengineering.emod.model.units.Unit;
import ch.zuestengineering.emod.utils.ComponentConfigReader;

/**
 * General linear DC motor model class
 * 
 * @author Simon Z�st
 *
 */
@XmlRootElement
public class MotorDCLinear extends ALinearMotor {
	@XmlElement
	protected String type;

	// Save last input values
	private double lastspeed;
	private double lastforce;

	// Parameters used by the model.
	private double frictionForce;
	private double kappa_a;
	private double kappa_i;
	private double armatureResistance;
	private int p;

	/**
	 * Constructor called from XmlUnmarshaller. Attribute 'type' is set by
	 * XmlUnmarshaller.
	 */
	public MotorDCLinear() {
		super();
	}

	/**
	 * @param u
	 * @param parent
	 */
	public void afterUnmarshal(Unmarshaller u, Object parent) {
		// post xml init method (loading physics data)
		init();
	}

	/**
	 * Servo Motor constructor
	 * 
	 * @param type
	 */
	public MotorDCLinear(String type) {
		super();

		this.type = type;
		init();
	}

	/**
	 * Called from constructor or after unmarshaller.
	 */
	@Override
	protected void init() {

		/* Define Input parameters */
		inputs = new ArrayList<IOContainer>();
		speed = new IOContainer("Speed", new SiUnit(Unit.M_S), 0, ContainerType.MECHANIC);
		force = new IOContainer("Force", new SiUnit(Unit.NEWTON), 0, ContainerType.MECHANIC);
		// inputs.add(brake);
		inputs.add(speed);
		inputs.add(force);

		/* Define output parameters */
		outputs = new ArrayList<IOContainer>();
		pel = new IOContainer("PTotal", new SiUnit(Unit.WATT), 0, ContainerType.ELECTRIC);
		ploss = new IOContainer("PLoss", new SiUnit(Unit.WATT), 0, ContainerType.THERMAL);
		pmech = new IOContainer("PUse", new SiUnit(Unit.WATT), 0, ContainerType.MECHANIC);
		efficiency = new IOContainer("Efficiency", new SiUnit(Unit.NONE), 0, ContainerType.INFORMATION);
		outputs.add(pel);
		outputs.add(ploss);
		outputs.add(pmech);
		outputs.add(efficiency);

		/* Read configuration parameters: */
		loadParameters();

		/* Boundary conditions */
		boundaryConditions = new ArrayList<BoundaryCondition>();
		bcHeatSrcStator = new BoundaryCondition("HeatSrcStator", new SiUnit("W"), 0, BoundaryConditionType.NEUMANN);
		bcHeatSrcRotor = new BoundaryCondition("HeatSrcRotor", new SiUnit("W"), 0, BoundaryConditionType.NEUMANN);
		boundaryConditions.add(bcHeatSrcStator);
		boundaryConditions.add(bcHeatSrcRotor);
	}

	/**
	 * Returns the desired IOContainer
	 * 
	 * If the desired input name matches BrakeOn, the input added to the set of
	 * avaiable inputs
	 * 
	 * @param name Name of the desired input
	 * @return temp IOContainer matched the desired name
	 * 
	 * @author simon
	 */
	@Override
	public IOContainer getInput(String name) {
		IOContainer temp = null;

		/*
		 * If the initialization has not been done, create a output with same unit as
		 * input
		 */
		for (IOContainer ioc : inputs) {
			if (ioc.getName().equals(name)) {
				temp = ioc;
				break;
			}
		}

		return temp;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.ethz.inspire.emod.model.APhysicalComponent#update()
	 */
	@Override
	public void update() {

		lastforce = force.getValue();
		lastspeed = speed.getValue();

		/*
		 * Check if component is running. If not, set all to 0 and exit.
		 */
		if (((lastforce == 0) && (lastspeed == 0))) {
			pel.setValue(0);
			ploss.setValue(0);
			pmech.setValue(0);
			efficiency.setValue(0);
			return;
		}

		/*
		 * The electrical power is equal to the motor power pel = (T_m [Nm] + T_f
		 * [Nm])/kappa_a [Nm/A] * (kappa_i [Vs/rad] * omega [rpm] + (T_m [Nm] + T_f
		 * [Nm])/kappa_a [Nm/A] * R_a [Ohm]) + P_brake [W]
		 */
		pel.setValue(p * (lastforce + frictionForce * Math.signum(lastspeed)) / kappa_a * (kappa_i * lastspeed
				+ (lastforce + frictionForce * Math.signum(lastspeed)) * armatureResistance / kappa_a));

		/*
		 * The heat loss is equal to the power by the resistor power plus the amplifier
		 * loss pel = ((T_m [Nm] + T_f [Nm])/kappa_a [Nm/A] )^2 * R_a [Ohm] +P_th_amp
		 * [W]
		 */
		ploss.setValue(p * Math.pow(((lastforce + frictionForce * Math.abs(Math.signum(lastspeed))) / kappa_a), 2)
				* armatureResistance);

		/*
		 * The mechanical power is given by the rotational speed and the torque: pmech =
		 * T_m [Nm] * omega [rpm] * pi/30 [rad/rpm]
		 */
		pmech.setValue(lastforce * lastspeed);

		if (pel.getValue() != 0)
			efficiency.setValue(pmech.getValue() / pel.getValue());
		else
			efficiency.setValue(0);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.ethz.inspire.emod.model.APhysicalComponent#getType()
	 */
	@Override
	public String getType() {
		return type;
	}

	@Override
	public void setType(String type) {
		this.type = type;
	}

	@Override
	public void updateBoundaryConditions() {
		bcHeatSrcRotor.setValue(0);
		bcHeatSrcStator.setValue(ploss.getValue());
	}

	@Override
	public ParameterCheckReport checkConfigParams() {
		ParameterCheckReport report = new ParameterCheckReport();

		// Check model parameters:
		// Check NonNegativ:
		if (frictionForce < 0) {
			report.add("StaticFriction", "Negative Value: StaticFriction must be non negative");
		}
		if (kappa_i < 0) {
			report.add("KappaI", "Negative Value: KappaI must be non negative");
		}
		if (armatureResistance < 0) {
			report.add("ResistanceI", "Negative Value: ResistanceI must be non negative");
		}
		// Strictly positive values
		if (kappa_a <= 0) {
			report.add("KappaA", "Non-positive Value: KappaA must be non negative and non zero");
		}
		if (p <= 0) {
			report.add("PolePairs", "Non-positive Value: PolePairs must be non negative and non zero");
		}

		return report;
	}

	@Override
	public void loadParameters() {
		super.loadParameters();

		frictionForce = parameterSet.getPhysicalValue("StaticFriction").getValue();
		kappa_a = parameterSet.getPhysicalValue("KappaA").getValue();
		kappa_i = parameterSet.getPhysicalValue("KappaI").getValue();
		armatureResistance = parameterSet.getPhysicalValue("ArmatureResistance").getValue();
		p = parameterSet.getInteger("PolePairs");

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.zuestengineering.emod.model.APhysicalComponent#updateParameterSet()
	 */
	@Override
	protected void updateParameterSet() {
		ComponentConfigReader params = null;
		/* Open file containing the parameters of the model type: */
		try {
			params = new ComponentConfigReader(getModelType(), type);
		} catch (Exception e) {
			e.printStackTrace();
		}

		/* Read the config parameter: */
		try {
			frictionForce = params.getValue("StaticFriction", new PhysicalValue(0.0, "N")).getValue();
			kappa_a = params.getValue("KappaA", new PhysicalValue(1.0, "N A^-1")).getValue();
			kappa_i = params.getValue("KappaI", new PhysicalValue(1.0, "V s m^-1")).getValue();
			armatureResistance = params.getValue("ArmatureResistance", new PhysicalValue(1.0, "Ohm")).getValue();
			p = params.getValue("PolePairs", 1);

			// Old Parameters
			params.saveValues();

		} catch (Exception e) {
			e.printStackTrace();
		}
		params.Close(); /* Model configuration file not needed anymore. */

		parameterSet = new ParameterSet(this.toString());
		parameterSet.setPhysicalValue("StaticFriction", frictionForce, new SiUnit("N"));
		parameterSet.setPhysicalValue("KappaA", kappa_a, new SiUnit("N A^-1"));
		parameterSet.setPhysicalValue("KappaI", kappa_i, new SiUnit("V s m^-1"));
		parameterSet.setPhysicalValue("ArmatureResistance", armatureResistance, new SiUnit("Ohm"));
		parameterSet.setInteger("PolePairs", p);
		parameterSet.save(getParameterFile());
		logger.info("Parameter file successfully converted to new format");

	}
}
