/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.model.pm;

import java.util.ArrayList;

import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import ch.zuestengineering.emod.model.APhysicalComponent;
import ch.zuestengineering.emod.model.ParameterCheckReport;
import ch.zuestengineering.emod.model.fluid.FECValve;
import ch.zuestengineering.emod.model.fluid.Floodable;
import ch.zuestengineering.emod.model.fluid.FluidCircuitProperties;
import ch.zuestengineering.emod.model.linking.FluidContainer;
import ch.zuestengineering.emod.model.linking.IOContainer;
import ch.zuestengineering.emod.model.parameters.ParameterSet;
import ch.zuestengineering.emod.model.units.ContainerType;
import ch.zuestengineering.emod.model.units.SiUnit;
import ch.zuestengineering.emod.model.units.Unit;
import ch.zuestengineering.emod.utils.ComponentConfigReader;

/**
 * General Valve model class. Implements the physical model of a valve. Can be
 * used for check valves, (magnetic) way valves, pressure reducing valves.
 * 
 * Assumptions: -2 States (idle, open) -No leakage
 * 
 * 
 * Inputlist: 1: ValveCtrl : [-] ON/OFF position of the valve. 1 means ON, 0
 * means OFF. Only needed for magnetic valves 2: FluidIn : [-]
 * 
 * Outputlist: 1: PLoss : [W] : Power loss 2: PTotal : [W] : Needed electrical
 * power to open and hold the valve 3: FluidOut : [-]
 * 
 * Config parameters: ElectricPower : [W] PressureLossCoefficient : [-] Area :
 * [m2]
 * 
 * @author kraandre
 * 
 */
@XmlRootElement
public class Valve extends APhysicalComponent implements Floodable {

	@XmlElement
	protected String type;

	// Input parameters:
	private IOContainer valveCtrl;
	private FluidContainer fluidIn;

	// Output parameters:
	private IOContainer ploss;
	private IOContainer ptotal;
	private IOContainer puse;
	private FluidContainer fluidOut;

	// Parameters used by the model.
	private double electricPower;
	private double zeta;
	private double area;

	// Fluid properties
	private FluidCircuitProperties fluidProperties;

	/**
	 * Constructor called from XmlUnmarshaller. Attribute 'type' is set by
	 * XmlUnmarshaller.
	 */
	public Valve() {
		super();
	}

	/**
	 * @param u
	 * @param parent
	 */
	public void afterUnmarshal(Unmarshaller u, Object parent) {
		// post xml init method (loading physics data)
		init();
	}

	/**
	 * Valve constructor
	 * 
	 * @param type
	 */
	public Valve(String type) {
		super();

		this.type = type;
		init();
	}

	/**
	 * Called from constructor or after unmarshaller.
	 */
	private void init() {

		/* Define Input parameters */
		inputs = new ArrayList<IOContainer>();
		valveCtrl = new IOContainer("ValveCtrl", new SiUnit(Unit.NONE), 0, ContainerType.CONTROL);
		inputs.add(valveCtrl);

		/* Fluid Properties */
		fluidProperties = new FluidCircuitProperties(new FECValve(this));

		/* Define output parameters */
		outputs = new ArrayList<IOContainer>();
		ploss = new IOContainer("PLoss", new SiUnit(Unit.WATT), 0, ContainerType.THERMAL);
		ptotal = new IOContainer("PTotal", new SiUnit(Unit.WATT), 0, ContainerType.ELECTRIC);
		puse = new IOContainer("PUse", new SiUnit(Unit.WATT), 0, ContainerType.FLUIDDYNAMIC);
		outputs.add(ptotal);
		outputs.add(puse);
		outputs.add(ploss);

		/* Fluid in- and output */
		fluidIn = new FluidContainer("FluidIn", new SiUnit(Unit.NONE), ContainerType.FLUIDDYNAMIC, fluidProperties);
		fluidOut = new FluidContainer("FluidOut", new SiUnit(Unit.NONE), ContainerType.FLUIDDYNAMIC, fluidProperties);
		inputs.add(fluidIn);
		outputs.add(fluidOut);

		/* Read configuration parameters: */
		loadParameters();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.ethz.inspire.emod.model.APhysicalComponent#update()
	 */
	@Override
	public void update() {
		
		if(electricPower == 0) {
			puse.setValue(0);
			ptotal.setValue(0);
			ploss.setValue(0);
			return;
		}

		if (valveCtrl.getValue() > 0) {
			puse.setValue(
					fluidProperties.getMaterial().getDensity(fluidIn.getTemperature(), fluidProperties.getPressure())
							/ Math.pow(valveCtrl.getValue() * area, 2) / 2
							* Math.pow(fluidProperties.getFlowRate(), 3));
		} else {
			puse.setValue(0);
		}

		ptotal.setValue(electricPower + fluidProperties.getFlowRate() * fluidProperties.getPressureDrop()
				+ fluidProperties.getMaterial().getDensity(fluidIn.getTemperature(), fluidProperties.getPressure())
						/ Math.pow(area, 2) / 2 * Math.pow(fluidProperties.getFlowRate(), 3));

		ploss.setValue(ptotal.getValue() - puse.getValue());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.ethz.inspire.emod.model.APhysicalComponent#getType()
	 */
	@Override
	public String getType() {
		return type;
	}

	@Override
	public void setType(String type) {
		this.type = type;
	}

	@Override
	public ArrayList<FluidCircuitProperties> getFluidPropertiesList() {
		ArrayList<FluidCircuitProperties> out = new ArrayList<FluidCircuitProperties>();
		out.add(fluidProperties);
		return out;
	}

	/**
	 * Returns the pressure loss at the given flow rate
	 * 
	 * @param flowRate
	 * @return
	 */
	public double getPressure(double flowRate) {
		return Math.pow(flowRate, 2) * getPressureLossCoefficient() * Math.signum(flowRate);
	}

	/**
	 * Returns the derivative of the pressure mpa at the given flow rate
	 * 
	 * @return
	 */
	public double getPressureLossCoefficient() {
		double rho = fluidProperties.getMaterial().getDensity(fluidProperties.getTemperatureIn(),
				fluidProperties.getPressure()), u = Math.min(1, Math.max(1E-3, valveCtrl.getValue()));

		return zeta * rho / 2 / Math.pow(u * area, 2);
	}

	/**
	 * Returns whether the valve is closed or not
	 * 
	 * @return
	 */
	public boolean isClosed() {
		if (valveCtrl.getValue() == 0)
			return true;
		else
			return false;
	}

	@Override
	public void flood() {/* Not used */
	}

	@Override
	public void updateBoundaryConditions() {/* Not used */
	}

	@Override
	public ParameterCheckReport checkConfigParams() {
		ParameterCheckReport report = new ParameterCheckReport();
		if (zeta <= 0)
			report.add("PressureLossCoefficient", "Pressure loss coefficient must be greater than zero");

		if (area <= 0)
			report.add("Area", "Area must be greater than zero");

		if (electricPower < 0)
			report.add("ElectricPower", "Electric power must be geq zero");

		return report;
	}

	@Override
	public void loadParameters() {
		super.loadParameters();

		electricPower = parameterSet.getPhysicalValue("ElectricPower").getValue();
		zeta = parameterSet.getPhysicalValue("PressureLossCoefficient").getValue();
		area = parameterSet.getPhysicalValue("Area").getValue();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.zuestengineering.emod.model.APhysicalComponent#updateParameterSet()
	 */
	@Override
	protected void updateParameterSet() {
		logger.warning(
				"Parameter set is empty; tying for old parameter set format (this option will be removed in later versions) ...");

		ComponentConfigReader params = null;
		/* Open file containing the parameters of the model type: */
		try {
			params = new ComponentConfigReader(getModelType(), type);
		} catch (Exception e) {
			e.printStackTrace();
		}

		/* Read the config parameter: */
		try {
			electricPower = params.getPhysicalValue("ElectricPower", new SiUnit("W")).getValue();
			zeta = params.getPhysicalValue("PressureLossCoefficient", new SiUnit()).getValue();
			area = params.getPhysicalValue("Area", new SiUnit("m^2")).getValue();
		} catch (Exception e) {
			e.printStackTrace();
		}
		params.Close(); /* Model configuration file not needed anymore. */

		parameterSet = new ParameterSet(this.toString());
		parameterSet.setPhysicalValue("ElectricPower", electricPower, new SiUnit("W"));
		parameterSet.setPhysicalValue("PressureLossCoefficient", zeta, new SiUnit(""));
		parameterSet.setPhysicalValue("Area", area, new SiUnit("m^2"));
		parameterSet.save(getParameterFile());
		logger.info("Parameter file successfully converted to new format");

	}

}
