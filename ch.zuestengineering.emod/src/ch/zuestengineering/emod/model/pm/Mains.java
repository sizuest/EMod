/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.model.pm;

import java.util.ArrayList;

import javax.xml.bind.Unmarshaller;

import ch.zuestengineering.emod.model.APhysicalComponent;
import ch.zuestengineering.emod.model.ParameterCheckReport;
import ch.zuestengineering.emod.model.linking.IOContainer;
import ch.zuestengineering.emod.model.units.ContainerType;
import ch.zuestengineering.emod.model.units.SiUnit;

/**
 * @author sizuest
 *
 */
public class Mains extends APhysicalComponent {
	protected String type = "Generic";

	// Input Lists
	private ArrayList<IOContainer> pDmds;
	private IOContainer newPDmd;

	// Output parameters:
	private IOContainer pTotal;

	// Sum
	private double tmpSum;

	/**
	 * Constructor called from XmlUnmarshaller. Attribute 'type' is set by
	 * XmlUnmarshaller.
	 */
	public Mains() {
		super();
	}
	
	/**
	 * Linear Motor constructor
	 * 
	 * @param type
	 */
	public Mains(String type) {
		super();

		this.type = type;
		init();
	}

	/**
	 * post xml init method (loading physics data)
	 * 
	 * @param u
	 * @param parent
	 */
	public void afterUnmarshal(Unmarshaller u, Object parent) {
		init();
	}

	/**
	 * Called from constructor or after unmarshaller.
	 */
	private void init() {
		/* Define Input parameters */
		inputs = new ArrayList<IOContainer>();
		pDmds = new ArrayList<IOContainer>();

		newPDmd = new IOContainer("PDmd", new SiUnit("W"), 0, ContainerType.ELECTRIC);
		inputs.add(newPDmd);

		/* Define output parameters */
		outputs = new ArrayList<IOContainer>();
		pTotal = new IOContainer("PTotal", new SiUnit("W"), 0, ContainerType.ELECTRIC);
		outputs.add(pTotal);

		// Validate the parameters:
		try {
			checkConfigParams();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Returns the desired IOContainer
	 * 
	 * If the desired input name matches PDmds, a new input is created and added to
	 * the set of available inputs
	 * 
	 * @param name Name of the desired input
	 * @return temp IOContainer matched the desired name
	 * 
	 * @author simon
	 */
	@Override
	public IOContainer getInput(String name) {
		IOContainer temp = null;

		/*
		 * If the initialization has not been done, create a output with same unit as
		 * input
		 */
		if (name.matches(newPDmd.getName())) {
			temp = new IOContainer(newPDmd.getName() + (pDmds.size() + 1), newPDmd);
			inputs.add(temp);
			pDmds.add(temp);
		} else {
			for (IOContainer ioc : inputs) {
				if (ioc.getName().equals(name)) {
					temp = ioc;
					break;
				}
			}
		}

		return temp;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.zuestengineering.emod.model.APhysicalComponent#getType()
	 */
	@Override
	public String getType() {
		return type;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.zuestengineering.emod.model.APhysicalComponent#update()
	 */
	@Override
	public void update() {
		tmpSum = 0;

		for (IOContainer in : pDmds)
			tmpSum += in.getValue();

		pTotal.setValue(tmpSum);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ch.zuestengineering.emod.model.APhysicalComponent#updateBoundaryConditions()
	 */
	@Override
	public void updateBoundaryConditions() {
		// Not used
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ch.zuestengineering.emod.model.APhysicalComponent#setType(java.lang.String)
	 */
	@Override
	public void setType(String type) {
		this.type = type;
		init();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.zuestengineering.emod.model.APhysicalComponent#checkConfigParams()
	 */
	@Override
	public ParameterCheckReport checkConfigParams() {
		return new ParameterCheckReport();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.zuestengineering.emod.model.APhysicalComponent#updateParameterSet()
	 */
	@Override
	protected void updateParameterSet() {
		// Not used
	}

}
