/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.model.pm;

import java.util.ArrayList;

import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import ch.zuestengineering.emod.dd.Duct;
import ch.zuestengineering.emod.dd.model.DuctPipe;
import ch.zuestengineering.emod.femexport.BoundaryCondition;
import ch.zuestengineering.emod.femexport.BoundaryConditionType;
import ch.zuestengineering.emod.model.APhysicalComponent;
import ch.zuestengineering.emod.model.ParameterCheckReport;
import ch.zuestengineering.emod.model.fluid.FECDuct;
import ch.zuestengineering.emod.model.fluid.Floodable;
import ch.zuestengineering.emod.model.fluid.Fluid;
import ch.zuestengineering.emod.model.fluid.FluidCircuitProperties;
import ch.zuestengineering.emod.model.linking.FluidContainer;
import ch.zuestengineering.emod.model.linking.IOContainer;
import ch.zuestengineering.emod.model.material.Material;
import ch.zuestengineering.emod.model.parameters.ParameterSet;
import ch.zuestengineering.emod.model.thermal.ThermalArray;
import ch.zuestengineering.emod.model.units.*;
import ch.zuestengineering.emod.simulation.DynamicState;
import ch.zuestengineering.emod.utils.ComponentConfigReader;

/**
 * General Pipe model class. Implements the physical model of a hydraulic pipe
 * 
 * Assumptions: -No leakage -Separation between laminar and turbulent flow
 * -Smooth surface -Pipe wall is rigid
 * 
 * Inputlist: 1: PressureOut : [Pa] : Pressure at the end 2: MassflowOut :
 * [kg/s] : Mass flow in the pipe 3: TemperatureIn : [K] : Inlet temperature 4:
 * TemperatureAmb: [K] : Ambient temperature Outputlist: 1: PressureIn : [Pa] :
 * Pressure in the cylinder chamber 2: MassFlowIn : [kg/s] : Mass flow into the
 * cylinder chamber 3: TemperatureOut: [K] : Outlet temperature 4: PLoss : [W] :
 * Power loss 5: PressureLoss : [Pa] : Pressure difference over the pipe
 * 
 * Config parameters: PipeDiameter : [m] PipeLength : [m] PipeThickness : [m]
 * PipeMaterial : [Material];
 * 
 * 
 * @author kraandre, sizuest
 * 
 */
@XmlRootElement
public class Pipe extends APhysicalComponent implements Floodable {
	// public class Pipe<T> extends APhysicalComponent<T>{

	@XmlElement
	protected String type;

	// Input parameters:
	private IOContainer temperatureAmb;
	private FluidContainer fluidIn;

	// Output parameters:
	private IOContainer ploss;
	private IOContainer pressureloss;
	private IOContainer temperaturePipe;
	private FluidContainer fluidOut;

	// Boundary Conditions
	private BoundaryCondition bcTemperature;
	private BoundaryCondition bcHTC;

	// Fluid Properties
	FluidCircuitProperties fluidProperties;

	// Parameters used by the model.
	private double pipeDiameter;
	private double pipeLength;
	private double pipeThickness;
	private double pipeRoughness;
	private Material pipeMaterial;
	private double volume;
	double pipeArea, pipeThTransmittance;
	private ThermalArray fluid;
	private Duct duct;

	// Global values
	private double thermalResistance;

	// Initial temperature
	double temperatureInit = 293;
	
	// Air properties
	protected Material materialAir = new Material("Air");

	/**
	 * Constructor called from XmlUnmarshaller. Attribute 'type' is set by
	 * XmlUnmarshaller.
	 */
	public Pipe() {
		super();

		this.type = "Example";
		this.temperatureInit = 293;
		init();
		this.fluidProperties.setMaterial(new Material("Monoethylenglykol_34"));
	}

	/**
	 * @param u
	 * @param parent
	 * @throws Exception
	 */
	public void afterUnmarshal(final Unmarshaller u, final Object parent) throws Exception {
		// post xml init method (loading physics data)
		init();

	}

	/**
	 * Pipe constructor
	 * 
	 * @param type
	 */
	public Pipe(String type) {
		super();

		this.type = type;
		init();
	}

	/**
	 * Pipe constructor
	 * 
	 * @param type
	 * @param temperatureInit
	 * @param fluidType
	 */
	public Pipe(String type, double temperatureInit, String fluidType) {
		super();

		this.type = type;
		this.temperatureInit = temperatureInit;
		init();
		this.fluidProperties.setMaterial(new Material(fluidType));
		this.fluid.getTemperature().setInitialCondition(temperatureInit);
		this.fluid.getTemperature().setInitialCondition();
	}

	/**
	 * Pipe constructor
	 * 
	 * @param type
	 * @param temperatureInit
	 * @param fluid
	 * @throws Exception
	 */
	/*
	 * public Pipe(String type, double temperatureInit, String materialName, double
	 * volume, int numElements) { super();
	 * 
	 * this.type = type; this.temperatureInit = temperatureInit; this.fluid = new
	 * ThermalArray(materialName, volume, numElements); init(); }
	 */

	/**
	 * Called from constructor or after unmarshaller.
	 * 
	 * @throws Exception
	 */
	private void init() {
		/* Define Input parameters */
		inputs = new ArrayList<IOContainer>();
		temperatureAmb = new IOContainer("TemperatureAmb", new SiUnit(Unit.KELVIN), temperatureInit,
				ContainerType.THERMAL);
		inputs.add(temperatureAmb);

		/* Define output parameters */
		outputs = new ArrayList<IOContainer>();
		ploss = new IOContainer("PLoss", new SiUnit(Unit.WATT), 0.00, ContainerType.THERMAL);
		pressureloss = new IOContainer("PressureLoss", new SiUnit(Unit.PA), 0.00, ContainerType.FLUIDDYNAMIC);
		temperaturePipe = new IOContainer("Temperature", new SiUnit(Unit.KELVIN), temperatureInit,
				ContainerType.THERMAL);
		outputs.add(ploss);
		outputs.add(pressureloss);
		outputs.add(temperaturePipe);

		/* Boundary conditions */
		boundaryConditions = new ArrayList<BoundaryCondition>();
		bcTemperature = new BoundaryCondition("Temperature", new SiUnit("K"), 293.15, BoundaryConditionType.ROBIN);
		bcHTC = new BoundaryCondition("HTC", new SiUnit("W/K"), 0, BoundaryConditionType.ROBIN);

		fluid = new ThermalArray("Example", 1, 10);
		duct = new Duct();

		/* Read configuration parameters: */
		loadParameters();

		/* Fluid circuit parameters */
		fluidProperties = new FluidCircuitProperties(new FECDuct(duct, fluid.getTemperature()),
				fluid.getTemperatureOut());
		fluidProperties.setMaterial(fluid.getMaterial());
		duct.setMaterial(fluidProperties.getMaterial());

		/* add fluid In/Output */
		fluidIn = new FluidContainer("FluidIn", new SiUnit(Unit.NONE), ContainerType.FLUIDDYNAMIC, fluidProperties);
		inputs.add(fluidIn);
		fluidOut = new FluidContainer("FluidOut", new SiUnit(Unit.NONE), ContainerType.FLUIDDYNAMIC, fluidProperties);
		outputs.add(fluidOut);

		dynamicStates = new ArrayList<DynamicState>();
		dynamicStates.add(fluid.getTemperature());

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.ethz.inspire.emod.model.APhysicalComponent#update()
	 */
	@Override
	public void update() {
		duct.setMaterial(fluidProperties.getMaterial());

		/* Local variables */
		double alphaFluid, alphaAir;

		/* Pressure loss */
		pressureloss.setValue(fluidProperties.getPressureDrop());

		/* Set fluid obj. boundary positions */
		fluid.setTemperatureIn(fluidIn.getTemperature());
		fluid.setFlowRate(fluidProperties);

		/* Calculate alphaFluid */
		if (fluid.getFlowRate() > 0)
			alphaFluid = Fluid.convectionForcedPipe(fluid.getMaterial(), fluid.getTemperature().getValue(),
					fluidProperties.getPressure(), pipeLength, pipeDiameter, fluid.getFlowRate());
		else
			alphaFluid = Fluid.convectionFreeCylinderHorz(fluid.getMaterial(), fluid.getTemperature().getValue(),
					temperatureAmb.getValue(), fluidProperties.getPressure(), pipeDiameter);

		/* Calculate alphaAir */
		alphaAir = Fluid.convectionFreeCylinderHorz(materialAir, fluid.getTemperature().getValue(),
				temperatureAmb.getValue(), 1E5, pipeDiameter);

		/* Calculate overall thermal Resistance of pipe */
		// double thermalResistance = 1/(alphaFluid * pipeArea) + 0.03 / (0.25 *
		// pipeArea) + 1/(alphaAir * pipeArea);
		thermalResistance = 1 / (alphaFluid * pipeArea) + 1 / (pipeThTransmittance * pipeArea)
				+ 1 / (alphaAir * pipeArea);
		if (Double.isNaN(thermalResistance) | Double.isInfinite(thermalResistance))
			thermalResistance = 0;
		else
			thermalResistance = 1 / thermalResistance;

		// set array boundary conditions
		fluid.setThermalResistance(thermalResistance);
		fluid.setHeatSource(pressureloss.getValue() * fluid.getFlowRate());
		fluid.setTemperatureAmb(temperatureAmb.getValue());

		/* Integration step: */
		fluid.integrate(timestep, 0, 0, 100000);
		// TODO: Pressure

		ploss.setValue(fluid.getHeatLoss());
		temperaturePipe.setValue(fluid.getTemperature().getValue());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.ethz.inspire.emod.model.APhysicalComponent#getType()
	 */
	@Override
	public String getType() {
		return type;
	}

	@Override
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * Fluid Type
	 * 
	 * @return {@link Material}
	 */
	public String getFluidType() {
		return fluid.getMaterial().getType();
	}

	@Override
	public ArrayList<FluidCircuitProperties> getFluidPropertiesList() {
		ArrayList<FluidCircuitProperties> out = new ArrayList<FluidCircuitProperties>();
		out.add(fluidProperties);
		return out;
	}

	@Override
	public void flood() {/* Not used */
	}

	@Override
	public void updateBoundaryConditions() {
		bcTemperature.setValue(fluid.getTemperature().getValue());
		bcHTC.setValue(thermalResistance);
	}

	@Override
	public ParameterCheckReport checkConfigParams() {
		ParameterCheckReport report = new ParameterCheckReport();
		if (0 > pipeDiameter) {
			report.add("pipeDiameter", "Non physical value: Variable 'pipeDiameter' must be bigger than zero!");
		}
		if (0 > pipeLength) {
			report.add("pistonLength", "Non physical value: Variable 'PipeLength' must be bigger than zero!");
		}
		if (0 > pipeThickness) {
			report.add("pistonLength", "Non physical value: Variable 'PipeThickness' must be bigger than zero!");
		}
		if (0 > pipeRoughness) {
			report.add("pistonLength", "Non physical value: Variable 'PipeRoughness' must be bigger than zero!");
		}

		return report;
	}

	@Override
	public void loadParameters() {
		super.loadParameters();

		pipeDiameter = parameterSet.getPhysicalValue("PipeDiameter").getValue();
		pipeLength = parameterSet.getPhysicalValue("PipeLength").getValue();
		pipeThickness = parameterSet.getPhysicalValue("PipeThickness").getValue();
		pipeRoughness = parameterSet.getPhysicalValue("PipeRoughness").getValue();
		pipeMaterial = parameterSet.getMaterial("PipeMaterial");

		// Calculate constant parameters
		pipeArea = 2 * Math.PI * pipeDiameter / 2 * pipeLength;
		pipeThTransmittance = pipeMaterial.getThermalConductivity(293.15, 1E5) / pipeThickness;

		volume = Math.pow(pipeDiameter / 2, 2) * Math.PI * pipeLength;
		fluid.setVolume(volume);
		duct.clear();
		duct.addElement(new DuctPipe(pipeLength, pipeDiameter, pipeRoughness, 1));

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.zuestengineering.emod.model.APhysicalComponent#updateParameterSet()
	 */
	@Override
	protected void updateParameterSet() {
		logger.warning(
				"Parameter set is empty; tying for old parameter set format (this option will be removed in later versions) ...");

		ComponentConfigReader params = null;
		/* Open file containing the parameters of the model type: */
		try {
			params = new ComponentConfigReader(getModelType(), type);
		} catch (Exception e) {
			e.printStackTrace();
		}

		/* Read the config parameter: */
		try {
			pipeDiameter = params.getPhysicalValue("PipeDiameter", new SiUnit("m")).getValue();
			pipeLength = params.getPhysicalValue("PipeLength", new SiUnit("m")).getValue();
			pipeThickness = params.getPhysicalValue("PipeThickness", new SiUnit("m")).getValue();
			pipeRoughness = params.getPhysicalValue("PipeRoughness", new SiUnit("m")).getValue();
			pipeMaterial = params.getMaterial("PipeMaterial");

			// Calculate constant parameters
			pipeArea = 2 * Math.PI * pipeDiameter / 2 * pipeLength;
			pipeThTransmittance = pipeMaterial.getThermalConductivity(293.15, 1E5) / pipeThickness;
		} catch (Exception e) {
			e.printStackTrace();
		}
		params.Close(); /* Model configuration file not needed anymore. */

		parameterSet = new ParameterSet(this.toString());
		parameterSet.setPhysicalValue("PipeDiameter", pipeDiameter, new SiUnit("m"));
		parameterSet.setPhysicalValue("PipeLength", pipeLength, new SiUnit("m"));
		parameterSet.setPhysicalValue("PipeThickness", pipeThickness, new SiUnit("m"));
		parameterSet.setPhysicalValue("PipeRoughness", pipeRoughness, new SiUnit("m"));
		parameterSet.setMaterial("PipeMaterial", pipeMaterial);
		parameterSet.save(getParameterFile());
		logger.info("Parameter file successfully converted to new format");

	}
}
