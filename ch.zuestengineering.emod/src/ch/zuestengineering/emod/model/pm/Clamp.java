/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.model.pm;

import java.util.ArrayList;

import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import ch.zuestengineering.emod.model.APhysicalComponent;
import ch.zuestengineering.emod.model.ParameterCheckReport;
import ch.zuestengineering.emod.model.linking.IOContainer;
import ch.zuestengineering.emod.model.parameters.ParameterSet;
import ch.zuestengineering.emod.model.units.*;
import ch.zuestengineering.emod.utils.ComponentConfigReader;

/**
 * General clamp model class. Implements the physical model of a clamp. From the
 * input process force and clamp position, the requested linear speed and motor
 * force are calculated
 * 
 * Assumptions: The interia and frictional losses are negligible
 * 
 * Inputlist: 1: Postion : [mm] : Actual translational speed Outputlist: 1:
 * ActuatorForce : [N] : Requested motor force
 * 
 * Config parameters: SpringStiffness : [N/mm] : Spring constant of the material
 * WorkPiecePostion : [mm] : Position of the working piece
 * 
 * @author simon
 * 
 */
@XmlRootElement
public class Clamp extends APhysicalComponent {

	@XmlElement
	protected String type;

	// Input parameters:
	private IOContainer position;
	// Output parameters:
	private IOContainer force;

	// Save last input values
	private double lastposition;

	// Parameters used by the model.
	private double springconst;
	private double wpposition;

	/**
	 * Constructor called from XmlUnmarshaller. Attribute 'type' is set by
	 * XmlUnmarshaller.
	 */
	public Clamp() {
		super();
	}

	/**
	 * post xml init method (loading physics data)
	 * 
	 * @param u
	 * @param parent
	 */
	public void afterUnmarshal(final Unmarshaller u, final Object parent) {
		init();
	}

	/**
	 * Clamp constructor
	 * 
	 * @param type
	 */
	public Clamp(String type) {
		super();

		this.type = type;
		init();
	}

	/**
	 * Called from constructor or after unmarshaller.
	 */
	private void init() {
		/* Define Input parameters */
		inputs = new ArrayList<IOContainer>();
		position = new IOContainer("Position", new SiUnit(Unit.M), 0, ContainerType.MECHANIC);
		inputs.add(position);

		/* Define output parameters */
		outputs = new ArrayList<IOContainer>();
		force = new IOContainer("Force", new SiUnit(Unit.NEWTON), 0, ContainerType.MECHANIC);
		outputs.add(force);

		/* Read configuration parameters: */
		loadParameters();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.ethz.inspire.emod.model.APhysicalComponent#update()
	 */
	@Override
	public void update() {

		if (lastposition == position.getValue()) {
			// Input values did not change, nothing to do.
			return;
		}
		lastposition = position.getValue(); // [m]

		/*
		 * Clamping force If the clamp is close enough, calculate a clamping force
		 */
		if (lastposition < wpposition)
			force.setValue(springconst * (wpposition - lastposition));
		else
			force.setValue(0);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.ethz.inspire.emod.model.APhysicalComponent#getType()
	 */
	@Override
	public String getType() {
		return type;
	}

	@Override
	public void setType(String type) {
		this.type = type;
	}

	@Override
	public void updateBoundaryConditions() {
		// TODO Auto-generated method stub

	}

	@Override
	public ParameterCheckReport checkConfigParams() {
		ParameterCheckReport report = new ParameterCheckReport();
		// Check model parameters:
		// Parameter must be non negative
		if (springconst < 0) {
			report.add("SpringStiffness", "Negative value: Spring stiffness must be non negative");
		}
		return report;
	}

	@Override
	public void loadParameters() {
		super.loadParameters();

		springconst = parameterSet.getPhysicalValue("SpringStiffness").getValue();
		wpposition = parameterSet.getPhysicalValue("WorkPiecePostion").getValue();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.zuestengineering.emod.model.APhysicalComponent#updateParameterSet()
	 */
	@Override
	protected void updateParameterSet() {
		logger.warning(
				"Parameter set is empty; tying for old parameter set format (this option will be removed in later versions) ...");
		ComponentConfigReader params = null;
		/* Open file containing the parameters of the model type: */
		try {
			params = new ComponentConfigReader(getModelType(), type);
		} catch (Exception e) {
			e.printStackTrace();
		}

		/* Read the config parameter: */
		try {
			springconst = params.getPhysicalValue("SpringStiffness", new SiUnit("N m^-1")).getValue();
			wpposition = params.getPhysicalValue("WorkPiecePostion", new SiUnit("m")).getValue();
		} catch (Exception e) {
			e.printStackTrace();
		}
		params.Close(); /* Model configuration file not needed anymore. */

		parameterSet = new ParameterSet(this.toString());
		parameterSet.setPhysicalValue("SpringStiffness", springconst, new SiUnit("N m^-1"));
		parameterSet.setPhysicalValue("WorkPiecePostion", wpposition, new SiUnit("m"));
		parameterSet.save(getParameterFile());
		logger.info("Parameter file successfully converted to new format");
	}
}
