/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.model.pm;

import java.util.ArrayList;

import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import ch.zuestengineering.emod.dd.Duct;
import ch.zuestengineering.emod.femexport.BoundaryCondition;
import ch.zuestengineering.emod.femexport.BoundaryConditionType;
import ch.zuestengineering.emod.model.APhysicalComponent;
import ch.zuestengineering.emod.model.ParameterCheckReport;
import ch.zuestengineering.emod.model.fluid.FECDuct;
import ch.zuestengineering.emod.model.fluid.Floodable;
import ch.zuestengineering.emod.model.fluid.FluidCircuitProperties;
import ch.zuestengineering.emod.model.linking.FluidContainer;
import ch.zuestengineering.emod.model.linking.IOContainer;
import ch.zuestengineering.emod.model.parameters.PhysicalValue;
import ch.zuestengineering.emod.model.thermal.ThermalArray;
import ch.zuestengineering.emod.model.thermal.ThermalElement;
import ch.zuestengineering.emod.model.units.ContainerType;
import ch.zuestengineering.emod.model.units.SiUnit;
import ch.zuestengineering.emod.model.units.Unit;
import ch.zuestengineering.emod.simulation.DynamicState;
import ch.zuestengineering.emod.utils.ComponentConfigReader;

/**
 * Implements the physical model of a single mass element with a heat sorce and
 * a thermal interface to a fluid (coolant)
 * 
 * @author Simon Z�st
 *
 */
@XmlRootElement
public class CooledHeatSource extends APhysicalComponent implements Floodable {

	@XmlElement
	protected String type;

	// Inputs
	protected IOContainer heatSource;
	protected ArrayList<FluidContainer> fluidIn = new ArrayList<FluidContainer>();
	// Outputs
	protected ArrayList<FluidContainer> fluidOut = new ArrayList<FluidContainer>();
	// Fluid Properties
	protected ArrayList<FluidCircuitProperties> fluidProperties = new ArrayList<FluidCircuitProperties>();

	// Model parameters
	protected double massStructure;
	protected int nDucts;

	// Intermediate results
	private double[] currentHTC;
	private double[] currentHeatTransfer;
	private double[] lastTemperatureIn;

	// Sub models
	protected ArrayList<Duct> duct = new ArrayList<Duct>();
	protected ThermalElement structure;
	protected ArrayList<ThermalArray> fluid = new ArrayList<ThermalArray>();

	// Boundary conditions
	protected BoundaryCondition bcHeatSource;
	protected ArrayList<BoundaryCondition> bcHeatFlux = new ArrayList<BoundaryCondition>();
	protected ArrayList<BoundaryCondition> bcFluidTemperature = new ArrayList<BoundaryCondition>();
	protected ArrayList<BoundaryCondition> bcHTC = new ArrayList<BoundaryCondition>();
	protected BoundaryCondition bcStructureTemperature;

	/**
	 * Constructor called from XmlUnmarshaller. Attribute 'type' is set by
	 * XmlUnmarshaller.
	 */
	public CooledHeatSource() {
		super();
	}

	/**
	 * @param u
	 * @param parent
	 */
	public void afterUnmarshal(Unmarshaller u, Object parent) {
		// post xml init method (loading physics data)
		init();
	}

	/**
	 * CooledHeatSource constructor
	 * 
	 * @param type
	 */
	public CooledHeatSource(String type) {
		super();

		this.type = type;
		init();
	}

	/**
	 * Initialize the model
	 */
	private void init() {
		// Inputs
		inputs = new ArrayList<IOContainer>();
		heatSource = new IOContainer("HeatInput", new SiUnit("W"), 0, ContainerType.THERMAL);
		inputs.add(heatSource);

		/* Read configuration parameters: */
		ComponentConfigReader params = null;
		/* Open file containing the parameters of the model type: */
		try {
			params = new ComponentConfigReader(getModelType(), type);
		} catch (Exception e) {
			e.printStackTrace();
		}

		try {
			massStructure = params.getValue("StructureMass", new PhysicalValue(1.0, new SiUnit("kg"))).getValue();
			nDucts = params.getValue("NumberOfDucts", 1);
			params.saveValues();

			currentHeatTransfer = new double[nDucts];
			currentHTC = new double[nDucts];
			lastTemperatureIn = new double[nDucts];

			structure = new ThermalElement(params.getValue("StructureMaterial", "Steel"), massStructure);

			// Add states
			dynamicStates = new ArrayList<DynamicState>();
			dynamicStates.add(0, structure.getTemperature());

			// Change state names
			structure.getTemperature().setName("TemperatureStructure");

			for (int i = 0; i < nDucts; i++) {
				// Build name
				String nameSuffix = "";
				if (i > 0)
					nameSuffix += "" + i;

				/* Struct */

				duct.add(Duct.buildFromFile(getModelType(), getType(),
						params.getValue("StructureDuct", "DuctCoolant" + nameSuffix)));
				/* Thermal models */
				fluid.add(new ThermalArray("Example", duct.get(i).getVolume(), 20));

				/* Fluid properties */
				fluidProperties.add(new FluidCircuitProperties(new FECDuct(duct.get(i), fluid.get(i).getTemperature()),
						fluid.get(i).getTemperature()));

				/* Define fluid in-/outputs */
				fluidIn.add(new FluidContainer("CoolantIn" + nameSuffix, new SiUnit(Unit.NONE),
						ContainerType.FLUIDDYNAMIC, fluidProperties.get(i)));
				inputs.add(fluidIn.get(i));
				fluidOut.add(new FluidContainer("CoolantOut" + nameSuffix, new SiUnit(Unit.NONE),
						ContainerType.FLUIDDYNAMIC, fluidProperties.get(i)));
				outputs.add(fluidOut.get(i));

				// Change state names
				fluid.get(i).getTemperature().setName("TemperatureCoolant" + nameSuffix);

				// Add state
				dynamicStates.add(1, fluid.get(i).getTemperature());

				/* Fluid circuit parameters */
				fluid.get(i).setMaterial(fluidProperties.get(i).getMaterial());
				duct.get(i).setMaterial(fluidProperties.get(i).getMaterial());
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		params.Close(); /* Model configuration file not needed anymore. */

		/* Boundary conditions */
		boundaryConditions = new ArrayList<BoundaryCondition>();

		for (int i = 0; i < nDucts; i++) {
			String nameSuffix = "";
			if (nDucts > 1)
				nameSuffix += "" + i;

			bcHTC.add(new BoundaryCondition("CoolantHTC" + nameSuffix, new SiUnit("W/K"), 0,
					BoundaryConditionType.ROBIN));
			bcFluidTemperature.add(new BoundaryCondition("CoolantTemperature" + nameSuffix, new SiUnit("K"), 293.15,
					BoundaryConditionType.ROBIN));
			bcHeatFlux.add(new BoundaryCondition("CoolantHeatFlux" + nameSuffix, new SiUnit("W"), 0,
					BoundaryConditionType.NEUMANN));
			bcHeatSource = new BoundaryCondition("HeatSource", new SiUnit("W"), 0, BoundaryConditionType.NEUMANN);
			bcStructureTemperature = new BoundaryCondition("StructureTemperature", new SiUnit("K"), 293.15,
					BoundaryConditionType.DIRICHLET);
		}

		boundaryConditions.addAll(bcHTC);
		boundaryConditions.addAll(bcFluidTemperature);
		boundaryConditions.addAll(bcHeatFlux);
		boundaryConditions.add(bcHeatSource);
		boundaryConditions.add(bcStructureTemperature);
	}

	@Override
	public ArrayList<FluidCircuitProperties> getFluidPropertiesList() {
		ArrayList<FluidCircuitProperties> out = new ArrayList<FluidCircuitProperties>();
		out.addAll(fluidProperties);
		return out;
	}

	@Override
	public void flood() {
	}

	@Override
	public String getType() {
		return type;
	}

	@Override
	public void update() {
		for (int i = 0; i < nDucts; i++) {
			if (fluidIn.get(i).getTemperature() <= 0) {
				if (Double.isNaN(lastTemperatureIn[i]))
					lastTemperatureIn[i] = structure.getTemperature().getValue();
			} else
				lastTemperatureIn[i] = fluidIn.get(i).getTemperature();

			// Thermal resistance
			currentHTC[i] = duct.get(i).getThermalResistance(fluidProperties.get(i).getFlowRate(),
					fluidProperties.get(i).getPressureIn(), fluidProperties.get(i).getTemperatureIn(),
					structure.getTemperature().getValue());

			// Coolant
			fluid.get(i).setThermalResistance(currentHTC[i]);
			fluid.get(i).setFlowRate(fluidProperties.get(i));
			fluid.get(i).setHeatSource(0.0);
			fluid.get(i).setTemperatureAmb(structure.getTemperature().getValue());
			fluid.get(i).setTemperatureIn(fluidIn.get(i).getTemperature());
		}

		// Thermal flows
		structure.setHeatInput(heatSource.getValue());
		for (int i = 0; i < nDucts; i++)
			structure.addHeatInput(fluid.get(i).getHeatLoss());

		// Update submodels
		structure.integrate(timestep);
		// TODO set Pressure!
		for (int i = 0; i < nDucts; i++)
			fluid.get(i).integrate(timestep, 0, 0, 100000);
	}

	@Override
	public void updateBoundaryConditions() {
		bcStructureTemperature.setValue(structure.getTemperature().getValue());
		bcHeatSource.setValue(heatSource.getValue());

		for (int i = 0; i < nDucts; i++) {
			bcHTC.get(i).setValue(currentHTC[i]);
			bcFluidTemperature.get(i).setValue(fluid.get(i).getTemperature().getValue());
			bcHeatFlux.get(i).setValue(currentHeatTransfer[i]);
		}
	}

	@Override
	public void setType(String type) {
		this.type = type;
	}

	@Override
	public ParameterCheckReport checkConfigParams() {
		return new ParameterCheckReport();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.zuestengineering.emod.model.APhysicalComponent#updateParameterSet()
	 */
	@Override
	protected void updateParameterSet() {
		// TODO Auto-generated method stub

	}

}
