/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.model.pm;

import java.util.ArrayList;

import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import ch.zuestengineering.emod.model.APhysicalComponent;
import ch.zuestengineering.emod.model.ParameterCheckReport;
import ch.zuestengineering.emod.model.linking.IOContainer;
import ch.zuestengineering.emod.model.units.*;
import ch.zuestengineering.emod.utils.ComponentConfigReader;

/**
 * Physical model for constant components.
 * <p>
 * configuration file:<br />
 * xml properties file according to http://java.sun.com/dtd/properties.dtd 1
 * entry named "level" with the power levels per state.
 * </p>
 * input: <br />
 * the current state as a double value without a unit.<br />
 * <p>
 * example:<br />
 * 3 use levels with power values 0.0, 50.0, 500.0<br />
 * input is 0.0, 1.0, 2.0
 * </p>
 * 
 * @author dhampl
 * 
 */
@XmlRootElement
public class ConstantComponent extends APhysicalComponent {

	@XmlElement
	protected String type;

	protected double[] levels;

	// input
	private IOContainer level;

	// output
	private IOContainer ptotal;

	/**
	 * ConstantComponent of Type type
	 * 
	 * @param type
	 */
	public ConstantComponent(String type) {
		this.type = type;
		init();
	}

	/**
	 * empty JAXB constructor
	 */
	public ConstantComponent() {

	}

	/**
	 * post xml init method (loading physics data)
	 * 
	 * @param u
	 * @param parent
	 */
	public void afterUnmarshal(final Unmarshaller u, final Object parent) {
		init();
	}

	private void init() {
		// input
		inputs = new ArrayList<IOContainer>();
		level = new IOContainer("level", new SiUnit(Unit.NONE), 0, ContainerType.CONTROL);
		inputs.add(level);
		// output
		outputs = new ArrayList<IOContainer>();
		ptotal = new IOContainer("PTotal", new SiUnit(Unit.WATT), 0, ContainerType.ELECTRIC);
		outputs.add(ptotal);

		loadParameters();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.ethz.inspire.emod.model.APhysicalComponent#update()
	 */
	@Override
	public void update() {
		if (level.getValue() < 0 || level.getValue() > levels.length - 1)
			ptotal.setValue(Double.NaN);
		else
			ptotal.setValue(levels[(int) level.getValue()]);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.ethz.inspire.emod.model.APhysicalComponent#getType()
	 */
	@Override
	public String getType() {
		return type;
	}

	@Override
	public void setType(String type) {
		this.type = type;
	}

	@Override
	public void updateBoundaryConditions() {/* Not used */
	}

	@Override
	public ParameterCheckReport checkConfigParams() {
		return new ParameterCheckReport();
	}

	@Override
	public void loadParameters() {
		ComponentConfigReader configReader = null;
		try {
			configReader = new ComponentConfigReader(getModelType(), type);
		} catch (Exception e) {
			e.printStackTrace();
		}

		try {
			levels = configReader.getPhysicalValue("levels", new SiUnit("W")).getValues();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.zuestengineering.emod.model.APhysicalComponent#updateParameterSet()
	 */
	@Override
	protected void updateParameterSet() {
		// TODO Auto-generated method stub

	}

}
