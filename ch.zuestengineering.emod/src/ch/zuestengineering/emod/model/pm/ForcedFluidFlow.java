/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.model.pm;

import java.util.ArrayList;

import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import ch.zuestengineering.emod.model.APhysicalComponent;
import ch.zuestengineering.emod.model.ParameterCheckReport;
import ch.zuestengineering.emod.model.fluid.FECForcedFlow;
import ch.zuestengineering.emod.model.fluid.Fillable;
import ch.zuestengineering.emod.model.fluid.Floodable;
import ch.zuestengineering.emod.model.fluid.FluidCircuitProperties;
import ch.zuestengineering.emod.model.linking.FluidContainer;
import ch.zuestengineering.emod.model.linking.IOContainer;
import ch.zuestengineering.emod.model.material.Material;
import ch.zuestengineering.emod.model.parameters.ParameterSet;
import ch.zuestengineering.emod.model.units.ContainerType;
import ch.zuestengineering.emod.model.units.SiUnit;
import ch.zuestengineering.emod.model.units.Unit;
import ch.zuestengineering.emod.simulation.DynamicState;
import ch.zuestengineering.emod.utils.ComponentConfigReader;

/**
 * Implements a forced fluid flow
 * 
 * @author sizuest
 * 
 */
@XmlRootElement
public class ForcedFluidFlow extends APhysicalComponent implements Floodable, Fillable {

	@XmlElement
	protected String type;

	// Input parameters:
	private IOContainer temperatureIn;
	private IOContainer pressureIn;
	private IOContainer flowRateCmd;
	private FluidContainer fluidIn;

	// Output parameters:
	private IOContainer pressureLoss;
	private IOContainer temperatureRaise;
	private FluidContainer fluidOut;
	private IOContainer pThermal;

	// Fluid Properties
	FluidCircuitProperties fluidProperties;
	DynamicState temperature;
	Material material;

	boolean fluidSet = false;

	/**
	 * Constructor called from XmlUnmarshaller. Attribute 'type' is set by
	 * XmlUnmarshaller.
	 */
	public ForcedFluidFlow() {
		super();
		this.type = "Example";
		init();
		material = new Material("Monoethylenglykol_34");
		fluidProperties.setMaterial(material);
	}

	/**
	 * post xml init method (loading physics data)
	 * 
	 * @param u
	 * @param parent
	 */
	public void afterUnmarshal(final Unmarshaller u, final Object parent) {
		init();
	}

	/**
	 * Pipe constructor
	 * 
	 * @param type
	 */
	public ForcedFluidFlow(String type) {
		super();

		this.type = type;
		init();
	}

	private void init() {

		// add inputs
		inputs = new ArrayList<IOContainer>();
		temperatureIn = new IOContainer("TemperatureIn", new SiUnit(Unit.KELVIN), 293.15, ContainerType.THERMAL);
		pressureIn = new IOContainer("PressureIn", new SiUnit(Unit.PA), 0, ContainerType.FLUIDDYNAMIC);
		flowRateCmd = new IOContainer("FlowRate", new SiUnit(Unit.METERCUBIC_S), 0, ContainerType.FLUIDDYNAMIC);
		inputs.add(temperatureIn);
		inputs.add(pressureIn);
		inputs.add(flowRateCmd);

		// add outputs
		outputs = new ArrayList<IOContainer>();
		temperatureRaise = new IOContainer("TemperatureRaise", new SiUnit(Unit.KELVIN), 0, ContainerType.THERMAL);
		pressureLoss = new IOContainer("PressureLoss", new SiUnit(Unit.PA), 0, ContainerType.FLUIDDYNAMIC);
		pThermal = new IOContainer("PThermal", new SiUnit("W"), 0, ContainerType.THERMAL);
		outputs.add(temperatureRaise);
		outputs.add(pressureLoss);
		outputs.add(pThermal);

		// Flow rate Obj
		temperature = new DynamicState("Temperature", new SiUnit("K"));
		temperature.setInitialCondition(293.15);
		fluidProperties = new FluidCircuitProperties(new FECForcedFlow(flowRateCmd), temperature);

		// add fluid In/Output
		fluidIn = new FluidContainer("FluidIn", new SiUnit(Unit.NONE), ContainerType.FLUIDDYNAMIC, fluidProperties);
		inputs.add(fluidIn);
		fluidOut = new FluidContainer("FluidOut", new SiUnit(Unit.NONE), ContainerType.FLUIDDYNAMIC, fluidProperties);
		outputs.add(fluidOut);

		fluidProperties.setPressureReferenceOut(pressureIn);

		loadParameters();
	}

	@Override
	public String getType() {
		return this.type;
	}

	@Override
	public void update() {
		if (!fluidSet) {
			fluidProperties.setMaterial(fluidProperties.getMaterial());
			fluidSet = true;
		}

		// Set forced output
		temperature.setValue(temperatureIn.getValue());
		// Calculate differences
		temperatureRaise.setValue(fluidIn.getTemperature() - temperatureIn.getValue());
		pressureLoss.setValue(pressureIn.getValue() - fluidIn.getPressure());

		pThermal.setValue(temperatureRaise.getValue() * fluidProperties.getMassFlowRate()
				* fluidProperties.getMaterial().getHeatCapacity(fluidProperties));
	}

	@Override
	public void setType(String type) {
		this.type = type;
	}

	@Override
	public ArrayList<FluidCircuitProperties> getFluidPropertiesList() {
		ArrayList<FluidCircuitProperties> out = new ArrayList<FluidCircuitProperties>();
		out.add(fluidProperties);
		return out;
	}

	@Override
	public void flood() {
		fluidProperties.setMaterial(this.material);
	}

	@Override
	public void updateBoundaryConditions() {/* Not used */
	}

	@Override
	public ParameterCheckReport checkConfigParams() {
		return new ParameterCheckReport();
	}

	@Override
	public void loadParameters() {

		super.loadParameters();

		/* Read the config parameter: */
		try {
			fluidProperties.getMaterial().setMaterial(parameterSet.getMaterial("Fluid"));
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.zuestengineering.emod.model.APhysicalComponent#updateParameterSet()
	 */
	@Override
	protected void updateParameterSet() {
		logger.warning(
				"Parameter set is empty; tying for old parameter set format (this option will be removed in later versions) ...");
		/* Read configuration parameters: */
		ComponentConfigReader params = null;
		Material fluid = new Material("Water");
		/* Open file containing the parameters of the model type: */
		try {
			params = new ComponentConfigReader(getModelType(), type);
			fluid = new Material(params.getString("Material"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		params.Close(); /* Model configuration file not needed anymore. */

		parameterSet = new ParameterSet(this.toString());
		parameterSet.setMaterial("Fluid", fluid);
		parameterSet.save(getParameterFile());
		logger.info("Parameter file successfully converted to new format");

	}

}
