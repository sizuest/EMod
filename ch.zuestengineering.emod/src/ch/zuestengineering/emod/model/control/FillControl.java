/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.model.control;

import java.util.ArrayList;

import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import ch.zuestengineering.emod.model.APhysicalComponent;
import ch.zuestengineering.emod.model.ParameterCheckReport;
import ch.zuestengineering.emod.model.linking.IOContainer;
import ch.zuestengineering.emod.model.parameters.ParameterSet;
import ch.zuestengineering.emod.model.units.SiUnit;

/**
 * General fill controller class.
 * 
 * Output value takes values Low or High if dependent on a control and
 * a fill gauge control signal
 * 
 * 
 * @author andreas
 * 
 */
@XmlRootElement
public class FillControl extends APhysicalComponent {

	@XmlElement
	protected String type;

	// Input parameters:
	private IOContainer ctrlSignal;
	private IOContainer tankSignal;
	// Output parameters:
	private IOContainer pumpSignal;
	private IOContainer valveSignal;

	/**
	 * Constructor called from XmlUnmarshaller. Attribute 'type' is set by
	 * XmlUnmarshaller.
	 */
	public FillControl() {
		super();
	}

	/**
	 * post xml init method (loading physics data)
	 * 
	 * @param u
	 * @param parent
	 */
	public void afterUnmarshal(Unmarshaller u, Object parent) {
		init();
	}

	/**
	 * Hysteresis constructor
	 * 
	 * @param type
	 */
	public FillControl(String type) {
		super();

		this.type = type;
		init();
	}

	/**
	 * Called from constructor or after unmarshaller.
	 */
	private void init() {

		outputs = new ArrayList<IOContainer>();
		inputs = new ArrayList<IOContainer>();

		/* Define input parameters */
		ctrlSignal = new IOContainer("State", new SiUnit(""), 0);
		tankSignal = new IOContainer("Level", new SiUnit(""), 0);
		inputs.add(ctrlSignal);
		inputs.add(tankSignal);

		/* Define output parameters */
		pumpSignal  = new IOContainer("Pump", new SiUnit(""), 0);
		valveSignal = new IOContainer("Valve", new SiUnit(""), 0);
		outputs.add(pumpSignal);
		outputs.add(valveSignal);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.ethz.inspire.emod.model.APhysicalComponent#update()
	 */
	@Override
	public void update() {

		// Filling is on and tank is not full
		if(ctrlSignal.getValue() >= 0.5 & tankSignal.getValue() < 0.95) {
			pumpSignal.setValue(1.0);
			valveSignal.setValue(0.0);
		}
		// Filling is on and tank is full
		else if(ctrlSignal.getValue() >= 0.5 & tankSignal.getValue() == 1) {
			pumpSignal.setValue(0.0);
			valveSignal.setValue(0.0);
		}
		// Filling is on and tank is full
		else if(ctrlSignal.getValue() >= 0.5 & tankSignal.getValue() > 1) {
			pumpSignal.setValue(0.0);
			valveSignal.setValue(Math.min(1, Math.max(0, 10*(tankSignal.getValue()-1.05))));
		}
		// Filling is off and tank not empty
		else if(ctrlSignal.getValue() < 0.5 & tankSignal.getValue() > 0) {
			pumpSignal.setValue(0.0);
			valveSignal.setValue(1.0);
		}
		// Filling is off and tank empty
		else if(ctrlSignal.getValue() < 0.5 & tankSignal.getValue() <= 0) {
			pumpSignal.setValue(0.0);
			valveSignal.setValue(1.0);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.ethz.inspire.emod.model.APhysicalComponent#getType()
	 */
	@Override
	public String getType() {
		return type;
	}

	@Override
	public void setType(String type) {
		this.type = type;
	}

	@Override
	public void updateBoundaryConditions() {
		// TODO Auto-generated method stub

	}

	@Override
	public ParameterCheckReport checkConfigParams() {
		ParameterCheckReport report = new ParameterCheckReport();

		return report;
	}

	@Override
	public void loadParameters() {
		super.loadParameters();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.zuestengineering.emod.model.APhysicalComponent#updateParameterSet()
	 */
	@Override
	protected void updateParameterSet() {
		parameterSet = new ParameterSet();
	}

}
