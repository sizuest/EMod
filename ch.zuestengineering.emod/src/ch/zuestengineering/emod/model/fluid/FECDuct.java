/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
/** $Id$
 *
 * $URL$
 * $Author$
 * $Date$
 * $Rev$
 *
 * Copyright (c) 2011 by Inspire AG, ETHZ
 * All rights reserved
 *
 ***********************************/

package ch.zuestengineering.emod.model.fluid;

import ch.zuestengineering.emod.dd.Duct;
import ch.zuestengineering.emod.simulation.DynamicState;

/**
 * Element with zeta from duct model
 * 
 * @author sizuest
 *
 */
public class FECDuct extends AFluidElementCharacteristic {

	Duct duct;
	DynamicState temperature;

	/**
	 * @param duct
	 * @param temperature
	 */
	public FECDuct(Duct duct, DynamicState temperature) {
		this.duct = duct;
		this.temperature = temperature;
	}

	@Override
	public double getA0(double flowRate, double pressureIn, double pressureOut) {
		double a0 = -duct.getPressureDrop(flowRate, pressureIn, temperature.getValue());// - flowRate *
																						// duct.getPressureLossDrivative(flowRate,
																						// pressureIn,
																						// temperature.getValue());
		return a0;
	}

	@Override
	public double getA1(double flowRate, double pressureIn, double pressureOut) {
		double a1;
		a1 = duct.getPressureLossDrivative(flowRate, pressureIn, temperature.getValue());
		return a1;
	}

	@Override
	public double getEp(double flowRate, double pressureIn, double pressureOut) {
		return 1;
	}

}
