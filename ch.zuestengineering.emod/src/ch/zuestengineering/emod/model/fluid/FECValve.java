/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.model.fluid;

import ch.zuestengineering.emod.model.pm.Valve;

/**
 * Element with zeta according to valve model
 * 
 * @author sizuest
 *
 */
public class FECValve extends AFluidElementCharacteristic {

	Valve valve;

	/**
	 * @param valve
	 */
	public FECValve(Valve valve) {
		this.valve = valve;
	}

	@Override
	public double getA0(double flowRate, double pressureIn, double pressureOut) {
		if (valve.isClosed())
			return pressureIn - pressureOut;

		double a0 = valve.getPressure(flowRate);

		if (Double.isInfinite(a0))
			if (a0 > 0)
				a0 = 1E24;
			else
				a0 = -1E24;

		return -a0;
	}

	@Override
	public double getA1(double flowRate, double pressureIn, double pressureOut) {
		if (valve.isClosed())
			return 1e24;

		return 2 * valve.getPressureLossCoefficient() * flowRate * Fluid.sign(flowRate);
	}

	@Override
	public double getEp(double flowRate, double pressureIn, double pressureOut) {
		if (valve.isClosed())
			return 1;

		return 1;
	}

}
