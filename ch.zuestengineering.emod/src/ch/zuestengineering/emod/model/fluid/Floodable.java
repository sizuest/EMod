/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.model.fluid;

import java.util.ArrayList;

/**
 * Interface to define common methods of Floodable Machine Components, such as:
 * - Pipe, Tank, Pump
 * 
 * Usage: - in ch.ethz.inspire.emod.utils.FluidCircuit to flood components with
 * fluid - in Machine class to get all the Floodable components
 * 
 * @author manick
 * 
 */
public interface Floodable {

	/**
	 * method to get the fluidtype of a floodable component needs to be overriden by
	 * the component
	 * 
	 * @return fluidType
	 */
	public ArrayList<FluidCircuitProperties> getFluidPropertiesList();

	/**
	 * Method to be called pre-simulation
	 */
	public void flood();

}
