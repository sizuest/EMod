/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
/** $Id$
 *
 * $URL$
 * $Author$
 * $Date$
 * $Rev$
 *
 * Copyright (c) 2011 by Inspire AG, ETHZ
 * All rights reserved
 *
 ***********************************/

package ch.zuestengineering.emod.model.fluid;

import ch.zuestengineering.emod.model.pm.Tank;

/**
 * Element with no pressure drop
 * 
 * @author sizuest
 *
 */
public class FECTank extends AFluidElementCharacteristic {
	private Tank tank;
	private boolean inlet;
	
	/**
	 * @param tank 
	 * @param inlet 
	 * 
	 */
	public FECTank(Tank tank, boolean inlet) {
		this.tank = tank;
		this.inlet = inlet;
	}

	@Override
	public double getA0(double flowRate, double pressureIn, double pressureOut) {
		return 0;
			//return -Math.pow(flowRate, 2) * pressureLostCoefficient * Math.signum(flowRate);
	}

	@Override
	public double getA1(double flowRate, double pressureIn, double pressureOut) {
		if(inlet)
			return 0;
		
		if(tank.isEmpty())
			return 1;
		else
			return 0;
			//return 2 * pressureLostCoefficient * flowRate * Fluid.sign(flowRate);
	}

	@Override
	public double getEp(double flowRate, double pressureIn, double pressureOut) {
		if(inlet)
			return 1.0;
		
		if(tank.isEmpty())
			return 0.0;
		else
			return 1.0;
	}	

}
