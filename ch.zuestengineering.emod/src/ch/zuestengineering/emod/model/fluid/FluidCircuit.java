/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.model.fluid;

import java.util.ArrayList;
import java.util.logging.Logger;

import ch.zuestengineering.emod.Machine;
import ch.zuestengineering.emod.model.MachineComponent;
import ch.zuestengineering.emod.model.linking.FluidContainer;

/**
 * Class to perform several checks and routines on fluid circuits
 * 
 * @author manick
 */
public class FluidCircuit {

	protected Logger logger = Logger.getLogger(FluidCircuit.class.getName());
	ArrayList<FluidCircuitProperties> members = new ArrayList<>();

	/**
	 * @param members
	 */
	public FluidCircuit(ArrayList<FluidCircuitProperties> members) {
		this.members = members;
	}

	/**
	 * Returns the array of members
	 * 
	 * @return
	 */
	public ArrayList<FluidCircuitProperties> getMembers() {
		return members;
	}

	/**
	 * Returns the name of the first component containing any of the stated FCPs
	 * Tank and HydrAcc are ignored
	 * 
	 * @return
	 */
	public String getName() {
		for (MachineComponent mc : Machine.getInstance().getFloodableMachineComponentList()) {
			for (FluidCircuitProperties f : ((Floodable) (mc.getComponent())).getFluidPropertiesList())
				if (getMembers().contains(f) & !(mc.getComponent() instanceof Fillable))
					return mc.getName() + "/" + f.getMaterial().getType();
		}

		return "none";

	}

	/**
	 * Checks if the circuit is fully closed
	 * 
	 * Only fillable mc are allowed to have no pre or post
	 * 
	 * @return
	 */
	public boolean isClosed() {
		for (MachineComponent mc : Machine.getInstance().getFloodableMachineComponentList()) {
			for (FluidCircuitProperties f : ((Floodable) (mc.getComponent())).getFluidPropertiesList()) {
				if (getMembers().contains(f) & !(mc.getComponent() instanceof Fillable)) {
					if (f.getPost().size() == 0 | f.getPre().size() == 0)
						return false;
				} else {
					if (f.getPost().size() == 0 & f.getPre().size() == 0)
						return false;
				}
			}

		}

		return true;

	}

	/**
	 * Checks if the curcuit contains at least one element which define the circuits
	 * material
	 * 
	 * @return
	 */
	public boolean hasSource() {
		for (MachineComponent mc : Machine.getInstance().getFloodableMachineComponentList()) {
			for (FluidCircuitProperties f : ((Floodable) (mc.getComponent())).getFluidPropertiesList())
				if (getMembers().contains(f) & mc.getComponent() instanceof Fillable)
					return true;
		}

		return false;
	}

	/**
	 * used in FluidConnection to flood the components following a tank
	 * 
	 * @param source
	 * @param target
	 */
	public static void floodCircuit(FluidContainer source, FluidContainer target) {
		// Set Fluid properties
		source.getFluidCircuitProperties().setPost(target.getFluidCircuitProperties());
	}
}
