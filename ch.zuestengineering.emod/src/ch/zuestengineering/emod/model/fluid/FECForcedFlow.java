/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.model.fluid;

import ch.zuestengineering.emod.model.linking.IOContainer;

/**
 * Element with fixed flow rate
 * 
 * @author sizuest
 *
 */
public class FECForcedFlow extends AFluidElementCharacteristic {

	IOContainer flowRate;

	/**
	 * @param flowRate
	 */
	public FECForcedFlow(IOContainer flowRate) {
		this.flowRate = flowRate;
	}

	@Override
	public double getA0(double flowRate, double pressureIn, double pressureOut) {
		return -this.flowRate.getValue();
	}

	@Override
	public double getA1(double flowRate, double pressureIn, double pressureOut) {
		return 1;
	}

	@Override
	public double getEp(double flowRate, double pressureIn, double pressureOut) {
		return 0;
	}

}
