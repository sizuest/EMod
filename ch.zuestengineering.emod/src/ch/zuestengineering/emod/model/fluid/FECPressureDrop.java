/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.model.fluid;

import ch.zuestengineering.emod.model.linking.IOContainer;

/**
 * Element with fixed pressure drop
 * 
 * @author sizuest
 *
 */
public class FECPressureDrop extends AFluidElementCharacteristic {

	IOContainer pressure;

	/**
	 * @param pressure
	 */
	public FECPressureDrop(IOContainer pressure) {
		this.pressure = pressure;
	}

	@Override
	public double getA0(double flowRate, double pressureIn, double pressureOut) {
		return this.pressure.getValue();
	}

	@Override
	public double getA1(double flowRate, double pressureIn, double pressureOut) {
		return 0;
	}

	@Override
	public double getEp(double flowRate, double pressureIn, double pressureOut) {
		return 1;
	}

}
