/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
/** $Id$
 *
 * $URL$
 * $Author$
 * $Date$
 * $Rev$
 *
 * Copyright (c) 2011 by Inspire AG, ETHZ
 * All rights reserved
 *
 ***********************************/

package ch.zuestengineering.emod.model.fluid;

/**
 * Element with no pressure drop
 * 
 * @author sizuest
 *
 */
public class FECIdeal extends AFluidElementCharacteristic {

	@Override
	public double getA0(double flowRate, double pressureIn, double pressureOut) {
		return 0;
	}

	@Override
	public double getA1(double flowRate, double pressureIn, double pressureOut) {
		return 0;
	}

	@Override
	public double getEp(double flowRate, double pressureIn, double pressureOut) {
		return 1;
	}

}
