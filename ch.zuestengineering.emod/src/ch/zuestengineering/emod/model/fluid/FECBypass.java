/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.model.fluid;

import ch.zuestengineering.emod.model.pm.Bypass;

/**
 * Element representina a bypass with fixed zeta and opening pressure
 * 
 * @author sizuest
 *
 */
public class FECBypass extends AFluidElementCharacteristic {

	Bypass bypass;

	/**
	 * @param bypass
	 */
	public FECBypass(Bypass bypass) {
		this.bypass = bypass;
	}

	@Override
	public double getA0(double flowRate, double pressureIn, double pressureOut) {

		if (pressureIn - pressureOut < bypass.getPressureMax())
			return 0;
		else
			return bypass.getPressureMax();
	}

	@Override
	public double getA1(double flowRate, double pressureIn, double pressureOut) {

		if (pressureIn - pressureOut < bypass.getPressureMax())
			return 1;
		else
			return 0;
	}

	@Override
	public double getEp(double flowRate, double pressureIn, double pressureOut) {
		if (pressureIn - pressureOut < bypass.getPressureMax())
			return 0;
		else
			return 1;
	}

}
