/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.model.fluid;

import ch.zuestengineering.emod.model.linking.IOContainer;
import ch.zuestengineering.emod.model.pm.Pump;

/**
 * Element with zeta approximated from pump map
 * 
 * @author sizuest
 *
 */
public class FECPump extends AFluidElementCharacteristic {

	Pump pump;
	IOContainer state;

	/**
	 * @param pump
	 * @param state
	 */
	public FECPump(Pump pump, IOContainer state) {
		this.pump = pump;
		this.state = state;
	}

	@Override
	public double getA0(double flowRate, double pressureIn, double pressureOut) {
		double a0;
		if (0 == state.getValue())
			a0 = 0;
		else
			a0 = -pump.getPressure(flowRate) + flowRate * pump.getPressureDrivative(flowRate);

		return a0;
	}

	@Override
	public double getA1(double flowRate, double pressureIn, double pressureOut) {
		if (0 == state.getValue())
			return 1;
		else
			return -pump.getPressureDrivative(flowRate);
	}

	@Override
	public double getEp(double flowRate, double pressureIn, double pressureOut) {
		if (0 == state.getValue())
			return 0;
		else
			return 1;
	}

}
