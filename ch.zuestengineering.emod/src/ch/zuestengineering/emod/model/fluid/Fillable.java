/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.model.fluid;

/**
 * Interface to indentify fillable machine components - such as tanks.
 * 
 * @author sizuest
 *
 */
public interface Fillable {

}
