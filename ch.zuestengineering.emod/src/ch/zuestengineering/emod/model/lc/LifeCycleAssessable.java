/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.model.lc;

import java.util.ArrayList;

import ch.zuestengineering.emod.lca.inventory.MaterialAmount;
import ch.zuestengineering.emod.lca.inventory.RessourceDemand;

/**
 * @author sizuest
 *
 */
public interface LifeCycleAssessable {

	/**
	 * Returns the list of installed materials (w.o. consumables)
	 * 
	 * @return
	 */
	public ArrayList<MaterialAmount> getMaterialAmount();

	/**
	 * Returns the list of consumables
	 * 
	 * @return
	 */
	public ArrayList<RessourceDemand> getMaterialDemand();
}
