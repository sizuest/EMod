/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.model.lc;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.Writer;
import java.util.ArrayList;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import ch.zuestengineering.emod.lca.inventory.MaterialAmount;
import ch.zuestengineering.emod.lca.inventory.RessourceDemand;
import ch.zuestengineering.emod.lca.inventory.Transportation;

/**
 * Represents the relevant LCProperties of a machine component
 * 
 * @author sizuest
 *
 */
@XmlRootElement
public class LCProperties implements Cloneable {
	@XmlElement
	protected ArrayList<MaterialAmount> materialAmount;
	@XmlElement
	protected ArrayList<RessourceDemand> ressourceDemand;
	@XmlElement
	protected ArrayList<Transportation> transportation;

	/**
	 * 
	 */
	public LCProperties() {
		materialAmount = new ArrayList<>();
		ressourceDemand = new ArrayList<>();
		transportation = new ArrayList<>();
	}

	/**
	 * @return the materialAmount
	 */
	public ArrayList<MaterialAmount> getMaterialAmount() {
		return materialAmount;
	}

	/**
	 * @return the materialDemand
	 */
	public ArrayList<RessourceDemand> getRessourceDemand() {
		return ressourceDemand;
	}

	/**
	 * @return the transportation
	 */
	public ArrayList<Transportation> getTransportation() {
		return transportation;
	}

	/**
	 * Saves the LC information at the given path
	 * 
	 * @param path
	 */
	public void save(String path) {
		try {
			JAXBContext context = JAXBContext.newInstance(LCProperties.class);
			Marshaller m = context.createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

			Writer w = new FileWriter(path);
			m.marshal(this, w);
			w.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Loads and returns the lc properties from the given path
	 * 
	 * @param path
	 * @return
	 */
	public static LCProperties load(String path) {
		LCProperties lc = null;

		File file = new File(path);
		if (!(file.exists()))
			(new LCProperties()).save(path);

		try {
			JAXBContext context = JAXBContext.newInstance(LCProperties.class);
			Unmarshaller um = context.createUnmarshaller();
			lc = (LCProperties) um.unmarshal(new FileReader(file));
		} catch (Exception e) {
			e.printStackTrace();
		}

		return lc;
	}

	@Override
	public LCProperties clone() {
		LCProperties clone = new LCProperties();
		for (MaterialAmount m : getMaterialAmount())
			clone.materialAmount.add(m.clone());
		for (RessourceDemand r : getRessourceDemand())
			clone.ressourceDemand.add(r.clone());
		for (Transportation t : getTransportation())
			clone.transportation.add(t.clone());

		return clone;
	}

	/**
	 * Updates the masses of the transports
	 */
	public void updateMasses() {
		double mass = 0;
		for (MaterialAmount m : getMaterialAmount())
			mass += m.getAmount();

		for (Transportation t : getTransportation())
			t.setWeight(mass);
	}

}
