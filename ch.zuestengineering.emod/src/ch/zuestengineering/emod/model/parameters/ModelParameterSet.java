/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.model.parameters;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import ch.zuestengineering.emod.Machine;
import ch.zuestengineering.emod.model.MachineComponent;

/**
 * @author sizuest
 *
 */
public class ModelParameterSet {
	ArrayList<MachineComponentParameterSet> parameters;
	
	/**
	 * 
	 */
	public ModelParameterSet() {
		init();
	}
	
	/**
	 * Exports the current Model PS as csv file
	 * 
	 * @param path
	 * @param sep
	 * @throws IOException 
	 */
	public static void export(String path, String sep) throws IOException {
		String out = ""; 
		
		// Write Header
		String[] head = {"COMPONENT", "MODEL", "TYPE", "PARAMETER", "UNIT", "VALUE"};
		for(String s: head)
			out += s+sep;
		
		out+="\n";
		
		for(MachineComponentParameterSet mc: (new ModelParameterSet()).parameters) {
			// Write type
			out += mc.getName() + sep;
			out += mc.getModeltype() + sep;
			out += mc.getType() + sep;
			
			// Write parameters
			boolean firstLineDone = false;
			for(AParameter<?> p: mc.getParametes().getParameterSet()) {
				if(firstLineDone)
					out += "" + sep + "" + sep + "" + sep;
				else
					firstLineDone = true;
				
				out+=p.toString(sep);
				out+="\n";
			}
			
			out+="\n";
		}
		
		// Write to file
		BufferedWriter writer = new BufferedWriter(new FileWriter(path));
	    writer.write(out);
	    writer.close();		
	}
	
	
	private void init() {
		parameters = new ArrayList<>();
		
		for(MachineComponent mc: Machine.getMachineComponentList())
			parameters.add(new MachineComponentParameterSet(mc));
	}

}

class MachineComponentParameterSet {
	private String name;
	private String mdltype;
	private String pstype;
	private ParameterSet params;
	
	public MachineComponentParameterSet(MachineComponent mc) {
		name    = mc.getName();
		mdltype = mc.getComponent().getModelType();
		pstype  = mc.getComponent().getType();
		params  = mc.getComponent().getParameterSet().clone();
	}
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @return the mdltype
	 */
	public String getModeltype() {
		return mdltype;
	}
	/**
	 * @return the pstype
	 */
	public String getType() {
		return pstype;
	}
	/**
	 * @return the params
	 */
	public ParameterSet getParametes() {
		return params;
	}
	
	
}
