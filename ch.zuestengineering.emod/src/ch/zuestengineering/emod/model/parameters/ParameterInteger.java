/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.model.parameters;

/**
 * @author sizuest
 *
 */
public class ParameterInteger extends AParameter<Integer> {

	/**
	 * 
	 */
	public ParameterInteger() {
		// For unmarshaller
	}

	/**
	 * @param name
	 * @param value
	 */
	public ParameterInteger(String name, int value) {
		super(name, value);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.zuestengineering.emod.model.parameters.AParameter#getValueAsString()
	 */
	@Override
	public String getValueAsString() {
		return value.toString();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.zuestengineering.emod.model.parameters.AParameter#clone()
	 */
	@Override
	public AParameter<Integer> clone() {
		ParameterInteger clone = new ParameterInteger(getName(), getValue());
		return clone;
	}

}
