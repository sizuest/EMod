/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.model.parameters;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlTransient;

import ch.zuestengineering.emod.dd.Duct;
import ch.zuestengineering.emod.dd.DuctSettings;
import ch.zuestengineering.emod.dd.model.ADuctElement;
import ch.zuestengineering.emod.dd.model.AHydraulicProfile;
import ch.zuestengineering.emod.dd.model.DuctArc;
import ch.zuestengineering.emod.dd.model.DuctBypass;
import ch.zuestengineering.emod.dd.model.DuctDefinedValues;
import ch.zuestengineering.emod.dd.model.DuctDefinedValues2;
import ch.zuestengineering.emod.dd.model.DuctDrilling;
import ch.zuestengineering.emod.dd.model.DuctElbowFitting;
import ch.zuestengineering.emod.dd.model.DuctExtension;
import ch.zuestengineering.emod.dd.model.DuctFitting;
import ch.zuestengineering.emod.dd.model.DuctFlowAround;
import ch.zuestengineering.emod.dd.model.DuctHelix;
import ch.zuestengineering.emod.dd.model.DuctPartialFlowAround;
import ch.zuestengineering.emod.dd.model.DuctPipe;
import ch.zuestengineering.emod.dd.model.DuctReduction;
import ch.zuestengineering.emod.dd.model.DuctStaggeredHelix;
import ch.zuestengineering.emod.dd.model.DuctWireTurbulator;
import ch.zuestengineering.emod.dd.model.HPAnnulus;
import ch.zuestengineering.emod.dd.model.HPCircular;
import ch.zuestengineering.emod.dd.model.HPRectangular;
import ch.zuestengineering.emod.dd.model.Isolation;
import ch.zuestengineering.emod.dd.model.IsolationLayer;
import ch.zuestengineering.emod.model.APhysicalComponent;
import ch.zuestengineering.emod.model.material.Material;
import ch.zuestengineering.emod.model.pm.Bearing;
import ch.zuestengineering.emod.model.units.SiUnit;

/**
 * Implementation of a parameter set
 * 
 * @author sizuest
 *
 */
@XmlRootElement(namespace = "ch.zuestengineering.emod")
@XmlSeeAlso({ ParameterInteger.class, ParameterBoolean.class, ParameterDuct.class, ParameterMaterial.class,
		ParameterMatrix.class, ParameterModel.class, ParameterModels.class, ParameterNumeric.class,
		ParameterPhysical.class, ParameterString.class, ParameterVector.class, AParameter.class,
		MachineComponentString.class, Matrix.class, ADuctElement.class, AHydraulicProfile.class, DuctDrilling.class,
		DuctPipe.class, DuctElbowFitting.class, DuctFlowAround.class, DuctFitting.class, DuctHelix.class,
		DuctElbowFitting.class, DuctDefinedValues.class, DuctDefinedValues2.class, DuctBypass.class, DuctArc.class,
		DuctWireTurbulator.class, DuctStaggeredHelix.class, DuctPartialFlowAround.class, DuctExtension.class,
		DuctReduction.class, HPRectangular.class, HPCircular.class, HPAnnulus.class, Isolation.class,
		IsolationLayer.class, Material.class, DuctSettings.class, PhysicalValue.class })
@XmlAccessorType(XmlAccessType.FIELD)
public class ParameterSet implements Cloneable {
	@XmlElement
	private String name;
	@XmlElement
	private String comment;
	@XmlElement
	private List<AParameter<?>> set = new ArrayList<>();

	protected static Logger logger = Logger.getLogger(ParameterSet.class.getName());

	/**
	 * Constructor for marshaller
	 */
	public ParameterSet() {
	}

	/**
	 * @param name
	 */
	public ParameterSet(String name) {
		this.name = name;
	}

	/**
	 * Returns the parameter sets name
	 * 
	 * @return name
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * Sets the parameter set comment
	 * 
	 * @param comment
	 */
	@XmlTransient
	public void setComment(String comment) {
		this.comment = comment;
	}

	/**
	 * Returns the parameter set comment
	 * 
	 * @return
	 */
	public String getComment() {
		return this.comment;
	}

	/**
	 * @param parameter
	 */
	public void set(AParameter<?> parameter) {
		int idx = getIndex(parameter.getName());

		if (idx >= 0) {
			set.remove(idx);
		}

		set.add(parameter);
	}

	/**
	 * @param otherSet
	 */
	public void merge(ParameterSet otherSet) {
		for (AParameter<?> p : otherSet.getParameterSet())
			set(p);
	}

	/**
	 * Sets the physical value with the stated name
	 * 
	 * @param name
	 * @param value
	 */
	public void setPhysicalValue(String name, PhysicalValue value) {
		set(new ParameterPhysical(name, value));
	}

	/**
	 * Sets the physical value with the stated name
	 * 
	 * @param name
	 * @param value
	 * @param unit
	 */
	public void setPhysicalValue(String name, double[] value, SiUnit unit) {
		setPhysicalValue(name, new PhysicalValue(value, unit));
	}

	/**
	 * Sets the physical value with the stated name
	 * 
	 * @param name
	 * @param value
	 * @param unit
	 */
	public void setPhysicalValue(String name, double value, SiUnit unit) {
		setPhysicalValue(name, new PhysicalValue(value, unit));
	}

	/**
	 * Sets the physical value with the stated name
	 * 
	 * @param name
	 * @param valueStr
	 * @param unit
	 */
	public void setPhysicalValue(String name, String valueStr, SiUnit unit) {

		String[] cand = valueStr.split(",");

		double[] value = new double[cand.length];

		for (int i = 0; i < cand.length; i++)
			value[i] = Double.valueOf(cand[i]);

		setPhysicalValue(name, new PhysicalValue(value, unit));
	}

	/**
	 * @param name
	 * @param value
	 */
	public void setBoolean(String name, boolean value) {
		set(new ParameterBoolean(name, value));
	}

	/**
	 * @param name
	 * @param value
	 */
	public void setDouble(String name, double value) {
		set(new ParameterNumeric(name, value));
	}

	/**
	 * @param name
	 * @param value
	 */
	public void setInteger(String name, int value) {
		set(new ParameterInteger(name, value));
	}

	/**
	 * @param name
	 * @param value
	 */
	public void setMatrix(String name, double[][] value) {
		set(new ParameterMatrix(name, value));
	}

	/**
	 * @param name
	 * @param value
	 */
	public void setString(String name, String value) {
		set(new ParameterString(name, value));
	}

	/**
	 * @param name
	 * @param value
	 */
	public void setVector(String name, double[] value) {
		set(new ParameterVector(name, value));
	}

	/**
	 * @param name
	 * @param value
	 */
	public void setMaterial(String name, String value) {
		set(new ParameterMaterial(name, value));
	}

	/**
	 * @param name
	 * @param value
	 */
	public void setMaterial(String name, Material value) {
		set(new ParameterMaterial(name, value));
	}

	/**
	 * @param name
	 * @param model
	 */
	public void setModel(String name, APhysicalComponent model) {
		set(new ParameterModel(name, model.getModelType(), model.getType()));
	}

	/**
	 * @param name
	 * @param model
	 * @param type
	 */
	public void setModel(String name, String model, String type) {
		set(new ParameterModel(name, model, type));
	}

	/**
	 * @param name
	 * @param models
	 */
	public void setModels(String name, ArrayList<APhysicalComponent> models) {
		set(new ParameterModels(name, models));
	}

	/**
	 * @param name
	 * @param models
	 */
	public void setBearings(String name, ArrayList<Bearing> models) {
		ArrayList<APhysicalComponent> list = new ArrayList<>();
		list.addAll(models);

		set(new ParameterModels(name, list));
	}

	/**
	 * @param name
	 * @param duct
	 */
	public void setDuct(String name, Duct duct) {
		set(new ParameterDuct(name, duct));
	}

	/**
	 * Returns the parameter with the given name, returns null if no parameter with
	 * the given name exists
	 * 
	 * @param name
	 * @return
	 */
	public AParameter<?> get(String name) {

		for (AParameter<?> p : set)
			if (p.getName().equals(name)) {
				return p;
			}

		return null;
	}

	/**
	 * Returns the parameter with the given name, returns null if no parameter with
	 * the given name exists
	 * 
	 * @param name
	 * @return
	 */
	private int getIndex(String name) {

		for (AParameter<?> p : set)
			if (p.getName().equals(name)) {
				return set.indexOf(p);
			}

		return -1;
	}

	/**
	 * @param name
	 * @return
	 */
	public String getString(String name) {
		AParameter<?> p = get(name);

		if (p instanceof ParameterString)
			return (String) p.getValue();

		logger.warning("ParameterSet '" + getName() + "': Parameter '" + name + "' of type 'String' not found!");
		return null;
	}

	/**
	 * Gets the physical value with the stated name
	 * 
	 * @param name
	 * @return
	 */
	public PhysicalValue getPhysicalValue(String name) {
		AParameter<?> p = get(name);

		if (p instanceof ParameterPhysical)
			return (PhysicalValue) p.getValue();

		logger.warning("ParameterSet '" + getName() + "': Parameter '" + name + "' of type 'Physical' not found!");
		return null;
	}

	/**
	 * @param name
	 * @return
	 */
	public int getInteger(String name) {
		AParameter<?> p = get(name);

		if (p instanceof ParameterInteger)
			return (int) p.getValue();

		logger.warning("ParameterSet '" + getName() + "': Parameter '" + name + "' of type 'Integer' not found!");
		return 0;
	}

	/**
	 * @param name
	 * @return
	 */
	public double getDouble(String name) {
		AParameter<?> p = get(name);

		if (p instanceof ParameterNumeric)
			return (double) p.getValue();

		logger.warning("ParameterSet '" + getName() + "': Parameter '" + name + "' of type 'Numeric' not found!");
		return Double.NaN;
	}

	/**
	 * @param name
	 * @return
	 */
	public boolean getBoolean(String name) {
		AParameter<?> p = get(name);

		if (p instanceof ParameterBoolean)
			return (boolean) p.getValue();

		logger.warning("ParameterSet '" + getName() + "': Parameter '" + name + "' of type 'Boolean' not found!");
		return false;
	}

	/**
	 * @param name
	 * @return
	 */
	public double[] getVector(String name) {
		AParameter<?> p = get(name);

		if (p instanceof ParameterVector)
			return ((ParameterVector) p).getValue();

		logger.warning("ParameterSet '" + getName() + "': Parameter '" + name + "' of type 'Vector' not found!");
		return null;
	}

	/**
	 * @param name
	 * @return
	 */
	public double[][] getMatrix(String name) {
		AParameter<?> p = get(name);

		if (p instanceof ParameterMatrix)
			return ((Matrix) p.getValue()).get();

		logger.warning("ParameterSet '" + getName() + "': Parameter '" + name + "' of type 'Matrix' not found!");
		return null;
	}

	/**
	 * @param name
	 * @return
	 */
	public Material getMaterial(String name) {
		AParameter<?> p = get(name);

		if (p instanceof ParameterMaterial)
			return ((ParameterMaterial) p).getMaterial();

		logger.warning("ParameterSet '" + getName() + "': Parameter '" + name + "' of type 'Material' not found!");
		return null;
	}
	
	/**
	 * getModel
	 * @param name
	 * @param dummy
	 * @return
	 */
	public ParameterModel getModel(String name, boolean dummy) {
		AParameter<?> p = get(name);

		if (p instanceof ParameterModel)
			return (ParameterModel) p;

		logger.warning("ParameterSet '" + getName() + "': Parameter '" + name + "' of type 'Model' not found!");
		return null;
	}

	/**
	 * @param name
	 * @return
	 */
	public APhysicalComponent getModel(String name) {
		AParameter<?> p = get(name);

		if (p instanceof ParameterModel)
			return ((ParameterModel) p).getModel();

		logger.warning("ParameterSet '" + getName() + "': Parameter '" + name + "' of type 'Model' not found!");
		return null;
	}

	/**
	 * @param name
	 * @param dummy 
	 * @return
	 */
	public ParameterModels getModels(String name, boolean dummy) {
		AParameter<?> p = get(name);

		if (p instanceof ParameterModels)
			return (ParameterModels) p;

		logger.warning("ParameterSet '" + getName() + "': Parameter '" + name + "' of type 'Models' not found!");
		return null;
	}
	
	/**
	 * @param name
	 * @return
	 */
	public ArrayList<APhysicalComponent> getModels(String name) {
		AParameter<?> p = get(name);

		if (p instanceof ParameterModels)
			return ((ParameterModels) p).getModels();

		logger.warning("ParameterSet '" + getName() + "': Parameter '" + name + "' of type 'Models' not found!");
		return null;
	}

	/**
	 * @param name
	 * @return
	 */
	public Duct getDuct(String name) {
		AParameter<?> p = get(name);

		if (p instanceof ParameterDuct)
			return (Duct) p.getValue();

		logger.warning("ParameterSet '" + getName() + "': Parameter '" + name + "' of type 'Duct' not found!");
		return null;
	}

	/**
	 * return the whole map of parameters
	 * 
	 * @return
	 */
	public List<AParameter<?>> getParameterSet() {
		return set;
	}

	/**
	 * Returns a list containing all the parameter names
	 * 
	 * @return
	 */
	public ArrayList<String> getNames() {
		ArrayList<String> ret = new ArrayList<>();

		for (AParameter<?> p : set)
			ret.add(p.getName());

		return ret;
	}

	/**
	 * @param path
	 * @return
	 */
	public static ParameterSet load(String path) {
		ParameterSet ps = null;
		try {
			JAXBContext context = JAXBContext.newInstance(ParameterSet.class);
			Unmarshaller um = context.createUnmarshaller();
			ps = (ParameterSet) um.unmarshal(new FileReader(path));
		} catch (Exception e) {
			logger.warning(
					"Failed  to load parameter-set '" + path + "': File does not exist or has the wrong format.");
			e.printStackTrace();
		}

		return ps;
	}

	/**
	 * @param path
	 */
	public void save(String path) {
		try {
			JAXBContext context = JAXBContext.newInstance(ParameterSet.class);
			Marshaller m = context.createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

			Writer w = new FileWriter(path);
			m.marshal(this, w);
			w.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof ParameterSet) {
			// Different number of keys --> can't be equal
			if (getNames().size() != ((ParameterSet) obj).getNames().size())
				return false;
			// Check each entry for equality
			for (String s : getNames()) {
				if (!get(s).equals(((ParameterSet) obj).get(s)))
					return false;
			}

			return true;
		}

		return super.equals(obj);
	}

	public ParameterSet clone() {
		ParameterSet clone = new ParameterSet(getName());
		clone.setComment(getComment());

		for (AParameter<?> p : set)
			clone.set(p.clone());

		return clone;
	}

}
