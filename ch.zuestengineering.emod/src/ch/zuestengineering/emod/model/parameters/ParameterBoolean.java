/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.model.parameters;

/**
 * @author sizuest
 *
 */
public class ParameterBoolean extends AParameter<Boolean> {
	/**
	 * 
	 */
	public ParameterBoolean() {
		// For unmarshaller
	}

	/**
	 * @param name
	 * @param value
	 */
	public ParameterBoolean(String name, Boolean value) {
		super(name, value);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.zuestengineering.emod.model.parameters.AParameter#getValueAsString()
	 */
	@Override
	public String getValueAsString() {
		return value.toString();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.zuestengineering.emod.model.parameters.AParameter#clone()
	 */
	@Override
	public AParameter<Boolean> clone() {
		ParameterBoolean clone = new ParameterBoolean(getName(), getValue());
		return clone;
	}

}
