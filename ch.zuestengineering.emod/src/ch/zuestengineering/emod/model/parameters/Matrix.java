/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.model.parameters;

import java.util.logging.Logger;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author sizuest
 *
 */
@XmlRootElement
public class Matrix implements Cloneable {

	protected Logger logger = Logger.getLogger(Matrix.class.getName());

	@XmlElement
	private double[][] data;

	/**
	 * 
	 */
	public Matrix() {
		// For unmarshaller
	}

	/**
	 * @param n
	 * @param m
	 */
	public Matrix(int n, int m) {
		data = new double[n][m];
	}

	/**
	 * @param data
	 */
	public Matrix(double[][] data) {
		this.data = data;
	}

	/**
	 * @return
	 */
	public double[][] get() {
		return data;
	}

	/**
	 * @param data
	 */
	public void set(double[][] data) {
		this.data = data;
	}

	/**
	 * @return
	 */
	public int getRowCount() {
		return data.length;
	}

	/**
	 * @return
	 */
	public int getColCount() {
		return data[0].length;
	}

	/**
	 * @param i
	 * @param j
	 * @return
	 */
	public double get(int i, int j) {
		if (i >= getRowCount() | j >= getColCount()) {
			logger.severe("Matrix: Element " + i + "/" + j + " out of bounds!");
			return Double.NaN;
		}

		return data[i][j];
	}

	public boolean equals(Object obj) {
		if (obj instanceof Matrix) {
			if (getRowCount() != ((Matrix) obj).getRowCount() | getColCount() != ((Matrix) obj).getColCount())
				return false;

			for (int i = 0; i < getRowCount(); i++)
				for (int j = 0; j < getColCount(); j++)
					if (get(i, j) != ((Matrix) obj).get(i, j))
						return false;

			return true;

		}
		return super.equals(obj);
	}

	public String toString() {
		String ret = "";
		for (int i = 0; i < getRowCount(); i++)
			for (int j = 0; j < getColCount(); j++) {
				ret += "" + data[i][j];
				if (j + 1 < getColCount())
					ret += ", ";
				else
					ret += "; ";
			}

		return ret;
	}

	/**
	 * @param i
	 * @param j
	 * @param value
	 */
	public void set(int i, int j, double value) {
		if (i >= getRowCount() | j >= getColCount()) {
			logger.severe("Matrix: Element " + i + "/" + j + " out of bounds!");
			return;
		}

		data[i][j] = value;
	}

	@Override
	public Matrix clone() {
		double clone[][] = new double[data.length][data[0].length];

		for (int i = 0; i < data.length; i++)
			for (int j = 0; j < data[i].length; j++)
				clone[i][j] = data[i][j];

		return new Matrix(clone);
	}
}
