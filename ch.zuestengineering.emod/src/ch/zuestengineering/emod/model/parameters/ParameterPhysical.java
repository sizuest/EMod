/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.model.parameters;

/**
 * @author sizuest
 *
 */
public class ParameterPhysical extends AParameter<PhysicalValue> {

	/**
	 * 
	 */
	public ParameterPhysical() {
		// For unmarshaller
	}

	/**
	 * @param name
	 * @param value
	 */
	public ParameterPhysical(String name, PhysicalValue value) {
		super(name, value);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.zuestengineering.emod.model.parameters.AParameter#getValueAsString()
	 */
	@Override
	public String getValueAsString() {
		return value.toString();
	}

	/**
	 * @param text
	 */
	public void setValue(String text) {
		String[] parts = text.split(",");
		double[] value = new double[parts.length];

		for (int i = 0; i < value.length; i++)
			value[i] = Double.valueOf(parts[i]);

		getValue().setValue(value);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.zuestengineering.emod.model.parameters.AParameter#clone()
	 */
	@Override
	public AParameter<PhysicalValue> clone() {
		ParameterPhysical clone = new ParameterPhysical(getName(), getValue().clone());
		return clone;
	}

}
