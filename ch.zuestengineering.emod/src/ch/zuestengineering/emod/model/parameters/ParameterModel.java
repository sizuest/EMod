/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.model.parameters;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlTransient;

import ch.zuestengineering.emod.model.APhysicalComponent;

/**
 * @author sizuest
 *
 */
@XmlRootElement
@XmlSeeAlso(MachineComponentString.class)
public class ParameterModel extends AParameter<MachineComponentString> {
	@XmlElement
	private String filter = "";
	

	/**
	 * 
	 */
	public ParameterModel() {
		// For unmarshaller
	}

	/**
	 * @param name
	 * @param value
	 */
	public ParameterModel(String name, MachineComponentString value) {
		super(name, value);
	}

	/**
	 * @param name
	 * @param model
	 * @param type
	 */
	public ParameterModel(String name, String model, String type) {
		super(name, new MachineComponentString(model, type));
	}

	/**
	 * @param value
	 */
	public void set(MachineComponentString value) {
		if (filter.equals("") | value.MODEL.matches(filter))
			super.setValue(value);
	}

	/**
	 * Returns the parametrized model
	 * 
	 * @return
	 */
	public APhysicalComponent getModel() {
		return value.createMachineComponent();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.zuestengineering.emod.model.parameters.AParameter#getValueAsString()
	 */
	@Override
	public String getValueAsString() {
		return value.toString();
	}

	/**
	 * @return the filter
	 */
	@XmlTransient
	public String getFilter() {
		return filter;
	}

	/**
	 * @param filter the filter to set
	 */
	public void setFilter(String filter) {
		this.filter = filter;
	}

	/**
	 * @param text
	 */
	public void setValue(String text) {
		set(new MachineComponentString(text));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.zuestengineering.emod.model.parameters.AParameter#clone()
	 */
	@Override
	public AParameter<MachineComponentString> clone() {
		ParameterModel clone = new ParameterModel(getName(), getValue().MODEL, getValue().TYPE);
		clone.setFilter(getFilter());
		return clone;
	}

}
