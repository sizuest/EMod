/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.model.parameters;

import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlRootElement;

import ch.zuestengineering.emod.model.material.Material;

/**
 * @author sizuest
 *
 */
@XmlRootElement
public class ParameterMaterial extends AParameter<String> {
	private Material material;

	/**
	 * 
	 */
	public ParameterMaterial() {
		// For unmarshaller
	}

	/**
	 * @param name
	 * @param value
	 */
	public ParameterMaterial(String name, Material value) {
		super(name, value.getType());
		this.material = value;
	}

	/**
	 * @param name
	 * @param value
	 */
	public ParameterMaterial(String name, String value) {
		super(name, value);
		initMaterial();
	}

	/**
	 * @param u
	 * @param parent
	 */
	public void afterUnmarshal(Unmarshaller u, Object parent) {
		initMaterial();
	}

	/**
	 * @param value
	 */
	public void setValue(Material value) {
		this.material = value;
		setValue(value.getType());
	}

	/**
	 * @return
	 */
	public Material getMaterial() {
		return material;
	}

	@Override
	public void setValue(String value) {
		super.setValue(value);
		initMaterial();
	}

	private void initMaterial() {
		material = new Material(getValue());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.zuestengineering.emod.model.parameters.AParameter#getValueAsString()
	 */
	@Override
	public String getValueAsString() {
		return value.toString();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.zuestengineering.emod.model.parameters.AParameter#clone()
	 */
	@Override
	public AParameter<String> clone() {
		ParameterMaterial clone = new ParameterMaterial(getName(), getValue());
		return clone;
	}

}
