/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.model.parameters;

import ch.zuestengineering.emod.dd.Duct;

/**
 * @author sizuest
 *
 */
public class ParameterDuct extends AParameter<Duct> {

	/**
	 * 
	 */
	public ParameterDuct() {
		// For unmarshaller
	}

	/**
	 * @param name
	 * @param value
	 */
	public ParameterDuct(String name, Duct value) {
		super(name, value);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.zuestengineering.emod.model.parameters.AParameter#getValueAsString()
	 */
	@Override
	public String getValueAsString() {
		return "Duct";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.zuestengineering.emod.model.parameters.AParameter#clone()
	 */
	@Override
	public AParameter<Duct> clone() {
		ParameterDuct clone = new ParameterDuct(getName(), getValue().clone());
		return clone;
	}

}
