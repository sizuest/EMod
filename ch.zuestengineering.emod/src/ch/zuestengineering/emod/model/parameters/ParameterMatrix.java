/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.model.parameters;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;

/**
 * @author sizuest
 *
 */
@XmlRootElement
@XmlSeeAlso(ArrayList.class)
public class ParameterMatrix extends AParameter<Matrix> {
	// @XmlElement
	// private ArrayList<ArrayList<Double>> value;

	/**
	 * 
	 */
	public ParameterMatrix() {
		// For unmarshaller
	}

	/**
	 * @param name
	 * @param value
	 */
	public ParameterMatrix(String name, Matrix value) {
		super(name, value);
		this.value = value;
	}

	/**
	 * @param name
	 * @param value
	 */
	public ParameterMatrix(String name, double[][] value) {
		super(name, new Matrix(value));
		// this.value = value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.zuestengineering.emod.model.parameters.AParameter#getValueAsString()
	 */
	@Override
	public String getValueAsString() {
		return value.toString();
	}

	/**
	 * @param text
	 */
	public void setValue(String text) {

		text = text.trim();

		String[] rows = text.split(";");

		int n, m;
		n = rows.length;
		m = 0;
		for (int i = 0; i < n; i++)
			if (rows[i].split(",").length > m)
				m = rows[i].split(",").length;

		Matrix value = new Matrix(n, m);

		for (int i = 0; i < n; i++) {
			String[] col = rows[i].split(",");
			for (int j = 0; j < col.length; j++)
				value.set(i, j, Double.valueOf(col[j]));
		}

		setValue(value);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.zuestengineering.emod.model.parameters.AParameter#clone()
	 */
	@Override
	public AParameter<Matrix> clone() {
		ParameterMatrix clone = new ParameterMatrix(getName(), getValue().clone());
		return clone;
	}

}
