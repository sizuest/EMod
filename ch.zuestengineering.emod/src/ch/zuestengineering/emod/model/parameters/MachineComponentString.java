/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.model.parameters;

import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import ch.zuestengineering.emod.Machine;
import ch.zuestengineering.emod.model.APhysicalComponent;
import ch.zuestengineering.emod.model.lc.LCProperties;

/**
 * Represents a machine component as a combination of two strings (model and
 * type)
 * 
 * @author sizuest
 *
 */
@XmlRootElement
public class MachineComponentString implements Cloneable {
	/**
	 * The model name (class name)
	 */
	@XmlElement
	public String MODEL;
	/**
	 * The type (parameter set)
	 */
	@XmlElement
	public String TYPE;
	
	@XmlElement
	private ParameterSet parameters;
	@XmlElement
	private LCProperties lcdata;

	/**
	 * 
	 */
	public MachineComponentString() {
		// For unmarshaller
	}
	
	/**
	 * post xml init method (loading physics data)
	 * 
	 * @param u
	 * @param parent
	 */
	public void afterUnmarshal(final Unmarshaller u, final Object parent) {
		updateParameters(createMachineComponent());
	}

	/**
	 * @param model
	 */
	public MachineComponentString(APhysicalComponent model) {
		if(model==null) {
			MODEL = "";
			TYPE = "";
		}
		else {
			MODEL = model.getModelType();
			TYPE = model.getType();
		}
		
		updateParameters(model);
	}

	/**
	 * @param model
	 * @param type
	 */
	public MachineComponentString(String model, String type) {
		MODEL = model;
		TYPE = type;
		
		updateParameters(createMachineComponent());
	}

	/**
	 * @param text
	 */
	public MachineComponentString(String text) {
		if(text.split("_", 2).length!=2) {
			MODEL = "";
			TYPE = "";
		}
		else {
			MODEL = text.split("_", 2)[0].trim();
			TYPE = text.split("_", 2)[1].trim();
		}
		
		updateParameters(createMachineComponent());
	}
	
	private void updateParameters(APhysicalComponent model) {
		if(null == model) {
			parameters = new ParameterSet();
			lcdata = new LCProperties();
		}
		else {
			parameters = model.getParameterSet();
			lcdata = model.getLcProperties();
		}
	}

	public String toString() {
		if(isEmpty())
			return "";
		
		return MODEL + "_" + TYPE;
	}

	/**
	 * Creates the component
	 * 
	 * @return
	 */
	public APhysicalComponent createMachineComponent() {
		if(isEmpty())
			return null;
		
		APhysicalComponent model = Machine.createNewMachineComponent(MODEL, TYPE);
		
		if(null == model) {
			model = Machine.createNewMachineComponent(MODEL, "Example");
			model.setType(TYPE);
			model.setLibraryLink(false);
			model.setParameterSet(parameters);
			model.setLcProperties(lcdata);
			model.loadParameters();
		}
		
		return model;
	}

	@Override
	public MachineComponentString clone() {
		MachineComponentString clone = new MachineComponentString(MODEL, TYPE);
		return clone;
	}
	
	private boolean isEmpty() {
		return MODEL.equals("") | TYPE.equals("");
	}
}
