/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.model.parameters;

/**
 * Interface for a parametrizable (physical parameters!) class
 * 
 * @author sizuest
 * 
 */

public interface Parameterizable {

	/**
	 * Return the parameter set
	 * 
	 * @return {@link ParameterSet}
	 */

	public ParameterSet getParameterSet();

	/**
	 * Write the parameter set given as ps
	 * 
	 * @param ps {@link ParameterSet}
	 */

	public void setParameterSet(ParameterSet ps);

}
