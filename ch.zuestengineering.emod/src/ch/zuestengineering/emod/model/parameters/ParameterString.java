/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.model.parameters;

/**
 * @author sizuest
 *
 */
public class ParameterString extends AParameter<String> {

	/**
	 * 
	 */
	public ParameterString() {
		// For unmarshaller
	}

	/**
	 * @param name
	 * @param value
	 */
	public ParameterString(String name, String value) {
		super(name, value);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.zuestengineering.emod.model.parameters.AParameter#getValueAsString()
	 */
	@Override
	public String getValueAsString() {
		return value.toString();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.zuestengineering.emod.model.parameters.AParameter#clone()
	 */
	@Override
	public AParameter<String> clone() {
		ParameterString clone = new ParameterString(getName(), getValue());
		return clone;
	}

}
