/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.model.parameters;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * @author sizuest
 * @param <T>
 *
 */
@XmlRootElement
public abstract class AParameter<T> implements Cloneable {
	@XmlElement
	protected String name;
	@XmlElement
	protected String description;
	@XmlElement
	protected T value;

	/**
	 * Constructor for unmarshaller
	 */
	public AParameter() {
		// Empty, only used from unmarshaller
	}

	/**
	 * @param name
	 * @param value
	 */
	public AParameter(String name, T value) {
		this.name = name;
		this.value = value;
	}

	/**
	 * @return the name
	 */
	@XmlTransient
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the value
	 */
	@XmlTransient
	public T getValue() {
		return value;
	}

	/**
	 * @param value the value to set
	 */
	public void setValue(T value) {
		this.value = value;
	}

	public boolean equals(Object obj) {
		if (obj instanceof AParameter) {
			return value.equals(((AParameter<?>) obj).value);
		}

		return super.equals(obj);
	}

	/**
	 * @return the description
	 */
	@XmlTransient
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return
	 */
	public abstract String getValueAsString();

	public abstract AParameter<T> clone();

	/**
	 * toString
	 * @param sep
	 * @return
	 */
	public String toString(String sep) {
		if(value instanceof PhysicalValue)
			return name+sep+((PhysicalValue) value).getUnit().toString()+sep+((PhysicalValue) value).getValueString();
		
		return name+sep+""+sep+getValueAsString();
	}

}
