/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.model.parameters;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author sizuest
 *
 */
@XmlRootElement
public class ParameterVector extends AParameter<double[]> {

	/**
	 * 
	 */
	public ParameterVector() {
		// For Unmarshaller
	}

	/**
	 * @param name
	 * @param value
	 */
	public ParameterVector(String name, double[] value) {
		super(name, value);
		this.value = value;
	}

	public boolean equals(Object obj) {
		if (obj instanceof ParameterVector) {
			if (value.length != ((ParameterVector) obj).value.length)
				return false;

			for (int i = 0; i < value.length; i++) {
				if (value[i] != ((ParameterVector) obj).value[i])
					return false;
			}

			return true;
		}

		return super.equals(obj);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.zuestengineering.emod.model.parameters.AParameter#getValueAsString()
	 */
	@Override
	public String getValueAsString() {
		String ret = "";

		for (double d : value)
			ret += d + ", ";

		ret.substring(0, ret.length() - 2);

		return ret;
	}

	/**
	 * @param text
	 */
	public void setValue(String text) {
		text = text.trim();

		String[] parts = text.split(",");
		double[] value = new double[parts.length];

		for (int i = 0; i < value.length; i++)
			value[i] = Double.valueOf(parts[i]);

		setValue(value);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.zuestengineering.emod.model.parameters.AParameter#clone()
	 */
	@Override
	public AParameter<double[]> clone() {
		ParameterVector clone = new ParameterVector(getName(), getValue().clone());
		return clone;
	}

}
