/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.model.parameters;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlTransient;

import ch.zuestengineering.emod.model.APhysicalComponent;

/**
 * @author sizuest
 *
 */
@XmlRootElement
@XmlSeeAlso(MachineComponentString.class)
public class ParameterModels extends AParameter<ArrayList<MachineComponentString>> {
	@XmlElement
	private ArrayList<MachineComponentString> value2;
	@XmlElement
	private String filter = "";

	/**
	 * 
	 */
	public ParameterModels() {
		// for unmarshaller
	}

	/**
	 * @param name
	 * @param models
	 */
	public ParameterModels(String name, ArrayList<APhysicalComponent> models) {
		super(name, null);

		ArrayList<MachineComponentString> names = new ArrayList<>();
		for (APhysicalComponent p : models)
			names.add(new MachineComponentString(p));

		setValue(names);
	}

	public void setValue(ArrayList<MachineComponentString> value) {
		this.value2 = value;
	}

	@XmlTransient
	public ArrayList<MachineComponentString> getValue() {
		if(this.value2 == null)
			return new ArrayList<MachineComponentString>();
		
		return this.value2;
	}

	/**
	 * @return
	 */
	public ArrayList<APhysicalComponent> getModels() {
		ArrayList<APhysicalComponent> ret = new ArrayList<>();

		if (null == getValue())
			return ret;

		for (MachineComponentString s : getValue())
			ret.add(s.createMachineComponent());

		return ret;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.zuestengineering.emod.model.parameters.AParameter#getValueAsString()
	 */
	@Override
	public String getValueAsString() {
		String ret = "";

		for (MachineComponentString s : value2)
			ret += s.toString() + ", ";

		ret = ret.substring(0, ret.length() - 2);

		return ret;
	}

	/**
	 * @return the filter
	 */
	@XmlTransient
	public String getFilter() {
		return filter;
	}

	/**
	 * @param filter the filter to set
	 */
	public void setFilter(String filter) {
		this.filter = filter;
	}

	/**
	 * @param text
	 */
	public void setValue(String text) {
		text = text.trim();

		String[] parts = text.split(",");
		ArrayList<MachineComponentString> value = new ArrayList<>();

		for (int i = 0; i < parts.length; i++)
			value.add(new MachineComponentString(parts[i]));

		setValue(value);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.zuestengineering.emod.model.parameters.AParameter#clone()
	 */
	@Override
	public AParameter<ArrayList<MachineComponentString>> clone() {
		ArrayList<MachineComponentString> models = new ArrayList<>();
		for (MachineComponentString mc : getValue())
			if(null==mc)
				models.add(null);
			else
				models.add(mc.clone());

		ParameterModels clone = new ParameterModels();
		clone.setName(getName());
		clone.setValue(models);
		clone.setFilter(getFilter());
		return clone;
	}

}
