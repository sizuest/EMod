/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.model.parameters;

/**
 * @author sizuest
 *
 */
public class ParameterNumeric extends AParameter<Double> {

	/**
	 * 
	 */
	public ParameterNumeric() {
		// For unmarshaller
	}

	/**
	 * @param name
	 * @param value
	 */
	public ParameterNumeric(String name, Double value) {
		super(name, value);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.zuestengineering.emod.model.parameters.AParameter#getValueAsString()
	 */
	@Override
	public String getValueAsString() {
		return value.toString();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.zuestengineering.emod.model.parameters.AParameter#clone()
	 */
	@Override
	public AParameter<Double> clone() {
		ParameterNumeric clone = new ParameterNumeric(getName(), getValue());
		return clone;
	}

}
