/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.model.units;

import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * SI unit class
 * 
 * Implements a representation of an SiUnit based on the seven base units
 * 
 * @author sizuest
 * 
 */
@XmlRootElement
public class SiUnit implements Cloneable {
	// Exponents of the dimensions
	private double L = 0, // length
			M = 0, // mass
			T = 0, // time
			I = 0, // current
			theta = 0, // temperature
			N = 0, // quantity
			J = 0; // light intensity

	private int prefix = 0;
	@XmlElement
	private String unitText = "";

	/**
	 * Constructor for unmarshaller
	 */
	public SiUnit() {
	}

	/**
	 * Used of compability issues with old unit definition (as ENUM)
	 * @param u
	 */
	public SiUnit(Unit u) {
		SiUnit su = SiUnitDefinition.getUpdateMap().get(u);
		this.set(su.get());
	}

	/**
	 * @param s
	 */
	public SiUnit(String s) {
		this.set(s);
	}

	/**
	 * If the nonSi flag is set, the unit provided as String will be taken as it is and
	 * the exponents for the base units are ignored
	 * 
	 * @param s
	 * @param nonSi
	 */
	public SiUnit(String s, boolean nonSi) {
		if (!nonSi)
			this.set(s);
		else {
			this.set(Double.NaN, Double.NaN, T, Double.NaN, Double.NaN, Double.NaN, Double.NaN);
			this.setPrefix(0);
			this.unitText = s;
		}
	}

	/**
	 * The array must include the exponents of the seven base units in the following order:
	 * length, mass, time, current, temperature, quantity, light intensity
	 * 
	 * @param e
	 */
	public SiUnit(double[] e) {
		this.set(e);
	}

	/**
	 * @param e
	 * @param p
	 */
	public SiUnit(double[] e, int p) {
		this.set(e);
		this.prefix = p;
	}

	/**
	 * called after the unmarshaller
	 * 
	 * @param u
	 * @param parent
	 */
	public void afterUnmarshal(Unmarshaller u, Object parent) {
		this.set(this.unitText.replace(" (!)", ""));
	}

	/**
	 * @param L
	 * @param M
	 * @param T
	 * @param I
	 * @param theta
	 * @param N
	 * @param J
	 */
	public SiUnit(double L, double M, double T, double I, double theta, double N, double J) {
		this.set(L, M, T, I, theta, N, J);
	}

	/**
	 * Sets the unit according the the exponents of the seven basic dimensions
	 * 
	 * @param L
	 * @param M
	 * @param T
	 * @param I
	 * @param theta
	 * @param N
	 * @param J
	 */
	public void set(double L, double M, double T, double I, double theta, double N, double J) {
		this.L = L;
		this.M = M;
		this.T = T;
		this.I = I;
		this.theta = theta;
		this.N = N;
		this.J = J;

		this.prefix = 0;


		this.unitText = toString().replace(" (!)", "");
	}

	/**
	 * Sets the unit according to the stated string REQUIRED FORMAT: x^i y^-j, where
	 * x and y are the units of the basic dimensions and i and j are the exponents.
	 * Example: kg m^-2
	 * 
	 * @param s
	 */
	public void set(String s) {
		this.set(SiUnitDefinition.convertToBaseUnit(s));
		unitText = s.replace(" (!)", "").trim();
	}

	/**
	 * Sets the unit according to the exponents in array u
	 * 
	 * @param u
	 */
	public void set(double[] u) {
		this.L = u[0];
		this.M = u[1];
		this.T = u[2];
		this.I = u[3];
		this.theta = u[4];
		this.N = u[5];
		this.J = u[6];
		this.prefix = 0;

		this.unitText = toString().replace(" (!)", "");
	}

	/**
	 * @param unit
	 */
	public void set(SiUnit unit) {
		this.set(unit.get());
		this.prefix = unit.prefix;
	}

	/**
	 * Returns the exponents as an array
	 * 
	 * @return array of exponents
	 */
	public double[] get() {
		double[] u = { this.L, this.M, this.T, this.I, this.theta, this.N, this.J };
		return u;
	}

	/**
	 * Returns the prefix
	 * 
	 * @return
	 */
	public int getPrefix() {
		return this.prefix;
	}

	/**
	 * @param k
	 */
	public void setPrefix(int k) {
		this.prefix = k;
	}

	/**
	 * @param a First operand
	 * @param b Second operand
	 * @return Multiplication of the units
	 */
	public static SiUnit multiply(SiUnit a, SiUnit b) {
		SiUnit su = new SiUnit();
		su.L = a.L + b.L;
		su.M = a.M + b.M;
		su.T = a.T + b.T;
		su.I = a.I + b.I;
		su.theta = a.theta + b.theta;
		su.N = a.N + b.N;
		su.J = a.J + b.J;
		return su;
	}

	/**
	 * @param a Nominator
	 * @param b Denominator
	 * @return Division of the units
	 */
	public static SiUnit divide(SiUnit a, SiUnit b) {
		SiUnit su = new SiUnit();
		su.L = a.L - b.L;
		su.M = a.M - b.M;
		su.T = a.T - b.T;
		su.I = a.I - b.I;
		su.theta = a.theta - b.theta;
		su.N = a.N - b.N;
		su.J = a.J - b.J;
		return su;
	}

	/**
	 * @param a basis
	 * @param e exponent
	 * @return
	 */
	public static SiUnit pow(SiUnit a, double e) {
		SiUnit su = new SiUnit();
		su.L = a.L * e;
		su.M = a.M * e;
		su.T = a.T * e;
		su.I = a.I * e;
		su.theta = a.theta * e;
		su.N = a.N * e;
		su.J = a.J * e;

		return su;
	}

	/**
	 * @return true if units are equal
	 */
	@Override
	public boolean equals(Object o) {
		boolean nonSi = false;
		for (int i = 0; i < this.get().length; i++)
			nonSi = nonSi | (Double.isNaN(this.get()[i]));
		if (nonSi & o instanceof SiUnit) {
			return unitText.equals(((SiUnit) o).unitText);
		}

		try {
			SiUnit u = (SiUnit) o;
			if (this.L == u.L & this.M == u.M & this.T == u.T & this.I == u.I & this.theta == u.theta & this.N == u.N
					& this.J == u.J)
				return true;
			else
				return false;
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * @return String representation of the unit
	 */
	@Override
	public String toString() {
		return SiUnitDefinition.getString(this);
	}

	/**
	 * Returns a readable version of the unit as HTML
	 * 
	 * @return
	 */
	public String toHTML() {
		return toString().replaceAll("[\\^]{1}([-+]?[0-9]+)", "<sup>$1</sup>");
	}

	/**
	 * Returns a si save string, non si units are marked with (!).
	 * 
	 * @return
	 */
	public String toSaveString() {

		String out = SiUnitDefinition.getString(this);

		/* Non Si Unit */
		boolean nonSi = false;
		for (int i = 0; i < this.get().length; i++)
			nonSi = nonSi | (Double.isNaN(this.get()[i]));
		if (nonSi) {
			out += " (!)";
		}

		return out;
	}

	/**
	 * @return
	 */
	public String getUnitName() {
		return unitText;
	}

	@Override
	public SiUnit clone() {
		SiUnit clone = new SiUnit(this.toString());
		return clone;
	}

}
