/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.model.units;

import ch.zuestengineering.emod.model.parameters.PhysicalValue;

/**
 * Implements varoius SI constants
 * 
 * @author sizuest
 * 
 */
public class SiConstants {
	/**
	 * R: Universal gas constant [J/mol/K]
	 */
	public static final PhysicalValue R = new PhysicalValue(8.3144598, new SiUnit("J mol^-1 K^-1"));
	/**
	 * g: Normal acceleration due to gravitation [m/s²]
	 */
	public static final PhysicalValue g = new PhysicalValue(9.81, new SiUnit("m s^-2"));
	/**
	 * kB: Boltzmann constant [J/K]
	 */
	public static final PhysicalValue kB = new PhysicalValue(1.38064852E-23, new SiUnit("J/K"));
	/**
	 * NA: Avogadro Zahl [-]
	 */
	public static final PhysicalValue NA = new PhysicalValue(6.022E23, new SiUnit(""));

}
