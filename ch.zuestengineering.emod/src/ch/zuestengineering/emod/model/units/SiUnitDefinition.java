/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.model.units;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Implementation of the SI definitions
 * 
 * @author sizuest
 * 
 */
public class SiUnitDefinition {

	// SI definition object
	private static SiUnitDefinition siDefinition = null;
	// Conversion Map
	private Map<String, SiUnit> convMap = new HashMap<String, SiUnit>();
	private Map<Unit, SiUnit> updateMap = new HashMap<Unit, SiUnit>();
	private Map<String, Integer> prefixMap = new HashMap<String, Integer>();
	// Units of the dimensions
	private String[] unitNames = { "m", "kg", "s", "A", "K", "mol", "cd" };

	/**
	 * Private constructor for singelton implementation
	 */
	private SiUnitDefinition() {
	}

	/**
	 * singleton implementation of the SI unit definition
	 * 
	 * @return instance of the SI defintions
	 */
	public static SiUnitDefinition getInstance() {
		if (siDefinition == null) {
			siDefinition = new SiUnitDefinition();
			getInstance().initPrefixMap();
			getInstance().initConversionMap();
			getInstance().initUpdateMap();
		}
		return siDefinition;

	}

	/**
	 * @return instance of the conversion map
	 */
	public static Map<String, SiUnit> getConversionMap() {
		return getInstance().convMap;
	}

	/**
	 * @return instance of the conversion map
	 */
	public static Map<Unit, SiUnit> getUpdateMap() {
		return getInstance().updateMap;
	}

	private void initUpdateMap() {
		updateMap.put(Unit.KELVIN, (new SiUnit("K")));
		updateMap.put(Unit.KG, (new SiUnit("kg")));
		updateMap.put(Unit.KG_MCUBIC, (new SiUnit("kg m^-3")));
		updateMap.put(Unit.KG_S, (new SiUnit("kg s^-1")));
		updateMap.put(Unit.M, (new SiUnit("m")));
		updateMap.put(Unit.M_S, (new SiUnit("m s^-1")));
		updateMap.put(Unit.METERCUBIC, (new SiUnit("m^3")));
		updateMap.put(Unit.METERCUBIC_S, (new SiUnit("m^3 s^-1")));
		updateMap.put(Unit.NEWTON, (new SiUnit("N")));
		updateMap.put(Unit.NEWTONMETER, (new SiUnit("Nm")));
		updateMap.put(Unit.NONE, (new SiUnit("")));
		updateMap.put(Unit.PA, (new SiUnit("Pa")));
		updateMap.put(Unit.S, (new SiUnit("s")));
		updateMap.put(Unit.WATT, (new SiUnit("W")));
		updateMap.put(Unit.REVOLUTIONS_S, (new SiUnit("Hz")));
		updateMap.put(Unit.RPM, (new SiUnit("Hz")));
	}

	private void initConversionMap() {
		// Base units
		// Have to be stated at very first in exponential representation!
		convMap.put("m", (new SiUnit(1, 0, 0, 0, 0, 0, 0)));
		convMap.put("kg", (new SiUnit(0, 1, 0, 0, 0, 0, 0)));
		convMap.put("s", (new SiUnit(0, 0, 1, 0, 0, 0, 0)));
		convMap.put("A", (new SiUnit(0, 0, 0, 1, 0, 0, 0)));
		convMap.put("K", (new SiUnit(0, 0, 0, 0, 1, 0, 0)));
		convMap.put("mol", (new SiUnit(0, 0, 0, 0, 0, 1, 0)));
		convMap.put("cd", (new SiUnit(0, 0, 0, 0, 0, 0, 1)));
		// Additional SI units
		convMap.put("none", (new SiUnit()));
		convMap.put("J", (new SiUnit("W s")));
		convMap.put("Hz", (new SiUnit("s^-1")));
		convMap.put("N", (new SiUnit("m kg s^-2")));
		convMap.put("Pa", (new SiUnit("kg m^-1 s^-2")));
		convMap.put("Nm", (new SiUnit("N m")));
		convMap.put("m/s", (new SiUnit("m s^-1")));
		convMap.put("m^3/s", (new SiUnit("m^3 s^-1")));
		convMap.put("W/K", (new SiUnit("W K^-1")));
		convMap.put("W/m^2/K", (new SiUnit("W m^-2 K^-1")));
		convMap.put("W", (new SiUnit("m^2 kg s^-3")));
		convMap.put("C", (new SiUnit("A s")));
		convMap.put("V", (new SiUnit("m^2 kg s^-3 A^-1")));
		convMap.put("Ohm", (new SiUnit("m^2 kg s^-3 A^-2")));
		convMap.put("S", (new SiUnit("m^-2 kg ^-1 s^3 A^2")));
		convMap.put("Wb", (new SiUnit("m^2 kg s^-2 A^-1")));
		convMap.put("T", (new SiUnit("kg s^-2 A^-1")));
		convMap.put("H", (new SiUnit("m^2 kg s^-2 A^-2")));
		convMap.put("lm", (new SiUnit("cd")));
		convMap.put("lx", (new SiUnit("m^-2 cd")));
		convMap.put("Gy", (new SiUnit("m^2 s^-2")));
		convMap.put("Sv", (new SiUnit("m^2 s^-2")));
		convMap.put("kat", (new SiUnit("mol s^-1")));
		// Nice outputs
		convMap.put("kg/m^3", (new SiUnit("kg m^-3")));
		convMap.put("J/kg/K", (new SiUnit("J kg^-1 K^-1")));
		convMap.put("N/m", (new SiUnit("N m^-1")));
		convMap.put("W/m/K", (new SiUnit("W m^-1 K^-1")));
		convMap.put("s^2 Pa/m^6", (new SiUnit("Pa s^2 m^-6")));
		convMap.put("Pas", (new SiUnit("Pa s")));
		// No SI-Unit
		convMap.put("rpm", new SiUnit("rpm", true));
		convMap.put("RPM", new SiUnit("rpm", true));
		convMap.put("deg", new SiUnit("deg", true));

	}

	private void initPrefixMap() {
		prefixMap.put("E", 18);
		prefixMap.put("P", 15);
		prefixMap.put("T", 12);
		prefixMap.put("G", 9);
		prefixMap.put("M", 6);
		prefixMap.put("k", 3);
		prefixMap.put("", 0);
		prefixMap.put("d", -1);
		prefixMap.put("c", -2);
		prefixMap.put("m", -3);
		prefixMap.put("u", -6);
		prefixMap.put("n", -9);
		prefixMap.put("p", -12);
		prefixMap.put("f", -15);
		prefixMap.put("a", -18);
	}

	/**
	 * Returns the multiplier corresponding to the stated SI-prefix
	 * 
	 * Example: getPrefixMultiplier("k") --> 10^3=1000
	 * 
	 * @param prefix
	 * @return
	 */
	public static double getPrefixMultiplier(String prefix) {
		return getPrefixMultiplier(getInstance().prefixMap.get(prefix));
	}

	/**
	 * Returns the multiplier corresponding to the stated SI-prefix
	 * 
	 * Example: getPrefixMultiplier("k") --> 10^3=1000
	 * 
	 * @param prefix
	 * @return
	 */
	public static double getPrefixMultiplier(int prefix) {
		return Math.pow(10, prefix);
	}

	private String getPrefixRegexPattern() {
		String out;

		String[] u = getInstance().prefixMap.keySet().toArray(new String[0]);

		u[0] = u[0].replaceAll("/", "\\\\/");
		out = "[" + u[0];
		for (int i = 1; i < u.length; i++) {
			out += u[i];
		}
		out += "]?";

		return out;
	}

	private String getUnitTextRegexPattern() {
		String out;

		String[] u = getConversionMap().keySet().toArray(new String[0]);

		u[0] = u[0].replaceAll("/", "\\\\/");
		out = "(" + u[0] + ")";
		for (int i = 1; i < u.length; i++) {
			if (!u[i].contains("/") & !u[i].contains("^")) {
				u[i] = u[i].replaceAll("/", "\\\\/");
				out = out + "|(" + u[i] + ")";
			}
		}

		return out;
	}

	private String getRegexPattern() {
		String out;

		String prefixes = getPrefixRegexPattern();

		String[] u = getConversionMap().keySet().toArray(new String[0]);

		u[0] = u[0].replaceAll("/", "\\\\/");
		out = "((" + prefixes + u[0] + "\\s)|(" + prefixes + u[0] + "$)|(" + prefixes + u[0] + "\\^-?\\d+))";
		for (int i = 1; i < u.length; i++) {
			u[i] = u[i].replaceAll("/", "\\\\/");
			out = out + "|((" + prefixes + u[i] + "\\s)|(" + prefixes + u[i] + "$)|(" + prefixes + u[i]
					+ "\\^[-]?\\d+))";
		}

		return out;
	}

	/**
	 * Returns the base units
	 * 
	 * @return Array with base units
	 */
	public static String[] getBaseUnits() {
		return getInstance().unitNames;
	}

	/**
	 * Unit to (nice) string
	 * 
	 * @param unit
	 * @return
	 */
	public static String getString(SiUnit unit) {
		String out = "";

		/* Non Si Unit */
		boolean nonSi = false;
		for (int i = 0; i < unit.get().length; i++)
			nonSi = nonSi | (Double.isNaN(unit.get()[i]));
		if (nonSi) {
			return unit.getUnitName();
		}

		/* Simplest case */
		boolean allZero = (0 == unit.get()[0]);
		for (int i = 1; i < unit.get().length; i++)
			allZero = allZero & (0 == unit.get()[i]);
		if (allZero)
			return "none";

		/* Test for simple representation */
		if (getInstance().convMap.containsValue(unit))
			for (String s : getInstance().convMap.keySet()) {
				if (getInstance().convMap.get(s).equals(unit)) {
					out = s;
					break;
				}
			}
		else {
			double[] exp = unit.get();

			for (int i = 0; i < exp.length; i++)
				if (0 != exp[i]) {
					out += SiUnitDefinition.getBaseUnits()[i]; // Add unit name
					if (1 != exp[i]) { // Add exponent if required
						if (0 == exp[i] % 1)
							out += "^" + (int) exp[i];
						else
							out += "^" + exp[i];
					}
					out += " ";

				}

			if (out.length() > 0)
				out = out.substring(0, out.length() - 1);
		}

		/* Add prefix */
		Pattern p = Pattern.compile("[a-zA-Z]+\\^([+-]?\\d+)");
		Matcher m = p.matcher(out);
		if (m.find()) {
			int pow = Integer.parseInt(m.group(1));
			out = getPrefixText(unit.getPrefix() / pow) + out;
		} else
			out = getPrefixText(unit.getPrefix()) + out;

		return out;
	}

	private static String extractPrefix(String s) {
		String out = "";

		String prefixPattern = getInstance().getPrefixRegexPattern();
		prefixPattern = prefixPattern.substring(0, prefixPattern.length() - 1);

		Pattern p = Pattern.compile(prefixPattern + "(" + getInstance().getUnitTextRegexPattern() + ")");
		Matcher m = p.matcher(s);

		if (m.find())
			out = s.substring(0, 1);

		return out;
	}

	/**
	 * @param prefix
	 * @return
	 */
	public static String getPrefixText(int prefix) {
		String out = "";

		for (String s : getInstance().prefixMap.keySet().toArray(new String[0])) {
			if (getInstance().prefixMap.get(s) == prefix) {
				out = s;
				break;
			}
		}

		return out;
	}

	/**
	 * Converts a SI unit to the SI base units E.g.: N -> kg m s^-2
	 * 
	 * @param s
	 * @return {@link SiUnit} objects
	 */
	public static SiUnit convertToBaseUnit(String s) {

		if (s.equals("")) {
			return new SiUnit();
		}

		s.replace(" ", "");

		double[] exp = { 0, 0, 0, 0, 0, 0, 0 };
		int prefix = 0;

		double[] expInner = exp;
		double expOut = 1;

		int prefixInner = 0;

		String subUnit;

		if (null != getInstance().convMap.get(s))
			return getInstance().convMap.get(s);

		// Search for numbers
		Pattern p = Pattern.compile(getInstance().getRegexPattern());
		Matcher m = p.matcher(s);

		// Prepare other search units
		Pattern pUnit = Pattern.compile("(" + getInstance().getPrefixRegexPattern() + "("
				+ getInstance().getUnitTextRegexPattern() + "))|([-+]?\\d)+");
		Matcher mUnit;

		boolean foundOne = false;

		while (m.find()) {
			foundOne = true;

			subUnit = m.group();

			mUnit = pUnit.matcher(subUnit);
			mUnit.find();

			// Get the corresponding base unit
			String tmp = mUnit.group();
			String tmpPrefix = extractPrefix(tmp);

			if (tmpPrefix.equals(""))
				prefixInner = 0;
			else {
				prefixInner = getInstance().prefixMap.get(tmpPrefix);
				tmp = tmp.substring(1);
			}

			expInner = getInstance().convMap.get(tmp).get();

			// Check if an exponent exists
			if (mUnit.find())
				expOut = Integer.valueOf(mUnit.group());
			else
				expOut = 1;

			// Add sub unit properties to global unit properties
			for (int i = 0; i < exp.length; i++)
				exp[i] += expOut * expInner[i];

			prefix += expOut * prefixInner;

		}

		if (!foundOne)
			return new SiUnit(Double.NaN, Double.NaN, Double.NaN, Double.NaN, Double.NaN, Double.NaN, Double.NaN);

		return (new SiUnit(exp, prefix));
	}

}
