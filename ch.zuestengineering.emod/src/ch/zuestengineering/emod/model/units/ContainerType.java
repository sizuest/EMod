/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.model.units;

/**
 * @author sizuest
 * 
 */
public enum ContainerType {
	/**
	 * For all electric power flows
	 */
	ELECTRIC,
	/**
	 * Mechanical power flows and variables
	 */
	MECHANIC,
	/**
	 * Thermal flows (power & temperature), suitable for FluidConnections
	 */
	THERMAL,
	/**
	 * Fluiddynamic power flows and variables, suitable for FluidConnections
	 */
	FLUIDDYNAMIC,
	/**
	 * Control signals
	 */
	CONTROL,
	/**
	 * Information, not intended to be used between components
	 */
	INFORMATION,
	/**
	 * Default
	 */
	NONE;
}
