/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.model;

import java.io.IOException;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import ch.zuestengineering.emod.gui.modelling.GraphElementPosition;
import ch.zuestengineering.emod.utils.PropertiesHandler;

/**
 * General machine component
 * 
 * @author dhampl
 * 
 */
@XmlRootElement
public class MachineComponent {

	private String name;
	private APhysicalComponent component;
	@XmlElement
	private GraphElementPosition position = new GraphElementPosition(0, 0);

	/**
	 * 
	 * @param name
	 */
	public MachineComponent(String name) {
		super();
		this.name = name;
	}

	/**
	 * Empty machine component
	 */
	public MachineComponent() {
		super();
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
		setInitialConditions();
	}

	/**
	 * @return the component
	 */
	public APhysicalComponent getComponent() {
		return component;
	}

	/**
	 * @param component the {@link APhysicalComponent} to set
	 */
	public void setComponent(APhysicalComponent component) {
		this.component = component;
		setInitialConditions();
	}

	private void setInitialConditions() {
		if (!component.equals(null))
			component.setDynamicStateParent(name);
	}

	/**
	 * Position of the element in the graph
	 * 
	 * @return
	 */
	public GraphElementPosition getPosition() {
		return position;
	}

	/**
	 * Set the position of the element in the graph
	 * 
	 * @param position
	 */
	@XmlTransient
	public void setPosition(GraphElementPosition position) {
		this.position = position;
	}
	
	/**
	 * newMachineComponentType
	 * @param stringCompTypeValue
	 * @param stringCompParamValue
	 */
	public static void newMachineComponentType(String stringCompTypeValue, String stringCompParamValue) {
		/*
		 * Technical parameters
		 */
		final String path = PropertiesHandler.getProperty("app.MachineComponentDBPathPrefix") + "/";
		Path from = Paths.get(path + stringCompTypeValue + "/" + stringCompTypeValue + "_Example.xml");
		Path to = Paths.get(path + stringCompTypeValue + "/" + stringCompTypeValue + "_" + stringCompParamValue + ".xml");
		
		// overwrite existing file, if exists
		CopyOption[] options = new CopyOption[] { StandardCopyOption.REPLACE_EXISTING,
				StandardCopyOption.COPY_ATTRIBUTES };
		try {
			Files.copy(from, to, options);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		from = Paths.get(path + stringCompTypeValue + "/" + stringCompTypeValue + "_Example_lc.xml");
		to = Paths.get(path + stringCompTypeValue + "/" + stringCompTypeValue + "_" + stringCompParamValue + "_lc.xml");
		
		/*
		 * LC parameters
		 */
		try {
			Files.copy(from, to, options);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
