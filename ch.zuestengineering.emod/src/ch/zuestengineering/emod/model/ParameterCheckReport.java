/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.model;

import java.util.ArrayList;
import java.util.logging.Logger;

/**
 * Implements a basic report of a parameter set check result
 * 
 * @author Simon Z�st
 *
 */
public class ParameterCheckReport {
	ArrayList<String> names = new ArrayList<>();
	ArrayList<String> messages = new ArrayList<>();

	Logger logger = Logger.getLogger(ParameterCheckReport.class.getName());

	/**
	 * Add a new errors message for the parameter with the given name
	 * 
	 * @param name
	 * @param message
	 */
	public void add(String name, String message) {
		logger.info(name + ":" + message);
		names.add(name);
		messages.add(message);
	}

	/**
	 * Returns if their are any errors reported
	 * 
	 * @return
	 */
	public boolean hasErrors() {
		return names.size() > 0;
	}

	/**
	 * Returns if their are any errors reported for the given parameter
	 * 
	 * @param name
	 * @return
	 */
	public boolean hasErrors(String name) {
		return getParametersWithErrors().contains(name);
	}

	/**
	 * returns a list with (unique) names of the parameters with errors
	 * 
	 * @return
	 */
	public ArrayList<String> getParametersWithErrors() {
		ArrayList<String> ret = new ArrayList<>();

		for (String s : names) {
			if (!ret.contains(s))
				ret.add(s);
		}

		return ret;
	}

	/**
	 * Returns all the messages for the parameter with the given name
	 * 
	 * @param name
	 * @return
	 */
	public String getMessages(String name) {
		String ret = "";
		for (int i = 0; i < names.size(); i++) {
			if (name.equals("") | names.get(i).equals(name))
				ret += messages.get(i) + "\n";
		}

		return ret;
	}

	/**
	 * Returns all the messages
	 * 
	 * @return
	 */
	public String getMessages() {
		return getMessages("");
	}

}
