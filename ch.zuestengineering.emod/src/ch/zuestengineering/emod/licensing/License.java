/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.licensing;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Logger;

/**
 * Representation of a license
 * 
 * @author simon
 *
 */
public class License {
	private String owner;
	private String organization;
	private String machine;
	private String user;
	private LicenseType type;
	private Date expire;
	private String key;

	protected Logger logger = Logger.getLogger(License.class.getName());

	/**
	 * Default constructor
	 */
	public License() {
		loadDefault();
	}

	/**
	 * @param owner the owner to set
	 */
	public void setOwner(String owner) {
		this.owner = owner;
	}

	/**
	 * @param organization the organization to set
	 */
	public void setOrganization(String organization) {
		this.organization = organization;
	}

	/**
	 * @param machine the machine to set
	 */
	public void setMachine(String machine) {
		this.machine = machine;
	}
	
	/**
	 * @param user the user to set
	 */
	public void setUser(String user) {
		this.user = user;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(LicenseType type) {
		this.type = type;
	}

	/**
	 * @param expire the expire to set
	 */
	public void setExpire(Date expire) {
		this.expire = expire;
	}

	/**
	 * @param key the key to set
	 */
	public void setKey(String key) {
		this.key = key;
	}

	/**
	 * Loads the license file at the provided path
	 * 
	 * @param path
	 */
	public License(String path) {
		loadLicenseFile(path);
	}

	/**
	 * Loads the default license file
	 */
	public void loadDefault() {
		loadLicenseFile("license.lic");
	}

	/**
	 * Generates a license file with the given information at the path provided
	 * 
	 * NOTE: This function does not generate a valid key, but uses the one provided.
	 * Hence, the validity of the generated license file is not guaranteed
	 * 
	 * @param path
	 */
	public void generateLicenseFile(String path) {
		try {
			BufferedWriter bw = new BufferedWriter(new FileWriter(path));
			bw.flush();
			bw.write(owner);
			bw.newLine();
			bw.write(organization);
			bw.newLine();
			bw.write(type.toString());
			bw.newLine();
			bw.write(getExpireString());
			bw.newLine();
			bw.write(key);
			bw.newLine();
			bw.write(machine);
			bw.newLine();
			bw.write(user);
			bw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Loads the license file from the provided path
	 * 
	 * @param path
	 */
	public void loadLicenseFile(String path) {
		try {
			BufferedReader br = new BufferedReader(new FileReader(path));
			DateFormat format = new SimpleDateFormat("dd.MM.yyyy");
			owner = br.readLine();
			organization = br.readLine();
			type = LicenseType.valueOf(br.readLine());
			expire = format.parse(br.readLine());
			key = br.readLine().replace("-", "");
			machine = br.readLine();
			user = br.readLine();
			
			if(machine==null) machine = "";
			if(user==null) user = "";
			
			br.close();
		} catch (Exception e) {
			logger.severe("Could not load license file '" + path + "'");

			setOwner(System.getProperty("user.name"));
			setOrganization(System.getProperty("os.name"));
			setType(LicenseType.VIEWER);
			setExpire(new Date());
			setKey("");
			setMachine("");
			setUser("");
		}
		
		/*
		 * Override
		 */
		setType(LicenseType.FREE);
	}

	/**
	 * @return the owner
	 */
	public String getOwner() {
		return owner;
	}

	/**
	 * @return the organization
	 */
	public String getOrganization() {
		return organization;
	}
	
	/**
	 * @return the machine
	 */
	public String getMachine() {
		return machine;
	}
	
	/**
	 * @return the machine
	 */
	public String getUser() {
		return user;
	}

	/**
	 * @return the type
	 */
	public LicenseType getType() {
		return type;
	}

	/**
	 * @return the key
	 */
	public String getKey() {
		return key;
	}

	/**
	 * @return the expire date
	 */
	public String getExpireString() {
		DateFormat format = new SimpleDateFormat("dd.MM.yyyy");
		String ret = format.format(expire);
		return ret;
	}

	/**
	 * Returns the expired date
	 * 
	 * @return
	 */
	public Date getExpire() {
		return expire;
	}

}
