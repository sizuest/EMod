/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.licensing;

/**
 * Implements the enumeration of the lic types
 * 
 * @author Simon Z�st
 *
 */
public enum LicenseType {
	/**
	 * Full functions
	 */
	PROFESSIONAL,
	/**
	 * Full functions, research only
	 */
	ACADEMIC,
	/**
	 * DuctDesigner only
	 */
	DUCTDESIGNER,
	/**
	 * Full function, product evaluation only
	 */
	EVALUATION,
	/**
	 * View the model, configure and run new simulations
	 */
	SIMULATION,
	/**
	 * Only view models and results
	 */
	VIEWER,
	/**
	 * Free (work arround)
	 */
	FREE
}
