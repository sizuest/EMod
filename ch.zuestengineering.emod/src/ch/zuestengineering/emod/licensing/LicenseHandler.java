/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.licensing;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.Key;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Date;
import java.util.logging.Logger;
//import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;
import java.util.Base64;

/**
 * Implements the license handling offering the following services: - license
 * verification - action management (allow/disallow) - license information
 * 
 * @author Simon Z�st
 *
 */
public class LicenseHandler {
	private static LicenseHandler instance;

	private License license;

	private Date lastLicenseCheck = null;
	private boolean holdsValidLic = false;

	protected Logger logger = Logger.getLogger(LicenseHandler.class.getName());

	private LicenseHandler() {
		license = new License();
	}

	/**
	 * Returns the (unque) instance of the licence handler
	 * 
	 * @return
	 */
	public static LicenseHandler getInstance() {
		if (null == instance)
			instance = new LicenseHandler();

		return instance;
	}

	/**
	 * Returns whether or not the given action can be performed with the currently
	 * loaded license
	 * 
	 * @param action
	 * @return
	 */
	public static boolean can(LicenseActions action) {
		/*
		 * BYPASS OF LIC-HANDLER
		 */
		
		return true;
		
		/*
		
		getInstance().updateLicenseValidity();

		if (!getInstance().holdsValidLic)
			return false;

		switch (action) {
		case EDIT_MDL:
			switch (getInstance().license.getType()) {
			case ACADEMIC:
			case EVALUATION:
			case PROFESSIONAL:
				return true;
			default:
				break;
			}
			break;
		case EDIT_MDLLIBRARY:
		case EDIT_MATLIBRARY:
			switch (getInstance().license.getType()) {
			case ACADEMIC:
			case PROFESSIONAL:
			case DUCTDESIGNER:
				return true;
			default:
				break;
			}
			break;
		case EDIT_SCENARIO:
			switch (getInstance().license.getType()) {
			case ACADEMIC:
			case EVALUATION:
			case PROFESSIONAL:
			case SIMULATION:
				return true;
			default:
				break;
			}
			break;
		case EDIT_SIMCFG:
			switch (getInstance().license.getType()) {
			case ACADEMIC:
			case EVALUATION:
			case PROFESSIONAL:
			case SIMULATION:
				return true;
			default:
				break;
			}
			break;
		case SAVE:
		case CREATENEW:
			switch (getInstance().license.getType()) {
			case ACADEMIC:
			case EVALUATION:
			case PROFESSIONAL:
			case SIMULATION:
				return true;
			default:
				break;

			}
			break;
		case RUNSIMULATION:
			switch (getInstance().license.getType()) {
			case ACADEMIC:
			case EVALUATION:
			case PROFESSIONAL:
			case SIMULATION:
				return true;
			default:
				break;

			}
			break;
		case EDIT_DUCTMDL:
			switch (getInstance().license.getType()) {
			case ACADEMIC:
			case EVALUATION:
			case PROFESSIONAL:
			case DUCTDESIGNER:
				return true;
			default:
				break;

			}
			break;
		default:
			break;

		}

		return false;
		
		*/
	}

	private void updateLicenseValidity() {

		Date now = new Date();

		/* Skip check if last successful check was done within the last 5min */
		if (lastLicenseCheck != null & getInstance().holdsValidLic)
			if (now.getTime() - lastLicenseCheck.getTime() < 300000)
				return;

		lastLicenseCheck = now;
		
		

		/* Validate the signature (Condition 1) */
		try {
			getInstance().holdsValidLic = LicenseHandler.checkLicenseKey(license);
		} catch (SignatureException e) {
			getInstance().holdsValidLic = false;
		}

		/* Check the expire date (Condition 2) */
		if(getInstance().holdsValidLic & license.getExpire().before(new Date())) {
			getInstance().holdsValidLic = false;
			logger.warning("License Expired!");
		}
		
		/* Check machine name (Condition 3) */
		if(!license.getMachine().equals("") & !license.getMachine().equals(getMachineName())) {
			getInstance().holdsValidLic = false;
			logger.warning("Wrong machine name for this license");
		}
		
		/* Check user name (Condition 4) */
		if(!license.getUser().equals("") & !license.getUser().equals(getUserName())) {
			getInstance().holdsValidLic = false;
			logger.warning("Wrong user name for this license");
		}
		
		/* Simple case: Free version */
		if(license.getType().equals(LicenseType.FREE))
			getInstance().holdsValidLic = true;
		

		/* Report check results */
		if (getInstance().holdsValidLic) {
			logger.info("License: " + getLicenseInfo());
		}
		
		
		

	}

	/**
	 * Generates the license key using RSA
	 * 
	 * @param lic
	 * @param path
	 * 
	 * @return
	 * @throws SignatureException
	 */
	public static String generateLicenseKey(License lic, String path) throws SignatureException {
		try {
			Signature sign = Signature.getInstance("SHA1withRSA");
			sign.initSign((PrivateKey) getPrivateKey(path));
			sign.update(getLicenseString(lic).getBytes("UTF-8"));
			return new String(Base64.getEncoder().encodeToString(sign.sign()));
		} catch (Exception ex) {
			throw new SignatureException(ex);
		}
	}

	/**
	 * Loads the default public rsa key
	 * 
	 * @return
	 * @throws Exception
	 */
	private static Key getPublicKey() throws Exception {
		// byte[] keyBytes = Files.readAllBytes(Paths.get("public_key.der"));
		byte[] keyBytes = { 48, -126, 1, 34, 48, 13, 6, 9, 42, -122, 72, -122, -9, 13, 1, 1, 1, 5, 0, 3, -126, 1, 15, 0,
				48, -126, 1, 10, 2, -126, 1, 1, 0, -101, 5, 10, -38, -86, 37, -113, 41, 112, -19, -78, 48, -37, -84,
				-54, 65, 108, -82, -64, 29, -49, 122, 74, -8, 122, -9, -46, 104, -60, -51, 43, -122, -90, -78, -123,
				-66, -78, 98, -17, 62, -84, 122, -76, 41, 13, 22, 93, -52, -45, -57, 16, -94, -113, -10, -6, 39, 106,
				-96, 67, 59, -40, 29, 17, -102, 71, 36, 88, -110, -89, 43, 55, 9, 39, -128, 2, -39, -26, -36, 4, -44,
				-117, 39, -25, -38, -33, -36, -97, 5, 47, -59, -24, 75, 65, -111, 97, -31, -91, -65, 105, 96, 124, -65,
				-61, 57, 14, -111, 92, -37, -37, 96, 80, 100, 38, -86, 10, 56, -75, -66, -34, -124, -113, -71, -116,
				118, -116, -59, 48, 37, -106, 45, -98, -59, 67, 4, -112, -40, 82, -70, -91, -57, -38, -123, 68, -14,
				-115, 7, 100, 67, 7, 63, 13, -126, -81, 104, -14, 42, 87, 30, 47, 29, -108, 98, 62, -54, -97, -104, 112,
				30, 84, 55, 3, -6, 5, -4, -72, -36, -51, -62, -89, -110, -59, -24, -106, 9, 82, -40, -80, -75, 63, 27,
				-69, -116, -55, -36, -18, 106, -126, -107, -59, 0, 43, 84, 45, -20, -58, -28, -95, 54, -2, -109, -83,
				18, -74, 116, -35, 38, -114, 35, 115, -72, 17, 28, 69, -70, -86, -108, -25, -112, -14, 127, -1, 18, 123,
				127, 46, -34, 4, 56, -37, 20, -50, 107, -26, 84, -50, -127, 25, 91, -122, -118, -5, 106, 80, 13, 57,
				-85, 2, 3, 1, 0, 1 };

		X509EncodedKeySpec spec = new X509EncodedKeySpec(keyBytes);
		KeyFactory kf = KeyFactory.getInstance("RSA");
		return kf.generatePublic(spec);
	}

	/**
	 * Loads the default private rsa key
	 * 
	 * @param path
	 * @return
	 * @throws Exception
	 */
	public static Key getPrivateKey(String path) throws Exception {
		byte[] keyBytes = Files.readAllBytes(Paths.get(path));

		PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(keyBytes);
		KeyFactory kf = KeyFactory.getInstance("RSA");
		return kf.generatePrivate(spec);
	}

	/**
	 * Computes the license text formatted as: 'OWNER/ORGANIZATION/TYPE/EXPIRE'
	 * 
	 * @param owner
	 * @param organization
	 * @param type
	 * @param expire
	 * @param machine 
	 * @param user 
	 * @return
	 */
	public static String getLicenseString(String owner, String organization, LicenseType type, String expire, String machine, String user) {
		String out =  owner + "/" + organization + "/" + type.toString() + "/" + expire;
		
		if(!machine.equals(""))
			out+="/"+machine;
		if(!user.equals(""))
			out+="/"+user;
		
		return out;
	}

	/**
	 * Computes the license text formatted as: 'OWNER/ORGANIZATION/TYPE/EXPIRE'
	 * 
	 * @param lic
	 * @return
	 */
	public static String getLicenseString(License lic) {
		String ret = getLicenseString(lic.getOwner(), lic.getOrganization(), lic.getType(), lic.getExpireString(), lic.getMachine(), lic.getUser());

		for (int i = 0; i < 4; i++)
			ret += "/" + ret;

		return ret;
	}

	/**
	 * Compares the provided license key to the expected one
	 * 
	 * @param lic
	 * @return
	 * @throws SignatureException
	 */
	public static boolean checkLicenseKey(License lic) throws SignatureException {
		try {
			Signature sign = Signature.getInstance("SHA1withRSA");
			sign.initVerify((PublicKey) getPublicKey());
			sign.update(getLicenseString(lic).getBytes("UTF-8"));
			return sign.verify(Base64.getDecoder().decode(lic.getKey()));
		} catch (Exception ex) {
			throw new SignatureException(ex);
		}
	}

	/**
	 * Provides license information as formatted string
	 * 
	 * @return
	 */
	public static String getLicenseInfo() {
		// getInstance().updateLicenseValidity();
		
		String machineInfo="", userInfo="";
		
		if(!getInstance().license.getMachine().equals(""))
			machineInfo = " on machine " + getInstance().license.getMachine();
		if(!getInstance().license.getUser().equals(""))
			userInfo = "(user: " + getInstance().license.getUser() + ")";

		return getInstance().license.getType().toString() + " " + getLicenseOwner() + userInfo + machineInfo + " valid until "
				+ getInstance().license.getExpireString();
	}

	/**
	 * @return
	 */
	public static String getLicenseOwner() {
		return getInstance().license.getOwner() + " (" + getInstance().license.getOrganization() + ")";
	}

	/**
	 * Returns the license type as String
	 * 
	 * @return
	 */
	public static String getLicenseType() {
		return getInstance().license.getType().toString();
	}

	/**
	 * @return
	 */
	public static boolean getLicenseValidity() {
		getInstance().updateLicenseValidity();

		return getInstance().holdsValidLic;
	}
	
	/**
	 * getMachineName
	 * @return
	 */
	public static String getMachineName() {
		String hostname = "Unknown";

		try
		{
		    InetAddress addr;
		    addr = InetAddress.getLocalHost();
		    hostname = addr.getHostName();
		}
		catch (UnknownHostException ex)
		{
		    System.out.println("Hostname can not be resolved");
		}
		
		return hostname;
	}
	
	/**
	 * getUserName
	 * @return
	 */
	public static String getUserName() {		
		return System.getProperty("user.name");
	}
}
