/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.licensing;

/**
 * Representation of the possible license controlled actions
 * 
 * @author Simon Z�st
 *
 */
public enum LicenseActions {
	/**
	 * Edit the energy model
	 */
	EDIT_MDL,
	/**
	 * Edit the simulation (integrator, fc-solver, ...) settings
	 */
	EDIT_SIMCFG,
	/**
	 * Edit the simualted scenario
	 */
	EDIT_SCENARIO,
	/**
	 * Open and edit the model library
	 */
	EDIT_MDLLIBRARY,
	/**
	 * Open and edit the material library
	 */
	EDIT_MATLIBRARY,
	/**
	 * Edit the duct model
	 */
	EDIT_DUCTMDL,
	/**
	 * Create a new model
	 */
	CREATENEW,
	/**
	 * Save the model an setup
	 */
	SAVE,
	/**
	 * ORun the simulation
	 */
	RUNSIMULATION
}
