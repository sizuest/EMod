/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.lang.reflect.Constructor;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;

import ch.zuestengineering.emod.model.APhysicalComponent;
import ch.zuestengineering.emod.model.ClampTest;
import ch.zuestengineering.emod.model.MachineComponent;
import ch.zuestengineering.emod.model.control.FillControl;
import ch.zuestengineering.emod.model.control.HysteresisControl;
import ch.zuestengineering.emod.model.control.SwitchControl;
import ch.zuestengineering.emod.model.fluid.Floodable;
import ch.zuestengineering.emod.model.fluid.FluidCircuit;
import ch.zuestengineering.emod.model.fluid.FluidCircuitProperties;
import ch.zuestengineering.emod.model.linking.FluidConnection;
import ch.zuestengineering.emod.model.linking.FluidContainer;
import ch.zuestengineering.emod.model.linking.IOConnection;
import ch.zuestengineering.emod.model.linking.IOContainer;
import ch.zuestengineering.emod.model.math.Gain;
import ch.zuestengineering.emod.model.math.Sum;
import ch.zuestengineering.emod.model.pm.Amplifier;
import ch.zuestengineering.emod.model.pm.Bearing;
import ch.zuestengineering.emod.model.pm.Bypass;
import ch.zuestengineering.emod.model.pm.CompressedFluid;
import ch.zuestengineering.emod.model.pm.ConstantComponent;
import ch.zuestengineering.emod.model.pm.CooledHeatSource;
import ch.zuestengineering.emod.model.pm.Cooler;
import ch.zuestengineering.emod.model.pm.CuttingProcess;
import ch.zuestengineering.emod.model.pm.Cylinder;
import ch.zuestengineering.emod.model.pm.EDMGenerator;
import ch.zuestengineering.emod.model.pm.Fan;
import ch.zuestengineering.emod.model.pm.Filter;
import ch.zuestengineering.emod.model.pm.ForcedFluidFlow;
import ch.zuestengineering.emod.model.pm.HeatExchanger;
import ch.zuestengineering.emod.model.pm.HeatExchangerAir;
import ch.zuestengineering.emod.model.pm.HydraulicAccumulator;
import ch.zuestengineering.emod.model.pm.LinAxis;
import ch.zuestengineering.emod.model.pm.Mains;
import ch.zuestengineering.emod.model.pm.Motor;
import ch.zuestengineering.emod.model.pm.MotorAC;
import ch.zuestengineering.emod.model.pm.MotorDC;
import ch.zuestengineering.emod.model.pm.MotorDCLinear;
import ch.zuestengineering.emod.model.pm.MovingMass;
import ch.zuestengineering.emod.model.pm.Pipe;
import ch.zuestengineering.emod.model.pm.Pump;
import ch.zuestengineering.emod.model.pm.Revolver;
import ch.zuestengineering.emod.model.pm.RotAxis;
import ch.zuestengineering.emod.model.pm.Servodrive;
import ch.zuestengineering.emod.model.pm.Spindle;
import ch.zuestengineering.emod.model.pm.Tank;
import ch.zuestengineering.emod.model.pm.Transmission;
import ch.zuestengineering.emod.model.pm.Valve;
import ch.zuestengineering.emod.model.thermal.ForcedHeatTransfer;
import ch.zuestengineering.emod.model.thermal.FreeHeatTransfer;
import ch.zuestengineering.emod.model.thermal.HomogStorage;
import ch.zuestengineering.emod.model.thermal.LayerStorage;
import ch.zuestengineering.emod.model.units.SiUnit;
import ch.zuestengineering.emod.model.units.Unit;
import ch.zuestengineering.emod.simulation.ASimulationControl;
import ch.zuestengineering.emod.simulation.ConstantSimulationControl;
import ch.zuestengineering.emod.simulation.DynamicState;
import ch.zuestengineering.emod.simulation.GeometricKienzleSimulationControl;
import ch.zuestengineering.emod.simulation.ProcessSimulationControl;
import ch.zuestengineering.emod.simulation.RandomSimulationControl;
import ch.zuestengineering.emod.simulation.StaticSimulationControl;
import ch.zuestengineering.emod.utils.Defines;
import ch.zuestengineering.emod.utils.PropertiesHandler;

/**
 * General implementation of a machine
 * 
 * @author andreas
 * 
 */
@XmlRootElement(namespace = "ch.zuestengineering.emod")
@XmlSeeAlso({ MachineComponent.class, APhysicalComponent.class, Motor.class, MotorAC.class, MotorDCLinear.class,
		LinAxis.class, RotAxis.class, ClampTest.class, MotorDC.class, Revolver.class, Fan.class,
		HydraulicAccumulator.class, Cooler.class, Transmission.class, CompressedFluid.class, Amplifier.class,
		ConstantComponent.class, Servodrive.class, Cylinder.class, Valve.class, Pipe.class, Filter.class,
		HeatExchanger.class, Bypass.class, HeatExchangerAir.class, MovingMass.class, Bearing.class, Spindle.class,
		Tank.class, Pump.class, CuttingProcess.class, ForcedFluidFlow.class, CooledHeatSource.class, EDMGenerator.class,
		HysteresisControl.class, Mains.class, SwitchControl.class, FillControl.class, Sum.class, Gain.class,
		HomogStorage.class, LayerStorage.class, ForcedHeatTransfer.class, FreeHeatTransfer.class,
		ASimulationControl.class, RandomSimulationControl.class, ConstantSimulationControl.class,
		StaticSimulationControl.class, ProcessSimulationControl.class, GeometricKienzleSimulationControl.class,
		IOConnection.class, FluidConnection.class })
@XmlAccessorType(XmlAccessType.FIELD)
public class Machine {

	private static Logger logger = Logger.getLogger(Machine.class.getName());

	/* List with machine components */
	@XmlElementWrapper(name = "machine")
	@XmlElement(name = "machineComponent")
	private ArrayList<MachineComponent> componentList;

	/* List with simulation objects */
	@XmlElementWrapper
	@XmlElement(name = "simController")
	private List<ASimulationControl> simulators;

	/* List with component output->input connection */
	@XmlElementWrapper(name = "linking")
	@XmlElement(name = "ioConnection")
	private List<IOConnection> connectionList;

	/* Model reference */
	private static Machine machineModel = null;

	/**
	 * Private constructor for singleton implementation.
	 */
	private Machine() {
	}

	/**
	 * Reads the machine config of the machine.
	 * 
	 * @param machineName   Name of machine
	 * @param machineConfig Name of machine config
	 */
	public static void buildMachine(String machineName, String machineConfig) {
		/* Create machine */
		if (machineModel != null)
			logger.info("Overwrinting current machine with '" + machineName + "' - '" + machineConfig + "'");

		/*
		 * Generate path to machine config: e.g.
		 * Machines/NDM200/MachineConfig/TestConfig1/
		 */
		String prefix = PropertiesHandler.getProperty("app.MachineDataPathPrefix");
		String path = prefix + "/" + machineName + "/" + Defines.MACHINECONFIGDIR + "/" + machineConfig + "/";

		/* ****************************************************************** */
		/* Init machine form config */
		/* ****************************************************************** */
		logger.info("Load machine from file: " + path + Defines.MACHINEFILENAME);
		initMachineFromFile(path + Defines.MACHINEFILENAME);

	}

	/**
	 * Saves the machine
	 * 
	 * @param machineName   Name of machine
	 * @param machineConfig Name of machine config
	 */
	public static void saveMachine(String machineName, String machineConfig) {
		/*
		 * Generate path to machine config: e.g.
		 * Machines/NDM200/MachineConfig/TestConfig1/
		 */
		String prefix = EModSession.getRootPath();
		String path = prefix + File.separator + Defines.MACHINECONFIGDIR + File.separator + machineConfig
				+ File.separator;

		/* Saves the machine */
		saveMachineToFile(path + Defines.MACHINEFILENAME);

		/* Clean up old simulator configs */
		cleanUpConfigurations(path);
	}

	/**
	 * Create a new machine
	 * 
	 * @param machineName      Name of the machine
	 * @param machineConfigDir Name of the machine configuration
	 */
	public static void newMachine(String machineName, String machineConfigDir) {

		/*
		 * Check, if the directories already exists
		 */
		String prefix = PropertiesHandler.getProperty("app.MachineDataPathPrefix");

		File path = new File(prefix + "/" + machineName + "/" + Defines.MACHINECONFIGDIR + "/" + machineConfigDir);
		// Creat directory if required
		if (path.exists()) {
			logger.warning(
					"Can't create new machine " + machineName + ":" + machineConfigDir + ": Machine already exists");
			return;
		}

		// Create directory if required
		path.mkdirs();

		// Create empty machine
		// clearMachine();

		// Save machine
		saveMachine(machineName, machineConfigDir);

		// Save IO Linking

	}

	/**
	 * Clears the current machine
	 */
	public static void clearMachine() {
		machineModel = new Machine();

		getInstance().componentList = new ArrayList<MachineComponent>();
		getInstance().simulators = new ArrayList<ASimulationControl>();
		getInstance().connectionList = new ArrayList<IOConnection>();
	}

	/**
	 * Deletes the machine
	 * 
	 * @param machineName
	 * @param machineConfigDir
	 */
	public static void deleteMachine(String machineName, String machineConfigDir) {
		// Clear current machine
		clearMachine();

		// Check for config dir
		String prefix = PropertiesHandler.getProperty("app.MachineDataPathPrefix");

		// TOOD manick: the correct filepath is
		// machineName/MachineConfig/machineConfigDir!
		// File path = new File(prefix+"/"+machineName+"/"+machineConfigDir);
		File path = new File(prefix + "/" + machineName + "/MachineConfig/" + machineConfigDir);
		// Creat directory if required
		if (!path.exists()) {
			logger.warning(
					"Can't delete machine " + machineName + ":" + machineConfigDir + ": Machine does not exists");
			return;
		}

		// Empty directory
		for (File f : path.listFiles()) {
			try {
				f.delete();
			} catch (Exception e) {
				logger.warning("Can't delete machine " + machineName + ":" + machineConfigDir
						+ ": Failed to remove configuration file " + f.getName());
			}
		}

		try {
			path.delete();
		} catch (Exception e) {
			logger.warning("Can't delete machine " + machineName + ":" + machineConfigDir
					+ ": Failed to remove configuration directory");
		}
	}

	/**
	 * Initializes a new machine based on the machine name provided
	 * 
	 * @param file
	 */
	public static void initMachineFromFile(String file) {
		machineModel = null;
		try {
			JAXBContext context = JAXBContext.newInstance(Machine.class);
			Unmarshaller um = context.createUnmarshaller();
			machineModel = (Machine) um.unmarshal(new FileReader(file));
		} catch (Exception e) {
			e.printStackTrace();
		}

		createIOConnections();
		// loadInitialConditions();
	}

	/**
	 * Builds all IOConnections based on the provided names
	 */
	public static void createIOConnections() {
		if (null != getInstance().getIOLinkList())
			for (int i = getInstance().getIOLinkList().size() - 1; i >= 0; i--)
				if (!(getInstance().getIOLinkList().get(i).createIOConnection()))
					getInstance().getIOLinkList().remove(i);
	}

	/**
	 * Saves the existing machine at file name provided
	 * 
	 * @param file
	 */
	public static void saveMachineToFile(String file) {
		// Save Machine Configuration
		try {
			JAXBContext context = JAXBContext.newInstance(Machine.class);
			Marshaller m = context.createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

			Writer w = new FileWriter(file);
			m.marshal(machineModel, w);
			w.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Saves the existing machine at a new location
	 * 
	 * @param file
	 */
	public static void saveMachineToNewFile(String file) {
		// Save Machine Configuration
		try {
			JAXBContext context = JAXBContext.newInstance(Machine.class);
			Marshaller m = context.createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

			Writer w = new FileWriter(file);
			m.marshal(machineModel, w);
			w.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Saves the initial conditions
	 */
	public static void saveInitialConditions() {
		String name;
		getInstance();
		for (DynamicState ds : Machine.getDynamicStatesList()) {
			name = "*unknown*";
			try {
				name = ds.getInitialConditionName();
				ds.saveInitialCondition();
			} catch (Exception e) {
				logger.warning("Failed to save initial condition " + name + ": " + e.getMessage());
			}
		}
	}

	/**
	 * Loads the initial conditions
	 */
	public static void loadInitialConditions() {
		String name;
		getInstance();
		for (DynamicState ds : Machine.getDynamicStatesList()) {
			name = "*unknown*";
			try {
				name = ds.getInitialConditionName();
				ds.loadInitialCondition();
			} catch (Exception e) {
				logger.warning("Failed to load initial condition " + name + ": " + e.getMessage());
			}
		}
	}

	/**
	 * Cleans up old simulator configs
	 * 
	 * All files names at the state path are checked for - Equal to Machine.xml
	 * (Defines.MACHINEFILENAME) - Equal to IOLinking.txt (Defines.LINKFILENAME) -
	 * Equal to an existing simulation control
	 * 
	 * if non of the statements above is true, the file will be removed
	 * 
	 * @param path
	 */
	public static void cleanUpConfigurations(String path) {
		File dir = new File(path);
		File[] files = dir.listFiles();

		ArrayList<String> simConfigFileNames = new ArrayList<String>();

		/*
		 * Create the set of required simulator config files
		 */
		if (null != getInstance().simulators)
			for (ASimulationControl sc : getInstance().simulators)
				try {
					simConfigFileNames.add(sc.getSimulationConfigReader().getFileName());
				} catch (Exception e) {
					e.printStackTrace();
				}

		/*
		 * Loop through all files and check if they are still required
		 */
		fileloop: for (File f : files) {
			/*
			 * Equal to machine config file?
			 */
			if (f.getName().contentEquals(Defines.MACHINEFILENAME))
				continue;
			/*
			 * Equal to io linking file?
			 */
			if (f.getName().contentEquals(Defines.LINKFILENAME))
				continue;

			/*
			 * All other files must be simulator configuration: Let's check if they are
			 * still required
			 */
			for (String s : simConfigFileNames)
				if (f.getName().contentEquals(s))
					continue fileloop;

			/*
			 * The file is not needed any more: delete it!
			 */
			f.delete();
		}
	}

	/**
	 * returns the first machine component with a specified name.
	 * 
	 * @param name
	 * @return the {@link MachineComponent} with the name.
	 */
	public static MachineComponent getMachineComponent(String name) {

		// Case: Empty machine
		if (null == machineModel)
			return null;
		else if (null == Machine.getInstance().componentList)
			return null;

		// Default case:
		MachineComponent temp = null;
		for (MachineComponent mc : Machine.getInstance().componentList) {
			if (mc.getName().equals(name)) {
				temp = mc;
				break;
			}
		}

		return temp;
	}

	/**
	 * returns the first simulator with a specified name.
	 * 
	 * @param name
	 * @return the {@link ASimulationControl} with the name.
	 */
	public static ASimulationControl getInputObject(String name) {

		// Case: Empty machine
		if (null == machineModel)
			return null;
		else if (null == Machine.getInstance().simulators)
			return null;

		// Default case:

		for (int i = 0; i < getInstance().simulators.size(); i++) {
			if (getInstance().simulators.get(i).getName().equals(name))
				return getInstance().simulators.get(i);
		}

		return null;
	}

	/**
	 * 
	 * @return list of all input-Output connections
	 */
	public static ArrayList<MachineComponent> getMachineComponentList() {
		return getInstance().componentList;
	}

	/**
	 * 
	 * @return list of all floodable components
	 */
	public ArrayList<MachineComponent> getFloodableMachineComponentList() {
		ArrayList<MachineComponent> fmc = new ArrayList<MachineComponent>();
		for (MachineComponent mc : componentList) {
			if (mc.getComponent() instanceof ch.zuestengineering.emod.model.fluid.Floodable) {
				fmc.add(mc);
			}
		}
		return fmc;
	}

	/**
	 * 
	 * @return List with input parameter simulation objects
	 */
	public List<ASimulationControl> getInputObjectList() {
		return simulators;
	}

	/**
	 * Returns the list with input parameter simulation objects with the stated unit
	 * 
	 * @param unit SiUnit
	 * @return List with input parameter simulation
	 */
	public static List<ASimulationControl> getInputObjectList(SiUnit unit) {
		List<ASimulationControl> simulatorsFiltered = new ArrayList<ASimulationControl>();

		for (ASimulationControl sc : getInstance().simulators)
			if (sc.getUnit().equals(unit))
				simulatorsFiltered.add(sc);

		return simulatorsFiltered;
	}

	/**
	 * Returns the list with process simulation control objects with the stated unit
	 * 
	 * @param unit SiUnit
	 * @return List with input parameter simulation
	 */
	public static List<ASimulationControl> getProcessSimulationControlList(SiUnit unit) {
		List<ASimulationControl> simulatorsFiltered = new ArrayList<ASimulationControl>();

		for (ASimulationControl sc : getInstance().simulators)
			if (sc.getUnit().equals(unit) & sc instanceof ProcessSimulationControl)
				simulatorsFiltered.add(sc);

		return simulatorsFiltered;
	}

	/**
	 * Returns a list with all input objects requiring a time dependent process
	 * signal
	 * 
	 * @return out
	 */
	public List<ASimulationControl> getVariableInputObjectList() {
		List<ASimulationControl> out = new ArrayList<ASimulationControl>();

		if (Machine.getInstance().getInputObjectList() == null)
			return out;

		for (ASimulationControl sc : Machine.getInstance().getInputObjectList()) {
			if (sc.getClass().getSimpleName().equals("ProcessSimulationControl"))
				out.add(sc);
		}

		return out;
	}

	/**
	 * 
	 * @return list of all input-Output connections
	 */
	public List<IOConnection> getIOLinkList() {
		return connectionList;
	}

	/**
	 * @return list of all fluid-connections
	 */
	public List<FluidConnection> getFluidConnectionList() {
		return getFluidConnectionList(null);
	}

	/**
	 * @param fc
	 * @return list of all fluid-connections which are related to the state parents
	 */
	public static ArrayList<FluidConnection> getFluidConnectionList(FluidCircuit fc) {
		ArrayList<FluidConnection> fcList = new ArrayList<FluidConnection>();

		for (IOConnection io : getInstance().connectionList) {
			if (io instanceof FluidConnection) {
				if (fc == null)
					fcList.add((FluidConnection) io);
				else {
					if (fc.getMembers().contains(((FluidContainer) (io.getSource())).getFluidCircuitProperties())
							| fc.getMembers().contains(((FluidContainer) (io.getTarget())).getFluidCircuitProperties()))
						fcList.add((FluidConnection) io);
				}
			}
		}

		return fcList;
	}

	/**
	 * Returns a list of independent fluid circuits (each represented as a list of
	 * fluid circuit proeprties)
	 * 
	 * @return
	 */
	public static ArrayList<FluidCircuit> getFluidCircuits() {
		// Fetch all ciruit properties
		ArrayList<FluidCircuitProperties> fluidPropertyListGlobal = new ArrayList<FluidCircuitProperties>();
		for (MachineComponent mc : Machine.getInstance().getFloodableMachineComponentList())
			fluidPropertyListGlobal.addAll(((Floodable) (mc.getComponent())).getFluidPropertiesList());

		ArrayList<FluidCircuit> out = new ArrayList<FluidCircuit>();

		while (fluidPropertyListGlobal.size() > 0) {
			out.add(new FluidCircuit(FluidCircuitProperties.getAllConnectedElements(fluidPropertyListGlobal.get(0))));
			fluidPropertyListGlobal.removeAll(out.get(out.size() - 1).getMembers());
		}

		return out;
	}

	/**
	 * singleton implementation of the machine model
	 * 
	 * @return instance of the machine model
	 */
	public static Machine getInstance() {
		if (machineModel == null) {
			logger.info("No machine existing: Creating empty machine");
			machineModel = new Machine();

			machineModel.componentList = new ArrayList<MachineComponent>();
			machineModel.connectionList = new ArrayList<IOConnection>();
			machineModel.simulators = new ArrayList<ASimulationControl>();
		}
		return machineModel;
	}

	/**
	 * Get unique component name. If the name stated in the argument already exists,
	 * a index number will be added and incremented
	 * 
	 * @param prefix Varbiable prefix
	 * @return Unique component name
	 */
	public static String getUniqueComponentName(String prefix) {

		String name = prefix;
		int idx = 0;

		// Loop until name is unique
		while (null != getMachineComponent(name))
			name = prefix + "_" + (++idx);

		return name;

	}

	/**
	 * Returns a unique component name based on the prefix and available component
	 * names Allready used prefixes will be supplemented by an incremented number
	 * 
	 * @param prefix
	 * @return name
	 */
	public static String getUniqueInputObjectName(String prefix) {
		String name = prefix;
		int idx = 0;

		// Loop unitil name is unique
		while (null != getInputObject(name))
			name = prefix + "_" + (++idx);

		return name;
	}

	/**
	 * Add new component. The function is "name save": If a component with the same
	 * name exists, a unique new name will be generated
	 * 
	 * @param component Component to be added
	 */
	public static void addMachineComponent(MachineComponent component) {

		// If it is an empty machine, create a new machine
		if (machineModel == null)
			machineModel = new Machine();

		// If it is the first component, create list
		if (getInstance().componentList == null)
			getInstance().componentList = new ArrayList<MachineComponent>();

		// Make name unique
		component.setName(getUniqueComponentName(component.getName()));
		// Add to component list
		getInstance().componentList.add(component);
	}

	/**
	 * Adds a new (non existent) machine component by its model type and parameter
	 * set.
	 * 
	 * @param mdlType   model type name (same as class name)
	 * @param paramType parameter set name
	 * @return created machine component object
	 */
	public static MachineComponent addNewMachineComponent(String mdlType, String paramType) {

		Object component = createNewMachineComponent(mdlType, paramType);

		if (null == component)
			return null;

		// Create new machine component object
		MachineComponent mc = new MachineComponent(mdlType);
		mc.setComponent((APhysicalComponent) component);

		// Add to machine
		addMachineComponent(mc);

		return mc;

	}

	/**
	 * Creates a new component by its model type and parameter set.
	 * 
	 * @param mdlType
	 * @param paramType
	 * @return
	 */
	public static APhysicalComponent createNewMachineComponent(String mdlType, String paramType) {

		Object component = null;

		// Try to create and parametrize the object
		try {
			// Get class and constructor objects
			String path = APhysicalComponent.class.getPackage().getName();
			Class<?> cl = Class.forName(path + ".pm." + mdlType);
			Constructor<?> co = cl.getConstructor(String.class);
			// initialize new component
			component = co.newInstance(paramType);
			return (APhysicalComponent) component;
		} catch (Exception e) {
			logger.log(LogLevel.WARNING,
					"Unable to create component " + mdlType + "(" + paramType + ")" + " : " + e.getMessage());
			return null;
		}
	}

	/**
	 * Add new simulator. The function is "name save": If a component with the same
	 * name exists, a unique new name will be generated
	 * 
	 * @param simulator Simulator to be added
	 */
	public static void addInputObject(ASimulationControl simulator) {

		// If it is an empty machine, create a new machine
		if (machineModel == null)
			machineModel = new Machine();

		// If it is the first component, create list
		if (getInstance().simulators == null)
			getInstance().simulators = new ArrayList<ASimulationControl>();

		// Make name unique
		simulator.setName(getUniqueInputObjectName(simulator.getName()));
		// Add to component list
		getInstance().simulators.add(simulator);
	}

	/**
	 * Creates a new simulator by its name
	 * 
	 * @param type Simulator type (class name)
	 * @param name Simulator name
	 * @param unit Simulator unit
	 * @return {@link ASimulationControl} with the simulator
	 * @throws Exception
	 */
	public static ASimulationControl createNewInputObject(String type, String name, SiUnit unit) throws Exception {
		Object simulator = null;

		// Try to create and parametrize the object
		try {
			// Get class and constructor objects
			String path = ASimulationControl.class.getPackage().getName();
			Class<?> cl = Class.forName(path + "." + name);
			Constructor<?> co = cl.getConstructor(String.class, SiUnit.class);
			// initialize new component
			simulator = co.newInstance(type, unit);
		} catch (Exception e) {
			throw new Exception(
					"Unable to create component " + name + "(" + unit.toString() + ")" + " : " + e.getMessage());
		}

		return (ASimulationControl) simulator;
	}

	/**
	 * Adds a new simulator by its name
	 * 
	 * @param name Simulator name
	 * @param unit Simulator unit
	 * @return {@link ASimulationControl} with the simulator
	 */
	public static ASimulationControl addNewInputObject(String name, SiUnit unit) {

		String uniqueName = getUniqueInputObjectName(name);

		final String path = PropertiesHandler.getProperty("app.MachineComponentDBPathPrefix") + "/";
		Path source = Paths.get(path + "/SimulationControl/" + name + "_Example.xml");
		Path target = Paths.get(EModSession.getMachineConfigDirPath() + "/", name + "_" + uniqueName + ".xml");
		// overwrite existing file, if existsR
		CopyOption[] options = new CopyOption[] { StandardCopyOption.REPLACE_EXISTING,
				StandardCopyOption.COPY_ATTRIBUTES };
		try {
			Files.copy(source, target, options);
		} catch (IOException e) {
			e.printStackTrace();
		}

		ASimulationControl simulator = null;

		try {
			simulator = createNewInputObject(uniqueName, name, unit);
			addInputObject(simulator);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return simulator;
	}

	/**
	 * Adds a new (non existent) control element by its model type set.
	 * 
	 * @param mdlType model type name (same as class name)
	 * @return created machine component object
	 */
	public static MachineComponent addNewControl(String mdlType) {

		Object component = null;

		// Try to create and parametrize the object
		try {
			// Get class and constructor objects
			String path = APhysicalComponent.class.getPackage().getName();
			Class<?> cl = Class.forName(path + ".control." + mdlType);
			Constructor<?> co = cl.getConstructor(String.class);
			// initialize new component
			component = co.newInstance("Example");
		} catch (Exception e) {
			Exception ex = new Exception("Unable to create component " + mdlType + " : " + e.getMessage());
			ex.printStackTrace();
			return null;
		}

		// Create new machine component object
		MachineComponent mc = new MachineComponent(mdlType);
		mc.setComponent((APhysicalComponent) component);

		// Add to machine
		addMachineComponent(mc);

		return mc;

	}

	/**
	 * Removes the component with the given name
	 * 
	 * @param mc Machine Component object
	 * @return success
	 */
	public static boolean removeMachineComponent(MachineComponent mc) {

		if (null != getInstance().getIOLinkList())
			getInstance().removeConnections(mc);

		// Try to remove the component
		if (!getMachineComponentList().remove(mc)) {
			Exception ex = new Exception(
					"Unable to remove component " + mc.getName() + " : Can't remove component from list");
			ex.printStackTrace();
			return false;
		}

		return true;

	}

	/**
	 * Removes the component with the given name
	 * 
	 * @param name Name of the component
	 * @return success
	 */
	public static boolean removeMachineComponent(String name) {
		// Check if component exists
		if (null == getMachineComponent(name)) {
			Exception ex = new Exception("Unable to remove component " + name + " : No component with this name");
			ex.printStackTrace();
			return false;
		} else
			return removeMachineComponent(getMachineComponent(name));
	}

	/**
	 * Removes the simulator with the given name
	 * 
	 * @param sc {@link ASimulationControl} simulator object
	 * @return sucess
	 */
	public static boolean removeInputObject(ASimulationControl sc) {

		if (null != getInstance().getIOLinkList())
			getInstance().removeConnections(sc);

		// Try to remove the component
		if (!getInstance().getInputObjectList().remove(sc)) {
			Exception ex = new Exception(
					"Unable to remove component " + sc.getName() + " : Can't remove component from list");
			ex.printStackTrace();
			return false;
		}

		return true;
	}

	/**
	 * Removes the simulator with the given name
	 * 
	 * @param name Name of the simulator
	 * @return success
	 */
	public static boolean removeInputObject(String name) {
		// Check if component exists
		if (null == getInputObject(name)) {
			Exception ex = new Exception("Unable to remove simulator " + name + " : No simulator with this name");
			ex.printStackTrace();
			return false;
		} else
			return removeInputObject(getInputObject(name));
	}

	/**
	 * Returns the io links for the stated machine component
	 * 
	 * @param mc
	 * @return
	 */
	public static ArrayList<IOConnection> getIOLinks(MachineComponent mc) {
		ArrayList<IOConnection> ioc = new ArrayList<IOConnection>();

		for (int i = 0; i < mc.getComponent().getInputs().size(); i++) {
			// Go through all links, at test if current input is part of it
			for (int j = 0; j < getInstance().getIOLinkList().size(); j++) {
				if (getInstance().getIOLinkList().get(j).getSource().equals(mc.getComponent().getInputs().get(i))
						| getInstance().getIOLinkList().get(j).getTarget().equals(mc.getComponent().getInputs().get(i)))
					ioc.add(getInstance().getIOLinkList().get(j));
			}
		}

		for (int i = 0; i < mc.getComponent().getOutputs().size(); i++) {
			// Go through all links, at test if current input is part of it
			for (int j = 0; j < getInstance().getIOLinkList().size(); j++) {
				if (getInstance().getIOLinkList().get(j).getSource().equals(mc.getComponent().getOutputs().get(i))
						| getInstance().getIOLinkList().get(j).getTarget()
								.equals(mc.getComponent().getOutputs().get(i)))
					ioc.add(getInstance().getIOLinkList().get(j));
			}
		}

		return ioc;
	}

	/**
	 * Returns the io links for the stated simulation control
	 * 
	 * @param sc
	 * @return
	 */
	public static ArrayList<IOConnection> getIOLinks(ASimulationControl sc) {
		ArrayList<IOConnection> ioc = new ArrayList<IOConnection>();

		for (int j = 0; j < getInstance().getIOLinkList().size(); j++) {
			if (getInstance().getIOLinkList().get(j).getSource().equals(sc.getOutput()))
				ioc.add(getInstance().getIOLinkList().get(j));
		}

		return ioc;
	}

	/**
	 * Removes all the IOConnections of the stated machine component from the list
	 * 
	 * @param mc Machine component object
	 */
	public void removeConnections(MachineComponent mc) {

		// Inputs
		try {
			for (int i = 0; i < mc.getComponent().getInputs().size(); i++) {
				// Go through all links, at test if current input is part of it
				// If so, delete it
				for (int j = 1; j < getInstance().getIOLinkList().size(); j++) {
					if (getInstance().getIOLinkList().get(j).getSource().equals(mc.getComponent().getInputs().get(i))
							|| getInstance().getIOLinkList().get(j).getTarget()
									.equals(mc.getComponent().getInputs().get(i)))
						getInstance().getIOLinkList().remove(j);
				}
			}
		} catch (Exception x) {
			Exception ex = new Exception("Unable to remove link on input list");
			ex.printStackTrace();
			return;
		}

		// Outputs
		try {
			for (int i = 0; i < mc.getComponent().getOutputs().size(); i++) {
				// Go through all links, at test if current input is part of it
				// If so, delete it
				for (int j = 0; j < getInstance().getIOLinkList().size(); j++) {
					if (getInstance().getIOLinkList().get(j).getSource().equals(mc.getComponent().getOutputs().get(i))
							|| getInstance().getIOLinkList().get(j).getTarget()
									.equals(mc.getComponent().getOutputs().get(i)))
						getInstance().getIOLinkList().remove(j);
				}
			}
		} catch (Exception x) {
			Exception ex = new Exception("Unable to remove link on output list");
			ex.printStackTrace();
			return;
		}
	}

	/**
	 * Removes all the IOConnections of the stated simulator from the list
	 * 
	 * @param sc Simulator object
	 */
	public void removeConnections(ASimulationControl sc) {

		try {
			for (int j = 0; j < getInstance().getIOLinkList().size(); j++) {
				if (getInstance().getIOLinkList().get(j).getSource().equals(sc.getOutput()))
					getInstance().getIOLinkList().remove(j);
			}
		} catch (Exception x) {
			Exception ex = new Exception("Unable to remove link on input list");
			ex.printStackTrace();
			return;
		}
	}

	/**
	 * Renames the component with the given name to a new name
	 * 
	 * @param name    Old name
	 * @param newname New name
	 */
	public static void renameMachineComponent(String name, String newname) {

		// Make new name name save
		newname = newname.replaceAll("[\\000]", "");
		newname = newname.replace("/", "");
		newname = newname.replace("\\", "");

		MachineComponent mc = getMachineComponent(name);

		if (null == mc) {
			Exception ex = new Exception("Unable to rename component " + name + " : No component with this name");
			ex.printStackTrace();
			return;
		}
		// No rename required if new and old name are the same
		else if (name.equals(newname))
			return;
		else {
			mc.setName(getUniqueComponentName(newname));

			for (IOConnection ioc : Machine.getIOLinks(mc))
				ioc.updateNames();
		}

	}

	/**
	 * Renames the input object with the given name to a new name
	 * 
	 * @param name    Old name
	 * @param newname New name
	 */
	public static void renameInputObject(String name, String newname) {

		// Make new name name save
		newname = newname.replaceAll("[\\000]", "");
		newname = newname.replace("/", "");
		newname = newname.replace("\\", "");

		ASimulationControl sc = getInputObject(name);

		if (null == sc) {
			Exception ex = new Exception("Unable to rename input " + name + " : No input with this name");
			ex.printStackTrace();
			return;
		}
		// No renamerenameInputObject required if new and old name are the same
		else if (name.equals(newname))
			return;
		else {
			sc.setName(getUniqueInputObjectName(newname));

			for (IOConnection ioc : Machine.getIOLinks(sc))
				ioc.updateNames();

			// change the name of file that belongs to the given simulator
			String prefix = EModSession.getRootPath() + File.separator + Defines.MACHINECONFIGDIR + File.separator
					+ EModSession.getMachineConfig() + File.separator;

			Path source = Paths.get(prefix, sc.getType() + "_" + name + ".xml");

			// overwrite existing file, if exists
			CopyOption[] options = new CopyOption[] { StandardCopyOption.REPLACE_EXISTING, };
			// try to rename the existing xml-file of the simulator to the new name
			try {
				Files.copy(source, source.resolveSibling(sc.getType() + "_" + newname + ".xml"), options);
			} catch (IOException ee) {
				ee.printStackTrace();
			}
		}
	}

	/**
	 * Adds a new IOConnection between the source and the target
	 * 
	 * @param source
	 * @param target
	 * @return ioc
	 */
	public static IOConnection addIOLink(IOContainer source, IOContainer target) {

		IOConnection ioc = null;

		// Add Element to List
		if (machineModel == null)
			machineModel = new Machine();
		if (null == getInstance().getIOLinkList())
			getInstance().connectionList = new ArrayList<IOConnection>();

		// Create new IOConnection
		try {
			if (source instanceof FluidContainer & target instanceof FluidContainer)
				ioc = new FluidConnection((FluidContainer) source, (FluidContainer) target);
			else
				ioc = new IOConnection(source, target);

			getInstance().getIOLinkList().add(ioc);
			ioc.createIOConnection();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return ioc;

	}

	/**
	 * Removes the first IOConnection in the list containing the target stated in
	 * the argument
	 * 
	 * @param target
	 */
	public static void removeIOLink(IOContainer target) {
		// Find candidates to be removed
		for (IOConnection ioc : getInstance().connectionList)
			if (ioc.getTarget().equals(target)) {
				getInstance().connectionList.remove(ioc);
				// Break after first candidate
				break;
			}
	}

	/**
	 * Remove the stated ioc
	 * 
	 * @param ioc
	 */
	public static void removeIOLink(IOConnection ioc) {
		getInstance().connectionList.remove(ioc);
	}

	/**
	 * adds a new FluidConnection to the machine
	 * 
	 * @param source
	 * @param target
	 */
	public static void addFluidLink(FluidContainer source, FluidContainer target) {
		FluidConnection fio;

		try {
			fio = new FluidConnection(source, target);
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}

		// Add Element to List
		if (machineModel == null)
			machineModel = new Machine();
		if (null == getInstance().getIOLinkList())
			getInstance().connectionList = new ArrayList<IOConnection>();

		getInstance().getIOLinkList().add(fio);
	}

	/**
	 * @return {@link IOContainer} List of all components and simulators outputs
	 */
	public static ArrayList<String> getOutputList() {
		return getOutputList(null, null);
	}

	/**
	 * @param unit {@link Unit} Unit of the outputs
	 * @return {@link IOContainer} List of all components and simulators outputs
	 *         with the declared unit
	 */
	public static ArrayList<String> getOutputList(SiUnit unit) {
		return getOutputList(null, unit);
	}

	/**
	 * Return a list of all IOContainers with the unit which do not include Outputs
	 * of the stated machine component mc. Both conditions are ignored when feeding
	 * null.
	 * 
	 * @param mc_excl {@link MachineComponent} who's elements are to be excluded
	 * @param unit    {@link Unit} of the desired outputs
	 * @return List of {@link IOContainer}
	 */
	public static ArrayList<String> getOutputList(MachineComponent mc_excl, SiUnit unit) {
		ArrayList<String> outputs = new ArrayList<String>();

		// Get all machine components
		ArrayList<MachineComponent> components = getMachineComponentList();
		// Get all simulator outputs
		List<ASimulationControl> simulators = getInstance().getInputObjectList();

		// Fetch all outputs
		if (null != components)
			for (MachineComponent mc : components)
				if (!mc.equals(mc_excl))
					for (IOContainer ic : mc.getComponent().getOutputs())
						if (!ic.equals(null) & (ic.getUnit().equals(unit) | null == unit)
								& !(ic instanceof FluidContainer))
							outputs.add(mc.getName() + "." + ic.getName());

		if (null != simulators)
			for (ASimulationControl sc : simulators)
				if (sc.getOutput().getUnit().equals(unit) | null == unit)
					outputs.add(sc.getName());

		return outputs;
	}

	/**
	 * Get a list of all names of the fluid inpputs available, except the one of the
	 * stated machine component
	 * 
	 * @param mc_excl
	 * @return
	 */
	public static ArrayList<String> getFluidOutputList(MachineComponent mc_excl) {
		ArrayList<String> outputs = new ArrayList<String>();

		// Get all machine components
		ArrayList<MachineComponent> components = getMachineComponentList();

		// Fetch all outputs
		if (null != components)
			for (MachineComponent mc : components)
				if (!mc.equals(mc_excl))
					for (IOContainer ic : mc.getComponent().getOutputs())
						if (ic instanceof FluidContainer)
							outputs.add(mc.getName() + "." + ic.getName());

		return outputs;
	}

	/**
	 * Returns a List of all {@link DynamicState}
	 * 
	 * @return {@link DynamicState}
	 */
	public static ArrayList<DynamicState> getDynamicStatesList() {
		ArrayList<DynamicState> output = new ArrayList<DynamicState>();

		for (MachineComponent mc : Machine.getInstance().componentList) {
			if (mc.getComponent().getDynamicStateList() != null)
				output.addAll(mc.getComponent().getDynamicStateList());
		}

		return output;
	}

	/**
	 * Returns the dynamic state "name" of the component "parent"
	 * 
	 * @param name
	 * @param parent
	 * @return {@link DynamicState}
	 */
	public DynamicState getDynamicState(String name, String parent) {
		return getMachineComponent(parent).getComponent().getDynamicState(name);
	}

	/**
	 * Returns the full output name of the container
	 * 
	 * @param container {@link IOContainer}
	 * @return name {@link String}
	 */
	public static String getOutputFullName(IOContainer container) {
		String out = null;

		for (MachineComponent mc : getMachineComponentList()) {
			for (IOContainer io : mc.getComponent().getOutputs()) {
				if (io.equals(container.getReference())) {
					out = mc.getName() + "." + io.getName();
					break;
				}
			}
			if (out != null)
				break;
		}

		if (out == null)
			for (ASimulationControl sc : getInstance().getInputObjectList()) {
				if (sc.getOutput().equals(container.getReference())) {
					out = sc.getName();
					break;
				}
			}

		return out;
	}
	
	/**
	 * Removes the library Links for all the components
	 * 
	 */
	public static void removeAllLibraryLinks() {
		for (MachineComponent mc : Machine.getInstance().componentList)
			mc.getComponent().setLibraryLink(false);
	}

	/**
	 * ========================================================================
	 * ======== Methods for test only! ==========================================
	 * ======================================
	 */

	/**
	 * Create machine instance. For test purpose only!
	 * 
	 */
	public static void dummyBuildMachine() {
		/* Create machine */
		if (machineModel == null)
			machineModel = new Machine();
	}

	/**
	 * Set or overwrite componentList: Use for test purpose only!
	 * 
	 * @param list
	 */
	public void setMachineComponentList(ArrayList<MachineComponent> list) {
		componentList = list;
	}

}
