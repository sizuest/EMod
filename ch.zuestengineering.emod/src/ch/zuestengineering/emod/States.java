/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.StringTokenizer;
import java.util.logging.Logger;

import ch.zuestengineering.emod.simulation.MachineState;
import ch.zuestengineering.emod.simulation.SimulationState;
import ch.zuestengineering.emod.utils.Defines;
import ch.zuestengineering.emod.utils.PropertiesHandler;

/**
 * Implements a state sequence
 * 
 * @author sizuest
 * 
 */
public class States {

	private static Logger logger = Logger.getLogger(SimulationState.class.getName());

	private static States statesMap = null;

	/* Array for Time states */
	private ArrayList<TimeStateMapper<MachineState>> timeStateMap = new ArrayList<TimeStateMapper<MachineState>>();

	/**
	 * Private constructor for singleton implementation.
	 */
	private States() {
	}

	/**
	 * singleton implementation of the machine model
	 * 
	 * @return instance of the machine model
	 */
	public static States getInstance() {
		if (statesMap == null) {
			logger.info("No state map existing: Creating empty state map");
			statesMap = new States();
		}
		return statesMap;
	}

	/**
	 * Returns the stateMap
	 * 
	 * @return StateMap
	 */
	public static ArrayList<TimeStateMapper<MachineState>> getStateMap() {
		return getInstance().timeStateMap;
	}

	/**
	 * Read the states for the currently selected configuration
	 */
	public static void readStates() {
		statesMap = new States();

		/*
		 * Generate file name with path: e.g.
		 * Machines/NDM200/MachineConfig/TestConfig1/MachineStateSequence.txt
		 */
		String file = EModSession.getStateSequenceConfigPath();

		getInstance().readStatesFromFile(file);
	}

	/**
	 * reads machine states from file.
	 * <p>
	 * syntax: time[s],{@link SimulationState};time[s],{@link SimulationState}
	 * ;...;<br />
	 * Comment lines begin with '#'.<br />
	 * Spaces are allowed.
	 * 
	 * @param file
	 */
	private void readStatesFromFile(String file) {

		logger.info("Read machine state sequence from file: " + file);

		double rtime = 0.0;
		double rduration;
		int linenr = 0;
		timeStateMap = new ArrayList<TimeStateMapper<MachineState>>();

		try {
			BufferedReader input = new BufferedReader(new FileReader(file));
			String line = null;

			while ((line = input.readLine()) != null) {
				// tokenize & append
				linenr++;

				// A comment is identified by a leading '#'.
				// Remove comments (regex can be use only by replaceAll())
				String l = line.replaceAll("#.*", "").replace("\t", " ").trim();

				/* (time,state)-pairs are separated by ';'. */
				StringTokenizer st = new StringTokenizer(l, ";");

				while (st.hasMoreTokens()) {
					// time and state are separated by a ',':
					StringTokenizer str = new StringTokenizer(st.nextToken().trim(), ",");
					rduration = Double.parseDouble(str.nextToken().trim());
					rtime += rduration;
					String state = str.nextToken().trim();
					MachineState ms = MachineState.valueOf(state);
					timeStateMap.add(new TimeStateMapper<MachineState>(rtime, rduration, ms));
				}
			}
			input.close();
		} catch (Exception e) {
			logger.severe("Format error in file '" + file + "' line " + linenr);
			e.printStackTrace();
			removeAllStates();
		}
	}

	/**
	 * Save states for the stated configuration
	 * 
	 * @param machineName
	 * @param simConfigName
	 */
	public static void saveStates(String machineName, String simConfigName) {

		/*
		 * Generate file name with path: e.g.
		 * Machines/NDM200/MachineConfig/TestConfig1/MachineStateSequence.txt
		 */
		String prefix = PropertiesHandler.getProperty("app.MachineDataPathPrefix");
		String file = prefix + File.separator + machineName + File.separator + Defines.SIMULATIONCONFIGDIR
				+ File.separator + simConfigName + File.separator + Defines.MACHINESTATEFNAME;

		getInstance().saveStatesToFile(file);
	}

	private void saveStatesToFile(String file) {

		logger.info("Saving machine state sequence to file: " + file);

		try {
			BufferedWriter output = new BufferedWriter(new FileWriter(file));

			for (TimeStateMapper<MachineState> ts : getInstance().timeStateMap) {
				// Write Line
				output.append(ts.Duration + ", " + ts.State.toString() + "\n");
			}
			output.close();
		} catch (Exception e) {
			logger.severe("Writing error in file '" + file + "'");
		}
	}

	/**
	 * Returns the simulation time for the provided index
	 * 
	 * @param index
	 * @return Time [s]
	 */
	public static Double getTime(int index) {
		if (index >= getInstance().timeStateMap.size() | index < 0)
			return Double.NaN;

		return getInstance().timeStateMap.get(index).Time;
	}

	/**
	 * Returns the duration for the provided time step
	 * 
	 * @param index
	 * @return Time
	 */
	public static Double getDuration(int index) {
		if (index >= getInstance().timeStateMap.size() | index < 0)
			return Double.NaN;

		return getInstance().timeStateMap.get(index).Duration;
	}

	/**
	 * Returns the state for the provided time step
	 * 
	 * @param index
	 * @return Time
	 */
	public static MachineState getState(int index) {
		if (index >= getInstance().timeStateMap.size() | index < 0)
			return MachineState.OFF;

		return getInstance().timeStateMap.get(index).State;
	}

	/**
	 * Inserts a new state at the provided index
	 * 
	 * @param index
	 * @param duration
	 * @param state
	 */
	public static void insertState(int index, double duration, MachineState state) {

		getInstance().timeStateMap.add(index, new TimeStateMapper<MachineState>(Double.NaN, duration, state));
		getInstance().updateTime();
	}

	/**
	 * Removes all states
	 */
	public static void removeAllStates() {
		getInstance().timeStateMap.clear();
	}

	/**
	 * Append a new state at the end of the state list
	 * 
	 * @param duration
	 * @param state
	 */
	public static void appendState(double duration, MachineState state) {
		getInstance().timeStateMap.add(new TimeStateMapper<MachineState>(Double.NaN, duration, state));
		getInstance().updateTime();
	}

	/**
	 * Switches places with the previous state
	 * 
	 * @param index
	 */
	public static void moveStateUp(int index) {
		if (index == 0)
			return;

		Collections.swap(getInstance().timeStateMap, index, index - 1);
		getInstance().updateTime();
	}

	/**
	 * Switches places with the next state
	 * 
	 * @param index
	 */
	public static void moveStateDown(int index) {
		if (getInstance().timeStateMap.size() - 1 == index)
			return;

		Collections.swap(getInstance().timeStateMap, index, index + 1);
		getInstance().updateTime();
	}

	/**
	 * @return Number of states
	 */
	public static int getStateCount() {
		int n = getInstance().timeStateMap.size();
		return n;
	}

	/**
	 * Sets (an existing) state
	 * 
	 * @param index
	 * @param duration
	 * @param state
	 */
	public static void setState(int index, double duration, MachineState state) {
		getInstance().timeStateMap.get(index).Duration = duration;
		getInstance().timeStateMap.get(index).State = state;
		getInstance().updateTime();
	}

	private void updateTime() {
		double time = 0;

		for (TimeStateMapper<MachineState> tsm : getInstance().timeStateMap) {
			time += tsm.Duration;
			tsm.Time = time;
		}
	}

	/**
	 * Returns the total duration of the given state
	 * 
	 * @param state
	 * @return
	 */
	public static double getTotalDuration(MachineState state) {
		double duration = 0;

		for (TimeStateMapper<MachineState> t : getStateMap())
			if (t.State.equals(state))
				duration += t.Duration;

		return duration;
	}

}

/**
 * @author andreas
 * 
 * @param <S>
 */
class TimeStateMapper<S> {
	public double Time, Duration;
	public S State;

	public TimeStateMapper(double t, S s) {
		Time = t;
		State = s;
		Duration = Double.NaN;
	}

	public TimeStateMapper(double t, double d, S s) {
		Time = t;
		State = s;
		Duration = d;
	}
}
