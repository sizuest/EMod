/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.analysis;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import ch.zuestengineering.emod.Machine;
import ch.zuestengineering.emod.gui.EModStatusBarGUI;
import ch.zuestengineering.emod.gui.utils.ConsumerData;
import ch.zuestengineering.emod.model.MachineComponent;
import ch.zuestengineering.emod.model.linking.IOConnection;
import ch.zuestengineering.emod.model.linking.IOContainer;
import ch.zuestengineering.emod.model.pm.ForcedFluidFlow;
import ch.zuestengineering.emod.model.units.SiUnit;
import ch.zuestengineering.emod.simulation.MachineState;

/**
 * abstract class to represent a simulation result and the i/o handling
 * 
 * @author dhampl
 * 
 */
public class SimulationResult {

	private static Logger logger = Logger.getLogger(SimulationResult.class.getName());
	private String dataFile;
	protected List<ConsumerData> availableConsumers = new ArrayList<ConsumerData>();
	private List<String[]> lines;

	private double[] time;
	private MachineState[] states;

	/**
	 * @param dataFile
	 */
	public SimulationResult(String dataFile) {
		this.dataFile = dataFile;
	}

	/**
	 * 
	 */
	public SimulationResult() {
		this.dataFile = "";
	}

	/**
	 * Set the path of the data file
	 * 
	 * @param path
	 */
	public void setDataFile(String path) {
		this.dataFile = path;
	}

	/**
	 * @return
	 */
	public String getDataFile() {
		return dataFile;
	}

	/**
	 * Returns the average power demand of the machine during a given state
	 * 
	 * @param state
	 * @return
	 */
	public double getAveragePowerDemand(MachineState state) {
		double sum = 0;

		for (ConsumerData cd : availableConsumers) {
			if (!isConnected(cd.getConsumer(), "PTotal"))
				sum += cd.getAverage("PTotal", state);
		}

		return sum;
	}

	/**
	 * Returns the average amount of heat extracted by a fluid in the given state
	 * 
	 * @param state
	 * @return
	 */
	public double getAverageFluidHeatFlux(MachineState state) {
		double sum = 0;

		for (ConsumerData cd : availableConsumers) {
			if (isFluidHeatExtract(cd.getConsumer()))
				sum += cd.getAverage("PThermal", state);
		}

		return sum;
	}

	/**
	 * reads the data from a specified datafile
	 */
	public void readData() {
		if (this.dataFile.equals(""))
			return;

		availableConsumers = new ArrayList<ConsumerData>();
		lines = new ArrayList<String[]>();

		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new FileReader(dataFile));

			String line = null;
			while ((line = reader.readLine()) != null) {
				lines.add(line.split("\t")); // reading and
												// splitting the
												// first line
			}
			reader.close();

		} catch (Exception e) {
			logger.severe("Result file " + dataFile + " is non existent");
		}

		// Check if result file is non-empty
		if (0 == lines.size())
			logger.info("Result file " + dataFile + " is empty");
		else {
			String[] headerLine = lines.get(0);

			// Find time & state vector
			for (int i = 0; i < headerLine.length; i++) {
				String token = headerLine[i];
				token = token.trim();
				if (token.equals("Time")) {
					getTimeVector(i);
					continue;
				}
				if (token.equals("State"))
					getStateVector(i);
				break;
			}

			// Add consumers
			for (int i = 0; i < headerLine.length; i++) {
				String token = headerLine[i];
				token = token.trim();
				if (token.equals("Time"))
					continue;
				if (token.equals("State"))
					continue;
				if (token.contains("Sim"))
					continue;
				String consumer = token.replaceAll("-(Output|Input|State|FluidCircuit|BC)-[0-9]+(-[pVT])?", "").trim();
				if (!consumerExists(consumer))
					createConsumer(consumer, i);
				else
					addDataToConsumer(consumer, i);

				EModStatusBarGUI.getProgressBar().updateProgressbar(i * 100 / headerLine.length);
			}
		}

		for (ConsumerData cd : availableConsumers) {
			cd.calculateEnergy();
			cd.calculate();
		}
	}

	/**
	 * check whether a given consumer already exists.
	 * 
	 * @param consumer
	 * @return true if consumer is already present in availableConsumers list
	 */
	public boolean consumerExists(String consumer) {
		boolean result = false;
		for (ConsumerData cd : availableConsumers) {
			if (cd.getConsumer().equals(consumer)) {
				result = true;
				break;
			}
		}
		return result;
	}

	private void getTimeVector(int col) {
		double[] values = new double[lines.size() - 3];
		for (int i = 4; i < lines.size(); i++)
			try {
				values[i - 4] = Double.parseDouble(lines.get(i)[col]);
			} catch (Exception e) {
				logger.warning(
						"Result file: Could not parse entier result file. Line " + i + " failed due to bad format.");
				e.printStackTrace();
			}

		time = values;
	}

	private void getStateVector(int col) {
		MachineState[] values = new MachineState[lines.size() - 3];
		for (int i = 4; i < lines.size(); i++)
			try {
				values[i - 4] = MachineState.valueOf(lines.get(i)[col]);
			} catch (Exception e) {
				logger.warning(
						"Result file: Could not parse entier result file. Line " + i + " failed due to bad format.");
				e.printStackTrace();
			}

		states = values;
	}

	/**
	 * creates a new consumer with a specified name
	 * 
	 * @param name
	 * @param col  column in the data file
	 */
	private void createConsumer(String name, int col) {
		ConsumerData data = new ConsumerData(name, time, states);
		String ioName = lines.get(1)[col].replace(name, "");
		ioName = ioName.substring(1);
		data.addName(ioName);
		String unit = lines.get(2)[col].replace("[", "").replace("]", "");
		data.addUnit(new SiUnit(unit));
		double[] values = new double[lines.size() - 4];
		for (int i = 4; i < lines.size(); i++)
			try {
				values[i - 4] = Double.parseDouble(lines.get(i)[col]);
			} catch (Exception e) {
				logger.warning(
						"Result file: Could not parse entier result file. Line " + i + " failed due to bad format.");
				e.printStackTrace();
			}
		data.addInputValues(values);
		availableConsumers.add(data);
	}

	/**
	 * adds sample values from the datafile to a consumer
	 * 
	 * @param consumer parent structure for the data
	 * @param col      column in the data file
	 */
	private void addDataToConsumer(String consumer, int col) {
		ConsumerData temp = null;
		for (ConsumerData cd : availableConsumers) {
			if (cd.getConsumer().equals(consumer)) {
				temp = cd;
				break;
			}
		}
		String ioName = lines.get(1)[col].replace(consumer, "");
		ioName = ioName.substring(1);
		temp.addName(ioName);
		String unit = lines.get(2)[col].replace("[", "").replace("]", "");
		temp.addUnit(new SiUnit(unit));
		double[] values = new double[lines.size() - 4];
		for (int i = 4; i < lines.size(); i++)
			try {
				values[i - 4] = Double.parseDouble(lines.get(i)[col]);
			} catch (Exception e) {
				logger.warning(
						"Result file: Could not parse entier result file. Line " + i + " failed due to bad format.");
				e.printStackTrace();
			}
		temp.addInputValues(values);
	}

	/**
	 * 
	 * @return list with {@link ConsumerData} elements
	 */
	public List<ConsumerData> getConsumerDataList() {
		return availableConsumers;
	}

	private boolean isConnected(String componentName, String outputName) {

		MachineComponent mc = Machine.getMachineComponent(componentName);
		if (null == mc)
			return false;

		IOContainer io = mc.getComponent().getOutput(outputName);
		if (null == io)
			return false;

		for (IOConnection ioc : Machine.getIOLinks(mc))
			if (ioc.getSource().equals(io))
				return true;

		return false;

	}

	/**
	 * Returns true if the component extracts heat bound to a fluid
	 * 
	 * @param consumer
	 * @return
	 */
	private boolean isFluidHeatExtract(String componentName) {
		MachineComponent mc = Machine.getMachineComponent(componentName);
		if (null == mc)
			return false;

		IOContainer io = mc.getComponent().getOutput("PThermal");
		if (null == io)
			return false;

		if (mc.getComponent() instanceof ForcedFluidFlow)
			return true;

		return false;
	}
}
