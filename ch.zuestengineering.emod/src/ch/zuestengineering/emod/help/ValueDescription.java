/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.help;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * Implements the generic description of a value or a signal
 * 
 * @author simon
 *
 */
@XmlRootElement
public class ValueDescription {

	@XmlElement
	protected String name;
	@XmlElement
	protected String symbol = "";
	@XmlElement
	protected String unit;
	@XmlElement
	protected String description;

	/**
	 * For unmarshaller
	 */
	public ValueDescription() {
	}

	/**
	 * @param name
	 * @param unit
	 * @param description
	 */
	public ValueDescription(String name, String unit, String description) {
		this.name = name;
		this.unit = unit;
		this.description = description;
	}

	/**
	 * Returns the whole description ready to be displayed as a row in a html table
	 * 
	 * @return
	 */
	public String getHTMLTableEntry() {
		return "\t\t<tr><td><i>" + name + "</i></td><td>\\(" + symbol + "\\)</td><td>" + unit + "</td><td>"
				+ description + "</td></tr>\n";
	}

	/**
	 * Returns an array of ValueDescirpions as HTML table
	 * 
	 * @param values
	 * @return
	 */
	public static String getAsHTMLTable(ArrayList<ValueDescription> values) {
		String ret = "<table>\n";

		ret += "\t<tr>\n<td><b>Parameter</b></td><td><b>Symbol</b></td><td><b>Unit</b></td><td><b>Description</b></td></tr>\n";

		for (ValueDescription vd : values)
			ret += vd.getHTMLTableEntry();

		ret += "</table>";

		return ret;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @param unit the unit to set
	 */
	public void setUnit(String unit) {
		this.unit = unit;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @param symbol the symbol to set
	 */
	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	/**
	 * @return the name
	 */
	@XmlTransient
	public String getName() {
		return name;
	}

	/**
	 * @return the symbol
	 */
	@XmlTransient
	public String getSymbol() {
		return symbol;
	}

	/**
	 * @return the unit
	 */
	@XmlTransient
	public String getUnit() {
		return unit;
	}

	/**
	 * @return the description
	 */
	@XmlTransient
	public String getDescription() {
		return description;
	}
}
