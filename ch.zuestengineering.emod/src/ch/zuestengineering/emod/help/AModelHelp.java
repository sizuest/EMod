/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.help;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import ch.zuestengineering.emod.utils.PropertiesHandler;

/**
 * Implements a basic help file
 * 
 * @author simon
 *
 */
@XmlRootElement
public abstract class AModelHelp {

	@XmlElement
	protected String title;
	@XmlElement
	protected String content;
	@XmlElement
	private String image = "";
	@XmlElement
	protected String type;

	/**
	 * @return the title
	 */
	@XmlTransient
	public String getTitle() {
		return title;
	}

	/**
	 * @return the content
	 */
	@XmlTransient
	public String getContent() {
		return content;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @param content the content to set
	 */
	public void setContent(String content) {
		this.content = content;
	}

	/**
	 * Returns the path of the image
	 * 
	 * @return
	 */
	public String getImagePath() {
		return "img/" + getImage();
	}

	/**
	 * Returns true if an image is defined
	 * 
	 * @return
	 */
	public boolean hasImage() {
		return !getImage().matches("");
	}

	/**
	 * Returns the html code to show the model's image
	 * 
	 * @return
	 */
	public String getImageHTML() {
		return "<img src=\"" + getImagePath() + "\">";
	}

	/**
	 * Returns the html code to show the model's image
	 * 
	 * @param height image height in px
	 * @return
	 */
	public String getImageHTML(int height) {
		return "<img style=\"width: auto; height: " + height + "px\" src=\"" + getImagePath() + "\">";
	}

	/**
	 * Returns the html code to show the model's image
	 * 
	 * @param scale
	 * @return
	 */
	public String getImageHTML(double scale) {
		return "<img onload=\"this.width*=" + scale + ";this.onload=null;\" src=\"" + getImagePath() + "\">";
	}

	/**
	 * Returns the path of the model documentation for the given model type
	 * 
	 * @param type
	 * @return
	 */
	public static String getPath(String type) {
		String filePath;

		String path_prefix = PropertiesHandler.getProperty("app.HelpPathPrefix");
		filePath = path_prefix + "doc_en_US_" + type + ".xml";

		return filePath;
	}

	/**
	 * Returns the assigned type
	 * 
	 * @return
	 */
	public String getType() {
		return type;
	}

	/**
	 * Returns the image's name
	 * 
	 * @return
	 */
	public String getImage() {
		return image;
	}

	/**
	 * Sets the image's name
	 * 
	 * @param image
	 */
	@XmlTransient
	public void setImage(String image) {
		this.image = image;
	}

	/**
	 * Retuns the formated html content
	 * 
	 * @return
	 */
	public abstract String getHTMLContent();

	/**
	 * Compares the model with the content of the help topic
	 */
	public abstract void compareToModel();

}
