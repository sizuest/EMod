/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.help;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Provides static functions for basic formatting operations for html help topic
 * printout
 * 
 * @author Simon Z�st
 *
 */
public class HelpFormater {

	/**
	 * Header for each html file to start with. This includes: - Tex2Jax import
	 * (Eq.-formatting) - Help style sheet (css)
	 * 
	 * @return
	 */
	public static String getHTMLHeader() {
		String ret = "<!DOCTYPE html>\n<html>\n" + "\t<head>\n" + "\t\t<script type=\"text/x-mathjax-config\">\n"
				+ "\t\t\tMathJax.Hub.Config({\n" + "\t\t\t\tTeX: { equationNumbers: { autoNumber: \"all\" } }\n"
				+ "\t\t\t});\n" + "\t\t</script>" + "\t\t<script type=\"text/x-mathjax-config\">\n"
				+ "\t\t\tMathJax.Hub.Config({\n"
				+ "\t\t\t\ttex2jax: { inlineMath: [['$','$'], ['\\\\(','\\\\)']], processEscapes: true }\n"
				+ "\t\t\t});\n" + "\t\t</script>"
				+ "\t\t<script src='https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML'></script>\n"
				+ "\t\t<link rel=\"stylesheet\" type=\"text/css\" href=\"file://" + System.getProperty("user.dir")
				+ "/help/style.css\" />\n" + "\t</head>\n" + "\t<body>\n";
		return ret;
	}

	/**
	 * Returns the finalization of a html file
	 * 
	 * @return
	 */
	public static String getHTMLFooter() {
		return "\t\t<div class=\"footer\"><hr><div style=\"float: left; display: table-cell;\">Generated: "
				+ (new SimpleDateFormat("dd.MM.yyyy HH:mm:ss")).format(new Date())
				+ ", � Z�st Engineering AG</div><div id=\"parallelogram\"></div></div>\n\t</body>\n</html>\n";
	}

	/**
	 * Returns the html syntax to represent a help-topic title
	 * 
	 * @param title
	 * @return
	 */
	public static String formatTitle(String title) {
		return formatTitle(title, title);
	}

	/**
	 * Returns the html syntax to represent a help-topic title including a model
	 * type
	 * 
	 * @param title
	 * @param type
	 * @return
	 */
	public static String formatTitle(String title, String type) {
		String ret = "\t\t<div><h1>" + title;
		if (!title.equals(type) && !type.equals(""))
			ret += "<br>(" + type + ")";
		ret += "</h1><hr></div>\n";

		return ret;
	}

	/**
	 * Returns the html syntax to represent a parapgraph of text
	 * 
	 * @param content
	 * @return
	 */
	public static String formatParagraph(String content) {
		String ret = "\t\t<div>" + content + "</div>\n";
		return ret;
	}

	/**
	 * Returns the html syntax to represent a parapgraph of text
	 * 
	 * @param title
	 * @param content
	 * @return
	 */
	public static String formatParagraph(String title, String... content) {
		String ret = "";
		if (title != null)
			ret += "\t\t" + formatSection(title) + "\n";

		for (String c : content)
			ret += "\t\t<div>" + c + "</div>\n";
		return ret;
	}

	/**
	 * Returns the html syntax to represent a paragraph with a table
	 * 
	 * @param title
	 * @param content
	 * @return
	 */
	public static String formatParagraph(String title, ArrayList<ValueDescription> content) {
		if (content.size() == 0)
			return "";

		String ret = "\t\t" + formatSection(title) + "\n";
		ret += "\t\t<div>" + ValueDescription.getAsHTMLTable(content) + "</div>\n";
		return ret;
	}

	/**
	 * Returns the html syntax for a section title
	 * 
	 * @param title
	 * @return
	 */
	public static String formatSection(String title) {
		return "<div><h2>" + title + "</h2></div>";
	}

	/**
	 * creates an index for the given topics
	 * 
	 * @param rootTopic
	 * @return
	 */
	public static String formatIndex(AHelpTopic rootTopic) {
		return formatIndex(rootTopic, true);
	}

	/**
	 * creates an index for the given topics
	 * 
	 * @param rootTopic
	 * @param subTopics
	 * @return
	 */
	public static String formatIndex(AHelpTopic rootTopic, boolean subTopics) {
		return formatIndex(rootTopic, 0, subTopics);
	}

	/**
	 * creates an index for the given topics
	 */
	private static String formatIndex(AHelpTopic rootTopic, int depth, boolean subTopics) {
		String ret = "";
		String tabs = "\t\t\t";
		String type = "";

		for (int i = 0; i < depth; i++)
			tabs += "\t";

		switch (depth) {
		case 1:
			type = "A";
			break;
		case 2:
			type = "1";
			break;
		case 3:
			type = "a";
			break;
		case 4:
			type = "I";
			break;
		case 5:
			type = "i";
			break;
		default:
			type = "1";
		}

		ret = tabs + "<ol type=\"" + type + "\">";

		for (AHelpTopic ht : rootTopic.getChildren()) {
			if (0 == depth)
				ret += tabs + "\t<li><b>" + ht.getHTMLLink() + "</b></li>";
			else
				ret += tabs + "\t<li>" + ht.getHTMLLink() + "</li>";

			if (subTopics)
				ret += formatIndex(ht, depth + 1, true);
		}

		ret = ret + "</ol>";

		return ret;
	}

}
