package ch.zuestengineering.emod.help;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Arrays;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;

/**
 * Implementation of a (inline) help
 * 
 * @author Simon Z�st
 *
 */
@XmlRootElement(namespace = "ch.zuestengineering.emod")
@XmlSeeAlso({ HelpTopicGeneric.class })
@XmlAccessorType(XmlAccessType.FIELD)
public class Help {
	@XmlElement
	private AHelpTopic rootEntry;

	private String path;

	private static Help instance;

	/**
	 * Constructor for unmarshaller
	 */
	private Help() {
	}

	/**
	 * @return
	 */
	public static Help getInstance() {
		if (null == instance)
			load("help/index.xml");

		return instance;
	}

	/**
	 * Saves the help as xml
	 */
	public static void save() {
		try {
			JAXBContext context = JAXBContext.newInstance(Help.class);
			Marshaller m = context.createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

			File path = new File(getInstance().getPath());
			path.getParentFile().mkdirs();
			path.createNewFile();

			Writer w = new FileWriter(path);
			m.marshal(getInstance(), w);
			w.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Loads the help
	 * 
	 * @param path
	 */
	public static void load(String path) {
		try {
			JAXBContext context = JAXBContext.newInstance(Help.class);
			Unmarshaller um = context.createUnmarshaller();
			instance = (Help) um.unmarshal(new FileReader(path));

			instance.rootEntry.setAsRootElement();
			Help.createHTMLFiles();
		} catch (Exception e) {
			e.printStackTrace();
			instance = new Help();
			instance.path = path;
			instance.rootEntry = new HelpTopicGeneric("New Help");
		}
	}

	/**
	 * Returns the path to the xml file
	 * 
	 * @return
	 */
	public String getPath() {
		return path;
	}

	/**
	 * Returns the help topic with the given path. If the path is not valid, the
	 * root element is returned instead
	 * 
	 * @param path
	 * @return
	 */
	public static AHelpTopic getTopic(String path) {
		AHelpTopic out = getRootEntry().getTopic(path);

		if (out == null)
			return getRootEntry();
		else
			return out;
	}

	/**
	 * Returns the help topic with the given path. If the path is not valid, the
	 * root element is returned instead
	 * 
	 * @param path
	 * @return
	 */
	public static AHelpTopic getTopic(String[] path) {
		AHelpTopic out = getRootEntry().getTopic(path);

		if (out == null)
			return getRootEntry();
		else
			return out;
	}

	/**
	 * Returns a list with all available topics in the tree and their sub topics
	 * 
	 * @return
	 */
	public static ArrayList<AHelpTopic> getAllTopics() {
		ArrayList<AHelpTopic> out = new ArrayList<>();
		out.addAll(getRootEntry().getAllChildren());

		return out;
	}

	/**
	 * Returns a list with all available topics in the tree
	 * 
	 * @return
	 */
	public static ArrayList<AHelpTopic> getTopics() {
		return getRootEntry().getChildren();
	}

	/**
	 * Creates the html files for each
	 */
	public static void createHTMLFiles() {
		getInstance().rootEntry.createAllHTML();
	}

	/**
	 * Returns the root element
	 * 
	 * @return
	 */
	public static AHelpTopic getRootEntry() {
		return getInstance().rootEntry;
	}

	/**
	 * Adds a new topic as subtopic of the provieded path
	 * 
	 * @param topic
	 * @param path
	 * @return
	 */
	public static String addTopic(AHelpTopic topic, String path) {
		return getTopic(path).addSubTopic(topic);
	}

	/**
	 * Removes the subtopic (and its children) with the provided path
	 * 
	 * @param path
	 */
	public static void removeTopic(String path) {
		String[] parentPath = path.split("/");

		if (parentPath.length < 2)
			return;

		String topicName = parentPath[parentPath.length - 1];
		parentPath = Arrays.copyOfRange(parentPath, 0, parentPath.length - 1);

		getTopic(parentPath).removeSubTopic(topicName);
	}

	/**
	 * Moves the topic with the provided path up
	 * 
	 * @param path
	 */
	public static void moveTopicUp(String path) {
		String[] parentPath = path.split("/");

		if (parentPath.length < 2)
			return;

		String topicName = parentPath[parentPath.length - 1];
		parentPath = Arrays.copyOfRange(parentPath, 0, parentPath.length - 1);

		getTopic(parentPath).moveSubTopicUp(topicName);
	}

	/**
	 * Moves the topic with the provided path down
	 * 
	 * @param path
	 */
	public static void moveTopicDown(String path) {
		String[] parentPath = path.split("/");

		if (parentPath.length < 2)
			return;

		String topicName = parentPath[parentPath.length - 1];
		parentPath = Arrays.copyOfRange(parentPath, 0, parentPath.length - 1);

		getTopic(parentPath).moveSubTopicDown(topicName);
	}
}
