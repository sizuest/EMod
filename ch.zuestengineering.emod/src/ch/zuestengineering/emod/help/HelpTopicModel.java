/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.help;

import ch.zuestengineering.emod.dd.help.DDModelDescription;
import ch.zuestengineering.emod.dd.help.DDProfileDescription;
import ch.zuestengineering.emod.model.help.MaterialDescription;
import ch.zuestengineering.emod.model.help.ModelDescription;
import ch.zuestengineering.emod.model.help.SimulationControlDescription;

/**
 * @author Simon Z�st
 *
 */
public class HelpTopicModel extends AHelpTopic {

	private AModelHelp model;

	@Override
	public void init() {
		String[] type = getContent().split("/");

		switch (type[0]) {
		case "EMOD":
			switch (type[1]) {
			case "MDL":
				model = ModelDescription.load(type[2]);
				break;
			case "SC":
				model = SimulationControlDescription.load(type[2]);
				break;
			case "MAT":
				model = MaterialDescription.load(type[2]);
				break;
			}
			break;
		case "DD":
			switch (type[1]) {
			case "E":
				model = DDModelDescription.load(type[2]);
				break;
			case "P":
				model = DDProfileDescription.load(type[2]);
				break;
			}
			break;
		}
	}

	@Override
	public String getHTMLTitle() {
		return HelpFormater.formatTitle(model.getTitle(), title);
	}

	@Override
	public String getHTMLContent() {
		return model.getHTMLContent();
	}

}
