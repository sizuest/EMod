package ch.zuestengineering.emod.help;

import java.io.File;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlTransient;

/**
 * Abstract class defining a generic help topic
 * 
 * @author Simon Z�st
 *
 */
@XmlSeeAlso(HelpTopicModel.class)
@XmlRootElement
public abstract class AHelpTopic {
	@XmlElement
	protected String title;
	@XmlElementWrapper
	@XmlElement
	protected ArrayList<String> keyWords = new ArrayList<>();
	@XmlElement
	protected String content;
	@XmlElementWrapper
	@XmlElement
	ArrayList<AHelpTopic> subTopics = new ArrayList<>();

	AHelpTopic parentElement = null;

	/**
	 * Empty constructor for unmarshaller
	 */
	public AHelpTopic() {
	}

	/**
	 * @param u
	 * @param parent
	 */
	public void afterUnmarshal(final Unmarshaller u, final Object parent) {
		// post xml init method (loading physics data)
		init();
	}

	/**
	 * Implementing classes should override this method
	 */
	public void init() {
	}

	/**
	 * Returns a list with containing all sub topics and their subTopics
	 * 
	 * @return
	 */
	public ArrayList<AHelpTopic> getAllChildren() {
		ArrayList<AHelpTopic> out = new ArrayList<>();

		for (AHelpTopic ht : subTopics) {
			out.add(ht);
			out.addAll(ht.getAllChildren());
		}

		return out;
	}

	/**
	 * Sets the roots element to the topic and its sub topics
	 * 
	 * @param parent
	 */
	public void setParentElement(AHelpTopic parent) {
		this.parentElement = parent;
		for (AHelpTopic ht : subTopics)
			ht.setParentElement(this);
	}

	/**
	 * @return
	 */
	public String getPathName() {
		return title.replace(" ", "").replaceAll("<[^>]*>", "");
	}

	/**
	 * Looks up for the help topic with the provided path
	 * 
	 * Path: subTopic/subSubItem/...
	 * 
	 * @param path
	 * @return
	 */
	public AHelpTopic getTopic(String path) {
		if (path.charAt(0) == '/')
			return getRootElement().getTopic(path.split("/"));

		return getTopic(path.split("/"));
	}

	/**
	 * @return
	 */
	private AHelpTopic getRootElement() {
		if (isRootElement())
			return this;
		return parentElement.getRootElement();
	}

	/**
	 * Looks up for the help topic with the provided path
	 * 
	 * Path: {subTopic, subSubItem, ...}
	 * 
	 * @param path
	 * @return
	 */
	public AHelpTopic getTopic(String[] path) {

		if (path.length > 1 && path[0].matches(""))
			path = Arrays.copyOfRange(path, 1, path.length);

		if (path.length == 0)
			return this;
		if (path[0].matches(".."))
			return parentElement.getTopic(Arrays.copyOfRange(path, 1, path.length));
		else
			for (AHelpTopic ht : subTopics) {
				if (ht.getPathName().matches(path[0]))
					return ht.getTopic(Arrays.copyOfRange(path, 1, path.length));
			}

		return null;
	}

	private AHelpTopic findChild(String name) {
		for (AHelpTopic ht : subTopics)
			if (ht.getPathName().matches(name))
				return ht;

		return null;
	}

	/**
	 * Returns all sub topics
	 * 
	 * @return
	 */
	public ArrayList<AHelpTopic> getChildren() {
		return subTopics;
	}

	/**
	 * Returns the title
	 * 
	 * @return
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param text
	 */
	@XmlTransient
	public void setTitle(String text) {
		this.title = text;
	}

	/**
	 * Add the the topic to the lists of sub-topics. If the name of the new topic is
	 * already used by one of the available topics, an enumerator will be appended
	 * 
	 * @param topic
	 * @return
	 */
	public String addSubTopic(AHelpTopic topic) {
		if (findChild(topic.getPathName()) != null) {
			int i = 1;
			String origName = topic.getTitle();
			do {
				topic.setTitle(origName + " " + i++);
			} while (findChild(topic.getPathName()) != null);
		}

		subTopics.add(topic);

		return topic.getPathName();
	}

	/**
	 * Removes the subtopic with the provided name
	 * 
	 * @param topicName
	 */
	public void removeSubTopic(String topicName) {
		for (AHelpTopic ht : subTopics)
			if (ht.getPathName().matches(topicName)) {
				subTopics.remove(ht);
				break;
			}
	}

	/**
	 * Moves the subtopic with the provided name up in the list
	 * 
	 * @param topicName
	 */
	public void moveSubTopicUp(String topicName) {
		int pos = subTopics.indexOf(findChild(topicName));

		if (pos < 1)
			return;

		AHelpTopic tmp = subTopics.get(pos);
		subTopics.remove(pos);
		subTopics.add(pos - 1, tmp);
	}

	/**
	 * Moves the subtopic with the provided name down in the list
	 * 
	 * @param topicName
	 */
	public void moveSubTopicDown(String topicName) {
		int pos = subTopics.indexOf(findChild(topicName));

		if (pos == -1 | pos > subTopics.size() - 2)
			return;

		AHelpTopic tmp = subTopics.get(pos);
		subTopics.remove(pos);
		subTopics.add(pos + 1, tmp);
	}

	/**
	 * Returns a String containing all key words separated by ';'
	 * 
	 * @return
	 */
	public String getKeyWords() {
		String ret = "";

		if (null == keyWords)
			return ret;

		for (int i = 0; i < keyWords.size(); i++) {
			ret += keyWords.get(i);
			if (i < keyWords.size() - 1)
				ret += "; ";
		}

		return ret;
	}

	/**
	 * Sets the key words based on a string containing the new keywords separated by
	 * ';'
	 * 
	 * @param k
	 */
	@XmlTransient
	public void setKeyWords(String k) {
		keyWords = new ArrayList<>();
		for (String kw : k.split("[\\s]*;[\\s]*"))
			keyWords.add(kw);
	}

	/**
	 * Returns the content
	 * 
	 * @return
	 */
	public String getContent() {
		return content;
	}

	private String addLinks(String content) {

		Pattern p = Pattern.compile("@\\{[A-Za-z/.]+\\}");
		Matcher m = p.matcher(content);

		while (m.find()) {
			String path = m.group();
			AHelpTopic ht = getTopic(path.replace("@{", "").replace("}", ""));

			if (ht != null)
				content = content.replace(path, ht.getHTMLLink());
		}

		return content;
	}

	protected String getHTMLTitle() {
		return HelpFormater.formatTitle(getTitle());
	}

	protected String getHTMLContent() {
		return HelpFormater.formatParagraph(null, getContent());
	}

	/**
	 * Creates the html file
	 * 
	 * @return
	 */
	public String createHTML() {
		String htmlContent = "";
		String htmlPath = this.getHTMLPath();

		htmlContent += HelpFormater.getHTMLHeader();
		htmlContent += getHTMLTitle();
		htmlContent += getHTMLBreadCrumb();
		htmlContent += addLinks(getHTMLContent());
		if (this.subTopics.size() > 0)
			htmlContent += HelpFormater.formatParagraph("Further readings", HelpFormater.formatIndex(this, false));
		htmlContent += HelpFormater.getHTMLFooter();

		File htmlFile = new File(htmlPath);
		htmlFile.getParentFile().mkdirs();

		try {
			htmlFile.createNewFile();
			PrintWriter printer = new PrintWriter(htmlFile);
			printer.print(htmlContent);
			printer.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return "file:///" + htmlFile.getAbsolutePath().replace("\\", "/");
	}

	/**
	 * @return
	 */
	private String getHTMLBreadCrumb() {
		if (isRootElement())
			return "";
		return "\t\t<div class=\"breadcrumb\">" + getHTMLBreadCrumbContinue() + "</div>";
	}

	/**
	 * @return
	 */
	private String getHTMLBreadCrumbContinue() {
		if (isRootElement())
			return getHTMLLink();
		return parentElement.getHTMLBreadCrumbContinue() + " > " + getHTMLLink();
	}

	/**
	 * Creates the html file for this and all sub-topics
	 */
	public void createAllHTML() {
		this.createHTML();

		for (AHelpTopic ht : subTopics)
			if (ht != null)
				ht.createAllHTML();
	}

	/**
	 * Sets the content
	 * 
	 * @param content
	 */
	@XmlTransient
	public void setContent(String content) {
		this.content = content;
	}

	/**
	 * Sets the topic as root element for the whole (sub-)tree
	 */
	public void setAsRootElement() {
		setParentElement(this);
	}

	/**
	 * Returns the topics path
	 * 
	 * @return
	 */
	public String getPath() {
		String path = getPathName();
		
		if(parentElement == null)
			return path;

		if (!isRootElement() && !parentElement.isRootElement()) {
			path = parentElement.getPath() + "/" + path;
		}

		return path;
	}

	/**
	 * @return
	 */
	private boolean isRootElement() {
		return this.equals(parentElement);
	}

	/**
	 * Returns the path to the html file
	 * 
	 * @return
	 */
	public String getHTMLPath() {
		return "help/" + getPath() + ".html";
	}

	/**
	 * @return
	 */
	public String getHTMLLink() {
		return "<a href=\"" + getHTMLFullPath() + "\">" + getTitle() + "</a>";
	}

	/**
	 * Returns the full path to the html file, including 'file://'
	 * 
	 * @return
	 */
	public String getHTMLFullPath() {
		String userDir = "";
		try {
			userDir = System.getProperty("user.dir");
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return "file:///" + userDir + File.separatorChar + getHTMLPath();
	}

}
