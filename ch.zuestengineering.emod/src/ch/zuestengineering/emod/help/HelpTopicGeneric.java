package ch.zuestengineering.emod.help;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Most basic implementation of a help topic
 * 
 * @author Simon Z�st
 *
 */
@XmlRootElement
public class HelpTopicGeneric extends AHelpTopic {

	/**
	 * Constructor for unmarshaller
	 */
	public HelpTopicGeneric() {
	}

	/**
	 * Constructor
	 * 
	 * @param title
	 */
	public HelpTopicGeneric(String title) {
		super();
		this.title = title;
		this.content = "";
		this.keyWords = new ArrayList<>();
		this.subTopics = new ArrayList<>();
	}

}
