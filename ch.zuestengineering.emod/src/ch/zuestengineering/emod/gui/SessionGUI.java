/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.gui;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import ch.zuestengineering.emod.EModSession;
import ch.zuestengineering.emod.gui.icons.IconHandler;
import ch.zuestengineering.emod.gui.modelling.IGraphEditable;
import ch.zuestengineering.emod.utils.LocalizationHandler;

/**
 * Selection of the machine to be displayed
 * 
 * @author sizuest
 *
 */
public class SessionGUI extends AGUITab implements IGraphEditable {

	// Text to let the user change the MachineConfig
	private static Text textMachineName;
	// name of the MachineConfig from the last use of EMod from app.config
	private static String machineName;

	// Text to let the user add some comments
	private static Text textComment;

	// Combo to let the user select the SimConfig
	private static Combo comboMachineConfigName;

	private static Combo comboSimConfigName;

	private static Combo comboProcName;
	// name of the SimConfig from the last use of EMod from app.config
	private static String machineConfigName;

	private static String simConfigName;

	private static String procName;

	/**
	 * @param parent
	 */
	public SessionGUI(Composite parent) {
		super(parent, SWT.NONE);
		init();
	}

	/**
	 * @param parent
	 * @param i
	 */
	public SessionGUI(CTabFolder parent, int i) {
		super(parent, i, LocalizationHandler.getItem("app.gui.menu.file.session"));
		init();
	}

	/**
	 * window to load existing machine configuration
	 */
	public void init() {

		// PropertiesHandler.setProperty("app.MachineDataPathPrefix",
		// "Machines");

		// Define content layout
		getContent().setLayout(new GridLayout(3, false));

		// get machineName and machineConfigName from app.config file
		machineName = EModSession.getMachineName();
		machineConfigName = EModSession.getMachineConfig();
		simConfigName = EModSession.getSimulationConfig();
		procName = EModSession.getProcessName();

		// text load machine config
		Label textLoadMachConfig = new Label(getContent(), SWT.TRANSPARENT);
		textLoadMachConfig.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1));
		textLoadMachConfig.setText(LocalizationHandler.getItem("app.gui.session.machinename"));

		// combo for the user to select the desired MachConfig
		textMachineName = new Text(getContent(), SWT.BORDER);
		textMachineName.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));
		textMachineName.setText(machineName);

		// text load machine config
		Label textLoadMachineConfig = new Label(getContent(), SWT.TRANSPARENT);
		textLoadMachineConfig.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1));
		textLoadMachineConfig.setText(LocalizationHandler.getItem("app.gui.session.machineconfigname"));

		// combo for the user to select the desired SimConfig
		comboMachineConfigName = new Combo(getContent(), SWT.NONE);
		comboMachineConfigName.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		// possible items of the combo are all SimConfig that match to the
		// selected MachConfig
		updatecomboMachineConfigName();
		// prefill the last used SimConfig as default value into the combo
		comboMachineConfigName.setText(machineConfigName);

		Button buttonDelete = new Button(getContent(), SWT.PUSH);
		buttonDelete.setImage(IconHandler.getIcon(getDisplay(), "delete_obj"));
		buttonDelete.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		buttonDelete.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				EModSession.removeMachineConfig(comboMachineConfigName.getText());
				updatecomboMachineConfigName();
				update();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		// text load simulation config
		Label textLoadSimConfig = new Label(getContent(), SWT.TRANSPARENT);
		textLoadSimConfig.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1));
		textLoadSimConfig.setText(LocalizationHandler.getItem("app.gui.session.simulationconfigname"));

		// combo for the user to select the desired SimConfig
		comboSimConfigName = new Combo(getContent(), SWT.NONE);
		comboSimConfigName.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		// prefill the last used SimConfig as default value into the combo
		comboSimConfigName.setText(simConfigName);

		// add selection listener to the combo
		comboSimConfigName.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				// disable comboMachineConfigName to prevent argument null for
				// updatecomboMachineConfigName
				comboProcName.setEnabled(false);

				String stringSimConfig = comboSimConfigName.getText();
				// update comboMachineConfigName according to the Selection of
				// MachineConfig
				updatecomboProcName(stringSimConfig);

				// enable comboMachineConfigName after update
				comboProcName.setEnabled(true);
				update();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent event) {
				// Not used
			}
		});

		buttonDelete = new Button(getContent(), SWT.PUSH);
		buttonDelete.setImage(IconHandler.getIcon(getDisplay(), "delete_obj"));
		buttonDelete.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		buttonDelete.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				EModSession.removeSimulationConfig(comboSimConfigName.getText());
				updatecomboSimConfigName();
				update();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		// text load process config
		Label textLoadProcConfig = new Label(getContent(), SWT.TRANSPARENT);
		textLoadProcConfig.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1));
		textLoadProcConfig.setText(LocalizationHandler.getItem("app.gui.session.processconfigname"));

		// combo for the user to select the desired process
		comboProcName = new Combo(getContent(), SWT.NONE);
		comboProcName.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		// prefill the last used process as default value into the combo
		comboProcName.setText(procName);

		buttonDelete = new Button(getContent(), SWT.PUSH);
		buttonDelete.setImage(IconHandler.getIcon(getDisplay(), "delete_obj"));
		buttonDelete.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		buttonDelete.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				EModSession.removeProcess(comboProcName.getText());
				updatecomboProcName(comboSimConfigName.getText());
				update();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		// text comment
		Label textLoadComment = new Label(getContent(), SWT.TRANSPARENT);
		textLoadComment.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, false, 1, 1));
		textLoadComment.setText(LocalizationHandler.getItem("app.gui.session.comment"));

		textComment = new Text(getContent(), SWT.MULTI | SWT.BORDER | SWT.V_SCROLL);
		textComment.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 2, 1));
		textComment.setText(EModSession.getNotes());
		textComment.setEditable(false);

		getContent().pack();

		updatecomboSimConfigName();

	}

	/**
	 * update the comboMachineConfigName according to the selection of
	 * comboMachineName
	 * 
	 * @param stringMachConfig update the selection of possible machine
	 *                         conifgurations
	 */
	protected static void updatecomboMachineConfigName() {
		String[] items = EModSession.getMachineConfigs();

		comboMachineConfigName.setItems(items);
		comboMachineConfigName.select(0);

		for (int i = 1; i < items.length; i++)
			if (items[i].equals(EModSession.getMachineConfig())) {
				comboMachineConfigName.select(i);
				break;
			}
	}

	// update the comboSimConfigName according to the selection of
	// comboMachineName
	protected static void updatecomboSimConfigName() {
		String[] items = EModSession.getSimulationConfigs();

		comboSimConfigName.setItems(items);
		comboSimConfigName.select(0);

		for (int i = 1; i < items.length; i++)
			if (items[i].equals(EModSession.getSimulationConfig())) {
				comboSimConfigName.select(i);
				break;
			}

		updatecomboProcName(comboSimConfigName.getText());
	}

	// update the comboProcName according to the selection of comboMachineName
	protected static void updatecomboProcName(String stringSimConfig) {
		String[] items = EModSession.getProcessNames(stringSimConfig);

		comboProcName.setItems(items);
		comboProcName.select(0);

		for (int i = 1; i < items.length; i++)
			if (items[i].equals(EModSession.getProcessName())) {
				comboProcName.select(i);
				break;
			}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.ethz.inspire.emod.gui.AConfigGUI#save()
	 */
	@Override
	public void save() {
		String machine = textMachineName.getText();
		String machConfig = comboMachineConfigName.getText();
		String simConfig = comboSimConfigName.getText();
		String procName = comboProcName.getText();

		EModSession.setMachineName(machine);
		EModSession.setMachineConfig(machConfig);
		EModSession.setSimulationConfig(simConfig);
		EModSession.setProcessName(procName);

		EModSession.save();

		EModStatusBarGUI.updateMachineInfo();

		updatecomboMachineConfigName();
		updatecomboSimConfigName();

		ContentHandlerGUI.updateSession();
	}

	/**
	 * Reset the form
	 */
	public void reset() {
		textMachineName.setText(EModSession.getMachineName());
		comboMachineConfigName.setText(EModSession.getMachineConfig());
		comboSimConfigName.setText(EModSession.getSimulationConfig());
		comboProcName.setText(EModSession.getProcessName());

	}

	@Override
	public void showAll() {
		// TODO Auto-generated method stub

	}

	@Override
	public void update() {
		// TODO Auto-generated method stub
	}

	@Override
	public void wasEdited() {
		// TODO Auto-generated method stub

	}

	private Composite getContent() {
		return this;
	}

}
