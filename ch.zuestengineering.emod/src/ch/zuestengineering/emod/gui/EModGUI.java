/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.gui;

import java.util.logging.Logger;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Monitor;
import org.eclipse.swt.widgets.Shell;

import ch.zuestengineering.emod.EModFileHandling;
import ch.zuestengineering.emod.EModSession;
import ch.zuestengineering.emod.LogLevel;
import ch.zuestengineering.emod.Machine;
import ch.zuestengineering.emod.dd.gui.DuctConfigGraphGUI;
import ch.zuestengineering.emod.gui.help.HelpBrowserGUI;
import ch.zuestengineering.emod.gui.icons.IconHandler;
import ch.zuestengineering.emod.gui.material.EditMaterialGUI;
import ch.zuestengineering.emod.gui.material.MaterialDBGUI;
import ch.zuestengineering.emod.gui.modelling.EditMachineComponentGUI;
import ch.zuestengineering.emod.licensing.LicenseActions;
import ch.zuestengineering.emod.licensing.LicenseHandler;
import ch.zuestengineering.emod.model.parameters.ModelParameterSet;
import ch.zuestengineering.emod.utils.LocalizationHandler;

/**
 * main gui class for emod application
 * 
 * @author dhampl
 * 
 */
public class EModGUI {

	private static Logger logger = Logger.getLogger(EModGUI.class.getName());

	protected static Shell shell;
	protected Display disp;

	protected StyledText console;

	/**
	 * @param display
	 */
	public EModGUI(Display display) {
		disp = display;
		shell = new Shell(display);

		shell.setText(LocalizationHandler.getItem("app.gui.title") + ": " + LicenseHandler.getLicenseType());
		if (display.getBounds().width >= 1024)
			shell.setSize(1024, 768);
		else
			shell.setSize(display.getBounds().width, display.getBounds().height);

		Monitor primary = display.getPrimaryMonitor();
		Rectangle bounds = primary.getBounds();
		Rectangle rect = shell.getBounds();

		int x = bounds.x + (bounds.width - rect.width) / 2;
		int y = bounds.y + (bounds.height - rect.height) / 2;

		shell.setLocation(x, y);

		shell.setLayout(new GridLayout(1, false));

		ContentHandlerGUI.initialize(shell, SWT.BORDER);
		// ContentHandlerGUI.getTabFolder().setLayoutData(new GridData(SWT.FILL,
		// SWT.FILL, true, true, 1, 1));

		// Icon
		shell.setImages(new Image[] { new Image(Display.getDefault(), "src/resources/icons/EModIcon_128x128.png"),
				new Image(Display.getDefault(), "src/resources/icons/EModIcon_48x48.png"),
				new Image(Display.getDefault(), "src/resources/icons/EModIcon_32x32.png"),
				new Image(Display.getDefault(), "src/resources/icons/EModIcon_22x22.png"),
				new Image(Display.getDefault(), "src/resources/icons/EModIcon_16x16.png") });

		// init menu bar
		logger.log(LogLevel.DEBUG, "init menu");
		initMenu();

		// Create a status bar
		EModStatusBarGUI.create(shell);

		shell.open();
		shell.setMaximized(true);

		shell.layout();

		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}

	protected void update() {
		shell.setEnabled(false);
		ContentHandlerGUI.update();
		EModStatusBarGUI.updateMachineInfo();
		shell.setEnabled(true);
	}

	/**
	 * Initializes the main menu bar.
	 */
	private void initMenu() {
		// create menu bar
		Menu menuBar = new Menu(shell, SWT.BAR);

		// create "File" tab and items
		MenuItem fileMenuHeader = new MenuItem(menuBar, SWT.CASCADE);
		fileMenuHeader.setText(LocalizationHandler.getItem("app.gui.menu.file"));
		Menu fileMenu = new Menu(shell, SWT.DROP_DOWN);
		fileMenuHeader.setMenu(fileMenu);
		MenuItem fileNewItem = new MenuItem(fileMenu, SWT.PUSH);
		fileNewItem.setText(LocalizationHandler.getItem("app.gui.menu.file.new"));
		MenuItem fileOpenItem = new MenuItem(fileMenu, SWT.PUSH);
		fileOpenItem.setText(LocalizationHandler.getItem("app.gui.menu.file.open"));
		// MenuItem fileSessionItem = new MenuItem(fileMenu, SWT.PUSH);
		// fileSessionItem.setText(LocalizationHandler.getItem("app.gui.menu.file.session"));
		/*
		 * MenuItem fileOpenLibItem = new MenuItem(fileMenu, SWT.PUSH);
		 * fileOpenLibItem.setImage(new Image(Display.getDefault(),
		 * "src/resources/Open16.gif")); fileOpenLibItem.setText(LocalizationHandler
		 * .getItem("app.gui.menu.file.openlib"));
		 */
		MenuItem fileSaveItem = new MenuItem(fileMenu, SWT.PUSH);
		fileSaveItem.setImage(IconHandler.getIcon(disp, "save_edit"));
		fileSaveItem.setText(LocalizationHandler.getItem("app.gui.menu.file.save"));
		MenuItem fileSaveAsItem = new MenuItem(fileMenu, SWT.PUSH);
		fileSaveAsItem.setImage(IconHandler.getIcon(disp, "saveas_edit"));
		fileSaveAsItem.setText(LocalizationHandler.getItem("app.gui.menu.file.saveas"));
		MenuItem fileExportParams = new MenuItem(fileMenu, SWT.PUSH);
		fileExportParams.setText(LocalizationHandler.getItem("app.gui.menu.file.exportparams"));
		MenuItem filePropertiesItem = new MenuItem(fileMenu, SWT.PUSH);
		filePropertiesItem.setText(LocalizationHandler.getItem("app.gui.menu.file.properties"));
		MenuItem fileExitItem = new MenuItem(fileMenu, SWT.PUSH);
		fileExitItem.setText(LocalizationHandler.getItem("app.gui.menu.file.exit"));

		// create "Database Components" tab and items
		MenuItem compDBMenuHeader = new MenuItem(menuBar, SWT.CASCADE);
		compDBMenuHeader.setText(LocalizationHandler.getItem("app.gui.menu.compDB"));
		Menu compDBMenu = new Menu(shell, SWT.DROP_DOWN);
		compDBMenuHeader.setMenu(compDBMenu);
		MenuItem compDBNewItem = new MenuItem(compDBMenu, SWT.PUSH);
		compDBNewItem.setImage(IconHandler.getIcon(disp, "new_wiz"));
		compDBNewItem.setText(LocalizationHandler.getItem("app.gui.menu.compDB.new"));
		MenuItem compDBOpenItem = new MenuItem(compDBMenu, SWT.PUSH);
		compDBOpenItem.setText(LocalizationHandler.getItem("app.gui.menu.compDB.open"));
		MenuItem compDBRemoveLibraryLinks = new MenuItem(compDBMenu, SWT.PUSH);
		compDBRemoveLibraryLinks.setText(LocalizationHandler.getItem("app.gui.menu.compDB.removelibrarylinks"));
		// MenuItem compDBImportItem = new MenuItem(compDBMenu, SWT.PUSH);
		// compDBImportItem.setText(LocalizationHandler.getItem("app.gui.menu.compDB.import"));

		// create "Database Material" tab and items
		MenuItem matDBMenuHeader = new MenuItem(menuBar, SWT.CASCADE);
		matDBMenuHeader.setText(LocalizationHandler.getItem("app.gui.menu.matDB"));
		Menu matDBMenu = new Menu(shell, SWT.DROP_DOWN);
		matDBMenuHeader.setMenu(matDBMenu);
		MenuItem matDBNewItem = new MenuItem(matDBMenu, SWT.PUSH);
		matDBNewItem.setImage(IconHandler.getIcon(disp, "new_wiz"));
		matDBNewItem.setText(LocalizationHandler.getItem("app.gui.menu.matDB.new"));
		MenuItem matDBOpenItem = new MenuItem(matDBMenu, SWT.PUSH);
		matDBOpenItem.setText(LocalizationHandler.getItem("app.gui.menu.matDB.open"));

		// create "Duct Design" tab and items
		/*
		 * MenuItem ductDesignMenuHeader = new MenuItem(menuBar, SWT.CASCADE);
		 * ductDesignMenuHeader.setText("Duct Designer"); Menu ductMenu = new
		 * Menu(shell, SWT.DROP_DOWN); ductDesignMenuHeader.setMenu(ductMenu); MenuItem
		 * ductDesignTestItem = new MenuItem(ductMenu, SWT.PUSH);
		 * ductDesignTestItem.setText("Test");
		 */

		// create "Help" tab and items
		MenuItem helpMenuHeader = new MenuItem(menuBar, SWT.CASCADE);
		helpMenuHeader.setText(LocalizationHandler.getItem("app.gui.menu.help"));
		Menu helpMenu = new Menu(shell, SWT.DROP_DOWN);
		helpMenuHeader.setMenu(helpMenu);
		// MenuItem helpContentItem = new MenuItem(helpMenu, SWT.PUSH);
		// helpContentItem.setText(LocalizationHandler.getItem("app.gui.menu.help.content"));
		MenuItem helpConsole = new MenuItem(helpMenu, SWT.PUSH);
		helpConsole.setText(LocalizationHandler.getItem("Console"));
		MenuItem helpItem = new MenuItem(helpMenu, SWT.PUSH);
		helpItem.setText(LocalizationHandler.getItem("app.gui.menu.help.openhelp"));

		// add listeners
		fileNewItem.addSelectionListener(new fileNewItemListener());
		fileOpenItem.addSelectionListener(new fileOpenItemListener());
		// fileSessionItem.addSelectionListener(new fileSessionListener());
		// fileOpenLibItem.addSelectionListener(new fileOpenLibItemListener());
		fileSaveItem.addSelectionListener(new fileSaveItemListener());
		fileSaveAsItem.addSelectionListener(new fileSaveAsItemListener());
		fileExportParams.addSelectionListener(new fileExportParametersItemListener());
		filePropertiesItem.addSelectionListener(new filePropertiesItemListener());
		fileExitItem.addSelectionListener(new fileExitItemListener());

		compDBNewItem.addSelectionListener(new compDBNewItemListener());
		compDBOpenItem.addSelectionListener(new compDBOpenItemListener());
		compDBRemoveLibraryLinks.addSelectionListener(new compDBRemoveAllLibraryLinks());

		matDBNewItem.addSelectionListener(new matDBNewItemListener());
		matDBOpenItem.addSelectionListener(new matDBOpenItemListener());

		helpItem.addSelectionListener(new helpItemListener());
		helpConsole.addSelectionListener(new consoleListener());

		// ductDesignTestItem.addSelectionListener(new
		// ductDesignTestItemListener());

		/* License dependent visibility */
		fileNewItem.setEnabled(LicenseHandler.can(LicenseActions.CREATENEW));
		fileSaveItem.setEnabled(LicenseHandler.can(LicenseActions.SAVE));
		fileSaveAsItem.setEnabled(LicenseHandler.can(LicenseActions.SAVE));
		fileExportParams.setEnabled(LicenseHandler.can(LicenseActions.SAVE));

		compDBMenu.setEnabled(LicenseHandler.can(LicenseActions.EDIT_MDLLIBRARY));
		compDBNewItem.setEnabled(LicenseHandler.can(LicenseActions.EDIT_MDLLIBRARY));
		compDBOpenItem.setEnabled(LicenseHandler.can(LicenseActions.EDIT_MDLLIBRARY));
		compDBRemoveLibraryLinks.setEnabled(LicenseHandler.can(LicenseActions.EDIT_MDL));
		matDBMenu.setEnabled(LicenseHandler.can(LicenseActions.EDIT_MATLIBRARY));
		matDBNewItem.setEnabled(LicenseHandler.can(LicenseActions.EDIT_MATLIBRARY));
		matDBOpenItem.setEnabled(LicenseHandler.can(LicenseActions.EDIT_MATLIBRARY));

		shell.setMenuBar(menuBar);
	}

	/**
	 * 
	 */
	private void saveAs() {
		logger.log(LogLevel.DEBUG, "menu save as item selected");
		FileDialog fd = new FileDialog(shell, SWT.SAVE);
		fd.setText(LocalizationHandler.getItem("app.gui.save"));
		fd.setFilterPath("C:/");
		String[] filterExt = { "*.emod" };
		fd.setFilterExtensions(filterExt);
		String selected = fd.open();
		if (selected == null) {
			logger.log(LogLevel.DEBUG, "no file specified, closed");
			return;
		}
		logger.log(LogLevel.DEBUG, "File to save to: " + selected);
		EModFileHandling.save(selected);
	}
	
	/**
	 * 
	 */
	private void exportParams() {
		logger.log(LogLevel.DEBUG, "menu export params item selected");
		FileDialog fd = new FileDialog(shell, SWT.SAVE);
		fd.setText(LocalizationHandler.getItem("app.gui.export"));
		fd.setFilterPath("C:/");
		String[] filterExt = { "*.csv" };
		fd.setFilterExtensions(filterExt);
		String selected = fd.open();
		if (selected == null) {
			logger.log(LogLevel.DEBUG, "no file specified, closed");
			return;
		}
		logger.log(LogLevel.DEBUG, "File to export to: " + selected);
		try {
			ModelParameterSet.export(selected, ";");
		} catch (Exception e) {
			logger.log(LogLevel.WARNING, "Parameter set export failed!");
		}
	}

	/**
	 * menu item action listener for save item
	 * 
	 * @author dhampl
	 * 
	 */
	class fileNewItemListener implements SelectionListener {
		@Override
		public void widgetSelected(SelectionEvent event) {
			// EModStartupGUI.createNewMachineGUI();
			EModSession.newSession("New machine", "ModelConfig1", "SimConfig1", "default");
			ContentHandlerGUI.closeAll();
			update();
			ContentHandlerGUI.showSession();
		}

		@Override
		public void widgetDefaultSelected(SelectionEvent event) {
			// Not used
		}
	}

	/**
	 * menu item action listener for save item
	 * 
	 * @author sizuest
	 * 
	 */
	class fileSaveItemListener implements SelectionListener {
		@Override
		public void widgetSelected(SelectionEvent event) {
			if (null != EModSession.getPath())
				EModFileHandling.save(EModSession.getPath());
			else
				saveAs();
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.eclipse.swt.events.SelectionListener#widgetDefaultSelected(org
		 * .eclipse.swt.events.SelectionEvent)
		 */
		@Override
		public void widgetDefaultSelected(SelectionEvent event) {
			// Not used
		}
	}

	/**
	 * menu item action listener for save as item
	 * 
	 * @author dhampl Process.
	 * 
	 */
	class fileSaveAsItemListener implements SelectionListener {
		@Override
		public void widgetSelected(SelectionEvent event) {
			saveAs();
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.eclipse.swt.events.SelectionListener#widgetDefaultSelected(org
		 * .eclipse.swt.events.SelectionEvent)
		 */
		@Override
		public void widgetDefaultSelected(SelectionEvent event) {
			// Not used
		}
	}
	
	/**
	 * menu item action listener for export parameters item
	 * 
	 * @author 
	 * 
	 */
	class fileExportParametersItemListener implements SelectionListener {
		@Override
		public void widgetSelected(SelectionEvent event) {
			exportParams();
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.eclipse.swt.events.SelectionListener#widgetDefaultSelected(org
		 * .eclipse.swt.events.SelectionEvent)
		 */
		@Override
		public void widgetDefaultSelected(SelectionEvent event) {
			// Not used
		}
	}

	/**
	 * menu item action listener for save as item
	 * 
	 * @author dhampl Process.
	 * 
	 */
	class fileOpenItemListener implements SelectionListener {
		@Override
		public void widgetSelected(SelectionEvent event) {
			FileDialog fd = new FileDialog(shell, SWT.OPEN);
			fd.setText(LocalizationHandler.getItem("app.gui.open"));
			fd.setFilterPath("C:/");
			String[] filterExt = { "*.emod" };
			fd.setFilterExtensions(filterExt);
			String selected = fd.open();
			if (selected == null) {
				logger.log(LogLevel.DEBUG, "no file specified, closed");
				return;
			}
			logger.log(LogLevel.DEBUG, "File to save to: " + selected);

			ContentHandlerGUI.closeAll();
			EModFileHandling.open(selected);

			update();

			ContentHandlerGUI.showSession();
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.eclipse.swt.events.SelectionListener#widgetDefaultSelected(org
		 * .eclipse.swt.events.SelectionEvent)
		 */
		@Override
		public void widgetDefaultSelected(SelectionEvent event) {
			// Not used
		}
	}

	/**
	 * menu item action listener for load item
	 * 
	 * @author dhampl
	 * 
	 */
	class fileOpenLibItemListener implements SelectionListener {
		@Override
		public void widgetSelected(SelectionEvent event) {

			Shell startupShell = EModStartupGUI.loadMachineGUI(shell);

			startupShell.addDisposeListener(new DisposeListener() {

				@Override
				public void widgetDisposed(DisposeEvent e) {
					ContentHandlerGUI.closeAll();
					update();
					ContentHandlerGUI.showSession();
				}
			});

		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.eclipse.swt.events.SelectionListener#widgetDefaultSelected(org
		 * .eclipse.swt.events.SelectionEvent)
		 */
		@Override
		public void widgetDefaultSelected(SelectionEvent event) {
			// Not used
		}
	}

	/**
	 * menu item action listener for properties item
	 * 
	 * @author manick
	 * 
	 */
	class filePropertiesItemListener implements SelectionListener {
		@Override
		public void widgetSelected(SelectionEvent event) {
			logger.log(LogLevel.DEBUG, "menu properties item selected");
			new PropertiesGUI();
		}

		@Override
		public void widgetDefaultSelected(SelectionEvent event) {
			// Not used
		}
	}

	/**
	 * menu item action listener for exit item
	 * 
	 * @author dhampl
	 * 
	 */
	class fileExitItemListener implements SelectionListener {
		@Override
		public void widgetSelected(SelectionEvent event) {
			logger.log(LogLevel.DEBUG, "menu exit item selected");
			shell.close();
			disp.dispose();
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.eclipse.swt.events.SelectionListener#widgetDefaultSelected(org
		 * .eclipse.swt.events.SelectionEvent)
		 */
		@Override
		public void widgetDefaultSelected(SelectionEvent e) {
			shell.close();
			disp.dispose();
		}
	}

	/**
	 * menu item action listener for comp DB new item
	 * 
	 * @author manick
	 * 
	 */
	class compDBNewItemListener implements SelectionListener {
		@Override
		public void widgetSelected(SelectionEvent event) {
			EditMachineComponentGUI.newMachineComponentGUI(shell);
		}

		@Override
		public void widgetDefaultSelected(SelectionEvent event) {
			// Not used
		}
	}

	/**
	 * menu item action listener for comp DB open item
	 * 
	 * @author manick
	 * 
	 */
	class compDBOpenItemListener implements SelectionListener {
		@Override
		public void widgetSelected(SelectionEvent event) {
			new MachineComponentDBGUI(shell);
		}

		@Override
		public void widgetDefaultSelected(SelectionEvent event) {
			// Not used
		}
	}
	
	/**
	 * menu item action listener for comp DB remove all library links
	 * 
	 * @author manick
	 * 
	 */
	class compDBRemoveAllLibraryLinks implements SelectionListener {
		@Override
		public void widgetSelected(SelectionEvent event) {
			Machine.removeAllLibraryLinks();
		}

		@Override
		public void widgetDefaultSelected(SelectionEvent event) {
			// Not used
		}
	}

	/**
	 * menu item action listener for material DB new item
	 * 
	 * @author manick
	 * 
	 */
	class matDBNewItemListener implements SelectionListener {
		@Override
		public void widgetSelected(SelectionEvent event) {
			EditMaterialGUI.newMaterialGUI(shell);
		}

		@Override
		public void widgetDefaultSelected(SelectionEvent event) {
			// Not used
		}
	}

	/**
	 * menu item action listener for material DB open item
	 * 
	 * @author manick
	 * 
	 */
	class matDBOpenItemListener implements SelectionListener {
		@Override
		public void widgetSelected(SelectionEvent event) {
			new MaterialDBGUI(shell);
		}

		@Override
		public void widgetDefaultSelected(SelectionEvent event) {
			// Not used
		}
	}

	/**
	 * menu item action listener for help about item
	 * 
	 * @author manick
	 * 
	 */
	class helpItemListener implements SelectionListener {
		@Override
		public void widgetSelected(SelectionEvent event) {
			logger.log(LogLevel.DEBUG, "help item selected");
			HelpBrowserGUI.show(shell, "/");
		}

		@Override
		public void widgetDefaultSelected(SelectionEvent event) {
			// Not used
		}
	}

	/**
	 * console item action listener
	 * 
	 */
	class consoleListener implements SelectionListener {
		@Override
		public void widgetSelected(SelectionEvent event) {
			logger.log(LogLevel.DEBUG, "console item selected");
			ContentHandlerGUI.showConsole();
		}

		@Override
		public void widgetDefaultSelected(SelectionEvent event) {
			// Not used
		}
	}

	/**
	 * menu item action listener for help about item
	 * 
	 * @author manick
	 * 
	 */
	class ductDesignTestItemListener implements SelectionListener {
		@Override
		public void widgetSelected(SelectionEvent event) {
			DuctConfigGraphGUI.editDuctGUI("Test");
		}

		@Override
		public void widgetDefaultSelected(SelectionEvent event) {
			// Not used
		}
	}

	/**
	 * returns the position of the shell (used to center new windows on current
	 * position)
	 * 
	 * @author manick
	 * @return shell position
	 * 
	 */

	public static int[] shellPosition() {
		// get postion of current shell
		Rectangle rect = shell.getBounds();

		// find the middle of the shell and return two dimensional array
		// position[0]: middle of the shell in horizontal direction
		// position[1]: middle of the shell in vertical direction
		int[] position = { 0, 0 };
		position[0] = rect.x + rect.width / 2;
		position[1] = rect.y + rect.height / 2;

		// return array
		return position;
	}

}
