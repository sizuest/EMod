/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.gui.modelling;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CCombo;
import org.eclipse.swt.custom.TableEditor;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

import ch.zuestengineering.emod.EModSession;
import ch.zuestengineering.emod.Machine;
import ch.zuestengineering.emod.gui.AEditInputComposite;
import ch.zuestengineering.emod.gui.utils.TableUtils;
import ch.zuestengineering.emod.model.units.SiUnit;
import ch.zuestengineering.emod.simulation.ASimulationControl;
import ch.zuestengineering.emod.simulation.GeometricKienzleSimulationControl;
import ch.zuestengineering.emod.utils.ConfigReader;
import ch.zuestengineering.emod.utils.Defines;
import ch.zuestengineering.emod.utils.LocalizationHandler;

/**
 * GUI to edit process control
 * 
 * @author sizuest
 *
 */
public class EditGeometricKienzleSimulationControlGUI extends AEditInputComposite {

	private Table tableInputProperties;
	protected ConfigReader input;

	/**
	 * @param parent
	 * @param style
	 * @param sc
	 */
	public EditGeometricKienzleSimulationControlGUI(Composite parent, int style, ASimulationControl sc) {
		super(parent, style, sc);

	}

	@Override
	public void init() {

		this.getContent().setLayout(new GridLayout(1, true));

		tableInputProperties = new Table(this.getContent(), SWT.BORDER | SWT.SINGLE | SWT.V_SCROLL);
		tableInputProperties.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		tableInputProperties.setLinesVisible(true);
		tableInputProperties.setHeaderVisible(true);

		String[] titles = { LocalizationHandler.getItem("app.gui.compdb.property"),
				LocalizationHandler.getItem("app.gui.compdb.value") };
		for (int i = 0; i < titles.length; i++) {
			TableColumn column = new TableColumn(tableInputProperties, SWT.NULL);
			column.setText(titles[i]);
		}

		try {
			TableUtils.addCellEditor(tableInputProperties, this, new int[] { 1 });
		} catch (Exception e) {
			e.printStackTrace();
		}

		String path = EModSession.getRootPath() + File.separator + Defines.MACHINECONFIGDIR + File.separator
				+ EModSession.getMachineConfig() + File.separator + sc.getType() + "_" + sc.getName() + ".xml";

		try {
			input = new ConfigReader(path);
		} catch (Exception e) {
			e.printStackTrace();
		}

		update();
	}

	@Override
	public void update() {
		tableInputProperties.setItemCount(0);

		String[] keysInputs = new String[] { "ap_name", "n_name", "v_name", "d_name" };
		String[] keysValues = new String[] { "kappa", "kc", "z" };

		/*
		 * Obtain the available simulation controls and put their names into
		 */
		List<ASimulationControl> scListMeter = Machine.getProcessSimulationControlList(new SiUnit("m")),
				scListRotSpeed = Machine.getProcessSimulationControlList(new SiUnit("Hz")),
				scListLinSpeed = Machine.getProcessSimulationControlList(new SiUnit("m/s"));

		String[] scNameMeter = new String[scListMeter.size() + 1],
				scNameRotSpeed = new String[scListRotSpeed.size() + 1],
				scNameLinSpeed = new String[scListLinSpeed.size() + 1];

		scNameMeter[0] = "";
		for (int i = 0; i < scNameMeter.length - 1; i++)
			scNameMeter[i + 1] = scListMeter.get(i).getName();

		scNameRotSpeed[0] = "";
		for (int i = 0; i < scNameRotSpeed.length - 1; i++)
			scNameRotSpeed[i + 1] = scListRotSpeed.get(i).getName();

		scNameLinSpeed[0] = "";
		for (int i = 0; i < scNameLinSpeed.length - 1; i++)
			scNameLinSpeed[i + 1] = scListLinSpeed.get(i).getName();

		/*
		 * Add the input combos to let the user select the process signal required
		 */
		for (String key : keysInputs) {
			final TableItem item = new TableItem(tableInputProperties, SWT.NONE);
			item.setText(0, key);

			TableEditor editor = new TableEditor(tableInputProperties);
			final CCombo comboInput = new CCombo(tableInputProperties, SWT.PUSH);

			// Set list of simulation controls with appropriate unit
			switch (key) {
			case "ap_name":
			case "d_name":
				comboInput.setItems(scNameMeter);
				break;
			case "n_name":
				comboInput.setItems(scNameRotSpeed);
				break;
			case "v_name":
				comboInput.setItems(scNameLinSpeed);
			}

			try {
				item.setText(1, input.getString(key));
				comboInput.setText(input.getString(key));
			} catch (Exception e) {
				e.printStackTrace();
			}

			comboInput.addSelectionListener(new SelectionListener() {

				@Override
				public void widgetSelected(SelectionEvent e) {
					item.setText(1, comboInput.getText());
					wasEdited();
				}

				@Override
				public void widgetDefaultSelected(SelectionEvent e) {/* Not used */
				}
			});

			editor.grabHorizontal = true;
			editor.horizontalAlignment = SWT.LEFT;
			editor.setEditor(comboInput, item, 1);
		}

		for (String key : keysValues) {
			TableItem item = new TableItem(tableInputProperties, SWT.NONE);
			item.setText(0, key);
			try {
				item.setText(1, input.getString(key));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		TableColumn[] columns = tableInputProperties.getColumns();
		for (int j = 0; j < columns.length; j++) {
			columns[j].pack();
		}

	}

	@Override
	public void save() {
		for (TableItem ti : tableInputProperties.getItems())
			input.setValue(ti.getText(0), ti.getText(1));

		try {
			input.saveValues();
		} catch (IOException e) {
			e.printStackTrace();
		}

		sc.readConfig();

		// Required to update unit if no diameter is selected
		((GeometricKienzleSimulationControl) sc).readConfigFromFile();

	}

	@Override
	public void reset() {
		try {
			input = new ConfigReader(input.getPath());
		} catch (Exception e) {
			e.printStackTrace();
		}

		update();
	}

}
