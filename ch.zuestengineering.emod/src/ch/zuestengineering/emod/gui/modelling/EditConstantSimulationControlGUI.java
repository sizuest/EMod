/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.gui.modelling;

import java.io.File;
import java.io.IOException;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import ch.zuestengineering.emod.EModSession;
import ch.zuestengineering.emod.gui.AEditInputComposite;
import ch.zuestengineering.emod.simulation.ASimulationControl;
import ch.zuestengineering.emod.utils.ConfigReader;
import ch.zuestengineering.emod.utils.Defines;

/**
 * GUI to edit generic input control
 * 
 * @author sizuest
 *
 */
public class EditConstantSimulationControlGUI extends AEditInputComposite {

	private Text textValue;
	protected ConfigReader input;

	/**
	 * @param parent
	 * @param style
	 * @param sc
	 */
	public EditConstantSimulationControlGUI(Composite parent, int style, ASimulationControl sc) {
		super(parent, style, sc);

	}

	@Override
	public void init() {

		this.getContent().setLayout(new GridLayout(2, true));

		Composite target;
		if (parent.getParent() instanceof EditInputGUI)
			target = ((EditInputGUI) parent.getParent()).getContent();
		else
			target = this.getContent();

		Label label = new Label(target, SWT.TRANSPARENT);
		label.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
		label.setText("Value");
		// TODO Localization

		textValue = new Text(target, SWT.BORDER);
		textValue.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));

		String path = EModSession.getRootPath() + File.separator + Defines.MACHINECONFIGDIR + File.separator
				+ EModSession.getMachineConfig() + File.separator + sc.getType() + "_" + sc.getName() + ".xml";

		try {
			input = new ConfigReader(path);
		} catch (Exception e) {
			e.printStackTrace();
		}

		update();

		// We have to rearrange the elements
		for (Control c : target.getChildren())
			if (c.equals(this)) {
				this.setParent(target.getParent());
				this.setParent(target);
				break;
			}
	}

	@Override
	public void update() {
		try {
			textValue.setText(input.getString("Value"));
		} catch (Exception e) {
			textValue.setText("1");
			e.printStackTrace();
		}
	}

	@Override
	public void save() {
		if (Double.isFinite(Double.valueOf(textValue.getText())))
			input.setValue("Value", Double.valueOf(textValue.getText()));
		else
			update();

		try {
			input.saveValues();
		} catch (IOException e) {
			e.printStackTrace();
		}

		sc.readConfig();

	}

	@Override
	public void reset() {
		try {
			input = new ConfigReader(input.getPath());
		} catch (Exception e) {
			e.printStackTrace();
		}

		update();
	}

}
