/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.gui.modelling;

import java.io.File;
import java.io.FilenameFilter;
import java.util.Arrays;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import ch.zuestengineering.emod.Machine;
import ch.zuestengineering.emod.gui.AConfigGUI;
import ch.zuestengineering.emod.gui.utils.ShellUtils;
import ch.zuestengineering.emod.model.MachineComponent;
import ch.zuestengineering.emod.utils.LocalizationHandler;
import ch.zuestengineering.emod.utils.PropertiesHandler;

/**
 * Edit the name and type of a machine component included in the model
 * 
 * @author sizuest
 * 
 */

public class EditMachineComponentProperties extends AConfigGUI {

	private Text textName;
	private Combo comboType;
	private MachineComponent mc;
	private boolean initialSettingLibraryLink;
	private String initialType;
	private Button buttonLibraryLink;

	/**
	 * @param parent
	 * @param style
	 * @param mc
	 */
	public EditMachineComponentProperties(final Composite parent, int style, final MachineComponent mc) {
		super(parent, style);

		this.mc = mc;

		this.getContent().setLayout(new GridLayout(3, false));

		Label labelName = new Label(this.getContent(), SWT.TRANSPARENT);
		labelName.setText(LocalizationHandler.getItem("app.gui.model.mc.name"));

		textName = new Text(this.getContent(), SWT.BORDER);
		textName.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 2, 1));
		textName.setText(mc.getName());
		textName.addKeyListener(new KeyListener() {

			@Override
			public void keyReleased(KeyEvent e) {
				wasEdited();
			}

			@Override
			public void keyPressed(KeyEvent e) {
				wasEdited();
			}
		});

		Label labelType = new Label(this.getContent(), SWT.TRANSPARENT);
		labelType.setText(LocalizationHandler.getItem("app.gui.model.mc.type"));

		comboType = new Combo(this.getContent(), SWT.NONE);
		comboType.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		comboType.setText(mc.getComponent().getType());

		String path = PropertiesHandler.getProperty("app.MachineComponentDBPathPrefix") + "/"
				+ mc.getComponent().getModelType() + "/";
		File subdir = new File(path);

		// check if the directory exists, then show possible parameter sets to
		// select
		if (subdir.exists()) {
			String[] subitems = subdir.list(new FilenameFilter() {
			    @Override
			    public boolean accept(File dir, String name) {
			        return !name.endsWith("_lc.xml");
			    }
			});

			// remove the "Type_" and the ".xml" part of the filename
			for (int i = 0; i < subitems.length; i++) {
				subitems[i] = subitems[i].replace(mc.getComponent().getModelType() + "_", "");
				subitems[i] = subitems[i].replace(".xml", "");
			}

			// sort by name
			Arrays.sort(subitems);

			// set the possible parameter sets to the combo
			comboType.setItems(subitems);
			comboType.setText(mc.getComponent().getType());
		}

		comboType.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				wasEdited();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {/* Not used */
			}
		});

		final Button buttonEditComponent = new Button(this.getContent(), SWT.PUSH);
		buttonEditComponent.setText("...");
		buttonEditComponent.setLayoutData(new GridData(SWT.RIGHT, SWT.FILL, false, true, 1, 1));
		buttonEditComponent.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				/*
				 * If the library link is selected: Edit library data.
				 * If the library link is not selected: Edit the component data directly
				 */
				if(buttonLibraryLink.getSelection()) {
					String model = mc.getComponent().getModelType();
					String type = comboType.getText();
					EditMachineComponentGUI.editMachineComponentGUI(getShell(), model, type);
				}
				else {
					mc.getComponent().setLibraryLink(false);
					EditMachineComponentGUI.editMachineComponentGUI(parent.getParent().getShell(), mc.getComponent());
				}
					
				wasEdited();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent event) {
				// Not used
			}
		});
		buttonEditComponent.pack();
		
		
		Label labelLibraryLink = new Label(this.getContent(), SWT.TRANSPARENT);
		labelLibraryLink.setText(LocalizationHandler.getItem("app.gui.model.mc.uselibrary"));
		labelLibraryLink.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, false, 2, 1));
		buttonLibraryLink = new Button(getContent(), SWT.CHECK);
		buttonLibraryLink.setLayoutData(new GridData(SWT.RIGHT, SWT.TOP, false, false, 1, 1));
		buttonLibraryLink.setSelection(mc.getComponent().hasLibraryLink());
		buttonLibraryLink.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent e) {
				wasEdited();
				
				if(buttonLibraryLink.getSelection()) {
					if(typeExists(comboType.getText()))
						askForDataSrcGUI();
					else {
						MachineComponent.newMachineComponentType(mc.getComponent().getModelType(), comboType.getText());
						mc.getComponent().setType(comboType.getText());
						mc.getComponent().setLibraryLink(true);
						mc.getComponent().saveParameters();
					}
				}
				else
					mc.getComponent().setLibraryLink(false);
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				// Not used
			}
		});
		
		
		/*
		 * Set initial settings
		 */
		initialSettingLibraryLink = mc.getComponent().hasLibraryLink();
		initialType               = mc.getComponent().getType();

	}

	/**
	 * Component Edit GUI for editing a existing Component of the Component DB
	 * 
	 * @param parent
	 * @param mc
	 * @return
	 */
	public static Shell editMachineComponentGUI(final Shell parent, final MachineComponent mc) {
		final Shell shell = new Shell(parent, SWT.TITLE | SWT.SYSTEM_MODAL | SWT.CLOSE | SWT.MAX);
		shell.setText(LocalizationHandler.getItem("app.gui.compdb.editcomp"));
		shell.setLayout(new GridLayout(1, true));

		EditMachineComponentProperties gui = new EditMachineComponentProperties(shell, SWT.NONE, mc);

		shell.setImages(parent.getImages());

		shell.pack();

		shell.layout();
		shell.redraw();
		shell.open();

		// width and height of the shell
		Rectangle rect = shell.getBounds();
		int[] size = { 0, 0 };
		size[0] = rect.width;
		size[1] = rect.height;

		// position the shell into the middle of the last window
		shell.setLocation(parent.getShell().getBounds().x + parent.getShell().getBounds().width / 2 - size[0] / 2,
				parent.getShell().getBounds().y + parent.getShell().getBounds().height / 2 - size[1] / 2);

		shell.addDisposeListener(new DisposeListener() {

			@Override
			public void widgetDisposed(DisposeEvent e) {
				parent.setEnabled(true);
			}
		});

		gui.addDisposeListener(new DisposeListener() {
			@Override
			public void widgetDisposed(DisposeEvent e) {
				shell.dispose();
			}
		});

		return shell;
	}

	@Override
	public void save() {
		Machine.renameMachineComponent(mc.getName(), textName.getText());
		mc.getComponent().setType(comboType.getText());
	}

	@Override
	public void reset() {
		
		mc.getComponent().setLibraryLink(initialSettingLibraryLink);
		mc.getComponent().setType(initialType);
		
		textName.setText(mc.getName());
		comboType.setText(mc.getComponent().getType());
		buttonLibraryLink.setSelection(mc.getComponent().hasLibraryLink());
	}
	
	protected Shell askForDataSrcGUI() {
		final Shell dialog = new Shell(this.getShell(), SWT.APPLICATION_MODAL);
		Button buttonKeepLibrary, buttonKeepMc;

		dialog.setLayout(new GridLayout(1, true));
		dialog.setSize(200, 200);
		dialog.setText(LocalizationHandler.getItem("app.gui.config.infomodify"));

		buttonKeepLibrary = new Button(dialog, SWT.NONE);
		buttonKeepLibrary.setLayoutData(new GridData(SWT.FILL, SWT.BOTTOM, true, false));
		buttonKeepLibrary.setText(LocalizationHandler.getItem("app.gui.model.mc.keeplibrary"));
		buttonKeepLibrary.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				mc.getComponent().setType(comboType.getText());
				mc.getComponent().setLibraryLink(true);
				mc.getComponent().loadParameters();
				dialog.dispose();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {/* Not used */
			}
		});

		buttonKeepMc = new Button(dialog, SWT.NONE);
		buttonKeepMc.setLayoutData(new GridData(SWT.FILL, SWT.BOTTOM, true, false));
		buttonKeepMc.setText(LocalizationHandler.getItem("app.gui.model.mc.keepmc"));
		buttonKeepMc.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				mc.getComponent().setType(comboType.getText());
				mc.getComponent().setLibraryLink(true);
				mc.getComponent().saveParameters();
				dialog.dispose();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {/* Not used */
			}
		});

		dialog.pack();
		dialog.open();

		ShellUtils.putToCenter(dialog, this.getShell());

		return dialog;
	}
	
	private boolean typeExists(String type) {
		for(String s: comboType.getItems())
			if(s.equals(type))
				return true;
		
		return false;
	}

}
