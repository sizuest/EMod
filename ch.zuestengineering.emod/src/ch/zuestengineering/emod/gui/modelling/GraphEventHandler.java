/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.gui.modelling;

import java.awt.Color;
import java.awt.event.MouseEvent;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Iterator;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;
import org.piccolo2d.PCamera;
import org.piccolo2d.PNode;
import org.piccolo2d.event.PInputEvent;
import org.piccolo2d.extras.event.PSelectionEventHandler;
import org.piccolo2d.extras.swt.PSWTPath;
import org.piccolo2d.util.PPickPath;

import ch.zuestengineering.emod.Machine;
import ch.zuestengineering.emod.gui.ContentHandlerGUI;
import ch.zuestengineering.emod.gui.help.HelpBrowserGUI;
import ch.zuestengineering.emod.gui.icons.IconHandler;
import ch.zuestengineering.emod.model.linking.IOConnection;
import ch.zuestengineering.emod.utils.LocalizationHandler;

/**
 * GraphEventHandler
 * 
 * Implements the mouse event handlings for the graph. Those are: - Selection of
 * elements by a marquee - Selection of a single element by left-click -
 * Selection of an additional element by CTRL+left-click - Editing an element by
 * right-click - Deletion of a selected element by DEL - Drawing of new
 * IOConnections
 * 
 * @author sizuest
 * 
 */
public class GraphEventHandler extends PSelectionEventHandler {

	/* Source and target node object (null if no line is to be drawn) */
	private AIONode sourceNode, targetNode;
	/* Selected lines points */
	private ConnectionLine selectedLine;
	private ArrayList<ConnectionLinePoint> selectedLinePoints;
	/* Line during the drawing of a connection */
	private PSWTPath line = new PSWTPath();
	/* Point where the mouse drag action was initiated */
	private Point2D dragSource;
	private Point2D dragSourceScaled;
	/* Line (rect) for the selection marquee */
	private PSWTPath selectionMarquee = new PSWTPath();

	private Menu menu;

	/*
	 * Parent shell, required when opening editor windows of type {@link
	 * AConfigGUI.java}
	 */
	private Shell parentShell;

	private ModelGraphGUI parent;

	/**
	 * Constructor
	 * 
	 * @param marqueeParent    PNode to add the selection marquee. Attention: This
	 *                         is for compability reasons only. The actual marquee
	 *                         is draw by this class at not by
	 *                         {@link PSelectionEventHandler} since their seams to
	 *                         be a compability issue with SWT. The marquee will
	 *                         alway be pointed the PNode raising the mouse drag
	 *                         event
	 * @param selectableParent PNode to select the children from
	 * @param parentShell      Shell of the parent, required for editor windows with
	 *                         the property SWT.APPLICATION_MODAL set
	 * @param parent
	 * @param menu
	 */
	public GraphEventHandler(final PNode marqueeParent, final PNode selectableParent, final Shell parentShell,
			ModelGraphGUI parent, final Menu menu) {
		super(marqueeParent, selectableParent);
		this.parentShell = parentShell;
		this.parent = parent;
		this.menu = menu;
	}

	/**
	 * dragActivityFirstStep
	 * 
	 * Handles the initialisation of a new drag. Compared to its parent class, the
	 * following actions are added: - If the drag is initiated over a AIONode is
	 * selected, a line is drawn form the node to the mouse pointer and all nodes of
	 * oposite type (input > output, output > input) are highlighted - If the drag
	 * is initiated over an empty space, a selection marquee is added.
	 * 
	 * @param event
	 */
	@Override
	protected void dragActivityFirstStep(final PInputEvent event) {

		if (event.getButton() == MouseEvent.BUTTON2)
			return;

		/* Check, if a connection line is selected */
		if (event.getPickedNode() instanceof ConnectionLine) {
			unselectConnectionLinePoints();
			undecorateSelectedNode(selectedLine);
			selectedLine = (ConnectionLine) event.getPickedNode();
			decorateSelectedNode(selectedLine);

			if (event.getButton() == MouseEvent.BUTTON1) {

				selectedLinePoints = selectedLine.getPoints();

				selectedLine.getParent().addChildren(selectedLinePoints);
			} else if (event.getButton() == MouseEvent.BUTTON3 & null != selectedLine) {
				selectedLine.addPoint(event.getPosition());
				selectedLinePoints = selectedLine.getPoints();

				selectedLine.getParent().addChildren(selectedLinePoints);
			}
		} else if (!(event.getPickedNode() instanceof ConnectionLinePoint)) {
			unselectConnectionLinePoints();
			undecorateSelectedNode(selectedLine);
		}

		/*
		 * We only have to care about this action, if it is raised due to a left-click
		 */
		if (event.getButton() == MouseEvent.BUTTON1) {

			/* Check, if a source node has been clicked */
			sourceNode = parent.getAIONode(event.getPosition());

			/* Save the source of the drag event */
			dragSource = event.getCanvasPosition();
			dragSourceScaled = event.getPosition();

			/* If a AIONode is selected -> start drawing the connection line */
			if (null != sourceNode) {

				/*
				 * We don't want the parent node to be selected in this particular case.
				 * Otherwise, the parent node would be moved by the mouse drag!
				 */
				unselectAll();

				/*
				 * Highlight the nodes of opposite type but the same unit, so the user knows o
				 * which nodes he can select the selected one
				 */
				if (sourceNode instanceof OutputNode)
					parent.setInputHighlight(true, sourceNode.ioObject);
				else
					parent.setOutputHighlight(true, sourceNode.ioObject);
				event.getCamera().addChild(line);
			}
			/* If the mouse is over the free area -> draw marquee */
			else if (event.getPickedNode() instanceof PCamera) {
				event.getCamera().addChild(selectionMarquee);
			}
		}

	}

	/**
	 * 
	 */
	private void unselectConnectionLinePoints() {
		if (null == selectedLinePoints)
			return;

		for (ConnectionLinePoint p : selectedLinePoints)
			try {
				p.removeFromParentOnly();
			} catch (Exception e) {
			}
	}

	/**
	 * dragActivityStep
	 * 
	 * Handles a mouse drag. Compared to its parent class, the following actions are
	 * added: - If a connection line is to be drawed, the line is updated according
	 * to the mouse position - If a selection marquee is to be drawed, the line is
	 * updated according to the mouse position
	 * 
	 * @param event
	 */
	@Override
	protected void dragActivityStep(final PInputEvent event) {

		/*
		 * Only if their is a mouse drag source different from null, a connection line
		 * needs an update
		 */

		if (event == null) {
		} else if (event.getButton() == 2)
			return;
		else if (null != sourceNode)
			updateLine(event);
		else if (event.getPickedNode() instanceof ConnectionLinePoint) {
			((ConnectionLinePoint) event.getPickedNode()).readPosition();
			decorateSelectedNode(selectedLine);
		} else if (event.getPickedNode() instanceof ConnectionLine) {
			((ConnectionLine) event.getPickedNode()).update();
			/*
			 * If the user trys to drag a line, a new point is added to the line and the
			 * selection is changed to this new point.
			 */
			/*
			 * Minimum drag distance for a new point: 10px Warning: If this is threshold is
			 * set to small, a new point will be added as soon as the line is selected. ZS
			 */
			if (dragSource.distance(event.getCanvasPosition()) > 2) {
				// Idx: Position in the line point array
				int idx = selectedLine.addPoint(dragSourceScaled);

				if (idx >= 0) {

					// Remove all old line points and fetch & display the new
					// ones
					unselectConnectionLinePoints();
					selectedLinePoints = selectedLine.getPoints();
					selectedLine.getParent().addChildren(selectedLinePoints);

					// Build a new pick path to simulated the selection of the
					// new line point
					PPickPath path = event.getPath();
					path.popNode(null);
					path.pushNode(selectedLinePoints.get(idx));

					// Unselect the line
					// if skipped, the handler will try to move the line, this
					// does not
					// look like at all. ZS
					unselectAll();
					// select the new point
					select(selectedLinePoints.get(idx));

					// Call the mouse pressed event to emulate a mouse down
					// event on the new line point
					event.setPath(path);
					mousePressed(event);
				}
			}
		}
		/*
		 * Their seams to be an other reason for an update -> let's update the marquee
		 */
		else
			updateMarquee2(event);

	}

	/**
	 * dragActivityFinalStep
	 * 
	 * Handles the finalization of a mouse drag. Compared to its parent class, the
	 * following actions are added: - Remove node highlightning - Save the element
	 * position to the elements of the {@link Machine.java} class - If a connection
	 * line was drawed, check if a new connection has to be added - As a result of a
	 * right click on a component, open a configuration windows for the particular
	 * component
	 * 
	 * @param event
	 */
	@Override
	protected void dragActivityFinalStep(final PInputEvent event) {

		if (event.getPickedNode() instanceof ConnectionLinePoint) {
			((ConnectionLinePoint) event.getPickedNode()).readPosition();
			decorateSelectedNode(selectedLine);
		}

		/* Remove highlightning */
		parent.setHighlight(false, null);

		/* Action is finished -> the line and marquee are not needed anymore */
		line.removeFromParent();
		selectionMarquee.removeFromParent();

		/* Check if their is a new connection to be added */
		IOConnection ioc = null;
		// Get the target node based on the final mouse position of the mouse
		// drag
		targetNode = parent.getAIONode(event.getPosition());
		// If the source and target node are not null, a new connection can be
		// added
		if (null != sourceNode & null != targetNode) {
			// Distinguish the two directions input>output & output>input
			if (targetNode instanceof InputNode & sourceNode instanceof OutputNode)
				ioc = Machine.addIOLink(sourceNode.getIOObject(), targetNode.getIOObject());
			else if (sourceNode instanceof InputNode & targetNode instanceof OutputNode)
				ioc = Machine.addIOLink(targetNode.getIOObject(), sourceNode.getIOObject());
		}
		// if ioc is still null, the user did a mistak (i.e. input>input)
		if (null != ioc) {
			// Let's update the model graph, so the new connection will be draw:
			parent.drawIOConnection(ioc);
			parent.updateConnections();
		}
		// We are done, set source and target node to null:
		sourceNode = null;
		targetNode = null;

		/* Save all graph element positions */
		parent.saveElementPositions();

	}

	/**
	 * keyPressed
	 * 
	 * implements the key actions for all graph elements
	 * 
	 * @param event
	 */
	@Override
	public void keyPressed(final PInputEvent event) {
		super.keyPressed(event);

		switch (event.getKeyCode()) {
		/* r: rotate 90 degrees */
		case 'r':
			@SuppressWarnings("rawtypes")
			final Iterator selectionEn = getSelection().iterator();
			while (selectionEn.hasNext()) {
				final PNode node = (PNode) selectionEn.next();
				if (node instanceof AGraphElement)
					((AGraphElement) node).rotate(.25);
			}
			/* Save all graph element positions */
			parent.saveElementPositions();
		}

		ContentHandlerGUI.updateSessionTree();
	}

	/**
	 * updates the connection line according to the mouse event
	 * 
	 * @param event
	 */
	private void updateLine(PInputEvent event) {
		// Line from the initial drag source to the current mouse position
		Point2D[] points = { dragSource, event.getCanvasPosition() };
		line.setPathToPolyline(points);
		// Apply graph scalings
		line.setScale(event.getCamera().getScale());
		// Color according to the defintions in {@link ModelGraphGUI.java}
		line.setStrokeColor(parent.getIOColor(sourceNode.getIOObject()));
	}

	/**
	 * updates the selection marquee according to the mouse event
	 * 
	 * @param event
	 */
	private void updateMarquee2(PInputEvent event) {
		// Nothing to do ...
		if (null == selectionMarquee | null == dragSource)
			return;

		/*
		 * Definitions x0/y0 : Initial drag source h x b : Current size of the marquee
		 * x1/y1 : Current mouse position
		 * 
		 * Calculations: case x0<x1 -> x0 stays coordinate, only b is adapted else -> x1
		 * becomes new marquee coordinate and b is adapted
		 * 
		 * same for y,h
		 */
		double x0 = dragSource.getX(), y0 = dragSource.getY(), h, b, x1 = event.getCanvasPosition().getX(),
				y1 = event.getCanvasPosition().getY();

		/* Determine new marquee rectangle */
		if (x0 < x1)
			b = x1 - x0;
		else {
			b = x0 - x1;
			x0 = x1;
		}

		if (y0 < y1)
			h = y1 - y0;
		else {
			h = y0 - y1;
			y0 = y1;
		}

		/* New marquee line */
		selectionMarquee.setPathToPolyline(
				new float[] { (float) x0, (float) (x0 + b), (float) (x0 + b), (float) x0, (float) x0 },
				new float[] { (float) y0, (float) y0, (float) (y0 + h), (float) (y0 + h), (float) y0 });

		/* Color properties */
		selectionMarquee.setStrokeColor(Color.BLACK);
		selectionMarquee.setPaint(Color.GRAY);
		selectionMarquee.setTransparency(0.5f);

		/* Adapt scale to camera */
		selectionMarquee.setScale(event.getCamera().getScale());
	}

	@Override
	public void decorateSelectedNode(final PNode node) {
		if (node instanceof AGraphElement)
			((AGraphElement) node).setSelected(true);
		else if (node instanceof ConnectionLine)
			((ConnectionLine) node).setSelected(true);
		else if (node instanceof ConnectionLinePoint)
			((ConnectionLinePoint) node).setSelected(true);

	}

	@Override
	public void undecorateSelectedNode(final PNode node) {
		if (node instanceof AGraphElement)
			((AGraphElement) node).setSelected(false);
		else if (node instanceof ConnectionLine)
			((ConnectionLine) node).setSelected(false);
		else if (node instanceof ConnectionLinePoint)
			((ConnectionLinePoint) node).setSelected(false);
	}

	/**
	 * Adjusts the model according to the selection
	 * 
	 * @param selection
	 */
	public void updateMenu(final PNode selection) {
		for (MenuItem mi : menu.getItems())
			mi.dispose();

		/* Machine component selected */
		if (selection instanceof MachineComponentGraphElement) {

			MenuItem itemEdit = getMenuItemProperties();
			MenuItem itemHelp = getMenuItemHelp();
			MenuItem itemDelete = getMenuItemDelete();

			itemEdit.addSelectionListener(new SelectionListener() {

				@Override
				public void widgetSelected(SelectionEvent e) {
					// Open the config window
					Shell shell = EditMachineComponentProperties.editMachineComponentGUI(parentShell,
							((MachineComponentGraphElement) selection).getMachineComponent());
					// Wait for the component window to be closed, to raise a
					// graph
					// update
					shell.addDisposeListener(new DisposeListener() {
						@Override
						public void widgetDisposed(DisposeEvent e) {
							// Update the name
							((MachineComponentGraphElement) selection).updateText();
						}
					});
				}

				@Override
				public void widgetDefaultSelected(SelectionEvent e) {
				}
			});

			itemHelp.addSelectionListener(new SelectionListener() {

				@Override
				public void widgetSelected(SelectionEvent e) {
					HelpBrowserGUI.show(parentShell.getShell(),
							"Models/Machinecomponents/" + ((MachineComponentGraphElement) selection)
									.getMachineComponent().getComponent().getModelType());
				}

				@Override
				public void widgetDefaultSelected(SelectionEvent e) {
				}
			});

			itemDelete.addSelectionListener(new SelectionListener() {

				@Override
				public void widgetSelected(SelectionEvent e) {
					((MachineComponentGraphElement) selection).removeFromParent();
					unselectAll();
				}

				@Override
				public void widgetDefaultSelected(SelectionEvent e) {
				}
			});
		} else if (selection instanceof SimulationControlGraphElement) {
			MenuItem itemEdit = getMenuItemProperties();
			MenuItem itemDelete = getMenuItemDelete();

			itemEdit.addSelectionListener(new SelectionListener() {

				@Override
				public void widgetSelected(SelectionEvent e) {
					// Open the config window
					Shell shell = EditInputGUI.editInputGUI(parentShell,
							((SimulationControlGraphElement) selection).getSimulationControl());
					// Wait for the component window to be closed, to raise a
					// graph
					// update
					shell.addDisposeListener(new DisposeListener() {
						@Override
						public void widgetDisposed(DisposeEvent e) {
							// Update the name and unit
							((SimulationControlGraphElement) selection).updateText();
						}
					});
				}

				@Override
				public void widgetDefaultSelected(SelectionEvent e) {
				}
			});

			itemDelete.addSelectionListener(new SelectionListener() {

				@Override
				public void widgetSelected(SelectionEvent e) {
					((SimulationControlGraphElement) selection).removeFromParent();
					unselectAll();
				}

				@Override
				public void widgetDefaultSelected(SelectionEvent e) {
				}
			});
		} else if (selection instanceof ConnectionLinePoint) {
			MenuItem itemDelete = getMenuItemDelete();
			itemDelete.addSelectionListener(new SelectionListener() {

				@Override
				public void widgetSelected(SelectionEvent e) {
					((ConnectionLinePoint) selection).removeFromParent();
					unselectAll();
				}

				@Override
				public void widgetDefaultSelected(SelectionEvent e) {
				}
			});
		}
	}

	@Override
	public void mousePressed(final PInputEvent event) {
		if (event.getButton() == MouseEvent.BUTTON3)
			handleRightClickEvent(event);

		super.mousePressed(event);
	}

	@Override
	public void mouseReleased(final PInputEvent event) {
		if (event.getButton() == MouseEvent.BUTTON3)
			handleRightClickEvent(event);

		super.mouseReleased(event);
	}

	private void handleRightClickEvent(final PInputEvent event) {
		updateMenu(event.getPickedNode());
	}

	private MenuItem getMenuItemProperties() {
		MenuItem item = new MenuItem(menu, SWT.NONE);
		item.setText(LocalizationHandler.getItem("app.gui.model.properties"));
		item.setImage(IconHandler.getIcon(parentShell.getDisplay(), "properties"));

		return item;
	}

	private MenuItem getMenuItemDelete() {
		MenuItem item = new MenuItem(menu, SWT.NONE);
		item.setText(LocalizationHandler.getItem("app.gui.model.delete"));
		item.setImage(IconHandler.getIcon(parentShell.getDisplay(), "delete_obj"));

		return item;
	}

	private MenuItem getMenuItemHelp() {
		MenuItem item = new MenuItem(menu, SWT.NONE);
		item.setText(LocalizationHandler.getItem("app.gui.model.help"));
		item.setImage(IconHandler.getIcon(parentShell.getDisplay(), "help_contents"));

		return item;
	}
}
