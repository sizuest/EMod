/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.gui.modelling;

/**
 * @author sizuest
 *
 */
public interface IGraphEditable {

	/**
	 * Sets the zoom level, such that all elements are visible
	 */
	public void showAll();

}
