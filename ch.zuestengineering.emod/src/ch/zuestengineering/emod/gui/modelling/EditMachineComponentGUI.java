/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.gui.modelling;

import java.io.File;
import java.util.Arrays;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.ControlListener;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;

import ch.zuestengineering.emod.Machine;
import ch.zuestengineering.emod.dd.gui.DuctConfigGraphGUI;
import ch.zuestengineering.emod.gui.AConfigGUI;
import ch.zuestengineering.emod.gui.SelectMachineComponentGUI;
import ch.zuestengineering.emod.gui.SelectMaterialGUI;
import ch.zuestengineering.emod.gui.lifecycle.EditLCPropertiesGUI;
import ch.zuestengineering.emod.gui.utils.ShellUtils;
import ch.zuestengineering.emod.gui.utils.ShowButtons;
import ch.zuestengineering.emod.model.APhysicalComponent;
import ch.zuestengineering.emod.model.MachineComponent;
import ch.zuestengineering.emod.utils.LocalizationHandler;
import ch.zuestengineering.emod.utils.PropertiesHandler;

/**
 * General GUI to Edit Machine Components
 * 
 * @author sizuest
 * 
 */
public class EditMachineComponentGUI extends AConfigGUI {

	private TabFolder tabFolder;
	private EditParameterSetGUI parameterSetGUI;
	private EditLCPropertiesGUI lcPropertiesGUI;
	private APhysicalComponent component;

	/**
	 * EditMachineComponentGUI
	 * 
	 * @param parent
	 * @param style
	 * @param type
	 * @param parameter
	 */
	public EditMachineComponentGUI(Composite parent, int style, String type, String parameter) {
		super(parent, style, ShowButtons.ALL, false);

		component = Machine.createNewMachineComponent(type, parameter);

		init();
	}

	/**
	 * EditMachineComponentGUI
	 * 
	 * @param parent
	 * @param style
	 * @param component
	 */
	public EditMachineComponentGUI(Composite parent, int style, APhysicalComponent component) {
		super(parent, style, ShowButtons.ALL, true);

		this.component = component;

		init();
	}

	private void init() {

		this.getContent().setLayout(new GridLayout(1, true));

		tabFolder = new TabFolder(getContent(), SWT.NONE);

		tabFolder.setLayout(new GridLayout(1, true));
		parameterSetGUI = new EditParameterSetGUI(tabFolder, SWT.NONE, component);
		lcPropertiesGUI = new EditLCPropertiesGUI(tabFolder, SWT.NONE, component);

		TabItem tabItemParameterSet = new TabItem(tabFolder, SWT.NONE);
		tabItemParameterSet.setText(LocalizationHandler.getItem("app.gui.model.parameterset"));
		tabItemParameterSet.setControl(parameterSetGUI);

		TabItem tabItemLC = new TabItem(tabFolder, SWT.NONE);
		tabItemLC.setText(LocalizationHandler.getItem("app.gui.model.lcproperties"));
		tabItemLC.setControl(lcPropertiesGUI);

		update();
	}

	/**
	 * Component Edit GUI for creating a new Component
	 * 
	 * @param parent
	 */
	public static void newMachineComponentGUI(final Shell parent) {
		final Shell shell = new Shell(parent, SWT.TITLE | SWT.SYSTEM_MODAL | SWT.CLOSE | SWT.MAX);
		shell.setText(LocalizationHandler.getItem("app.gui.compdb.newcomp"));
		shell.setLayout(new GridLayout(2, false));

		// Text "Type" of the Component
		Label labelComponentType = new Label(shell, SWT.NONE);
		labelComponentType.setText(LocalizationHandler.getItem("app.gui.model.type") + ":");
		labelComponentType.setLayoutData(new GridData(SWT.BEGINNING, SWT.CENTER, true, true, 1, 1));

		// Combo to select Value of "Type" of the Component
		final Combo comboComponentType = new Combo(shell, SWT.NONE);

		// according to the given component, get the path for the parameter sets
		final String path = PropertiesHandler.getProperty("app.MachineComponentDBPathPrefix") + "/";
		File subdir = new File(path);

		// check if the directory exists, then show possible parameter sets to
		// select
		if (subdir.exists()) {
			String[] subitems = subdir.list();
			Arrays.sort(subitems);
			comboComponentType.setItems(subitems);
		}

		comboComponentType.setText(LocalizationHandler.getItem("app.gui.compdb.selecttype"));
		comboComponentType.setLayoutData(new GridData(SWT.BEGINNING, SWT.CENTER, true, true, 1, 1));

		// "Manufacturer" of the component
		Label labelManufacturer = new Label(shell, SWT.NONE);
		labelManufacturer.setText(LocalizationHandler.getItem("app.gui.compdb.manufacturer"));
		labelManufacturer.setLayoutData(new GridData(SWT.BEGINNING, SWT.CENTER, true, true, 1, 1));

		final Text textManufacturer = new Text(shell, SWT.BORDER);
		textManufacturer.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true, 1, 1));

		// "Type" of the component
		Label labelType = new Label(shell, SWT.NONE);
		labelType.setText(LocalizationHandler.getItem("app.gui.compdb.type"));
		labelType.setLayoutData(new GridData(SWT.BEGINNING, SWT.CENTER, true, true, 1, 1));

		final Text textType = new Text(shell, SWT.BORDER);
		textType.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true, 1, 1));

		// button to continue
		Button buttonContinue = new Button(shell, SWT.NONE);
		buttonContinue.setText(LocalizationHandler.getItem("app.gui.continue"));
		// selection Listener for the button, actions when button is pressed
		buttonContinue.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				String stringCompTypeValue = comboComponentType.getText();
				String stringCompParamValue = textManufacturer.getText() + "_" + textType.getText();

				MachineComponent.newMachineComponentType(stringCompTypeValue, stringCompParamValue);

				shell.close();
				
				
				// Create a dummy
				APhysicalComponent component = Machine.createNewMachineComponent(stringCompTypeValue, stringCompParamValue);

				// open the edit ComponentEditGUI with the newly created
				// component file
				EditMachineComponentGUI.editMachineComponentGUI(parent, component);
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent event) {
				// Not used
			}
		});
		buttonContinue.setLayoutData(new GridData(SWT.END, SWT.CENTER, false, true, 2, 1));

		shell.pack();

		// open the new shell
		shell.open();

		ShellUtils.putToCenter(shell, parent);
	}
	
	/**
	 * editMachineComponentGUI
	 * @param parent
	 * @param mdlType
	 * @param type
	 * @return
	 */
	public static Shell editMachineComponentGUI(final Shell parent, String mdlType, String type) {
		APhysicalComponent component = Machine.createNewMachineComponent(mdlType, type);
		return editMachineComponentGUI(parent, component);
	}

	/**
	 * Component Edit GUI for editing a existing Component of the Component DB
	 * 
	 * @param parent
	 * @param component 
	 * @return
	 */
	public static Shell editMachineComponentGUI(final Shell parent, APhysicalComponent component) {
		final Shell shell = new Shell(parent, SWT.TITLE | SWT.SYSTEM_MODAL | SWT.CLOSE | SWT.MAX);
		shell.setText(LocalizationHandler.getItem("app.gui.compdb.editcomp"));
		shell.setLayout(new GridLayout(1, true));
		shell.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		final EditMachineComponentGUI gui = new EditMachineComponentGUI(shell, SWT.NONE, component);

		shell.setImages(parent.getImages());

		shell.pack();

		shell.layout();
		shell.redraw();
		shell.open();

		// width and height of the shell
		Rectangle rect = shell.getBounds();
		int[] size = { 0, 0 };
		size[0] = rect.width;
		size[1] = rect.height;

		// position the shell into the middle of the last window
		shell.setLocation(parent.getBounds().x + parent.getBounds().width / 2 - size[0] / 2,
				parent.getBounds().y + parent.getBounds().height / 2 - size[1] / 2);

		shell.setImages(parent.getShell().getImages());

		shell.addControlListener(new ControlListener() {

			@Override
			public void controlResized(ControlEvent e) {
				gui.layout();
			}

			@Override
			public void controlMoved(ControlEvent e) {
				gui.layout();
			}
		});

		shell.addDisposeListener(new DisposeListener() {

			@Override
			public void widgetDisposed(DisposeEvent e) {
				parent.setEnabled(true);
			}
		});

		gui.addDisposeListener(new DisposeListener() {
			@Override
			public void widgetDisposed(DisposeEvent e) {
				try {
					shell.dispose();
				} catch (Exception e2) {
					logger.severe("EditMachineComponentGUI: Failed to dispose shell");
				}
			}
		});

		return shell;
	}

	/**
	 * Let the user choose from the model list and write it to the table item
	 * 
	 * @param type
	 * @param item
	 */
	public void openModelSelectGUI(String type, TableItem item) {
		SelectMachineComponentGUI compGUI = new SelectMachineComponentGUI(this.getShell());
		String selection = compGUI.open(type);
		if (selection != "" & selection != null)
			item.setText(1, selection);
	}

	/**
	 * Let the user choose from the model list and write it to the table item
	 * 
	 * @param type
	 * @param item
	 * @param alowMultiselect
	 */
	public void openModelSelectGUI(String type, TableItem item, boolean alowMultiselect) {
		SelectMachineComponentGUI compGUI = new SelectMachineComponentGUI(this.getShell(), alowMultiselect);
		String selection = compGUI.open(type);
		if (selection != "" & selection != null)
			item.setText(1, selection);
	}

	/**
	 * Let the user choose from the material list and write it to the table item
	 * 
	 * @param item
	 */
	public void openMaterialSelectGUI(TableItem item) {
		SelectMaterialGUI matGUI = new SelectMaterialGUI(this.getShell());
		String selection = matGUI.open();
		if (selection != "" & selection != null)
			item.setText(1, selection);
	}

	/**
	 * Open DD for the given duct
	 * 
	 * @param type
	 * @param parameter
	 * @param name
	 */
	public void openDuctDesigner(String type, String parameter, String name) {
		DuctConfigGraphGUI.editDuctGUI(this.getShell(), type, parameter, name);
	}

	/**
	 * Set the model type in the table item
	 * 
	 * @param type
	 * @param item
	 */
	public void setModelType(String type, TableItem item) {
		if (item.getText().matches(""))
			item.setText(1, type);
		else
			item.setText(1, item.getText(1) + ", " + type);
	}

	/**
	 * Set the material type in the table item
	 * 
	 * @param type
	 * @param item
	 */
	public void setMaterialType(String type, TableItem item) {
		item.setText(1, type);
	}

	@Override
	public void update() {
		parameterSetGUI.update();
		lcPropertiesGUI.update();
	}

	@Override
	public void save() {
		parameterSetGUI.save();
		lcPropertiesGUI.save();

		update();
	}

	@Override
	public void reset() {
		parameterSetGUI.reset();
		lcPropertiesGUI.reset();

		update();
	}

}
