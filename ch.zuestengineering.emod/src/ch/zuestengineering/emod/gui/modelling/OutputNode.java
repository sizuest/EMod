/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.gui.modelling;

import org.piccolo2d.extras.swt.PSWTPath;

import ch.zuestengineering.emod.model.linking.IOContainer;

/**
 * OutputNode class
 * 
 * Graphical representation of a {@link IOContainer} used as an output in the
 * graphical model representation.
 * 
 * @author sizuest
 * 
 */
public class OutputNode extends AIONode {

	private static final long serialVersionUID = 1L;

	/**
	 * Constructor
	 * 
	 * @param parent
	 * 
	 * @param ioObject {@link IOContainer} to be represented
	 */
	public OutputNode(ModelGraphGUI parent, IOContainer ioObject) {
		super(parent);
		this.ioObject = ioObject;
		this.ioText.setText(ioObject.getName() + " [" + ioObject.getUnit().toString() + "]");

		setRight();
	}

	@Override
	public IOContainer getIOObject() {
		return this.ioObject;
	}

	@Override
	public PSWTPath getIONode() {
		return this.ioNode;
	}

	@Override
	public void updateText() {
		// Set new name and unit
		ioText.setText(ioObject.getName() + " [" + ioObject.getUnit().toString() + "]");

		// Update offset to fit the new text length
		this.ioNode.setOffset(this.ioText.getWidth() + getSize(), ioText.getHeight() / 2 - getSize() / 2);

		// Update bounds to fit the new text length
		this.setBounds(0, 0, getSize() + 5 + ioText.getWidth(), ioText.getHeight());

	}

}
