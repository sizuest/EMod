/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.gui.modelling;

import java.util.ArrayList;
import java.util.Collections;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.TableEditor;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.ControlListener;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

import ch.zuestengineering.emod.dd.gui.DuctConfigGraphGUI;
import ch.zuestengineering.emod.gui.AConfigGUI;
import ch.zuestengineering.emod.gui.SelectMachineComponentGUI;
import ch.zuestengineering.emod.gui.SelectMaterialGUI;
import ch.zuestengineering.emod.gui.utils.ShowButtons;
import ch.zuestengineering.emod.gui.utils.TableUtils;
import ch.zuestengineering.emod.model.APhysicalComponent;
import ch.zuestengineering.emod.model.ParameterCheckReport;
import ch.zuestengineering.emod.model.help.ModelDescription;
import ch.zuestengineering.emod.model.parameters.AParameter;
import ch.zuestengineering.emod.model.parameters.ParameterBoolean;
import ch.zuestengineering.emod.model.parameters.ParameterDuct;
import ch.zuestengineering.emod.model.parameters.ParameterInteger;
import ch.zuestengineering.emod.model.parameters.ParameterMaterial;
import ch.zuestengineering.emod.model.parameters.ParameterMatrix;
import ch.zuestengineering.emod.model.parameters.ParameterModel;
import ch.zuestengineering.emod.model.parameters.ParameterModels;
import ch.zuestengineering.emod.model.parameters.ParameterNumeric;
import ch.zuestengineering.emod.model.parameters.ParameterPhysical;
import ch.zuestengineering.emod.model.parameters.ParameterSet;
import ch.zuestengineering.emod.model.parameters.ParameterString;
import ch.zuestengineering.emod.model.parameters.ParameterVector;
import ch.zuestengineering.emod.model.parameters.PhysicalValue;
import ch.zuestengineering.emod.utils.LocalizationHandler;

/**
 * @author sizuest
 *
 */
public class EditParameterSetGUI extends AConfigGUI {

	private static Table tableComponent;
	private ParameterSet parameterSet;
	private ModelDescription description;
	private APhysicalComponent component;

	private boolean useParameterSet = true;

	/**
	 * EditMachineComponentGUI
	 * 
	 * @param parent
	 * @param style
	 * @param component
	 */
	public EditParameterSetGUI(Composite parent, int style, APhysicalComponent component) {
		super(parent, style, ShowButtons.NONE, false);

		this.component = component;

		init();
	}

	private void init() {

		parameterSet = component.getParameterSet().clone();

		description = ModelDescription.load(component.getModelType());

		this.getContent().setLayout(new GridLayout(1, true));

		tableComponent = new Table(this.getContent(), SWT.BORDER | SWT.SINGLE | SWT.V_SCROLL);
		tableComponent.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		tableComponent.setLinesVisible(true);
		tableComponent.setHeaderVisible(true);

		String[] titles = { LocalizationHandler.getItem("app.gui.compdb.property"),
				LocalizationHandler.getItem("app.gui.compdb.value"),
				LocalizationHandler.getItem("app.gui.compdb.unit") };
		for (int i = 0; i < titles.length; i++) {
			TableColumn column = new TableColumn(tableComponent, SWT.NULL);
			column.setText(titles[i]);
		}

		update();
	}

	/**
	 * Component Edit GUI for editing a existing Component of the Component DB
	 * 
	 * @param parent
	 * @param type
	 * @param parameter
	 * @return
	 */
	public static Shell editMachineComponentGUI(final Shell parent, final String type, final String parameter) {
		final Shell shell = new Shell(parent, SWT.TITLE | SWT.SYSTEM_MODAL | SWT.CLOSE | SWT.MAX);
		shell.setText(LocalizationHandler.getItem("app.gui.compdb.editcomp"));
		shell.setLayout(new GridLayout(1, true));
		shell.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		final EditMachineComponentGUI gui = new EditMachineComponentGUI(shell, SWT.NONE, type, parameter);

		shell.setImages(parent.getImages());

		shell.pack();

		shell.layout();
		shell.redraw();
		shell.open();

		// width and height of the shell
		Rectangle rect = shell.getBounds();
		int[] size = { 0, 0 };
		size[0] = rect.width;
		size[1] = rect.height;

		// position the shell into the middle of the last window
		shell.setLocation(parent.getBounds().x + parent.getBounds().width / 2 - size[0] / 2,
				parent.getBounds().y + parent.getBounds().height / 2 - size[1] / 2);

		shell.setImages(parent.getShell().getImages());

		shell.addControlListener(new ControlListener() {

			@Override
			public void controlResized(ControlEvent e) {
				gui.layout();
			}

			@Override
			public void controlMoved(ControlEvent e) {
				gui.layout();
			}
		});

		shell.addDisposeListener(new DisposeListener() {

			@Override
			public void widgetDisposed(DisposeEvent e) {
				parent.setEnabled(true);
			}
		});

		gui.addDisposeListener(new DisposeListener() {
			@Override
			public void widgetDisposed(DisposeEvent e) {
				try {
					shell.dispose();
				} catch (Exception e2) {
					logger.severe("EditMachineComponentGUI: Failed to dispose shell");
				}
			}
		});

		return shell;
	}

	/**
	 * Let the user choose from the model list and write it to the table item
	 * 
	 * @param type
	 * @param item
	 */
	public void openModelSelectGUI(String type, TableItem item) {
		SelectMachineComponentGUI compGUI = new SelectMachineComponentGUI(this.getShell());
		String selection = compGUI.open(type);
		if (selection != "" & selection != null)
			item.setText(1, selection);
	}

	/**
	 * Let the user choose from the model list and write it to the table item
	 * 
	 * @param type
	 * @param item
	 * @param alowMultiselect
	 */
	public void openModelSelectGUI(String type, TableItem item, boolean alowMultiselect) {
		SelectMachineComponentGUI compGUI = new SelectMachineComponentGUI(this.getShell(), alowMultiselect);
		String selection = compGUI.open(type);
		if (selection != "" & selection != null)
			item.setText(1, selection);
	}

	/**
	 * Let the user choose from the material list and write it to the table item
	 * 
	 * @param item
	 */
	public void openMaterialSelectGUI(TableItem item) {
		SelectMaterialGUI matGUI = new SelectMaterialGUI(this.getShell());
		String selection = matGUI.open();
		if (selection != "" & selection != null)
			item.setText(1, selection);
	}

	/**
	 * Open DD for the given duct
	 * 
	 * @param type
	 * @param parameter
	 * @param name
	 */
	public void openDuctDesigner(String type, String parameter, String name) {
		DuctConfigGraphGUI.editDuctGUI(this.getShell(), type, parameter, name);
	}

	/**
	 * Set the model type in the table item
	 * 
	 * @param type
	 * @param item
	 */
	public void setModelType(String type, TableItem item) {
		if (item.getText().matches(""))
			item.setText(1, type);
		else
			item.setText(1, item.getText(1) + ", " + type);
	}

	/**
	 * Set the material type in the table item
	 * 
	 * @param type
	 * @param item
	 */
	public void setMaterialType(String type, TableItem item) {
		item.setText(1, type);
	}

	@Override
	public void update() {
		if ((parameterSet == null & useParameterSet) | (null == component & !useParameterSet))
			return;

		for (Control c : tableComponent.getChildren())
			c.dispose();

		try {
			TableUtils.addCellEditor(tableComponent, this, new int[] { 1 });
			TableUtils.addItemToolTip(tableComponent, this);
		} catch (Exception e) {
			e.printStackTrace();
		}

		/* Fetch error report */
		ParameterCheckReport report = component.checkConfigParams();

		tableComponent.setItemCount(0);

		ArrayList<String> keys = parameterSet.getNames();
		Collections.sort(keys);

		for (String key : keys) {

			try {
				String value, unit;
				final AParameter<?> parameter = parameterSet.get(key);

				if (parameter instanceof ParameterPhysical) {
					value = ((PhysicalValue) parameter.getValue()).getValuesAsString();
					unit  = ((PhysicalValue) parameter.getValue()).getUnit().toString();
				} else {
					value = parameter.getValueAsString();
					unit = "";
				}

				int i = tableComponent.getItemCount();
				final TableItem itemProp = new TableItem(tableComponent, SWT.NONE, i);
				TableEditor editorButton = new TableEditor(tableComponent);

				itemProp.setText(0, key);
				itemProp.setText(1, value);
				itemProp.setText(2, unit);
				tableComponent.setToolTipText("");

				if (report.hasErrors(key)) {
					itemProp.setBackground(new Color(getDisplay(), 255, 200, 200));
					// TODO: Add info about error
				}

				/* SPECIAL CASE: Material */
				if (parameter instanceof ParameterMaterial) {
					final Button selectMaterialButton = new Button(tableComponent, SWT.PUSH);
					selectMaterialButton.setText("...");
					selectMaterialButton.setLayoutData(new GridData(SWT.FILL, SWT.TOP, false, true, 1, 1));
					selectMaterialButton.addSelectionListener(new SelectionListener() {
						@Override
						public void widgetSelected(SelectionEvent event) {
							openMaterialSelectGUI(itemProp);
							wasEdited();
						}

						@Override
						public void widgetDefaultSelected(SelectionEvent event) {
							// Not used
						}
					});
					selectMaterialButton.pack();
					editorButton.minimumWidth = selectMaterialButton.getSize().x;
					editorButton.horizontalAlignment = SWT.LEFT;
					editorButton.setEditor(selectMaterialButton, itemProp, 2);
				}
				/* SPECIAL CASE: Model */
				else if (parameter instanceof ParameterModel) {
					final Button selectMaterialButton = new Button(tableComponent, SWT.PUSH);
					selectMaterialButton.setText("...");
					selectMaterialButton.setLayoutData(new GridData(SWT.FILL, SWT.TOP, false, true, 1, 1));
					selectMaterialButton.addSelectionListener(new SelectionListener() {
						@Override
						public void widgetSelected(SelectionEvent event) {
							openModelSelectGUI(((ParameterModel) parameter).getFilter(), itemProp, false);
							wasEdited();
						}

						@Override
						public void widgetDefaultSelected(SelectionEvent event) {
							// Not used
						}
					});
					selectMaterialButton.pack();
					editorButton.minimumWidth = selectMaterialButton.getSize().x;
					editorButton.horizontalAlignment = SWT.LEFT;
					editorButton.setEditor(selectMaterialButton, itemProp, 2);
				}

				/* SPECIAL CASE: Models */
				else if (parameter instanceof ParameterModels) {
					final Button selectMaterialButton = new Button(tableComponent, SWT.PUSH);
					selectMaterialButton.setText("...");
					selectMaterialButton.setLayoutData(new GridData(SWT.FILL, SWT.TOP, false, true, 1, 1));
					selectMaterialButton.addSelectionListener(new SelectionListener() {
						@Override
						public void widgetSelected(SelectionEvent event) {
							openModelSelectGUI(((ParameterModels) parameter).getFilter(), itemProp, true);
							wasEdited();
						}

						@Override
						public void widgetDefaultSelected(SelectionEvent event) {
							// Not used
						}
					});
					selectMaterialButton.pack();
					editorButton.minimumWidth = selectMaterialButton.getSize().x;
					editorButton.horizontalAlignment = SWT.LEFT;
					editorButton.setEditor(selectMaterialButton, itemProp, 2);
				}

				/* SPECIAL CASE: Duct */
				else if (parameter instanceof ParameterDuct) {
					final Button editDuctButton = new Button(tableComponent, SWT.PUSH);
					editDuctButton.setText("...");
					editDuctButton.setLayoutData(new GridData(SWT.FILL, SWT.TOP, false, true, 1, 1));
					editDuctButton.addSelectionListener(new SelectionListener() {
						@Override
						public void widgetSelected(SelectionEvent event) {
							DuctConfigGraphGUI.editDuctGUI(getShell(), ((ParameterDuct) parameter).getValue());
							wasEdited();
						}

						@Override
						public void widgetDefaultSelected(SelectionEvent event) {
							// Not used
						}
					});
					editDuctButton.pack();
					editorButton.minimumWidth = editDuctButton.getSize().x;
					editorButton.horizontalAlignment = SWT.LEFT;
					editorButton.setEditor(editDuctButton, itemProp, 2);
				}

				/* SPECIAL CASE: Number of ... */
				else if (parameter instanceof ParameterInteger) {
					final Spinner spinner = new Spinner(tableComponent, SWT.NONE);
					spinner.setSelection((int) (double) Double.valueOf(value));
					spinner.addSelectionListener(new SelectionListener() {

						@Override
						public void widgetSelected(SelectionEvent e) {
							itemProp.setText(1, spinner.getSelection() + "");
						}

						@Override
						public void widgetDefaultSelected(SelectionEvent e) {
						}
					});

					spinner.pack();
					editorButton.minimumWidth = spinner.getSize().x;
					editorButton.horizontalAlignment = SWT.LEFT;
					editorButton.grabHorizontal = true;
					editorButton.setEditor(spinner, itemProp, 1);
				}
			} catch (Exception e) {
				logger.warning("Failed to load Property '" + key + "' from '" + component.getModelType() + ":"
						+ component.getType() + "'");
			}
		}

		TableColumn[] columns = tableComponent.getColumns();
		for (int j = 0; j < columns.length; j++) {
			columns[j].pack();
		}

		if (columns[1].getWidth() > 500) {
			columns[1].setWidth(500);
		}
	}

	@Override
	public void save() {

		for (TableItem ti : tableComponent.getItems()) {
			AParameter<?> parameter = parameterSet.get(ti.getText(0));

			if (parameter instanceof ParameterInteger)
				((ParameterInteger) parameter).setValue(Integer.valueOf(ti.getText(1)));
			if (parameter instanceof ParameterNumeric)
				((ParameterNumeric) parameter).setValue(Double.valueOf(ti.getText(1)));
			if (parameter instanceof ParameterBoolean)
				((ParameterBoolean) parameter).setValue(Boolean.valueOf(ti.getText(1)));
			if (parameter instanceof ParameterString)
				((ParameterString) parameter).setValue(ti.getText(1));
			if (parameter instanceof ParameterVector)
				((ParameterVector) parameter).setValue(ti.getText(1));
			if (parameter instanceof ParameterMatrix)
				((ParameterMatrix) parameter).setValue(ti.getText(1));
			if (parameter instanceof ParameterPhysical)
				((ParameterPhysical) parameter).setValue(ti.getText(1));
			if (parameter instanceof ParameterMaterial)
				((ParameterMaterial) parameter).setValue(ti.getText(1));
			if (parameter instanceof ParameterModel)
				((ParameterModel) parameter).setValue(ti.getText(1));
			if (parameter instanceof ParameterModels)
				((ParameterModels) parameter).setValue(ti.getText(1));

		}

		component.setParameterSet(parameterSet);
		component.saveTechnicalParameters();

		update();
	}

	@Override
	public void reset() {
		parameterSet = component.getParameterSet().clone();

		update();
	}

	@Override
	public String getToolTip(TableItem item) {
		return item.getText(0) + " [" + item.getText(2) + "]: " + description.getParameterDescription(item.getText(0));
	}
}
