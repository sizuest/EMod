/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.gui;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;

import ch.zuestengineering.emod.gui.utils.MaterialHandler;
import ch.zuestengineering.emod.model.material.MaterialType;
import ch.zuestengineering.emod.utils.LocalizationHandler;

/**
 * Implements a Material selection GUI;
 * 
 * @author sizuest
 * 
 */
public class SelectMaterialGUI extends Dialog {
	private String input;
	private boolean allowMultiSelection = false; 
	private MaterialType[] filter = MaterialType.values();

	// tree to list all the materials
	private Tree treeMaterialDBView;

	/**
	 * SelectMaterialGUI
	 * 
	 * @param parent
	 */
	public SelectMaterialGUI(Shell parent) {
		this(parent, SWT.DIALOG_TRIM | SWT.APPLICATION_MODAL);
	}

	/**
	 * New material selection gui
	 * 
	 * @param parent
	 * @param style
	 */
	public SelectMaterialGUI(Shell parent, int style) {
		super(parent, style);
	}
	
	/**
	 * New material selection gui
	 * 
	 * @param parent
	 * @param allowMultiSelection 
	 */
	public SelectMaterialGUI(Shell parent, boolean allowMultiSelection) {
		super(parent, SWT.DIALOG_TRIM | SWT.APPLICATION_MODAL);
		
		this.allowMultiSelection = allowMultiSelection;
	}
	
	/**
	 * New material selection gui
	 * 
	 * @param parent
	 * @param allowMultiSelection 
	 * @param filter 
	 */
	public SelectMaterialGUI(Shell parent, boolean allowMultiSelection, MaterialType[] filter) {
		super(parent, SWT.DIALOG_TRIM | SWT.APPLICATION_MODAL);
		
		this.allowMultiSelection = allowMultiSelection;
		this.filter = filter;
	}
	
	/**
	 * New material selection gui
	 * 
	 * @param parent
	 * @param style
	 * @param allowMultiSelection 
	 */
	public SelectMaterialGUI(Shell parent, int style, boolean allowMultiSelection) {
		super(parent, style);
		
		this.allowMultiSelection = allowMultiSelection;
	}

	/**
	 * Open the selection GUI
	 * 
	 * @return
	 */
	public String open() {
		Shell shell = new Shell(getParent(), getStyle());
		shell.setText(LocalizationHandler.getItem("app.gui.matdb.title"));
		shell.setSize(400, 600);
		shell.setLayout(new GridLayout(2, false));

		createContents(shell);
		shell.pack();
		shell.layout();
		shell.open();

		shell.setSize(400, 600);

		// width and height of the shell
		Rectangle rect = shell.getBounds();
		int[] size = { 0, 0 };
		size[0] = rect.width;
		size[1] = rect.height;

		// position the shell into the middle of the last window
		shell.setLocation(
				getParent().getShell().getBounds().x + getParent().getShell().getBounds().width / 2 - size[0] / 2,
				getParent().getShell().getBounds().y + getParent().getShell().getBounds().height / 2 - size[1] / 2);

		Display display = getParent().getDisplay();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		// Return the entered value, or null
		return input;

	}

	private void createContents(final Shell shell) {
		// create tree element and fill it with the components from the DB
		if(allowMultiSelection)
			treeMaterialDBView = new Tree(shell, SWT.MULTI | SWT.BORDER | SWT.V_SCROLL);
		else
			treeMaterialDBView = new Tree(shell, SWT.SINGLE | SWT.BORDER | SWT.V_SCROLL);
		treeMaterialDBView.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 2, 1));
		if(null==filter)
			MaterialHandler.fillTree(treeMaterialDBView);
		else
			MaterialHandler.fillTree(treeMaterialDBView, filter);

		/* Close Button */
		final Button closeComponentButton = new Button(shell, SWT.PUSH);
		closeComponentButton.setText("Close");
		closeComponentButton.setLayoutData(new GridData(SWT.RIGHT, SWT.TOP, false, false, 1, 1));
		closeComponentButton.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				input = "";
				shell.close();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent event) {
				// Not used
			}
		});

		/* Select Button */
		final Button selectComponentButton = new Button(shell, SWT.PUSH);
		selectComponentButton.setText("OK");
		selectComponentButton.setLayoutData(new GridData(SWT.RIGHT, SWT.TOP, false, false, 1, 1));
		selectComponentButton.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				input = getSelectionToString();
				shell.close();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent event) {
				// Not used
			}
		});

		shell.setDefaultButton(selectComponentButton);
	}

	private String getSelectionToString() {
		if(allowMultiSelection)
			return getMultiSelectionToString();
		else
			return getSingleSelectionToString();
	}
	
	private String getSingleSelectionToString() {
		for (TreeItem ti : treeMaterialDBView.getItems())
			if (treeMaterialDBView.getSelection()[0].equals(ti))
				return "";
		
		return treeMaterialDBView.getSelection()[0].getText();
	}
	
	private String getMultiSelectionToString() {
		String ret="";
		for(TreeItem ti: treeMaterialDBView.getSelection())
			for (TreeItem tii : treeMaterialDBView.getItems())
				if(!ti.equals(tii))
					ret+=ti.getText()+",";
		
		return ret.substring(0, ret.length()-1);
			
	}

}
