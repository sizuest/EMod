/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.gui.lifecycle;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

import ch.zuestengineering.emod.gui.ContentHandlerGUI;
import ch.zuestengineering.emod.gui.utils.TableUtils;
import ch.zuestengineering.emod.lca.inventory.StateDistribution;
import ch.zuestengineering.emod.simulation.MachineState;

/**
 * @author sizuest
 *
 */
public class StateList extends AList {

	StateDistribution states;

	/**
	 * @param parent
	 * @param style
	 * @param title
	 * @param states
	 */
	public StateList(Composite parent, int style, String title, StateDistribution states) {
		super(parent, style);

		this.states = states;

		String[] bTitles = { "State", "Share" };
		for (int i = 0; i < bTitles.length; i++) {
			TableColumn column = new TableColumn(list, SWT.NULL);
			column.setText(bTitles[i]);

			if (i > 0)
				column.setAlignment(SWT.RIGHT);
		}

		update();

	}

	public void update() {
		for (TableItem ti : list.getItems())
			ti.dispose();
		list.setItemCount(0);

		if (null == states) {
			TableItem item = new TableItem(list, SWT.NONE);
			item.setText(0, "none");
			return;
		}
		for (MachineState s : MachineState.values()) {
			TableItem item = new TableItem(list, SWT.NONE);
			item.setText(0, s.toString());
			item.setText(1, states.getStateShareInPrct(s) + " %");
		}

		TableUtils.packTable(list);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.zuestengineering.emod.gui.lifecycle.AList#edit()
	 */
	@Override
	public void save() {
		ContentHandlerGUI.showStatesConfig();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.zuestengineering.emod.gui.lifecycle.AList#reset()
	 */
	@Override
	public void reset() {
		// TODO Auto-generated method stub

	}

}
