/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.gui.lifecycle;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CCombo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

import ch.zuestengineering.emod.gui.utils.TableUtils;
import ch.zuestengineering.emod.lca.inventory.RessourceDemand;
import ch.zuestengineering.emod.model.material.Material;
import ch.zuestengineering.emod.simulation.MachineState;
import ch.zuestengineering.emod.utils.LocalizationHandler;

/**
 * @author sizuest
 *
 */
public class RessourceList extends AList {

	ArrayList<RessourceDemand> ressources;

	/**
	 * @param parent
	 * @param style
	 * @param ressources
	 */
	public RessourceList(Composite parent, int style, ArrayList<RessourceDemand> ressources) {
		super(parent, style);

		this.ressources = ressources;

		init();
	}

	private void init() {

		TableColumn column = new TableColumn(list, SWT.NULL);
		column.setText(LocalizationHandler.getItem("app.gui.model.lcproperties.ressource.type"));
		for (MachineState ms : MachineState.values()) {
			TableColumn column2 = new TableColumn(list, SWT.NULL);
			column2.setText(
					ms.toString() + " " + LocalizationHandler.getItem("app.gui.model.lcproperties.ressource.unit"));
			column2.setAlignment(SWT.RIGHT);
		}

		try {
			TableUtils.addCellEditor(list, this.getClass().getDeclaredMethod("checkAmounts"), this, null,
					new int[] { 0 });
		} catch (Exception e) {
		}

		update();
		packTable();

	}

	/**
	 * Removes all items in the list with zero amount
	 */
	public void checkAmounts() {

		for (TableItem ti : list.getItems()) {
			if (ti.getText(0).equals(""))
				ti.dispose();
			else {
				double amount = 0;

				for (int j = 0; j < MachineState.values().length; j++) {
					try {
						amount += Double.valueOf(ti.getText(j + 1));
					} catch (Exception e) {
					}
				}

				if (0 == amount)
					ti.dispose();
			}
		}

		packTable();

	}

	private void packTable() {
		for (Control c : list.getChildren())
			if (c instanceof CCombo)
				c.dispose();

		TableUtils.packTable(list);

		for (Control c : list.getChildren())
			if (c instanceof CCombo) {
				list.getColumn(2).setWidth(c.getSize().x);
				break;
			}

		// Add new item at the bottom
		TableItem item = new TableItem(list, SWT.NONE);
		item.setText(0, "");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.zuestengineering.emod.gui.lifecycle.AList#save()
	 */
	@Override
	public void save() {
		ressources.clear();

		for (TableItem ti : list.getItems()) {
			if (ti.getText(0).equals(""))
				continue;
			try {
				Map<MachineState, Double> demands = new HashMap<MachineState, Double>();
				int i = 1;
				for (MachineState ms : MachineState.values()) {
					try {
						demands.put(ms, Double.valueOf(ti.getText(i++)) / 60.0);
					} catch (Exception e) {
						demands.put(ms, 0.0);
					}
				}
				ressources.add(new RessourceDemand(demands, new Material(ti.getText(0))));
			} catch (Exception e) {
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.zuestengineering.emod.gui.lifecycle.AList#reset()
	 */
	@Override
	public void reset() {
		update();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.zuestengineering.emod.gui.lifecycle.AList#update()
	 */
	@Override
	public void update() {
		for (TableItem ti : list.getItems())
			ti.dispose();

		list.setItemCount(0);

		if (null == ressources) {
			TableItem item = new TableItem(list, SWT.NONE);
			item.setText(0, "none");
			return;
		}
		if (ressources.size() == 0) {
			TableItem item = new TableItem(list, SWT.NONE);
			item.setText(0, "none");
			return;
		}
		for (RessourceDemand r : ressources) {
			TableItem item = new TableItem(list, SWT.NONE);
			item.setText(0, r.getMaterial().toString());

			int i = 1;
			for (MachineState ms : MachineState.values())
				item.setText(i++, r.getDemand(ms) * 60 + "");
		}

		packTable();

	}
}
