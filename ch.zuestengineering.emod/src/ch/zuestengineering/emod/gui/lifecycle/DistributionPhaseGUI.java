/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.gui.lifecycle;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;

import ch.zuestengineering.emod.gui.AConfigGUI;
import ch.zuestengineering.emod.gui.utils.ShowButtons;
import ch.zuestengineering.emod.lca.inventory.AInventory;
import ch.zuestengineering.emod.lca.inventory.InventoryMachine;
import ch.zuestengineering.emod.utils.LocalizationHandler;

/**
 * @author sizuest
 *
 */
public class DistributionPhaseGUI extends AConfigGUI {

	private MaterialList packaging;
	private MaterialList commissioning;
	private TransportList transportation;

	private AInventory inventory;

	/**
	 * @param parent
	 * @param style
	 * @param inventory
	 */
	public DistributionPhaseGUI(Composite parent, int style, AInventory inventory) {
		super(parent, style, ShowButtons.NONE);

		this.inventory = inventory;

		init();
	}

	private void init() {

		final Group groupPackaging = new Group(getContent(), SWT.NONE);
		groupPackaging.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		groupPackaging.setLayout(new GridLayout(1, false));
		groupPackaging.setText(LocalizationHandler.getItem("app.gui.model.lc.distribution.packaging"));

		final Group groupTransportation = new Group(getContent(), SWT.NONE);
		groupTransportation.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		groupTransportation.setLayout(new GridLayout(1, false));
		groupTransportation.setText(LocalizationHandler.getItem("app.gui.model.lc.distribution.transportation"));

		packaging = new MaterialList(groupPackaging, SWT.NONE, inventory.getPackagingMaterials());
		packaging.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));

		transportation = new TransportList(groupTransportation, SWT.NONE, inventory.getDistributionTransport());
		transportation.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));

		if (inventory instanceof InventoryMachine) {

			final Group groupCommissioning = new Group(getContent(), SWT.NONE);
			groupCommissioning.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, true, 1, 1));
			groupCommissioning.setLayout(new GridLayout(1, false));
			groupCommissioning.setText(LocalizationHandler.getItem("app.gui.model.lc.distribution.commissioning"));

			commissioning = new MaterialList(groupCommissioning, SWT.NONE,
					((InventoryMachine) inventory).getCommissioningMaterials());
			commissioning.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		}

		reset();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.zuestengineering.emod.gui.AConfigGUI#save()
	 */
	@Override
	public void save() {
		packaging.save();
		transportation.save();
		if (inventory instanceof InventoryMachine)
			commissioning.save();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.zuestengineering.emod.gui.AConfigGUI#reset()
	 */
	@Override
	public void reset() {
		packaging.reset();
		transportation.reset();
		if (inventory instanceof InventoryMachine)
			commissioning.reset();

		redraw();

	}

}
