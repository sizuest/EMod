/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.gui.lifecycle;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.layout.FillLayout;

import ch.zuestengineering.emod.EModSession;
import ch.zuestengineering.emod.LifeCycle;
import ch.zuestengineering.emod.gui.AGUITab;
import ch.zuestengineering.emod.gui.EModStatusBarGUI;
import ch.zuestengineering.emod.lca.LifeCycleResultFile;
import ch.zuestengineering.emod.utils.LocalizationHandler;

/**
 * @author sizuest
 *
 */
public class LifeCycleAnalysis extends AGUITab {

	private LifeCycleChart lcChart;
	private LifeCycleResultFile results;

	/**
	 * @param parent
	 */
	public LifeCycleAnalysis(CTabFolder parent) {
		super(parent, SWT.CLOSE, LocalizationHandler.getItem("app.gui.tabs.lc"));
		
		results = LifeCycleResultFile.load(EModSession.getLCResultFilePath());
		
		init();
	}
	
	/**
	 * @param parent
	 * @param machine 
	 * @param sim 
	 * @param process 
	 */
	public LifeCycleAnalysis(CTabFolder parent, String machine, String sim, String process) {
		super(parent, SWT.CLOSE, LocalizationHandler.getItem("app.gui.tabs.lc"));
		
		results = LifeCycleResultFile.load(EModSession.getLCResultFilePath(machine, sim, process));
		
		init();
	}
	
	
	/**
	 * @param parent
	 * @param path 
	 */
	public LifeCycleAnalysis(CTabFolder parent, String path) {
		super(parent, SWT.CLOSE, LocalizationHandler.getItem("app.gui.tabs.lc"));
		
		results = LifeCycleResultFile.load(path);
		
		init();
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.zuestengineering.emod.gui.AGUITab#init()
	 */
	@Override
	public void init() {
		this.setLayout(new FillLayout());
		lcChart = new LifeCycleChart(this, results.getResult());
		EModStatusBarGUI.getProgressBar().reset();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.zuestengineering.emod.gui.AGUITab#update()
	 */
	@Override
	public void update() {
		LifeCycle.fetch();
		lcChart.fillChart(results.getResult());
		EModStatusBarGUI.getProgressBar().reset();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.zuestengineering.emod.gui.AGUITab#save()
	 */
	@Override
	public void save() {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.zuestengineering.emod.gui.AGUITab#wasEdited()
	 */
	@Override
	public void wasEdited() {
		// TODO Auto-generated method stub

	}

}
