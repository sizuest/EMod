/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.gui.lifecycle;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;

import ch.zuestengineering.emod.gui.AConfigGUI;
import ch.zuestengineering.emod.gui.utils.ShowButtons;
import ch.zuestengineering.emod.lca.LCScenario;
import ch.zuestengineering.emod.lca.building.BuildingClass;
import ch.zuestengineering.emod.lca.typedef.ElectricEnergySource;
import ch.zuestengineering.emod.utils.LocalizationHandler;

/**
 * @author sizuest
 *
 */
public class LifeCycleScenarioGUI extends AConfigGUI {

	private Spinner spinnerNumberOfUnits;
	private Text textLifeTime;
	private Text textServiceInterval;
	private Button checkHVAC;
	private Combo comboBuildingClass;
	private Combo comboEnergySource;
	private Button checkLocalService;

	private LCScenario scenario;

	/**
	 * @param parent
	 * @param style
	 * @param scenario
	 */
	public LifeCycleScenarioGUI(Composite parent, int style, LCScenario scenario) {
		super(parent, style, ShowButtons.NONE);

		this.scenario = scenario;
		init();
	}

	private void init() {
		this.getContent().setLayout(new GridLayout(1, true));

		Group groupGeneral = new Group(getContent(), SWT.NONE);
		groupGeneral.setLayoutData(new GridData(SWT.FILL, SWT.TOP, false, false, 1, 1));
		groupGeneral.setLayout(new GridLayout(2, false));
		groupGeneral.setText(LocalizationHandler.getItem("app.gui.model.lcproperties.general"));

		Group groupBuilding = new Group(getContent(), SWT.NONE);
		groupBuilding.setLayoutData(new GridData(SWT.FILL, SWT.TOP, false, false, 1, 1));
		groupBuilding.setLayout(new GridLayout(2, false));
		groupBuilding.setText(LocalizationHandler.getItem("app.gui.model.lcproperties.building"));

		Group groupService = new Group(getContent(), SWT.NONE);
		groupService.setLayoutData(new GridData(SWT.FILL, SWT.TOP, false, false, 1, 1));
		groupService.setLayout(new GridLayout(2, false));
		groupService.setText(LocalizationHandler.getItem("app.gui.model.lcproperties.services"));

		Label labelNumberOfUnits = new Label(groupGeneral, SWT.NONE);
		labelNumberOfUnits.setText(LocalizationHandler.getItem("app.gui.model.lcproperties.general.numberofunits"));
		labelNumberOfUnits.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));
		spinnerNumberOfUnits = new Spinner(groupGeneral, SWT.BORDER);
		spinnerNumberOfUnits.setLayoutData(new GridData(SWT.FILL, SWT.TOP, false, false, 1, 1));

		Label labelLifeTime = new Label(groupGeneral, SWT.NONE);
		labelLifeTime.setText(LocalizationHandler.getItem("app.gui.model.lcproperties.general.lifetime"));
		labelLifeTime.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));
		textLifeTime = new Text(groupGeneral, SWT.BORDER);
		textLifeTime.setLayoutData(new GridData(SWT.FILL, SWT.TOP, false, false, 1, 1));

		Label labelBuildingClass = new Label(groupBuilding, SWT.NONE);
		labelBuildingClass.setText(LocalizationHandler.getItem("app.gui.model.lcproperties.building.class"));
		labelBuildingClass.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));
		comboBuildingClass = new Combo(groupBuilding, SWT.BORDER);
		comboBuildingClass.setLayoutData(new GridData(SWT.FILL, SWT.TOP, false, false, 1, 1));
		String[] entriesBC = new String[BuildingClass.values().length];
		for (int i = 0; i < entriesBC.length; i++)
			entriesBC[i] = BuildingClass.values()[i].toString();
		comboBuildingClass.setItems(entriesBC);

		Label labelBuildingEnergy = new Label(groupBuilding, SWT.NONE);
		labelBuildingEnergy.setText(LocalizationHandler.getItem("app.gui.model.lcproperties.building.energysource"));
		labelBuildingEnergy.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));
		comboEnergySource = new Combo(groupBuilding, SWT.BORDER);
		comboEnergySource.setLayoutData(new GridData(SWT.FILL, SWT.TOP, false, false, 1, 1));
		String[] entriesEES = new String[ElectricEnergySource.values().length];
		for (int i = 0; i < entriesEES.length; i++)
			entriesEES[i] = ElectricEnergySource.values()[i].toString();
		comboEnergySource.setItems(entriesEES);

		Label labelBuildingHVAC = new Label(groupBuilding, SWT.NONE);
		labelBuildingHVAC.setText(LocalizationHandler.getItem("app.gui.model.lcproperties.building.hvac"));
		labelBuildingHVAC.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));
		checkHVAC = new Button(groupBuilding, SWT.CHECK);
		checkHVAC.setLayoutData(new GridData(SWT.RIGHT, SWT.TOP, false, false, 1, 1));

		Label labelBuildingServiceInterval = new Label(groupService, SWT.NONE);
		labelBuildingServiceInterval
				.setText(LocalizationHandler.getItem("app.gui.model.lcproperties.services.interval"));
		labelBuildingServiceInterval.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));
		textServiceInterval = new Text(groupService, SWT.BORDER);
		textServiceInterval.setLayoutData(new GridData(SWT.FILL, SWT.TOP, false, false, 1, 1));

		Label labelBuildingServiceLocal = new Label(groupService, SWT.NONE);
		labelBuildingServiceLocal.setText(LocalizationHandler.getItem("app.gui.model.lcproperties.services.local"));
		labelBuildingServiceLocal.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));
		checkLocalService = new Button(groupService, SWT.CHECK);
		checkLocalService.setLayoutData(new GridData(SWT.RIGHT, SWT.TOP, false, false, 1, 1));

		reset();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.zuestengineering.emod.gui.AConfigGUI#save()
	 */
	@Override
	public void save() {
		// Number of units
		try {
			scenario.setNumberOfUnits(spinnerNumberOfUnits.getSelection());
		} catch (NumberFormatException e) {
		}
		// Life time
		try {
			scenario.setProductLifeTimeInYears(Double.valueOf(textLifeTime.getText()));
		} catch (NumberFormatException e) {
		}
		// Efficiency class
		try {
			scenario.setBuildingClass(BuildingClass.valueOf(comboBuildingClass.getText()));
		} catch (NumberFormatException e) {
		}
		// Electric energy source
		try {
			scenario.setElectricEnergySource(ElectricEnergySource.valueOf(comboEnergySource.getText()));
		} catch (NumberFormatException e) {
		}
		// Consider hvac
		scenario.setIncludeHVAC(checkHVAC.getSelection());
		// Service interval
		try {
			scenario.setServiceIntervalInYears(Double.valueOf(textServiceInterval.getText()));
		} catch (NumberFormatException e) {
		}
		// Service location
		scenario.setLocalService(checkLocalService.getSelection());

		reset();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.zuestengineering.emod.gui.AConfigGUI#reset()
	 */
	@Override
	public void reset() {
		textLifeTime.setText(scenario.getProductLifeTimeInYears() + "");
		spinnerNumberOfUnits.setSelection(scenario.getNumberOfUnits());
		textServiceInterval.setText(scenario.getServiceIntervalInYears() + "");

		comboBuildingClass.setText(scenario.getBuildingClass().toString());
		comboEnergySource.setText(scenario.getElectricEnergySource().toString());

		checkHVAC.setSelection(scenario.getIncludeHVAC());
		checkLocalService.setSelection(scenario.isLocalService());
	}

}
