/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.gui.lifecycle;

import java.text.DecimalFormat;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.swtchart.Chart;

import ch.zuestengineering.emod.gui.utils.AChart;
import ch.zuestengineering.emod.lca.LifeCycleResult;
import ch.zuestengineering.emod.lca.SingleResult;

/**
 * Implementation of the generic chart functions for an LC analysis
 * 
 * @author simon
 *
 */
public class LifeCycleChart extends AChart {

	/**
	 * @param parent
	 * @param data
	 */
	public LifeCycleChart(Composite parent, LifeCycleResult data) {
		super(parent, SWT.NONE);

		fillChart(data);

	}

	/**
	 * Redraws the chart with the provided data
	 * 
	 * @param data
	 */
	public void redrawChart(LifeCycleResult data) {
		fillChart(data);
		getChart().setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		adjustLineWidth();
		getChart().redraw();
	}

	protected Chart fillChart(LifeCycleResult data) {
		if (getChart() != null)
			try {
				clearChart();
			} catch(Exception e) {}

		if (0 == data.size())
			return getChart();

		data.toChart(getChart());

		try {

			String[] xss = SingleResult.getCategories();
			getChart().getAxisSet().getXAxis(0).setCategorySeries(xss);
			getChart().getAxisSet().getXAxis(0).enableCategory(true);
			getChart().getAxisSet().getXAxis(0).getTick().setTickLabelAngle(45);
			getChart().getAxisSet().getXAxis(0).getTick().setForeground(Display.getDefault().getSystemColor(0));
			getChart().getAxisSet().getXAxis(0).getTitle().setForeground(Display.getDefault().getSystemColor(0));
			getChart().getAxisSet().adjustRange();
			getChart().getAxisSet().getXAxis(0).getTitle().setText("");

			getChart().getAxisSet().getYAxis(0).getTitle().setText("[" + data.getUnit() + "]");
			getChart().getAxisSet().getYAxis(0).getTick().setFormat(new DecimalFormat("#,##0.##########"));
			getChart().getAxisSet().getYAxis(0).getTick().setForeground(Display.getDefault().getSystemColor(0));
			getChart().getAxisSet().getYAxis(0).getTitle().setForeground(Display.getDefault().getSystemColor(0));
			
			getChart().setForeground(Display.getDefault().getSystemColor(0));

			getChart().getTitle().setVisible(false);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return getChart();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.zuestengineering.emod.gui.utils.AChart#getLineWidth()
	 */
	@Override
	protected int getLineWidth() {
		// TODO Auto-generated method stub
		return 0;
	}

}
