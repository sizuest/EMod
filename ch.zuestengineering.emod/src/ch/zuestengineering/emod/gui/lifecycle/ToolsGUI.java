/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.gui.lifecycle;

import java.util.ArrayList;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.TableEditor;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

import ch.zuestengineering.emod.gui.AConfigGUI;
import ch.zuestengineering.emod.gui.icons.IconHandler;
import ch.zuestengineering.emod.gui.utils.ShowButtons;
import ch.zuestengineering.emod.lca.inventory.InventoryTool;
import ch.zuestengineering.emod.utils.LocalizationHandler;

/**
 * @author sizuest
 *
 */
public class ToolsGUI extends AConfigGUI {

	private Table table;
	ArrayList<InventoryTool> tools;

	/**
	 * @param parent
	 * @param style
	 * @param tools
	 */
	public ToolsGUI(Composite parent, int style, ArrayList<InventoryTool> tools) {
		super(parent, style, ShowButtons.NONE);
		this.tools = tools;

		init();
	}

	private void init() {
		table = new Table(getContent(), SWT.BORDER);
		getContent().setLayout(new GridLayout(1, false));
		table.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		table.setHeaderVisible(true);
		table.setLinesVisible(true);

		String[] colNames = { LocalizationHandler.getItem("app.gui.model.lc.use.tools.name"),
				LocalizationHandler.getItem("app.gui.model.lc.use.tools.lifetime"),
				LocalizationHandler.getItem("app.gui.model.lc.use.tools.activetimeshare"), "", "" };
		for (String s : colNames) {
			TableColumn col = new TableColumn(table, SWT.NONE);
			col.setText(s);
			col.pack();
		}

		reset();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.zuestengineering.emod.gui.AConfigGUI#save()
	 */
	@Override
	public void save() {
		// TODO Auto-generated method stub

	}

	@Override
	public void update() {
		for (TableItem ti : table.getItems())
			ti.dispose();
		table.clearAll();

		for (final InventoryTool t : tools) {
			final TableItem item = new TableItem(table, SWT.NONE);
			item.setText(0, t.getName());
			item.setText(1, "" + t.getToolLifeTime() / 60);
			item.setText(2, t.getToolActiveShare() * 100 + "%");

			final Button buttonEdit = new Button(table, SWT.PUSH);
			final Button buttonDelete = new Button(table, SWT.PUSH);

			// create button to delete
			buttonEdit.setText("...");
			buttonEdit.addSelectionListener(new SelectionListener() {
				@Override
				public void widgetSelected(SelectionEvent event) {
					edit(t);
					update();
					wasEdited();
				}

				@Override
				public void widgetDefaultSelected(SelectionEvent event) {
					// Not used
				}
			});
			// pack the button and set it into the cell
			buttonEdit.pack();
			TableEditor editor0 = new TableEditor(table);
			editor0.minimumWidth = buttonEdit.getSize().x;
			editor0.horizontalAlignment = SWT.LEFT;
			editor0.setEditor(buttonEdit, item, 3);

			// create button to delete
			buttonDelete.setImage(IconHandler.getIcon(getDisplay(), "delete_obj"));
			buttonDelete.addSelectionListener(new SelectionListener() {
				@Override
				public void widgetSelected(SelectionEvent event) {
					tools.remove(t);
					update();
					wasEdited();
				}

				@Override
				public void widgetDefaultSelected(SelectionEvent event) {
					// Not used
				}
			});
			// pack the button and set it into the cell
			buttonDelete.pack();
			TableEditor editor1 = new TableEditor(table);
			editor1.minimumWidth = buttonDelete.getSize().x;
			editor1.horizontalAlignment = SWT.LEFT;
			editor1.setEditor(buttonDelete, item, 4);

			// if a cell gets deleted, make shure that the buttons get
			// deleted too!
			item.addDisposeListener(new DisposeListener() {
				@Override
				public void widgetDisposed(DisposeEvent e) {
					buttonDelete.dispose();
					buttonEdit.dispose();
				}
			});
		}

		final TableItem item = new TableItem(table, SWT.NONE);
		final Button buttonAdd = new Button(table, SWT.PUSH);
		// create button to delete
		buttonAdd.setImage(IconHandler.getIcon(getDisplay(), "add_obj"));
		buttonAdd.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				InventoryTool tool = new InventoryTool();
				tools.add(tool);
				edit(tool);
				update();
				wasEdited();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent event) {
				// Not used
			}
		});
		// pack the button and set it into the cell
		buttonAdd.pack();
		TableEditor editor = new TableEditor(table);
		editor.minimumWidth = buttonAdd.getSize().x;
		editor.horizontalAlignment = SWT.LEFT;
		editor.setEditor(buttonAdd, item, 4);

		// if a cell gets deleted, make shure that the buttons get
		// deleted too!
		item.addDisposeListener(new DisposeListener() {
			@Override
			public void widgetDisposed(DisposeEvent e) {
				buttonAdd.dispose();
			}
		});
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.zuestengineering.emod.gui.AConfigGUI#reset()
	 */
	@Override
	public void reset() {
		update();
	}

	private void edit(InventoryTool tool) {
		Shell shell = ToolGUI.editToolGUI(getShell(), tool);
		shell.addDisposeListener(new DisposeListener() {
			@Override
			public void widgetDisposed(DisposeEvent e) {
				update();
			}
		});

	}
}
