/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.gui.lifecycle;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;

import ch.zuestengineering.emod.gui.AConfigGUI;
import ch.zuestengineering.emod.gui.utils.ShowButtons;
import ch.zuestengineering.emod.model.APhysicalComponent;
import ch.zuestengineering.emod.model.lc.LCProperties;
import ch.zuestengineering.emod.utils.LocalizationHandler;

/**
 * @author sizuest
 *
 */
public class EditLCPropertiesGUI extends AConfigGUI {

	protected TabFolder tabFolder;
	protected APhysicalComponent component;
	protected LCProperties properties;

	protected MaterialList materialList;
	protected RessourceList ressourceList;
	protected TransportList transportList;

	/**
	 * @param parent
	 * @param style
	 * @param component
	 */
	public EditLCPropertiesGUI(Composite parent, int style, APhysicalComponent component) {
		super(parent, style, ShowButtons.NONE, false);

		this.component = component;

		init();
	}

	private void init() {

		properties = component.getLcProperties().clone();

		this.getContent().setLayout(new GridLayout(1, true));

		tabFolder = new TabFolder(getContent(), SWT.NONE);

		tabFolder.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));

		materialList = new MaterialList(tabFolder, SWT.NONE, properties.getMaterialAmount());
		ressourceList = new RessourceList(tabFolder, SWT.NONE, properties.getRessourceDemand());
		transportList = new TransportList(tabFolder, SWT.NONE, properties.getTransportation());

		TabItem tabItemMaterial = new TabItem(tabFolder, SWT.NONE);
		tabItemMaterial.setText(LocalizationHandler.getItem("app.gui.model.lcproperties.material"));
		tabItemMaterial.setControl(materialList);

		TabItem tabItemRessources = new TabItem(tabFolder, SWT.NONE);
		tabItemRessources.setText(LocalizationHandler.getItem("app.gui.model.lcproperties.ressource"));
		tabItemRessources.setControl(ressourceList);

		TabItem tabItemTransports = new TabItem(tabFolder, SWT.NONE);
		tabItemTransports.setText(LocalizationHandler.getItem("app.gui.model.lcproperties.transportation"));
		tabItemTransports.setControl(transportList);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.zuestengineering.emod.gui.AConfigGUI#save()
	 */
	@Override
	public void save() {
		materialList.save();
		ressourceList.save();
		transportList.save();

		component.setLcProperties(properties);
		component.saveLCProperties();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.zuestengineering.emod.gui.AConfigGUI#reset()
	 */
	@Override
	public void reset() {
		materialList.reset();
		ressourceList.reset();
		transportList.reset();
	}

}
