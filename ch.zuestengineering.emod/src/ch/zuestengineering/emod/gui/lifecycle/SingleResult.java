/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.gui.lifecycle;

import java.util.HashMap;

import ch.zuestengineering.emod.lca.LCPhases;
import ch.zuestengineering.emod.utils.LocalizationHandler;

/**
 * @author  sizuest
 */
public class SingleResult {
	private HashMap<LCPhases, Double> data;
	private LCPhases phase;

	/**
	 * 
	 */
	public SingleResult() {
		data = new HashMap<>();
		for(LCPhases p: LCPhases.values())
			data.put(p, 0.0);
	}
	
	/**
	 * @param phase
	 * @param value
	 */
	public void set(LCPhases phase, double value) {
		data.put(phase, value);
		this.phase = phase;
	}

	/**
	 * @return
	 */
	public double[] toArray() {
		double[] ret = new double[LCPhases.values().length];
		for(int i=0; i<LCPhases.values().length; i++)
			ret[i] = data.get(LCPhases.values()[i]);
		
		return ret;
	}

	/**
	 * @return
	 */
	public static String[] getCategories() {
		String[] ret = new String[LCPhases.values().length];
		for(int i=0; i<LCPhases.values().length; i++)
			ret[i] = getPhaseName(LCPhases.values()[i]);
		
		return ret;
	}
	
	/**
	 * @param phase
	 * @return
	 */
	public static String getPhaseName(LCPhases phase) {
		switch(phase) {
		case EOL:
			return LocalizationHandler.getItem("app.gui.lc.eol");
		case PRODUCTION:
			return LocalizationHandler.getItem("app.gui.lc.production");
		case RAWMATERIAL:
			return LocalizationHandler.getItem("app.gui.lc.rawmaterial");
		case SETUP:
			return LocalizationHandler.getItem("app.gui.lc.setup");
		case USE:
			return LocalizationHandler.getItem("app.gui.lc.use");
		default:
			return "?";
		
		}
	}

	/**
	 * @return the phase
	 */
	public LCPhases getPhase() {
		return phase;
	}
}