/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.gui.lifecycle;

import java.util.ArrayList;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CCombo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

import ch.zuestengineering.emod.gui.utils.TableUtils;
import ch.zuestengineering.emod.lca.inventory.Transportation;
import ch.zuestengineering.emod.lca.typedef.TransportationType;
import ch.zuestengineering.emod.utils.LocalizationHandler;

/**
 * @author sizuest
 *
 */
public class TransportList extends AList {

	ArrayList<Transportation> transports;

	/**
	 * @param parent
	 * @param style
	 * @param transports
	 */
	public TransportList(Composite parent, int style, ArrayList<Transportation> transports) {
		super(parent, style);

		this.transports = transports;

		init();
	}

	private void init() {
		String[] bTitles = { LocalizationHandler.getItem("app.gui.model.lcproperties.transportation.type"),
				LocalizationHandler.getItem("app.gui.model.lcproperties.transportation.distance") };
		for (int i = 0; i < bTitles.length; i++) {
			TableColumn column = new TableColumn(list, SWT.NULL);
			column.setText(bTitles[i]);

			if (i > 0)
				column.setAlignment(SWT.RIGHT);
		}

		try {
			TableUtils.addCellEditor(list, this.getClass().getDeclaredMethod("checkDistance"), this, new int[] { 1 });
		} catch (Exception e) {
			e.printStackTrace();
		}

		update();
		checkDistance();
	}

	/**
	 * Removes all items in the list with zero distance
	 */
	public void checkDistance() {

		for (TableItem ti : list.getItems()) {
			if (ti.getText(0).equals("") | ti.getText(1).equals(""))
				ti.dispose();
			else
				try {
					double amount = Double.valueOf(ti.getText(1));

					if (amount <= 0)
						ti.dispose();
				} catch (Exception e) {
					ti.setText(1, 1 + "");
				}
		}

		packTable();
	}

	public void update() {
		for (TableItem ti : list.getItems())
			ti.dispose();

		for (Control c : list.getChildren())
			if (c instanceof CCombo)
				c.dispose();

		list.setItemCount(0);

		if (null != transports) {

			for (Transportation t : transports) {
				TableItem item = new TableItem(list, SWT.NONE);
				item.setText(0, t.getType().toString());
				item.setText(1, t.getDistance() + "");
			}
		}

		packTable();
	}

	private void packTable() {
		for (Control c : list.getChildren())
			if (c instanceof CCombo)
				c.dispose();

		String[] tValues = new String[TransportationType.values().length];
		for (int i = 0; i < TransportationType.values().length; i++)
			tValues[i] = TransportationType.values()[i].toString();

		// Empty item to add new
		TableItem item = new TableItem(list, SWT.NONE);
		item.setText(0, "");

		for (TableItem ti : list.getItems())
			TableUtils.addCombo(list, ti, 0, tValues, ti.getText(0));

		TableUtils.packTable(list);

		for (Control c : list.getChildren())
			if (c instanceof CCombo) {
				list.getColumn(0).setWidth(c.getSize().x);
				break;
			}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.zuestengineering.emod.gui.lifecycle.AList#edit()
	 */
	@Override
	public void save() {
		checkDistance();

		transports.clear();
		for (TableItem ti : list.getItems()) {
			try {
				transports.add(new Transportation(TransportationType.valueOf(ti.getText(0)),
						Double.valueOf(ti.getText(1)), Double.NaN));
			} catch (Exception e) {
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.zuestengineering.emod.gui.lifecycle.AList#reset()
	 */
	@Override
	public void reset() {
		update();
	}
}
