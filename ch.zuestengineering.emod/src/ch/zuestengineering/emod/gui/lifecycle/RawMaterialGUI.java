/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.gui.lifecycle;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;

import ch.zuestengineering.emod.gui.AConfigGUI;
import ch.zuestengineering.emod.gui.utils.ShowButtons;
import ch.zuestengineering.emod.lca.inventory.AInventory;

/**
 * @author sizuest
 *
 */
public class RawMaterialGUI extends AConfigGUI {

	private MaterialList materials;

	private AInventory inventory;

	/**
	 * @param parent
	 * @param style
	 * @param inventory
	 */
	public RawMaterialGUI(Composite parent, int style, AInventory inventory) {
		super(parent, style, ShowButtons.NONE);

		this.inventory = inventory;

		init();
	}

	private void init() {

		materials = new MaterialList(getContent(), SWT.NONE, inventory.getProductMaterials());
		materials.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));

		reset();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.zuestengineering.emod.gui.AConfigGUI#save()
	 */
	@Override
	public void save() {
		materials.save();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.zuestengineering.emod.gui.AConfigGUI#reset()
	 */
	@Override
	public void reset() {
		materials.reset();

		redraw();

	}
}
