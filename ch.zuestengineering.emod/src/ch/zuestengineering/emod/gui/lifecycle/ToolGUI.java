/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.gui.lifecycle;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.ControlListener;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Text;

import ch.zuestengineering.emod.gui.AConfigGUI;
import ch.zuestengineering.emod.gui.utils.ShellUtils;
import ch.zuestengineering.emod.lca.inventory.InventoryTool;
import ch.zuestengineering.emod.utils.LocalizationHandler;

/**
 * @author sizuest
 *
 */
public class ToolGUI extends AConfigGUI {

	private InventoryTool tool;

	private Text textName;
	private Text textHoldingTime;
	private Text textActiveTime;

	private TabFolder folder;
	private RawMaterialGUI guiRawMaterial;
	private DistributionPhaseGUI guiDistribution;

	/**
	 * @param parent
	 * @param style
	 * @param tool
	 */
	public ToolGUI(Composite parent, int style, InventoryTool tool) {
		super(parent, style);

		this.tool = tool;

		init();
	}

	private void init() {

		folder = new TabFolder(getContent(), SWT.NONE);
		folder.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));

		Composite general = new Composite(folder, SWT.NONE);
		general.setLayout(new GridLayout(2, false));

		Label labelName = new Label(general, SWT.NONE);
		labelName.setText(LocalizationHandler.getItem("app.gui.model.lc.use.tools.name"));
		labelName.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, false, 1, 1));

		textName = new Text(general, SWT.BORDER);
		textName.setLayoutData(new GridData(SWT.FILL, SWT.TOP, false, false, 1, 1));

		Label labelHoldingTime = new Label(general, SWT.NONE);
		labelHoldingTime.setText(LocalizationHandler.getItem("app.gui.model.lc.use.tools.lifetime"));
		labelHoldingTime.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, false, 1, 1));

		textHoldingTime = new Text(general, SWT.BORDER);
		textHoldingTime.setLayoutData(new GridData(SWT.FILL, SWT.TOP, false, false, 1, 1));

		Label labelActiveShare = new Label(general, SWT.NONE);
		labelActiveShare.setText(LocalizationHandler.getItem("app.gui.model.lc.use.tools.activetimeshare"));
		labelActiveShare.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, false, 1, 1));

		textActiveTime = new Text(general, SWT.BORDER);
		textActiveTime.setLayoutData(new GridData(SWT.FILL, SWT.TOP, false, false, 1, 1));

		guiRawMaterial = new RawMaterialGUI(folder, SWT.NONE, tool);
		guiDistribution = new DistributionPhaseGUI(folder, SWT.NONE, tool);

		TabItem itemGeneral = new TabItem(folder, SWT.NONE);
		itemGeneral.setText(LocalizationHandler.getItem("app.gui.model.lc.general"));
		itemGeneral.setControl(general);

		TabItem itemMaterials = new TabItem(folder, SWT.NONE);
		itemMaterials.setText(LocalizationHandler.getItem("app.gui.model.lc.rawmaterial"));
		itemMaterials.setControl(guiRawMaterial);

		TabItem itemDistribution = new TabItem(folder, SWT.NONE);
		itemDistribution.setText(LocalizationHandler.getItem("app.gui.model.lc.distribution"));
		itemDistribution.setControl(guiDistribution);

		reset();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.zuestengineering.emod.gui.AConfigGUI#save()
	 */
	@Override
	public void save() {

		tool.setName(textName.getText());
		tool.setToolLifeTime(Double.valueOf(textHoldingTime.getText()) * 60);
		tool.setToolActiveShare(Double.valueOf(textActiveTime.getText().replaceAll("%", "")) / 100);

		guiRawMaterial.save();
		guiDistribution.save();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.zuestengineering.emod.gui.AConfigGUI#reset()
	 */
	@Override
	public void reset() {

		textName.setText(tool.getName());
		textHoldingTime.setText(tool.getToolLifeTime() / 60 + "");
		textActiveTime.setText(tool.getToolActiveShare() * 100 + "%");

		guiRawMaterial.reset();
		guiDistribution.reset();
	}

	/**
	 * @param parent
	 * @param tool
	 * @return
	 */
	public static Shell editToolGUI(final Shell parent, InventoryTool tool) {

		final Shell shell = new Shell(parent, SWT.TITLE | SWT.APPLICATION_MODAL | SWT.CLOSE | SWT.MAX | SWT.RESIZE);
		shell.setText("Edit Tool");
		shell.setLayout(new GridLayout(1, true));
		shell.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		final ToolGUI gui = new ToolGUI(shell, SWT.NONE, tool);

		shell.setImages(parent.getImages());

		shell.pack();

		shell.layout();
		shell.redraw();
		shell.open();

		shell.setSize(shell.computeSize(SWT.DEFAULT, SWT.DEFAULT));

		shell.addControlListener(new ControlListener() {

			@Override
			public void controlResized(ControlEvent e) {
				gui.layout();
			}

			@Override
			public void controlMoved(ControlEvent e) {
				gui.layout();
			}
		});

		shell.addDisposeListener(new DisposeListener() {

			@Override
			public void widgetDisposed(DisposeEvent e) {
				parent.setEnabled(true);
			}
		});

		gui.addDisposeListener(new DisposeListener() {
			@Override
			public void widgetDisposed(DisposeEvent e) {
				shell.dispose();
			}
		});

		ShellUtils.putToCenter(shell, parent);

		return shell;
	}

}
