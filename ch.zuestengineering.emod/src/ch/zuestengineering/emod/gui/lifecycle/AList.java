/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.gui.lifecycle;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Table;

import ch.zuestengineering.emod.gui.utils.TableUtils;

/**
 * @author sizuest
 *
 */
public abstract class AList extends Composite {

	protected Label titleLable;
	protected Table list;
	protected String title;
	protected boolean isEditable;

	/**
	 * @param parent
	 * @param style
	 */
	public AList(Composite parent, int style) {
		super(parent, style);
		this.isEditable = true;

		init();
	}

	/**
	 * @param parent
	 * @param style
	 * @param isEditable
	 */
	public AList(Composite parent, int style, boolean isEditable) {
		super(parent, style);
		this.isEditable = isEditable;

		init();
	}

	private void init() {

		this.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false));
		this.setLayout(new GridLayout(2, false));

		if (null != title) {
			titleLable = new Label(this, SWT.NONE);
			titleLable.setText(title);
			titleLable.setLayoutData(new GridData(SWT.FILL, SWT.BOTTOM, false, false, 1, 1));
		}

		list = new Table(this, SWT.BORDER | SWT.MULTI | SWT.V_SCROLL | SWT.H_SCROLL);
		list.setLinesVisible(true);
		list.setHeaderVisible(true);
		list.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 2, 1));
		list.setEnabled(isEditable);

		try {
			TableUtils.addCopyToClipboard(list);
			TableUtils.addPastFromClipboard(list, true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 */
	public abstract void save();

	/**
	 * 
	 */
	public abstract void reset();

	@Override
	public abstract void update();

}
