/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.gui.lifecycle;

import java.util.ArrayList;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CCombo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

import ch.zuestengineering.emod.gui.utils.TableUtils;
import ch.zuestengineering.emod.lca.inventory.MaterialAmount;
import ch.zuestengineering.emod.lca.typedef.EOLType;
import ch.zuestengineering.emod.model.material.Material;
import ch.zuestengineering.emod.utils.LocalizationHandler;

/**
 * @author sizuest
 *
 */
public class MaterialList extends AList {

	private ArrayList<MaterialAmount> materials;

	/**
	 * @param parent
	 * @param style
	 * @param materials
	 */
	public MaterialList(Composite parent, int style, ArrayList<MaterialAmount> materials) {
		super(parent, style);

		this.materials = materials;

		init();
	}

	private void init() {
		String[] bTitles = { LocalizationHandler.getItem("app.gui.model.lcproperties.material.type")+"     ",
				LocalizationHandler.getItem("app.gui.model.lcproperties.material.amount"),
				LocalizationHandler.getItem("app.gui.model.lcproperties.material.eol") };
		for (int i = 0; i < bTitles.length; i++) {
			TableColumn column = new TableColumn(list, SWT.NULL);
			column.setText(bTitles[i]);

			if (i == 1)
				column.setAlignment(SWT.RIGHT);

		}

		try {
			TableUtils.addCellEditor(list, this.getClass().getDeclaredMethod("checkAmounts"), this, new int[] { 1 },
					new int[] { 0 });
		} catch (Exception e) {}

		update();
		checkAmounts();
	}

	/**
	 * Removes all items in the list with zero amount
	 */
	public void checkAmounts() {

		for (TableItem ti : list.getItems()) {
			if (ti.getText(0).equals("") | ti.getText(1).equals(""))
				ti.dispose();
			else
				try {
					double amount = Double.valueOf(ti.getText(1));

					if (amount <= 0)
						ti.dispose();
				} catch (Exception e) {
					ti.setText(1, 1 + "");
				}
		}

		packTable();

	}

	public void update() {
		for (TableItem ti : list.getItems())
			ti.dispose();

		list.setItemCount(0);

		if (null == materials) {
			TableItem item = new TableItem(list, SWT.NONE);
			item.setText(0, "none");
			return;
		}
		if (materials.size() == 0) {
			TableItem item = new TableItem(list, SWT.NONE);
			item.setText(0, "none");
			return;
		}
		for (MaterialAmount m : materials) {
			TableItem item = new TableItem(list, SWT.NONE);
			item.setText(0, m.getMaterial().toString());
			item.setText(1, m.getAmount() + "");
			item.setText(2, m.getEol().toString());
		}

		packTable();
	}

	private void packTable() {
		for (Control c : list.getChildren())
			if (c instanceof CCombo)
				c.dispose();

		String[] eolValues = new String[EOLType.values().length];
		for (int i = 0; i < EOLType.values().length; i++)
			eolValues[i] = EOLType.values()[i].toString();

		for (TableItem ti : list.getItems())
			TableUtils.addCombo(list, ti, 2, eolValues, ti.getText(2));

		TableUtils.packTable(list);

		for (Control c : list.getChildren())
			if (c instanceof CCombo) {
				list.getColumn(2).setWidth(c.getSize().x);
				break;
			}

		// Add new item at the bottom
		TableItem item = new TableItem(list, SWT.NONE);
		item.setText(0, "");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.zuestengineering.emod.gui.lifecycle.AList#edit()
	 */
	@Override
	public void save() {
		checkAmounts();

		materials.clear();
		for (TableItem ti : list.getItems()) {
			try {
				materials.add(new MaterialAmount(new Material(ti.getText(0)), Double.valueOf(ti.getText(1)),
						EOLType.valueOf(ti.getText(2))));
			} catch (Exception e) {
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.zuestengineering.emod.gui.lifecycle.AList#reset()
	 */
	@Override
	public void reset() {
		update();
	}
}
