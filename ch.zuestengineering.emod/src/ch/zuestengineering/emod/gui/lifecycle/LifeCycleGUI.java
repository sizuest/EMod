/*******************************************************************************
 * Copyright (C) Zst Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.gui.lifecycle;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;

import ch.zuestengineering.emod.EModSession;
import ch.zuestengineering.emod.LifeCycle;
import ch.zuestengineering.emod.gui.AConfigGUI;
import ch.zuestengineering.emod.gui.AGUITab;
import ch.zuestengineering.emod.utils.LocalizationHandler;

/**
 * @author sizuest
 *
 */
public class LifeCycleGUI extends AGUITab {

	private TabFolder folder;
	private AConfigGUI generalGUI, distGUI, toolsGUI;

	/**
	 * @param parent
	 * @param i
	 */
	public LifeCycleGUI(CTabFolder parent, int i) {
		super(parent, i, LocalizationHandler.getItem("app.gui.sim.lc.title"));
		init();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.zuestengineering.emod.gui.AGUITab#init()
	 */
	@Override
	public void init() {

		LifeCycle.load(EModSession.getSimulationConfig());

		this.setLayout(new GridLayout(1, true));
		LifeCycle.getInstance();

		folder = new TabFolder(this, SWT.NONE);
		folder.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));

		generalGUI = new LifeCycleScenarioGUI(folder, SWT.NONE, LifeCycle.getScenario());
		generalGUI.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));

		distGUI = new DistributionPhaseGUI(folder, SWT.NONE, LifeCycle.getMachine());
		distGUI.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));

		toolsGUI = new ToolsGUI(folder, SWT.NONE, LifeCycle.getTools());
		toolsGUI.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));

		TabItem tiGeneral = new TabItem(folder, SWT.NONE);
		tiGeneral.setText(LocalizationHandler.getItem("app.gui.model.lc.general"));
		tiGeneral.setControl(generalGUI);

		TabItem tiDist = new TabItem(folder, SWT.NONE);
		tiDist.setText(LocalizationHandler.getItem("app.gui.model.lc.distribution"));
		tiDist.setControl(distGUI);

		TabItem tiTools = new TabItem(folder, SWT.NONE);
		tiTools.setText(LocalizationHandler.getItem("app.gui.model.lc.use.tools"));
		tiTools.setControl(toolsGUI);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.zuestengineering.emod.gui.AGUITab#update()
	 */
	@Override
	public void update() {
		LifeCycle.getScenario().getStates().fetch();

		generalGUI.update();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.zuestengineering.emod.gui.AGUITab#save()
	 */
	@Override
	public void save() {
		generalGUI.save();
		distGUI.save();
		toolsGUI.save();
		LifeCycle.save(EModSession.getSimulationConfig());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.zuestengineering.emod.gui.AGUITab#wasEdited()
	 */
	@Override
	public void wasEdited() {
		// TODO Auto-generated method stub

	}

}

class LifePhaseSelectorGUI extends Composite {

	LifeCycleGUI parent;
	Button bGeneral, bRawMaterial, bProduction, bDist, bUse, bEol;
	Color cSelected = new Color(getDisplay(), 255, 255, 0), cNormal;

	/**
	 * @param parent
	 * @param style
	 */
	public LifePhaseSelectorGUI(LifeCycleGUI parent, int style) {
		super(parent, style);

		this.parent = parent;

		this.setLayout(new GridLayout(6, false));
		this.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));

		bGeneral = new Button(this, SWT.PUSH);
		bGeneral.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, false, 1, 1));
		bGeneral.setText(LocalizationHandler.getItem("app.gui.model.lc.general"));

		bRawMaterial = new Button(this, SWT.PUSH);
		bRawMaterial.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));
		bRawMaterial.setText(LocalizationHandler.getItem("app.gui.model.lc.rawmaterial"));
		// bRawMaterial.setBackgroundImage(new Image(getDisplay(),
		// "src/resources/lc/lc_bg_selected.png"));

		bProduction = new Button(this, SWT.PUSH);
		bProduction.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));
		bProduction.setText(LocalizationHandler.getItem("app.gui.model.lc.production"));

		bDist = new Button(this, SWT.PUSH);
		bDist.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));
		bDist.setText(LocalizationHandler.getItem("app.gui.model.lc.distribution"));

		bUse = new Button(this, SWT.PUSH);
		bUse.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));
		bUse.setText(LocalizationHandler.getItem("app.gui.model.lc.use"));

		bEol = new Button(this, SWT.PUSH);
		bEol.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));
		bEol.setText(LocalizationHandler.getItem("app.gui.model.lc.eol"));

	}

}
