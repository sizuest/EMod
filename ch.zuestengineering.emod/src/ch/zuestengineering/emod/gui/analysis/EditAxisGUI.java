/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.gui.analysis;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.ControlListener;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.ColorDialog;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.swtchart.Chart;
import org.swtchart.IAxis;
import org.swtchart.Range;

import ch.zuestengineering.emod.gui.AConfigGUI;
import ch.zuestengineering.emod.gui.utils.ShellUtils;
import ch.zuestengineering.emod.gui.utils.ShowButtons;

/**
 * Implements a simple GUI to edit a chart axes properties
 * 
 * @author Simon Z�st
 *
 */
public class EditAxisGUI extends AConfigGUI {

	private Chart chart;
	private IAxis axis;

	private Text textUpper, textLower;
	private Button buttonColor;
	private ColorDialog colorSelector;

	/**
	 * @param parent
	 * @param style
	 * @param chart
	 * @param axis
	 */
	public EditAxisGUI(Composite parent, int style, Chart chart, IAxis axis) {
		super(parent, style, ShowButtons.ALL);

		this.chart = chart;
		this.axis = axis;

		this.getContent().setLayout(new GridLayout(2, false));

		Label lableUpper = new Label(getContent(), SWT.NONE);
		lableUpper.setText("Upper limit:");

		textUpper = new Text(getContent(), SWT.BORDER);
		textUpper.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false));

		Label lableLower = new Label(getContent(), SWT.NONE);
		lableLower.setText("Lower limit:");

		textLower = new Text(getContent(), SWT.BORDER);
		textLower.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false));

		Label lableColor = new Label(getContent(), SWT.NONE);
		lableColor.setText("Color:");

		buttonColor = new Button(getContent(), SWT.PUSH);
		buttonColor.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, true, false));
		buttonColor.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				colorSelector.open();
				setColor();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		colorSelector = new ColorDialog(getShell(), SWT.BORDER);

		reset();
	}

	private Color getColor() {
		return new Color(getDisplay(), colorSelector.getRGB());
	}

	private void setColor() {
		Image imageColor = new Image(getDisplay(), 16, 16);
		GC gc = new GC(imageColor);
		gc.setBackground(getColor());
		gc.fillRectangle(imageColor.getBounds());
		gc.dispose();

		buttonColor.setImage(imageColor);
	}

	/**
	 * Opens a new shell with the config GUI
	 * 
	 * @param parent
	 * @param point
	 * @param chart
	 * @param axis
	 * @return
	 */
	public static Shell editAxisGUI(final Shell parent, final Point point, final Chart chart, final IAxis axis) {
		final Shell shell = new Shell(parent, SWT.APPLICATION_MODAL);
		shell.setText("Edit Axis settings");
		shell.setLayout(new GridLayout(1, true));
		shell.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		final EditAxisGUI gui = new EditAxisGUI(shell, SWT.NONE, chart, axis);

		shell.setImages(parent.getImages());

		shell.pack();

		shell.layout();
		shell.redraw();
		shell.open();

		shell.setLocation(point);

		shell.setImages(parent.getShell().getImages());

		shell.addControlListener(new ControlListener() {

			@Override
			public void controlResized(ControlEvent e) {
				gui.layout();
			}

			@Override
			public void controlMoved(ControlEvent e) {
				gui.layout();
			}
		});

		shell.addDisposeListener(new DisposeListener() {

			@Override
			public void widgetDisposed(DisposeEvent e) {
				parent.setEnabled(true);
			}
		});

		gui.addDisposeListener(new DisposeListener() {
			@Override
			public void widgetDisposed(DisposeEvent e) {
				try {
					shell.dispose();
				} catch (Exception e2) {
					logger.severe("EditAxisRangeGUI: Failed to dispose shell");
				}
			}
		});

		ShellUtils.putToCenter(shell, parent);

		return shell;
	}

	@Override
	public void save() {
		double upper, lower;

		upper = Double.parseDouble(textUpper.getText());
		lower = Double.parseDouble(textLower.getText());

		if (!Double.isFinite(upper))
			upper = axis.getRange().upper;

		if (!Double.isFinite(lower))
			lower = axis.getRange().lower;

		if (lower < upper)
			axis.setRange(new Range(lower, upper));

		axis.getTick().setForeground(getColor());
		axis.getTitle().setForeground(getColor());

		chart.redraw();

	}

	@Override
	public void reset() {
		colorSelector.setRGB(axis.getTick().getForeground().getRGB());
		textUpper.setText(axis.getRange().upper + "");
		textLower.setText(axis.getRange().lower + "");

		setColor();
	}

}
