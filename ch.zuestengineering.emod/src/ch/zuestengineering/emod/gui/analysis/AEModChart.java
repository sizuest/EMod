/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.gui.analysis;

import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.swtchart.Chart;

import ch.zuestengineering.emod.gui.utils.AChart;
import ch.zuestengineering.emod.gui.utils.ConsumerData;

/**
 * Implementation of the generic chart functions for an EMod analyses
 * 
 * @author simon
 *
 */
public abstract class AEModChart extends AChart {

	/**
	 * @param parent
	 * @param data
	 */
	public AEModChart(Composite parent, List<ConsumerData> data) {
		super(parent, SWT.NONE);

		fillChart(data);

	}

	/**
	 * Redraws the chart with the provided data
	 * 
	 * @param data
	 */
	public void redrawChart(List<ConsumerData> data) {
		fillChart(data);
		getChart().setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		adjustLineWidth();
		getChart().redraw();
	}

	protected abstract Chart fillChart(List<ConsumerData> data);

	@Override
	protected int getLineWidth() {
		double range = getChart().getAxisSet().getXAxis(0).getRange().upper
				- getChart().getAxisSet().getXAxis(0).getRange().lower;
		double sampleTime = 1;

		if (getChart().getSeriesSet().getSeries().length > 0) {
			sampleTime = getChart().getSeriesSet().getSeries()[0].getXSeries()[1]
					- getChart().getSeriesSet().getSeries()[0].getXSeries()[0];
		}

		return 1 + (int) (sampleTime * 300 / range);
	}

}
