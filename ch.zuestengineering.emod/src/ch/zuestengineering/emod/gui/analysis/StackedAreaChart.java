/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.gui.analysis;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.swtchart.Chart;
import org.swtchart.ILineSeries;
import org.swtchart.ILineSeries.PlotSymbolType;
import org.swtchart.ISeries.SeriesType;

import ch.zuestengineering.emod.gui.utils.ConsumerData;
import ch.zuestengineering.emod.model.units.SiUnit;

/**
 * @author dhampl
 * 
 */
public class StackedAreaChart extends AEModChart {

	private List<ConsumerData> localdata;

	/**
	 * @param parent
	 * @param data
	 */
	public StackedAreaChart(Composite parent, List<ConsumerData> data) {
		super(parent, data);

		localdata = data;
	}

	/**
	 * @param data
	 * @return
	 */
	@Override
	public Chart fillChart(List<ConsumerData> data) {
		if (getChart() != null)
			clearChart();

		int color = 3;
		if (0 == data.size())
			return getChart();

		localdata = new ArrayList<ConsumerData>(data);

		for (int i = localdata.size() - 1; i >= 0; i--) {
			if (Double.isNaN(localdata.get(i).getVariancePower()))
				localdata.remove(i);
		}

		sort();

		List<double[]> series = createStackedSeries();

		for (int i = localdata.size() - 1; i >= 0; i--) {
			ILineSeries lineSeries = (ILineSeries) getChart().getSeriesSet().createSeries(SeriesType.LINE,
					localdata.get(i).getConsumer());

			lineSeries.setYSeries(series.get(i));
			lineSeries.setSymbolType(PlotSymbolType.NONE);

			lineSeries.setLineColor(Display.getDefault().getSystemColor(color));
			lineSeries.enableArea(true);
			lineSeries.enableStep(true);
			color++;
		}

		getChart().getAxisSet().getXAxis(0).getTitle().setText("time [s]");
		getChart().getAxisSet().getXAxis(0).getTick().setForeground(Display.getDefault().getSystemColor(0));
		getChart().getAxisSet().getXAxis(0).getTitle().setForeground(Display.getDefault().getSystemColor(0));

		getChart().getAxisSet().getYAxis(0).getTitle().setText("[" + (new SiUnit("W")).toString() + "]");
		getChart().getAxisSet().getYAxis(0).getTick().setFormat(new DecimalFormat("#,##0.##########"));
		getChart().getAxisSet().getYAxis(0).getTick().setForeground(Display.getDefault().getSystemColor(0));
		getChart().getAxisSet().getYAxis(0).getTitle().setForeground(Display.getDefault().getSystemColor(0));

		getChart().getTitle().setVisible(false);

		getChart().getAxisSet().adjustRange();

		return getChart();
	}

	private void sort() {
		for (int i = 0; i < localdata.size(); i++) {
			for (int j = i; j < localdata.size(); j++) {
				if (localdata.get(i).getVariancePower()
						/ localdata.get(i).getAveragePower() > localdata.get(j).getVariancePower()
								/ localdata.get(j).getAveragePower()) {
					Collections.swap(localdata, i, j);
				}
			}
		}

	}

	private List<double[]> createStackedSeries() {
		List<double[]> result = new ArrayList<double[]>();
		if (localdata.size() < 1)
			result.add(new double[1]);
		else
			result.add(localdata.get(0).getPTotal());
		for (int i = 1; i < localdata.size(); i++) {
			double[] temp = new double[localdata.get(i).getPTotal().length];
			for (int j = 0; j < temp.length; j++) {
				if (Double.isFinite(localdata.get(i).getPTotal()[j]))
					temp[j] = localdata.get(i).getPTotal()[j] + result.get(i - 1)[j];
			}
			result.add(temp);
		}

		return result;
	}
}
