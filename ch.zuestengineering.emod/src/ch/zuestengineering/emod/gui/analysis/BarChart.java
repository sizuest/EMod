/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.gui.analysis;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseMoveListener;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.swtchart.Chart;
import org.swtchart.IBarSeries;
import org.swtchart.ISeries;
import org.swtchart.ISeries.SeriesType;

import ch.zuestengineering.emod.gui.utils.ConsumerData;
import ch.zuestengineering.emod.model.units.SiUnit;
import ch.zuestengineering.emod.model.units.Unit;
import ch.zuestengineering.emod.utils.LocalizationHandler;

/**
 * creates bar chart. the chart is a singleton implementation
 * 
 * @author dhampl
 * 
 */
public class BarChart extends AEModChart {

	/**
	 * @param parent
	 * @param data
	 */
	public BarChart(Composite parent, List<ConsumerData> data) {
		super(parent, data);
	}

	/**
	 * creates a bar chart from {@link ConsumerData} objects. only energy values are
	 * displayed
	 * 
	 * @param data
	 * @return
	 */
	@Override
	protected Chart fillChart(List<ConsumerData> data) {
		if (getChart() != null)
			clearChart();

		if (0 == data.size())
			return getChart();

		ArrayList<String> consumers = new ArrayList<String>();

		for (ConsumerData cd : data) {
			for (int i = 0; i < cd.getActive().size(); i++) {
				if (cd.getUnits().get(i).equals(new SiUnit(Unit.WATT))) {
					consumers.add(cd.getConsumer());
					break;
				}
			}
		}

		double[] ptotal = new double[consumers.size()];
		double[] ploss = new double[consumers.size()];
		double[] puse = new double[consumers.size()];
		// double[] xs = new ArrayList<String>();

		int idx = 0;
		for (ConsumerData cd : data) {
			for (int i = 0; i < cd.getActive().size(); i++) {
				if (cd.getUnits().get(i).equals(new SiUnit(Unit.WATT))) {
					idx = consumers.indexOf(cd.getConsumer());
					switch (cd.getNames().get(i)) {
					case "PTotal":
						ptotal[idx] = cd.getEnergy().get(i) / 3600000;
						break;
					case "PUse":
						puse[idx] = cd.getEnergy().get(i) / 3600000;
						break;
					case "PLoss":
						ploss[idx] = cd.getEnergy().get(i) / 3600000;
						break;
					}

				}
			}
		}

		IBarSeries seriesTotal = (IBarSeries) getChart().getSeriesSet().createSeries(SeriesType.BAR, "Input energy");
		IBarSeries seriesUse = (IBarSeries) getChart().getSeriesSet().createSeries(SeriesType.BAR, "Usefull energy");
		IBarSeries seriesLoss = (IBarSeries) getChart().getSeriesSet().createSeries(SeriesType.BAR, "Losses");

		seriesTotal.setYSeries(ptotal);
		seriesUse.setYSeries(puse);
		seriesLoss.setYSeries(ploss);

		seriesTotal.setBarColor(Display.getCurrent().getSystemColor(SWT.COLOR_BLUE));
		seriesUse.setBarColor(Display.getCurrent().getSystemColor(SWT.COLOR_GREEN));
		seriesLoss.setBarColor(Display.getCurrent().getSystemColor(SWT.COLOR_RED));

		String[] xss = new String[consumers.size()];
		consumers.toArray(xss);
		getChart().getAxisSet().getXAxis(0).setCategorySeries(xss);
		getChart().getAxisSet().getXAxis(0).enableCategory(true);
		getChart().getAxisSet().getXAxis(0).getTick().setTickLabelAngle(45);
		getChart().getAxisSet().getXAxis(0).getTick().setForeground(Display.getDefault().getSystemColor(0));
		getChart().getAxisSet().getXAxis(0).getTitle().setForeground(Display.getDefault().getSystemColor(0));
		getChart().getAxisSet().adjustRange();
		getChart().getTitle().setText(LocalizationHandler.getItem("app.gui.analysis.bargetChart().title"));
		getChart().getAxisSet().getXAxis(0).getTitle().setText("");

		getChart().getAxisSet().getYAxis(0).getTitle().setText("[" + (new SiUnit("kWh")).toString() + "]");
		getChart().getAxisSet().getYAxis(0).getTick().setFormat(new DecimalFormat("#,##0.##########"));
		getChart().getAxisSet().getYAxis(0).getTick().setForeground(Display.getDefault().getSystemColor(0));
		getChart().getAxisSet().getYAxis(0).getTitle().setForeground(Display.getDefault().getSystemColor(0));

		getChart().getTitle().setVisible(false);

		/*
		getChart().getPlotArea().addMouseMoveListener(new MouseMoveListener() {
			@Override
			public void mouseMove(MouseEvent e) {
				for (ISeries series : getChart().getSeriesSet().getSeries()) {
					Rectangle[] rs = ((IBarSeries) series).getBounds();
					for (int i = 0; i < rs.length; i++) {
						if (rs[i] != null) {
							if (rs[i].x < e.x && e.x < rs[i].x + rs[i].width && rs[i].y < e.y
									&& e.y < rs[i].y + rs[i].height) {
								setToolTipText(series, i);
								return;
							}
						}
					}
				}
				getChart().getPlotArea().setToolTipText(null);
			}

			private void setToolTipText(ISeries series, int index) {
				getChart().getPlotArea().setToolTipText(series.getYSeries()[index] + " kWh");
			}
		});
		*/

		return getChart();
	}

}
