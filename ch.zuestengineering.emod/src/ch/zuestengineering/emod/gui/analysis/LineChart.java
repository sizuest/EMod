/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.gui.analysis;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.swtchart.Chart;
import org.swtchart.ILineSeries;
import org.swtchart.ILineSeries.PlotSymbolType;
import org.swtchart.ISeries.SeriesType;

import ch.zuestengineering.emod.gui.utils.ConsumerData;

import org.swtchart.LineStyle;

/**
 * creates a chart from active ConsumerData objects. currently only one chart
 * can be drawn.
 * 
 * @author dhampl
 * 
 */
public class LineChart extends AEModChart {

	ArrayList<Integer> unitCounter;
	ArrayList<Integer> colorCounter;
	ArrayList<ILineSeries> lines;
	Map<String, Integer> axisMap;
	List<ConsumerData> data;

	/**
	 * @param parent
	 * @param data
	 */
	public LineChart(Composite parent, List<ConsumerData> data) {
		super(parent, data);

		getChart().addPaintListener(new PaintListener() {

			@Override
			public void paintControl(PaintEvent e) {
				updateLineColors();
			}
		});
	}

	/**
	 * @param parent
	 * @param data
	 * @return
	 */
	@Override
	protected Chart fillChart(List<ConsumerData> data) {

		this.data = data;

		axisMap = new HashMap<String, Integer>();
		unitCounter = new ArrayList<Integer>();
		colorCounter = new ArrayList<Integer>();
		lines = new ArrayList<ILineSeries>();

		if (getChart() != null)
			clearChart();

		getChart().getTitle().setText("");

		int color = 3;
		for (ConsumerData cd : data) {
			for (int i = 0; i < cd.getNames().size(); i++) {
				if (cd.getActive().get(i)) {
					int axId;
					// Check for axis
					if (axisMap.containsKey(cd.getUnits().get(i).toString())) {
						axId = axisMap.get(cd.getUnits().get(i).toString());
						unitCounter.set(axId, unitCounter.get(axId) + 1);
					} else {
						if (getChart().getAxisSet().getYAxes().length == 1 & axisMap.size() == 0)
							axId = 0;
						else
							axId = getChart().getAxisSet().createYAxis();

						axisMap.put(cd.getUnits().get(i).toString(), axId);
						unitCounter.add(axId, 1);

						getChart().getAxisSet().getYAxis(axId).getTitle().setText("[" + cd.getUnits().get(i).toString() + "]");
						getChart().getAxisSet().getYAxis(axId).getTick().setFormat(new DecimalFormat("#,##0.##########"));
						getChart().getAxisSet().getYAxis(axId).getTick().setForeground(Display.getDefault().getSystemColor(color));
						getChart().getAxisSet().getYAxis(axId).getTitle().setForeground(Display.getDefault().getSystemColor(color));

						color++;
					}

					ILineSeries lineSeries = (ILineSeries) getChart().getSeriesSet().createSeries(SeriesType.LINE,
							cd.getConsumer() + "." + cd.getNames().get(i));
					lineSeries.setXSeries(cd.getTime());
					lineSeries.setYSeries(cd.getValues().get(i));
					lineSeries.setSymbolType(PlotSymbolType.NONE);
					lineSeries.setYAxisId(axId);
					lineSeries.setAntialias(SWT.ON);

					lines.add(lineSeries);

				}
			}
		}

		/* Update line colors */
		updateLineColors();

		/* Format time axis */
		getChart().getAxisSet().getXAxis(0).getTitle().setText("time [s]");
		getChart().getAxisSet().getXAxis(0).getTick().setForeground(Display.getDefault().getSystemColor(0));
		getChart().getAxisSet().getXAxis(0).getTitle().setForeground(Display.getDefault().getSystemColor(0));

		/* Adjust range */
		try {
			getChart().getAxisSet().adjustRange();
		} catch (Exception e) {
			logger.warning("Could not adjust Range");
		}

		getChart().getLegend().setPosition(SWT.BOTTOM);

		return getChart();
	}

	private void updateLineColors() {
		for (int j = 0; j < data.size(); j++) {
			if (unitCounter.size() > 1) {

				/* Adjust colors */
				for (int i = 0; i < unitCounter.size(); i++)
					colorCounter.add(i, 0);

				for (ILineSeries l : lines) {
					int axId = l.getYAxisId(), r, g, b;
					Color lineColor = getChart().getAxisSet().getYAxis(axId).getTick().getForeground();

					// First: Vary the line style
					switch (colorCounter.get(axId) % 5) {
					case 0:
						l.setLineStyle(LineStyle.SOLID);
						break;
					case 1:
						l.setLineStyle(LineStyle.DOT);
						break;
					case 2:
						l.setLineStyle(LineStyle.DASHDOT);
						break;
					case 3:
						l.setLineStyle(LineStyle.DASH);
						break;
					case 4:
						l.setLineStyle(LineStyle.DASHDOTDOT);
						break;
					}

					// Second: vary the color
					r = (int) (lineColor.getRed() * (1 - .75 / unitCounter.get(axId) * (colorCounter.get(axId))));
					g = (int) (lineColor.getGreen() * (1 - .75 / unitCounter.get(axId) * (colorCounter.get(axId))));
					b = (int) (lineColor.getBlue() * (1 - .75 / unitCounter.get(axId) * (colorCounter.get(axId))));

					colorCounter.set(axId, colorCounter.get(axId) + 1);

					l.setLineColor(new Color(Display.getCurrent(), r, g, b));

				}
			} else {
				int color = 3;
				for (ILineSeries l : lines) {
					l.setLineColor(Display.getDefault().getSystemColor(color));
					color++;
				}
				getChart().getAxisSet().getYAxis(0).getTick().setFormat(new DecimalFormat("#,##0.##########"));
				getChart().getAxisSet().getYAxis(0).getTick().setForeground(Display.getDefault().getSystemColor(0));
				getChart().getAxisSet().getYAxis(0).getTitle().setForeground(Display.getDefault().getSystemColor(0));
			}
		}
	}
}
