/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.gui.scenario;

import java.io.IOException;
import java.util.ArrayList;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

import ch.zuestengineering.emod.EModSession;
import ch.zuestengineering.emod.Machine;
import ch.zuestengineering.emod.Process;
import ch.zuestengineering.emod.gui.AGUITab;
import ch.zuestengineering.emod.gui.ContentHandlerGUI;
import ch.zuestengineering.emod.gui.EModStatusBarGUI;
import ch.zuestengineering.emod.gui.utils.TableUtils;
import ch.zuestengineering.emod.model.units.SiUnit;
import ch.zuestengineering.emod.simulation.ASimulationControl;
import ch.zuestengineering.emod.utils.LocalizationHandler;

/**
 * GUI for process editing
 * 
 * @author sizuest
 * 
 */

public class ProcessGUI extends AGUITab {

	// private ProcessManageGUI processManageGUI;
	private Table tableProcessParam;
	ArrayList<String> scNames;
	ArrayList<SiUnit> scUnits;
	private boolean updatethreadRunning = false;

	/**
	 * ProcessGUI
	 * 
	 * @param parent
	 * @param style
	 */
	public ProcessGUI(Composite parent, int style) {
		super(parent, style);

		init();
	}

	/**
	 * @param parent
	 * @param style
	 */
	public ProcessGUI(CTabFolder parent, int style) {
		super(parent, style, LocalizationHandler.getItem("app.gui.sim.inputs.title"));

		init();
	}

	@Override
	public void update() {
		tableProcessParam.setEnabled(false);
		tableProcessParam.clearAll();
		tableProcessParam.setItemCount(0);
		// processManageGUI.update();

		threadedUpdate();
	}

	@Override
	public void save() {
		// write back data

		// Remove all entries with non numeric time
		for (int i = tableProcessParam.getItemCount() - 1; i >= 0; i--)
			try {
				Double.valueOf(tableProcessParam.getItem(i).getText(0));
			} catch (Exception e) {
				tableProcessParam.remove(i);
			}

		// Sort time vector
		// TODO

		// write back time
		double[] time = new double[tableProcessParam.getItemCount()];
		for (int j = 0; j < time.length; j++) {
			time[j] = Double.valueOf(tableProcessParam.getItem(j).getText(0));
		}

		try {
			Process.setTimeVector(time);
		} catch (Exception e) {
			e.printStackTrace();
		}

		// Write back values
		double[] data = new double[tableProcessParam.getItemCount()];
		for (int i = 0; i < scNames.size(); i++) {
			for (int j = 0; j < data.length; j++) {
				try {
					data[j] = Double.valueOf(tableProcessParam.getItem(j).getText(i + 1));
				} catch (Exception e) {
					data[j] = 0;
				}
			}

			try {
				Process.setProcessVariable(scNames.get(i), data);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		try {
			Process.getInstance().saveValues();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	/**
	 * 
	 */
	@Override
	public void wasEdited() {
		save();
		addEmptyLine();
		ContentHandlerGUI.updateSessionTree();
	}

	/**
	 * Checks if the last line is empty, if not, an empty line is added
	 */
	private void addEmptyLine() {

		if (0 >= tableProcessParam.getItemCount()) {
			new TableItem(tableProcessParam, SWT.NONE);
			return;
		}

		TableItem lastItem = tableProcessParam.getItem(tableProcessParam.getItemCount() - 1);

		for (int i = 0; i < tableProcessParam.getColumnCount(); i++) {
			if (!lastItem.getText().equals("")) {
				new TableItem(tableProcessParam, SWT.NONE);
				return;
			}
		}
	}

	/**
	 * 
	 */
	public void reset() {
		Process.loadProcess(EModSession.getProcessName());
		update();
	}

	private void threadedUpdate() {
		if (updatethreadRunning)
			return;

		updatethreadRunning = true;

		Thread updateThread = new Thread() {
			@Override
			public void run() {
				getDisplay().syncExec(new Runnable() {

					@Override
					public void run() {

						TableItem[] item;

						if (tableProcessParam.isDisposed())
							return;

						for (TableColumn tc : tableProcessParam.getColumns())
							tc.dispose();

						TableColumn column = new TableColumn(tableProcessParam, SWT.NULL);
						column.setText(LocalizationHandler.getItem("app.gui.sim.inputs.time") + " [s]");

						/*
						 * Fill the table We have two sources - Process file: Keys and values exist -
						 * New Simulators: Keys and values must be added
						 */

						// Process File

						// Simulators
						scNames = new ArrayList<String>();
						scUnits = new ArrayList<SiUnit>();
						for (ASimulationControl sc : Machine.getInstance().getVariableInputObjectList()) {
							scNames.add(sc.getName());
							scUnits.add(sc.getUnit());
							if (!(Process.getVariableNames().contains(sc.getName())))
								try {
									Process.addProcessVariable(sc.getName());
								} catch (Exception e) {
									logger.info("Added new variable to process file: " + sc.getName());
								}
						}

						// Write Heads
						for (String s : scNames) {
							column = new TableColumn(tableProcessParam, SWT.NULL);
							column.setText(s + " [" + scUnits.get(scNames.indexOf(s)).toString() + "]");
						}

						// Table items
						item = new TableItem[Process.getNumberOfTimeStamps() + 1];

						// Time Data
						for (int i = 0; i < Process.getNumberOfTimeStamps(); i++) {
							item[i] = new TableItem(tableProcessParam, SWT.NONE);
							item[i].setText(0, Double.toString(Process.getTime()[i]));
						}

						// Variable Data
						for (int j = 0; j < scNames.size(); j++) {
							double[] tmp = Process.getProcessVariable(scNames.get(j));
							for (int i = 0; i < Process.getNumberOfTimeStamps(); i++) {
								if (i < tmp.length)
									item[i].setText(j + 1, Double.toString(tmp[i]));
								else
									item[i].setText(j + 1, "0");
							}

						}

						// Add empty entry
						addEmptyLine();

						EModStatusBarGUI.getProgressBar().setText("Loading process file ...");
						EModStatusBarGUI.getProgressBar().updateProgressbar(0, false);

						// Tabelle packen
						TableColumn[] columns = tableProcessParam.getColumns();
						for (int i = 0; i < columns.length; i++) {
							columns[i].pack();
							EModStatusBarGUI.getProgressBar().updateProgressbar(i * 100 / columns.length, false);
						}

						EModStatusBarGUI.getProgressBar().reset();

						tableProcessParam.setEnabled(true);

						updatethreadRunning = false;
					}
				});
			}
		};
		// background thread
		updateThread.setDaemon(true);
		updateThread.start();
	}

	@Override
	public void init() {
		setLayout(new GridLayout(1, false));

		Process.loadProcess(EModSession.getProcessName());

		// Auswahl Prozess
		// processManageGUI = new ProcessManageGUI(this, SWT.NONE);

		// Tabelle fuer Prozess initieren
		tableProcessParam = new Table(this, SWT.BORDER | SWT.MULTI | SWT.V_SCROLL | SWT.H_SCROLL);
		tableProcessParam.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		tableProcessParam.setLinesVisible(true);
		tableProcessParam.setHeaderVisible(true);

		try {
			TableUtils.addCellEditor(tableProcessParam, this, null);
			TableUtils.addCopyToClipboard(tableProcessParam);
			TableUtils.addPastFromClipboard(tableProcessParam, this, true);
		} catch (Exception e) {
			e.printStackTrace();
		}

		update();
	}
}
