/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.gui.scenario;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CCombo;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.TableEditor;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

import ch.zuestengineering.emod.EModSession;
import ch.zuestengineering.emod.States;
import ch.zuestengineering.emod.gui.AGUITab;
import ch.zuestengineering.emod.gui.ContentHandlerGUI;
import ch.zuestengineering.emod.gui.icons.IconHandler;
import ch.zuestengineering.emod.gui.utils.TableUtils;
import ch.zuestengineering.emod.simulation.MachineState;
import ch.zuestengineering.emod.utils.LocalizationHandler;

/**
 * GUI for state sequence editing
 * 
 * @author sizuest
 * 
 */
public class StatesGUI extends AGUITab {

	private Table tableStateSequence;

	// fields used in the Table for the State Sequences
	private String[] stateList;

	/**
	 * New state editor in the given parent
	 * 
	 * @param parent
	 * @param style
	 */
	public StatesGUI(Composite parent, int style) {
		super(parent, style);

		init();
	}

	/**
	 * @param parent
	 * @param style
	 */
	public StatesGUI(CTabFolder parent, int style) {
		super(parent, style, LocalizationHandler.getItem("app.gui.sim.machinestatesequence.title"));

		init();
	}

	/**
	 * 
	 */
	@Override
	public void wasEdited() {
		save();
		update();
		ContentHandlerGUI.updateSessionTree();
	}

	private void addStateSequenceItem(final int index) {
		tableStateSequence.setRedraw(false);

		// create new table item in the tableModelView
		final TableItem item = new TableItem(tableStateSequence, SWT.NONE, index);

		// create combo for drop-down selection of the state
		final CCombo comboEditState = new CCombo(tableStateSequence, SWT.PUSH);
		// create button to append a new state
		final Button buttonAddState = new Button(tableStateSequence, SWT.PUSH);
		// create button to move state up
		final Button buttonUpState = new Button(tableStateSequence, SWT.PUSH);
		// create button to move state down
		final Button buttonDownState = new Button(tableStateSequence, SWT.PUSH);
		// create button to delete the last state of the list
		final Button buttonDeleteState = new Button(tableStateSequence, SWT.PUSH);

		// write id, time, duration and state to table
		// first colum: id
		item.setText(0, String.valueOf(index));

		// second column: time (if first item, time = 0.00)
		if (index == 0) {
			item.setText(1, "0.00");
		} else {
			Double startTime = // States.getTime(index-1) +
								// States.getDuration(index-1);
					Double.parseDouble(tableStateSequence.getItem(index - 1).getText(1))
							+ Double.parseDouble(tableStateSequence.getItem(index - 1).getText(2));
			item.setText(1, String.valueOf(startTime));
		}

		// third column: duration
		item.setText(2, States.getDuration(index).toString());
		// fourth column: state
		item.setText(3, States.getState(index).toString());

		// get the values for the drop-down combo
		String[] comboItems = new String[MachineState.values().length];
		for (MachineState ms : MachineState.values()) {
			comboItems[ms.ordinal()] = ms.name();
		}
		comboEditState.setItems(comboItems);

		// prefill the combo with the current state
		final int id = index;
		comboEditState.setText(States.getState(id).toString());
		comboEditState.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				// set the selected value into the cell behind the combo
				item.setText(3, comboEditState.getText());
				wasEdited();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent event) {
				// Not used
			}
		});

		// pack the combo and set it into the cell
		comboEditState.pack();
		final TableEditor editor = new TableEditor(tableStateSequence);
		editor.minimumWidth = comboEditState.getSize().x;
		int widthColumnFour = comboEditState.getSize().x;
		editor.grabHorizontal = true;
		editor.horizontalAlignment = SWT.LEFT;
		editor.setEditor(comboEditState, item, 3);

		// create button to add a new row
		buttonAddState.setImage(IconHandler.getIcon(getDisplay(), "add_obj"));
		buttonAddState.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				// append a state with duration 0 and state OFF
				// States.appendState(0, MachineState.OFF);
				States.insertState(index + 1, 0, MachineState.OFF);
				// get rid of the buttons, refresh table
				buttonAddState.dispose();
				buttonDeleteState.dispose();
				update();
				wasEdited();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent event) {
				// Not used
			}
		});
		// pack the button and set it into the cell
		buttonAddState.pack();
		TableEditor editor2 = new TableEditor(tableStateSequence);
		editor2.minimumWidth = buttonAddState.getSize().x;
		editor2.horizontalAlignment = SWT.LEFT;
		editor2.setEditor(buttonAddState, item, 4);

		// create button to move element up
		if (index > 0) {
			buttonUpState.setImage(IconHandler.getIcon(getDisplay(), "backward_nav"));
			buttonUpState.addSelectionListener(new SelectionListener() {
				@Override
				public void widgetSelected(SelectionEvent event) {
					States.moveStateUp(index);
					update();
					wasEdited();
				}

				@Override
				public void widgetDefaultSelected(SelectionEvent event) {
					// Not used
				}
			});
			// pack the button and set it into the cell
			buttonUpState.pack();
			TableEditor editor3 = new TableEditor(tableStateSequence);
			editor3.minimumWidth = buttonUpState.getSize().x;
			editor3.horizontalAlignment = SWT.LEFT;
			editor3.setEditor(buttonUpState, item, 5);
		}

		// create button to move element up
		if (index + 1 < States.getStateCount()) {
			buttonDownState.setImage(IconHandler.getIcon(getDisplay(), "forward_nav"));
			buttonDownState.addSelectionListener(new SelectionListener() {
				@Override
				public void widgetSelected(SelectionEvent event) {

					States.moveStateDown(index);
					update();
					wasEdited();
				}

				@Override
				public void widgetDefaultSelected(SelectionEvent event) {
					// Not used
				}
			});
			// pack the button and set it into the cell
			buttonDownState.pack();
			TableEditor editor4 = new TableEditor(tableStateSequence);
			editor4.minimumWidth = buttonDownState.getSize().x;
			editor4.horizontalAlignment = SWT.LEFT;
			editor4.setEditor(buttonDownState, item, 6);
		}

		// create button to delete last row
		buttonDeleteState.setImage(IconHandler.getIcon(getDisplay(), "delete_obj"));
		buttonDeleteState.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				// delete the cell, remove the state from the statesList,
				// refresh table
				item.dispose();
				States.getStateMap().remove(id);
				update();
				wasEdited();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent event) {
				// Not used
			}
		});
		// pack the button and set it into the cell
		buttonDeleteState.pack();
		TableEditor editor5 = new TableEditor(tableStateSequence);
		editor5.minimumWidth = buttonDeleteState.getSize().x;
		editor5.horizontalAlignment = SWT.LEFT;
		editor5.setEditor(buttonDeleteState, item, 7);

		if (States.getStateCount() == 1)
			buttonDeleteState.setEnabled(false);

		TableColumn[] columns = tableStateSequence.getColumns();
		for (int i = 0; i < columns.length; i++) {
			columns[i].pack();
		}
		tableStateSequence.getColumn(3).setWidth(widthColumnFour);

		// if a cell gets deleted, make shure that the combo and buttons get
		// deleted too!
		item.addDisposeListener(new DisposeListener() {
			@Override
			public void widgetDisposed(DisposeEvent e) {
				comboEditState.dispose();
				buttonAddState.dispose();
				buttonDeleteState.dispose();
				buttonDownState.dispose();
				buttonUpState.dispose();
			}
		});
		tableStateSequence.setRedraw(true);
	}

	/**
	 * 
	 */
	public void reset() {
		// Load state Data
		States.readStates();

		update();
	}

	@Override
	public void update() {
		// delete the content of the table
		tableStateSequence.setRedraw(false);
		tableStateSequence.clearAll();
		tableStateSequence.setItemCount(0);

		// fill the table with the values form States
		for (int i = 0; i < States.getStateCount(); i++) {
			addStateSequenceItem(i);
		}

		// show new Table
		tableStateSequence.setRedraw(true);
		this.redraw();
		redraw();
	}

	@Override
	public void save() {
		States.removeAllStates();

		if (tableStateSequence == null)
			return;

		for (TableItem ti : tableStateSequence.getItems())
			try {
				States.appendState(Double.valueOf(ti.getText(2)), MachineState.valueOf(ti.getText(3)));
			} catch (Exception e) {
				break;
			}

		States.saveStates(EModSession.getMachineName(), EModSession.getSimulationConfig());
	}

	@Override
	public void init() {

		States.readStates();

		this.setLayout(new FillLayout());

		tableStateSequence = new Table(this, SWT.BORDER | SWT.MULTI | SWT.V_SCROLL);
		// tableStateSequence.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true,
		// 1, 1));
		tableStateSequence.setLinesVisible(true);
		tableStateSequence.setHeaderVisible(true);

		String[] bTitles = { "    ", LocalizationHandler.getItem("app.gui.sim.machinestatesequence.time"),
				LocalizationHandler.getItem("app.gui.sim.machinestatesequence.duration"),
				LocalizationHandler.getItem("app.gui.sim.machinestatesequence.state"), "       ", "       ", "       ",
				"       " };
		for (int i = 0; i < bTitles.length; i++) {
			TableColumn column = new TableColumn(tableStateSequence, SWT.NULL);
			column.setText(bTitles[i]);
		}

		try {
			TableUtils.addCellEditor(tableStateSequence, this, new int[] { 2 });
		} catch (Exception e) {
			e.printStackTrace();
		}

		// workaround for the combo to select the state
		stateList = new String[States.getStateCount()];
		for (int i = 0; i < States.getStateCount(); i++) {
			stateList[i] = States.getState(i).toString();
		}

		for (int i = 0; i < States.getStateCount(); i++) {
			addStateSequenceItem(i);
		}

		this.update();
		this.layout();
		this.pack();
	}
}
