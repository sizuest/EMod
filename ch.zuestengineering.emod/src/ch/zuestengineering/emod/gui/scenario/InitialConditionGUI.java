/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.gui.scenario;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

import ch.zuestengineering.emod.Machine;
import ch.zuestengineering.emod.gui.AGUITab;
import ch.zuestengineering.emod.gui.utils.TableUtils;
import ch.zuestengineering.emod.simulation.DynamicState;
import ch.zuestengineering.emod.utils.LocalizationHandler;

/**
 * GUI to edit the initial conditions
 * 
 * @author sizuest
 *
 */
public class InitialConditionGUI extends AGUITab {

	private Table tableSimParam;

	/**
	 * @param parent
	 * @param style
	 */
	public InitialConditionGUI(Composite parent, int style) {
		super(parent, style);
		init();
	}

	/**
	 * @param parent
	 * @param style
	 */
	public InitialConditionGUI(CTabFolder parent, int style) {
		super(parent, style, LocalizationHandler.getItem("app.gui.sim.initialconditions.title"));
		init();
	}

	/**
	 * 
	 */
	@Override
	public void wasEdited() {
		save();
	}

	@Override
	public void update() {
		tableSimParam.clearAll();
		tableSimParam.setItemCount(0);

		try {
			for (DynamicState s : Machine.getInstance().getDynamicStatesList()) {
				TableItem item = new TableItem(tableSimParam, SWT.NONE);
				item.setText(0, s.getParent());
				item.setText(1, s.getName());
				item.setText(2, s.getInitialValue() + "");
				item.setText(3, s.getUnit().toString());
			}
		} catch (Exception e) {
			logger.warning("Failed to display initial states.");
		}

		// Tabelle packen
		TableColumn[] columns = tableSimParam.getColumns();
		for (int i = 0; i < columns.length; i++) {
			columns[i].pack();
		}
	}

	@Override
	public void save() {

		for (TableItem ti : tableSimParam.getItems()) {
			Machine.getInstance().getDynamicState(ti.getText(1), ti.getText(0))
					.setInitialCondition(Double.parseDouble(ti.getText(2)));
		}

		Machine.saveInitialConditions();
	}

	/**
	 * 
	 */
	public void reset() {
		Machine.loadInitialConditions();
		update();

	}

	@Override
	public void init() {
		setLayout(new FillLayout());

		// Tabelle fuer Maschinenmodell initieren
		tableSimParam = new Table(this, SWT.BORDER);
		tableSimParam.setLinesVisible(true);
		tableSimParam.setHeaderVisible(true);

		// Titel der Spalten setzen
		String[] aTitles = { LocalizationHandler.getItem("app.gui.sim.initialconditions.component"),
				LocalizationHandler.getItem("app.gui.sim.initialconditions.state"),
				LocalizationHandler.getItem("app.gui.sim.initialconditions.value"),
				LocalizationHandler.getItem("app.gui.sim.initialconditions.unit") };

		for (int i = 0; i < aTitles.length; i++) {
			TableColumn column = new TableColumn(tableSimParam, SWT.NULL);
			column.setText(aTitles[i]);
		}

		try {
			TableUtils.addCellEditor(tableSimParam, this, new int[] { 2 });
		} catch (Exception e) {
			e.printStackTrace();
		}

		reset();
		save();
	}
}
