/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.gui.utils;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

import ch.zuestengineering.emod.utils.LocalizationHandler;

/**
 * Implements a simple GUI to confirm a delete action
 * 
 * @author simon
 *
 */
public class ConfirmGUI extends Composite {

	private Label lable;
	private Button buttonCancel, buttonAccept;
	private boolean answer = false;

	/**
	 * @param parent
	 * @param s
	 */
	public ConfirmGUI(Composite parent, String s) {
		super(parent, SWT.NONE);

		this.setLayout(new GridLayout(2, false));

		lable = new Label(this, SWT.NONE);
		lable.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 2, 1));
		lable.setText(s);

		buttonCancel = new Button(this, SWT.PUSH);
		buttonCancel.setText(LocalizationHandler.getItem("app.gui.config.no"));
		buttonCancel.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, true, false, 1, 1));
		buttonCancel.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				dispose();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		buttonAccept = new Button(this, SWT.PUSH);
		buttonAccept.setText(LocalizationHandler.getItem("app.gui.config.yes"));
		buttonAccept.setLayoutData(new GridData(SWT.RIGHT, SWT.TOP, true, false, 1, 1));
		buttonAccept.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				answer = true;
				dispose();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		pack();
		layout();
	}

	/**
	 * @param parent
	 * @param question
	 * @return
	 */
	public static boolean getNewName(Shell parent, String question) {
		final Shell shell = new Shell(parent, SWT.APPLICATION_MODAL);

		ConfirmGUI name = new ConfirmGUI(shell, question);

		name.addDisposeListener(new DisposeListener() {

			@Override
			public void widgetDisposed(DisposeEvent e) {
				shell.dispose();
			}
		});

		shell.pack();
		shell.layout();
		shell.open();

		while (!shell.isDisposed()) {
			if (!shell.getDisplay().readAndDispatch()) {
				shell.getDisplay().sleep();
			}
		}

		return name.answer;
	}

}
