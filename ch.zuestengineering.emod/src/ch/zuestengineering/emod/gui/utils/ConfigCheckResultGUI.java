/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.gui.utils;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

import ch.zuestengineering.emod.ConfigurationChecker;
import ch.zuestengineering.emod.gui.icons.IconHandler;
import ch.zuestengineering.emod.simulation.ConfigCheckResult;
import ch.zuestengineering.emod.simulation.ConfigCheckResult.MessageBundle;

/**
 * @author simon
 *
 */
public class ConfigCheckResultGUI extends Composite {

	private ConfigCheckResult results;
	private Table table;
	private Button buttonCheckCfg;

	/**
	 * @param parent
	 * @param style
	 */
	public ConfigCheckResultGUI(Composite parent, int style) {
		super(parent, style);

		results = new ConfigCheckResult();

		this.setLayout(new GridLayout(1, false));

		table = new Table(this, SWT.BORDER | SWT.MULTI | SWT.V_SCROLL);
		table.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		table.setLinesVisible(true);
		table.setHeaderVisible(true);

		try {
			TableUtils.addCopyToClipboard(table);
		} catch (Exception e) {
			e.printStackTrace();
		}

		String[] heads = { "Origin", "Message" };
		for (String s : heads) {
			TableColumn col = new TableColumn(table, SWT.NONE);
			col.setText(s);
		}

		buttonCheckCfg = new Button(this, SWT.NONE);
		buttonCheckCfg.setLayoutData(new GridData(SWT.RIGHT, SWT.BOTTOM, false, false));
		buttonCheckCfg.setText("Run");
		buttonCheckCfg.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				runChecks();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				/* Not used */}
		});

		this.layout();
		this.update();
	}

	/**
	 * Sets the results to be displayed and updates the composite
	 * 
	 * @param results
	 */
	public void setResults(ConfigCheckResult results) {
		this.results = results;
		update();
	}

	@Override
	public void update() {
		super.update();

		// Clear the table
		for (TableItem ti : table.getItems())
			ti.dispose();
		table.setItemCount(0);

		TableItem item;

		// Fill Table
		for (MessageBundle mb : results.getMessages()) {
			item = new TableItem(table, SWT.NONE);
			item.setText(0, mb.getOrigin() + "  ");
			item.setText(1, mb.getMessage());

			item.setImage(IconHandler.getIcon(getDisplay(), mb.getState(), "information"));
		}

		if (results.getMessages().size() == 0)
			item = new TableItem(table, SWT.NONE);

		// Pack
		for (TableColumn col : table.getColumns())
			col.pack();

		this.layout();
	}

	/**
	 * 
	 */
	public void runChecks() {
		buttonCheckCfg.setEnabled(false);

		ConfigCheckResult ccrMachine = new ConfigCheckResult();
		ConfigCheckResult ccrSimCfg = new ConfigCheckResult();
		ConfigCheckResult ccrProcess = new ConfigCheckResult();
		ConfigCheckResult ccrResults = new ConfigCheckResult();

		ConfigCheckResult ccrAll = new ConfigCheckResult();

		ccrMachine.addAll(ConfigurationChecker.checkMachineConfig());
		ccrSimCfg.addAll(ConfigurationChecker.checkSimulationConfig());
		ccrProcess.addAll(ConfigurationChecker.checkProcess());
		ccrResults.addAll(ConfigurationChecker.checkResults());

		ccrAll.addAll(ccrMachine);
		ccrAll.addAll(ccrSimCfg);
		ccrAll.addAll(ccrProcess);
		ccrAll.addAll(ccrResults);

		setResults(ccrAll);

		buttonCheckCfg.setEnabled(true);
	}

}
