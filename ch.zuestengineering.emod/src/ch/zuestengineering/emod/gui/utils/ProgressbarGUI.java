/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.gui.utils;

import java.text.SimpleDateFormat;
import java.util.logging.Logger;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Monitor;
import org.eclipse.swt.widgets.ProgressBar;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TaskBar;
import org.eclipse.swt.widgets.TaskItem;

import ch.zuestengineering.emod.gui.icons.IconHandler;
import ch.zuestengineering.emod.utils.LocalizationHandler;

/**
 * Implements the display of a process by its name and current status
 * (progressbar). A button can be displayed to interup the process. If the
 * button is pressed the corresponding flag will be set to true and can be
 * accessed by the method getCancelStatus().
 * 
 * @author sizuest
 *
 */
public class ProgressbarGUI extends Composite {

	private Label textLoad;
	private String textString;
	private ProgressBar pb;
	private Button buttonCancel;
	protected Logger logger = Logger.getLogger(ProgressbarGUI.class.getName());

	private boolean cancelPressed = false;

	SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");

	/**
	 * Create a new ProgressbarGUI with the status in textString This will create a
	 * new shell
	 * 
	 * @param textString
	 * @return
	 */
	public static ProgressbarGUI newProgressbarGUI(String textString) {
		final Shell shell = new Shell(Display.getCurrent(), SWT.SYSTEM_MODAL | SWT.CLOSE);
		shell.setLocation(Display.getCurrent().getBounds().x / 2, Display.getCurrent().getBounds().y / 2);
		shell.setText(LocalizationHandler.getItem(textString));

		ProgressbarGUI gui = new ProgressbarGUI(shell, textString);

		Display display = Display.getCurrent();
		Monitor primary = display.getPrimaryMonitor();
		Rectangle bounds = primary.getBounds();
		Rectangle rect = shell.getBounds();

		int x = bounds.x + (bounds.width - rect.width) / 2;
		int y = bounds.y + (bounds.height - rect.height) / 2;

		shell.setLocation(x, y);
		// open the new shell
		shell.open();
		shell.pack();

		gui.addDisposeListener(new DisposeListener() {
			@Override
			public void widgetDisposed(DisposeEvent e) {
				shell.dispose();
			}
		});

		return gui;

	}

	/**
	 * Create a new ProgressbarGUI with the status in textString, placed in the
	 * existing composite parent
	 * 
	 * @param parent
	 * @param textString
	 */
	public ProgressbarGUI(Composite parent, String textString) {
		super(parent, SWT.NONE | SWT.NO_TRIM);

		this.textString = textString;

		init();

	}

	/**
	 * Update the status text
	 * 
	 * @param text
	 */
	public void setText(String text) {
		this.textString = text;
		textLoad.setText(this.textString);

		this.layout();
		this.getParent().layout();

		pb.setVisible(true);
		buttonCancel.setVisible(true);
	}

	/**
	 * Reset the GUI - no text - no progress bar - no button
	 */
	public void reset() {
		cancelPressed = false;
		this.textString = "";
		textLoad.setText(this.textString);
		pb.setSelection(0);
		pb.setVisible(false);
		buttonCancel.setVisible(false);
		pb.setEnabled(false);

		TaskItem ti = getTaskBarItem();
		if (ti != null) {
			ti.setProgressState(SWT.DEFAULT);
		}

		this.layout();
		this.getParent().layout();
	}

	private void init() {

		this.setLayout(new GridLayout(3, false));

		textLoad = new Label(this, SWT.TRANSPARENT);
		textLoad.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));

		pb = new ProgressBar(this, SWT.SMOOTH);
		pb.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		buttonCancel = new Button(this, SWT.PUSH);
		buttonCancel.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 1));
		buttonCancel.setImage(IconHandler.getIcon(getDisplay(), "progress_stop"));

		buttonCancel.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				cancelPressed = true;
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {/* Not used */
			}
		});

		this.layout();

		reset();
	}

	/**
	 * Returns, whether the cancel button has be pressed, since it has been enabled
	 * last time
	 * 
	 * @return
	 */
	public boolean getCancelStatus() {
		return cancelPressed;
	}

	/**
	 * Update the progress bar with the current value: 0<=progress<=100
	 * 
	 * @param progress
	 */
	public void updateProgressbar(int progress) {
		try {
			updateProgressbar(progress, false);
		} catch(Exception e) {}
	}

	/**
	 * update the progress bar
	 * 
	 * @param progress         set from 0 to 100 as percentage of progress
	 * @param showCancelButton
	 */
	public void updateProgressbar(int progress, boolean showCancelButton) {
		pb.setVisible(true);

		try {
			pb.setSelection(progress);
			pb.setEnabled(true);

			TaskItem ti = getTaskBarItem();
			if (ti != null) {
				ti.setProgress(pb.getSelection());
				ti.setProgressState(SWT.NORMAL);
			}

			if (!(buttonCancel.isVisible() == showCancelButton))
				buttonCancel.setVisible(false);

			this.update();
			this.layout();
			this.getParent().layout();
		} catch (Exception e) {
			logger.warning("ProgressbarGUI: Update failed!");
		}
	}

	/**
	 * Update the progress bar with the current value: 0<=progress<=100
	 * 
	 * @param progress
	 */
	public void updateProgressbar(double progress) {
		updateProgressbar((int) (progress));
	}

	private TaskItem getTaskBarItem() {
		TaskBar bar = Display.getCurrent().getSystemTaskBar();
		if (bar == null)
			return null;

		TaskItem item = bar.getItem(getShell());
		if (item == null)
			item = bar.getItem(null);

		return item;
	}
}
