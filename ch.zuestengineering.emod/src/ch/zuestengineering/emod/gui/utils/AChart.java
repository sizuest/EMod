/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.gui.utils;

import java.util.logging.Logger;

import org.eclipse.swt.SWT;
import org.eclipse.swt.dnd.Clipboard;
import org.eclipse.swt.dnd.ImageTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.events.MouseMoveListener;
import org.eclipse.swt.events.MouseWheelListener;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;
import org.swtchart.Chart;
import org.swtchart.IAxis;
import org.swtchart.ILineSeries;
import org.swtchart.ISeries;
import org.swtchart.Range;

import ch.zuestengineering.emod.gui.analysis.EditAxisGUI;
import ch.zuestengineering.emod.gui.icons.IconHandler;

/**
 * @author Simon Z�st
 *
 */
public abstract class AChart extends Composite {
	private Chart chart = null;
	protected ToolBar toolBar;
	private ChartAction action = ChartAction.NONE;
	private Point mouseStartAction, mouseMidAction;
	protected Logger logger = Logger.getLogger(AChart.class.getName());

	/**
	 * @param parent
	 * @param style
	 */
	public AChart(Composite parent, int style) {
		super(parent, style);

		this.setLayout(new GridLayout(1, false));

		createToolBar();

		// Listener for the whole chart
		getChart().addMouseWheelListener(new MouseWheelListener() {
			@Override
			public void mouseScrolled(MouseEvent e) {
				if (e.count > 0)
					zoomOut(e.x, e.y);
				else
					zoomIn(e.x, e.y);

			}

		});
		getChart().addMouseListener(new MouseListener() {

			@Override
			public void mouseUp(MouseEvent e) {
			}

			@Override
			public void mouseDown(MouseEvent e) {
			}

			@Override
			public void mouseDoubleClick(MouseEvent e) {
				for (IAxis ax : getChart().getAxisSet().getAxes())
					if (ax.getTick().getBounds().contains(e.x, e.y)) {
						EditAxisGUI.editAxisGUI(getShell(), getChart().toDisplay(e.x, e.y), getChart(), ax);
						break;
					}
			}
		});

		// Listeners for the plot area
		getChart().getPlotArea().addMouseListener(new MouseListener() {

			@Override
			public void mouseUp(MouseEvent e) {
				if (e.button != 1)
					return;

				Point mouseEndAction = new Point(e.x, e.y);
				switch (action) {
				case NONE:
					break;
				case PAN:
					shift(mouseStartAction, mouseEndAction);
					break;
				case ZOOMIN:
					zoomIn(e.x, e.y);
					break;
				case ZOOMOUT:
					zoomOut(e.x, e.y);
					break;
				case ZOOM:
					zoom(mouseStartAction, mouseEndAction);
					break;
				default:
					break;

				}

				mouseStartAction = null;
			}

			@Override
			public void mouseDown(MouseEvent e) {
				if (e.button != 1)
					return;

				mouseStartAction = new Point(e.x, e.y);

				switch (action) {
				case ZOOM:
					break;
				default:
					break;
				}
			}

			@Override
			public void mouseDoubleClick(MouseEvent e) {
				getChart().getAxisSet().adjustRange();
				getChart().redraw();
			}
		});

		getChart().getPlotArea().addMouseMoveListener(new MouseMoveListener() {

			@Override
			public void mouseMove(MouseEvent e) {
				if (null != mouseStartAction) {

					mouseMidAction = new Point(e.x, e.y);

					switch (action) {
					case PAN:
						shift(mouseStartAction, mouseMidAction);
						mouseStartAction = mouseMidAction;
						break;
					case ZOOM:
						getChart().redraw();
						break;
					default:
						break;
					}
				}

			}
		});

		getChart().getPlotArea().addPaintListener(new PaintListener() {

			@Override
			public void paintControl(PaintEvent e) {
				if (action.equals(ChartAction.ZOOM) && null != mouseStartAction) {
					GC gc = e.gc;

					int minX = Math.min(mouseStartAction.x, mouseMidAction.x);
					int maxX = Math.max(mouseStartAction.x, mouseMidAction.x);

					int minY = Math.min(mouseStartAction.y, mouseMidAction.y);
					int maxY = Math.max(mouseStartAction.y, mouseMidAction.y);

					int width = maxX - minX;
					int height = maxY - minY;

					if (width < 10 || height < 10) {
						gc.setForeground(Display.getDefault().getSystemColor(SWT.COLOR_BLACK));
						gc.setBackground(Display.getDefault().getSystemColor(SWT.COLOR_BLACK));
						gc.setAlpha(255);
					} else {
						gc.setForeground(Display.getDefault().getSystemColor(SWT.COLOR_BLACK));
						gc.setBackground(Display.getDefault().getSystemColor(SWT.COLOR_GRAY));
						gc.setAlpha(128);
					}

					gc.fillRectangle(minX, minY, width, height);
				}
			}
		});

		toolBar.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));
		getChart().setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));

		this.layout();
	}

	private void shift(Point mouseDownAction, Point mouseUpAction) {
		double shift;

		shift = getChart().getAxisSet().getXAxis(0).getDataCoordinate(mouseUpAction.x)
				- getChart().getAxisSet().getXAxis(0).getDataCoordinate(mouseDownAction.x);

		Range range;

		range = getChart().getAxisSet().getXAxis(0).getRange();
		range.lower -= shift;
		range.upper -= shift;

		getChart().getAxisSet().getXAxis(0).setRange(range);

		for (IAxis ax : getChart().getAxisSet().getYAxes()) {
			shift = ax.getDataCoordinate(mouseUpAction.y) - ax.getDataCoordinate(mouseDownAction.y);

			range = ax.getRange();
			range.lower -= shift;
			range.upper -= shift;

			ax.setRange(range);
		}

		adjustLineWidth();
		getChart().redraw();
	}

	protected void zoomIn(int x, int y) {
		for (IAxis ax : getChart().getAxisSet().getXAxes())
			ax.zoomIn(ax.getDataCoordinate(x));
		for (IAxis ax : getChart().getAxisSet().getYAxes())
			ax.zoomIn(ax.getDataCoordinate(y));

		adjustLineWidth();
		getChart().redraw();
	}

	protected void zoomOut(int x, int y) {
		for (IAxis ax : getChart().getAxisSet().getXAxes())
			ax.zoomOut(ax.getDataCoordinate(x));
		for (IAxis ax : getChart().getAxisSet().getYAxes())
			ax.zoomOut(ax.getDataCoordinate(y));

		adjustLineWidth();
		getChart().redraw();
	}

	private void zoom(Point mouseDownAction, Point mouseUpAction) {

		int xl, xh, yl, yh;
		double y1, y2;

		xl = Math.min(mouseDownAction.x, mouseUpAction.x);
		xh = Math.max(mouseDownAction.x, mouseUpAction.x);
		yl = Math.min(mouseDownAction.y, mouseUpAction.y);
		yh = Math.max(mouseDownAction.y, mouseUpAction.y);

		Range range;

		if (xh - xl > 10) {
			range = getChart().getAxisSet().getXAxis(0).getRange();
			range.lower = getChart().getAxisSet().getXAxis(0).getDataCoordinate(xl);
			range.upper = getChart().getAxisSet().getXAxis(0).getDataCoordinate(xh);

			getChart().getAxisSet().getXAxis(0).setRange(range);
		}

		if (yh - yl > 10) {
			for (IAxis ax : getChart().getAxisSet().getYAxes()) {

				y1 = ax.getDataCoordinate(yl);
				y2 = ax.getDataCoordinate(yh);

				range = ax.getRange();
				range.lower = Math.min(y1, y2);
				range.upper = Math.max(y1, y2);

				ax.setRange(range);
			}
		}

		adjustLineWidth();
		getChart().redraw();
	}

	/**
	 * Create the tools bar for the chart
	 * 
	 */
	protected void createToolBar() {
		toolBar = new ToolBar(this, SWT.WRAP);
		toolBar.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));
		// toolBar.setBackground(getParent().getBackground());

		ToolItem itemZoomIn, itemZoomOut, itemZoomReset, itemZoom, itemPan, itemSave, itemCopy;

		itemZoomIn = new ToolItem(toolBar, SWT.CHECK);
		itemZoomIn.setImage(IconHandler.getIcon(getDisplay(), "graph_zoomin"));

		itemZoomOut = new ToolItem(toolBar, SWT.CHECK);
		itemZoomOut.setImage(IconHandler.getIcon(getDisplay(), "graph_zoomout"));

		itemZoomReset = new ToolItem(toolBar, SWT.PUSH);
		itemZoomReset.setImage(IconHandler.getIcon(getDisplay(), "graph_zoomall"));

		itemZoom = new ToolItem(toolBar, SWT.CHECK);
		itemZoom.setImage(IconHandler.getIcon(getDisplay(), "graph_zoom"));

		itemPan = new ToolItem(toolBar, SWT.CHECK);
		itemPan.setImage(IconHandler.getIcon(getDisplay(), "graph_pan"));

		itemSave = new ToolItem(toolBar, SWT.PUSH);
		itemSave.setImage(IconHandler.getIcon(getDisplay(), "save_edit"));

		itemCopy = new ToolItem(toolBar, SWT.PUSH);
		itemCopy.setImage(IconHandler.getIcon(getDisplay(), "copy_edit"));

		itemZoomIn.addSelectionListener(getToolbarSelectionListener(ChartAction.ZOOMIN));
		itemZoomOut.addSelectionListener(getToolbarSelectionListener(ChartAction.ZOOMOUT));
		itemPan.addSelectionListener(getToolbarSelectionListener(ChartAction.PAN));
		itemZoom.addSelectionListener(getToolbarSelectionListener(ChartAction.ZOOM));

		itemZoomReset.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				getChart().getAxisSet().adjustRange();
				getChart().redraw();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		itemSave.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				saveImage();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		itemCopy.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				copyToClipboard();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		toolBar.pack();
	}

	private SelectionListener getToolbarSelectionListener(final ChartAction a) {
		SelectionListener listener = new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				for (ToolItem ti : toolBar.getItems())
					if (!e.getSource().equals(ti))
						ti.setSelection(false);
					else if (ti.getSelection())
						action = a;
					else
						action = ChartAction.NONE;

			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		};

		return listener;
	}

	/**
	 * Get the chart object
	 * 
	 * @return
	 */
	public Chart getChart() {
		if (chart == null)
			chart = new Chart(this, SWT.NONE);

		return chart;
	}

	/**
	 * Clears all items from the chart
	 */
	public void clearChart() {
		// clear the chart
		for (ISeries s : getChart().getSeriesSet().getSeries())
			getChart().getSeriesSet().deleteSeries(s.getId());

		// and clear any axes
		for (IAxis ay : getChart().getAxisSet().getYAxes())
			if (0 != ay.getId())
				getChart().getAxisSet().deleteYAxis(ay.getId());
		for (IAxis ax : getChart().getAxisSet().getXAxes())
			if (0 != ax.getId())
				getChart().getAxisSet().deleteXAxis(ax.getId());
	}

	/**
	 * 
	 */
	public void copyToClipboard() {

		Point size = getChart().getSize();
		GC gc = new GC(chart);
		Image image = new Image(Display.getDefault(), size.x, size.y);
		gc.copyArea(image, 0, 0);
		gc.dispose();

		ImageTransfer imageTransfer = ImageTransfer.getInstance();

		Clipboard clipboard = new Clipboard(Display.getCurrent());
		clipboard.setContents(new Object[] { image.getImageData() }, new Transfer[] { imageTransfer });
		clipboard.dispose();
	}

	/**
	 * Saves the image at the given path
	 * 
	 * @param path
	 */
	public void saveImage(String path) {
		getChart().save(path, SWT.IMAGE_PNG);
	}

	/**
	 * Opens a file dialog and saves the image
	 */
	public void saveImage() {
		FileDialog fd = new FileDialog(getShell(), SWT.SAVE);
		fd.setFilterPath("C:/");
		String[] filterExt = { "*.png" };
		fd.setFilterExtensions(filterExt);
		String selected = fd.open();
		if (selected == null) {
			return;
		}

		saveImage(selected);
	}

	protected abstract int getLineWidth();

	protected void adjustLineWidth() {
		int w = getLineWidth();

		for (ISeries s : getChart().getSeriesSet().getSeries())
			if (s instanceof ILineSeries)
				((ILineSeries) s).setLineWidth(w);
	}

	/**
	 * @author simon
	 *
	 */
	public enum ChartAction {
		/**
		 * 
		 */
		NONE,
		/**
		 * 
		 */
		ZOOM,
		/**
		 * 
		 */
		ZOOMIN,
		/**
		 * 
		 */
		ZOOMOUT,
		/**
		 * 
		 */
		PAN
	}
}
