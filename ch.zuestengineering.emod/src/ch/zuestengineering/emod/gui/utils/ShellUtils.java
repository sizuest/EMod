/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.gui.utils;

import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Shell;

/**
 * Implements various functions for SWT shells
 * 
 * @author Simon Z�st
 *
 */
public class ShellUtils {

	/**
	 * Moves the shell's center to the traget's center
	 * 
	 * @param shell
	 * @param target
	 */
	public static void putToCenter(Shell shell, Shell target) {

		Rectangle posShell, posTarget;
		int centerX, centerY;

		posTarget = target.getBounds();
		// Target position
		centerX = posTarget.x + posTarget.width / 2;
		centerY = posTarget.y + posTarget.height / 2;

		posShell = shell.getBounds();
		posShell.x = centerX - posShell.width / 2;
		posShell.y = centerY - posShell.height / 2;

		shell.setBounds(posShell);
	}

}
