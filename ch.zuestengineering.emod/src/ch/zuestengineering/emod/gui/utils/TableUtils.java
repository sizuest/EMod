/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.gui.utils;

import java.awt.Toolkit;
import java.awt.datatransfer.DataFlavor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CCombo;
import org.eclipse.swt.custom.ControlEditor;
import org.eclipse.swt.custom.TableCursor;
import org.eclipse.swt.custom.TableEditor;
import org.eclipse.swt.dnd.Clipboard;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

import ch.zuestengineering.emod.gui.AConfigGUI;
import ch.zuestengineering.emod.gui.AGUITab;
import ch.zuestengineering.emod.gui.SelectMaterialGUI;

/**
 * Implements general table functions for SWT Tables
 * 
 * @author sizuest
 * 
 */
public class TableUtils {

	/**
	 * Adds following key-event related functions: Edit cell
	 * 
	 * @param table
	 * @param idx
	 * @throws Exception
	 */
	public static void addCellEditor(Table table, int[] idx) throws Exception {
		addCellEditor(table, null, null, idx);
	}

	/**
	 * Add the cell editor to a table part of a config gui This will force the
	 * listener to call {@link AConfigGUI}.wasEdited() when the table is modified
	 * 
	 * @param table
	 * @param gui
	 * @param idx
	 * @throws Exception
	 */
	public static void addCellEditor(Table table, AConfigGUI gui, int[] idx) throws Exception {
		addCellEditor(table, AConfigGUI.class.getMethod("wasEdited"), gui, idx);
	}

	/**
	 * Add the cell editor to a table part of a config gui This will force the
	 * listener to call {@link AConfigGUI}.wasEdited() when the table is modified
	 * 
	 * @param table
	 * @param gui
	 * @param idx
	 * @throws Exception
	 */
	public static void addCellEditor(Table table, AGUITab gui, int[] idx) throws Exception {
		addCellEditor(table, AGUITab.class.getMethod("wasEdited"), gui, idx);
	}

	/**
	 * Adds following key-event related functions: Edit cell
	 * 
	 * @param table
	 * @param fun
	 * @param funObj
	 */
	public static void addCellEditor(Table table, final Method fun, final Object funObj) {
		addCellEditor(table, fun, funObj, null);
	}

	/**
	 * Adds following key-event related functions: Edit cell
	 * 
	 * @param table
	 * @param fun
	 * @param funObj
	 * @param idx
	 */
	public static void addCellEditor(final Table table, final Method fun, final Object funObj, final int[] idx) {
		addCellEditor(table, fun, funObj, idx, null);
	}

	/**
	 * Adds following key-event related functions: Edit cell
	 * 
	 * @param table
	 * @param fun
	 * @param funObj
	 * @param idx
	 * @param materialIdx
	 */
	public static void addCellEditor(final Table table, final Method fun, final Object funObj, final int[] idx,
			final int[] materialIdx) {
		// SOURCE
		// http://www.tutorials.de/threads/in-editierbarer-swt-tabelle-ohne-eingabe-von-enter-werte-aendern.299858/
		// create a TableCursor to navigate around the table
		final TableCursor cursor = new TableCursor(table, SWT.NO_FOCUS);
		// create an editor to edit the cell when the user hits "ENTER"
		// while over a cell in the table
		final ControlEditor editor = new ControlEditor(cursor);
		editor.grabHorizontal = true;
		editor.grabVertical = true;

		cursor.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				/* Check if the column is marked as material input */
				if (null != materialIdx)
					for (int i = 0; i < materialIdx.length; i++)
						if (cursor.getColumn() == materialIdx[i]) {
							selectMaterial(cursor.getRow(), i);
							return;
						}

				/* Check if the column is marked as selectable */
				if (null != idx)
					for (int i = 0; i < idx.length; i++) {
						if (cursor.getColumn() == idx[i])
							break;
						if (i == idx.length - 1)
							return;
					}

				/* Add input field */
				final ParameterEditor text = new ParameterEditor(SWT.NONE, cursor);

				// If a function is defined, call it as soon as the editor is
				// disposed
				if (fun != null & funObj != null)
					text.addDisposeListener(new DisposeListener() {

						@Override
						public void widgetDisposed(DisposeEvent e) {
							try {
								fun.invoke(funObj);
							} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e1) {
								e1.printStackTrace();
							}
						}
					});

				TableItem row = cursor.getRow();
				int column = cursor.getColumn();
				text.setText(row.getText(column));

				editor.setEditor(text);

			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				/* Not used */}
		});

		cursor.addKeyListener(new KeyAdapter() {

			@Override
			public void keyPressed(KeyEvent e) {

				if (null != idx)
					for (int i = 0; i < idx.length; i++) {
						if (cursor.getColumn() == idx[i])
							break;
						if (i == idx.length - 1)
							return;
					}

				switch (e.keyCode) {
				case SWT.ARROW_UP:
				case SWT.ARROW_RIGHT:
				case SWT.ARROW_DOWN:
				case SWT.ARROW_LEFT:
					// an dieser stelle fehlen auch noch alle anderen tasten
					// die
					// ignoriert werden sollen...wie F1-12, esc,bsp,....
					break;

				default:

				}
			}
		});
	}

	/**
	 * Copy the stated table to the clip board
	 * 
	 * @param table
	 */
	public static void addCopyToClipboard(final Table table) {
		table.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.stateMask == SWT.CTRL && e.keyCode == 'c') {
					String text = TableUtils.toText(table, false);
					Clipboard clip = new Clipboard(Display.getCurrent());
					clip.setContents(new Object[] { text }, new TextTransfer[] { TextTransfer.getInstance() });
					clip.dispose();
				}
			}
		});
	}

	/**
	 * Copy the clip board content to the stated table
	 * 
	 * @param table
	 * @param gui
	 * @param checkForHeader
	 * @throws Exception
	 */
	public static void addPastFromClipboard(Table table, AGUITab gui, boolean checkForHeader) throws Exception {
		addPastFromClipboard(table, AGUITab.class.getMethod("wasEdited"), gui, checkForHeader);
	}

	/**
	 * Copy the clip board content to the stated table
	 * 
	 * @param table
	 * @param checkForHeader
	 * @throws Exception
	 */
	public static void addPastFromClipboard(Table table, boolean checkForHeader) throws Exception {
		addPastFromClipboard(table, null, null, checkForHeader);
	}

	/**
	 * Copy the clip board content to the stated table after the past to the table,
	 * the provided method will be called on the provided object
	 * 
	 * @param table
	 * @param fun
	 * @param funObj
	 * @param checkForHeaders
	 */
	public static void addPastFromClipboard(final Table table, final Method fun, final Object funObj,
			final boolean checkForHeaders) {
		table.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.stateMask == SWT.CTRL && e.keyCode == 'v') {
					Clipboard clip = new Clipboard(Display.getCurrent());
					try {
						String text = (String) Toolkit.getDefaultToolkit().getSystemClipboard()
								.getData(DataFlavor.stringFlavor);

						if (checkForHeaders) {
							String[] cols = text.split("\n");

							if (cols.length == 0)
								return;

							if (!checkHeaders(table, cols[0].split("\t")))
								return;

							// Strip header
							text = text.replace(cols[0] + "\n", "");
						}

						fromText(table, text);

						if (fun != null & funObj != null)
							try {
								fun.invoke(funObj);
							} catch (Exception e1) {
								e1.printStackTrace();
							}

					} catch (Exception e1) {
						e1.printStackTrace();
					}
					clip.dispose();
				}
			}
		});
	}

	/**
	 * Convert the table to csv text
	 * 
	 * @param table
	 * @return
	 */
	public static String toText(Table table) {
		return toText(table, false);
	}

	/**
	 * Tests if the tables column titles match the heads
	 * 
	 * @param table
	 * @param heads
	 * @return
	 */
	public static boolean checkHeaders(Table table, String[] heads) {
		TableColumn[] cols = table.getColumns();

		if (cols.length != heads.length)
			return false;

		for (int i = 0; i < cols.length; i++) {
			if (!(cols[i].getText().equals(heads[i])))
				return false;
		}

		return true;
	}

	/**
	 * Converts the text to table entries
	 * 
	 * @param table
	 * @param text
	 * @return
	 */
	public static boolean fromText(Table table, String text) {

		String[] cols = text.split("\n");

		if (cols.length == 0)
			return false;

		String[] firstLineEntries = cols[0].split("\t");

		if (table.getColumnCount() != firstLineEntries.length)
			return false;

		/* Clear the table */
		for (TableItem ti : table.getItems())
			ti.dispose();

		table.setItemCount(0);

		/* Write the entries */
		for (String lineText : cols) {
			String[] lineEntries = lineText.split("\t");
			if (table.getColumnCount() != lineEntries.length)
				return false;

			TableItem item = new TableItem(table, SWT.NONE);
			item.setText(lineEntries);
		}

		// Tabelle packen
		TableColumn[] columns = table.getColumns();
		for (int i = 0; i < columns.length; i++) {
			columns[i].pack();
		}

		return true;

	}

	/**
	 * Convert the selected table entries to csv text
	 * 
	 * @param table
	 * @param selectionOnly
	 * @return
	 */
	public static String toText(Table table, boolean selectionOnly) {
		String out = "";
		String sep = "\t";
		String eol = "\r\n";

		// Headers
		for (int i = 0; i < table.getColumnCount(); i++) {
			out += table.getColumn(i).getText();
			if (table.getColumnCount() - 1 == i)
				out += eol;
			else
				out += sep;
		}

		// Content
		TableItem[] candidates;
		if (selectionOnly)
			candidates = table.getSelection();
		else
			candidates = table.getItems();

		for (TableItem item : candidates)
			for (int i = 0; i < table.getColumnCount(); i++) {
				out += item.getText(i);
				if (table.getColumnCount() - 1 == i)
					out += eol;
				else
					out += sep;
			}

		return out;
	}

	/**
	 * Adds tool-tip-functionality for the items of the stated table. If the mouse
	 * hovers over a table item, a tool tip is shown, where as the text of the tool
	 * tip is obtained by the function {@link AConfigGUI#getToolTip(TableItem)}
	 * 
	 * @param table
	 * @param gui
	 * @throws Exception
	 */
	public static void addItemToolTip(Table table, AConfigGUI gui) throws Exception {
		addItemToolTip(table, AConfigGUI.class.getMethod("getToolTip", TableItem.class), gui);
	}

	/**
	 * Adds tool-tip-functionality for the items of the stated table. If the mouse
	 * hovers over a table item, a tool tip is shown, where as the text of the tool
	 * tip is obtained by the provided function pointer
	 * 
	 * @param table
	 * @param fun
	 * @param funObj
	 */
	public static void addItemToolTip(final Table table, final Method fun, final Object funObj) {
		final Listener labelListener;
		Listener tableListener;

		labelListener = new Listener() {
			public void handleEvent(Event event) {
				Label label = (Label) event.widget;
				Shell shell = label.getShell();
				switch (event.type) {
				case SWT.MouseDown:
					Event e = new Event();
					e.item = (TableItem) label.getData("_TABLEITEM");
					// Assuming table is single select, set the selection as
					// if
					// the mouse down event went through to the table
					table.setSelection(new TableItem[] { (TableItem) e.item });
					table.notifyListeners(SWT.Selection, e);
					// fall through
				case SWT.MouseExit:
					shell.dispose();
					break;
				}
			}
		};

		tableListener = new Listener() {
			Shell tip = null;

			Label label = null;

			public void handleEvent(Event event) {
				switch (event.type) {
				case SWT.Dispose:
				case SWT.KeyDown:
				case SWT.MouseMove: {
					if (tip == null)
						break;
					tip.dispose();
					tip = null;
					label = null;
					break;
				}
				case SWT.MouseHover: {
					TableItem item = table.getItem(new Point(event.x, event.y));
					if (item != null) {
						if (tip != null && !tip.isDisposed())
							tip.dispose();
						tip = new Shell(table.getShell(), SWT.ON_TOP | SWT.TOOL);
						tip.setLayout(new FillLayout());
						label = new Label(tip, SWT.NONE | SWT.WRAP);
						label.setForeground(table.getDisplay().getSystemColor(SWT.COLOR_INFO_FOREGROUND));
						label.setBackground(table.getDisplay().getSystemColor(SWT.COLOR_INFO_BACKGROUND));
						label.setData("_TABLEITEM", item);
						try {
							label.setText((String) fun.invoke(funObj, item));
						} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
							e.printStackTrace();
						}
						label.addListener(SWT.MouseExit, labelListener);
						label.addListener(SWT.MouseDown, labelListener);
						Point size = tip.computeSize(200, SWT.DEFAULT);
						Rectangle rect = item.getBounds(0);
						Point pt = table.toDisplay(rect.x, rect.y);
						tip.setBounds(pt.x, pt.y, size.x, size.y);
						tip.setVisible(true);
					}
				}
				}
			}
		};

		table.addListener(SWT.Dispose, tableListener);
		table.addListener(SWT.KeyDown, tableListener);
		table.addListener(SWT.MouseMove, tableListener);
		table.addListener(SWT.MouseHover, tableListener);
	}

	/**
	 * Packs the table's column to fit the content
	 * 
	 * @param table
	 */
	public static void packTable(Table table) {
		TableColumn[] columns = table.getColumns();
		for (int j = 0; j < columns.length; j++) {
			columns[j].pack();
		}
	}

	/**
	 * Adds a combo to a table cell
	 * 
	 * @param table
	 * @param item
	 * @param index
	 * @param comboItems
	 * @param selection
	 */
	public static void addCombo(Table table, final TableItem item, final int index, String[] comboItems,
			String selection) {
		final CCombo comboEditState = new CCombo(table, SWT.PUSH);

		comboEditState.setItems(comboItems);

		// prefill the combo with the current state
		comboEditState.setText(selection);
		comboEditState.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				// set the selected value into the cell behind the combo
				item.setText(index, comboEditState.getText());
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent event) {
				// Not used
			}
		});

		comboEditState.pack();
		final TableEditor editor = new TableEditor(table);
		editor.minimumWidth = comboEditState.getSize().x;
		editor.grabHorizontal = true;
		editor.horizontalAlignment = SWT.LEFT;
		editor.setEditor(comboEditState, item, index);
	}

	private static void selectMaterial(TableItem item, int idx) {
		SelectMaterialGUI matGUI = new SelectMaterialGUI(item.getParent().getShell());
		String selection = matGUI.open();
		if (selection != "" & selection != null)
			item.setText(idx, selection);
	}

}
