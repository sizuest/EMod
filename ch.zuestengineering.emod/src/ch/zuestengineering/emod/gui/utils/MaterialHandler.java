/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.gui.utils;

import java.io.File;
import java.util.Arrays;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;

import ch.zuestengineering.emod.model.material.Material;
import ch.zuestengineering.emod.model.material.MaterialType;
import ch.zuestengineering.emod.utils.PropertiesHandler;

/**
 * Handler Class for material GUIs
 * 
 * @author sizuest
 * 
 */
public class MaterialHandler {
	
	/**
	 * fill a tree element with the machine component from the DB
	 * 
	 * @param aTree tree element to fill
	 */
	public static void fillTree(Tree aTree) {
		fillTree(aTree, MaterialType.values());
	}

	/**
	 * fill a tree element with the machine component from the DB
	 * 
	 * @param aTree tree element to fill
	 * @param filter 
	 */
	public static void fillTree(Tree aTree, MaterialType[] filter) {
		String matName;

		// read material db folder from the current path
		String path = PropertiesHandler.getProperty("app.MaterialDBPathPrefix") + "/";
		File dir = new File(path);
		File[] subDirsMaterials = dir.listFiles();
		Arrays.sort(subDirsMaterials);

		// Top folders
		TreeItem[] topCats = new TreeItem[MaterialType.values().length];
		for (MaterialType mt : MaterialType.values()) {
			topCats[mt.ordinal()] = new TreeItem(aTree, SWT.NONE);
			String catName = mt.toString().toLowerCase().replace("_", " ");
			topCats[mt.ordinal()].setText(catName);
		}

		// iterate over existing Materials
		for (int i = 0; i < subDirsMaterials.length; i++) {
			if(subDirsMaterials[i].isDirectory())
				continue;
			
			if(subDirsMaterials[i].getName().endsWith("Example.xml"))
				continue;
			
			matName = subDirsMaterials[i].getName().replace("Material_", "").replace(".xml", "");
			MaterialType matType = new Material(matName).getMdlType();
			TreeItem child = new TreeItem(topCats[matType.ordinal()], SWT.NONE);
			child.setText(matName);
		}
		
		// Hide filtered materials
		for (MaterialType mt : MaterialType.values()) {			
			if(!filter(filter, mt))
				topCats[mt.ordinal()].dispose();
		}
	}
	
	private static boolean filter(MaterialType[] filter, MaterialType type) {
		for(MaterialType mt: filter)
			if(mt.equals(type))
				return true;
		
		return false;
	}
}
