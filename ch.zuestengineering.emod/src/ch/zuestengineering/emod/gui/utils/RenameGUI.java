/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.gui.utils;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import ch.zuestengineering.emod.utils.LocalizationHandler;

/**
 * Implements a simple GUI to enter / change a name
 * 
 * @author simon
 *
 */
public class RenameGUI extends Composite {

	private Text text;
	private Button buttonCancel, buttonAccept;
	private String newText = null;

	/**
	 * @param parent
	 * @param s
	 */
	public RenameGUI(Composite parent, String s) {
		super(parent, SWT.NONE);

		this.setLayout(new GridLayout(2, false));

		text = new Text(this, SWT.BORDER);
		text.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 2, 1));
		text.setText(s);
		newText = s;
		text.selectAll();

		buttonCancel = new Button(this, SWT.PUSH);
		buttonCancel.setText(LocalizationHandler.getItem("app.gui.config.cancel"));
		buttonCancel.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, true, false, 1, 1));
		buttonCancel.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				dispose();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		buttonAccept = new Button(this, SWT.PUSH);
		buttonAccept.setText(LocalizationHandler.getItem("app.gui.config.ok"));
		buttonAccept.setLayoutData(new GridData(SWT.RIGHT, SWT.TOP, true, false, 1, 1));
		buttonAccept.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				newText = text.getText();
				dispose();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		pack();
		layout();
	}

	/**
	 * @param parent
	 * @param oldName
	 * @return
	 */
	public static String getNewName(Shell parent, String oldName) {
		final Shell shell = new Shell(parent, SWT.APPLICATION_MODAL);

		RenameGUI name = new RenameGUI(shell, oldName);

		name.addDisposeListener(new DisposeListener() {

			@Override
			public void widgetDisposed(DisposeEvent e) {
				shell.dispose();
			}
		});

		shell.pack();
		shell.layout();
		shell.open();

		while (!shell.isDisposed()) {
			if (!shell.getDisplay().readAndDispatch()) {
				shell.getDisplay().sleep();
			}
		}

		return name.newText;
	}
}
