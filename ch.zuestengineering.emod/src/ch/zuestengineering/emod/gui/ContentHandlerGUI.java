/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.gui;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;

import ch.zuestengineering.emod.gui.SessionGUI;
import ch.zuestengineering.emod.gui.analysis.AnalysisGUI;
import ch.zuestengineering.emod.gui.lifecycle.LifeCycleAnalysis;
import ch.zuestengineering.emod.gui.lifecycle.LifeCycleGUI;
import ch.zuestengineering.emod.gui.modelling.ModelGraphGUI;
import ch.zuestengineering.emod.gui.scenario.InitialConditionGUI;
import ch.zuestengineering.emod.gui.scenario.ProcessGUI;
import ch.zuestengineering.emod.gui.scenario.StatesGUI;
import ch.zuestengineering.emod.gui.solver.CheckConfigGUI;
import ch.zuestengineering.emod.gui.solver.FEMExportGUI;
import ch.zuestengineering.emod.gui.solver.SolverGUI;
import ch.zuestengineering.emod.gui.solver.SolverSettingGUI;
import ch.zuestengineering.emod.licensing.LicenseActions;
import ch.zuestengineering.emod.licensing.LicenseHandler;

/**
 * @author Simon Z�st
 *
 */
public class ContentHandlerGUI {

	private static ContentHandlerGUI instance;

	private SashForm sash;

	private SessionTreeGUI sessionTree;

	private CTabFolder folder;

	private SessionGUI guiSession;
	private ModelGraphGUI guiModel;
	private LifeCycleGUI guiLC;
	private StatesGUI guiStates;
	private CheckConfigGUI guiCheckConfig;
	private SolverSettingGUI guiSolverSettings;
	private SolverGUI guiSolver;
	private InitialConditionGUI guiIC;
	private ProcessGUI guiProcess;
	private Console guiConsole;

	private AGUITab lastActiveTab = null;

	private static final int MIN_WIDTH_TAB = 300;
	private static final int MIN_WIDTH_TREE = 300;

	private ContentHandlerGUI() {
	}

	/**
	 * @return
	 */
	public static ContentHandlerGUI getInstance() {
		if (instance == null)
			instance = new ContentHandlerGUI();

		return instance;

	}

	/**
	 * @param parent
	 * @param style
	 */
	public static void initialize(Composite parent, int style) {
		getInstance().sash = new SashForm(parent, SWT.NONE);
		getInstance().sash.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		getInstance().sash.setLayout(new GridLayout(2, false));

		getInstance().sessionTree = new SessionTreeGUI(getInstance().sash, SWT.BORDER);
		getInstance().sessionTree.setLayoutData(new GridData(SWT.LEFT, SWT.FILL, false, true, 1, 1));

		getInstance().folder = new CTabFolder(getInstance().sash, style);
		getInstance().folder.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {

				AGUITab newActiveTab = null;

				// Find the new tab selected
				AGUITab[] candidates = { getInstance().guiSession, getInstance().guiModel, getInstance().guiProcess,
						getInstance().guiSolver, getInstance().guiStates, getInstance().guiIC };

				for (AGUITab a : candidates)
					if (null != a)
						if (!a.isDisposed())
							if (e.item.equals(a.getCTab())) {
								newActiveTab = a;
								break;
							}

				// handle focus change
				switchFocus(newActiveTab);

			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		getInstance().sash.getShell().addListener(SWT.Resize, new Listener() {
			@Override
			public void handleEvent(Event arg0) {
				updateSashSize();
				// updateCanvasSize();
			}
		});
	}

	/**
	 * Handles the event of a focus switch between different AGUITabs: 1. save
	 * config in the "old" tab 2. update display in the "new" tab
	 * 
	 * @param newActiveTab
	 */
	private static void switchFocus(AGUITab newActiveTab) {

		// Save the changes in the last open tab
		save(getInstance().lastActiveTab);
		//
		getInstance().lastActiveTab = newActiveTab;

		update(getInstance().lastActiveTab);

		if (newActiveTab != null)
			getTabFolder().setSelection(newActiveTab.getCTab());
	}

	/**
	 * @return
	 */
	public static CTabFolder getTabFolder() {
		return getInstance().folder;
	}

	/**
	 * Updates all active tabs
	 */
	public static void update() {
		getInstance().sessionTree.update();

		update(getInstance().guiIC);
		update(getInstance().guiModel);
		update(getInstance().guiLC);
		update(getInstance().guiProcess);
		update(getInstance().guiSolverSettings);
		update(getInstance().guiSolver);
		update(getInstance().guiStates);
		update(getInstance().guiSession);
	}

	private static void update(AGUITab tab) {
		getInstance().folder.getShell().setEnabled(false);
		if (null != tab)
			if (!tab.isDisposed())
				tab.update();
		getInstance().folder.getShell().setEnabled(true);
	}

	private static void save(AGUITab tab) {
		if (null != tab)
			if (!tab.isDisposed())
				tab.save();
	}

	/**
	 * 
	 */
	public static void updateModel() {
		update(getInstance().guiModel);
	}

	/**
	 * 
	 */
	public static void updateSession() {
		getInstance().sessionTree.update();
		update(getInstance().guiSession);
	}

	/**
	 * 
	 */
	public static void updateScenario() {
		update(getInstance().guiLC);
		update(getInstance().guiIC);
		update(getInstance().guiProcess);
		update(getInstance().guiSolver);
		update(getInstance().guiStates);
	}

	/**
	 * 
	 */
	public static void updateLC() {
		update(getInstance().guiLC);
	}

	/**
	 * 
	 */
	public static void updateProcess() {
		update(getInstance().guiProcess);
	}

	/**
	 * 
	 */
	public static void showModel() {
		if (getInstance().guiModel == null) {
			getInstance().guiModel = new ModelGraphGUI(getTabFolder(), SWT.CLOSE);
			getInstance().guiModel.showAll();
		} else if (getInstance().guiModel.isDisposed()) {
			getInstance().guiModel = new ModelGraphGUI(getTabFolder(), SWT.CLOSE);
			getInstance().guiModel.showAll();
		}

		switchFocus(getInstance().guiModel);

	}

	/**
	 * 
	 */
	public static void showSession() {
		if (getInstance().guiSession == null) {
			getInstance().guiSession = new SessionGUI(getTabFolder(), SWT.CLOSE);
		} else if (getInstance().guiSession.isDisposed()) {
			getInstance().guiSession = new SessionGUI(getTabFolder(), SWT.CLOSE);
		}

		switchFocus(getInstance().guiSession);
	}

	/**
	 * Closes all open tabs
	 */
	public static void closeAll() {
		for (CTabItem cti : getInstance().folder.getItems())
			cti.dispose();
	}

	/**
	 * @param dataFile 
	 * 
	 */
	public static void showLifeCycleChart(String dataFile) {
		LifeCycleAnalysis lcAnalysis = new LifeCycleAnalysis(getTabFolder(), dataFile);
		getTabFolder().setSelection(lcAnalysis.getCTab());

		switchFocus(lcAnalysis);
	}

	/**
	 * @param dataFile
	 */
	public static void showAnalysis(String dataFile) {
		AnalysisGUI guiAnalysis = new AnalysisGUI(getTabFolder(), dataFile);
		getTabFolder().setSelection(guiAnalysis.getCTab());

		switchFocus(guiAnalysis);
	}

	/**
	 * 
	 */
	public static void showCheckConfiguration() {
		if (getInstance().guiCheckConfig == null)
			getInstance().guiCheckConfig = new CheckConfigGUI(getTabFolder(), SWT.CLOSE);
		else if (getInstance().guiCheckConfig.isDisposed())
			getInstance().guiCheckConfig = new CheckConfigGUI(getTabFolder(), SWT.CLOSE);

		getInstance().guiCheckConfig.setEnabled(LicenseHandler.can(LicenseActions.RUNSIMULATION));

		switchFocus(getInstance().guiCheckConfig);
	}

	/**
	 * 
	 */
	public static void showSimulationSettings() {
		if (getInstance().guiSolverSettings == null)
			getInstance().guiSolverSettings = new SolverSettingGUI(getTabFolder(), SWT.CLOSE);
		else if (getInstance().guiSolverSettings.isDisposed())
			getInstance().guiSolverSettings = new SolverSettingGUI(getTabFolder(), SWT.CLOSE);

		getInstance().guiSolverSettings.setEnabled(LicenseHandler.can(LicenseActions.RUNSIMULATION));

		switchFocus(getInstance().guiSolverSettings);
	}

	/**
	 * 
	 */
	public static void showSolver() {
		if (getInstance().guiSolver == null)
			getInstance().guiSolver = new SolverGUI(getTabFolder(), SWT.CLOSE);
		else if (getInstance().guiSolver.isDisposed())
			getInstance().guiSolver = new SolverGUI(getTabFolder(), SWT.CLOSE);

		getInstance().guiSolver.setEnabled(LicenseHandler.can(LicenseActions.RUNSIMULATION));

		switchFocus(getInstance().guiSolver);
	}

	/**
	 * 
	 */
	public static void showBCConfig() {
		if (getInstance().guiIC == null)
			getInstance().guiIC = new InitialConditionGUI(getTabFolder(), SWT.CLOSE);
		else if (getInstance().guiIC.isDisposed())
			getInstance().guiIC = new InitialConditionGUI(getTabFolder(), SWT.CLOSE);

		getInstance().guiIC.setEnabled(LicenseHandler.can(LicenseActions.EDIT_SCENARIO));

		switchFocus(getInstance().guiIC);
	}

	/**
	 * 
	 */
	public static void showStatesConfig() {
		if (getInstance().guiStates == null)
			getInstance().guiStates = new StatesGUI(getTabFolder(), SWT.CLOSE);
		else if (getInstance().guiStates.isDisposed())
			getInstance().guiStates = new StatesGUI(getTabFolder(), SWT.CLOSE);

		getInstance().guiStates.setEnabled(LicenseHandler.can(LicenseActions.EDIT_SCENARIO));

		switchFocus(getInstance().guiStates);
	}

	/**
	 * @param dataFile
	 */
	public static void showFEMExport(String dataFile) {
		FEMExportGUI guiFEM = new FEMExportGUI(getTabFolder(), dataFile);
		getTabFolder().setSelection(guiFEM.getCTab());

		switchFocus(guiFEM);
	}

	/**
	 * 
	 */
	public static void showProcessConfig() {
		if (getInstance().guiProcess == null)
			getInstance().guiProcess = new ProcessGUI(getTabFolder(), SWT.CLOSE);
		else if (getInstance().guiProcess.isDisposed())
			getInstance().guiProcess = new ProcessGUI(getTabFolder(), SWT.CLOSE);

		getInstance().guiProcess.setEnabled(LicenseHandler.can(LicenseActions.EDIT_SCENARIO));

		switchFocus(getInstance().guiProcess);
	}

	/**
	 * 
	 */
	public static void showConsole() {
		if (getInstance().guiConsole == null)
			getInstance().guiConsole = new Console(getTabFolder(), SWT.CLOSE);
		else if (getInstance().guiConsole.isDisposed())
			getInstance().guiConsole = new Console(getTabFolder(), SWT.CLOSE);

		switchFocus(getInstance().guiConsole);
	}

	/**
	 * Update the size of the shash
	 */
	private static void updateSashSize() {
		if (getInstance().sash.isDisposed())
			return;

		int width = getInstance().sash.getShell().getClientArea().width;
		int[] weights = getInstance().sash.getWeights();

		if (width >= MIN_WIDTH_TAB + MIN_WIDTH_TREE) {
			weights[0] = 1000000 * MIN_WIDTH_TREE / width;
			weights[1] = 1000000 - weights[1];
		} else {
			weights[1] = 1000000 * MIN_WIDTH_TAB / (MIN_WIDTH_TAB + MIN_WIDTH_TREE);
			weights[0] = 1000000 * MIN_WIDTH_TREE / (MIN_WIDTH_TAB + MIN_WIDTH_TREE);
		}

		getInstance().sash.setWeights(weights);
	}

	/**
	 * @param b
	 */
	public static void setSimulationRunning(boolean b) {
		getInstance().sessionTree.setSimulationRunning(b);
		getInstance().sessionTree.update();

		if (getInstance().guiSolver == null)
			return;
		if (getInstance().guiSolver.isDisposed())
			return;

		getInstance().guiSolver.setEnabled(!b);
		getInstance().guiSolver.getCTab().setShowClose(!b);
	}

	/**
	 * 
	 */
	public static void updateSessionTree() {
		getInstance().sessionTree.update();
	}

	/**
	 * 
	 */
	public static void showLCConfig() {
		if (getInstance().guiLC == null)
			getInstance().guiLC = new LifeCycleGUI(getTabFolder(), SWT.CLOSE);
		else if (getInstance().guiLC.isDisposed())
			getInstance().guiLC = new LifeCycleGUI(getTabFolder(), SWT.CLOSE);

		getInstance().guiLC.setEnabled(LicenseHandler.can(LicenseActions.EDIT_SCENARIO));

		switchFocus(getInstance().guiLC);

	}
}
