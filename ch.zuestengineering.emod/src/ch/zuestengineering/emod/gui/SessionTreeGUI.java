/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.gui;

import java.io.File;
import java.util.ArrayList;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;

import ch.zuestengineering.emod.ConfigurationChecker;
import ch.zuestengineering.emod.EModFileHandling;
import ch.zuestengineering.emod.EModSession;
import ch.zuestengineering.emod.gui.icons.IconHandler;
import ch.zuestengineering.emod.gui.utils.ConfirmGUI;
import ch.zuestengineering.emod.gui.utils.RenameGUI;
import ch.zuestengineering.emod.simulation.ConfigState;
import ch.zuestengineering.emod.simulation.EModSimulationMain;
import ch.zuestengineering.emod.utils.Defines;
import ch.zuestengineering.emod.utils.LocalizationHandler;

/**
 * Implements a graphical representation of the current session as a tree
 * 
 * @author Simon Z�st
 *
 */
public class SessionTreeGUI extends Composite {

	private Tree sessionTree;

	private Label title;

	/* Main Topics */
	private TreeItem itemGeneral, itemConfiguration, itemSolver, itemAnalysis;
	private TreeItem itemMachine, itemScenario, itemProcess, itemCheckCfg, itemSimSettings, itemSimulation, itemResults,
			itemFEM, itemLC, itemLCConfig;
	private Font fontBold, fontItalic;
	private ArrayList<TreeItem> modelItems, analysisItems, femItems, simItems, bcItems, lcItems, statesItems, processItems;

	private Menu menu;

	/**
	 * @param parent
	 * @param style
	 */
	public SessionTreeGUI(Composite parent, int style) {
		super(parent, style);

		this.setLayout(new GridLayout(1, false));

		title = new Label(this, SWT.NONE);
		title.setText(LocalizationHandler.getItem("app.gui.session.tree.title"));
		title.setFont(fontBold);

		sessionTree = new Tree(this, SWT.V_SCROLL | SWT.FULL_SELECTION | SWT.BORDER);
		sessionTree.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));

		itemGeneral = new TreeItem(sessionTree, SWT.NONE);
		itemGeneral.setText(LocalizationHandler.getItem("app.gui.session.tree.general"));
		FontData font = itemGeneral.getFont().getFontData()[0];
		font.setStyle(SWT.BOLD);
		fontBold = new Font(getDisplay(), font);
		font.setStyle(SWT.ITALIC);
		fontItalic = new Font(getDisplay(), font);
		itemGeneral.setFont(fontBold);

		itemConfiguration = new TreeItem(sessionTree, SWT.NONE);
		itemConfiguration.setText(LocalizationHandler.getItem("app.gui.session.tree.configuration"));
		itemConfiguration.setFont(fontBold);

		itemSolver = new TreeItem(sessionTree, SWT.NONE);
		itemSolver.setText(LocalizationHandler.getItem("app.gui.session.tree.solving"));
		itemSolver.setFont(fontBold);

		itemAnalysis = new TreeItem(sessionTree, SWT.NONE);
		itemAnalysis.setText(LocalizationHandler.getItem("app.gui.session.tree.analysis"));
		itemAnalysis.setFont(fontBold);

		itemMachine = new TreeItem(itemConfiguration, SWT.NONE);
		itemMachine.setText(LocalizationHandler.getItem("app.gui.session.tree.model"));

		itemScenario = new TreeItem(itemConfiguration, SWT.NONE);
		itemScenario.setText(LocalizationHandler.getItem("app.gui.session.tree.scenario"));

		itemCheckCfg = new TreeItem(itemSolver, SWT.NONE);
		itemCheckCfg.setText(LocalizationHandler.getItem("app.gui.session.tree.checkconfig"));

		itemSimSettings = new TreeItem(itemSolver, SWT.NONE);
		itemSimSettings.setText(LocalizationHandler.getItem("app.gui.session.tree.simulationconfig"));

		itemSimulation = new TreeItem(itemSolver, SWT.NONE);
		itemSimulation.setText(LocalizationHandler.getItem("app.gui.session.tree.simulation"));

		itemResults = new TreeItem(itemAnalysis, SWT.NONE);
		itemResults.setText(LocalizationHandler.getItem("app.gui.session.tree.results"));

		itemFEM = new TreeItem(itemAnalysis, SWT.NONE);
		itemFEM.setText(LocalizationHandler.getItem("app.gui.session.tree.fem"));

		itemLC = new TreeItem(itemAnalysis, SWT.NONE);
		itemLC.setText(LocalizationHandler.getItem("app.gui.session.tree.lc"));

		sessionTree.addMouseListener(new MouseListener() {

			@Override
			public void mouseUp(MouseEvent e) {
			}

			@Override
			public void mouseDown(MouseEvent e) {
				// Adapt the menu
				setMenu();
			}

			@Override
			public void mouseDoubleClick(MouseEvent e) {

				TreeItem selection = sessionTree.getSelection()[0];

				if (itemGeneral.equals(selection)) {
					ContentHandlerGUI.showSession();
				}
				// Test for selected models
				else if (modelItems.contains(selection)) {
					if (selection.getText().equals(EModSession.getMachineConfig()))
						ContentHandlerGUI.showModel();
					else {
						EModSession.setMachineConfig(selection.getText());
						ContentHandlerGUI.updateModel();
					}
				}
				// Test for selected sim cfg
				else if (simItems.contains(selection)) {
					if (!selection.getText().equals(EModSession.getSimulationConfig())) {
						EModSession.setSimulationConfig(selection.getText());
						ContentHandlerGUI.updateScenario();
					}
				}
				// Test for selected lc cfg
				else if (itemLCConfig.equals(selection)) {
					ContentHandlerGUI.showLCConfig();
				}
				// Test for selected bc cfg
				else if (bcItems.contains(selection)) {
					ContentHandlerGUI.showBCConfig();
				}
				// Test for selected states cfg
				else if (statesItems.contains(selection)) {
					ContentHandlerGUI.showStatesConfig();
				}
				// Test for selected process cfg
				else if (processItems.contains(selection)) {
					if (selection.getText().equals(EModSession.getProcessName()))
						ContentHandlerGUI.showProcessConfig();
					else {
						EModSession.setProcessName(selection.getText());
						ContentHandlerGUI.updateScenario();
					}
				}
				// Test for selected test config
				else if (itemCheckCfg.equals(selection)) {
					ContentHandlerGUI.showCheckConfiguration();
				}

				// Test for selected sim settings
				else if (itemSimSettings.equals(selection)) {
					ContentHandlerGUI.showSimulationSettings();
				}
				// Test for selected solver
				else if (itemSimulation.equals(selection)) {
					ContentHandlerGUI.showSolver();
				}
				// Test for selected Analysis
				else if (analysisItems.contains(selection)) {
					ContentHandlerGUI.showAnalysis(EModSession.getResultFolderPath() + selection.getText()+".dat");
				}
				// Test for selected FEM
				else if (femItems.contains(selection)) {
					ContentHandlerGUI.showFEMExport(EModSession.getResultFolderPath() + selection.getText()+"_FEM.dat");
				}
				// Test for LC Analysis
				else if (lcItems.contains(selection)) {
					ContentHandlerGUI.showLifeCycleChart(EModSession.getResultFolderPath() + selection.getText()+"_LC.dat");
				}

				update();
			}
		});

		this.layout();

		update();

		createMenu();

	}

	@Override
	public void update() {
		if (super.isDisposed())
			return;

		super.update();
		updateModel();
		updateSimulation();
		updateAnalysis();

		EModStatusBarGUI.updateMachineInfo();

		unfoldTree(sessionTree);
	}

	private void updateAnalysis() {
		// Remove present entries
		for (TreeItem ti : itemResults.getItems())
			ti.dispose();
		for (TreeItem ti : itemFEM.getItems())
			ti.dispose();
		for (TreeItem ti : itemLC.getItems())
			ti.dispose();

		/* Simulation Results */
		analysisItems = new ArrayList<TreeItem>();

		for (String m : EModSession.getMachineConfigs()) {
			for (String s : EModSession.getSimulationConfigs()) {
				for (String p : EModSession.getProcessNames(s)) {
					ConfigState status = ConfigurationChecker.checkResults(m, s, p).getStatus();
					if (status != ConfigState.ERROR) {
						TreeItem item = new TreeItem(itemResults, SWT.NONE);
						item.setImage(IconHandler.getIcon(getDisplay(), status, "overview_obj"));
						item.setText(EModSession.getResultFilePath(m, s, p).replace(
								EModSession.getRootPath() + File.separator + Defines.RESULTDIR + File.separator, "").replace(".dat", ""));

						analysisItems.add(item);
					}
				}
			}
		}

		/* FEM Output */
		femItems = new ArrayList<TreeItem>();

		for (String m : EModSession.getMachineConfigs()) {
			for (String s : EModSession.getSimulationConfigs()) {
				for (String p : EModSession.getProcessNames(s)) {
					ConfigState status = ConfigurationChecker.checkFEM(m, s, p).getStatus();
					if (status != ConfigState.ERROR) {
						TreeItem item = new TreeItem(itemFEM, SWT.NONE);
						item.setImage(IconHandler.getIcon(getDisplay(), status, "overview_obj"));
						item.setText(EModSession.getFEMExportFilePath(m, s, p).replace(
								EModSession.getRootPath() + File.separator + Defines.RESULTDIR + File.separator, "").replace("_FEM.dat", ""));
						femItems.add(item);
					}
				}
			}
		}
		
		/* LC Results */
		lcItems = new ArrayList<TreeItem>();

		for (String m : EModSession.getMachineConfigs()) {
			for (String s : EModSession.getSimulationConfigs()) {
				for (String p : EModSession.getProcessNames(s)) {
					ConfigState status = ConfigurationChecker.checkLC(m, s, p).getStatus();
					if (status != ConfigState.ERROR) {
						TreeItem item = new TreeItem(itemLC, SWT.NONE);
						item.setImage(IconHandler.getIcon(getDisplay(), status, "overview_obj"));
						item.setText(EModSession.getLCResultFilePath(m, s, p).replace(
								EModSession.getRootPath() + File.separator + Defines.RESULTDIR + File.separator, "").replace("_LC.dat", ""));

						lcItems.add(item);
					}
				}
			}
		}

	}

	/**
	 * 
	 */
	public void updateModel() {
		// Remove present entries
		for (TreeItem ti : itemMachine.getItems())
			ti.dispose();

		modelItems = new ArrayList<TreeItem>();

		// Add icon of current config state
		// itemMachine.setImage(
		// IconHandler.getIcon(getDisplay(),
		// ConfigurationChecker.checkMachineConfig().getStatus(), "blank"));

		for (String s : EModSession.getMachineConfigs()) {
			TreeItem item = new TreeItem(itemMachine, SWT.NONE);
			if (s.equals(EModSession.getMachineConfig()))
				item.setFont(fontItalic);
			item.setImage(IconHandler.getIcon(getDisplay(), "keygroups_obj"));
			item.setText(s);
			modelItems.add(item);
		}
	}

	/**
	 * 
	 */
	public void updateSimulation() {
		// Remove present entries
		for (TreeItem ti : itemScenario.getItems())
			ti.dispose();

		simItems     = new ArrayList<>();
		statesItems  = new ArrayList<>();
		bcItems      = new ArrayList<>();
		processItems = new ArrayList<>();

		// Add icon of current config state
		itemScenario.setImage(
				IconHandler.getIcon(getDisplay(), ConfigurationChecker.checkSimulationConfig().getStatus(), "blank"));

		for (String s : EModSession.getSimulationConfigs()) {
			TreeItem item = new TreeItem(itemScenario, SWT.NONE);
			if (s.equals(EModSession.getSimulationConfig())) {
				item.setImage(IconHandler.getIcon(getDisplay(), "prj_obj"));

				TreeItem itemStates, itemInitialConditions;

				itemLCConfig = new TreeItem(item, SWT.NONE);
				itemLCConfig.setText(LocalizationHandler.getItem("app.gui.session.tree.lc"));
				itemLCConfig.setImage(IconHandler.getIcon(getDisplay(),
						ConfigurationChecker.checkInitialConditions().getStatus(), "datasheet"));

				itemInitialConditions = new TreeItem(item, SWT.NONE);
				itemInitialConditions.setText(LocalizationHandler.getItem("app.gui.sim.initialconditions.title"));
				itemInitialConditions.setImage(IconHandler.getIcon(getDisplay(),
						ConfigurationChecker.checkInitialConditions().getStatus(), "datasheet"));

				itemStates = new TreeItem(item, SWT.NONE);
				itemStates.setText(LocalizationHandler.getItem("app.gui.sim.machinestatesequence.title"));
				itemStates.setImage(IconHandler.getIcon(getDisplay(), ConfigurationChecker.checkStateMap().getStatus(),
						"datasheet"));

				itemProcess = new TreeItem(item, SWT.NONE);
				itemProcess.setText(LocalizationHandler.getItem("app.gui.sim.inputs.title"));
				itemProcess.setImage(
						IconHandler.getIcon(getDisplay(), ConfigurationChecker.checkProcess().getStatus(), "blank"));

				for (String p : EModSession.getProcessNames(s)) {
					TreeItem itemProc = new TreeItem(itemProcess, SWT.NONE);
					if (p.equals(EModSession.getProcessName())) {
						itemProc.setFont(fontItalic);
					}
					itemProc.setText(p);
					itemProc.setImage(IconHandler.getIcon(getDisplay(), "datasheet"));
					processItems.add(itemProc);
				}

				bcItems.add(itemInitialConditions);
				statesItems.add(itemStates);

			} else
				item.setImage(IconHandler.getIcon(getDisplay(), "cprj_obj"));
			item.setText(s);

			simItems.add(item);
		}
	}

	private void unfoldTree(Tree item) {

		for (TreeItem ti : item.getItems())
			unfoldTree(ti);

	}

	private void unfoldTree(TreeItem item) {
		item.setExpanded(true);

		for (TreeItem ti : item.getItems())
			unfoldTree(ti);

	}

	/**
	 * @param b
	 */
	public void setSimulationRunning(boolean b) {
		if (b)
			itemSimulation.setImage(IconHandler.getIcon(getDisplay(), "waiting"));
		else
			itemSimulation.setImage(IconHandler.getIcon(getDisplay(), "blank"));
	}

	private void createMenu() {
		menu = new Menu(getShell(), SWT.POP_UP);
		sessionTree.setMenu(menu);
	}

	private void setMenu() {

		// Remove all entries
		for (MenuItem mi : menu.getItems())
			mi.dispose();

		if (sessionTree.getSelectionCount() == 0)
			return;

		final TreeItem selection = sessionTree.getSelection()[0];

		if (itemMachine.equals(selection)) {
			MenuItem itemNew;

			itemNew = new MenuItem(menu, SWT.PUSH);
			itemNew.setText("Create new model");
			itemNew.setImage(IconHandler.getIcon(getDisplay(), "new_con"));
			itemNew.addSelectionListener(new SelectionListener() {

				@Override
				public void widgetSelected(SelectionEvent e) {
					newMachineCfg();
				}

				@Override
				public void widgetDefaultSelected(SelectionEvent e) {
				}
			});
		} else if (itemScenario.equals(selection)) {
			MenuItem itemNew;

			itemNew = new MenuItem(menu, SWT.PUSH);
			itemNew.setText("Create new scenario");
			itemNew.setImage(IconHandler.getIcon(getDisplay(), "new_con"));
			itemNew.addSelectionListener(new SelectionListener() {

				@Override
				public void widgetSelected(SelectionEvent e) {
					newSimulationCfg();
				}

				@Override
				public void widgetDefaultSelected(SelectionEvent e) {
				}
			});
		} else if (itemProcess.equals(selection)) {
			MenuItem itemNew;

			itemNew = new MenuItem(menu, SWT.PUSH);
			itemNew.setText("Create new process");
			itemNew.setImage(IconHandler.getIcon(getDisplay(), "new_con"));
			itemNew.addSelectionListener(new SelectionListener() {

				@Override
				public void widgetSelected(SelectionEvent e) {
					newProcess();
				}

				@Override
				public void widgetDefaultSelected(SelectionEvent e) {
				}
			});
		} else if (modelItems.contains(selection)) {
			MenuItem itemRename, itemDelete, itemActive;

			itemActive = new MenuItem(menu, SWT.CHECK);
			itemActive.setText("Active");
			itemActive.setSelection(EModSession.getMachineConfig().equals(selection.getText()));
			itemActive.addSelectionListener(new SelectionListener() {

				@Override
				public void widgetSelected(SelectionEvent e) {
					EModSession.setMachineName(selection.getText());
					ContentHandlerGUI.updateModel();
				}

				@Override
				public void widgetDefaultSelected(SelectionEvent e) {
				}
			});

			itemRename = new MenuItem(menu, SWT.PUSH);
			itemRename.setText("Rename");
			itemRename.setImage(IconHandler.getIcon(getDisplay(), "wordassist_co"));
			itemRename.addSelectionListener(new SelectionListener() {

				@Override
				public void widgetSelected(SelectionEvent e) {
					renameMachineCfg(selection.getText());
				}

				@Override
				public void widgetDefaultSelected(SelectionEvent e) {
				}
			});

			itemDelete = new MenuItem(menu, SWT.PUSH);
			itemDelete.setText("Delete");
			itemDelete.setImage(IconHandler.getIcon(getDisplay(), "delete_obj"));
			itemDelete.addSelectionListener(new SelectionListener() {

				@Override
				public void widgetSelected(SelectionEvent e) {
					deleteMachineCfg(selection.getText());
				}

				@Override
				public void widgetDefaultSelected(SelectionEvent e) {
				}
			});
		} else if (simItems.contains(selection)) {
			MenuItem itemRename, itemDelete, itemActive;

			itemActive = new MenuItem(menu, SWT.CHECK);
			itemActive.setText("Active");
			itemActive.setSelection(EModSession.getSimulationConfig().equals(selection.getText()));
			itemActive.addSelectionListener(new SelectionListener() {

				@Override
				public void widgetSelected(SelectionEvent e) {
					EModSession.setSimulationConfig(selection.getText());
					ContentHandlerGUI.updateScenario();
					update();
				}

				@Override
				public void widgetDefaultSelected(SelectionEvent e) {
				}
			});

			itemRename = new MenuItem(menu, SWT.PUSH);
			itemRename.setText("Rename");
			itemRename.setImage(IconHandler.getIcon(getDisplay(), "wordassist_co"));
			itemRename.addSelectionListener(new SelectionListener() {

				@Override
				public void widgetSelected(SelectionEvent e) {
					renameSimulationCfg(selection.getText());
				}

				@Override
				public void widgetDefaultSelected(SelectionEvent e) {
				}
			});

			itemDelete = new MenuItem(menu, SWT.PUSH);
			itemDelete.setText("Delete");
			itemDelete.setImage(IconHandler.getIcon(getDisplay(), "delete_obj"));
			itemDelete.addSelectionListener(new SelectionListener() {

				@Override
				public void widgetSelected(SelectionEvent e) {
					deleteSimulationCfg(selection.getText());
				}

				@Override
				public void widgetDefaultSelected(SelectionEvent e) {
				}
			});
		} else if (processItems.contains(selection)) {
			MenuItem itemRename, itemDelete, itemActive;

			itemActive = new MenuItem(menu, SWT.CHECK);
			itemActive.setText("Active");
			itemActive.setSelection(EModSession.getProcessName().equals(selection.getText()));
			itemActive.addSelectionListener(new SelectionListener() {

				@Override
				public void widgetSelected(SelectionEvent e) {
					EModSession.setProcessName(selection.getText());
					ContentHandlerGUI.updateProcess();
					update();
				}

				@Override
				public void widgetDefaultSelected(SelectionEvent e) {
				}
			});

			itemRename = new MenuItem(menu, SWT.PUSH);
			itemRename.setText("Rename");
			itemRename.setImage(IconHandler.getIcon(getDisplay(), "wordassist_co"));
			itemRename.addSelectionListener(new SelectionListener() {

				@Override
				public void widgetSelected(SelectionEvent e) {
					renameProcess(selection.getText());
				}

				@Override
				public void widgetDefaultSelected(SelectionEvent e) {
				}
			});

			itemDelete = new MenuItem(menu, SWT.PUSH);
			itemDelete.setText("Delete");
			itemDelete.setImage(IconHandler.getIcon(getDisplay(), "delete_obj"));
			itemDelete.addSelectionListener(new SelectionListener() {

				@Override
				public void widgetSelected(SelectionEvent e) {
					deleteProcess(selection.getText());
				}

				@Override
				public void widgetDefaultSelected(SelectionEvent e) {
				}
			});
		} else if (analysisItems.contains(selection) | femItems.contains(selection)) {
			MenuItem itemExport, itemDelete;

			itemExport = new MenuItem(menu, SWT.PUSH);
			itemExport.setText("Export");
			itemExport.setImage(IconHandler.getIcon(getDisplay(), "exportpref_obj"));
			itemExport.addSelectionListener(new SelectionListener() {

				@Override
				public void widgetSelected(SelectionEvent e) {
					FileDialog fd = new FileDialog(getShell(), SWT.SAVE);
					fd.setText("Export");
					fd.setFilterPath("C:/");
					String[] filterExt = { "*.csv" };
					fd.setFilterExtensions(filterExt);
					String selected = fd.open();
					if (selected == null) {
						return;
					}
					EModFileHandling.exportResultFile(selection.getText(), selected);
				}

				@Override
				public void widgetDefaultSelected(SelectionEvent e) {
				}
			});

			itemDelete = new MenuItem(menu, SWT.PUSH);
			itemDelete.setText("Delete");
			itemDelete.setImage(IconHandler.getIcon(getDisplay(), "delete_obj"));
			itemDelete.addSelectionListener(new SelectionListener() {

				@Override
				public void widgetSelected(SelectionEvent e) {
					deleteResults(selection.getText());
				}

				@Override
				public void widgetDefaultSelected(SelectionEvent e) {
				}
			});
		} else if (lcItems.contains(selection)) {
			MenuItem itemDelete, itemUpdate;
			
			itemUpdate = new MenuItem(menu, SWT.PUSH);
			itemUpdate.setText("Update");
			itemUpdate.setImage(IconHandler.getIcon(getDisplay(), "updates_obj"));
			itemUpdate.addSelectionListener(new SelectionListener() {

				@Override
				public void widgetSelected(SelectionEvent e) {
					EModSimulationMain.computeLCA();
				}

				@Override
				public void widgetDefaultSelected(SelectionEvent e) {
				}
			});

			itemDelete = new MenuItem(menu, SWT.PUSH);
			itemDelete.setText("Delete");
			itemDelete.setImage(IconHandler.getIcon(getDisplay(), "delete_obj"));
			itemDelete.addSelectionListener(new SelectionListener() {

				@Override
				public void widgetSelected(SelectionEvent e) {
					deleteResults(selection.getText());
				}

				@Override
				public void widgetDefaultSelected(SelectionEvent e) {
				}
			});
		}

	}

	private void newMachineCfg() {
		String name = RenameGUI.getNewName(getShell(), "Machine");

		if (name == null)
			return;

		EModSession.newMachineConfig(name);

		update();
	}

	private void newSimulationCfg() {
		String name = RenameGUI.getNewName(getShell(), "Scenario");

		if (name == null)
			return;

		EModSession.newSimulationConfig(name, "Process");

		update();
	}

	private void newProcess() {
		String name = RenameGUI.getNewName(getShell(), "Process");

		if (name == null)
			return;

		EModSession.newProcess(name);

		update();
	}

	private void deleteMachineCfg(String name) {
		if (ConfirmGUI.getNewName(getShell(), "Delete '" + name + "'?"))
			EModSession.removeMachineConfig(name);

		update();
	}

	private void deleteSimulationCfg(String name) {
		if (ConfirmGUI.getNewName(getShell(), "Delete '" + name + "'?"))
			EModSession.removeSimulationConfig(name);

		update();
	}

	private void deleteProcess(String name) {
		if (ConfirmGUI.getNewName(getShell(), "Delete '" + name + "'?"))
			EModSession.removeProcess(name);

		update();
	}

	private void deleteResults(String name) {
		if (ConfirmGUI.getNewName(getShell(), "Delete '" + name + "'?"))
			EModSession.removeResults(name);

		update();
	}

	private void renameMachineCfg(String oldName) {
		String name = RenameGUI.getNewName(getShell(), oldName);

		if (name == null)
			return;

		EModSession.renameMachineConfig(oldName, name);

		update();
	}

	private void renameSimulationCfg(String oldName) {
		String name = RenameGUI.getNewName(getShell(), oldName);

		if (name == null)
			return;

		EModSession.renameSimulationConfig(oldName, name);

		update();
	}

	private void renameProcess(String oldName) {
		String name = RenameGUI.getNewName(getShell(), oldName);

		if (name == null)
			return;

		EModSession.renameProcess(EModSession.getSimulationConfig(), oldName, name);

		update();
	}

}
