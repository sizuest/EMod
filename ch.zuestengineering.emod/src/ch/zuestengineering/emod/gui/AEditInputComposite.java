/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.gui;

import org.eclipse.swt.widgets.Composite;

import ch.zuestengineering.emod.gui.utils.ShowButtons;
import ch.zuestengineering.emod.simulation.ASimulationControl;

/**
 * Generic composite to edit simulation inputs
 * 
 * @author sizuest
 *
 */
public abstract class AEditInputComposite extends AConfigGUI {

	protected Composite parent;
	protected ASimulationControl sc;

	/**
	 * @param parent
	 * @param style
	 * @param sc
	 */
	public AEditInputComposite(Composite parent, int style, ASimulationControl sc) {
		super(parent, style, ShowButtons.NONE);

		this.parent = parent;
		this.sc = sc;

		init();
	}

	/**
	 * Init the gui
	 */
	public abstract void init();

	@Override
	public abstract void save();

	@Override
	public abstract void reset();

}
