/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.gui.icons;

import org.eclipse.swt.graphics.Device;
import org.eclipse.swt.graphics.Image;

import ch.zuestengineering.emod.simulation.ConfigState;

/**
 * Implements the handling of icon images
 * 
 * @author Simon Z�st
 *
 */
public class IconHandler {

	/**
	 * returns the image with the given name and state
	 * 
	 * @param device
	 * @param name
	 * @param activated
	 * @return
	 */
	public static Image getIcon(Device device, String name, boolean activated) {
		Image image;

		String path = "src/resources/";

		if (activated)
			path += "activated/";
		else
			path += "deactivated/";

		path += name + ".gif";

		try {
			image = new Image(device, path);
		} catch (Exception e) {
			e.printStackTrace();
			image = null;
		}

		return image;

	}

	/**
	 * returns the image with the given name
	 * 
	 * @param device
	 * @param name
	 * @return
	 */
	public static Image getIcon(Device device, String name) {
		return getIcon(device, name, true);
	}

	/**
	 * returns the image for the given config state
	 * 
	 * @param device
	 * @param state
	 * @param okIco
	 * @return
	 */
	public static Image getIcon(Device device, ConfigState state, String okIco) {
		switch (state) {
		case OK:
			return getIcon(device, okIco);
		case WARNING:
			return getIcon(device, "warning");
		case ERROR:
			return getIcon(device, "error");
		default:
			return getIcon(device, "error");
		}
	}
}
