/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.gui;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Tree;

import ch.zuestengineering.emod.gui.utils.MachineComponentHandler;
import ch.zuestengineering.emod.utils.LocalizationHandler;

/**
 * Machine component selection GUI
 * 
 * @author sizuest
 * 
 */
public class SelectMachineComponentGUI extends Dialog {
	private String filter = "";
	private String input;
	private boolean alowMultiSelect = true;

	// tree to list all the components
	private Tree treeComponentDBView;

	/**
	 * SelectMachineComponentGUI
	 * 
	 * @param parent
	 */
	public SelectMachineComponentGUI(Shell parent) {
		this(parent, SWT.DIALOG_TRIM | SWT.APPLICATION_MODAL);
	}

	/**
	 * SelectMachineComponentGUI
	 * 
	 * @param parent
	 * @param alowMultiSelect
	 */
	public SelectMachineComponentGUI(Shell parent, boolean alowMultiSelect) {
		this(parent, SWT.DIALOG_TRIM | SWT.APPLICATION_MODAL);
		this.alowMultiSelect = alowMultiSelect;
	}

	/**
	 * @param parent
	 * @param style
	 */
	public SelectMachineComponentGUI(Shell parent, int style) {
		super(parent, style);
	}

	/**
	 * Open with no filter
	 * 
	 * @return
	 */
	public String open() {
		return open("");
	}

	/**
	 * Open with active filter
	 * 
	 * @param filter
	 * @return
	 */
	public String open(String filter) {
		Shell shell = new Shell(getParent(), getStyle());
		shell.setText(LocalizationHandler.getItem("app.gui.compdb.editcomp"));
		shell.setSize(400, 600);
		shell.setLayout(new GridLayout(2, false));

		this.filter = filter;

		createContents(shell);
		shell.pack();
		shell.layout();
		shell.open();

		shell.setSize(400, 600);

		Display display = getParent().getDisplay();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		// Return the entered value, or null
		return input;

	}

	private void createContents(final Shell shell) {
		// create tree element and fill it with the components from the DB
		if (alowMultiSelect)
			treeComponentDBView = new Tree(shell, SWT.MULTI | SWT.BORDER | SWT.V_SCROLL);
		else
			treeComponentDBView = new Tree(shell, SWT.SINGLE | SWT.BORDER | SWT.V_SCROLL);

		treeComponentDBView.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 2, 1));
		MachineComponentHandler.fillMachineComponentTree(filter, treeComponentDBView);

		/* Close Button */
		final Button closeComponentButton = new Button(shell, SWT.PUSH);
		closeComponentButton.setText("Close");
		closeComponentButton.setLayoutData(new GridData(SWT.RIGHT, SWT.TOP, false, false, 1, 1));
		closeComponentButton.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				input = "";
				shell.close();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent event) {
				// Not used
			}
		});

		/* Select Button */
		final Button selectComponentButton = new Button(shell, SWT.PUSH);
		selectComponentButton.setText("OK");
		selectComponentButton.setLayoutData(new GridData(SWT.RIGHT, SWT.TOP, false, false, 1, 1));
		selectComponentButton.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				input = getSelectionToString();
				shell.close();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent event) {
				// Not used
			}
		});

		shell.setDefaultButton(selectComponentButton);
	}

	private String getSelectionToString() {
		int n = treeComponentDBView.getSelectionCount();
		String out = "";
		boolean fullName = false;

		if (treeComponentDBView.getItemCount() > 1)
			fullName = true;

		for (int i = 0; i < n; i++) {
			if (fullName)
				out += treeComponentDBView.getSelection()[i].getParentItem().getText() + "_";

			if (i + 1 == n)
				out += treeComponentDBView.getSelection()[i].getText();
			else
				out += treeComponentDBView.getSelection()[i].getText() + ", ";
		}

		return out;
	}
}
