/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.gui.licensing;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import ch.zuestengineering.emod.licensing.LicenseHandler;

/**
 * SWT visualization of the license status
 * 
 * @author Simon Z�st
 *
 */
public class LicenseStatusGUI extends Composite {

	private Label labelText;
	private Label labelOwner;

	/**
	 * @param parent
	 */
	public LicenseStatusGUI(Composite parent) {
		super(parent, SWT.NONE);

		this.setLayout(new GridLayout(2, false));

		labelText = new Label(this, SWT.BORDER);
		labelText.setText("***");
		labelOwner = new Label(this, SWT.NONE);
		labelOwner.setText("***");

		this.layout();
		this.pack();

		update();
	}

	@Override
	public void update() {
		labelText.setText(LicenseHandler.getLicenseType());
		labelOwner.setText(LicenseHandler.getLicenseOwner());
		if (LicenseHandler.getLicenseValidity()) {
			labelText.setBackground(new Color(labelText.getDisplay(), 0, 255, 0));
			labelText.setForeground(new Color(labelText.getDisplay(), 0, 0, 0));
		} else {
			labelText.setBackground(new Color(labelText.getDisplay(), 255, 0, 0));
			labelText.setForeground(new Color(labelText.getDisplay(), 255, 255, 255));
		}
	}

}
