/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.gui.material;

import java.io.IOException;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Arrays;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.TableEditor;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.ControlListener;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;

import ch.zuestengineering.emod.gui.AConfigGUI;
import ch.zuestengineering.emod.gui.SelectMaterialGUI;
import ch.zuestengineering.emod.gui.utils.ShellUtils;
import ch.zuestengineering.emod.gui.utils.ShowButtons;
import ch.zuestengineering.emod.gui.utils.TableUtils;
import ch.zuestengineering.emod.lca.typedef.RawMaterialType;
import ch.zuestengineering.emod.model.material.Material;
import ch.zuestengineering.emod.model.material.MaterialType;
import ch.zuestengineering.emod.model.parameters.PhysicalValue;
import ch.zuestengineering.emod.utils.LocalizationHandler;
import ch.zuestengineering.emod.utils.MaterialConfigReader;
import ch.zuestengineering.emod.utils.PropertiesHandler;

/**
 * GUI to edit material properties
 * 
 * @author sizuest
 *
 */
public class EditMaterialGUI extends AConfigGUI {

	private MaterialConfigReader material;
	private Combo comboMdlType, comboMatClass;
	private Table tableMaterialProperties;

	/**
	 * @param parent
	 * @param style
	 * @param materialName
	 */
	public EditMaterialGUI(Composite parent, int style, String materialName) {
		super(parent, style, ShowButtons.ALL);

		try {
			new Material(materialName);
			material = new MaterialConfigReader("Material", materialName);
		} catch (Exception e) {
			e.printStackTrace();
		}

		this.getContent().setLayout(new GridLayout(2, false));
		
		// Text "Material type" of the material
		Label textModelType = new Label(this.getContent(), SWT.NONE);
		textModelType.setText(LocalizationHandler.getItem("app.gui.matdb.modtype")+":");
		textModelType.setLayoutData(new GridData(SWT.BEGINNING, SWT.CENTER, false, false, 1, 1));

		comboMdlType = getModelTypeCombo(this.getContent());
		comboMdlType.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		comboMdlType.setEnabled(false);
		
		// Text "eco type" of the material
		Label textMaterialClass = new Label(this.getContent(), SWT.NONE);
		textMaterialClass.setText(LocalizationHandler.getItem("app.gui.matdb.mattype")+":");
		textMaterialClass.setLayoutData(new GridData(SWT.BEGINNING, SWT.CENTER, false, false, 1, 1));

		comboMatClass = getMaterialClassCombo(this.getContent());
		comboMatClass.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		//comboMatClass.setEnabled(false);

		tableMaterialProperties = new Table(getContent(), SWT.FILL | SWT.SINGLE | SWT.V_SCROLL | SWT.BORDER);
		tableMaterialProperties.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 2, 1));
		tableMaterialProperties.setLinesVisible(true);
		tableMaterialProperties.setHeaderVisible(true);

		String[] titles = { LocalizationHandler.getItem("app.gui.compdb.property"),
				LocalizationHandler.getItem("app.gui.compdb.value"), LocalizationHandler.getItem("app.gui.compdb.unit"),
				LocalizationHandler.getItem("app.gui.compdb.description") };
		for (int i = 0; i < titles.length; i++) {
			TableColumn column = new TableColumn(tableMaterialProperties, SWT.NULL);
			column.setText(titles[i]);
		}

		try {
			TableUtils.addCellEditor(tableMaterialProperties, this, new int[] { 1 });
		} catch (Exception e) {
			e.printStackTrace();
		}

		update();
	}
	
	/**
	 * Creates a combo with the model types
	 * 
	 * getModelTypeCombo
	 * @param shell
	 * @return
	 */
	public static Combo getModelTypeCombo(Composite shell) {
		Combo comboModelType = new Combo(shell, SWT.BORDER);
		String[] modTypes = new String[MaterialType.values().length];
		for (int i = 0; i < MaterialType.values().length; i++)
			modTypes[i] = MaterialType.values()[i].toString();
		comboModelType.setItems(modTypes);
		comboModelType.select(0);
		
		return comboModelType;
	}
	
	/**
	 * Creates a combo with the material types
	 * 
	 * getMaterialTypeCombo
	 * @param shell
	 * @return
	 */
	public static Combo getMaterialClassCombo(Composite shell) {
		Combo comboMaterialType = new Combo(shell, SWT.BORDER);
		String[] matTypes = new String[RawMaterialType.values().length];
		for (int i = 0; i < RawMaterialType.values().length; i++)
			matTypes[i] = RawMaterialType.values()[i].toString();
		Arrays.sort(matTypes);
		comboMaterialType.setItems(matTypes);
		comboMaterialType.select(0);
		
		return comboMaterialType;
	}

	/**
	 * GUI in a new shell
	 * 
	 * @param parent
	 * @param type
	 */
	public static void editMaterialGUI(final Shell parent, String type) {

		final Shell shell = new Shell(parent, SWT.TITLE | SWT.APPLICATION_MODAL | SWT.CLOSE | SWT.MAX | SWT.RESIZE);
		shell.setText("Edit Material: " + type);
		shell.setLayout(new GridLayout(1, true));
		shell.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		final EditMaterialGUI gui = new EditMaterialGUI(shell, SWT.NONE, type);

		shell.setImages(parent.getImages());

		shell.pack();

		shell.layout();
		shell.redraw();
		shell.open();

		shell.setSize(shell.computeSize(SWT.DEFAULT, SWT.DEFAULT));

		shell.addControlListener(new ControlListener() {

			@Override
			public void controlResized(ControlEvent e) {
				gui.layout();
			}

			@Override
			public void controlMoved(ControlEvent e) {
				gui.layout();
			}
		});

		shell.addDisposeListener(new DisposeListener() {

			@Override
			public void widgetDisposed(DisposeEvent e) {
				parent.setEnabled(true);
			}
		});

		gui.addDisposeListener(new DisposeListener() {
			@Override
			public void widgetDisposed(DisposeEvent e) {
				shell.dispose();
			}
		});

		ShellUtils.putToCenter(shell, parent);
	}

	/**
	 * Component Edit GUI for creating a new Material
	 * 
	 * @param parent
	 */
	public static void newMaterialGUI(final Shell parent) {
		final Shell shell = new Shell(parent, SWT.TITLE | SWT.SYSTEM_MODAL | SWT.CLOSE | SWT.MAX);
		shell.setText(LocalizationHandler.getItem("app.gui.matdb.newmat"));
		shell.setLayout(new GridLayout(2, false));

		// Text "Material type" of the material
		Label textModelType = new Label(shell, SWT.NONE);
		textModelType.setText(LocalizationHandler.getItem("app.gui.matdb.modtype"));
		textModelType.setLayoutData(new GridData(SWT.BEGINNING, SWT.CENTER, true, true, 1, 1));

		final Combo comboModelType = getModelTypeCombo(shell);
		comboModelType.setLayoutData(new GridData(SWT.BEGINNING, SWT.CENTER, true, true, 1, 1));

		// Text "Material name" of the material
		Label textMaterialName = new Label(shell, SWT.NONE);
		textMaterialName.setText(LocalizationHandler.getItem("app.gui.matdb.matname"));
		textMaterialName.setLayoutData(new GridData(SWT.BEGINNING, SWT.CENTER, true, true, 1, 1));

		// Textfield to let the user enter the desired name of the material
		final Text textMaterialNameValue = new Text(shell, SWT.BORDER);
		textMaterialNameValue.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true, 1, 1));

		// button to continue
		Button buttonContinue = new Button(shell, SWT.NONE);
		buttonContinue.setText(LocalizationHandler.getItem("app.gui.continue"));
		// selection Listener for the button, actions when button is pressed
		buttonContinue.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				String stringMaterialNameValue = textMaterialNameValue.getText();
				MaterialType materialType = MaterialType.valueOf(comboModelType.getText());

				Path from = Paths.get(PropertiesHandler.getProperty("app.MaterialDBPathPrefix") + "/" + "Material_"+materialType+"-Example.xml");
				Path to = Paths.get(PropertiesHandler.getProperty("app.MaterialDBPathPrefix") + "/" + "Material_" + stringMaterialNameValue + ".xml");
				// overwrite existing file, if exists
				CopyOption[] options = new CopyOption[] { StandardCopyOption.COPY_ATTRIBUTES };
				try {
					if(!to.toFile().exists())
						Files.copy(from, to, options);
					new Material(materialType, stringMaterialNameValue);
					
					shell.close();

					// open the edit ComponentEditGUI with the newly created
					// component file
					EditMaterialGUI.editMaterialGUI(parent, stringMaterialNameValue);

				} catch (Exception e) {
					e.printStackTrace();
					
					shell.close();
				}

				
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent event) {
				// Not used
			}
		});
		buttonContinue.setLayoutData(new GridData(SWT.END, SWT.CENTER, false, true, 2, 1));

		shell.pack();

		// open the new shell
		shell.open();

		ShellUtils.putToCenter(shell, parent);
	}

	@Override
	public void update() {
		
		try {
			comboMdlType.setText(material.getString("ModelType"));
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		try {
			comboMatClass.setText(material.getString("MaterialClass"));
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		
		
		tableMaterialProperties.setItemCount(0);

		for (String key : material.getKeys()) {
			
			if(key.equals("ModelType"))
				continue;
			
			if(key.equals("MaterialClass"))
				continue;
			
			try {
				/* SPECIAL CASE: Material */
				if (key.equals("Solids")) {
					TableItem item = new TableItem(tableMaterialProperties, SWT.NONE);
					item.setText(0, key);
					item.setText(1, material.getString(key));
					final Button selectMaterialButton = new Button(tableMaterialProperties, SWT.PUSH);
					TableEditor editorButton = new TableEditor(tableMaterialProperties);
					selectMaterialButton.setText("...");
					selectMaterialButton.setLayoutData(new GridData(SWT.FILL, SWT.TOP, false, true, 1, 1));
					selectMaterialButton.addSelectionListener(new SelectionListener() {
						@Override
						public void widgetSelected(SelectionEvent event) {
							openMaterialSelectGUI(item, new MaterialType[] {MaterialType.SOLID});
							wasEdited();
						}
	
						@Override
						public void widgetDefaultSelected(SelectionEvent event) {
							// Not used
						}
					});
					selectMaterialButton.pack();
					editorButton.minimumWidth = selectMaterialButton.getSize().x;
					editorButton.horizontalAlignment = SWT.LEFT;
					editorButton.setEditor(selectMaterialButton, item, 2);
					
					continue;
				}
				
				if (key.equals("Fluids")) {
					TableItem item = new TableItem(tableMaterialProperties, SWT.NONE);
					item.setText(0, key);
					item.setText(1, material.getString(key));
					final Button selectMaterialButton = new Button(tableMaterialProperties, SWT.PUSH);
					TableEditor editorButton = new TableEditor(tableMaterialProperties);
					selectMaterialButton.setText("...");
					selectMaterialButton.setLayoutData(new GridData(SWT.FILL, SWT.TOP, false, true, 1, 1));
					selectMaterialButton.addSelectionListener(new SelectionListener() {
						@Override
						public void widgetSelected(SelectionEvent event) {
							openMaterialSelectGUI(item, new MaterialType[] {MaterialType.FLUID});
							wasEdited();
						}
	
						@Override
						public void widgetDefaultSelected(SelectionEvent event) {
							// Not used
						}
					});
					selectMaterialButton.pack();
					editorButton.minimumWidth = selectMaterialButton.getSize().x;
					editorButton.horizontalAlignment = SWT.LEFT;
					editorButton.setEditor(selectMaterialButton, item, 2);
					
					continue;
				}

			
				String value, unit;
				try {
					PhysicalValue pvalue = material.getPhysicalValue(key);
					value = pvalue.valuesToString();
					unit = pvalue.getUnit().toString();
					TableItem item = new TableItem(tableMaterialProperties, SWT.NONE);
					item.setText(0, key);
					item.setText(1, value);
					item.setText(2, unit);
				} catch (Exception e2) {
					value = material.getString(key);
					unit = "";
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		TableColumn[] columns = tableMaterialProperties.getColumns();
		for (int j = 0; j < columns.length; j++) {
			columns[j].pack();
		}
	}

	@Override
	public void save() {
		
		material.setValue("MaterialClass", comboMatClass.getText());

		for (TableItem ti : tableMaterialProperties.getItems()) {
			material.setValue(ti.getText(0), ti.getText(1) + " " + ti.getText(2));
		}

		try {
			material.saveValues();
		} catch (IOException e) {
			e.printStackTrace();
		}

		update();

	}

	@Override
	public void reset() {
		try {
			material = new MaterialConfigReader(material.getPath());
		} catch (Exception e) {
			e.printStackTrace();
		}

		update();
	}
	
	/**
	 * Let the user choose from the material list and write it to the table item
	 * 
	 * @param item
	 */
	private void openMaterialSelectGUI(TableItem item, MaterialType[] filter) {
		SelectMaterialGUI matGUI = new SelectMaterialGUI(this.getShell(), true, filter);
		String selection = matGUI.open();
		if (selection != "" & selection != null)
			item.setText(1, selection);
	}
}
