/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.gui;

import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.StyleRange;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

/**
 * Implements a simple console
 * 
 * @author Simon Z�st
 *
 */
public class Console extends AGUITab {

	private StyledText console;

	protected Console(Composite parent, int i) {
		super(parent, i);
		init();
	}

	protected Console(CTabFolder parent, int i) {
		super(parent, i, "Console");
		init();
	}

	@Override
	public void init() {
		super.setLayout(new GridLayout(1, true));
		console = new StyledText(this, SWT.V_SCROLL | SWT.H_SCROLL);
		console.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		console.setEditable(false);
		console.setFont(new Font(getDisplay(), "Mono", 8, SWT.NORMAL));

		// Pipe Console
		final PrintStream backupSystemOutStream = System.out;
		final PrintStream backupSystemErrStream = System.err;
		System.setOut(new PrintStream(backupSystemOutStream) {
			@Override
			public void print(final String s) {
				getDisplay().asyncExec(new Runnable() {
					@Override
					public void run() {
						String msg = "[" + (new SimpleDateFormat("yyyy.MM.dd HH:mm:ss")).format((new Date()))
								+ "] INFO:  " + s + "\n";
						console.append(msg);

						StyleRange[] sr = new StyleRange[1];
						sr[0] = new StyleRange(console.getCharCount() - msg.length(), msg.length(),
								getDisplay().getSystemColor(SWT.COLOR_BLACK),
								getDisplay().getSystemColor(SWT.COLOR_WHITE));
						console.replaceStyleRanges(sr[0].start, sr[0].length, sr);
						console.getVerticalBar().setSelection(console.getVerticalBar().getMaximum());
					}
				});
				super.print(s);
			}
		});
		System.setErr(new PrintStream(backupSystemErrStream) {
			@Override
			public void print(final String s) {
				getDisplay().asyncExec(new Runnable() {
					@Override
					public void run() {
						String msg = "[" + (new SimpleDateFormat("yyyy.MM.dd HH:mm:ss")).format((new Date()))
								+ "] ERROR: " + s + "\n";
						console.append(msg);

						StyleRange[] sr = new StyleRange[1];
						sr[0] = new StyleRange(console.getCharCount() - msg.length(), msg.length(),
								getDisplay().getSystemColor(SWT.COLOR_RED),
								getDisplay().getSystemColor(SWT.COLOR_WHITE));
						console.replaceStyleRanges(sr[0].start, sr[0].length, sr);
						console.getVerticalBar().setSelection(console.getVerticalBar().getMaximum());
					}
				});
				super.print(s);
			}
		});

		this.layout();
	}

	@Override
	public void update() {
		layout();
	}

	@Override
	public void save() {
	}

	@Override
	public void wasEdited() {
	}

}
