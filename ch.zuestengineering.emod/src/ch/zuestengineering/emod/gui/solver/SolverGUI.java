/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.gui.solver;

import java.util.logging.Logger;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;

import ch.zuestengineering.emod.gui.AGUITab;
import ch.zuestengineering.emod.gui.ContentHandlerGUI;
import ch.zuestengineering.emod.gui.EModStatusBarGUI;
import ch.zuestengineering.emod.licensing.LicenseActions;
import ch.zuestengineering.emod.licensing.LicenseHandler;
import ch.zuestengineering.emod.simulation.EModSimulationMain;
import ch.zuestengineering.emod.simulation.EModSimulationSimulationThread;
import ch.zuestengineering.emod.utils.LocalizationHandler;

/**
 * @author manick
 * 
 */

public class SolverGUI extends AGUITab {

	private static Logger logger = Logger.getLogger(SolverGUI.class.getName());

	private StyledText textLog;
	private Button buttonRunSim;

	private boolean simulationWasRunning = false;

	/**
	 * @param parent
	 */
	public SolverGUI(Composite parent) {
		super(parent, SWT.NONE);
		init();
	}

	/**
	 * @param parent
	 * @param style
	 */
	public SolverGUI(CTabFolder parent, int style) {
		super(parent, style, LocalizationHandler.getItem("app.gui.tabs.sim"));

		init();
	}

	@Override
	public void init() {

		this.setLayout(new GridLayout(1, false));

		Group groupRun = new Group(this, SWT.NONE);
		groupRun.setText(LocalizationHandler.getItem("app.gui.sim.general.runsim"));
		groupRun.setLayout(new GridLayout(2, false));
		groupRun.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));

		buttonRunSim = new Button(groupRun, SWT.NONE);
		buttonRunSim.setText("Run");
		buttonRunSim.setEnabled(true);
		buttonRunSim.setLayoutData(new GridData(SWT.RIGHT, SWT.FILL, false, false, 2, 1));
		buttonRunSim.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				buttonRunSim.setEnabled(false);
				simulationWasRunning = true;
				ContentHandlerGUI.setSimulationRunning(true);
				startSimulation();
				periodicUpdate();

			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {/* Not used */
			}
		});

		textLog = new StyledText(this, SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL);
		textLog.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		textLog.setEditable(false);
		textLog.setFont(new Font(getDisplay(), "Consolas", 10, SWT.NONE));

		this.layout();

		/* License dependent visibility */
		buttonRunSim.setEnabled(LicenseHandler.can(LicenseActions.RUNSIMULATION));
	}

	@Override
	public void update() {

		/* Print messages */
		for (String s : SimulationLogger.getMessages())
			textLog.append(s);

		textLog.setTopIndex(textLog.getLineCount() - 1);

		/* Update simulation information */
		if (EModSimulationMain.getRunningStatus()) {
			EModStatusBarGUI.getProgressBar().setText(EModSimulationMain.getMessage());
			EModStatusBarGUI.getProgressBar().updateProgressbar(EModSimulationMain.getProgress(),
					!EModSimulationMain.getForcedStop());
			EModSimulationMain.setForcedStop(EModStatusBarGUI.getProgressBar().getCancelStatus());
		} else if (simulationWasRunning) {
			buttonRunSim.setEnabled(true);
			EModStatusBarGUI.getProgressBar().reset();
			simulationWasRunning = false;
			ContentHandlerGUI.setSimulationRunning(false);
		}

		this.redraw();
	}

	private void startSimulation() {
		textLog.setText("");
		logger.info("Simulation start: initialization ...");

		// EModSimulationRun.EModSimRun();
		(new EModSimulationSimulationThread()).start();

		logger.info("Simulation start: running ...");
	}

	private void periodicUpdate() {
		Thread updateThread = new Thread() {
			@Override
			public void run() {
				do {

					try {
						Thread.sleep(5000);
					} catch (InterruptedException ee) {
						ee.printStackTrace();
					}

					getDisplay().syncExec(new Runnable() {

						@Override
						public void run() {
							update();
						}
					});

				} while (simulationWasRunning);
			}
		};
		// background thread
		updateThread.setDaemon(true);
		updateThread.start();
	}

	@Override
	public void save() {
	}

	@Override
	public void wasEdited() {
		save();
	}

	@Override
	public void setEnabled(boolean b) {
		super.setEnabled(b);
	}

}
