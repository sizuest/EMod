/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.gui.solver;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;

import ch.zuestengineering.emod.gui.AGUITab;
import ch.zuestengineering.emod.gui.ContentHandlerGUI;
import ch.zuestengineering.emod.gui.EModStatusBarGUI;
import ch.zuestengineering.emod.licensing.LicenseActions;
import ch.zuestengineering.emod.licensing.LicenseHandler;
import ch.zuestengineering.emod.simulation.EModSimulationMain;
import ch.zuestengineering.emod.utils.LocalizationHandler;

/**
 * @author manick
 * 
 */

public class SolverSettingGUI extends AGUITab {

	private boolean simulationWasRunning = false;

	private EditSimTimeStepGUI guiTimeStep;
	private EditFluidSolverParametersGUI guiFluidSolver;
	private EditOutputGUI guiOutputCtrl;

	/**
	 * @param parent
	 */
	public SolverSettingGUI(Composite parent) {
		super(parent, SWT.NONE);
		init();
	}

	/**
	 * @param parent
	 * @param style
	 */
	public SolverSettingGUI(CTabFolder parent, int style) {
		super(parent, style, LocalizationHandler.getItem("app.gui.tabs.sim"));

		init();
	}

	@Override
	public void init() {

		this.setLayout(new GridLayout(1, false));

		Group groupIntegrator = new Group(this, SWT.NONE);
		groupIntegrator.setLayout(new GridLayout(1, false));
		groupIntegrator.setText(LocalizationHandler.getItem("app.gui.sim.integrator.titel"));
		groupIntegrator.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));

		Group groupFluid = new Group(this, SWT.NONE);
		groupFluid.setLayout(new GridLayout(1, false));
		groupFluid.setText(LocalizationHandler.getItem("app.gui.sim.fc.titel"));
		groupFluid.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));

		Group groupOutput = new Group(this, SWT.NONE);
		groupOutput.setLayout(new GridLayout(1, false));
		groupOutput.setText(LocalizationHandler.getItem("app.gui.sim.output.title"));
		groupOutput.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));

		/* Create sub controls */
		guiTimeStep = new EditSimTimeStepGUI(groupIntegrator, SWT.NONE);
		guiTimeStep.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		guiFluidSolver = new EditFluidSolverParametersGUI(groupFluid, SWT.NONE);
		guiFluidSolver.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		guiOutputCtrl = new EditOutputGUI(groupOutput, SWT.NONE);
		guiOutputCtrl.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));

		this.layout();

		/* License dependent visibility */
		guiFluidSolver.setEnabled(LicenseHandler.can(LicenseActions.EDIT_SIMCFG));
		guiOutputCtrl.setEnabled(LicenseHandler.can(LicenseActions.EDIT_SIMCFG));
		guiTimeStep.setEnabled(LicenseHandler.can(LicenseActions.EDIT_SIMCFG));
	}

	@Override
	public void update() {

		/* Update simulation information */
		if (EModSimulationMain.getRunningStatus()) {
			EModStatusBarGUI.getProgressBar().setText(EModSimulationMain.getMessage());
			EModStatusBarGUI.getProgressBar().updateProgressbar(EModSimulationMain.getProgress(),
					!EModSimulationMain.getForcedStop());
			EModSimulationMain.setForcedStop(EModStatusBarGUI.getProgressBar().getCancelStatus());
		} else if (simulationWasRunning) {
			EModStatusBarGUI.getProgressBar().reset();
			simulationWasRunning = false;
			ContentHandlerGUI.setSimulationRunning(false);
		}

		guiFluidSolver.update();
		guiOutputCtrl.update();
		guiTimeStep.update();

		this.redraw();
	}

	@Override
	public void save() {
		guiTimeStep.save();
		guiFluidSolver.save();
		guiOutputCtrl.save();
	}

	@Override
	public void wasEdited() {
		save();
	}

	@Override
	public void setEnabled(boolean b) {
		super.setEnabled(b);
		guiFluidSolver.setEnabled(b);
		guiOutputCtrl.setEnabled(b);
		guiTimeStep.setEnabled(b);
	}

}
