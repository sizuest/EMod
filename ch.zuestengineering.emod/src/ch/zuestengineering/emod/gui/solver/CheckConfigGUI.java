/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.gui.solver;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import ch.zuestengineering.emod.gui.AGUITab;
import ch.zuestengineering.emod.gui.utils.ConfigCheckResultGUI;
import ch.zuestengineering.emod.utils.LocalizationHandler;

/**
 * @author manick
 * 
 */

public class CheckConfigGUI extends AGUITab {

	private ConfigCheckResultGUI checkResults;

	/**
	 * @param parent
	 */
	public CheckConfigGUI(Composite parent) {
		super(parent, SWT.NONE);
		init();
	}

	/**
	 * @param parent
	 * @param style
	 */
	public CheckConfigGUI(CTabFolder parent, int style) {
		super(parent, style, LocalizationHandler.getItem("app.gui.tabs.sim"));

		init();
	}

	@Override
	public void init() {

		this.setLayout(new GridLayout(1, false));

		checkResults = new ConfigCheckResultGUI(this, SWT.NONE);
		checkResults.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));

		this.layout();

		checkResults.update();
	}

	@Override
	public void update() {
		this.redraw();
		checkResults.runChecks();
	}

	@Override
	public void save() {
	}

	@Override
	public void wasEdited() {
		save();
	}

	@Override
	public void setEnabled(boolean b) {
		super.setEnabled(b);
	}

}
