/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.gui.solver;

import java.util.ArrayList;

/**
 * Implements a logger for a simulation Run
 * 
 * @author sizuest
 *
 */
public class SimulationLogger {

	private ArrayList<String> messages = new ArrayList<>();

	private static SimulationLogger logger;
	
	private static int CHARSPERLINE = 75;

	private SimulationLogger() {
	}

	/**
	 * Returns the instance
	 * 
	 * @return
	 */
	public static SimulationLogger getInstance() {
		if (null == logger)
			logger = new SimulationLogger();

		return logger;
	}

	/**
	 * @param text
	 */
	public static void info(String text) {
		getInstance().textToFile(text);
		getInstance().textToConsoles(text + "\n");
	}

	/**
	 * @param text
	 */
	public static void progress(String text) {
		getInstance().textToFile(text + " ...");
		getInstance().textToConsoles(text + " ...");
	}

	/**
	 */
	public static void done() {
		info(flushRight(' ', 5)+" done");
	}
	
	/**
	 */
	public static void failed() {
		info(flushRight(' ', 7)+" failed");
	}
	
	private static String flushRight(char c, int spaceRight) {
		int nChars = CHARSPERLINE - spaceRight;
		
		if(getInstance().messages.size()>0)
			nChars -= (getInstance().messages.get(getInstance().messages.size()-1).length() % CHARSPERLINE);
		
		String spaceString  = "";
		for(int i=0; i<nChars; i++)
			spaceString += c;
		
		return spaceString;
	}
	

	/**
	 * @param text
	 */
	public static void title(String text) {
		String divider = "\n"+line('=')+"\n";
		info(divider + text.toUpperCase() + divider);
	}

	/**
	 * @param text
	 */
	public static void subtitle(String text) {
		String divider = "\n"+line('-')+"\n";
		info(divider + text.toUpperCase() + divider);
	}
	
	private static String line(char c) {
		String line = "";
		for(int i=0; i<CHARSPERLINE; i++)
			line += c;
		
		return line;
	}

	private void textToFile(String text) {
		// TODO
	}

	private void textToConsoles(String text) {
		messages.add(text);
	}

	/**
	 * @return
	 */
	public static ArrayList<String> getMessages() {
		ArrayList<String> ret = new ArrayList<>();

		for (String m : getInstance().messages)
			ret.add(m);

		getInstance().messages.clear();

		return ret;
	}

}
