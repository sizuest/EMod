/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.gui.help;

import org.eclipse.swt.SWT;
import org.eclipse.swt.browser.Browser;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.ControlListener;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Shell;

import ch.zuestengineering.emod.gui.utils.ShellUtils;
import ch.zuestengineering.emod.help.AHelpTopic;
import ch.zuestengineering.emod.help.Help;

import java.awt.Desktop;
import java.net.URI;
import java.util.logging.Logger;

/**
 * Implements a GUI to display model information
 * 
 * @author simon
 *
 */
public class HelpBrowserGUI extends Composite {

	protected Browser browser;
	protected AHelpTopic topic;
	protected static Logger logger = Logger.getLogger(HelpBrowserGUI.class.getName());

	/**
	 * @param parent
	 * @param style
	 * @param topic
	 */
	public HelpBrowserGUI(Composite parent, int style, AHelpTopic topic) {
		super(parent, style);

		this.topic = topic;

		init();
	}

	private void init() {
		setLayout(new GridLayout(1, false));

		browser = new Browser(this, SWT.BORDER);
		browser.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		browser.layout();
		browser.setUrl(topic.getHTMLFullPath().replace("\\", "/"));

		update();
	}

	/**
	 * Opens a new help browser for the given model type. If available, the system's
	 * browser will be used
	 * 
	 * @param parent
	 * @param path
	 */
	public static void show(final Shell parent, final String path) {
		AHelpTopic topic = Help.getTopic(path);

		// If the system browser is accessible, use it!
		if (Desktop.isDesktopSupported()) {
			try {
				String u = topic.getHTMLFullPath();
				u = u.replace("\\", "/");
				u = u.replace(" ", "%20");
				URI uri = new URI(u);
				Desktop.getDesktop().browse(uri);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		// If not, create a browser
		else
			modelDescriptionGUI(parent, topic);
	}

	/**
	 * @param parent
	 * @param topic
	 * @return
	 */
	private static Shell modelDescriptionGUI(final Shell parent, final AHelpTopic topic) {
		final Shell shell = new Shell(parent.getDisplay());
		shell.setText(topic.getTitle());
		shell.setLayout(new GridLayout(1, true));

		final HelpBrowserGUI gui = new HelpBrowserGUI(shell, SWT.NONE, topic);
		gui.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		shell.setImages(parent.getImages());

		shell.pack();

		shell.layout();
		shell.redraw();
		shell.open();
		shell.setSize(600, 700);

		shell.setImages(parent.getShell().getImages());

		shell.addControlListener(new ControlListener() {

			@Override
			public void controlResized(ControlEvent e) {
				gui.layout();
			}

			@Override
			public void controlMoved(ControlEvent e) {
				gui.layout();
			}
		});

		shell.addDisposeListener(new DisposeListener() {

			@Override
			public void widgetDisposed(DisposeEvent e) {
				parent.setEnabled(true);
			}
		});

		gui.addDisposeListener(new DisposeListener() {
			@Override
			public void widgetDisposed(DisposeEvent e) {
				try {
					shell.dispose();
				} catch (Exception e2) {
					logger.severe("HelpGUI: Failed to dispose shell");
				}
			}
		});

		ShellUtils.putToCenter(shell, parent);

		return shell;
	}

}
