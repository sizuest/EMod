/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.gui.help;

import org.eclipse.swt.SWT;
import org.eclipse.swt.browser.Browser;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Tree;

/**
 * Implementation of the help GUI
 * 
 * @author simon
 *
 */
public class HelpGUI extends Composite {

	SashForm sash;
	Browser browser;
	Tree content;

	/**
	 * @param parent
	 * @param style
	 */
	public HelpGUI(Composite parent, int style) {
		super(parent, style);

		init();
	}

	private void init() {
		setLayout(new FillLayout());

		sash = new SashForm(this, SWT.NONE);
		sash.setLayout(new GridLayout(2, false));

		content = new Tree(sash, SWT.BORDER);
		content.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		fillContent(content);

		browser = new Browser(sash, SWT.NONE);
		browser.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
	}

	private void fillContent(Tree tree) {
		// TODO
	}

}
