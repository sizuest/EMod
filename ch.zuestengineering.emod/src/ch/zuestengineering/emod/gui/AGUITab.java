/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.gui;

import java.util.logging.Logger;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.widgets.Composite;

/**
 * abstract class for guis, containing configuration steps.
 * 
 * @author sizuest
 * 
 */
public abstract class AGUITab extends Composite {

	private CTabItem tab;
	protected Logger logger = Logger.getLogger(AGUITab.class.getName());

	protected AGUITab(Composite parent, int i) {
		super(parent, i);
	}

	protected AGUITab(CTabFolder parent, int i, String title) {
		super(parent, SWT.NONE);

		tab = new CTabItem(parent, i);
		tab.setText(title);
		tab.setControl(this);

		tab.addDisposeListener(new DisposeListener() {

			@Override
			public void widgetDisposed(DisposeEvent e) {
				save();
				dispose();
			}
		});

		this.layout();
		parent.layout();
	}

	/**
	 * @return
	 */
	public CTabItem getCTab() {
		return tab;
	}

	/**
	 * Init the gui tab
	 */
	abstract public void init();

	@Override
	abstract public void update();

	/**
	 * 
	 */
	abstract public void save();

	/**
	 * 
	 */
	abstract public void wasEdited();

}
