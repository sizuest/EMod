/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.gui;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import ch.zuestengineering.emod.EModSession;
import ch.zuestengineering.emod.Machine;
import ch.zuestengineering.emod.utils.LocalizationHandler;
import ch.zuestengineering.emod.utils.PropertiesHandler;

/**
 * Selection of the machine to be displayed
 * 
 * @author sizuest
 *
 */
public class EModStartupGUI {
	private static Shell shell;

	// Combo to let the user select the MachineConfig
	private static Combo comboMachineName;
	// name of the MachineConfig from the last use of EMod from app.config
	private static String machineName;

	// Combo to let the user select the SimConfig
	private static Combo comboMachineConfigName;

	private static Combo comboSimConfigName;

	private static Combo comboProcName;
	// name of the SimConfig from the last use of EMod from app.config
	private static String machineConfigName;

	private static String simConfigName;

	private static String procName;

	/**
	 * window to load existing machine configuration
	 */
	protected static Shell loadMachineGUI(Shell parent) {

		PropertiesHandler.setProperty("app.MachineDataPathPrefix", "Machines");

		// new shell in style modal to prevent user from skipping window
		shell = new Shell(parent, SWT.APPLICATION_MODAL);
		shell.setText("EMod startup");
		shell.setLayout(new GridLayout(2, true));

		// get machineName and machineConfigName from app.config file
		machineName = EModSession.getMachineName();
		machineConfigName = EModSession.getMachineConfig();
		simConfigName = EModSession.getSimulationConfig();
		procName = EModSession.getProcessName();

		// text load machine config
		Label textLoadMachConfig = new Label(shell, SWT.TRANSPARENT);
		textLoadMachConfig.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		textLoadMachConfig.setText(LocalizationHandler.getItem("app.gui.startup.machinename"));

		// combo for the user to select the desired MachConfig
		comboMachineName = new Combo(shell, SWT.NONE);
		comboMachineName.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		// possible items of the combo are all Machines present in the folder
		// Machines
		String path = PropertiesHandler.getProperty("app.MachineDataPathPrefix") + "/";
		File dir = new File(path);
		String[] items = dir.list();
		Arrays.sort(items);
		comboMachineName.setItems(items);

		// prefill the last used MachineConfig as default value into the combo
		comboMachineName.setText(machineName);

		// add selection listener to the combo
		// the possible selection of the comboMachineConfigName has to change
		// according to the selected value
		comboMachineName.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				// disable comboMachineConfigName to prevent argument null for
				// updatecomboMachineConfigName
				comboMachineConfigName.setEnabled(false);
				comboSimConfigName.setEnabled(false);
				comboProcName.setEnabled(false);

				// get Text of chosen MachineConfig
				String stringMachConfig = comboMachineName.getText();
				String stringSimConfig = comboSimConfigName.getText();
				// update comboMachineConfigName according to the Selection of
				// MachineConfig
				updatecomboMachineConfigName(stringMachConfig);
				updatecomboSimConfigName(stringMachConfig);
				updatecomboProcName(stringMachConfig, stringSimConfig);

				// enable comboMachineConfigName after update
				comboMachineConfigName.setEnabled(true);
				comboSimConfigName.setEnabled(true);
				comboProcName.setEnabled(true);
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent event) {
				// Not used
			}
		});

		// text load machine config
		Label textLoadMachineConfig = new Label(shell, SWT.TRANSPARENT);
		textLoadMachineConfig.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		textLoadMachineConfig.setText(LocalizationHandler.getItem("app.gui.startup.machineconfigname"));

		// combo for the user to select the desired SimConfig
		comboMachineConfigName = new Combo(shell, SWT.NONE);
		comboMachineConfigName.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		// possible items of the combo are all SimConfig that match to the
		// selected MachConfig
		updatecomboMachineConfigName(machineName);
		// prefill the last used SimConfig as default value into the combo
		comboMachineConfigName.setText(machineConfigName);

		// text load simulation config
		Label textLoadSimConfig = new Label(shell, SWT.TRANSPARENT);
		textLoadSimConfig.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		textLoadSimConfig.setText(LocalizationHandler.getItem("app.gui.startup.simulationconfigname"));

		// combo for the user to select the desired SimConfig
		comboSimConfigName = new Combo(shell, SWT.NONE);
		comboSimConfigName.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		// possible items of the combo are all SimConfig that match to the
		// selected MachConfig
		updatecomboSimConfigName(machineName);
		// prefill the last used SimConfig as default value into the combo
		comboSimConfigName.setText(simConfigName);

		// add selection listener to the combo
		comboSimConfigName.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				// disable comboMachineConfigName to prevent argument null for
				// updatecomboMachineConfigName
				comboProcName.setEnabled(false);

				// get Text of chosen MachineConfig
				String stringMachConfig = comboMachineName.getText();
				String stringSimConfig = comboSimConfigName.getText();
				// update comboMachineConfigName according to the Selection of
				// MachineConfig
				updatecomboProcName(stringMachConfig, stringSimConfig);

				// enable comboMachineConfigName after update
				comboProcName.setEnabled(true);
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent event) {
				// Not used
			}
		});

		// text load process config
		Label textLoadProcConfig = new Label(shell, SWT.TRANSPARENT);
		textLoadProcConfig.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		textLoadProcConfig.setText(LocalizationHandler.getItem("app.gui.startup.processconfigname"));

		// combo for the user to select the desired process
		comboProcName = new Combo(shell, SWT.NONE);
		comboProcName.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		// possible items of the combo are all process that match to the
		// selected MachConfig and SimConfig
		updatecomboProcName(machineName, simConfigName);
		// prefill the last used process as default value into the combo
		comboProcName.setText(procName);

		// Button to create a new machine
		Button buttonNew = new Button(shell, SWT.NONE);
		buttonNew.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));
		buttonNew.setText(LocalizationHandler.getItem("app.gui.startup.newmachine"));
		buttonNew.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				// open new window to input the desired machine name and config
				// name
				createNewMachineGUI();

				shell.close();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent event) {
				// Not used
			}
		});

		// Button to continue (exit the window, load the selected configuration)
		Button buttonContinue = new Button(shell, SWT.NONE);
		buttonContinue.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, true, false, 1, 1));
		buttonContinue.setText(LocalizationHandler.getItem("app.gui.continue"));
		buttonContinue.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				// get the values for the Machine Name and the Machine Config
				// Name
				String machine = comboMachineName.getText();
				String machConfig = comboMachineConfigName.getText();
				String simConfig = comboSimConfigName.getText();
				String procName = comboProcName.getText();

				// check if a machine config was selected, otherwise stop
				if (machConfig.equals(LocalizationHandler.getItem("app.gui.startup.selectmachineconfigname"))) {
					return;
				}

				EModSession.setMachineName(machine);
				EModSession.setMachineConfig(machConfig);
				EModSession.setSimulationConfig(simConfig);
				EModSession.setProcessName(procName);

				EModSession.save();

				// build machine and add components to the table of the model
				// gui tab
				loadMachineModelConfig(machine, machConfig);

				shell.close();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent event) {
				// Not used
			}
		});

		shell.pack();

		Rectangle rect = shell.getBounds();

		// width and height of the shell
		int[] size = { 0, 0 };
		size[0] = rect.width;
		size[1] = rect.height;

		// position the shell into the middle of the last window
		int[] position;
		position = EModGUI.shellPosition();
		shell.setLocation(position[0] - size[0] / 2, position[1] - size[1] / 2);

		// open the new shell
		shell.open();

		return shell;
	}

	/**
	 * window to create a new machine configuration
	 */
	protected static void createNewMachineGUI() {
		final Shell shellCreateMachine = new Shell(Display.getCurrent(), SWT.APPLICATION_MODAL);
		shellCreateMachine.setText("EMod startup");
		shellCreateMachine.setLayout(new GridLayout(2, true));

		// text load machine config
		Text textMachineName = new Text(shellCreateMachine, SWT.READ_ONLY | SWT.LEFT);
		textMachineName.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false, 1, 1));
		textMachineName.setText(LocalizationHandler.getItem("app.gui.startup.newmachinename"));

		// text for the user to enter the desired Machine Name
		final Text textMachineNameValue = new Text(shellCreateMachine, SWT.NONE);
		textMachineNameValue.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false, 1, 1));

		// text load machine config
		Text textMachineConfigName = new Text(shellCreateMachine, SWT.READ_ONLY | SWT.LEFT);
		textMachineConfigName.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false, 1, 1));
		textMachineConfigName.setText(LocalizationHandler.getItem("app.gui.startup.newmachineconfigname"));

		// text for the user to enter the desired Machine Name
		final Text textMachineConfigNameValue = new Text(shellCreateMachine, SWT.NONE);
		textMachineConfigNameValue.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false, 1, 1));

		// Button to continue (exit the window, load the selected configuration)
		Button buttonContinue = new Button(shellCreateMachine, SWT.NONE);
		GridData gridData = new GridData(GridData.END, GridData.CENTER, true, false);
		gridData.horizontalSpan = 2;
		buttonContinue.setLayoutData(gridData);
		buttonContinue.setText(LocalizationHandler.getItem("app.gui.continue"));
		buttonContinue.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				// get the values for the Machine Name and the Machine Config
				// Name
				String machine = textMachineNameValue.getText();
				String config = textMachineConfigNameValue.getText();

				// create the new machine
				createNewMachine(machine, config);

				// close the shell
				shellCreateMachine.close();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent event) {
				// Not used
			}
		});

		shellCreateMachine.pack();

		Rectangle rect = shellCreateMachine.getBounds();

		// width and height of the shell
		int[] size = { 0, 0 };
		size[0] = rect.width;
		size[1] = rect.height;

		// position the shell into the middle of the last window
		int[] position;
		position = EModGUI.shellPosition();
		shellCreateMachine.setLocation(position[0] - size[0] / 2, position[1] - size[1] / 2);

		// open the new shell
		shellCreateMachine.open();
	}

	/**
	 * method to create files and folders of a new machine
	 * 
	 * @param machine name of the machine to add
	 * @param config  name of the machine conifguration to add
	 */
	protected static void createNewMachine(String machine, String config) {

		String simconfig = "SimConfig", process = "default";

		EModSession.setMachineName(machine);
		EModSession.setMachineConfig(config);
		EModSession.setSimulationConfig(simconfig);
		EModSession.setProcessName(process);

		// create the according folders and files (machine.xml, iolinking.txt)
		File machinexml = new File(EModSession.getMachineConfigPath());
		File simxml = new File(EModSession.getSimulationConfigPath());
		File processxml = new File(EModSession.getProcessConfigPath());
		File stateseq = new File(EModSession.getStateSequenceConfigPath());
		try {
			machinexml.getParentFile().mkdirs();
			simxml.getParentFile().mkdirs();
			machinexml.createNewFile();
			simxml.createNewFile();
			processxml.createNewFile();
			stateseq.createNewFile();
		} catch (IOException e) {
			e.printStackTrace();
		}

		EModSession.save();
	}

	/**
	 * update the comboMachineConfigName according to the selection of
	 * comboMachineName
	 * 
	 * @param stringMachConfig update the selection of possible machine
	 *                         conifgurations
	 */
	protected static void updatecomboMachineConfigName(String stringMachConfig) {
		String path = PropertiesHandler.getProperty("app.MachineDataPathPrefix") + "/" + stringMachConfig
				+ "/MachineConfig/";
		File subdir = new File(path);

		// check if subdirectory exists, then show possible configurations to
		// select
		if (subdir.exists()) {
			String[] subitems = subdir.list();
			Arrays.sort(subitems);
			comboMachineConfigName.setItems(subitems);
			if (1 == subitems.length)
				comboMachineConfigName.setText(subitems[0]);
			else
				comboMachineConfigName.setText(LocalizationHandler.getItem("app.gui.startup.selectmachineconfigname"));
		}
		// otherwise inform the user to create a new SimConfig
		else {
			comboMachineConfigName.removeAll();
			comboMachineConfigName.setText(LocalizationHandler.getItem("app.gui.startup.newmachineconfigname"));
		}
	}

	// update the comboSimConfigName according to the selection of
	// comboMachineName
	protected static void updatecomboSimConfigName(String stringMachConfig) {
		String path = PropertiesHandler.getProperty("app.MachineDataPathPrefix") + "/" + stringMachConfig
				+ "/SimulationConfig/";
		File subdir = new File(path);

		// check if subdirectory exists, then show possible configurations to
		// select
		if (subdir.exists()) {
			String[] subitems = subdir.list();
			Arrays.sort(subitems);
			comboSimConfigName.setItems(subitems);
			if (1 == subitems.length)
				comboSimConfigName.setText(subitems[0]);
			else
				comboSimConfigName.setText(LocalizationHandler.getItem("app.gui.startup.selectmachineconfigname"));
		}
		// otherwise inform the user to create a new SimConfig
		else {
			comboSimConfigName.removeAll();
			comboSimConfigName.setText(LocalizationHandler.getItem("app.gui.startup.newmachineconfigname"));
		}
	}

	// update the comboProcName according to the selection of comboMachineName
	protected static void updatecomboProcName(String stringMachConfig, String stringSimConfig) {
		String path = PropertiesHandler.getProperty("app.MachineDataPathPrefix") + "/" + stringMachConfig
				+ "/SimulationConfig/" + stringSimConfig;
		File files = new File(path);

		// check if subdirectory exists, then show possible configurations to
		// select
		if (files.exists()) {
			comboProcName.removeAll();
			for (File f : files.listFiles()) {
				if (f.getName().startsWith("process_")) {
					comboProcName.add(f.getName().substring(8, f.getName().length() - 4));
				}
			}

			if (1 == comboProcName.getItems().length)
				comboProcName.setText(comboProcName.getItems()[0]);
			else
				comboProcName.setText(LocalizationHandler.getItem("app.gui.startup.selectmachineconfigname"));
		}
		// otherwise inform the user to create a new SimConfig
		else {
			comboProcName.removeAll();
			comboProcName.setText(LocalizationHandler.getItem("app.gui.startup.newmachineconfigname"));
		}
	}

	/**
	 * load a machine configuration from DB and write it to the machine table
	 * 
	 * @param machine name of the machine to load
	 * @param config  name of the configuration to load
	 */
	public static void loadMachineModelConfig(String machine, String config) {
		// Build machine: Read and check machine configuration
		Machine.buildMachine(machine, config);
	}

}
