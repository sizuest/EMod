/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.femexport;

/**
 * Types of boundary conditions given is a ODE y'
 * 
 * @author simon
 *
 */
public enum BoundaryConditionType {
	/**
	 * y = f
	 */
	DIRICHLET,
	/**
	 * y' = f
	 */
	NEUMANN,
	/**
	 * c0*y+c1*y'=f
	 */
	ROBIN,
	/**
	 * y=f and c0*y+c1*y'=f
	 */
	MIXED,
	/**
	 * y=f and c0*y'=f
	 */
	CAUCHY,
	/**
	 * every thing else
	 */
	OTHER
}
