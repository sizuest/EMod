/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.utils;

/**
 * Defines for EMod
 * 
 * @author sizuest
 *
 */
public class Defines {

	/* Path and filename definitions: */
	/* Machine Config */
	/**
	 * DIR
	 */
	public static final String MACHINECONFIGDIR = "MachineConfig";
	/**
	 * MACHINE FILE
	 */
	public static final String MACHINEFILENAME = "Machine.xml";
	/**
	 * LINKING FILE
	 */
	public static final String LINKFILENAME = "IOLinking.txt";

	/* Simulation Config */
	/**
	 * DIR
	 */
	public static final String SIMULATIONCONFIGDIR = "SimulationConfig";
	/**
	 * STATE FILE
	 */
	public static final String MACHINESTATEFNAME = "MachineStateSequence.txt";
	/**
	 * PROCESS FILE
	 */
	public static final String PROCESSDEFFILE_PREFIX = "process_";
	/**
	 * SIMULATION CONFIG FILE
	 */
	public static final String SIMULATIONCONFIGFILE = "Simulation.xml";
	/**
	 * LIFE CYCLE CONFIG FILE
	 */
	public static final String LIFECYCLECONFIGFILE = "LifeCycle.xml";
	/**
	 * TEMP FILE SPACE
	 */
	public static final String TEMPFILESPACE = "Temp";
	/**
	 * LIB FILE SPACE
	 */
	public static final String LIBFILESPACE = "Machines";
	/**
	 * SESSION FILE NAME
	 */
	public static final String SESSIONFILE = "Session.xml";
	/**
	 * RESULT FOLDER NAME
	 */
	public static final String RESULTDIR = "Results";
}
