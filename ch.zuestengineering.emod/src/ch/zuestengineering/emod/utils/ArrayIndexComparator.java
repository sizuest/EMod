/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.utils;

import java.util.Comparator;

/**
 * @author sizuest
 * 
 *         Used for sorting arrays
 * 
 */

public class ArrayIndexComparator implements Comparator<Integer> {
	private final double[] array;

	/**
	 * ArrayIndexComparator
	 * 
	 * @param array
	 */
	public ArrayIndexComparator(double[] array) {
		this.array = array;
	}

	/**
	 * Create index vector
	 * 
	 * @return
	 */
	public Integer[] createIndexArray() {
		Integer[] idx = new Integer[array.length];
		for (int i = 0; i < array.length; i++)
			idx[i] = i;
		return idx;
	}

	@Override
	public int compare(Integer index1, Integer index2) {
		return Double.valueOf(array[index1]).compareTo(array[index2]);
	}
}
