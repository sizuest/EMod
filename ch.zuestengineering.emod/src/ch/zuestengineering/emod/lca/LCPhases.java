/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.lca;

/**
 * @author sizuest
 *
 */
public enum LCPhases {
	/**
	 * Raw material procurement
	 */
	RAWMATERIAL,
	/**
	 * Production
	 */
	PRODUCTION,
	/**
	 * Set-up
	 */
	SETUP,
	/**
	 * Use
	 */
	USE,
	/**
	 * End of life
	 */
	EOL
}
