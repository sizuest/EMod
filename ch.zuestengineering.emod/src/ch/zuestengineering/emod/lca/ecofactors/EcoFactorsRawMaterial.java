/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.lca.ecofactors;

import java.io.File;
import java.util.ArrayList;

import ch.zuestengineering.emod.lca.LCScenario;
import ch.zuestengineering.emod.lca.inventory.MaterialAmount;
import ch.zuestengineering.emod.lca.inventory.RessourceDemand;
import ch.zuestengineering.emod.lca.typedef.RawMaterialType;
import ch.zuestengineering.emod.simulation.MachineState;
import ch.zuestengineering.emod.utils.PropertiesHandler;

/**
 * Implements a
 * 
 * @author sizuest
 *
 */
public class EcoFactorsRawMaterial extends AEcoFactorSet<RawMaterialType> {

	private static EcoFactorsRawMaterial instance = null;

	/**
	 * Private constructor
	 */
	private EcoFactorsRawMaterial() {
	}

	/**
	 * Returns the static eco factors object
	 * 
	 * @return
	 */
	public static EcoFactorsRawMaterial getInstance() {
		if (null == instance) {
			instance = new EcoFactorsRawMaterial();
			instance.init();
		}

		return instance;
	}

	/**
	 * Load the factors from file
	 */
	private void init() {
		load(getInstance().getPath());
	}

	/**
	 * @param type
	 * @return
	 */
	public static EcoFactor get(RawMaterialType type) {
		return getInstance().list.get(type);
	}

	/**
	 * Returns the eco impact of the transportation based on the selected eco factor
	 * 
	 * @param ecofactor
	 * @param material
	 * @return
	 */
	public static double getEcoImpact(EcoFactorTypes ecofactor, MaterialAmount material) {
		if(material.getMaterial().getMaterialClass().equals(RawMaterialType.UNDEFINED))
			return 0.0;
		
		return getInstance().list.get(material.getMaterial().getMaterialClass()).getEcoImpact(ecofactor,
				material.getAmount());
	}

	/**
	 * Returns the eco impact of a list of transportations based on the selected eco
	 * factor
	 * 
	 * @param ecofactor
	 * @param materials
	 * @return
	 */
	public static double getEcoImpact(EcoFactorTypes ecofactor, ArrayList<MaterialAmount> materials) {
		double out = 0;

		for (MaterialAmount e : materials)
			out += getEcoImpact(ecofactor, e);

		return out;

	}

	/**
	 * Returns the eco impact of the transportation based on the selected eco factor
	 * 
	 * @param ecofactor
	 * @param demand
	 * @param scenario
	 * @return
	 */
	public static double getEcoImpact(EcoFactorTypes ecofactor, RessourceDemand demand, LCScenario scenario) {
		double sum = 0;

		if(demand.getMaterial().getMaterialClass().equals(RawMaterialType.UNDEFINED))
			return 0;
		
		for (MachineState s : MachineState.values())
			sum += getInstance().list.get(demand.getMaterial().getMaterialClass()).getEcoImpact(ecofactor,
					demand.getDemand(s, scenario.getStateDuration(s)));

		return sum;
	}

	/**
	 * Returns the eco impact of a list of transportations based on the selected eco
	 * factor
	 * 
	 * @param ecofactor
	 * @param materials
	 * @param scenario
	 * @return
	 */
	public static double getEcoImpact(EcoFactorTypes ecofactor, ArrayList<RessourceDemand> materials,
			LCScenario scenario) {
		double out = 0;

		for (RessourceDemand e : materials)
			out += getEcoImpact(ecofactor, e, scenario);

		return out;

	}

	/**
	 * Save
	 */
	public static void save() {
		getInstance().save(getInstance().getPath());
	}

	/**
	 * @return the ecoFactors
	 */
	public static EcoFactorList<RawMaterialType> getEcoFactors() {
		return getInstance().list;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.zuestengineering.emod.lca.ecofactors.AEcoFactorSet#getPath()
	 */
	@Override
	public String getPath() {
		String path = PropertiesHandler.getProperty("app.EcoFactorDBPathPrefix") + File.separator
				+ "RawMaterialProcurement.xml";
		return path;
	}
}
