/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.lca.ecofactors;

import java.util.ArrayList;

import ch.zuestengineering.emod.lca.inventory.MaterialAmount;

/**
 * Implements a
 * 
 * @author sizuest
 *
 */
public class EcoFactorsEOL {

	private static EcoFactorsEOL instance = null;

	/**
	 * Private constructor
	 */
	private EcoFactorsEOL() {
	}

	/**
	 * Returns the static eco factors object
	 * 
	 * @return
	 */
	public static EcoFactorsEOL getInstance() {
		if (null == instance) {
			instance = new EcoFactorsEOL();
		}

		return instance;
	}

	/**
	 * Returns the eco impact of the transportation based on the selected eco factor
	 * 
	 * @param ecofactor
	 * @param demand
	 * @return
	 */
	public static double getEcoImpact(EcoFactorTypes ecofactor, MaterialAmount demand) {
		switch (demand.getEol()) {
		case INCINERATION:
			return EcoFactorsBurning.getEcoImpact(ecofactor, demand);
		case LANDFILL:
			return EcoFactorsLandFill.getEcoImpact(ecofactor, demand);
		case RECYCLE:
			return EcoFactorsRecycling.getEcoImpact(ecofactor, demand);
		case REUSE:
			return 0;
		default:
			return Double.NaN;
		}

	}

	/**
	 * Returns the eco impact of a list of transportations based on the selected eco
	 * factor
	 * 
	 * @param ecofactor
	 * @param demand
	 * @return
	 */
	public static double getEcoImpact(EcoFactorTypes ecofactor, ArrayList<MaterialAmount> demand) {
		double out = 0;

		for (MaterialAmount e : demand)
			out += getEcoImpact(ecofactor, e);

		return out;

	}
}
