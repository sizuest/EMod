/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.lca.ecofactors;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.Writer;
import java.util.logging.Logger;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

/**
 * @author sizuest
 * @param <T>
 *
 */
public abstract class AEcoFactorSet<T> {

	protected EcoFactorList<T> list;

	private static Logger logger = Logger.getLogger(EcoFactorList.class.getName());

	/**
	 * Saves the eco factors to a file
	 * 
	 * @param file
	 */
	public void save(String file) {

		logger.info("Saving eco factors sequence to file: " + file);

		try {
			JAXBContext context = JAXBContext.newInstance(EcoFactorList.class);
			Marshaller m = context.createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

			Writer w = new FileWriter(file);
			m.marshal(list, w);
			w.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Loads the eco factors from the stated path
	 * 
	 * @param path
	 */
	@SuppressWarnings("unchecked")
	public void load(String path) {
		logger.info("Loading eco factors from file: " + path);

		list = null;
		try {
			JAXBContext context = JAXBContext.newInstance(EcoFactorList.class);
			Unmarshaller um = context.createUnmarshaller();
			list = (EcoFactorList<T>) um.unmarshal(new FileReader(path));
		} catch (Exception e) {
			logger.warning("File for eco factors does not exist: " + path);
			list = new EcoFactorList<>();
		}
	}

	/**
	 * Returns the path to the db file
	 * 
	 * @return
	 */
	public abstract String getPath();

}
