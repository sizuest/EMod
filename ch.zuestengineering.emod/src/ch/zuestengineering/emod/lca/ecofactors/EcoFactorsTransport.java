/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.lca.ecofactors;

import java.io.File;
import java.util.ArrayList;

import ch.zuestengineering.emod.lca.inventory.Transportation;
import ch.zuestengineering.emod.lca.typedef.TransportationType;
import ch.zuestengineering.emod.utils.PropertiesHandler;

/**
 * Implements a
 * 
 * @author sizuest
 *
 */
public class EcoFactorsTransport extends AEcoFactorSet<TransportationType> {

	private static EcoFactorsTransport instance = null;

	/**
	 * Private constructor
	 */
	private EcoFactorsTransport() {
	}

	/**
	 * Returns the static eco factors object
	 * 
	 * @return
	 */
	public static EcoFactorsTransport getInstance() {
		if (null == instance) {
			instance = new EcoFactorsTransport();
			instance.init();
		}

		return instance;
	}

	/**
	 * Load the factors from file
	 */
	private void init() {
		load(getInstance().getPath());
	}

	/**
	 * @param type
	 * @return
	 */
	public static EcoFactor get(TransportationType type) {
		return getInstance().list.get(type);
	}

	/**
	 * Returns the eco impact of the transportation based on the selected eco factor
	 * 
	 * @param ecofactor
	 * @param transport
	 * @return
	 */
	public static double getEcoImpact(EcoFactorTypes ecofactor, Transportation transport) {
		return getInstance().list.get(transport.getType()).getEcoImpact(ecofactor, transport.getWeigthDistance());
	}

	/**
	 * Returns the eco impact of a list of transportations based on the selected eco
	 * factor
	 * 
	 * @param ecofactor
	 * @param transports
	 * @return
	 */
	public static double getEcoImpact(EcoFactorTypes ecofactor, ArrayList<Transportation> transports) {
		double out = 0;

		for (Transportation t : transports)
			out += getEcoImpact(ecofactor, t);

		return out;

	}

	/**
	 * Save
	 */
	public static void save() {
		getInstance().save(getInstance().getPath());
	}

	/**
	 * @return the ecoFactors
	 */
	public static EcoFactorList<TransportationType> getEcoFactors() {
		return getInstance().list;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.zuestengineering.emod.lca.ecofactors.AEcoFactorSet#getPath()
	 */
	@Override
	public String getPath() {
		String path = PropertiesHandler.getProperty("app.EcoFactorDBPathPrefix") + File.separator
				+ "Transportation.xml";
		return path;
	}
}
