/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.lca.ecofactors;

import java.util.HashMap;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlTransient;

import ch.zuestengineering.emod.lca.typedef.ElectricEnergySource;
import ch.zuestengineering.emod.lca.typedef.RawMaterialType;
import ch.zuestengineering.emod.lca.typedef.TransportationType;
import ch.zuestengineering.emod.model.units.SiUnit;

/**
 * Implements a lookup table for eco factors
 * 
 * @author sizuest
 * @param <T>
 *
 */
@XmlRootElement
@XmlSeeAlso({ RawMaterialType.class, TransportationType.class, ElectricEnergySource.class })
public class EcoFactorList<T> {
	@XmlElement
	private HashMap<T, EcoFactor> factors = new HashMap<>();
	@XmlElement
	private SiUnit referenceUnit;

	/**
	 * @param type
	 * @return
	 */
	public EcoFactor get(T type) {
		return factors.get(type);
	}

	/**
	 * @param type
	 * @param ef
	 */
	public void put(T type, EcoFactor ef) {
		factors.put(type, ef);
	}

	/**
	 * Returns the eco impact of the amount consumed based on the selected eco
	 * factor
	 * 
	 * @param ecofactor
	 * @param t
	 * @param amount
	 * @return
	 */
	public double getEcoImpact(EcoFactorTypes ecofactor, T t, double amount) {
		return factors.get(t).getEcoImpact(ecofactor, amount);
	}

	/**
	 * Returns the unit of the eco factor
	 * 
	 * @param ecofactor
	 * @return
	 */
	public String getEcoFactorUnit(EcoFactorTypes ecofactor) {
		return EcoFactorProperties.getUnit(ecofactor) + "/" + referenceUnit.toString();
	}

	/**
	 * Returns the unit of the eco impact
	 * 
	 * @param ecofactor
	 * @return
	 */
	public String getEcoImpactUnit(EcoFactorTypes ecofactor) {
		return EcoFactorProperties.getUnit(ecofactor);
	}

	/**
	 * @return the referenceUnit
	 */
	@XmlTransient
	public SiUnit getReferenceUnit() {
		return referenceUnit;
	}

	/**
	 * @param referenceUnit the referenceUnit to set
	 */
	public void setReferenceUnit(SiUnit referenceUnit) {
		this.referenceUnit = referenceUnit;
	}

	/**
	 * containsKey
	 * @param key 
	 * @return
	 */
	public boolean containsKey(T key) {
		return factors.containsKey(key);
	}
}
