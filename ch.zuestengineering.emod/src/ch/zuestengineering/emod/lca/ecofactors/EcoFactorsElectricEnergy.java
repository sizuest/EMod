/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.lca.ecofactors;

import java.io.File;
import java.util.ArrayList;

import ch.zuestengineering.emod.lca.LCScenario;
import ch.zuestengineering.emod.lca.inventory.ElectricityDemand;
import ch.zuestengineering.emod.lca.typedef.ElectricEnergySource;
import ch.zuestengineering.emod.simulation.MachineState;
import ch.zuestengineering.emod.utils.PropertiesHandler;

/**
 * Implements a
 * 
 * @author sizuest
 *
 */
public class EcoFactorsElectricEnergy extends AEcoFactorSet<ElectricEnergySource> {

	private static EcoFactorsElectricEnergy instance = null;

	/**
	 * Private constructor
	 */
	private EcoFactorsElectricEnergy() {
	}

	/**
	 * Returns the static eco factors object
	 * 
	 * @return
	 */
	public static EcoFactorsElectricEnergy getInstance() {
		if (null == instance) {
			instance = new EcoFactorsElectricEnergy();
			instance.init();
		}

		return instance;
	}

	/**
	 * Load the factors from file
	 */
	private void init() {
		load(getInstance().getPath());
	}

	/**
	 * @param type
	 * @return
	 */
	public static EcoFactor get(ElectricEnergySource type) {
		return getInstance().list.get(type);
	}

	/**
	 * Returns the eco impact of the transportation based on the selected eco factor
	 * 
	 * @param ecofactor
	 * @param demand
	 * @param scenario
	 * @return
	 */
	public static double getEcoImpact(EcoFactorTypes ecofactor, ElectricityDemand demand, LCScenario scenario) {
		double sum = 0;
		for (MachineState s : MachineState.values()) {
			if (scenario.getStateDuration(s) > 0)
				sum += getInstance().list.get(scenario.getElectricEnergySource()).getEcoImpact(ecofactor,
						demand.getDemand(s) * scenario.getStateDuration(s));
		}
		return sum;
	}

	/**
	 * Returns the eco impact of a list of transportations based on the selected eco
	 * factor
	 * 
	 * @param ecofactor
	 * @param demand
	 * @param scenario
	 * @return
	 */
	public static double getEcoImpact(EcoFactorTypes ecofactor, ArrayList<ElectricityDemand> demand,
			LCScenario scenario) {
		double out = 0;

		for (ElectricityDemand e : demand)
			out += getEcoImpact(ecofactor, e, scenario);

		return out;

	}

	/**
	 * Save
	 */
	public static void save() {
		getInstance().save(getInstance().getPath());
	}

	/**
	 * @return the ecoFactors
	 */
	public static EcoFactorList<ElectricEnergySource> getEcoFactors() {
		return getInstance().list;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.zuestengineering.emod.lca.ecofactors.AEcoFactorSet#getPath()
	 */
	@Override
	public String getPath() {
		String path = PropertiesHandler.getProperty("app.EcoFactorDBPathPrefix") + File.separator
				+ "ElectricEnergy.xml";
		return path;
	}
}
