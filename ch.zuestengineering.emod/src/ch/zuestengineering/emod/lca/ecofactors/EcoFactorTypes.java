/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.lca.ecofactors;

/**
 * @author sizuest
 *
 */
public enum EcoFactorTypes {
	/**
	 * Primary energy
	 */
	PRIMARYENERGY,
	/**
	 * Unweltbelastungspunkte
	 */
	UBP,
	/**
	 * CO2 equivalent
	 */
	CO2EQ,
}
