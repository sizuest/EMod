/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.lca.ecofactors;

import java.io.File;
import java.util.ArrayList;

import ch.zuestengineering.emod.lca.inventory.MaterialAmount;
import ch.zuestengineering.emod.lca.typedef.RawMaterialType;
import ch.zuestengineering.emod.utils.PropertiesHandler;

/**
 * Implements a
 * 
 * @author sizuest
 *
 */
public class EcoFactorsBurning extends AEcoFactorSet<RawMaterialType> {

	private static EcoFactorsBurning instance = null;

	/**
	 * Private constructor
	 */
	private EcoFactorsBurning() {
	}

	/**
	 * Returns the static eco factors object
	 * 
	 * @return
	 */
	public static EcoFactorsBurning getInstance() {
		if (null == instance) {
			instance = new EcoFactorsBurning();
			instance.init();
		}

		return instance;
	}

	/**
	 * Load the factors from file
	 */
	private void init() {
		load(getInstance().getPath());
	}

	/**
	 * @param type
	 * @return
	 */
	public static EcoFactor get(RawMaterialType type) {
		return getInstance().list.get(type);
	}

	/**
	 * Returns the eco impact of the transportation based on the selected eco factor
	 * 
	 * @param ecofactor
	 * @param material
	 * @return
	 */
	public static double getEcoImpact(EcoFactorTypes ecofactor, MaterialAmount material) {
		return getInstance().list.get(material.getMaterial().getMaterialClass()).getEcoImpact(ecofactor,
				material.getAmount());
	}

	/**
	 * Returns the eco impact of a list of transportations based on the selected eco
	 * factor
	 * 
	 * @param ecofactor
	 * @param materials
	 * @return
	 */
	public static double getEcoImpact(EcoFactorTypes ecofactor, ArrayList<MaterialAmount> materials) {
		double out = 0;

		for (MaterialAmount e : materials)
			out += getEcoImpact(ecofactor, e);

		return out;

	}

	/**
	 * Save
	 */
	public static void save() {
		getInstance().save(getInstance().getPath());
	}

	/**
	 * @return the ecoFactors
	 */
	public static EcoFactorList<RawMaterialType> getEcoFactors() {
		return getInstance().list;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.zuestengineering.emod.lca.ecofactors.AEcoFactorSet#getPath()
	 */
	@Override
	public String getPath() {
		String path = PropertiesHandler.getProperty("app.EcoFactorDBPathPrefix") + File.separator + "Burning.xml";
		return path;
	}
}
