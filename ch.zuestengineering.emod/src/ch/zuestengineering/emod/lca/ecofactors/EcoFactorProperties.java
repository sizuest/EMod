/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.lca.ecofactors;

import java.util.HashMap;

/**
 * Implements the static information for the eco factors
 * 
 * @author sizuest
 *
 */
public class EcoFactorProperties {
	private static HashMap<EcoFactorTypes, String> units;

	private static void initUnits() {
		units = new HashMap<>();
		for (EcoFactorTypes ef : EcoFactorTypes.values())
			units.put(ef, "");

		/* Eco Factor Definition */
		units.put(EcoFactorTypes.PRIMARYENERGY, "MJ");
		units.put(EcoFactorTypes.UBP, "UBP");
		units.put(EcoFactorTypes.CO2EQ, "kg CO2 eq.");
	}

	/**
	 * Returns the Unit of the Ecofactor as String
	 * 
	 * @param ecofactor
	 * @return
	 */
	public static String getUnit(EcoFactorTypes ecofactor) {
		if (null == units)
			initUnits();

		return units.get(ecofactor);
	}
}
