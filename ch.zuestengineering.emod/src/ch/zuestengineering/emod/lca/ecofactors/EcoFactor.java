/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.lca.ecofactors;

import java.util.HashMap;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author sizuest
 *
 */
@XmlRootElement
public class EcoFactor {
	@XmlElement
	private HashMap<EcoFactorTypes, Double> factors;

	/**
	 * 
	 */
	public EcoFactor() {
	}

	/**
	 * @param factors
	 */
	public EcoFactor(HashMap<EcoFactorTypes, Double> factors) {
		this.factors = factors;
	}

	/**
	 * Computes the eco impact of the quantity based on the selected ecofactor
	 * 
	 * @param ecofactor
	 * @param quantity
	 * @return
	 */
	public double getEcoImpact(EcoFactorTypes ecofactor, double quantity) {
		if(!factors.containsKey(ecofactor))
			return 0;
		
		return factors.get(ecofactor) * quantity;
	}
}
