/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.lca.typedef;

/**
 * Types of raw materials
 * 
 * @author sizuest
 *
 */
public enum RawMaterialType {
	/**
	 * Steel and iron
	 */
	STEEL_IRON,
	/**
	 * Cast iron
	 */
	CAST_IRON,
	/**
	 * Steel high alloyed
	 */
	STEEL_HIGHALLOYED,
	/**
	 * PE, PP, PET and Polyester
	 */
	PE_PP_PET_POLYESTER,
	/**
	 * Polystyrole
	 */
	PS,
	/**
	 * Non-ferrous metal (aluminum, copepr, brass)
	 */
	NON_FERROUS_METAL,
	/**
	 * PUR and PC
	 */
	PUR_PC,
	/**
	 * Polyamids and eppoxi
	 */
	POLYAMIDS_EP,
	/**
	 * Electronics
	 */
	ELECTRONICS,
	/**
	 * Wood
	 */
	WOOD,
	/**
	 * Cardboard and paper
	 */
	CARDBOARD,
	/**
	 * Brick, concrete
	 */
	BRICK_CONCRETE,
	/**
	 * Cement
	 */
	CEMENT,
	/**
	 * Sand, gravel
	 */
	SAND_GRAVEL,
	/**
	 * Oil (lubricant)
	 */
	LUBRICANT,
	/**
	 * Pressured air
	 */
	COMPRESSED_AIR,
	/**
	 * Gas
	 */
	PROCESS_GAS,
	/**
	 * Water
	 */
	WATER, 
	/**
	 * Undefined
	 */
	UNDEFINED
}
