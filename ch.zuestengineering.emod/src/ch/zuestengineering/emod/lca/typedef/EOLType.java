/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.lca.typedef;

/**
 * @author sizuest
 *
 */
public enum EOLType {
	/**
	 * Material reuse
	 */
	REUSE,
	/**
	 * Material recycling
	 */
	RECYCLE,
	/**
	 * Industrial incineration (transformation into thermal energy)
	 */
	INCINERATION,
	/**
	 * land fill
	 */
	LANDFILL
}
