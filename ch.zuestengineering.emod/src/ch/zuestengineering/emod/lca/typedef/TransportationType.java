/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.lca.typedef;

/**
 * Definition of different transportation types
 * 
 * @author sizuest
 *
 */
public enum TransportationType {
	/**
	 * 
	 */
	PLANE,
	/**
	 * 
	 */
	CAR,
	/**
	 * 
	 */
	VAN,
	/**
	 * 
	 */
	TRUCK,
	/**
	 * 
	 */
	TRAIN,
	/**
	 * 
	 */
	SHIP
}
