/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.lca.typedef;

/**
 * @author sizuest
 *
 */
public enum LifeCyclePhase {
	/**
	 * Raw material procurement
	 */
	RAWMATERIALS,
	/**
	 * Production and procurement
	 */
	PRODUCTION,
	/**
	 * Packaging, transportation and commissioning
	 */
	DISTRIBUTION,
	/**
	 * Use
	 */
	USE,
	/**
	 * end of life
	 */
	EOL
}
