/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.lca.building;

/**
 * @author sizuest
 *
 */
public enum BuildingClass {
	/**
	 * A
	 */
	A,
	/**
	 * B
	 */
	B,
	/**
	 * C
	 */
	C,
	/**
	 * D
	 */
	D,
	/**
	 * E
	 */
	E,
	/**
	 * F
	 */
	F,
	/**
	 * No classification
	 */
	NONE
}
