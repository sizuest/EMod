/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.lca.building;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Logger;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import ch.zuestengineering.emod.lca.LCScenario;
import ch.zuestengineering.emod.lca.ecofactors.EcoFactorList;
import ch.zuestengineering.emod.lca.ecofactors.EcoFactorTypes;
import ch.zuestengineering.emod.lca.ecofactors.EcoFactorsElectricEnergy;
import ch.zuestengineering.emod.lca.inventory.CoolingDemand;
import ch.zuestengineering.emod.lca.inventory.ElectricityDemand;
import ch.zuestengineering.emod.simulation.MachineState;
import ch.zuestengineering.emod.utils.PropertiesHandler;

/**
 * @author sizuest
 *
 */
public class BuildingEnergyDemand {
	protected BuildingERRTable list;

	private static Logger logger = Logger.getLogger(EcoFactorList.class.getName());

	private static BuildingEnergyDemand instance = null;

	/**
	 * Private constructor
	 */
	private BuildingEnergyDemand() {
	}

	/**
	 * Returns the static eco factors object
	 * 
	 * @return
	 */
	public static BuildingEnergyDemand getInstance() {
		if (null == instance) {
			instance = new BuildingEnergyDemand();
			instance.init();
		}

		return instance;
	}

	/**
	 * Load the factors from file
	 */
	private void init() {
		load(getInstance().getPath());
	}

	/**
	 * Saves the eco factors to a file
	 * 
	 * @param file
	 */
	public void save(String file) {

		logger.info("Saving eco factors sequence to file: " + file);

		try {
			JAXBContext context = JAXBContext.newInstance(BuildingERRTable.class);
			Marshaller m = context.createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

			Writer w = new FileWriter(file);
			m.marshal(list, w);
			w.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Loads the eco factors from the stated path
	 * 
	 * @param path
	 */
	public void load(String path) {
		logger.info("Loading eco factors from file: " + path);

		list = null;
		try {
			JAXBContext context = JAXBContext.newInstance(BuildingERRTable.class);
			Unmarshaller um = context.createUnmarshaller();
			list = (BuildingERRTable) um.unmarshal(new FileReader(path));
		} catch (Exception e) {
			logger.warning("File for eco factors does not exist: " + path);
			list = new BuildingERRTable();
		}
	}

	/**
	 * returns the electric energy demand to provide the cooling functions
	 * 
	 * @param demand
	 * @param scenario
	 * @return
	 */
	public static ElectricityDemand getElectricEnergyDemand(CoolingDemand demand, LCScenario scenario) {
		HashMap<MachineState, Double> edemand = new HashMap<>();

		for (MachineState s : MachineState.values())
			edemand.put(s, demand.getDemand(s) / getInstance().list.get(scenario.getBuildingClass(), demand.getType()));

		return new ElectricityDemand(edemand);
	}

	/**
	 * Returns the eco impact of the transportation based on the selected eco factor
	 * 
	 * @param ecofactor
	 * @param demand
	 * @param scenario
	 * @return
	 */
	public static double getEcoImpact(EcoFactorTypes ecofactor, CoolingDemand demand, LCScenario scenario) {
		return EcoFactorsElectricEnergy.getEcoImpact(ecofactor, getElectricEnergyDemand(demand, scenario), scenario);
	}

	/**
	 * Returns the eco impact of a list of transportations based on the selected eco
	 * factor
	 * 
	 * @param ecofactor
	 * @param demands
	 * @param scenario
	 * @return
	 */
	public static double getEcoImpact(EcoFactorTypes ecofactor, ArrayList<CoolingDemand> demands, LCScenario scenario) {
		double out = 0;

		for (CoolingDemand e : demands)
			out += getEcoImpact(ecofactor, e, scenario);

		return out;

	}

	/**
	 * Save
	 */
	public static void save() {
		getInstance().save(getInstance().getPath());
	}

	/**
	 * @return the ecoFactors
	 */
	public static BuildingERRTable getERRTable() {
		return getInstance().list;
	}

	/**
	 * Returns the path to the db file
	 * 
	 * @return
	 */
	public String getPath() {
		String path = PropertiesHandler.getProperty("app.EcoFactorDBPathPrefix") + File.separator + "BuildingERR.xml";
		return path;
	}
}
