/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.lca.building;

import java.util.HashMap;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;

/**
 * @author sizuest
 *
 */
@XmlRootElement
@XmlSeeAlso({ CoolingType.class })
public class BuildingERRTable {
	@XmlElement
	HashMap<String, Double> table;

	/**
	 * 
	 */
	public BuildingERRTable() {
		fill();
	}

	/**
	 * @param bClass
	 * @param type
	 * @param err
	 */
	public void set(BuildingClass bClass, CoolingType type, double err) {
		table.put(getIdString(bClass, type), err);
	}

	/**
	 * @param bClass
	 * @param type
	 * @return
	 */
	public double get(BuildingClass bClass, CoolingType type) {
		return table.get(getIdString(bClass, type));
	}

	private void fill() {
		if (null == table)
			table = new HashMap<>();

		for (BuildingClass b : BuildingClass.values()) {
			for (CoolingType t : CoolingType.values()) {
				if (!table.containsKey(getIdString(b, t)))
					table.put(getIdString(b, t), Double.NaN);
			}
		}

	}

	private String getIdString(BuildingClass bClass, CoolingType type) {
		return bClass.toString() + "_" + type.toString();
	}
}
