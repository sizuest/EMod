/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.lca.inventory;

import java.util.Map;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import ch.zuestengineering.emod.lca.building.CoolingType;
import ch.zuestengineering.emod.simulation.MachineState;

/**
 * Implements the characterization of an energy demand for the LCA
 * 
 * @author sizuest
 *
 */
@XmlRootElement
public class CoolingDemand extends ADemand {
	@XmlElement
	private CoolingType type;

	/**
	 * 
	 */
	public CoolingDemand() {
		super();
	}

	/**
	 * @param demand
	 * @param type
	 */
	public CoolingDemand(Map<MachineState, Double> demand, CoolingType type) {
		super(demand);
		this.type = type;
	}

	/**
	 * @return the type
	 */
	@XmlTransient
	public CoolingType getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(CoolingType type) {
		this.type = type;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.zuestengineering.emod.lca.inventory.ADemand#clone()
	 */
	@Override
	public CoolingDemand clone() {
		CoolingDemand out = new CoolingDemand();
		out.setType(getType());
		out.setDemand(getDemandClone());
		return out;
	}
}
