/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.lca.inventory;

import java.util.ArrayList;
import java.util.HashMap;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import ch.zuestengineering.emod.EModSession;
import ch.zuestengineering.emod.Machine;
import ch.zuestengineering.emod.analysis.SimulationResult;
import ch.zuestengineering.emod.lca.building.CoolingType;
import ch.zuestengineering.emod.model.MachineComponent;
import ch.zuestengineering.emod.simulation.MachineState;

/**
 * Definition of an Inventory for a LC-Phase
 * 
 * @author sizuest
 *
 */
@XmlRootElement
public class InventoryMachine extends AInventory {

	/* RAW MATERIALS, PRODUCTION, PROCUREMENT, EOL */
	/* Component Transportation */
	@XmlElement
	protected ArrayList<Transportation> componentTransportations;

	/* COMMISSIONING */
	/* Commissioning materials */
	@XmlElement
	protected ArrayList<MaterialAmount> commissioningMaterials;

	/* USE */
	/* Scrap Materials */
	@XmlElement
	protected ArrayList<RessourceDemand> scrapMaterials;
	/* Electric energy during use phase */
	@XmlElement
	protected ArrayList<ElectricityDemand> electricityDemand;
	/* Energy during use phase */
	@XmlElement
	protected ArrayList<CoolingDemand> coolingDemand;
	/* Operational Materials */
	@XmlElement
	protected ArrayList<RessourceDemand> operationalMaterials;

	/**
	 * 
	 */
	public InventoryMachine() {
		super();
		componentTransportations = new ArrayList<>();
		commissioningMaterials = new ArrayList<>();
		operationalMaterials = new ArrayList<>();
		scrapMaterials = new ArrayList<>();
		electricityDemand = new ArrayList<>();
		coolingDemand = new ArrayList<>();
	}

	/**
	 * Fetches the LC information from the machine and the simulation
	 */
	public void fetch() {
		fetchComponentMaterials();
		fetchComponentTransportation();
		fetchRessourceDemand();
		fetchElectricEnergyDemand();
		fetchCoolingDemand();
	}

	/**
	 * Loads the material demands from all machine components in the machine
	 */
	public void fetchComponentMaterials() {
		productMaterials.clear();

		for (MachineComponent mc : Machine.getMachineComponentList())
			productMaterials.addAll(mc.getComponent().getMaterialAmount());
	}

	/**
	 * Loads the resource demands from all machine components in the machine
	 */
	public void fetchRessourceDemand() {
		operationalMaterials.clear();
		for (MachineComponent mc : Machine.getMachineComponentList())
			operationalMaterials.addAll(mc.getComponent().getRessourceDemand());
	}

	/**
	 * Loads the component transportations for all machine components in the machine
	 */
	public void fetchComponentTransportation() {
		componentTransportations.clear();
		for (MachineComponent mc : Machine.getMachineComponentList())
			componentTransportations.addAll(mc.getComponent().getTransportation());
	}

	/**
	 * Loads the state specific electric energy demand from the simulation results
	 */
	public void fetchElectricEnergyDemand() {
		SimulationResult results = new SimulationResult(EModSession.getResultFilePath());
		results.readData();

		electricityDemand.clear();

		HashMap<MachineState, Double> demand = new HashMap<>();
		for (MachineState state : MachineState.values()) {
			demand.put(state, results.getAveragePowerDemand(state));
		}

		ElectricityDemand eed = new ElectricityDemand();
		eed.setDemand(demand);

		electricityDemand.add(eed);
	}

	/**
	 * Loads the state specific electric energy demand from the simulation results
	 */
	public void fetchCoolingDemand() {
		SimulationResult results = new SimulationResult(EModSession.getResultFilePath());
		results.readData();

		coolingDemand.clear();

		HashMap<MachineState, Double> demandFluid = new HashMap<>();
		HashMap<MachineState, Double> demandAir = new HashMap<>();

		double elPwr, thPwr;

		for (MachineState state : MachineState.values()) {
			elPwr = results.getAveragePowerDemand(state);
			thPwr = results.getAverageFluidHeatFlux(state);

			demandFluid.put(state, thPwr);
			demandAir.put(state, elPwr - thPwr);
		}

		coolingDemand.add(new CoolingDemand(demandFluid, CoolingType.WATER));
		coolingDemand.add(new CoolingDemand(demandAir, CoolingType.AIR));
	}

	/**
	 * @return the operationalMaterials
	 */
	@XmlTransient
	public ArrayList<RessourceDemand> getOperationalMaterials() {
		return operationalMaterials;
	}

	protected ArrayList<RessourceDemand> getOperationalMaterialsClone() {
		ArrayList<RessourceDemand> out = new ArrayList<>();
		for (RessourceDemand t : operationalMaterials)
			out.add(t.clone());

		return out;
	}

	/**
	 * @param operationalMaterials the operationalMaterials to set
	 */
	public void setOperationalMaterials(ArrayList<RessourceDemand> operationalMaterials) {
		this.operationalMaterials = operationalMaterials;
	}

	/**
	 * @return the scrapMaterials
	 */
	@XmlTransient
	public ArrayList<RessourceDemand> getScrapMaterials() {
		return scrapMaterials;
	}

	protected ArrayList<RessourceDemand> getScrapMaterialsClone() {
		ArrayList<RessourceDemand> out = new ArrayList<>();
		for (RessourceDemand t : scrapMaterials)
			out.add(t.clone());

		return out;
	}

	/**
	 * @param scrapMaterials the scrapMaterials to set
	 */
	public void setScrapMaterials(ArrayList<RessourceDemand> scrapMaterials) {
		this.scrapMaterials = scrapMaterials;
	}

	/**
	 * @return the operationalEnergies
	 */
	@XmlTransient
	public ArrayList<ElectricityDemand> getElectricityDemand() {
		return electricityDemand;
	}

	protected ArrayList<ElectricityDemand> getElectricityDemandClone() {
		ArrayList<ElectricityDemand> out = new ArrayList<>();
		for (ElectricityDemand t : electricityDemand)
			out.add(t.clone());

		return out;
	}

	/**
	 * @param operationalEnergies the operationalEnergies to set
	 */
	public void setElectricityDemand(ArrayList<ElectricityDemand> operationalEnergies) {
		this.electricityDemand = operationalEnergies;
	}

	/**
	 * @return the coolingDemand
	 */
	@XmlTransient
	public ArrayList<CoolingDemand> getCoolingDemand() {
		return coolingDemand;
	}

	protected ArrayList<CoolingDemand> getCoolingDemandClone() {
		ArrayList<CoolingDemand> out = new ArrayList<>();
		for (CoolingDemand t : coolingDemand)
			out.add(t.clone());

		return out;
	}

	/**
	 * @param coolingDemand the coolingDemand to set
	 */
	public void setCoolingDemand(ArrayList<CoolingDemand> coolingDemand) {
		this.coolingDemand = coolingDemand;
	}

	/**
	 * @return the componentTransportations
	 */
	@XmlTransient
	public ArrayList<Transportation> getComponentTransport() {
		return componentTransportations;
	}

	protected ArrayList<Transportation> getComponentTransportClone() {
		ArrayList<Transportation> out = new ArrayList<>();
		for (Transportation t : componentTransportations)
			out.add(t.clone());

		return out;
	}

	/**
	 * @param componentTransportations the componentTransportations to set
	 */
	public void setComponentTransportations(ArrayList<Transportation> componentTransportations) {
		this.componentTransportations = componentTransportations;
	}

	/**
	 * @return the commissioningMaterials
	 */
	@XmlTransient
	public ArrayList<MaterialAmount> getCommissioningMaterials() {
		return commissioningMaterials;
	}

	protected ArrayList<MaterialAmount> getCommissioningMaterialsClone() {
		ArrayList<MaterialAmount> out = new ArrayList<>();
		for (MaterialAmount t : commissioningMaterials)
			out.add(t.clone());

		return out;
	}

	/**
	 * @param commissioningMaterials the commissioningMaterials to set
	 */
	public void setCommissioningMaterials(ArrayList<MaterialAmount> commissioningMaterials) {
		this.commissioningMaterials = commissioningMaterials;
	}

	@Override
	public InventoryTool clone() {
		InventoryMachine out = new InventoryMachine();
		out.setCommissioningMaterials(getCommissioningMaterialsClone());
		out.setComponentTransportations(getComponentTransportClone());
		out.setCoolingDemand(getCoolingDemandClone());
		out.setElectricityDemand(getElectricityDemandClone());
		out.setOperationalMaterials(getOperationalMaterialsClone());
		out.setScrapMaterials(getOperationalMaterialsClone());
		out.setPackagingMaterials(getPackagingMaterialsClone());
		out.setDistributionTransport(getDistributionTransportClone());
		out.setProductMaterials(getProductMaterialsClone());
		return null;
	}

}
