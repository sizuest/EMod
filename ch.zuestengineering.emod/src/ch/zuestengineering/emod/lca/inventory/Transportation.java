/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.lca.inventory;

import ch.zuestengineering.emod.lca.typedef.TransportationType;

/**
 * Characterization of a transport in the context of an lca
 * 
 * @author sizuest
 *
 */
public class Transportation implements Cloneable {
	private TransportationType type;
	private double distance;
	private double weight;

	/**
	 * 
	 */
	public Transportation() {
		type = TransportationType.CAR;
		distance = 0;
		weight = 0;
	}

	/**
	 * @param type
	 * @param distance
	 * @param weight
	 */
	public Transportation(TransportationType type, double distance, double weight) {
		super();
		this.type = type;
		this.distance = distance;
		this.weight = weight;
	}

	/**
	 * @return the type
	 */
	public TransportationType getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(TransportationType type) {
		this.type = type;
	}

	/**
	 * @return the distance
	 */
	public double getDistance() {
		return distance;
	}

	/**
	 * @param distance the distance to set
	 */
	public void setDistance(double distance) {
		if (distance < 0)
			distance = 0;
		this.distance = distance;
	}

	/**
	 * @return the weight
	 */
	public double getWeight() {
		return weight;
	}

	/**
	 * @param weight the weight to set
	 */
	public void setWeight(double weight) {
		if (weight < 0)
			weight = 0;
		this.weight = weight;
	}

	/**
	 * @return
	 */
	public double getWeigthDistance() {
		return distance * weight;
	}

	@Override
	public Transportation clone() {
		Transportation clone = new Transportation(getType(), getDistance(), getWeight());
		return clone;
	}

}
