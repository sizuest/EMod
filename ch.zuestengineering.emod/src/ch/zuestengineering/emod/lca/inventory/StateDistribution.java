/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.lca.inventory;

import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import ch.zuestengineering.emod.States;
import ch.zuestengineering.emod.simulation.MachineState;

/**
 * @author sizuest
 *
 */
@XmlRootElement
public class StateDistribution {
	@XmlElement
	private Map<MachineState, Double> stateMap;

	/**
	 * StateDistribution
	 */
	public StateDistribution() {
		fetch();
	}

	/**
	 * @param stateMap
	 */
	public StateDistribution(Map<MachineState, Double> stateMap) {
		this.stateMap = stateMap;
		normalizeStateDistribution();
	}

	/**
	 * Returns the given states relative time share (0 ... 1)
	 * 
	 * @param state
	 * @return
	 */
	public double getStateShare(MachineState state) {
		return stateMap.get(state);
	}

	/**
	 * Returns the given states relative time share (0 ... 100)
	 * 
	 * @param state
	 * @return
	 */
	public int getStateShareInPrct(MachineState state) {
		return (int) (getStateShare(state) * 100);
	}

	/**
	 * Fetches the state duration from the State-classs
	 */
	public void fetch() {
		if (null == stateMap)
			stateMap = new HashMap<>();
		else
			stateMap.clear();

		for (MachineState s : MachineState.values())
			stateMap.put(s, States.getTotalDuration(s));

		normalizeStateDistribution();
	}

	private void normalizeStateDistribution() {
		double sum = 0;

		for (MachineState s : MachineState.values())
			sum += stateMap.get(s);

		for (MachineState s : MachineState.values())
			stateMap.put(s, stateMap.get(s) / sum);

	}
}
