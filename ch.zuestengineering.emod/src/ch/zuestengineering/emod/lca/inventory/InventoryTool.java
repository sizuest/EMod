/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.lca.inventory;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import ch.zuestengineering.emod.lca.LCScenario;
import ch.zuestengineering.emod.simulation.MachineState;

/**
 * Definition of a tool inventory for a LC-Phase
 * 
 * @author sizuest
 *
 */
@XmlRootElement
public class InventoryTool extends AInventory {
	@XmlElement
	private String name = "Tool";
	@XmlElement
	private double toolLifeTime = 3600;
	@XmlElement
	private double toolActiveShare = .5;

	/**
	 * @return the toolActiveShare (0...1)
	 */
	@XmlTransient
	public double getToolActiveShare() {
		return toolActiveShare;
	}

	/**
	 * @param toolActiveShare the toolActiveShare to set (0...1)
	 */
	public void setToolActiveShare(double toolActiveShare) {
		this.toolActiveShare = toolActiveShare;
	}

	/**
	 * @return the toolLifeTime [s]
	 */
	@XmlTransient
	public double getToolLifeTime() {
		return toolLifeTime;
	}

	/**
	 * @param toolLifeTime the toolLifeTime to set [s]
	 */
	public void setToolLifeTime(double toolLifeTime) {
		this.toolLifeTime = toolLifeTime;
	}

	/**
	 * Number of tools used during the life time
	 * 
	 * @param scenario
	 * @return
	 */
	public double getNumberOfTools(LCScenario scenario) {
		return scenario.getStateDuration(MachineState.PROCESS) / toolLifeTime * toolActiveShare;
	}

	/**
	 * @return the name
	 */
	@XmlTransient
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.zuestengineering.emod.lca.inventory.AInventory#clone()
	 */
	@Override
	public InventoryTool clone() {
		InventoryTool out = new InventoryTool();
		out.setName(getName());
		out.setToolActiveShare(getToolActiveShare());
		out.setToolLifeTime(getToolLifeTime());
		out.setPackagingMaterials(getPackagingMaterialsClone());
		out.setDistributionTransport(getDistributionTransportClone());
		out.setProductMaterials(getProductMaterialsClone());
		return null;
	}

}
