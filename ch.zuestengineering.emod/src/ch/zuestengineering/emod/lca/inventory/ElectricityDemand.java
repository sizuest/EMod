/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.lca.inventory;

import java.util.Map;

import javax.xml.bind.annotation.XmlRootElement;

import ch.zuestengineering.emod.simulation.MachineState;

/**
 * Implements the characterization of an energy demand for the LCA
 * 
 * @author sizuest
 *
 */
@XmlRootElement
public class ElectricityDemand extends ADemand {

	/**
	 * 
	 */
	public ElectricityDemand() {
		super();
	}

	/**
	 * @param demand
	 */
	public ElectricityDemand(Map<MachineState, Double> demand) {
		super(demand);
	}

	@Override
	public ElectricityDemand clone() {
		ElectricityDemand out = new ElectricityDemand();
		out.setDemand(getDemandClone());
		return out;
	}
}
