/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.lca.inventory;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * Definition of an Inventory for a LC-Phase
 * 
 * @author sizuest
 *
 */
@XmlRootElement
public abstract class AInventory implements Cloneable {
	/* Materials installed in the machine */
	@XmlElement
	protected ArrayList<MaterialAmount> productMaterials;
	/* Packaging materials */
	@XmlElement
	protected ArrayList<MaterialAmount> packagingMaterials;
	/* Initial Transport */
	@XmlElement
	protected ArrayList<Transportation> distributionTransport;

	/**
	 * 
	 */
	public AInventory() {
		productMaterials = new ArrayList<>();
		packagingMaterials = new ArrayList<>();
		distributionTransport = new ArrayList<>();
	}

	/**
	 * Computation of the commissioning transportation weigth defined as Mass
	 * Machine + Mass Packaging Material Unit: kg
	 * 
	 * @return
	 */
	private double getTransportationWeigth() {
		double w = 0;

		for (MaterialAmount m : productMaterials)
			w += m.getAmount();

		for (MaterialAmount m : packagingMaterials)
			w += m.getAmount();

		return w;
	}

	/**
	 * Updates the transportation weigth according to the product and packaging
	 * weight
	 */
	public void updateTransportationWeight() {
		double weigth = getTransportationWeigth();

		for (Transportation t : distributionTransport)
			t.setWeight(weigth);
	}

	/**
	 * @return the productMaterials
	 */
	@XmlTransient
	public ArrayList<MaterialAmount> getProductMaterials() {
		return productMaterials;
	}

	/**
	 * @param productMaterials the productMaterials to set
	 */
	public void setProductMaterials(ArrayList<MaterialAmount> productMaterials) {
		this.productMaterials = productMaterials;
		updateTransportationWeight();
	}

	/**
	 * @return the packagingMaterials
	 */
	@XmlTransient
	public ArrayList<MaterialAmount> getPackagingMaterials() {
		return packagingMaterials;
	}

	/**
	 * @param packagingMaterials the packagingMaterials to set
	 */
	public void setPackagingMaterials(ArrayList<MaterialAmount> packagingMaterials) {
		this.packagingMaterials = packagingMaterials;
		updateTransportationWeight();
	}

	/**
	 * @param distributionTransport the commissioningTransport to set
	 */
	public void setDistributionTransport(ArrayList<Transportation> distributionTransport) {
		this.distributionTransport = distributionTransport;
		updateTransportationWeight();
	}

	/**
	 * @return the distributionTransport
	 */
	@XmlTransient
	public ArrayList<Transportation> getDistributionTransport() {
		updateTransportationWeight();
		return distributionTransport;
	}

	/**
	 * @return the distributionTransport
	 */
	protected ArrayList<Transportation> getDistributionTransportClone() {
		ArrayList<Transportation> out = new ArrayList<>();
		for (Transportation t : distributionTransport)
			out.add(t.clone());

		return out;
	}

	/**
	 * @return the distributionTransport
	 */
	protected ArrayList<MaterialAmount> getPackagingMaterialsClone() {
		ArrayList<MaterialAmount> out = new ArrayList<>();
		for (MaterialAmount t : packagingMaterials)
			out.add(t.clone());

		return out;
	}

	/**
	 * @return the distributionTransport
	 */
	protected ArrayList<MaterialAmount> getProductMaterialsClone() {
		ArrayList<MaterialAmount> out = new ArrayList<>();
		for (MaterialAmount t : productMaterials)
			out.add(t.clone());

		return out;
	}

	@Override
	public abstract AInventory clone();

}
