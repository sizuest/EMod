/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.lca.inventory;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import ch.zuestengineering.emod.lca.typedef.EOLType;
import ch.zuestengineering.emod.model.material.Material;

/**
 * Implements a representation of a specific amount of a specific material
 * 
 * @author sizuest
 *
 */
@XmlRootElement
public class MaterialAmount implements Cloneable {
	@XmlElement
	private Material material;
	@XmlElement
	private double amount;
	@XmlElement
	private EOLType eol;

	/**
	 * 
	 */
	public MaterialAmount() {
		material = new Material();
		amount = 0;
		eol = EOLType.LANDFILL;
	}

	/**
	 * @param material
	 * @param amount
	 * @param eol
	 */
	public MaterialAmount(Material material, double amount, EOLType eol) {
		this.material = material;
		this.amount = amount;
		this.eol = eol;
	}

	/**
	 * @return the material
	 */
	@XmlTransient
	public Material getMaterial() {
		return material;
	}

	/**
	 * @param material the material to set
	 */
	public void setMaterial(Material material) {
		this.material = material;
	}

	/**
	 * @return the amount
	 */
	@XmlTransient
	public double getAmount() {
		return amount;
	}

	/**
	 * @param amount the amount to set
	 */
	public void setAmount(double amount) {
		if (amount < 0)
			amount = 0;
		this.amount = amount;
	}

	/**
	 * @return the eol
	 */
	@XmlTransient
	public EOLType getEol() {
		return eol;
	}

	/**
	 * @param eol the eol to set
	 */
	public void setEol(EOLType eol) {
		this.eol = eol;
	}

	/**
	 * Reads and returns the list of materials from the the given component type
	 * 
	 * @param component
	 * @param type
	 * @return
	 */
	public static ArrayList<MaterialAmount> getMaterials(String component, String type) {
		ArrayList<MaterialAmount> materials = new ArrayList<>();

		// TODO

		return materials;
	}

	@Override
	public MaterialAmount clone() {
		MaterialAmount clone = new MaterialAmount(new Material(getMaterial().toString()), getAmount(), getEol());
		return clone;
	}

	/**
	 * @param materials
	 * @return
	 */
	public static double getWeight(ArrayList<MaterialAmount> materials) {
		double sum = 0;
		for (MaterialAmount m : materials)
			sum += m.getAmount();

		return sum;
	}

}
