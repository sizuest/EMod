/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.lca.inventory;

import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import ch.zuestengineering.emod.simulation.MachineState;

/**
 * Implements a generic state dependent demand
 * 
 * @author sizuest
 *
 */
@XmlRootElement
public abstract class ADemand implements Cloneable {
	@XmlElement
	protected Map<MachineState, Double> demand;

	/**
	 * 
	 */
	public ADemand() {
		super();
		this.demand = new HashMap<MachineState, Double>();
		addMissingStates();
	}

	/**
	 * @param demand
	 */
	public ADemand(Map<MachineState, Double> demand) {
		super();
		this.demand = demand;
		addMissingStates();
	}

	/**
	 * @return the demand
	 */
	@XmlTransient
	public Map<MachineState, Double> getDemand() {
		return demand;
	}

	protected Map<MachineState, Double> getDemandClone() {
		Map<MachineState, Double> out = new HashMap<MachineState, Double>();

		for (MachineState s : demand.keySet())
			out.put(s, demand.get(s));

		return out;
	}

	/**
	 * @param demand the demand to set
	 */
	public void setDemand(Map<MachineState, Double> demand) {
		this.demand = demand;
		addMissingStates();
	}

	private void addMissingStates() {
		for (MachineState s : MachineState.values())
			demand.putIfAbsent(s, 0.0);
	}

	/**
	 * Returns the demand for the given state
	 * 
	 * @param state
	 * @return
	 */
	public double getDemand(MachineState state) {
		return demand.get(state);
	}

	@Override
	public String toString() {
		try {
			JAXBContext context = JAXBContext.newInstance(RessourceDemand.class);
			Marshaller m = context.createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

			StringWriter w = new StringWriter();
			m.marshal(this, w);

			return w.toString();
		} catch (JAXBException e) {
			e.printStackTrace();
			return "";
		}
	}

	@Override
	public abstract ADemand clone();

}
