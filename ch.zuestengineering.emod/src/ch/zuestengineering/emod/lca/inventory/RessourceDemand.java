/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.lca.inventory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import ch.zuestengineering.emod.model.material.Material;
import ch.zuestengineering.emod.simulation.MachineState;

/**
 * Implements the characterization of a state dependent material demand for the
 * LCA
 * 
 * @author sizuest
 *
 */
@XmlRootElement
public class RessourceDemand extends ADemand implements Cloneable {
	@XmlElement
	Material material;

	/**
	 * 
	 */
	public RessourceDemand() {
		super();
		material = new Material();
	}

	/**
	 * @param demand
	 * @param material
	 */
	public RessourceDemand(Map<MachineState, Double> demand, Material material) {
		super(demand);
		this.material = material;
	}

	/**
	 * @return the form
	 */
	@XmlTransient
	public Material getMaterial() {
		return material;
	}

	/**
	 * @param material
	 */
	public void setMaterial(Material material) {
		this.material = material;
	}

	/**
	 * Returns the amount of cansumable required for the given state duration
	 * 
	 * @param state
	 * @param duration
	 * @return
	 */
	public double getDemand(MachineState state, double duration) {
		return demand.get(state) * duration;
	}

	/**
	 * Reads and returns the list of consumables from the the given component type
	 * 
	 * @param component
	 * @param type
	 * @return
	 */
	public static ArrayList<RessourceDemand> getMaterials(String component, String type) {
		ArrayList<RessourceDemand> materials = new ArrayList<>();

		// TODO

		return materials;
	}

	@Override
	public RessourceDemand clone() {
		Map<MachineState, Double> demand = new HashMap<MachineState, Double>();
		for (MachineState ms : getDemand().keySet())
			demand.put(ms, getDemand(ms));

		RessourceDemand clone = new RessourceDemand(demand, getMaterial().clone());
		return clone;
	}
}
