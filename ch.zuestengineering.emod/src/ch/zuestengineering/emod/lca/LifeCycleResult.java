/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.lca;

import java.util.ArrayList;
import java.util.HashMap;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;

import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Device;
import org.eclipse.swt.widgets.Display;
import org.swtchart.Chart;
import org.swtchart.IBarSeries;
import org.swtchart.ISeries.SeriesType;

/**
 * @author sizuest
 *
 */
@XmlRootElement
@XmlSeeAlso({SingleResult.class, LCPhases.class})
public class LifeCycleResult {
	@XmlElement
	private HashMap<String, SingleResult> data;
	@XmlElement
	private String unit = "";
	
	/**
	 * Empty constructor for marshaler
	 */
	public LifeCycleResult() {}

	/**
	 * @param unit
	 */
	public LifeCycleResult(String unit) {
		this.unit = unit;
		data = new HashMap<>();
	}

	/**
	 * @param name
	 * @param result
	 */
	public void put(String name, SingleResult result) {
		data.put(name, result);
	}

	/**
	 * @param name
	 * @param phase
	 * @param value
	 */
	public void put(String name, LCPhases phase, double value) {
		SingleResult result = new SingleResult();
		result.set(phase, value);
		data.put(name, result);
	}

	/**
	 * @param name
	 * @return
	 */
	public SingleResult get(String name) {
		return data.get(name);
	}

	private ArrayList<String> getOrderedList() {
		ArrayList<String> names = new ArrayList<>();

		for (LCPhases p : LCPhases.values())
			for (String name : data.keySet())
				if (data.get(name).getPhase().equals(p))
					names.add(name);

		return names;
	}

	/**
	 * @param chart
	 */
	public void toChart(Chart chart) {

		HashMap<LCPhases, Integer> cCoutner = new HashMap<>();
		for (LCPhases p : LCPhases.values())
			cCoutner.put(p, 0);
		
		String elEnergyName = "Electric Energy";

		for (String name : getOrderedList()) {
			try {
				IBarSeries serie = (IBarSeries) chart.getSeriesSet().createSeries(SeriesType.BAR, name);
				serie.getLabel().setVisible(true);
				serie.getLabel().setFormat("#,##0");
				serie.setYSeries(data.get(name).toArray());
				
				serie.enableStack(true);
				
				if(elEnergyName.equals(name))
					serie.setBarColor(new Color(chart.getDisplay(), 112, 48, 160));
				else
					serie.setBarColor(
							getColor(chart.getDisplay(), data.get(name).getPhase(), cCoutner.get(data.get(name).getPhase())));
	
				cCoutner.put(data.get(name).getPhase(), cCoutner.get(data.get(name).getPhase()) + 1);
			} catch(Exception e) {}
		}
	}

	/**
	 * @return
	 */
	public int size() {
		return data.size();
	}

	/**
	 * @return
	 */
	public String getUnit() {
		return unit;
	}

	private Color getColor(Device device, LCPhases phase, int i) {
		Color color = new Color(device, 0, 0, 0);
		switch (phase) {
		case EOL:
			color = new Color(device, 152, 72, 7);
			break;
		case PRODUCTION:
			color = new Color(device, 255, 0, 0);
			break;
		case RAWMATERIAL:
			color = new Color(device, 192, 0, 0);
			break;
		case SETUP:
			color = new Color(device, 0, 176, 240);
			break;
		case USE:
			color = new Color(device, 79, 98, 40);
			break;
		default:
			break;
		}

		while (i > 0) {
			int r = (int) Math.min(255, color.getRed() * (1 + .1 * i));
			int g = (int) Math.min(255, color.getGreen() * (1 + .1 * i));
			int b = (int) Math.min(255, color.getBlue() * (1 + .1 * i));

			color = new Color(Display.getCurrent(), r, g, b);

			i--;
		}

		return color;
	}
}
