/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.lca;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import ch.zuestengineering.emod.lca.building.BuildingClass;
import ch.zuestengineering.emod.lca.inventory.StateDistribution;
import ch.zuestengineering.emod.lca.typedef.ElectricEnergySource;
import ch.zuestengineering.emod.simulation.MachineState;

/**
 * @author sizuest
 *
 */
@XmlRootElement
public class LCScenario {
	@XmlElement
	private int numberOfUnits = 1;
	@XmlElement
	private boolean includeHVAC = true;
	@XmlElement
	private double productLifeTime = 365 * 24 * 5;
	@XmlElement
	private double serviceInterval = 365 * 24;
	@XmlElement
	private StateDistribution states = new StateDistribution();
	@XmlElement
	private BuildingClass buildingClass = BuildingClass.C;
	@XmlElement
	private ElectricEnergySource electricEnergySource = ElectricEnergySource.GLOBAL;
	@XmlElement
	private boolean localService = true;

	/**
	 * @return the numberOfUnits
	 */
	@XmlTransient
	public int getNumberOfUnits() {
		return numberOfUnits;
	}

	/**
	 * @param numberOfUnits the numberOfUnits to set
	 */
	public void setNumberOfUnits(int numberOfUnits) {
		this.numberOfUnits = numberOfUnits;
	}

	/**
	 * @return the includeHVAC
	 */
	@XmlTransient
	public boolean getIncludeHVAC() {
		return includeHVAC;
	}

	/**
	 * @param includeHVAC the includeHVAC to set
	 */
	public void setIncludeHVAC(boolean includeHVAC) {
		this.includeHVAC = includeHVAC;
	}

	/**
	 * @return the productLifeTime
	 */
	@XmlTransient
	public double getProductLifeTime() {
		return productLifeTime;
	}

	/**
	 * @return
	 */
	public double getProductLifeTimeInYears() {
		return productLifeTime / 24.0 / 365.0;
	}

	/**
	 * @param productLifeTime the productLifeTime to set
	 */
	public void setProductLifeTime(double productLifeTime) {
		this.productLifeTime = productLifeTime;
	}

	/**
	 * @param productLifeTime the productLifeTime to set
	 */
	public void setProductLifeTimeInYears(double productLifeTime) {
		this.productLifeTime = productLifeTime * 365 * 24;
	}

	/**
	 * @return the serviceInterval
	 */
	@XmlTransient
	public double getServiceInterval() {
		return serviceInterval;
	}

	/**
	 * @return
	 */
	public double getServiceIntervalInYears() {
		return serviceInterval / 365 / 24;
	}

	/**
	 * @param serviceInterval the serviceInterval to set
	 */
	public void setServiceInterval(double serviceInterval) {
		this.serviceInterval = serviceInterval;
	}

	/**
	 * @param serviceInterval the serviceInterval to set
	 */
	public void setServiceIntervalInYears(double serviceInterval) {
		this.serviceInterval = serviceInterval * 365 * 24;
	}

	/**
	 * retuns the number of services over the whole life time
	 * 
	 * @return
	 */
	public int getNumberOfservices() {
		return (int) Math.ceil(getProductLifeTime() / getServiceInterval());
	}

	/**
	 * @return the states
	 */
	@XmlTransient
	public StateDistribution getStates() {
		return states;
	}

	/**
	 * @param states the states to set
	 */
	public void setStates(StateDistribution states) {
		this.states = states;
	}

	/**
	 * @param state
	 * @return
	 */
	public double getStateDuration(MachineState state) {
		return getProductLifeTime() * getStates().getStateShare(state);
	}

	/**
	 * @return the buildingClass
	 */
	@XmlTransient
	public BuildingClass getBuildingClass() {
		return buildingClass;
	}

	/**
	 * @param buildingClass the buildingClass to set
	 */
	public void setBuildingClass(BuildingClass buildingClass) {
		this.buildingClass = buildingClass;
	}

	/**
	 * @return the electricEnergySource
	 */
	@XmlTransient
	public ElectricEnergySource getElectricEnergySource() {
		return electricEnergySource;
	}

	/**
	 * @param electricEnergySource the electricEnergySource to set
	 */
	public void setElectricEnergySource(ElectricEnergySource electricEnergySource) {
		this.electricEnergySource = electricEnergySource;
	}

	/**
	 * @return the localService
	 */
	@XmlTransient
	public boolean isLocalService() {
		return localService;
	}

	/**
	 * @param localService the localService to set
	 */
	public void setLocalService(boolean localService) {
		this.localService = localService;
	}

}
