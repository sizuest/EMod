/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod.lca;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.Writer;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;

import ch.zuestengineering.emod.EModSession;
import ch.zuestengineering.emod.lca.ecofactors.EcoFactorProperties;
import ch.zuestengineering.emod.lca.ecofactors.EcoFactorTypes;

/**
 * Wrapper class to store life cycle analysis results
 * @author sizuest
 *
 */
@XmlRootElement
@XmlSeeAlso({SingleResult.class, LCPhases.class})
public class LifeCycleResultFile {
	@XmlElement
	LifeCycleResult result = new LifeCycleResult(EcoFactorProperties.getUnit(EcoFactorTypes.PRIMARYENERGY));
	@XmlElement
	EcoFactorTypes ecoFactor = EcoFactorTypes.PRIMARYENERGY;
	
	/**
	 * Empty constructor for marshaler
	 */
	public LifeCycleResultFile() {}
	
	/**
	 * @param result
	 * @param ecoFactor
	 */
	public LifeCycleResultFile(LifeCycleResult result, EcoFactorTypes ecoFactor) {
		this.result = result;
		this.ecoFactor = ecoFactor;
	}

	/**
	 * @return the result
	 */
	public LifeCycleResult getResult() {
		return result;
	}

	/**
	 * @return the ecoFactor
	 */
	public EcoFactorTypes getEcoFactor() {
		return ecoFactor;
	}
	
	/**
	 * save
	 */
	public void save() {
		saveToFile(EModSession.getLCResultFilePath());
	}
	
	/**
	 * save
	 * @param path
	 */
	public void save(String path) {
		/* Saves the result */
		saveToFile(path);
	}
	
	/**
	 * Saves the result at the stated file path
	 * 
	 * @param file
	 */
	public void saveToFile(String file) {
		try {
			JAXBContext context = JAXBContext.newInstance(LifeCycleResultFile.class);
			Marshaller m = context.createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

			Writer w = new FileWriter(file);
			m.marshal(this, w);
			w.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * load
	 * @param path
	 * @return
	 */
	public static LifeCycleResultFile load(String path) {
		LifeCycleResultFile ret = new LifeCycleResultFile();
		try {
			JAXBContext context = JAXBContext.newInstance(LifeCycleResultFile.class);
			Unmarshaller um = context.createUnmarshaller();
			ret = (LifeCycleResultFile) um.unmarshal(new FileReader(path));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return ret;
	}

	
}
