/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.util.logging.Logger;

import ch.zuestengineering.emod.utils.Defines;
import ch.zuestengineering.emod.utils.PropertiesHandler;
import ch.zuestengineering.emod.utils.ZipUtils;

/**
 * General functions for the i/o handling
 * 
 * @author sizuest
 *
 */
public class EModFileHandling {
	private static Logger logger = Logger.getLogger(EModFileHandling.class.getName());

	/**
	 * Save the machine at the given location (as zip)
	 * 
	 * @param path
	 */
	public static void save(String path) {
		try {
			Machine.saveMachine(EModSession.getMachineName(), EModSession.getMachineConfig());
			States.saveStates(EModSession.getMachineName(), EModSession.getSimulationConfig());
			LifeCycle.save(EModSession.getSimulationConfig());
			EModSession.addNote("Saved project at: '" + path + "'");
			EModSession.save();
			ZipUtils.zipFolder(getMachinePath(), path);
			EModSession.setPath(path);
		} catch (Exception e) {
			logger.severe("SAVING FAILED: Saving the set-up at'" + path + "' was not successful: ");
			EModSession.addNote("SAVING FAILED!");
			e.printStackTrace();
		}
	}

	/**
	 * Loads the config from the stated path
	 * 
	 * @param path
	 */
	public static void open(String path) {
		try {
			File file = new File(getMachineTempPath() + File.separator + "temp");
			clearTempPath();
			ZipUtils.unzipFolder(path, file.getPath());
			EModSession.initSessionFromFile(
					getMachineTempPath() + File.separator + "temp" + File.separator + Defines.SESSIONFILE);
			while (!file.renameTo(new File(getMachineTempPath() + File.separator + EModSession.getMachineName()))) {
				Thread.sleep(1000);
			}
			PropertiesHandler.setProperty("app.MachineDataPathPrefix", Defines.TEMPFILESPACE);

			migrateToNewVersion(EModSession.getMachineConfigPath());

			Machine.initMachineFromFile(EModSession.getMachineConfigPath());
			States.readStates();
			Process.loadProcess(EModSession.getProcessName());
			LifeCycle.load(EModSession.getSimulationConfig());

			EModSession.setPath(path);

		} catch (Exception e) {
			logger.severe("LOADING FAILED: Loading the set-up at'" + path + "' was not successful: ");
			e.printStackTrace();
		}
	}

	/**
	 * Returns the path to the current loaded machine
	 * 
	 * @return
	 */
	public static String getMachinePath() {
		return EModSession.getRootPath();
	}

	/**
	 * Returns the path to the directory for tempoary loaded machines
	 * 
	 * @return
	 */
	public static String getMachineTempPath() {
		String path = Defines.TEMPFILESPACE;

		return path;
	}

	/**
	 * Clears the temp path for machine configs
	 * 
	 * @return
	 */
	public static boolean clearTempPath() {
		try {
			clearFolder(new File(getMachineTempPath()));
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}

		return true;
	}

	private static void clearFolder(File folder) {
		File[] files = folder.listFiles();
		if (files != null) {
			for (File f : files) {
				if (f.isDirectory())
					clearFolder(f);

				f.delete();
			}
		}
	}

	/**
	 * Copies the stated result file to the given destination
	 * 
	 * @param resultFile
	 * @param exportPath
	 */
	public static void exportResultFile(String resultFile, String exportPath) {

		try {
			String text = readFile(EModSession.getResultFilePath(resultFile)).replaceAll("\t", ";");
			BufferedWriter out = new BufferedWriter(new FileWriter(exportPath));
			out.write(text);
			out.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static String readFile(String path) throws IOException {
		FileInputStream stream = new FileInputStream(new File(path));
		try {
			FileChannel fc = stream.getChannel();
			MappedByteBuffer bb = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size());
			/* Instead of using default, pass in a decoder. */
			return Charset.defaultCharset().decode(bb).toString();
		} finally {
			stream.close();
		}
	}

	/**
	 * Migrates a xml file to the new version
	 * 
	 * @param path
	 * @throws Exception
	 */
	public static void migrateToNewVersion(String path) throws Exception {
		String content = readFile(path);
		if (content.contains("ch.ethz.inspire")) {
			content = content.replaceAll("ch.ethz.inspire", "ch.zuestengineering");
			File mcfg_old = new File(path);
			File mcfg_new = new File(path + "_tmp");
			BufferedWriter outStream = new BufferedWriter(new FileWriter(mcfg_new));
			System.gc();
			outStream.write(content);
			outStream.flush();
			outStream.close();
			mcfg_old.delete();
			mcfg_new.renameTo(new File(path));
		}
	}

}
