/*******************************************************************************
 * Copyright (C) Z�st Engineering AG - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 ******************************************************************************/
package ch.zuestengineering.emod;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.Writer;
import java.util.ArrayList;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;

import ch.zuestengineering.emod.lca.LCPhases;
import ch.zuestengineering.emod.lca.LCScenario;
import ch.zuestengineering.emod.lca.LifeCycleResult;
import ch.zuestengineering.emod.lca.building.BuildingEnergyDemand;
import ch.zuestengineering.emod.lca.ecofactors.EcoFactorProperties;
import ch.zuestengineering.emod.lca.ecofactors.EcoFactorTypes;
import ch.zuestengineering.emod.lca.ecofactors.EcoFactorsEOL;
import ch.zuestengineering.emod.lca.ecofactors.EcoFactorsElectricEnergy;
import ch.zuestengineering.emod.lca.ecofactors.EcoFactorsProduction;
import ch.zuestengineering.emod.lca.ecofactors.EcoFactorsRawMaterial;
import ch.zuestengineering.emod.lca.ecofactors.EcoFactorsTransport;
import ch.zuestengineering.emod.lca.inventory.InventoryMachine;
import ch.zuestengineering.emod.lca.inventory.InventoryTool;
import ch.zuestengineering.emod.lca.inventory.MaterialAmount;
import ch.zuestengineering.emod.lca.inventory.RessourceDemand;
import ch.zuestengineering.emod.lca.inventory.StateDistribution;
import ch.zuestengineering.emod.lca.inventory.Transportation;
import ch.zuestengineering.emod.lca.typedef.EOLType;
import ch.zuestengineering.emod.lca.typedef.TransportationType;
import ch.zuestengineering.emod.model.fluid.Fluid;
import ch.zuestengineering.emod.model.material.IdealGas;
import ch.zuestengineering.emod.model.material.Material;
import ch.zuestengineering.emod.model.material.MaterialType;
import ch.zuestengineering.emod.model.material.Solid;
import ch.zuestengineering.emod.simulation.MachineState;
import ch.zuestengineering.emod.utils.Defines;

/**
 * Implements the LCA calculation based on an Inventory
 * 
 * @author sizuest
 *
 */
@XmlRootElement
@XmlSeeAlso({ InventoryTool.class, MaterialAmount.class, RessourceDemand.class, Transportation.class, Material.class,
		EOLType.class, MaterialType.class, Solid.class, Fluid.class, IdealGas.class, TransportationType.class,
		StateDistribution.class, MachineState.class })
public class LifeCycle {
	@XmlElement
	protected LCScenario scenario;
	@XmlElement
	protected InventoryMachine machine;
	@XmlElement
	protected ArrayList<InventoryTool> tools;
	@XmlElement
	protected EcoFactorTypes ecoFactor;

	protected static LifeCycle lifeCycle;

	/**
	 * Private constructor for singelton implementation
	 */
	private LifeCycle() {
		scenario = new LCScenario();
		machine = new InventoryMachine();
		tools = new ArrayList<>();
		ecoFactor = EcoFactorTypes.PRIMARYENERGY;
	}

	/**
	 * Returns the instance of the singleton implementation
	 * 
	 * @return
	 */
	public static LifeCycle getInstance() {
		if (null == lifeCycle) {
			lifeCycle = new LifeCycle();
		}

		return lifeCycle;
	}

	/**
	 * Saves the configuration for the stated configuration
	 * 
	 * @param simConfig
	 */
	public static void save(String simConfig) {
		String prefix = EModSession.getRootPath();
		String path = prefix + File.separator + Defines.SIMULATIONCONFIGDIR + File.separator + simConfig
				+ File.separator;

		/* Saves the config */
		saveToFile(path + Defines.LIFECYCLECONFIGFILE);
	}

	/**
	 * Saves the configuration at the stated file path
	 * 
	 * @param file
	 */
	public static void saveToFile(String file) {
		if (null == lifeCycle)
			return;
		// Save Machine Configuration
		try {
			JAXBContext context = JAXBContext.newInstance(LifeCycle.class);
			Marshaller m = context.createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

			Writer w = new FileWriter(file);
			m.marshal(lifeCycle, w);
			w.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Loads the configuration for the stated configuration
	 * 
	 * @param simConfig
	 */
	public static void load(String simConfig) {
		String prefix = EModSession.getRootPath();
		String path = prefix + File.separator + Defines.SIMULATIONCONFIGDIR + File.separator + simConfig
				+ File.separator;

		/* Loads the config */
		loadFromFile(path + Defines.LIFECYCLECONFIGFILE);
	}

	/**
	 * Loads the configuration from the stated file path
	 * 
	 * @param file
	 */
	public static void loadFromFile(String file) {
		lifeCycle = null;
		try {
			JAXBContext context = JAXBContext.newInstance(LifeCycle.class);
			Unmarshaller um = context.createUnmarshaller();
			lifeCycle = (LifeCycle) um.unmarshal(new FileReader(file));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Fetch data from machine and scenario configuration
	 */
	public static void fetch() {
		fetchDataFromMachine();
		fetchTimeSharesFromScenario();
	}

	/**
	 * Fetch data from machine configuration
	 */
	public static void fetchDataFromMachine() {
		getInstance().machine.fetch();
	}

	/**
	 * Fetch data from scenario configuration
	 */
	public static void fetchTimeSharesFromScenario() {
		getInstance().scenario.getStates().fetch();
	}

	/**
	 * @return the scenario
	 */
	public static LCScenario getScenario() {
		return getInstance().scenario;
	}

	/**
	 * @param scenario the scenario to set
	 */
	public static void setScenario(LCScenario scenario) {
		getInstance().scenario = scenario;
	}

	/**
	 * @return the machine
	 * 
	 */
	public static InventoryMachine getMachine() {
		return getInstance().machine;
	}

	/**
	 * @return the tools
	 */
	public static ArrayList<InventoryTool> getTools() {
		return getInstance().tools;
	}

	/**
	 * @param tools the tools to set
	 */
	public static void setTools(ArrayList<InventoryTool> tools) {
		getInstance().tools = tools;
	}

	/**
	 * @return the ecoFactor
	 */
	public static EcoFactorTypes getEcoFactor() {
		return getInstance().ecoFactor;
	}

	/**
	 * @param ecoFactor the ecoFactor to set
	 */
	public static void setEcoFactor(EcoFactorTypes ecoFactor) {
		getInstance().ecoFactor = ecoFactor;
	}

	/**
	 * Returns the total eco impact
	 * 
	 * The total eco-impact is the sum of the eco-impacts generated
	 * by the material procurement, the production, the distribution
	 * (packaging and transportation), the commissioning, the use phase
	 * and the end-of-life actions
	 * 
	 * @return
	 */
	public static double getEcoImpact() {
		return getEcoImpactRawMaterials()
				+ getEcoImpactProduction() 
				+ getEcoImpactPackagingMaterials()
				+ getEcoImpactTransportation() 
				+ getEcoImpactCommissioningMaterials() 
				+ getEcoImpactUse()
				+ getEcoImpactEOL();
	}

	/**
	 * Returns the eco impact of the raw material procurement
	 * 
	 * @return
	 */
	public static double getEcoImpactRawMaterials() {
		return EcoFactorsRawMaterial.getEcoImpact(getEcoFactor(), getMachine().getProductMaterials());
	}

	/**
	 * Returns the eco impact of the production
	 * 
	 * @return
	 */
	public static double getEcoImpactProduction() {
		return EcoFactorsProduction.getEcoImpact(getEcoFactor(), getMachine().getProductMaterials());
	}

	/**
	 * Returns the eco impact of the packaging material procurement
	 * 
	 * @return
	 */
	public static double getEcoImpactPackagingMaterials() {
		return EcoFactorsRawMaterial.getEcoImpact(getEcoFactor(), getMachine().getPackagingMaterials());
	}

	/**
	 * Returns the eco impact of the transportation
	 * 
	 * @return
	 */
	public static double getEcoImpactTransportation() {
		return getEcoImpactTransportationComponents() + getEcoImpactTransportationMachine();
	}

	/**
	 * Returns the eco impact of the transportation of the components to the
	 * assembly
	 * 
	 * @return
	 */
	public static double getEcoImpactTransportationComponents() {
		return EcoFactorsTransport.getEcoImpact(getEcoFactor(), getMachine().getComponentTransport());
	}

	/**
	 * Returns the eco impact of the transportation of the machine
	 * 
	 * @return
	 */
	public static double getEcoImpactTransportationMachine() {
		return EcoFactorsTransport.getEcoImpact(getEcoFactor(), getMachine().getDistributionTransport());
	}

	/**
	 * Returns the eco impact of the commissioning materials procurement
	 * 
	 * @return
	 */
	public static double getEcoImpactCommissioningMaterials() {
		return EcoFactorsRawMaterial.getEcoImpact(getEcoFactor(), getMachine().getCommissioningMaterials());
	}

	/**
	 * Returns the eco impact of the use phase
	 * 
	 * The eco-impact of the use phase consists of the electric energy supplied
	 * the used-up tools, the infrastructure use, the requirement of operational
	 * resources, the scrap materials and the services 
	 * 
	 * @return
	 */
	public static double getEcoImpactUse() {
		return getEcoImpactUseElectricity() + getEcoImpactUseTools() + getEcoImpactUseInfrastructure()
				+ getEcoImpactUseResources() + getEcoImpactUseScrapMaterials() + getEcoImpactUseServices();
	}

	/**
	 * Returns the eco impact of the required electric energy
	 * 
	 * @return
	 */
	public static double getEcoImpactUseElectricity() {
		return EcoFactorsElectricEnergy.getEcoImpact(getEcoFactor(), getMachine().getElectricityDemand(),
				getScenario());
	}

	/**
	 * retuns the eco impact of the used tools
	 * 
	 * The total eco-impact is the sum of the eco-impacts generated
	 * by the material procurement, the production, the distribution
	 * (packaging and transportation), and the end-of-life actions
	 * 
	 * @return
	 */
	public static double getEcoImpactUseTools() {
		double sum = 0;

		for (InventoryTool t : getTools()) {
			t.updateTransportationWeight();
			sum += t.getNumberOfTools(getScenario())
					* ( 	  EcoFactorsRawMaterial.getEcoImpact(getEcoFactor(), t.getProductMaterials())
							+ EcoFactorsRawMaterial.getEcoImpact(getEcoFactor(), t.getPackagingMaterials())
							+ EcoFactorsProduction.getEcoImpact(getEcoFactor(), t.getProductMaterials())
							+ EcoFactorsTransport.getEcoImpact(getEcoFactor(), t.getDistributionTransport())
							+ EcoFactorsEOL.getEcoImpact(getEcoFactor(), t.getProductMaterials()));
		}

		return sum;
	}

	/**
	 * Returns the eco impact of the infrastructure operation
	 * 
	 * @return
	 */
	public static double getEcoImpactUseInfrastructure() {
		return BuildingEnergyDemand.getEcoImpact(getEcoFactor(), getMachine().getCoolingDemand(), getScenario());
	}

	/**
	 * Returns the eco impact of the operational materials
	 * 
	 * @return
	 */
	public static double getEcoImpactUseResources() {
		return EcoFactorsRawMaterial.getEcoImpact(getEcoFactor(), getMachine().getOperationalMaterials(),
				getScenario());
	}

	/**
	 * Returns the eco impact of the scrap material (e.g. chips)
	 * 
	 * @return
	 */
	public static double getEcoImpactUseScrapMaterials() {
		return EcoFactorsRawMaterial.getEcoImpact(getEcoFactor(), getMachine().getOperationalMaterials(),
				getScenario());
	}

	/**
	 * Returns the eco impact of the services
	 * 
	 * Elements:
	 * - Replaced materials
	 * - Required transportations
	 * 
	 * @return
	 */
	public static double getEcoImpactUseServices() {
		ArrayList<MaterialAmount> serviceMaterials = new ArrayList<>();
		ArrayList<Transportation> serviceTransportation = new ArrayList<>();

		/*
		 * Assumption: During each service, 5% of the machine's mass are repaced
		 */
		for (MaterialAmount m : getMachine().getProductMaterials())
			serviceMaterials.add(new MaterialAmount(m.getMaterial().clone(), m.getAmount() * 0.05, m.getEol()));

		/*
		 * The transportation weight during the service depends on the location of
		 * the service:
		 * - local: Only the service materials
		 * - central: The machine must be transported (2x)
		 */
		double transportationWeight;
		if (getScenario().isLocalService())
			transportationWeight = MaterialAmount.getWeight(serviceMaterials);
		else
			transportationWeight = MaterialAmount.getWeight(getMachine().getProductMaterials()) * 2;

		for (Transportation t : getMachine().getDistributionTransport())
			serviceTransportation.add(new Transportation(t.getType(), t.getDistance(), transportationWeight));

		return EcoFactorsRawMaterial.getEcoImpact(getEcoFactor(), serviceMaterials)
				+ EcoFactorsTransport.getEcoImpact(getEcoFactor(), serviceTransportation);

	}

	/**
	 * Returns the eco impact of the eol treatment of the product materials
	 * 
	 * @return
	 */
	public static double getEcoImpactEOL() {
		return EcoFactorsEOL.getEcoImpact(getEcoFactor(), getMachine().getProductMaterials());
	}

	/**
	 * Returns the detailed results
	 * 
	 * Same as getResultSimple, but with the following details:
	 * 
	 * - RAWMATERIAL
	 * -- Raw materials
	 * - PRODUCTION
	 * -- Production
	 * - SETUP
	 * -- Packaging materials
	 * -- Transportation components 
	 * -- Transportation machine
	 * -- Commissioning
	 * - USE
	 * -- Electric Energy
	 * -- Resources
	 * -- Tools
	 * -- Scrap materials
	 * -- Infrastructure
	 * -- Services
	 * - EOL
	 * 
	 * @return
	 */
	public static LifeCycleResult getResultDetailed() {
		LifeCycleResult result = new LifeCycleResult(EcoFactorProperties.getUnit(getEcoFactor()));

		result.put("Raw materials", LCPhases.RAWMATERIAL, getEcoImpactRawMaterials());
		result.put("Production", LCPhases.PRODUCTION, getEcoImpactProduction());
		result.put("Packaging materials", LCPhases.SETUP, getEcoImpactPackagingMaterials());
		result.put("Transportation components", LCPhases.SETUP, getEcoImpactTransportationComponents());
		result.put("Transportation machine", LCPhases.SETUP, getEcoImpactTransportationMachine());
		result.put("Commissioning", LCPhases.SETUP, getEcoImpactCommissioningMaterials());
		result.put("Electric Energy", LCPhases.USE, getEcoImpactUseElectricity());
		result.put("Resources", LCPhases.USE, getEcoImpactUseResources());
		result.put("Tools", LCPhases.USE, getEcoImpactUseTools());
		result.put("Scrap materials", LCPhases.USE, getEcoImpactUseScrapMaterials());
		result.put("Infrastructure", LCPhases.USE, getEcoImpactUseInfrastructure());
		result.put("Services", LCPhases.USE, getEcoImpactUseServices());
		result.put("EOL", LCPhases.EOL, Math.max(0.0, getEcoImpactEOL()));

		return result;
	}

	/**
	 * Returns the simplified results
	 * 
	 * Returns the eco-impact for each life-phase of the machine
	 * 
	 * @return
	 */
	public static LifeCycleResult getResultSimple() {
		LifeCycleResult result = new LifeCycleResult(EcoFactorProperties.getUnit(getEcoFactor()));

		result.put("Raw materials", LCPhases.RAWMATERIAL, getEcoImpactRawMaterials());
		result.put("Production", LCPhases.PRODUCTION, getEcoImpactProduction());
		result.put("Packaging materials", LCPhases.SETUP, getEcoImpactTransportation());
		result.put("Use", LCPhases.USE, getEcoImpactUse());
		result.put("EOL", LCPhases.EOL, Math.max(0.0, getEcoImpactEOL()));

		return result;
	}
}
