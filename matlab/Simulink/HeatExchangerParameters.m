% HEATEXCHANGER PARAMETERS

% Initial temperature [K]
T_init = 300;

% Mass in the pipe [kg]
m = 100;

% Area of the pipe [m^2]
A = 1;

% Heat capacity of the fluid [J/kg/K]
c_p = 1;

% Heat Resistance [W/K/m^2]
k = 0.1;

% Number of pipe elements [1]
N = 10;
