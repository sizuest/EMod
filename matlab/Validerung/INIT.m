%% INITIALISATION
% Initialisation for the validation fuctions. Bascially the setting of
% machine specific paths.

%% HOMEDIR
% Location of the simulation folder

global HOMEDIR;
HOMEDIR = '../../../4_Simulations/';

%% MEASDIR
% Root folder containing the measuremetns

global MEASDIR;
MEASDIR = '../../../3_Measurements/';

%% Additional functions
addpath('../supportingFunctions');
